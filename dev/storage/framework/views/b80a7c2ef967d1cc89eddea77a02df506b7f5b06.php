<?php $__env->startSection('title', 'Member Register'); ?>
<?php $__env->startSection('content'); ?>
    <?php echo e(Html::style('css/saleme/layout/login-page.css')); ?>

    <div class="container">
        <div class="modal-dialog modal-md omb_login login-register">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center"><span class="no-1"><i
                                    class="lnr lnr-mustache linier-icon"></i> </span> &nbsp; Login / Register</h4>
                </div>
                <div class="col-md-12 pd-0-xs">
                    <div class="panel panel-login">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-6">
                                    <a href="<?php echo e(asset('')); ?>member-login">Login</a>
                                    
                                </div>
                                <div class="col-xs-6">
                                    <a href="<?php echo e(asset('')); ?>member/register" class="active">Register</a>
                                    

                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">

                                    
                                    <div id="register-form">
                                        <div class="col-md-12 reg-success-thumb">
                                            <p class="text-center">
                                                <i class="lnr lnr-thumbs-up thumb-up-image" aria-hidden="true"></i>
                                            </p>
                                        </div>
                                        <div class="messager"></div>
                                        <form id="register_form" method="post" role="form" action="/memberregister">
                                            <?php echo e(csrf_field()); ?>

                                            <div class="form-group">
                                                <input type="text" name="signup_username" id="signup_username"
                                                       tabindex="1"
                                                       class="form-control" placeholder="Name *" value="<?php echo e(old('signup_username')); ?>">
                                            </div>
                                            <div class="form-group">
                                                <input type="email" name="signup_email" id="signup_email" tabindex="1"
                                                       class="form-control" placeholder="Email Address *" value="<?php echo e(old('email')); ?>">
                                            </div>
                                            <div class="form-group position-relative">
                                                <input type="password" name="signup_pass" id="signup_pass" tabindex="2"
                                                       class="form-control" placeholder="Password *">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" name="pass_confirmation" id="pass_confirmation"
                                                       tabindex="2" class="form-control"
                                                       placeholder="Confirm Password *">
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-6 pull-right">
                                                        <input type="submit" name="login-submit" id="btn_submit_signup"
                                                               tabindex="4" class="form-control btn btn-login"
                                                               value="REGISTER">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear-fix"></div>
                                            <div class="col-md-12">
                                                <p class="agree-txt-1">
                                                    By login you agree to
                                                    <a href="<?php echo e(asset('')); ?>terms-conditions"
                                                       title="View Terms & Conditions"
                                                       target="_blank">Terms-Conditions</a>
                                                    and <a href="<?php echo e(asset('')); ?>privacy-policy"
                                                           title="View Privacy Policy"
                                                           target="_blank">Privacy-Policy</a>

                                                </p>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                    
                    
                        
                             
                        
                    
                    
                        
                             
                        
                    
                    
                        
                             
                        
                    
                    
                
                <div class="clear-fix"></div>
            </div>
        </div>
    </div>
    <?php echo e(Html::script('js/saleme/plugin/jquery.validate.js')); ?>

    <script>
        // login form
        $("#memberlogin_form").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                }
            }
        });

        //member register
        $("#register_form").validate({
            rules: {
                signup_username: {
                    required: true,
                },
                signup_phone: {
                    number: true,
                    required: true,
                },
                signup_email: {
                    required: true,
                    email: true
                },
                signup_pass: {
                    required: true,
                },
                pass_confirmation: {
                    required: true,
                    equalTo: "#signup_pass"
                }
            },
            messages: {
                signup_username: {
                    required: "Please enter username",
                },
                signup_phone: {
                    required: "Please enter mobile number",
                },
                signup_email: {
                    required: "Please enter email",
                },
                signup_pass: {
                    required: "Please enter password",
                },
                pass_confirmation: {
                    equalTo: "does not match password",
                }
            }
        });

    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.adddetails', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>