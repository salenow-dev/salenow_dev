<li class="">
    
    <a href="/dashboard">
        <span class="fa fa-dashboard"></span>
        <span class="xn-text">Dashboard</span></a>
</li>
<?php if (Auth::check() && Auth::user()->hasRole('superadmin')): ?>
<li class="xn-openable <?php echo e((Request::segment(1)=='user') ?'active' : ''); ?>">
    <a href="#"><span class="fa fa-user"></span> User</a>
    <ul>
        <li><a href="/user/list"
               class="<?php echo e((Request::segment(1)=='user' && Request::segment(2)=='list')?'active-sub' : ''); ?>"
            ><span class="fa fa-angle-double-right "></span> User List</a></li>
        <li><a href="/user/register"
               class="<?php echo e((Request::segment(1)=='user' && Request::segment(2)=='register')?'active-sub' : ''); ?>"
            ><span class="fa fa-angle-double-right "></span> Register User</a></li>
        <li><a href="/user/auth"
               class="<?php echo e((Request::segment(1)=='user' && Request::segment(2)=='auth')?'active-sub' : ''); ?>"
            ><span class="fa fa-angle-double-right "></span> Authorization</a></li>
    </ul>
</li>
<?php endif; ?>
<li class="xn-openable <?php echo e((Request::segment(1)=='ads-manage')?'active' : ''); ?>">
    <a href="#"><span class="fa fa-tag"></span>Ad's Manager</a>
    <ul>
        <li><a href="/ads-manage/ad-review"
               class="<?php echo e((Request::segment(1)=='ads-manage' && Request::segment(2)=='ad-review')&& Request::segment(3)==''?'active-sub' : ''); ?>"><span
                        class="fa fa-angle-double-right "></span>Review Pending Ads
                <div class="informer informer-info"><?php echo e(adsStatusCount('pending')); ?></div>
            </a></li>
        <li><a href="/ads-manage/publish-ads"
               class="<?php echo e((Request::segment(1)=='ads-manage' && Request::segment(2)=='publish-ads')?'active-sub' : ''); ?>"><span
                        class="fa fa-angle-double-right "></span>Published Ads</a>
            <div class="informer informer-success "><?php echo e(adsStatusCount('confirm')); ?></div>
        </li>
        <li><a href="/ads-manage/spam"
               class="<?php echo e((Request::segment(1)=='ads-manage' && Request::segment(2)=='spam')?'active-sub' : ''); ?>"><span
                        class="fa fa-angle-double-right "></span>Spam Ads</a>
            <div class="informer informer-success"><?php echo e(adsStatusCount('cancel')); ?></div>
        </li>
        <?php if (Auth::check() && Auth::user()->hasRole('superadmin')): ?>
        <li><a href="/ads-manage/ad-review/draft-ads"
               class="<?php echo e((Request::segment(1)=='ads-manage' && Request::segment(2)=='ad-review' && Request::segment(3)=='draft-ads')?'active-sub' : ''); ?>"><span
                        class="fa fa-angle-double-right "></span>Draft Ads
                <div class="informer informer-danger"><?php echo e(adsStatusCount('draft')); ?></div>
            </a></li>
        <?php endif; ?>
        <li><a href="/ads-manage/ad-review/blocked-ads"
               class="<?php echo e((Request::segment(1)=='ads-manage' && Request::segment(2)=='ad-review' && Request::segment(3)=='blocked-ads')?'active-sub' : ''); ?>"><span
                        class="fa fa-angle-double-right "></span>Blocked Ads
                <div class="informer informer-danger"><?php echo e(adsStatusCount('blocked')); ?></div>
            </a></li>
        <?php if (Auth::check() && Auth::user()->hasRole('admin')): ?>
        <li><a href="/ads-manage/ad-review/deleted-ads"
               class="<?php echo e((Request::segment(1)=='ads-manage' && Request::segment(2)=='ad-review' && Request::segment(3)=='blocked-ads')?'active-sub' : ''); ?>"><span
                        class="fa fa-angle-double-right "></span>Deleted Ads
                <div class="informer informer-danger"><?php echo e(adsStatusCount('deleted')); ?></div>
            </a></li>
        <?php endif; ?>
        <li><a href="/ads-manage/boosting/pin-to-top-pending"
               class="<?php echo e((Request::segment(1)=='ads-manage' && Request::segment(2)=='pin-to-top-pending')?'active-sub' : ''); ?>"><span
                        class="fa fa-angle-double-right "></span>Pin-to-Top Pending
                <div class="informer informer-danger"><?php echo e(pintotopRecordsCount('Membervoucher','activated_at')); ?></div>
            </a></li>
        <li><a href="/ads-manage/ad-review/pending-otp"
               class="<?php echo e((Request::segment(1)=='ads-manage' && Request::segment(2)=='ad-review' && Request::segment(3)=='pending-otp')?'active-sub' : ''); ?>"><span
                        class="fa fa-angle-double-right "></span>Pending OTPs
                <div class="informer informer-danger"><?php echo e(pendingOtpCount()); ?></div>
            </a></li>
    </ul>
</li>

<li class="xn-openable <?php echo e((Request::segment(1)=='members')?'active' : ''); ?>">
    <a href="#"><span class="fa fa-users"></span>Careers</a>
    <ul>
        <li><a href="/admin/member/career"><span class="fa fa-angle-double-right "></span>Add New Job</a></li>
        <li><a href="/admin/member/career/edit"><span class="fa fa-angle-double-right "></span>Edit Job</a></li>
        <li><a href="/admin/member/careerview"><span class="fa fa-angle-double-right "></span>View Candidates</a></li>
    </ul>
</li>
<li class="xn-openable <?php echo e((Request::segment(2)=='members')?'active' : ''); ?>">
    <a href="#"><span class="fa fa-users"></span>Members</a>
    <ul>
        <li><a href="/admin/member/premium-request"><span class="fa fa-angle-double-right "></span>Premium Member
                Requests</a></li>
        <li><a href="/admin/member/premium-list"><span class="fa fa-angle-double-right "></span>Premium Member
                List</a></li>
        <li><a href="/admin/member"><span class="fa fa-angle-double-right "></span>All Members List</a></li>
        <li><a href="/admin/agent"><span class="fa fa-angle-double
        -right "></span>Agent</a></li>
        <li><a href="/admin/member/managemember"><span class="fa fa-angle-double-right "></span>Premium Member Search</a></li>
        <li><a href="/admin/member/sendsms"><span class="fa fa-angle-double-right "></span>Send SMS to Member</a></li>
        <li><a href="/ads-manage/assign-ads-pack/view-all"><span class="fa fa-angle-double-right "></span>All Assign Ad Packs</a></li>
        <li><a href="/ads-manage/assign-ads-pack"><span class="fa fa-angle-double-right "></span>Assign Ads Packs</a></li>
    </ul>
</li>
<li class="xn-openable <?php echo e((Request::segment(1)=='agents')?'active' : ''); ?>">
    <a href="#"><span class="fa fa-users"></span>Sells Agent</a>
    <ul>
        <li><a href="/agents/allagents"><span class="fa fa-angle-double-right "></span>All Agents List</a></li>
        <li><a href="/agents/addagent"><span class="fa fa-angle-double-right "></span>Add New Agent</a></li>

    </ul>
</li>
<?php if (Auth::check() && Auth::user()->hasRole('admin')): ?>
<li class="xn-openable <?php echo e((Request::segment(1)=='settings')?'active' : ''); ?>">
    <a href="#"><span class="fa fa-cogs"></span>Settings</a>
    <ul>
        <li><a href="/settings/category"
               class="<?php echo e((Request::segment(1)=='settings' && Request::segment(2)=='category')?'active-sub' : ''); ?>"><span
                        class="fa fa-cog "></span>Main Categories</a></li>
        <li><a href="/settings/subcategory"
               class="<?php echo e((Request::segment(1)=='settings' && Request::segment(2)=='subcategory')?'active-sub' : ''); ?>"><span
                        class="fa fa-cog "></span>Sub Categories</a></li>
        <li><a href="/settings/filters"
               class="<?php echo e((Request::segment(1)=='settings' && Request::segment(2)=='filters')?'active-sub' : ''); ?>"><span
                        class="fa fa-cog "></span>Category Filters</a></li>
        <li><a href="/settings/adtypes"
               class="<?php echo e((Request::segment(1)=='settings' && Request::segment(2)=='adtypes')?'active-sub' : ''); ?>"><span
                        class="fa fa-cog "></span>Advertisement Types</a></li>
        <li><a href="/settings/changebanner"
               class="<?php echo e((Request::segment(1)=='settings' && Request::segment(2)=='changebanner')?'active-sub' : ''); ?>"><span
                        class="fa fa-cog "></span>Change Home Banner</a></li>
    </ul>
</li>
<?php endif; ?>


<?php if (Auth::check() && Auth::user()->hasRole('admin')): ?>
<li class="xn-openable <?php echo e((Request::segment(1)=='playzone')?'active' : ''); ?>">
    <a href="#"><span class="fa fa-play-circle-o"></span>Play Zone</a>
    <ul>
        <li><a href="/playzone/prizes-buy-requests/list/all" style="background: #6e3d3c;"
               class="<?php echo e((Request::segment(1)=='playzone' && Request::segment(2)=='prizes-buy-requests' && Request::segment(3)=='list')?'active-sub' : ''); ?>"><span
                        class="fa fa-bell "></span>Prizes, Items Buy Requests</a></li>
        <li><a href="/playzone/players/list"
               class="<?php echo e((Request::segment(1)=='playzone' && Request::segment(2)=='players' && Request::segment(3)=='list')?'active-sub' : ''); ?>"><span
                        class="fa fa-list "></span>Players List</a></li>
        <li><a href="/playzone/daily-questions/list"
               class="<?php echo e((Request::segment(1)=='playzone' && Request::segment(2)=='daily-questions' && Request::segment(3)=='list')?'active-sub' : ''); ?>"><span
                        class="fa fa-list "></span>Daily Questions List</a></li>
        <li><a href="/playzone/daily-questions/add"
               class="<?php echo e((Request::segment(1)=='playzone' && Request::segment(2)=='daily-questions' && Request::segment(3)=='add')?'active-sub' : ''); ?>"><span
                        class="fa fa-plus "></span>Daily Questions Add</a></li>
        <li><a href="/playzone/extra-questions/list"
               class="<?php echo e((Request::segment(1)=='playzone' && Request::segment(2)=='extra-questions'&& Request::segment(3)=='list')?'active-sub' : ''); ?>"><span
                        class="fa fa-list "></span>Extra Questions List</a></li>
        <li><a href="/playzone/extra-questions/add"
               class="<?php echo e((Request::segment(1)=='playzone' && Request::segment(2)=='extra-questions' && Request::segment(3)=='add')?'active-sub' : ''); ?>"><span
                        class="fa fa-plus "></span>Extra Questions Add</a></li>
        <li><a href="/playzone/prizes/list"
               class="<?php echo e((Request::segment(1)=='playzone' && Request::segment(2)=='prizes' && Request::segment(3)=='list')?'active-sub' : ''); ?>"><span
                        class="fa fa-list "></span>All Prizes List</a></li>
        <li><a href="/playzone/prizes/add"
               class="<?php echo e((Request::segment(1)=='playzone' && Request::segment(2)=='prizes' && Request::segment(3)=='add')?'active-sub' : ''); ?>"><span
                        class="fa fa-plus "></span>Prizes Add</a></li>
    </ul>
</li>
<?php endif; ?>


<?php if (Auth::check() && Auth::user()->hasRole('superadmin')): ?>
<li class="xn-openable <?php echo e((Request::segment(1)=='cache')?'active' : ''); ?>">
    <a href="#"><span class="fa fa-bolt"></span>Cache</a>
    <ul>
        <li><a href="/cache/clear-cache"><span class="fa fa-angle-double-right "></span>Clear Views Cache</a></li>
        <li><a href="/cache/configclear"><span class="fa fa-angle-double-right "></span>Clear Config Cache</a></li>
    </ul>
</li>
<?php endif; ?>