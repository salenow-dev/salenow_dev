<?php if(session()->has('flash_message')): ?>
    <div class="col-md-12 col-sm-12">
        <div class="alert alert-<?php echo e(session('flash_message_level')); ?>">
            <?php echo e(session('flash_message')); ?>

        </div>
    </div>
<?php endif; ?>
