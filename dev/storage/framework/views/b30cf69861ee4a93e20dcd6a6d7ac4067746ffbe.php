<div class="row  ">
    <div class="col-md-12">
        <h4 class="category-titles primary-d">Primary Details</h4>
    </div>
</div>
<div class="row  ">
    <?php echo $__env->make('layouts.frontend.filter_primary', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <select id="itemcondition" class="selectpicker show-tick form-control" name="itemcondition"
                    data-live-search="false">
                <option value="">Select Condition *</option>
                <?php $__currentLoopData = $vehicleconditionlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $condition): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <option value="<?php echo e($condition->id); ?>" <?php echo e((!empty($editAd->itemcondition) && $condition->id==$editAd->itemcondition)?'selected':''); ?>>
                        <?php echo e($condition->condition_name); ?>

                    </option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </select>
        </div>
        <label id="itemcondition-error" class="error" for="itemcondition" style="display: none;"></label>
    </div>
    <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <select id="brand_id" name="brand_id" class="selectpicker form-control show-tick" data-live-search="true">
                <option value="">Select Vehicle Brand *</option>
                <?php $__currentLoopData = $vehiclebrandlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <option value="<?php echo e($brand->id); ?>" <?php echo e((!empty($editAd->brand_id) && $brand->id==$editAd->brand_id)?'selected':''); ?>>
                        <?php echo e($brand->brand_name); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </select>
        </div>
        <label id="brand_id-error" class="error" for="brand_id" style="display: none;"></label>
    </div>
    <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <input type="text" class="form-control" id="model" name="model"
                   value="<?php echo e((!empty($editAd->model))?$editAd->model:''); ?>" placeholder="Model *">
        </div>
        <label id="model-error" class="error" for="model" style="display: none;"></label>
    </div>

</div>
<div class="row  ">
    <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <?php if(!empty($editAd->registry_year)): ?>
                <input type="number" class=" selectpicker show-tick form-control" id="registry_year" name="registry_year"
                       value="<?php echo e((!empty($editAd->registry_year))?$editAd->registry_year:''); ?>" placeholder="E.g 35,000">
            <?php else: ?>
                <select id="registry_year" name="registry_year" class="form-control show-tick form-control"
                        data-live-search="true">
                    <option value="">Select Model Year *</option>
                    <?php
                    $thisyear = date('Y');
                    $yeraslist = range($thisyear, 1970); ?>
                    <?php $__currentLoopData = $yeraslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $year): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <option value="<?php echo e($year); ?>">
                            <?php echo e($year); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                </select>
            <?php endif; ?>
        </div>
        <label id="registry_year-error" class="error" for="registry_year" style="display: none;"></label>
    </div>

    <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <select id="fuel_id" class="selectpicker show-tick form-control" name="fuel_id" data-live-search="false">
                <option value="">Select Fuel Type *</option>
                <?php $__currentLoopData = $vehiclefuellist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fuel): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <option value="<?php echo e($fuel->id); ?>" <?php echo e((!empty($editAd->fuel_id) && $fuel->id==$editAd->fuel_id)?'selected':''); ?>>
                        <?php echo e($fuel->fuel_name); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </select>
        </div>
        <label id="fuel_id-error" class="error" for="fuel_id" style="display: none;"></label>
    </div>
    <div class="col-md-4 col-sm-4" >
        <div class="form-group">
            <select id="tranmission_id" class="selectpicker show-tick form-control" name="tranmission_id"
                    data-live-search="false">
                <option value=""> Select Transmission Type *</option>
                <?php $__currentLoopData = $vehicletransmissionslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trans): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <option value="<?php echo e($trans->id); ?>" <?php echo e((!empty($editAd->tranmission_id) && $trans->id==$editAd->tranmission_id)?'selected':''); ?>>
                        <?php echo e($trans->transmission_name); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </select>
        </div>
        <label id="tranmission_id-error" class="error" for="tranmission_id" style="display: none;"></label>
    </div>

</div>
<div class="row ">
    <div class="col-md-4 col-sm-4">
        <div class="form-group">
            <select class="selectpicker bodytype" name="bodytype_id" id="bodytype_id">
                <option value="">Select Body Type *</option>
                <?php $__currentLoopData = $vehiclebodytype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bodytype): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <option value="<?php echo e($bodytype->id); ?>"
                            <?php echo e((!empty($editAd->bodytype_id) && $bodytype->id==$editAd->bodytype_id)?'selected':''); ?>

                            data-content='<img src="<?php echo e(asset('')); ?>images/saleme/icons/<?php echo str_replace(' ', '-', strtolower($bodytype->type_name)) ?>.png"/> <span>&nbsp;<?php echo e($bodytype->type_name); ?></span>'></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </select>
        </div>
        <label id="bodytype_id-error" class="error" for="bodytype_id" style="display: none;"></label>
    </div>
    <div class="col-md-4 mar-top-10-xs col-sm-4">
        <div class="form-group">
            <div class="input-group">
                <input type="number" class="form-control" id="mileage" name="mileage"
                       value="<?php echo e((!empty($editAd->mileage))?$editAd->mileage:''); ?>" placeholder="Mileage *" min="0"/>
                <span class="input-group-addon">Km</span>
            </div>
            <label id="mileage-error" class="error" for="mileage"></label>
        </div>
    </div>

    <div class="col-md-4 mar-top-10-xs col-sm-4">
        <div class="form-group">
            <div class="input-group">
                <input type="number" class="form-control" id="enginesize" name="enginesize" min="0"
                       value="<?php echo e((!empty($editAd->enginesize))?$editAd->enginesize:''); ?>" placeholder="Engine Capacity *">
                <span class="input-group-addon">CC</span>
            </div>
            <label id="mileage-error" class="error" for="enginesize"></label>
        </div>
    </div>
    
    
    
    
    
    
    <div class="clear-fix "></div>
</div>
<div class="row mar-top-20 hover-eee">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading col-panel-heading" data-toggle="collapse" data-parent="#accordion"
                 href="#cars">
                <h4 class="panel-title category-title2">
                    Value Added Features
                    <span class="ion-ios-plus-outline pull-right"></span>
                </h4>
            </div>
            <div id="cars" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="row">
                        <?php if($vehicleoptions): ?>
                            <?php $i = 1;
                            $check = array();
                            if (!empty($editAd->features)) {
                                foreach ($editAd->features as $feature) {
                                    $check[$feature] = true;
                                }
                            }
                            ?>
                            <?php $__currentLoopData = $vehicleoptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $voptions): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <div class="col-md-3 col-xs-6 col-sm-3">
                                    <div class="checkbox checkbox-primary">
                                        <input id="vl-ad<?php echo e($i); ?>" name="option[]"
                                               value="<?php echo e($voptions->id); ?>" type="checkbox"
                                                <?php echo e((isset($check[$voptions->id]))?'checked':''); ?>>
                                        <label for="vl-ad<?php echo e($i); ?>"> <?php echo e($voptions->display_name); ?> </label>
                                    </div>
                                </div>
                                <?php $i++ ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
