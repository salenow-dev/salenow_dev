<?php $__env->startSection('title', '| Member Requests for Become Premium'); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3><span class="fa fa-mail-forward"></span> Add Home Page Banner</h3>
                    <h5 class="text-danger">Home Banner Size must be <b>width: 1505px</b> and <b>Height: 560px</b></h5>
                    <h5 class="text-danger">Playzone Banner Size must be <b>width: 2048px</b> and <b>Height: 1152px</b></h5>
                    <h5 class="text-danger">Playzone Inner Banner Size must be <b>width: 1440px</b> and <b>Height: 960px</b></h5>

                    <p>Add class <code>file</code> to file input to get Bootstrap FileInput plugin</p>
                    <form enctype="multipart/form-data" method="post" action="uploadbanner">
                        <?php echo e(csrf_field()); ?>

                        <div class="form-group">
                            <div class="col-md-3">
                                <div class="input-group">
                                    <input class="form-control" name="file" type="file" id="file" required>

                                </div> <!-- input-group.// -->
                            </div>
                            <div class="col-md-1">
                                <div class="input-group">
                                    <b>Location:</b>
                                </div> <!-- input-group.// -->
                            </div>
                            <div class="col-md-1">
                                <div class="input-group">
                                    <input type="radio" name="location" value="home" checked>  Home

                                </div> <!-- input-group.// -->
                            </div>
                            <div class="col-md-1">
                                <div class="input-group">
                                    <input type="radio" name="location" value="playzone">  Playzone

                                </div> <!-- input-group.// -->
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <input type="radio" name="location" value="playzone_inner">  Playzone Inner

                                </div> <!-- input-group.// -->
                            </div>
                        </div> <!-- form-group// -->
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary  btn-md btn-block" value="submit">
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3><span class="fa fa-mail-forward"></span>All Home Banners</h3>


                    <div class="table-responsive">

                        <table class="table table-bordered table-striped table-actions">
                            <thead>
                            <tr>

                                <th>Image</th>
                                <th width="130">Location</th>
                                <th width="70">Status</th>
                                <th width="130">Date</th>
                                <th width="50">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(count($banners)): ?>
                                <?php $__currentLoopData = $banners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $images): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <tr>
                                        <td class=""><img
                                                    src="<?php echo e(asset('images/homepage/main-banner')); ?>/<?php echo e($images->filename); ?>"
                                                    width="40%"></td>
                                        <td><?php echo e(ucwords($images->location)); ?></td>
                                        <td class=text-center">
                                            <?php if($images->status): ?>
                                                <label class="switch">
                                                    <input type="checkbox" checked="" value="1" name="autoCrop"
                                                           onclick="changeStatus(<?php echo e($images->id); ?>,0)">
                                                    <span></span>
                                                </label>
                                            <?php else: ?>
                                                <label class="switch">
                                                    <input type="checkbox" value="1" name="autoCrop"
                                                           onclick="changeStatus(<?php echo e($images->id); ?>,1)">
                                                    <span></span>
                                                </label>
                                            <?php endif; ?>
                                        </td>
                                        <td class=text-center"><?php echo e($images->created_at->todatestring()); ?></td>
                                        <td class=text-center">

                                            <button class="btn btn-danger btn-rounded btn-condensed btn-sm"
                                                    onclick="deleteImage(<?php echo e($images->id); ?>)"><span
                                                        class="fa fa-times"></span>Delete
                                            </button>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            <?php endif; ?>


                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function changeStatus(id, status) {
            var data = {'id': id, 'status': status};
            $.ajax({
                type: "post",
                url: '/settings/banners/change-status/' + id,
                data: data,
                success: function (res) {
                    if (res.status) {

                    }
                }
            });
        }

        function deleteImage(id) {

            var x = confirm('do you need to delete ?');
            if (x) {

                var data = {'id': id};
                $.ajax({
                    type: "post",
                    url: '/settings/banners/delete-image/' + id,
                    data: data,
                    success: function (res) {
                        if (res.status) {
                            noty({
                                text: res.message,
                                layout: 'topRight',

                            })
                            setTimeout(function () {
                                location.reload();
                            }, 1000)
                        }

                    }
                });
            }
            else {
                return false;
            }

        }


    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>