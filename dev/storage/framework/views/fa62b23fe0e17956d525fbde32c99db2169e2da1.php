<div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
    <label class="col-md-3 col-xs-12 control-label">Name</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <?php echo Form::text('name', null, ['class' => 'form-control','required']); ?>

            <?php if($errors->has('name')): ?>
                <span class="help-block"><strong><?php echo e($errors->first('name')); ?></strong></span>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php if(!$editMode): ?>
    <div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
        <label class="col-md-3 col-xs-12 control-label">User Roles</label>
        <div class="col-md-6 col-xs-12">
            <?php if(!empty($roles)): ?>
                <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <div>
                        <div class="checkbox checkbox-primary">
                            <?php echo Form::checkbox('role[]',$role->slug,!empty($user)?$user->hasRole($role->slug):null ,['id' =>$role->name]); ?>

                            <label for="<?php echo e($role->name); ?>"> <?php echo e($role->name); ?></label>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
<div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
    <label class="col-md-3 col-xs-12 control-label">Email</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-envelope"></span></span>
            <?php echo Form::text('email', null, ['class' => 'form-control','required', ($editMode)?'readonly':'']); ?>

            <?php if($errors->has('email')): ?>
                <span class="help-block"><strong><?php echo e($errors->first('email')); ?></strong></span>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
    <label class="col-md-3 col-xs-12 control-label">Password</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
            <?php echo Form::password('password', ['class' => 'form-control','required']); ?>

            <?php if($errors->has('password')): ?>
                <span class="help-block"><strong><?php echo e($errors->first('password')); ?></strong></span>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Confirm Password</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
            <?php echo Form::password('password_confirmation', ['class' => 'form-control','required']); ?>

        </div>
    </div>
</div>