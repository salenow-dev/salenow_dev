<?php $__env->startSection('title', 'Dashboard'); ?>
<?php $__env->startSection('content'); ?>
    <!-- START WIDGETS -->
    <div class="row">
        <div class="col-md-3">
            <!-- START WIDGET SLIDER -->
            <div class="widget widget-default widget-carousel">
                <div class="owl-carousel" id="owl-example">
                    <div>
                        <div class="widget-title">Today Members</div>
                        <div class="widget-subtitle"><?php echo e(\Carbon\Carbon::now()->toFormattedDateString()); ?></div>
                        <div class="widget-int"><?php echo e($todayMemberCount); ?></div>
                    </div>

                </div>
            </div>
            <!-- END WIDGET SLIDER -->
        </div>
        <div class="col-md-3">
            <!-- START WIDGET MESSAGES -->
            <div class="widget widget-default widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-users"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?php echo e($memberCount); ?></div>
                    <div class="widget-title">Registred Members</div>
                    <div class="widget-subtitle">in SaleMe.lk</div>
                </div>

            </div>
            <!-- END WIDGET MESSAGES -->
        </div>
        <div class="col-md-3">
            <!-- START WIDGET REGISTRED -->
            <div class="widget widget-default widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-tags"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?php echo e($todatAds); ?></div>
                    <div class="widget-title">Today Ads</div>
                    <div class="widget-subtitle">On SaleMe.lk</div>
                </div>

            </div>
            <!-- END WIDGET REGISTRED -->
        </div>
        <div class="col-md-3">
            <!-- START WIDGET CLOCK -->
            <div class="widget widget-danger widget-padding-sm">
                <div class="widget-big-int plugin-clock">00:00</div>
                <div class="widget-subtitle plugin-date">Loading...</div>
                
                
                
                
                
                
                
                
                
                
                
            </div>
            <!-- END WIDGET CLOCK -->
        </div>
    </div>
    <!-- END WIDGETS -->
    <div class="row">
        <div class="col-md-3">
            <!-- START PROJECTS BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Ads Count Analysis</h3>
                        <span>Last Two Weeks</span>
                    </div>

                </div>
                <div class="panel-body panel-body-table">
                    <div class="table-responsive">
                        <table class="table table-condensed table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="50%">Date</th>
                                <th width="20%">Ads</th>
                                
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($pastWeekAds)): ?>
                                
                                <?php $__currentLoopData = $pastWeekAds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <?php $myDate = new \Carbon\Carbon($item->date); ?>
                                    
                                    <tr>
                                        <td><strong><?php echo e($myDate->format('M d, l')); ?></strong></td>
                                        <td><span class="label label-danger"><?php echo e($item->count); ?></span></td>
                                        
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END PROJECTS BLOCK -->
        </div>
        <div class="col-md-6">

            <!-- START LINE CHART -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Ads Count Graph</h3>
                        <span>Last Two Weeks</span>
                    </div>
                </div>
                <div class="panel-body">
                    <div id="morris-line-example" style="height: 400px;"></div>
                </div>
            </div>
            <!-- END LINE CHART -->

        </div>
        <div class="col-md-3">
            <!-- START PROJECTS BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Premium Members</h3>
                        <span>Expired as at <b><?php echo e(\Carbon\Carbon::now()->toFormattedDateString()); ?></b></span>
                    </div>

                </div>
                <div class="panel-body panel-body-table">
                    <div class="table-responsive">
                        <table class="table table-condensed table-bordered table-striped">
                            
                            
                                
                                
                            
                            
                            <tbody>
                            <?php if(count($expiredMembers)): ?>
                                <?php $x = 1; ?>
                                <?php $__currentLoopData = $expiredMembers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mem): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <tr>
                                        <td><?php echo e($x); ?></td>
                                        <td>
                                            Name : <b><?php echo e($mem->company_name); ?></b><br>
                                            Contact : <b>
                                                <?php if(count($mem->mobile)): ?>
                                                    <?php $__currentLoopData = $mem->mobile; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mob): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                        <?php echo e($mob); ?> /
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                                <?php endif; ?>
                                            </b><br>
                                            Expired On : <b>
                                                <?php
                                                $expiredate = \Carbon\Carbon::parse($mem->expire_on);
                                                ?>
                                                <?php echo e($expiredate->toFormattedDateString()); ?></b><br>
                                            <span class="text-danger pull-right"><?php echo e($expiredate->diffInDays(\Carbon\Carbon::now())); ?>

                                                days Ago</span>
                                        </td>
                                    </tr>
                                    <?php $x++; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            

            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <span>Expired as at <b><?php echo e(\Carbon\Carbon::now()->toFormattedDateString()); ?></b></span>
                    </div>

                </div>
                <div class="panel-body panel-body-table">
                    <div class="table-responsive">
                        <table class="table table-condensed table-bordered table-striped">
                            <tbody>
                            <?php if(count($expirThisWeek)): ?>
                                <?php $x = 1; ?>
                                <?php $__currentLoopData = $expirThisWeek; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mem): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <tr>
                                        <td><?php echo e($x); ?></td>
                                        <td>
                                            Name : <b><?php echo e($mem->company_name); ?></b><br>
                                            Contact : <b>
                                                <?php if(count($mem->mobile)): ?>
                                                    <?php $__currentLoopData = $mem->mobile; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mob): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                        <?php echo e($mob); ?> /
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                                <?php endif; ?>
                                            </b><br>
                                            Expired On : <b>
                                                <?php
                                                $expiredate = \Carbon\Carbon::parse($mem->expire_on);
                                                ?>
                                                <?php echo e($expiredate->toFormattedDateString()); ?></b><br>
                                            <span class="text-success pull-right">Will expire <?php echo e($expiredate->diffInDays(\Carbon\Carbon::now())); ?>

                                                Within days</span>
                                        </td>
                                    </tr>
                                    <?php $x++; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END PROJECTS BLOCK -->
        </div>

    </div>


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    <!-- START DASHBOARD CHART -->
    
    
    
    
    
    
    <!-- END DASHBOARD CHART -->
    <script type="text/javascript" src="<?php echo e(asset("backend/assets/js/plugins/morris/raphael-min.js")); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset("backend/assets/js/plugins/morris/morris.min.js")); ?>"></script>
    <script type="text/javascript">
        var morrisCharts = function () {

            Morris.Line({
                element: 'morris-line-example',
                data: [
                        <?php $__currentLoopData = $pastWeekAds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    {
                        y: '<?php echo e($item->date); ?>', a: '<?php echo e($item->count); ?>'
                    },
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                ],
                xkey: 'y',
                ykeys: ['a'],
                labels: ['Ads'],
                resize: true,
                lineColors: ['#33414E']
            });
        }();
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backend.backend_dashboard_layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>