<?php $__env->startSection('title', '| Category Filters'); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <?php echo Form::model($filter, ['url'=>'settings/filters/'.$filter->id,'class'=>'form-horizontal','autocomplete' => 'off']); ?>

            <?php echo e(method_field('PATCH')); ?>

            <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Category Filters</strong> | Update Record</h3>
                    </div>
                    <div class="panel-body">
                        <?php echo $__env->make('backend.categoryfilters.form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-default">Clear Form</button>
                        <button class="btn btn-primary pull-right">Submit</button>
                    </div>
                </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>