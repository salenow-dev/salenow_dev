<?php $__env->startSection('title', ' Post Ad Review | SaleMe.lk'); ?>
<?php $__env->startSection('content'); ?>
<section>
    <div class="container cont-bg ">
        <div class="row">
            <div class="mar-review">
                <div class="text-center top-mar-50">
                    <h2 class="review-headings">Thanks for posting your ad on Saleme.lk</h2>
                    <h4>(Ad Ref: <?php echo $_GET['ref']; ?>)</h4>
                    <p class="review-text">Your Ad will be reviewed shortly.
                        <br>Once approved, it will go live and be visible on the site.</p>

                    <div class="top-mar-20">
<!--                        <img src="assets/img/rocket.gif" class="top-mar-20">-->
                           <?php echo e(Html::image('images/salenow/rocket.gif', 'saleme.lk', array('class' => 'img-responsive  top-mar-20'))); ?>

                              <?php echo e(Html::image('images/salenow/clouds-bg.png', 'saleme.lk', array('class' => 'img-responsive clouds1'))); ?>

                    </div>
                </div>
                    <?php echo e(Html::image('images/salenow/banner.gif', 'saleme.lk', array('class' => 'img-responsive'))); ?>

                    <div class="top-mar-50 hidden-sm"></div>
            </div>
        </div>
    </div>
</section>
<!--category icons-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.saleme-shortfooter', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>