<?php $__env->startSection('title', 'User Autherization'); ?>
<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>User</strong> Authorization</h3>

                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <th scope="row">Name</th>
                                <th>E-Mail</th>
                                <?php if(!empty($roles)): ?>
                                    <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <th><?php echo e($role->name); ?></th>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                <?php endif; ?>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                <?php if(!empty($roles)): ?>
                                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <?php $rand = rand(0, 1000); ?>
                                        <tr>
                                            <?php echo Form::open(['url' => 'user/assignroles']); ?>

                                            <td><?php echo e($user->name); ?></td>
                                            <td><?php echo e($user->email); ?> <input type="hidden" name="email"
                                                                          value="<?php echo e($user->email); ?>">
                                            </td>
                                            <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                <td>
                                                    
                                                    

                                                    <div class="checkbox checkbox-primary">
                                                        <input id="<?php echo e($user->id.'_'.$role->id); ?>" type="checkbox"
                                                               <?php echo e($user->hasRole($role->slug) ? 'checked' : ''); ?> name="role[]"
                                                               value="<?php echo e($role->slug); ?>">
                                                        <label for="<?php echo e($user->id.'_'.$role->id); ?>"><?php echo e($role->name); ?></label>
                                                    </div>
                                                </td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                            <td>
                                                <input type="submit" value="Save" class="btn btn-success">
                                            </td>
                                            <?php echo Form::close(); ?>

                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>