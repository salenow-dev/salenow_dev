<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Name</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <?php echo Form::text('category_name', null, ['class' => 'form-control']); ?>

        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Sequence</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <?php echo Form::text('sequence', null, ['class' => 'form-control']); ?>

        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Referance</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <?php echo Form::text('category_code', null, ['class' => 'form-control']); ?>

        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Icon</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            <?php echo Form::text('icon', null, ['class' => 'form-control']); ?>

        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Status</label>
    <div class="col-md-6 col-xs-12">
        <?php echo Form::select('status', array('active'=>'Active','inactive'=>'Inactive'), 'active', ['class' => 'form-control' ]); ?>

    </div>
</div>