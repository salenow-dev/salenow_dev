<?php
$filtersnew = $filters;
unset($filtersnew['Item condition']);
$filterscont = count($filtersnew);

?>

<?php echo $__env->make('layouts.filters.add_type', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div id="elec_condition">
    <?php $__currentLoopData = $filters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filterName => $filterValues): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        <?php if($filterName == 'Item condition'): ?>
            <div class="condition-div">
                <p class="section-title">Condition</p>
                
                <?php $__currentLoopData = $filterValues; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $filtervalue): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <label><input value="<?php echo e($id); ?>" name="comn[filter_condition]"
                                  <?php if(!empty($querydata['comn_data']['filter_condition'])): ?> <?php if($querydata['comn_data']['filter_condition'] == $id): ?> <?php echo e('checked'); ?> <?php endif; ?> <?php endif; ?> type="radio"> <?php echo e($filtervalue); ?>

                    </label>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                <hr class="fade-line">
            </div>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
</div>
<?php if($filterscont > 0): ?>
    <div class="sort-div">
        <p class="section-title">Filter By</p>
        
        <?php $__currentLoopData = $filters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filterName => $filterValues): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <?php if($filterName == 'Item type'): ?>
                <div class="col-sm-12 col-xs-12 pd-0-xs filter-brand-div">
                    <div class="panel filter-pannel">
                        <div class="panel-heading filter-heading text-left clickable panel-collapsed ">
                            <h3 class="panel-title clickable panel-collapsed pull-left"><?php echo e($filterName); ?></h3>
                            <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                            <div class="clear-fix"></div>
                        </div>

                        <div class="panel-body filter-body" id="elec_service_type"
                             style="<?php echo e(!empty($querydata['comn_data']['item_type'])?'display:block;':'display: none;'); ?>">
                            <?php $__currentLoopData = $filterValues; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $filtervalue): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <div class="checkbox  checkbox-primary">
                                    <input id="<?php echo e($id); ?>"
                                           <?php if(!empty($querydata['comn_data']['item_type']) && is_array($querydata['comn_data']['item_type'])): ?>  <?php if(in_array($id,$querydata['comn_data']['item_type'])): ?> <?php echo e('checked'); ?><?php endif; ?> <?php endif; ?> name="comn[item_type][]"
                                           value="<?php echo e($id); ?>" type="checkbox">
                                    <label for="<?php echo e($id); ?>">&nbsp;<?php echo e($filtervalue); ?></label>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>

                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        <?php $__currentLoopData = $filters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filterName => $filterValues): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <?php if($filterName == 'Gender'): ?>
                <div class="col-sm-12 col-xs-12 pd-0-xs filter-brand-div">
                    <div class="panel filter-pannel">
                        <div class="panel-heading filter-heading text-left clickable panel-collapsed ">
                            <h3 class="panel-title clickable panel-collapsed pull-left"><?php echo e($filterName); ?></h3>
                            <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                            <div class="clear-fix"></div>
                        </div>

                        <div class="panel-body filter-body" id="elec_service_type"
                             style="<?php echo e(!empty($querydata['comn_data']['gender'])?'display:block;':'display: none;'); ?>">
                            <?php $__currentLoopData = $filterValues; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $filtervalue): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <div class="checkbox  checkbox-primary">
                                    <input id="<?php echo e($id); ?>"
                                           <?php if(!empty($querydata['comn_data']['gender']) && is_array($querydata['comn_data']['gender'])): ?>  <?php if(in_array($id,$querydata['comn_data']['gender'])): ?> <?php echo e('checked'); ?><?php endif; ?> <?php endif; ?> name="comn[gender][]"
                                           value="<?php echo e($id); ?>" type="checkbox">
                                    <label for="<?php echo e($id); ?>">&nbsp;<?php echo e($filtervalue); ?></label>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>

                        </div>


                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        
        
        
        <?php $__currentLoopData = $filters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filterName => $filterValues): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <?php if($filterName == 'Type'): ?>
                <div class="col-sm-12 col-xs-12 pd-0-xs filter-brand-div">
                    <div class="panel filter-pannel">
                        <div class="panel-heading filter-heading text-left clickable panel-collapsed ">
                            <h3 class="panel-title clickable panel-collapsed pull-left"><?php echo e($filterName); ?></h3>
                            <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                            <div class="clear-fix"></div>
                        </div>
                        <div class="panel-body filter-body" id="elec_service_type"
                             style="<?php echo e(!empty($querydata['comn_data']['type'])?'display:block;':'display: none;'); ?>">
                            <?php $__currentLoopData = $filterValues; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $filtervalue): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <div class="checkbox  checkbox-primary">
                                    <input id="<?php echo e($id); ?>"
                                           <?php if(!empty($querydata['comn_data']['type']) && is_array($querydata['comn_data']['type'])): ?>  <?php if(in_array($id,$querydata['comn_data']['type'])): ?> <?php echo e('checked'); ?><?php endif; ?> <?php endif; ?> name="comn[type][]"
                                           value="<?php echo e($id); ?>" type="checkbox">
                                    <label for="<?php echo e($id); ?>">&nbsp;<?php echo e($filtervalue); ?></label>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        
        <?php $__currentLoopData = $filters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filterName => $filterValues): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <?php if($filterName == 'Number of rooms'): ?>
                <div class="col-sm-12 col-xs-12 pd-0-xs filter-brand-div">
                    <div class="panel filter-pannel">
                        <div class="panel-heading filter-heading text-left clickable panel-collapsed ">
                            <h3 class="panel-title clickable panel-collapsed pull-left"><?php echo e($filterName); ?></h3>
                            <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                            <div class="clear-fix"></div>
                        </div>
                        <div class="panel-body filter-body" id="elec_service_type"
                             style="<?php echo e(!empty($querydata['comn_data']['numberofrooms'])?'display:block;':'display: none;'); ?>">
                            <?php $__currentLoopData = $filterValues; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $filtervalue): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <div class="checkbox  checkbox-primary">
                                    <input id="<?php echo e($id); ?>"
                                           <?php if(!empty($querydata['comn_data']['numberofrooms']) && is_array($querydata['comn_data']['numberofrooms'])): ?>  <?php if(in_array($id,$querydata['comn_data']['numberofrooms'])): ?> <?php echo e('checked'); ?><?php endif; ?> <?php endif; ?> name="comn[numberofrooms][]"
                                           value="<?php echo e($id); ?>" type="checkbox">
                                    <label for="<?php echo e($id); ?>">&nbsp;<?php echo e($filtervalue); ?></label>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        <?php $__currentLoopData = $filters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filterName => $filterValues): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <?php if($filterName == 'Number of guest'): ?>
                <div class="col-sm-12 col-xs-12 pd-0-xs filter-brand-div">
                    <div class="panel filter-pannel">
                        <div class="panel-heading filter-heading text-left clickable panel-collapsed ">
                            <h3 class="panel-title clickable panel-collapsed pull-left"><?php echo e($filterName); ?></h3>
                            <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                            <div class="clear-fix"></div>
                        </div>
                        <div class="panel-body filter-body" id="elec_service_type"
                             style="<?php echo e(!empty($querydata['comn_data']['numberofguest'])?'display:block;':'display: none;'); ?>">
                            <?php $__currentLoopData = $filterValues; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $filtervalue): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <div class="checkbox  checkbox-primary">
                                    <input id="<?php echo e($id); ?>"
                                           <?php if(!empty($querydata['comn_data']['numberofguest']) && is_array($querydata['comn_data']['numberofguest'])): ?>  <?php if(in_array($id,$querydata['comn_data']['numberofguest'])): ?> <?php echo e('checked'); ?><?php endif; ?> <?php endif; ?> name="comn[numberofguest][]"
                                           value="<?php echo e($id); ?>" type="checkbox">
                                    <label for="<?php echo e($id); ?>">&nbsp;<?php echo e($filtervalue); ?></label>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        <?php $__currentLoopData = $filters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filterName => $filterValues): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <?php if($filterName == 'Beds'): ?>
                <div class="col-sm-12 col-xs-12 pd-0-xs filter-brand-div">
                    <div class="panel filter-pannel">
                        <div class="panel-heading filter-heading text-left clickable panel-collapsed ">
                            <h3 class="panel-title clickable panel-collapsed pull-left"><?php echo e($filterName); ?></h3>
                            <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                            <div class="clear-fix"></div>
                        </div>
                        <div class="panel-body filter-body" id="elec_service_type"
                             style="<?php echo e(!empty($querydata['comn_data']['beds'])?'display:block;':'display: none;'); ?>">
                            <?php $__currentLoopData = $filterValues; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $filtervalue): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <div class="checkbox  checkbox-primary">
                                    <input id="<?php echo e($id); ?>"
                                           <?php if(!empty($querydata['comn_data']['beds']) && is_array($querydata['comn_data']['beds'])): ?>  <?php if(in_array($id,$querydata['comn_data']['beds'])): ?> <?php echo e('checked'); ?><?php endif; ?> <?php endif; ?> name="comn[beds][]"
                                           value="<?php echo e($id); ?>" type="checkbox">
                                    <label for="<?php echo e($id); ?>">&nbsp;<?php echo e($filtervalue); ?></label>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        <?php $__currentLoopData = $filters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filterName => $filterValues): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <?php if($filterName == 'Bed size'): ?>
                <div class="col-sm-12 col-xs-12 pd-0-xs filter-brand-div">
                    <div class="panel filter-pannel">
                        <div class="panel-heading filter-heading text-left clickable panel-collapsed ">
                            <h3 class="panel-title clickable panel-collapsed pull-left"><?php echo e($filterName); ?></h3>
                            <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                            <div class="clear-fix"></div>
                        </div>
                        <div class="panel-body filter-body" id="elec_service_type"
                             style="<?php echo e(!empty($querydata['comn_data']['bedsize'])?'display:block;':'display: none;'); ?>">
                            <?php $__currentLoopData = $filterValues; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $filtervalue): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <div class="checkbox  checkbox-primary">
                                    <input id="<?php echo e($id); ?>"
                                           <?php if(!empty($querydata['comn_data']['bedsize']) && is_array($querydata['comn_data']['bedsize'])): ?>  <?php if(in_array($id,$querydata['comn_data']['bedsize'])): ?> <?php echo e('checked'); ?><?php endif; ?> <?php endif; ?> name="comn[bedsize][]"
                                           value="<?php echo e($id); ?>" type="checkbox">
                                    <label for="<?php echo e($id); ?>">&nbsp;<?php echo e($filtervalue); ?></label>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        <?php $__currentLoopData = $filters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filterName => $filterValues): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <?php if($filterName == 'Bath rooms'): ?>
                <div class="col-sm-12 col-xs-12 pd-0-xs filter-brand-div">
                    <div class="panel filter-pannel">
                        <div class="panel-heading filter-heading text-left clickable panel-collapsed ">
                            <h3 class="panel-title clickable panel-collapsed pull-left"><?php echo e($filterName); ?></h3>
                            <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                            <div class="clear-fix"></div>
                        </div>
                        <div class="panel-body filter-body" id="elec_service_type"
                             style="<?php echo e(!empty($querydata['comn_data']['bathrooms'])?'display:block;':'display: none;'); ?>">
                            <?php $__currentLoopData = $filterValues; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $filtervalue): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <div class="checkbox  checkbox-primary">
                                    <input id="<?php echo e($id); ?>"
                                           <?php if(!empty($querydata['comn_data']['bathrooms']) && is_array($querydata['comn_data']['bathrooms'])): ?>  <?php if(in_array($id,$querydata['comn_data']['bathrooms'])): ?> <?php echo e('checked'); ?><?php endif; ?> <?php endif; ?> name="comn[bathrooms][]"
                                           value="<?php echo e($id); ?>" type="checkbox">
                                    <label for="<?php echo e($id); ?>">&nbsp;<?php echo e($filtervalue); ?></label>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        <?php $__currentLoopData = $filters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filterName => $filterValues): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <?php if($filterName == 'Bath room types'): ?>
                <div class="col-sm-12 col-xs-12 pd-0-xs filter-brand-div">
                    <div class="panel filter-pannel">
                        <div class="panel-heading filter-heading text-left clickable panel-collapsed ">
                            <h3 class="panel-title clickable panel-collapsed pull-left"><?php echo e($filterName); ?></h3>
                            <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                            <div class="clear-fix"></div>
                        </div>
                        <div class="panel-body filter-body" id="elec_service_type"
                             style="<?php echo e(!empty($querydata['comn_data']['bathroomtypes'])?'display:block;':'display: none;'); ?>">
                            <?php $__currentLoopData = $filterValues; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $filtervalue): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <div class="checkbox  checkbox-primary">
                                    <input id="<?php echo e($id); ?>"
                                           <?php if(!empty($querydata['comn_data']['bathroomtypes']) && is_array($querydata['comn_data']['bathroomtypes'])): ?>  <?php if(in_array($id,$querydata['comn_data']['bathroomtypes'])): ?> <?php echo e('checked'); ?><?php endif; ?> <?php endif; ?> name="comn[bathroomtypes][]"
                                           value="<?php echo e($id); ?>" type="checkbox">
                                    <label for="<?php echo e($id); ?>">&nbsp;<?php echo e($filtervalue); ?></label>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        
        <?php $__currentLoopData = $filters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filterName => $filterValues): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <?php if($filterName == 'Occupancy'): ?>
                <div class="col-sm-12 col-xs-12 pd-0-xs filter-brand-div">
                    <div class="panel filter-pannel">
                        <div class="panel-heading filter-heading text-left clickable panel-collapsed ">
                            <h3 class="panel-title clickable panel-collapsed pull-left"><?php echo e($filterName); ?></h3>
                            <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                            <div class="clear-fix"></div>
                        </div>
                        <div class="panel-body filter-body" id="elec_service_type"
                             style="<?php echo e(!empty($querydata['comn_data']['occupancy'])?'display:block;':'display: none;'); ?>">
                            <?php $__currentLoopData = $filterValues; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $filtervalue): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <div class="checkbox  checkbox-primary">
                                    <input id="<?php echo e($id); ?>"
                                           <?php if(!empty($querydata['comn_data']['occupancy']) && is_array($querydata['comn_data']['occupancy'])): ?>  <?php if(in_array($id,$querydata['comn_data']['occupancy'])): ?> <?php echo e('checked'); ?><?php endif; ?> <?php endif; ?> name="comn[occupancy][]"
                                           value="<?php echo e($id); ?>" type="checkbox">
                                    <label for="<?php echo e($id); ?>">&nbsp;<?php echo e($filtervalue); ?></label>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if($filterName == 'Size'): ?>
                <div class="col-sm-12 col-xs-12 pd-0-xs filter-brand-div">
                    <div class="panel filter-pannel">
                        <div class="panel-heading filter-heading text-left clickable panel-collapsed ">
                            <h3 class="panel-title clickable panel-collapsed pull-left"><?php echo e($filterName); ?></h3>
                            <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                            <div class="clear-fix"></div>
                        </div>
                        <div class="panel-body filter-body" id="elec_service_type"
                             style="<?php echo e(!empty($querydata['comn_data']['size'])?'display:block;':'display: none;'); ?>">
                            <?php $__currentLoopData = $filterValues; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $filtervalue): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <div class="checkbox  checkbox-primary">
                                    <input id="<?php echo e($id); ?>"
                                           <?php if(!empty($querydata['comn_data']['size']) && is_array($querydata['comn_data']['size'])): ?>  <?php if(in_array($id,$querydata['comn_data']['size'])): ?> <?php echo e('checked'); ?><?php endif; ?> <?php endif; ?> name="comn[size][]"
                                           value="<?php echo e($id); ?>" type="checkbox">
                                    <label for="<?php echo e($id); ?>">&nbsp;<?php echo e($filtervalue); ?></label>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>

        

        <div class="clear-fix"></div>
        <button type="button" id="elec_filter1" class="btn btn-warning pull-right filter-btn">Filter</button>
        <div class="clear-fix"></div>
        <hr class="fade-line">
    </div>
<?php endif; ?>

<?php echo $__env->make('layouts.filters.price_range', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
