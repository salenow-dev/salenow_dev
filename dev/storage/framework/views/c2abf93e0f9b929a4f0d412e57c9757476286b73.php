<?php $__currentLoopData = $filters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filterName => $filterValues): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
    <div class="col-md-4">
        <div class="form-group">
            <select id="<?php echo e(strtolower(str_replace(' ','',$filterName))); ?>" class="selectpicker show-tick form-control"
                    name="<?php echo e(strtolower(str_replace(' ','',$filterName))); ?>" data-live-search="false">
                <option value=""><?php echo e($filterName); ?> *</option>
                <?php $__currentLoopData = $filterValues; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $filtervalue): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <option value="<?php echo e($id); ?>" <?php echo e((isset($editAd->dbFilters[$id]))?'selected':''); ?>>
                        <?php echo e($filtervalue); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </select>
        </div>
        <label id="<?php echo e(strtolower(str_replace(' ','',$filterName))); ?>-error" class="error" for="<?php echo e(strtolower(str_replace(' ','',$filterName))); ?>" style="display: none"></label>
    </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>