<?php $__env->startSection('title', 'Member Login'); ?>
<?php $__env->startSection('content'); ?>
    <?php echo e(Html::style('css/saleme/layout/login-page.css')); ?>

    <div class="container">
        <div class="modal-dialog modal-md omb_login login-register">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center"><span class="no-1"><i
                                    class="lnr lnr-mustache linier-icon"></i> </span> &nbsp; Login / Register</h4>
                </div>
                <div class="col-md-12 pd-0-xs">
                    <div class="panel panel-login">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-6">
                                    <a href="<?php echo e(asset('')); ?>member-login" class="active">Login</a>
                                    

                                </div>
                                <div class="col-xs-6">
                                    <a href="<?php echo e(asset('')); ?>member/register">Register</a>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="login-form">
                                        <?php if($errors->any()): ?>
                                            <div class="alert alert-danger">
                                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                    <i class="fa fa-exclamation-triangle"></i>&nbsp;<?php echo e($error); ?>

                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                            </div>
                                        <?php endif; ?>
                                        <div class="message"></div>
                                        <div class="omb_row-sm-offset-6 omb_socialButtons">
                                            <form action="/facebook/authorize" target="" method="get"
                                                  id="facebook_form">
                                                <div class="col-xs-6 col-sm-6 margin-0">
                                                    <button type="submit" class="btn btn-lg btn-block omb_btn-facebook">
                                                        <i class="fa fa-facebook visible-xs"></i>
                                                        <span class="">Facebook</span>
                                                    </button>
                                                </div>
                                            </form>
                                            <form action="/google/authorize" target="" method="get" id="google_form">
                                                <div class="col-xs-6 col-sm-6 margin-0">
                                                    <button type="submit" class="btn btn-lg btn-block omb_btn-google">
                                                        <i class="fa fa-google-plus visible-xs"></i>
                                                        <span class="">Google+</span>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="clear-fix"></div>
                                        <div class="omb_loginOr">
                                            <div class="col-xs-12 col-sm-12 margin-0">
                                                <hr class="omb_hrOr">
                                                <span class="omb_spanOr">or</span>
                                            </div>
                                        </div>
                                        <form id="memberlogin_form" action="/memberlogin" method="post" role="form">
                                            <?php echo e(csrf_field()); ?>

                                            <div class="form-group">
                                                <input type="text" name="email" id="email" tabindex="1"
                                                       class="form-control" placeholder="Email"
                                                       value="<?php echo e(old('email')); ?>">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" name="password" id="password"
                                                       tabindex="2" class="form-control" placeholder="Password">
                                            </div>
                                            <div class="form-group remember-me-div">
                                                <div class="checkbox checkbox-primary">
                                                    <input id="checkbox2" type="checkbox">
                                                    <label for="checkbox2">&nbsp;Remember Me</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-6 pull-right margin-0">
                                                        <input type="submit" name="login-submit" id="login-submit"
                                                               tabindex="4" class="form-control btn btn-login"
                                                               value="Log In">
                                                    </div>
                                                    <div class="col-sm-6 col-xs-6 pull-left forget-pass">
                                                        <div class="">
                                                            <a href="/member/password/reset" tabindex="5"
                                                               class="forgot-password">Forgot Password?</a>
                                                        </div>
                                                    </div>
                                                    <div class="clear-fix"></div>
                                                    <div class="col-md-12">
                                                        <p class="agree-txt-1">
                                                            By login you agree to
                                                            <a href="<?php echo e(asset('')); ?>terms-conditions"
                                                               title="View Terms & Conditions"
                                                               target="_blank">Terms-Conditions</a>
                                                            and <a href="<?php echo e(asset('')); ?>privacy-policy"
                                                                   title="View Privacy Policy"
                                                                   target="_blank">Privacy-Policy</a>

                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                    
                    
                        
                             
                        
                    
                    
                        
                             
                        
                    
                    
                        
                             
                        
                    
                    
                
                <div class="clear-fix"></div>
            </div>
        </div>
    </div>
    <?php echo e(Html::script('js/saleme/plugin/jquery.validate.js')); ?>

    <script>
        // login form
        $("#memberlogin_form").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                }
            }
        });

        //member register
        $("#register_form").validate({
            rules: {
                signup_username: {
                    required: true,
                },
                signup_phone: {
                    number: true,
                    required: true,
                },
                signup_email: {
                    required: true,
                    email: true
                },
                signup_pass: {
                    required: true,
                },
                pass_confirmation: {
                    required: true,
                    equalTo: "#signup_pass"
                }
            },
            messages: {
                signup_username: {
                    required: "Please enter username",
                },
                signup_phone: {
                    required: "Please enter mobile number",
                },
                signup_email: {
                    required: "Please enter email",
                },
                signup_pass: {
                    required: "Please enter password",
                },
                pass_confirmation: {
                    equalTo: "does not match password",
                }
            }
        });

    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.adddetails', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>