<?php if(!empty($filters)): ?>
    <div class="row border-eee mg-b-10">
        <div class="col-md-3">
            <h4 class="category-titles primary-d">Primary Details</h4>
        </div>
        <div class="col-md-9">
            <div class="row">
                <?php echo $__env->make('layouts.frontend.filter_primary', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
            <div class="clear-fix"></div>
        </div>
    </div>
<?php endif; ?>
