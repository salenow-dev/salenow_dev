<div class="fade modal in location select-city-modal" aria-labelledby=myLargeModalLabel role=dialog tabindex=-1>
    <div class="modal-dialog modal-md">
        <div class=modal-content>
            <div class=modal-header>
                <button class=close type=button data-dismiss=modal><i class=ion-ios-close-empty></i></button>
                <h4 class="text-center modal-title"><span class=no-1><i class=ion-ios-location-outline></i> </span> 
                    Select Your City</h4></div>
            <div id=custom-search-input-desktop>
                <div class="col-md-12 input-group loc-pop-search">
                    <div class="col-xs-12 col-md-6 col-sm-8 pull-right">
                        <div id=custom-search-input>
                            <div class="col-md-12 input-group"><input id=search name=query class="form-control input-lg"
                                                                      placeholder="Type Your City Here"> <span
                                        class=input-group-btn><button class="btn btn-info btn-lg" type=button><i
                                                class=ion-ios-search-strong></i></button></span></div>
                        </div>
                    </div>
                </div>
                <ul class="list-group search-result"><h3 class=text-center
                                                         id=all-srilanka-txt></h3><?php if(!empty($citylist)): ?> <?php $__currentLoopData = $citylist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <a href="<?php echo e(asset('')); ?>ads/<?php echo e(strtolower(trans($city->slug))); ?><?php echo e(!empty($queryString)?$queryString:''); ?>">
                            <li class=list-group-item><i class="loc-io ion-ios-location-outline"></i>
                                 <?php echo e($city->city_name); ?></a><?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?> <?php endif; ?></ul>
                <div class=clear-fix></div>
            </div>
            <div class="col-md-5 col-xs-5 district-list popular-city">
                <div>
                    <a href="<?php echo e(asset('')); ?>ads/new-zealand/<?php echo e(!empty($scategory_slug) ? $scategory_slug :!empty($category_slug) ? $category_slug:''); ?><?php echo e(!empty($queryString)?$queryString:''); ?>">
                        <p class=allsrilankalink> All New Zealand <i class="loc-io ion-ios-arrow-right pull-right"></i>
                        </p></a>
                    <h3 class=pop-title-1>All Regions</h3><?php if(!empty($citywithdis)): ?>
                        <div class="col-xs-12 pd-r-0"><?php $__currentLoopData = $alldidcity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $discity): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?><p class="accordion-toggle loc"
                                                                                           data-parent=#accordion3
                                                                                           data-toggle=collapse
                                                                                           href="#d_<?php echo e($discity->id); ?>">
                                 <?php echo e($discity->district_name); ?> <i class="loc-io ion-ios-arrow-right pull-right"></i>
                            </p><?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?></div><?php endif; ?></div>
                <div class=clear-fix></div>
            </div>
            <div class="city-list-right col-md-7 col-xs-7 res-div">
                <div class=panel-group id=accordion3>
                    <div class="panel panel-default"><?php if(!empty($alldidcity)): ?> <?php $__currentLoopData = $alldidcity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$discity): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <div class="collapse panel-collapse" id="d_<?php echo e($discity->id); ?>">
                                <div class="panel-body pd-t-5"><h3
                                            class="text-center m-t-0 pop-title"><?php echo e($discity->district_name); ?></h3>
                                    <ul class=loc-city-list>
                                        <li>
                                            <a href="<?php echo e(asset('')); ?>ads/<?php echo e($discity->slug); ?>/<?php echo e(!empty($category_slug) ? $category_slug :''); ?><?php echo e(!empty($queryString)?$queryString:''); ?>"><i
                                                        class="loc-io ion-ios-location"></i>  all
                                                of <?php echo e($discity->district_name); ?></a>
                                        </li><?php if(!empty($discity->cities)): ?> <?php $__currentLoopData = $discity->cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dcity): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                            <li>
                                                <a href="<?php echo e(asset('')); ?>ads/<?php echo e($dcity->slug); ?>/<?php echo e(!empty($category_slug) ? $category_slug :''); ?><?php echo e(!empty($queryString)?$queryString:''); ?>"><i
                                                            class="loc-io ion-ios-location"></i>  <?php echo e($dcity->city_name); ?>

                                                </a></li><?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?> <?php endif; ?></ul>
                                    <div class=clear-fix></div>
                                </div>
                            </div><?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?> <?php endif; ?></div>
                </div>
            </div>
            <div class=clear-fix></div>
        </div>
    </div>
</div>
<div class="fade modal select-cat-modal select-category-modal" aria-labelledby=myLargeModalLabel role=dialog
     tabindex=-1>
    <div class="modal-dialog modal-md">
        <div class=modal-content>
            <div class=modal-header>
                <button class=close type=button data-dismiss=modal><i class=ion-ios-close-empty></i></button>
                <h4 class="text-center modal-title"><span class=no-1><i class=ion-ios-settings></i> </span>  Select
                    Category</h4></div>
            <div class=panel-group id=accordion>
                <div class="panel panel-default">
                    <div class=panel-heading><a
                                href="<?php echo e(asset('')); ?>ads/<?php echo e(!empty($location_slug) ? $location_slug :'sri-lanka'); ?>/<?php echo e(!empty($queryString)?$queryString:''); ?>">
                            <h4 class=panel-title><i class="cat-icons-big ion-social-buffer-outline"></i>   All
                                Categories <span class="pull-right ion-ios-arrow-right cat-pop-right-arrow"></span></h4>
                        </a></div>
                </div><?php if(!empty($allcategory)): ?> <?php $__currentLoopData = $allcategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$allcate): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <div class="panel panel-default">
                        <div class=panel-heading data-parent=#accordion data-toggle=collapse
                             href="#cat_<?php echo e($allcate->id); ?>"><h4 class=panel-title><i
                                        class="cat-icons-big <?php echo e($allcate->icon); ?>"></i>   <?php echo e($allcate->category_name); ?><span
                                        class="pull-right ion-ios-arrow-right cat-pop-right-arrow"></span></h4></div>
                        <div class="collapse panel-collapse" id="cat_<?php echo e($allcate->id); ?>">
                            <div class=panel-body>
                                <table class=table>
                                    <tr>
                                        <td><i class="loc-io ion-ios-pricetag"></i>   <a
                                                    href="<?php echo e(asset('')); ?>ads/<?php echo e(!empty($location_slug) ? $location_slug :'sri-lanka'); ?>/<?php echo e($allcate->slug); ?><?php echo e(!empty($queryString)?$queryString:''); ?>"
                                                    class=sub-cat-txt>All <?php echo e($allcate->category_name); ?></a>
                                    </tr><?php if(!empty($allcate->subcategories)): ?> <?php $__currentLoopData = $allcate->subcategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$suballcate): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <tr>
                                            <td><i class="loc-io ion-ios-pricetag"></i>   <a
                                                        href="<?php echo e(asset('')); ?>ads/<?php echo e(!empty($location_slug) ? $location_slug :'sri-lanka'); ?>/<?php echo e($suballcate->slug); ?><?php echo e(!empty($queryString)?$queryString:''); ?>"
                                                        class=sub-cat-txt><?php echo e($suballcate->sub_category_name); ?></a>
                                        </tr><?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?> <?php endif; ?></table>
                            </div>
                        </div>
                    </div><?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?> <?php endif; ?></div>
        </div>
    </div>
</div>
<div class="fade modal inquiry inquiry-model" id=modalinquiry aria-labelledby=myLargeModalLabel role=dialog tabindex=-1>
    <div class="modal-dialog modal-md">
        <div class=modal-content>
            <div class=modal-header>
                <button class=close type=button data-dismiss=modal><i class=ion-ios-close-empty></i></button>
                <h4 class="text-center modal-title"><span class=no-1><i class=ion-paper-airplane></i> </span> Make an
                    Inquiry</h4></div>
            <div class="col-xs-12 col-lg-12 pd-0-xs">
                <form class=inquiry-form id=inquiry-form role=form><input id=ad_id name=ad_id type=hidden
                                                                          value="<?php echo e((!empty($ad_details->adid))?$ad_details->adid:''); ?>">
                    <div>
                        <div class="col-xs-12 col-md-6">
                            <div class=form-group><input id=name name=name class=form-control placeholder=Name
                                                         autocomplete=off></div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class=form-group><input id=mobile name=mobile class=form-control
                                                         placeholder="Mobile Number" autocomplete=off></div>
                        </div>
                        <div class="col-xs-12 col-md-12">
                            <div class=form-group><input id=email name=email class=form-control
                                                         placeholder="Your E-mail Address" autocomplete=off type=email>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="col-xs-12 col-md-12 mg-b-10">
                            <div class=form-group><textarea class="form-control textarea" id=message name=message
                                                            placeholder=Message rows=3></textarea></div>
                        </div>
                    </div>
                    <div class=clear-fix></div>
                    <div>
                        <div class="col-md-12 top-mar-20">
                            <button class="pull-right btn main-btn" type=button id=inquiry_post_ad>Send a message
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>