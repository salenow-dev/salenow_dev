<!DOCTYPE html>
<html lang=en>
<meta charset=utf-8>
<meta content="text/html; charset=utf-8" http-equiv=Content-Type>
<meta content="<?php echo e(csrf_token()); ?>" name=csrf-token>

<title><?php echo e(config('app.name')); ?> - <?php echo $__env->yieldContent('title'); ?></title>
<meta content="width=device-width,initial-scale=1"
      name=viewport><?php if((!empty($category) || !empty($scategory)) && is_string ($category) ): ?> <?php if(!empty($scategory) ): ?>
    <meta content="Find any <?php echo e($scategory); ?> for sale in New Zealand - find the best online deals on salenow.co.nz, free classified New Zealand!"
          name=description><?php else: ?>
    <meta content="Find any <?php echo e($category); ?> for sale in New Zealand - find the best online deals on salenow.co.nz, free classified New Zealand!"
          name=description><?php endif; ?> <?php else: ?>
    <meta content="SaleNow is to buy and sell anything by online. we help you to find better deals from all over land for sale, used cars New Zealand, used phones New Zealand, used phones for sale and cars for sale."
          name=description><?php endif; ?>
<meta content="land for sale, used cars New Zealand, mobile phones New Zealand, used phones for sale, cars for sale, laptop for sale, bikes for sale, sale me"
      name=keywords>

<meta content=#fea502 name=theme-color>
<link href="<?php echo e(asset('/images/salenow/favicon-16x16.png')); ?>" rel="shortcut icon" type=image/x-icon>
<link href="<?php echo e(asset('/images/salenow/favicon-16x16.png')); ?>" rel=icon type=image/x-icon>
<meta content=<?php echo e(Request::url()); ?> property=og:url>
<meta content=www.salenow.co.nz property=og:site_name>
<meta content=product property=og:type>
<meta content=" <?php echo $__env->yieldContent('og-title'); ?>" property=og:title>
<meta content=" <?php echo $__env->yieldContent('og-description'); ?>" property=og:description>
<meta content=600 property=og:image:width>
<meta content=315 property=og:image:height>
<meta content="<?php echo $__env->yieldContent('og-image'); ?>" property=og:image>
<meta content=1867918153484824
      property=fb:app_id><?php echo e(Html::style('css/salenow/layout/bootstrap-3.3.7/css/bootstrap-theme.min.css')); ?> <?php echo e(Html::style('css/salenow/layout/bootstrap-3.3.7/css/bootstrap.min.css')); ?> <?php echo e(Html::style('css/salenow/layout/salenow-custom.css')); ?> <?php echo e(Html::style('css/salenow/layout/ionicons/css/ionicons.min.css')); ?> <?php echo e(Html::style('css/salenow/layout/linericons/style.min.css')); ?> <?php echo e(Html::style('css/salenow/layout/font-awesome-4.7.0/css/font-awesome.min.css')); ?> <?php echo e(Html::style('css/salenow/fonts/work.css')); ?> <?php echo e(Html::style('css/salenow/layout/salenow-responsive.css?ver=0.2')); ?>

<script>
    window.Laravel = <?php
    echo json_encode([
        'csrfToken' => csrf_token(),
    ]);
    ?>
</script>
<link rel="stylesheet" href="<?php echo e(asset('')); ?>assets/icons/icofont/icofont.min.css">
<?php echo e(Html::script('js/salenow/jquery.min.js')); ?><?php echo e(Html::script('js/salenow/bootstrap.min.js')); ?>

<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
      rel="stylesheet">
<style>
    .top-nav {
        min-height: 40px !important;
        margin: 0px !important;
        background: #2c2134;
        border-radius: 0px;
    }

    .top-nav ul {
        float: left;
        margin: 0;
    }

    .top-nav ul li {
        float: left;
        position: relative;
        display: block;
    }

    .top-nav ul li a {
        padding: 10px !important;
        font-size: 14px !important;
        margin-right: 5px !important;
        color: #fff;
        position: relative;
        display: block;
        line-height: 20px;
        font-family: "Montserrat", "Helvetica Neue", Arial, sans-serif;
    }

    .top-nav ul li a:hover {
        color: #fffffc !important;
    }

</style>
<body>

<h1 style=display:none>land for sale</h1>
<h1 style=display:none>used cars New Zealand</h1>
<h1 style=display:none>used phones New Zealand</h1>
<h1 style=display:none>used phones for sale</h1>
<h1 style=display:none>cars for sale</h1>
<h2 style=display:none>land for sale</h2>
<h2 style=display:none>used cars New Zealand</h2>
<h2 style=display:none>used phones New Zealand</h2>
<h2 style=display:none>used phones for sale</h2>
<h2 style=display:none>cars for sale</h2>
<section class="visible-xs leader-board-top-mobile">
    <div class=image-div-xs align=center>
        <!-- /21634329919/salenow_mobile_leaderboard_320x50 -->
        <div id='div-gpt-ad-1527658535738-0' style='height:50px; width:320px;'>





        </div>
    </div>
</section>

<div id=fb-root></div>
<script>
    !function (e, t, n) {
        var c, o = e.getElementsByTagName("script")[0];
        e.getElementById(n) || ((c = e.createElement("script")).id = n, c.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=1054163521396174", o.parentNode.insertBefore(c, o))
    }(document, 0, "facebook-jssdk")
</script>

<nav class="top-nav">
    <div class="container">
        <ul class="">
            <li>
                <a href="#">
                    <i class="ion-home" style="font-size: 18px"></i>
                </a>
            </li>
            <li><a href="#">Motors</a></li>
            <li><a href="#">Property</a></li>
            <li><a href="#">Jobs</a></li>
        </ul>
    </div>
</nav>


<div class="navbar navbar-info hidden-xs">
    <div class=container>
        <div class=navbar-header>
            <button class=navbar-toggle type=button data-target=.navbar-material-light-blue-collapse
                    data-toggle=collapse><span class=icon-bar></span> <span class=icon-bar></span> <span
                        class=icon-bar></span></button>
            <a href="<?php echo e(URL::to('/')); ?>"
               class=navbar-brand><?php echo e(Html::image('images/salenow/salenow-logo.png', 'salenow logo', array('class'=> 'img-responsive'))); ?></a>
        </div>
        <div class="collapse navbar-collapse navbar-material-light-blue-collapse">
            <ul class=dropdown-menu>
                <li><a href=#>Normal</a>
                <li class=disabled><a href=#>Disabled</a>
                <li class=active><a href=#>Active</a>
                <li><a href=#>Normal</a>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php echo e(asset('')); ?>ads" class="all pd-t-32">All Ads</a></li><?php if(checkMember()): ?>
                    <?php $member = getMember(); ?><?php else: ?>
                    <?php $member = ''; ?><?php endif; ?> <?php if(checkCompany()): ?>
                    <?php $checkMember = true;?><?php else: ?>
                    <?php $checkMember = false;?><?php endif; ?> <?php if(!empty($member)): ?>
                    <li>
                        <div class="dropdown user-drop"><span class=dropdown-toggle data-toggle=dropdown><span
                                        class="lnr lnr-mustache"></span>  <span
                                        class="username-md"><?php echo e(($checkMember)?(session()->has('profile_type'))?$member->first_name:'Premium Account':$member->first_name); ?></span> <span
                                        class="caret"></span></span>
                            <ul class=dropdown-menu>
                                <li><a href="<?php echo e(asset('')); ?>myprofile"><span
                                                class=ion-person></span>   <?php echo e(($checkMember)?(temporyPrivateLogin())?'My Accounts':'Company Account':'My Account'); ?>

                                    </a></li><?php if($member->employee && !temporyPrivateLogin()): ?>
                                    <li><a href="/employee/<?php echo e($member->employee->id); ?>/edit"><span
                                                    class=ion-ios-pricetags></span> Company Profile</a>
                                    </li><?php endif; ?> <?php if(!$checkMember || temporyPrivateLogin()): ?>
                                    <li><a href="<?php echo e(asset('')); ?>my_ads"><span class=ion-ios-pricetags></span>    My
                                            Ads</a></li><?php endif; ?>
                                <li class=divider>
                                <li><a href="<?php echo e(asset('')); ?>member_logout"><span class=ion-power></span>   Logout</a>
                            </ul>
                        </div>
                    </li><?php else: ?>
                    <li><a href=/member-login class="login pd-t-30"><span class="lnr lnr-mustache"></span>  Login/
                            Register</a></li><?php endif; ?>
                <li>
                    <a href="<?php echo e((checkMember())?asset('').'addtype':'/member-login'); ?>" class=post-free-ad-btn>
                        <button class="btn btn-lg post-lg" type=button>Post Free Ad</button>
                    </a>
            </ul>
        </div>
    </div>
</div>
<div class="navbar navbar-info visible-xs">
    <div class=mar-top-10>
        <div class="col-xs-4 col-md-4 pd-r-0"><a
                    href="<?php echo e(URL::to('/')); ?>"><?php echo e(Html::image('images/salenow/salenow-logo.png', 'salmenow logo', array('class'=> 'img-responsive'))); ?></a>
        </div>
        <div class="pd-l-0 col-md-5 col-xs-5 mar-top-5">
            <div class="pd-l-0 col-xs-8"><a href="<?php echo e(asset('')); ?>ads"><span
                            class="allads-nav pull-right">All Ads</span></a></div>
            <div class="col-xs-4 no-padding text-center"><?php if(!empty($member)): ?>
                    <div class=dropdown><span class=dropdown-toggle
                                              data-toggle=dropdown><span><?php echo e(Html::image('images/user_black.png', 'salenow user', array('class'=> 'user-fixed-img'))); ?></span> <span
                                    class="crt-dpdwn ion-arrow-down-b"></span></span>
                        <ul class=dropdown-menu>
                            <li><a href="<?php echo e(asset('')); ?>myprofile"><span
                                            class=ion-person></span><?php echo e(($checkMember)?(temporyPrivateLogin())?'My Account':'Company Account':'My Account'); ?>

                                </a></li><?php if($member->employee): ?>
                                <li><a href="/employee/<?php echo e($member->employee->id); ?>/edit"><span
                                                class=ion-ios-pricetags></span> Company Profile</a>
                                </li><?php endif; ?> <?php if(!$checkMember || temporyPrivateLogin()): ?>
                                <li><a href="<?php echo e(asset('')); ?>my_ads"><span class=ion-ios-pricetags></span> My Ads</a>
                                </li><?php endif; ?>
                            <li class=divider>
                            <li><a href="<?php echo e(asset('')); ?>member_logout"><span class=ion-power></span>   Logout</a>
                        </ul>
                    </div><?php else: ?> <a
                            href=/member-login><span><?php echo e(Html::image('images/user_black.png', 'salenow user', array('class'=> 'user-fixed-img'))); ?></span></a> <?php endif; ?>
            </div>
        </div>
        <div class="no-padding col-md-3 col-xs-3">
            <a href="<?php echo e((checkMember())?asset('').'addtype':'/member-login'); ?>" class=post-free-ad-btn>
                <button class="btn btn-lg post" type=button>Post Free Ad</button>
            </a>
        </div>
    </div>
</div>

