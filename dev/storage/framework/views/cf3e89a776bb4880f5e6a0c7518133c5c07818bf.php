<?php $__env->startSection('title', '| Sub-Category'); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <?php echo Form::open(['url' => 'settings/subcategory','id'=>'frmRegister','class'=>'form-horizontal','autocomplete' => 'off']); ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Sub-Category</strong> | Add New Record</h3>
                </div>
                <div class="panel-body">
                    <?php echo $__env->make('backend.subcategory.form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                <div class="panel-footer">
                    <button class="btn btn-default">Clear Form</button>
                    <button class="btn btn-primary pull-right" type="submit" id="aaa">Submit</button>
                </div>
            </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Manage</strong> | Sub-Categories</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table datatable table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Category Name</th>
                                    <th>Sub Category Name</th>
                                    <th>Referance</th>
                                    <th>Icon</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__empty_1 = true; $__currentLoopData = $subcategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); $__empty_1 = false; ?>
                                    <tr id="<?php echo e($category->id); ?>">
                                        <td><?php echo e($category->category->category_name); ?></td>
                                        <td><?php echo e($category->sub_category_name); ?></td>
                                        <td><?php echo e($category->sub_category_code); ?></td>
                                        <td><?php echo e($category->icon); ?></td>
                                        <td><?php echo e($category->status); ?></td>
                                        <td class="action-col"><a href="/settings/subcategory/<?php echo e($category->id); ?>/edit" class="edit unline"><span class="lnr lnr-pencil"></span></a> |
                                            <a class="delete unline"
                                               uid="<?php echo e($category->id); ?>" tag="subcategory"><span class="lnr lnr-trash"></span></a>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); if ($__empty_1): ?>
                                    <tr>
                                        <td colspan="5">No Records</td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php echo Form::open(['url' => 'settings/subcategory/assign-brands','id'=>'frmRegister','class'=>'form-horizontal','autocomplete' => 'off']); ?>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Assign</strong> | Brands to Sub-Categories</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Main Category</label>
                            <div class="col-md-6 col-xs-12">
                                <?php echo Form::select('subcategory_id', $allSubCategories+['0'=>'Select'], '0', ['id'=>'subcategory_id','class' => 'form-control select','data-live-search'=>'true' ]); ?>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Brands</label>
                            <div class="col-md-6 col-xs-12" id="brands-div">
                                

                            </div>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-default">Clear Form</button>
                            <button class="btn btn-primary pull-right" type="submit" id="aaa">Submit</button>
                        </div>
                    </div>
                </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
    <script src="<?php echo e(asset('backend/assets/js/subcategories/assignbrands.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>