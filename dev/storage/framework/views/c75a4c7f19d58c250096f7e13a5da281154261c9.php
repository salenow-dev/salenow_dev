<?php echo $__env->make('layouts.filters.add_type', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    
        
            
            
            
                
                              
                
            
            
        
    

<?php if(!empty($vehicleconditionlist)): ?>
    <div class="condition-div">
        <p class="section-title">Car Body Type</p>
        
        <div id="vehicle_type" class="body-type">
            <?php $__currentLoopData = $vehiclebodytype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vtype): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                <div class="col-xs-4 pd-l-5" title="Sedan">
                    <input type="radio" value="<?php echo e($vtype->id); ?>" name="vehi[type][]" id="body-<?php echo e($vtype->id); ?>"
                           <?php if(!empty($querydata['vehi_data']['type'])  && is_array($querydata['vehi_data']['type'])): ?> <?php if(in_array($vtype->id,$querydata['vehi_data']['type'])): ?> <?php echo e('checked'); ?><?php endif; ?> <?php endif; ?>   class="input-hidden"/>
                    <label for="body-<?php echo e($vtype->id); ?>">
                        <img src="<?php echo e(asset('')); ?>images/saleme/icons/<?php echo e($vtype->image_name); ?>.png" class="v-type hidden-xs">
                        <img src="<?php echo e(asset('')); ?>images/saleme/icons/<?php echo e($vtype->image_name); ?>_mobi.png"
                             class="v-type visible-xs">
                        <p class="text-center vehicle-type-title"><?php echo e($vtype->type_name); ?></p>
                    </label>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            <div class="clear-fix"></div>
        </div>
        <hr class="fade-line">
    </div>
<?php endif; ?>
<div class="sort-div filter-by">
    <p class="section-title">Filter By</p>
    
    <?php if(!empty($vehiclebrandlist)): ?>
        <div class="col-sm-12 col-xs-12 pd-0-xs filter-brand-div">
            <div class="panel filter-pannel">
                <div class="panel-heading filter-heading text-left clickable panel-collapsed">
                    <h3 class="panel-title  pull-left">All Brands</h3>
                    <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                    <div class="clear-fix"></div>
                </div>
                <div class="panel-body filter-body" id="vehicle_brand"
                     style="<?php echo e(!empty($querydata['vehi_data']['brand'])?'display:block;':'display: none;'); ?>">
                    <?php $__currentLoopData = $vehiclebrandlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vbrand): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <div class="checkbox  checkbox-primary">
                            <input id="<?php echo e($vbrand->id); ?>"
                                   <?php if(!empty($querydata['vehi_data']['brand']) && is_array($querydata['vehi_data']['brand'])): ?>  <?php if(in_array($vbrand->id,$querydata['vehi_data']['brand'])): ?> <?php echo e('checked'); ?><?php endif; ?> <?php endif; ?> name="vehi[brand][]"
                                   value="<?php echo e($vbrand->id); ?>" type="checkbox">
                            <label for="<?php echo e($vbrand->id); ?>">&nbsp;<?php echo e($vbrand->brand_name); ?></label>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>

                </div>
            </div>
            <div class="clear-fix"></div>
        </div>

    <?php endif; ?>
    <div class="col-sm-12 col-xs-12 pd-0-xs filter-brand-div">
        <div class="panel filter-pannel model-year-panel-head">
            <div class="panel-heading filter-heading text-left clickable panel-collapsed">
                <h3 class="panel-title clickable panel-collapsed pull-left">Year</h3>
                <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                <div class="clear-fix"></div>
            </div>
            <div class="panel-body filter-body model-year-panel"
                 style="<?php echo e(!empty($querydata['from_year']) ||  !empty($querydata['to_year']) ?'display:block;':'display: none;'); ?>">
                <div class="col-md-6 col-xs-6">
                    <input type="number"
                           value="<?php if(!empty($querydata['from_year'])): ?><?php echo e($querydata['from_year']); ?><?php else: ?> <?php echo e(''); ?> <?php endif; ?>"
                           name="f_year" placeholder="Min" class="form-control">
                </div>
                <div class="col-md-6 col-xs-6">
                    <input type="number"
                           value="<?php if(!empty($querydata['to_year'])): ?><?php echo e($querydata['to_year']); ?><?php else: ?> <?php echo e(''); ?> <?php endif; ?>"
                           name="t_year" placeholder="Max" class="form-control">
                </div>
                <div class="clear-fix"></div>
                <button type="button" id="vehi_filter2" class="btn btn-warning pull-right">Filter</button>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-xs-12 pd-0-xs select-option-div">
        <select id="vehicle_milage" name="vehi[milage]" class="form-control">
            <option value="">Mileage</option>
            <option <?php if(!empty($querydata['vehi_data']['milage'])): ?> <?php if($querydata['vehi_data']['milage'] == '0-5000'): ?> <?php echo e('selected'); ?><?php endif; ?> <?php endif; ?>  value="0-5000">
                <5,000 km
            </option>
            <option <?php if(!empty($querydata['vehi_data']['milage'])): ?> <?php if($querydata['vehi_data']['milage'] == '5000-10000'): ?> <?php echo e('selected'); ?><?php endif; ?> <?php endif; ?>  value="5000-10000">
                5,000km - 10,000 km
            </option>
            <option <?php if(!empty($querydata['vehi_data']['milage'])): ?> <?php if($querydata['vehi_data']['milage'] == '10000-30000'): ?> <?php echo e('selected'); ?><?php endif; ?> <?php endif; ?>  value="10000-30000">
                10,000km - 30,000 km
            </option>
            <option <?php if(!empty($querydata['vehi_data']['milage'])): ?> <?php if($querydata['vehi_data']['milage'] == '30000-50000'): ?> <?php echo e('selected'); ?><?php endif; ?> <?php endif; ?>  value="30000-50000">
                30,000km - 50,000 km
            </option>
            <option <?php if(!empty($querydata['vehi_data']['milage'])): ?> <?php if($querydata['vehi_data']['milage'] == '50000-100000'): ?> <?php echo e('selected'); ?><?php endif; ?> <?php endif; ?>  value="50000-100000">
                50,000km - 100,000 km
            </option>
            <option <?php if(!empty($querydata['vehi_data']['milage'])): ?> <?php if($querydata['vehi_data']['milage'] == '100000-1500000'): ?> <?php echo e('selected'); ?><?php endif; ?> <?php endif; ?>  value="100000-1500000">
                100,000km - 1500,000 km
            </option>
            <option <?php if(!empty($querydata['vehi_data']['milage'])): ?> <?php if($querydata['vehi_data']['milage'] == '150000-2500000'): ?> <?php echo e('selected'); ?><?php endif; ?> <?php endif; ?>  value="150000-2500000">
                1500,000km - 2500,000 km
            </option>
        </select>
    </div>
    <?php if(!empty($vehiclefuellist)): ?>
        <div class="col-sm-12 col-xs-12 pd-0-xs select-option-div">
            <select id="vehi_fuel" name="vehi[fuel]" class="form-control">

                <option value="">Fuel Type</option>
                <?php $__currentLoopData = $vehiclefuellist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vfuel): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <option <?php if(!empty($querydata['vehi_data']['fuel'])): ?>  <?php if($querydata['vehi_data']['fuel'] ==$vfuel->id): ?> <?php echo e('selected'); ?><?php endif; ?> <?php endif; ?> value="<?php echo e($vfuel->id); ?>"><?php echo e($vfuel->fuel_name); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>

            </select>
        </div>
    <?php endif; ?>

    <?php if(!empty($vehicletransmissionslist)): ?>
        <div class="col-sm-12 col-xs-12 pd-0-xs select-option-div">
            <select id="vehi_trans" name="vehi[trans]" class="form-control">
                <option value="">Transmissions Type</option>
                <?php $__currentLoopData = $vehicletransmissionslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vtrans): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <option <?php if(!empty($querydata['vehi_data']['trans'])): ?> <?php if($querydata['vehi_data']['trans'] == $vtrans->id): ?> <?php echo e('selected'); ?><?php endif; ?>  <?php endif; ?> value="<?php echo e($vtrans->id); ?>"><?php echo e($vtrans->transmission_name); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>

            </select>
        </div>
    <?php endif; ?>
    <div class="clear-fix"></div>
    <hr class="fade-line">
</div>


<?php echo $__env->make('layouts.filters.price_range', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


