<?php $__env->startSection('title', 'Post Ad'); ?>
<?php $__env->startSection('content_post_ad'); ?>
    <?php
    $sub_layout = '';
    $tableswitch = '';
    $js = 'default';
    if (!empty($basedata)) {
        $category_id = (!empty($basedata['category_id'])) ? $basedata['category_id'] : '';
        $category_name = (!empty($basedata['category_name'])) ? $basedata['category_name'] : '';
        $category_code = (!empty($basedata['category_code'])) ? $basedata['category_code'] : '';
        $sub_cat_id = (!empty($basedata['sub_cat_id'])) ? $basedata['sub_cat_id'] : '';
        $sub_cat_ref = (!empty($basedata['sub_cat_ref'])) ? $basedata['sub_cat_ref'] : '';
        $sub_cat_name = (!empty($basedata['sub_cat_name'])) ? $basedata['sub_cat_name'] : '';
        $ad_type = (!empty($basedata['ad_type'])) ? $basedata['ad_type'] : '';
    }
    $adTitleStatus = true;
    if ($ad_type == 'sell') {
        if ($sub_cat_ref == 'car' OR $sub_cat_ref == 'bike' OR $sub_cat_ref == 'van' OR $sub_cat_ref == 'mobi') {
            $adTitleStatus = false;
        }
    }
    switch ($category_code) {
        case 'veh':
            $tableswitch = 'veh';
            break;
        case 'ele':
            $tableswitch = 'ele';
            break;
        case 'pro':
            $tableswitch = 'pro';
            break;
        case 'hote':
            $tableswitch = 'hote';
            break;
        case 'job':
            $tableswitch = 'job';
            break;
        default:
            $tableswitch = 'default';
    }
    switch ($sub_cat_ref) {
        case 'car':
            $sub_layout = $sub_cat_ref;
            break;
        case 'bike':
            $sub_layout = $sub_cat_ref;
            break;
        case 'three':
            $sub_layout = $sub_cat_ref;
            break;
        case 'van':
            $sub_layout = $sub_cat_ref;
            break;
        case 'hea':
            $sub_layout = $sub_cat_ref;
            break;
        case 'auto-ser':
            $sub_layout = $sub_cat_ref;
            break;
        case 'auto-acc':
            $sub_layout = $sub_cat_ref;
            break;
        case 'push':
            $sub_layout = $sub_cat_ref;
            break;
        case 'boat':
            $sub_layout = $sub_cat_ref;
            break;
        case 'mobi':
            $sub_layout = $sub_cat_ref;
            break;
        case 'mobi-acc':
            $sub_layout = $sub_cat_ref;
            break;
        case 'com-tab':
            $sub_layout = $sub_cat_ref;
            break;
        default:
        case 'tv':
            $sub_layout = $sub_cat_ref;
            break;
        case 'cam':
            $sub_layout = $sub_cat_ref;
            break;
        case 'land':
            $sub_layout = $sub_cat_ref;
            break;
        case 'aprt':
            $sub_layout = $sub_cat_ref;
            break;
        case 'com-prop':
            $sub_layout = $sub_cat_ref;
            break;
        case 'hous':
            $sub_layout = $sub_cat_ref;
            break;
        case 'holi-rent':
            $sub_layout = $sub_cat_ref;
            break;
        case 'room':
            $sub_layout = $sub_cat_ref;
            break;
        case 'pets':
            $sub_layout = $sub_cat_ref;
            break;
        case 'hotel':
            $sub_layout = $sub_cat_ref;
            break;
//        default:
//            $sub_layout = 'default';
    }
    ?>
    <style>
        .phone-section {
            background: #eee;
            padding-top: 15px;
            padding-bottom: 15px;
            border: 1px dashed #ccc;
            border-radius: 5px;
            margin-top: 15px;
        }

        .add-phone-number-btn {
            background: #c31414;
            margin-top: 15px;
            color: #fff;
            border-radius: 30px;
        }
    </style>
    <div class="col-md-10 col-sm-12">
        <div class="row bg-eee mg-b-10 mg-b-10-xs">
            <div class="col-md-12 category-details">
                <h4 class="category-title">Category</h4>
                <ul class="prime-features">
                    <li><?php echo e($category_name); ?></li>
                    <li><?php echo e($sub_cat_name); ?></li>
                </ul>
                <p><a href="/selectcat?type=<?php echo e($ad_type); ?>" class="change-cat pull-right"><i
                                class="ion-ios-arrow-thin-left ico2"></i>&nbsp; Change Category</a></p>
            </div>
            <div class="clear-fix"></div>
        </div>

        <div class="col-md-12">
            <?php echo $__env->make('errors.flash', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo $__env->make('errors.list', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="alert alert-danger hide" id="error-div" role="alert">
                <i class="fa fa-exclamation-triangle"></i>&nbsp;
                <span class="error-title">Alert! The following errors were found.</span>
                <hr>
                <div id="error-message"></div>
            </div>
        </div>
        <form id="from-submit" action="" method="post" role="form" enctype="multipart/form-data">
            
            <input type="hidden" name="formtype" value="<?php echo e((!empty($editAd->id))?'edit':'create'); ?>">
            <input type="hidden" name="adId" id="adId" value="<?php echo e((!empty($editAd->id))?$editAd->id:''); ?>">
            <input type="hidden" name="adRef" id="adRef"
                   value="<?php echo e((!empty($editAd->ad_referance))?$editAd->ad_referance:''); ?>">
            <input type="hidden" name="category_id" value="<?php echo e($category_id); ?>">
            <input type="hidden" name="sub_category_id" value="<?php echo e($sub_cat_id); ?>">
            <input type="hidden" name="sub_category_ref" value="<?php echo e($sub_cat_ref); ?>">
            <input type="hidden" name="tableswitch" value="<?php echo e($tableswitch); ?>">
            <input type="hidden" name="ad_type" value="<?php echo e($ad_type); ?>" id="ad_type">

            <input type="hidden" name="company" value="<?php echo e((temporyPrivateLogin())?'1':'0'); ?>" id="company">
            
            
            <section id="common">
                <div class="row  hover-eee mar-b-5-xs">
                    <div class="col-md-3 col-xs-12 col-sm-12">
                        <h4 class="category-titles">Your City *</h4>
                    </div>
                    <div class="col-md-5 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <select id="district_id" name="district_id"
                                    class="selectpicker show-tick form-control" data-live-search="true">
                                <option default selected value=""> Select location</option>
                                <?php $__currentLoopData = $locationdistrictslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $district): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <option value="<?php echo e($district->id); ?>" <?php echo e((!empty($member->district_id) && $district->id == $member->district_id)? 'selected':''); ?>

                                            <?php echo e(((!empty($editAd->district_id)) && $district->id == $editAd->district_id)? 'selected':''); ?>>
                                        <?php echo e($district->district_name); ?>

                                    </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </select>
                        </div>
                        <label id="district_id-error" class="error" for="district_id" style="display: none;"></label>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <div class="form-group">
                            <select id="city_id" name="city_id" class="selectpicker show-tick form-control"
                                    data-live-search="true">
                                
                                <?php if(!empty($locationcities)): ?>
                                    <?php $__currentLoopData = $locationcities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <option value="<?php echo e($city->id); ?>" <?php echo e((!empty($member->city_id) && $city->id == $member->city_id)? 'selected':''); ?>

                                                <?php echo e(((!empty($editAd->city_id)) && $city->id == $editAd->city_id)? 'selected':''); ?>>
                                            <?php echo e($city->city_name); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                <?php endif; ?>
                            </select>
                        </div>
                        <label id="city_id-error" class="error" for="city_id" style="display: none;"></label>
                    </div>
                    <div class="clear-fix"></div>
                </div>
                <div class="clear-fix"></div>
                <?php if($ad_type == 'jobs'): ?>
                    <section id="image" data-toggle="tooltip" data-html="true" title="
                <span class='ion-information-circled tooltip-icon'></span>
                <p class='bold-font'>Add Job artwork for 5x more responses!</p>
                <p>Images must be JPG or PNG format (max 5 MB). Do not upload images with watermarks</p>"
                             data-placement="right">
                        <?php else: ?>
                            <section id="image" data-toggle="tooltip" data-html="true" title="
                <span class='ion-information-circled tooltip-icon'></span>
                <p class='bold-font'>Ads with 1-5 photos get 5x more responses!</p>
                <p>Ads with real photos get 10 times more views than with catalogue images</p>
                <p>Images must be JPG or PNG format (max 5 MB). Do not upload images with watermarks</p>"
                                     data-placement="right">
                                <?php endif; ?>
                                <?php if($ad_type == 'sell' OR $ad_type == 'rent' OR $ad_type == 'jobs'): ?>
                                    <div class="row  mg-b-10 hover-eee">
                                        <div class="clear-fix"></div>

                                        <div class="col-md-3">
                                            <h3 class="category-titles"><?php echo e(($ad_type == 'jobs')?'Upload Job Image' :'Upload Photos *'); ?></h3>
                                            
                                            
                                        </div>
                                        <div class="col-md-9">
                                            <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css"
                                                  rel="stylesheet">
                                            <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script>
                                            <div id="image-upload" class="dropzone">
                                                <h3>Click to Upload Multiple Images</h3>
                                            </div>
                                            <div class="row" id="dropzone_previews">
                                                <p id="error-message-image"></p>
                                                <?php if(!empty($adImagesJson)): ?>
                                                    <?php $__currentLoopData = $adImagesJson; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                        <div class="col-xs-6 col-md-3 pd-r-5 pd-l-5"
                                                             id="res_<?php echo e($image->id); ?>">
                                                            <div class="thumbnail">
                                                                <a href="javascript:;" class="">
                                                                    <img src="<?php echo e(url('/saleme/images/uploads/'.$editAd->id.'/'.$image->name.'')); ?>"
                                                                         alt="<?php echo e($image->name); ?>">
                                                                </a>
                                                                <span class="display-block main-image-div">
                                                    <input type="radio" id="featured_<?php echo e($image->id); ?>" name="featured"
                                                           value="<?php echo e($editAd->id); ?>"
                                                           <?php echo e(($image->name==$editAd->featuredimage)?'checked':''); ?>

                                                           onclick="setFeatured(<?php echo e($image->id); ?>)"> Main Image
                                                    </span>
                                                                <span class="display-block delete-div">
                                                    <a href="javascript:;" class="delete-link"
                                                       onclick="deleteFromAdImages(<?php echo e($image->id); ?>)"><i
                                                                class="ion-trash-a"></i> Delete</a>
                                                    </span>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </section>
                            <?php if($ad_type == 'sell' OR $ad_type == 'rent'): ?>
                                <section id="price" data-toggle="tooltip" data-html="true" title="
                <span class='ion-information-circled tooltip-icon'></span>
                <p class='bold-font'>Pick a good price</p>" data-placement="right">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <h4 class="category-titles">
                                                <?php if($ad_type=='rent'): ?>
                                                    <?php echo e('Rent per month'); ?>

                                                <?php elseif($sub_cat_ref == 'hotel'): ?>
                                                    <?php echo e('Price per Night'); ?>

                                                <?php else: ?>
                                                    <?php echo e('Price'); ?>

                                                <?php endif; ?>
                                                *</h4>
                                            
                                        </div>
                                        <div class="col-md-4 col-xs-8 col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon">Rs.</span>
                                                    <input type="text" class="form-control price"
                                                           value="<?php echo e((!empty($editAd->price))?$editAd->price:''); ?>"
                                                           placeholder="Pick a good price (LKR)" name="price"
                                                           id="price"/>
                                                </div>
                                                <label id="price-error" class="error" for="price"></label>
                                            </div>
                                        </div>
                                        <div class="col-md-5 col-xs-4 pd-l-0-xs col-sm-6">
                                            <div class="checkbox Negotiable  checkbox-primary">
                                                <input id="Negotiable" name="nego"
                                                       type="checkbox" <?php echo e((!empty($editAd->nego) && $editAd->nego == true)?'checked':''); ?>>
                                                <label for="Negotiable">&nbsp; Negotiable</label>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            <?php endif; ?>
                    </section>
                    

                    <section id="unique" data-toggle="tooltip" data-html="true" title="
            <span class='ion-information-circled tooltip-icon'></span>
                <p>Enter Item general details condition, Item type, Value added features etc</p>"
                             data-placement="right">
                        <?php if($category_code == 'veh' OR $category_code == 'ele' OR $category_code == 'pro' OR $category_code == 'hote'
                         OR $sub_layout == 'jobs-in-sl'): ?>
                            <?php echo $__env->make('layouts.frontend.layout-'.$sub_layout, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            <?php
                            $js = $sub_layout;
                            ?>
                        <?php else: ?>
                            <?php echo $__env->make('layouts.frontend.layout-master-default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        <?php endif; ?>
                    </section>
                    
                    <div class="row  mg-b-10 hover-eee " data-toggle="tooltip" data-html="true" title="
            <span class='ion-information-circled tooltip-icon'></span>
                <p class='bold-font'>Give your ad an attractive title.</p>
                <p>Think of words that people would use to search for your Ad.</p>" data-placement="right">
                        <div class="clear-fix"></div>
                        <?php if($adTitleStatus): ?>
                            <div class="col-md-3">
                                <h4 class="category-titles">
                                    <?php if($sub_cat_ref == 'hotel'): ?>
                                        <?php echo e('Hotel / Room Title'); ?>

                                    <?php else: ?>
                                        <?php echo e('Title for your Ad'); ?>

                                    <?php endif; ?>
                                    *</h4>
                                
                            </div>
                            <div class="col-md-9">
                                <div class="form-group main-title-div">
                                    <input type="text" name="adtitle" class="form-control" id="adtitle" maxlength="50"
                                           value="<?php echo e((!empty($editAd->adtitle))?$editAd->adtitle:''); ?>"
                                           placeholder="What's unique about your ad?">
                                    <p id="chars_title" class="text-right margin-0"></p>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="clear-fix"></div>
                    </div>
                    <div class="row border-eee mg-b-10 hover-eee" data-toggle="tooltip" data-html="true" title="
            <span class='ion-information-circled tooltip-icon'></span>
                <p class='bold-font'>Items with good description sell faster!</p>
                <p>Mention other features, reason for selling and if the item is still under warranty.</p>
                <p>Remember, a good description needs at least 2-3 sentences.</p>" data-placement="right">
                        <div class="col-md-3">
                            <h4 class="category-titles">Description *
                                <span class="visible-xs mobile-cution">Discription with Contact Numbers will be
                            Rejected</span></h4>
                            <p class="photo-txt hidden-xs" style="color: red;"><strong>
                                    Do not add <b>Phone numbers</b> in description
                                </strong></p>
                        </div>
                        <div class="col-md-9 ">
                            <div class="form-group main-des-div">
                                <textarea class="form-control" name="description" rows="5" maxlength="5000"
                                          id="description"><?php echo (!empty($editAd->description))?$editAd->description:''; ?>

                                </textarea>
                                <p id="chars" class="text-right margin-0"></p>
                            </div>
                            <p class="photo-txt hidden-xs" style="color: red; font-size: 12px"><strong>Description with
                                    Contact Numbers
                                    will be Rejected</strong></p>
                        </div>
                        <div class="clear-fix"></div>
                    </div>

                    <div class="row border-eee mg-b-10 hover-eee phone-section" data-toggle="tooltip" data-html="true"
                         title="
                <span class='ion-information-circled tooltip-icon'></span>
                <p class='bold-font'>Add Phone Numbers</p>
                <p>Add your mobile number here</p>
                <p>Add at least 2 phone numbers for more inquiries </p>" data-placement="right">
                        <div class="col-md-3">
                            <h4 class="category-titles">Mobile Numbers *</h4>
                        </div>
                        <div class="col-md-9 ">
                            
                            <img src="<?php echo e(asset('')); ?>/images/loadingnew.gif" class="loading-post hide" id="loading"
                                 width="20">
                            
                            <div class="blockui text-center">
                                <button type="submit" name="formsubmit"
                                        class="vehicel_btn btn  btn-lg blockui add-button add-phone-number-btn">
                                    <span class="lnr lnr-phone-handset"></span> Add phone Numbers
                                </button>
                            </div>
                            
                            <br>
                            

                        </div>
                        <div class="clear-fix"></div>
                    </div>
                    <section id="contact">
                        <p class="required-txt pull-left">* are required</p>
                        <p class="pull-right"><i>By posting this ad, you agree to the <a href="#">Terms & Conditions</a>
                                of
                                this site.</i>
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                    </section>
            
        </form>
    </div>
    <div class="clear-fix"></div>
    <!--verify phone popup-->
    <div class="modal fade add-phone-number" id="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="ion-ios-close-empty"></i>
                    </button>
                    <h4 class="modal-title text-center"><span class="no-1"><i
                                    class="ion-ios-telephone-outline"></i> </span> &nbsp; Verify Phone Number</h4>
                </div>
                <div class="col-lg-12">
                    <h3>Please enter your mobile phone number</h3>
                    <p>For first time use, Please Obtain our Verification PIN Code via SMS alert to getting touch with
                        you.</p>
                    <form id="formNumberVerify" class="verify-number-form">
                        <div id="message" class="alert alert-info" style="display: none;"></div>
                        <div class="inline-group" id="newnumber">
                            <input type="text" name="number" id="number" class="form-control inline-item enter-phone"
                                   required="required" placeholder="eg:07xxxxxxxx"/>
                            <input type="button" name="numberSubmit" id="numberSubmit"
                                   class="btn btn-primary inline-item verify-btn"
                                   value="Add Number"/>
                        </div>
                        <div class="inline-group" id="verifynumber">
                            <input type="text" name="number_verify" class="form-control inline-item enter-phone"
                                   required="required" placeholder="Enter Pin Code"/>
                            <input type="button" name="verify" id="verify"
                                   class="btn btn-primary inline-item verify-btn"
                                   value="Verify"/>

                            
                            <input type="hidden" name="resend" id="resend" value="">
                            <div>
                                Did not receive PIN code?&nbsp;
                                <input type="button" name="resendpin" id="resendpin" onclick="resendPIN()"
                                       class="btn btn-primary inline-item verify-btn"
                                       value="Resend PIN "/>

                                <div class="inline-group" id="wrong_number">
                                    <p class="text-center margin-0"> Wrong No?</p>
                                    <input type="text" name="wrong_" id="wrong_"
                                           class="form-control inline-item enter-phone"
                                           placeholder="re-enter number"/>
                                    <input type="button" name="numberSubmit" id="numberSubmit"
                                           class="btn btn-primary inline-item verify-btn"
                                           value="Re-enter" onclick="reEnterNumber()"/>
                                </div>
                            </div>
                        </div>
                        <div class="inline-group">
                            <p class="nopadding text-center">
                                <label id="number-error" class="error" for="number" style="display: none"></label></p>
                        </div>
                    </form>
                </div>
                <div class="clear-fix"></div>
            </div>
        </div>
    </div>
    <div class="callcenterBox  ">
        <div class="boxcontent">
            <p>Need help?</p>
            <p class="number">Call us at<br>
                <span class="block">
            <a class="call" href="tel:0115818188" id="callTollFree">011-5 81 81 88</a>
             </span>
            </p>
            <p class="color-1">(9am - 7pm all 7 days)</p>
        </div>
        <a href="#" class="slider"></a>
    </div>
    <!--bootstap select with search-->
    <?php echo e(Html::script('js/saleme/plugin/select-search/bootstrap-select.min.js')); ?>

    <?php echo e(Html::script('js/saleme/plugin/jquery.validate.js')); ?>

    <script type="text/javascript" src="<?php echo e(asset("plugins/blockui/jquery.blockUI.js")); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset("plugins/mask/jquery.mask.min.js")); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset("js/layout-validation/submithandler.js")); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset("js/post-ad/number-verify.js")); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset("js/post-ad/dropzone.js")); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset("js/layout-validation/". $js.".js")); ?>"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".gallery-item-remove").on("click", function () {
            var id = $(this).attr('uid');
            var data = {'id': id};
            $.ajax({
                type: "POST",
                url: '/ad/deleteimage',
                data: data,
                success: function (res) {
                    if (res.message == 'success') {
                        $('#gallery_image_' + id).fadeOut(400);
                        return false;
                    }
                }
            });
        });
        //featured image delete
        $(".gallery-item-remove-featured").on("click", function () {
            var id = $(this).attr('uid');
            var data = {'id': id, 'type': 'featured'};
            $.ajax({
                type: "POST",
                url: '/ad/deleteimage',
                data: data,
                success: function (res) {
                    if (res.message == 'success') {
                        $('#featuredimage_db').val('');
                        $('#fetured_' + id).fadeOut(400);
                        return false;
                    }
                }
            });
        });
        $('.selectpicker').selectpicker();

    </script>

    <script>
        $(".callcenterBox").click(function (e) {
            $(this).toggleClass('active');
        });
        $(function () {
            $('[data-toggle="tooltip"]').tooltip({
                template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner large"></div></div>'
            })
        });
    </script>
    <script type="text/javascript">
        // price mask
        $('.price').mask("#,##0", {reverse: true});
        //        character count
        $(document).ready(function () {
            //description
            $('#description').each(function () {
                var $this = $(this);
                var counter = $('#chars');
                var maxlength = $this.attr('maxlength');
                counter.text(maxlength + ' characters allowed');
                $this.bind('input keyup keydown', function (e) {
                    var value = $this.val();
                    if (value.length > 0) {
                        counter.text((maxlength - $this.val().length) + ' characters left');
                        if ((maxlength - $this.val().length) == 0) {
                            counter.css('color', 'red');
                        } else {
                            counter.css('color', '#333');
                        }
                    } else {
                        counter.text(maxlength + ' characters allowed');
                    }
                });
            });

            // Ad title
            $('#adtitle').each(function () {
                var $this = $(this);
                var counter = $('#chars_title');
                var maxlength = $this.attr('maxlength');
                counter.text(maxlength + ' characters allowed');
                $this.bind('input keyup keydown', function (e) {
                    var value = $this.val();
                    if (value.length > 0) {
                        counter.text((maxlength - $this.val().length) + ' characters left');
                        if ((maxlength - $this.val().length) == 0) {
                            counter.css('color', 'red');
                        } else {
                            counter.css('color', '#333');
                        }
                    } else {
                        counter.text(maxlength + ' characters allowed');
                    }
                });
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.postadd', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>