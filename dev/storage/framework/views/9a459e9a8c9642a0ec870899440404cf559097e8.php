<!DOCTYPE html>
<html lang=en>
<meta charset=utf-8>
<meta content="text/html; charset=utf-8" http-equiv=Content-Type>
<meta content="<?php echo e(csrf_token()); ?>" name=csrf-token>
<title><?php echo e(config('app.name')); ?> - <?php echo $__env->yieldContent('title'); ?></title>

<meta content="width=device-width,initial-scale=1" name=viewport><?php if((!empty($category) || !empty($scategory)) && is_string ($category) ): ?> <?php if(!empty($scategory) ): ?>
    <meta content="Find any<?php echo e($scategory); ?>for sale in Sri Lanka - find the best online deals on saleme.lk, free classified sri lanka!" name=description><?php else: ?>
    <meta content="Find any<?php echo e($category); ?>for sale in Sri Lanka - find the best online deals on saleme.lk, free classified sri lanka!" name=description><?php endif; ?> <?php else: ?>
    <meta content="SaleMe is to buy and sell anything by online. we help you to find better deals from all over land for sale, used cars sri lanka, used phones sri lanka, used phones for sale and cars for sale." name=description><?php endif; ?>
<meta content="land for sale, used cars sri lanka, mobile phones sri lanka, used phones for sale, cars for sale, laptop for sale, bikes for sale, sale me" name=keywords>
<meta content=#fea502 name=theme-color>
<link href="<?php echo e(asset('/images/saleme/favicon-16x16.png')); ?>" rel="shortcut icon" type=image/x-icon>
<link href="<?php echo e(asset('/images/saleme/favicon-16x16.png')); ?>" rel=icon type=image/x-icon>
<meta content=<?php echo e(Request::url()); ?>property=og:url>
<meta content=www.saleme.lk property=og:site_name>
<meta content=product property=og:type>
<meta content=" <?php echo $__env->yieldContent('og-title'); ?>" property=og:title>
<meta content=" <?php echo $__env->yieldContent('og-description'); ?>" property=og:description>
<meta content=600 property=og:image:width>
<meta content=315 property=og:image:height>
<meta content="<?php echo $__env->yieldContent('og-image'); ?>" property=og:image>

<meta content=1867918153484824 property=fb:app_id><?php echo e(Html::style('css/saleme/layout/bootstrap-3.3.7/css/bootstrap-theme.min.css')); ?><?php echo e(Html::style('css/saleme/layout/bootstrap-3.3.7/css/bootstrap.min.css')); ?><?php echo e(Html::style('css/saleme/layout/saleme-custom.css')); ?><?php echo e(Html::style('css/saleme/layout/ionicons/css/ionicons.min.css')); ?><?php echo e(Html::style('css/saleme/layout/linericons/style.min.css')); ?><?php echo e(Html::style('css/saleme/layout/font-awesome-4.7.0/css/font-awesome.min.css')); ?><?php echo e(Html::style('css/saleme/fonts/work.css')); ?><?php echo e(Html::style('css/saleme/layout/saleme-responsive.css?ver=0.2')); ?>

<script>
    window.Laravel = <?php echo json_encode([ 'csrfToken'=> csrf_token(),]); ?>
</script><?php echo e(Html::script('js/saleme/jquery.min.js')); ?><?php echo e(Html::script('js/saleme/bootstrap.min.js')); ?>

<script async src=https://www.googletagservices.com/tag/js/gpt.js></script>
<script>
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [], googletag.cmd.push(function() {
        googletag.defineSlot("/21634329919/slm_leaderboard_970x90_atf", [320, 50], "div-gpt-ad-1511940698204-0").addService(googletag.pubads()), googletag.defineSlot("/21634329919/slm_leaderboard_970x90_atf", [970, 90], "div-gpt-ad-1511940698204-1").addService(googletag.pubads()), googletag.defineSlot("/21634329919/slm_skyscraper_160x600_atf", [160, 600], "div-gpt-ad-1512040136576-0").addService(googletag.pubads()).setTargeting("subcategory-mobile", "<?php echo e((!empty($subcategory_code))?$subcategory_code:''); ?>"), googletag.defineSlot("/21634329919/slm_bottom_970x90", [300, 100], "div-gpt-ad-1512033969149-0").addService(googletag.pubads()), googletag.defineSlot("/21634329919/slm_bottom_970x90", [970, 90], "div-gpt-ad-1512033969149-1").addService(googletag.pubads()), googletag.defineSlot("/21634329919/slm_square_300x250_btf", [300, 250], "div-gpt-ad-1512731616849-0").addService(googletag.pubads()), googletag.enableServices()
    })
</script>
<script>
    ! function(e, t, a, n, r) {
        e[n] = e[n] || [], e[n].push({
            "gtm.start": (new Date).getTime(),
            event: "gtm.js"
        });
        var s = t.getElementsByTagName("script")[0],
            g = t.createElement("script");
        g.async = !0, g.src = "https://www.googletagmanager.com/gtm.js?id=GTM-PQQWX8C", s.parentNode.insertBefore(g, s)
    }(window, document, 0, "dataLayer")
</script>
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-834902320"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments)
    }
    gtag("js", new Date), gtag("config", "AW-834902320")
</script>

<body>
<noscript>
    <iframe height=0 src="https://www.googletagmanager.com/ns.html?id=GTM-PQQWX8C" style="display:none;visibility:hidden"
            width=0></iframe>

</noscript>
<h1 style=display:none>land for sale</h1>
<h1 style=display:none>used cars sri lanka</h1>
<h1 style=display:none>used phones sri lanka</h1>
<h1 style=display:none>used phones for sale</h1>
<h1 style=display:none>cars for sale</h1>
<h2 style=display:none>land for sale</h2>
<h2 style=display:none>used cars sri lanka</h2>
<h2 style=display:none>used phones sri lanka</h2>
<h2 style=display:none>used phones for sale</h2>
<h2 style=display:none>cars for sale</h2>
<section class="visible-xs leader-board-top-mobile">

    <div class=image-div-xs align=center>
    
    
    
    
    
    
    
    
    <!-- /21634329919/saleme_mobile_leaderboard_320x50 -->
        <div id='div-gpt-ad-1527136399877-0' style='height:50px; width:320px;'>
            <script>
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1527136399877-0'); });
            </script>
        </div>
    </div>
</section>
<div id=fb-root></div>
<script>
    !function (e, t, n) {

        var c, o = e.getElementsByTagName("script")[0];
        e.getElementById(n) || ((c = e.createElement("script")).id = n, c.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=1054163521396174", o.parentNode.insertBefore(c, o))
    }(document, 0, "facebook-jssdk")
</script>
<div class="navbar navbar-info hidden-xs">
    <div class=container>
        <div class=navbar-header>
            <button class=navbar-toggle type=button data-target=.navbar-material-light-blue-collapse
                    data-toggle=collapse><span class=icon-bar></span> <span class=icon-bar></span> <span
                        class=icon-bar></span></button>
            <a href="<?php echo e(URL::to('/')); ?>"
               class=navbar-brand><?php echo e(Html::image('images/saleme/saleme-logo.png', 'saleme.lk logo', array('class'=> 'img-responsive'))); ?></a>
        </div>
        <div class="collapse navbar-collapse navbar-material-light-blue-collapse">

            <ul class=dropdown-menu>
                <li><a href=#>Normal</a>
                <li class=disabled><a href=#>Disabled</a>
                <li class=active><a href=#>Active</a>
                <li><a href=#>Normal</a>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php echo e(asset('')); ?>ads" class="all pd-t-32">All Ads</a></li><?php if(checkMember()): ?>
                    <?php $member = getMember(); ?><?php else: ?>
                    <?php $member = ''; ?><?php endif; ?> <?php if(checkCompany()): ?>
                    <?php $checkMember = true;?><?php else: ?>
                    <?php $checkMember = false;?><?php endif; ?> <?php if(!empty($member)): ?>
                    <li>
                        <ul class="nav navbar-nav user-nav-desktop">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle user-drop2" data-toggle="dropdown"> <span
                                            class="lnr lnr-mustache"></span> <?php echo e(($checkMember)?(session()->has('profile_type'))?$member->first_name:'Premium Account':$member->first_name); ?>

                                    <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo e(asset('')); ?>myprofile"><?php echo e(($checkMember)?(temporyPrivateLogin())?'&nbsp;&nbsp;My Account':'&nbsp;&nbsp;Company Account':'&nbsp;&nbsp;My Account'); ?><span
                                                    class="ion-ios-person pull-left"></span></a></li>
                                    <li class="divider"></li>
                                    <?php if($member->employee && !temporyPrivateLogin()): ?>
                                        <li><a href="<?php echo e(asset('')); ?>employee/<?php echo e($member->employee->id); ?>/edit">&nbsp;&nbsp;User Account<span
                                                        class="ion-edit pull-left"></span></a></li>
                                        <li class="divider"></li>
                                        <li><a href="<?php echo e(asset('')); ?>my_ads?type=private">&nbsp;&nbsp;Private Account<span
                                                        class="ion-ios-locked-outline pull-left"></span></a></li>
                                        <li class="divider"></li>
                                    <?php endif; ?>
                                    <?php if($member->employee && !$checkMember || temporyPrivateLogin()): ?>
                                        <li><a href="<?php echo e(asset('')); ?>myprofile?type=company">&nbsp;&nbsp;Company Account<span
                                                        class="ion-flag pull-left"></span></a></li>
                                        <li class="divider"></li>
                                    <?php endif; ?>
                                    <?php if(!$checkMember || temporyPrivateLogin()): ?>
                                        <li><a href="<?php echo e(asset('')); ?>my_ads">&nbsp;&nbsp;My Ads<span
                                                        class="ion-ios-pricetags-outline pull-left"></span></a></li>
                                        <li class="divider"></li>
                                    <?php endif; ?>
                                    <li><a href="<?php echo e(asset('')); ?>wishlist">&nbsp;&nbsp;Wish List<span
                                                    class="ion-ios-heart pull-left"></span></a></li>
                                    <li class="divider"></li>
                                    
                                    <li><a href="<?php echo e(asset('')); ?>member_logout">&nbsp;&nbsp;Logout<span
                                                    class="ion-power pull-left"></span></a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    </li><?php else: ?>
                    <li><a href=/member-login class="login pd-t-30"><span class="lnr lnr-mustache"></span>Â  Login/
                            Register</a></li><?php endif; ?>
                <li>
                    <a href="<?php echo e((checkMember())?asset('').'addtype':'/member-login'); ?>" class=post-free-ad-btn>
                        <button class="btn btn-lg post-lg" type=button>Post Free Ad</button>
                    </a>
            </ul>
        </div>
    </div>
</div>
<div class="navbar navbar-info visible-xs">
    <div class=mar-top-10>
        <div class="col-xs-4 col-md-4 pd-r-0"><a href="<?php echo e(URL::to('/')); ?>"><?php echo e(Html::image('images/saleme/saleme-logo.png', 'saleme.lk logo', array('class'=> 'img-responsive'))); ?></a> </div>
        <div class="pd-l-0 col-md-5 col-xs-8 mar-top-5">
            <div class="pd-l-0 col-xs-4"><a href="<?php echo e(asset('')); ?>ads"><span class="allads-nav pull-right">All Ads</span></a></div>
            <div class="col-xs-8 no-padding text-center"><?php if(!empty($member)): ?>
                    <div class=dropdown> <span class=dropdown-toggle data-toggle=dropdown><span><?php echo e(Html::image('images/user_black.png', 'saleme.lk user', array('class'=> 'user-fixed-img'))); ?></span> <span class="crt-dpdwn ion-arrow-down-b"></span></span>
                        <ul class=dropdown-menu>
                            <li><a href="<?php echo e(asset('')); ?>myprofile"><span class=ion-person></span><?php echo e(($checkMember)?(temporyPrivateLogin())?'My Account':'Company Account':'My Account'); ?></a></li><?php if($member->employee): ?>
                                <li><a href="/employee/<?php echo e($member->employee->id); ?>/edit"><span class=ion-ios-pricetags></span> Company Profile</a> </li><?php endif; ?> <?php if(!$checkMember || temporyPrivateLogin()): ?>
                                <li><a href="<?php echo e(asset('')); ?>my_ads"><span class=ion-ios-pricetags></span> My Ads</a> </li><?php endif; ?>
                            <li class=divider>
                            <li><a href="<?php echo e(asset('')); ?>member_logout"><span class=ion-power></span>Â Â Â Logout</a> </ul>
                    </div><?php else: ?> <a href=/member-login><span><?php echo e(Html::image('images/user_black.png', 'saleme.lk user', array('class'=> 'user-fixed-img'))); ?></span> Login / Register</a> <?php endif; ?> </div>
        </div>
    </div>
</div>