<?php $__env->startSection('title', '| Advertisement Types'); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <?php echo Form::open(['url' => 'settings/adtypes','id'=>'frmRegister','class'=>'form-horizontal','autocomplete' => 'off']); ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Advertisement Types</strong> | Add New Record</h3>
                </div>
                <div class="panel-body">
                    <?php echo $__env->make('backend.advertismenttypes.form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                <div class="panel-footer">
                    <button class="btn btn-default">Clear Form</button>
                    <button class="btn btn-primary pull-right" type="submit" id="aaa">Submit</button>
                </div>
            </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Manage</strong> | Advertisement Types</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table datatable table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Type Name</th>
                                    <th>Types Ref</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(!empty($allList)): ?>
                                    <?php $__currentLoopData = $allList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <tr id="<?php echo e($item->id); ?>">
                                            <td><?php echo e($item->typename); ?></td>
                                            <td><?php echo e($item->typeref); ?></td>
                                            <td><?php echo e($item->status); ?></td>
                                            <td>
                                                <a href="/settings/adtypes/<?php echo e($item->id); ?>/edit" class="edit unline"><span class="lnr lnr-pencil"></span></a>
                                                |
                                                <a class="delete unline"
                                                   uid="<?php echo e($item->id); ?>" tag="advertisementtype"><span class="lnr lnr-trash"></span></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>