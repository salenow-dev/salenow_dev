<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title><?php echo e(config('app.name')); ?> <?php echo $__env->yieldContent('title'); ?></title>
    <!-- START third party CSS -->
    <link href="<?php echo e(asset("backend/assets/css/theme-default.css")); ?>" rel="stylesheet">
    <link href="<?php echo e(asset("backend/assets/css/icon-font.min.css")); ?>" rel="stylesheet">
    <link href="<?php echo e(asset("backend/assets/css/saleme-backend-custom.css")); ?>" rel="stylesheet">
    <!-- END third party CSS -->
    <!-- START third party JS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- END third party JS -->
    <!-- Scripts -->
    <script type="text/javascript" src="<?php echo e(asset("backend/assets/js/plugins/jquery/jquery.min.js")); ?>"></script>

    <script type="text/javascript" src="<?php echo e(asset("backend/assets/js/plugins/jquery/jquery-ui.min.js")); ?>"></script>
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
<!-- START PAGE CONTAINER -->
<div class="page-container">
    <!-- START PAGE SIDEBAR -->
    <div class="page-sidebar">
        <!-- START X-NAVIGATION -->
        <ul class="x-navigation">
            <li class="xn-logo">
                <a href="<?php echo e(asset('')); ?>/dashboard">Saleme.lk</a>
                <a href="#" class="x-navigation-control"></a>
            </li>
            <li class="xn-profile">
                <a href="#" class="profile-mini">
                    <img src="../backend/assets/img/users/avatar.jpg" alt=""/>
                </a>
                <div class="profile">
                    <div class="profile-image">
                        <img src="../backend/assets/img/users/avatar.jpg" alt=""/>
                    </div>
                    <div class="profile-data">
                        <div class="profile-data-name">
                            <?php if(Auth::check()): ?>
                                <?php echo e(Auth::user()->name); ?>

                            <?php endif; ?>
                        </div>
                        <?php if(Auth::check()): ?>
                            <div class="profile-data-title"><?php echo e(Auth::user()->roles[0]->name); ?></div>
                        <?php endif; ?>
                    </div>

                </div>
            </li>
            <?php echo $__env->make('layouts.backendsidemenu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </ul>
        <!-- END X-NAVIGATION -->
    </div>
    <!-- END PAGE SIDEBAR -->
    <!-- PAGE CONTENT -->
    <div class="page-content">
        <!-- START X-NAVIGATION VERTICAL -->
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
            <!-- TOGGLE NAVIGATION -->
            <li class="xn-icon-button">
                <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
            </li>
            <!-- END TOGGLE NAVIGATION -->
            <!-- SEARCH -->
            <li class="xn-search">
                <form role="form">
                    <input type="text" name="search" placeholder="Search..."/>
                </form>
            </li>
            <!-- END SEARCH -->
            <!-- POWER OFF -->
            <li class="xn-icon-button pull-right last">
                <a href="#"><span class="fa fa-power-off"></span></a>
                <ul class="xn-drop-left animated zoomIn">
                    <?php if(Auth::guest()): ?>
                        <li><a href="<?php echo e(url('/login')); ?>">Login</a></li>
                        <li><a href="<?php echo e(url('/register')); ?>">Register</a></li>
                    <?php else: ?>
                        <li><a href="/user/<?php echo e(getUserId()); ?>/edit"><span class="fa fa-lock"></span> My Account</a></li>
                        <li>
                            <a href="<?php echo e(url('/logout')); ?>" class="mb-control"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <span class="fa fa-sign-out"></span>
                                SignOut</a>
                            <form id="logout-form" action="<?php echo e(url('/logout')); ?>" method="POST"
                                  style="display: none;">
                                <?php echo e(csrf_field()); ?>

                            </form>
                        </li>
                    <?php endif; ?>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                </ul>
            </li>
            <!-- END POWER OFF -->
            <!-- MESSAGES -->
            
                
                
                
                    
                        
                        
                            
                        
                    
                    
                        
                            
                            
                            
                            
                        
                        
                            
                            
                            
                            
                        
                        
                            
                            
                            
                            
                        
                        
                            
                            
                            
                            
                        
                    
                    
                        
                    
                
            
            <!-- END MESSAGES -->
            <!-- TASKS -->
            
                
                
                
                    
                        
                        
                            
                        
                    
                    
                        
                            
                            
                                
                                     
                                
                            
                            
                        
                        
                            
                            
                                
                                     
                                
                            
                            
                        
                        
                            
                            
                                
                                     
                                
                            
                            
                        
                        
                            
                            
                                
                                     
                                
                            
                            
                            
                        
                    
                    
                        
                    
                
            
            <!-- END TASKS -->
            <!-- LANG BAR -->
            
                
                
                    
                    
                    
                
            
            <!-- END LANG BAR -->
        </ul>
        <!-- END X-NAVIGATION VERTICAL -->
        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active"><?php echo e(ucwords(Request::segment(1))); ?></li>
            <li class="active"><?php echo e(ucwords(Request::segment(2))); ?></li>
        </ul>
        <!-- END BREADCRUMB -->
        <!-- PAGE CONTENT WRAPPER -->
        <div class="page-content-wrap">
            <div class="row">
                <div class="col-md-12">
                    <?php echo $__env->make('errors.flash', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make('errors.list', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <div class="alert alert-danger hide" id="error-div" role="alert">
                        <i class="fa fa-exclamation-triangle"></i>&nbsp;
                        <span class="error-title">Alert! The following errors were found.</span>
                        <hr>
                        <div id="error-message"></div>
                    </div>
                </div>
            </div>
            <?php echo $__env->yieldContent('content'); ?>
        </div>
        <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- Scripts -->

<!-- START SCRIPTS -->
<!-- START PLUGINS -->


<script type="text/javascript" src="<?php echo e(asset("backend/assets/js/plugins/bootstrap/bootstrap.min.js")); ?>"></script>
<script type="text/javascript"
        src="<?php echo e(asset("backend/assets/js/plugins/jquery-validation/jquery.validate.js")); ?>"></script>
<!-- END PLUGINS -->

<!-- START THIS PAGE PLUGINS-->
<script type='text/javascript' src='<?php echo e(asset("backend/assets/js/plugins/icheck/icheck.min.js")); ?>'></script>
<script type="text/javascript"
        src="<?php echo e(asset("backend/assets/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("backend/assets/js/plugins/scrolltotop/scrolltopcontrol.js")); ?>"></script>
<script type='text/javascript' src='<?php echo e(asset("backend/assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js")); ?>'></script>
<script type='text/javascript'
        src='<?php echo e(asset("backend/assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js")); ?>'></script>
<script type='text/javascript'
        src='<?php echo e(asset("backend/assets/js/plugins/bootstrap/bootstrap-datepicker.js")); ?>'></script>
<script type="text/javascript" src="<?php echo e(asset("backend/assets/js/plugins/owl/owl.carousel.min.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("backend/assets/js/plugins/datatables/jquery.dataTables.min.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("backend/assets/js/plugins/moment.min.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("backend/assets/js/plugins/daterangepicker/daterangepicker.js")); ?>"></script>
<!-- END THIS PAGE PLUGINS-->
<script type="text/javascript" src="<?php echo e(asset("backend/assets/js/plugins/bootstrap/bootstrap-select.js")); ?>"></script>
<script type='text/javascript' src='<?php echo e(asset("backend/assets/js/plugins/noty/jquery.noty.js")); ?>'></script>
<script type='text/javascript' src='<?php echo e(asset("backend/assets/js/plugins/noty/layouts/topCenter.js")); ?>'></script>
<script type='text/javascript' src='<?php echo e(asset("backend/assets/js/plugins/noty/layouts/topLeft.js")); ?>'></script>
<script type='text/javascript' src='<?php echo e(asset("backend/assets/js/plugins/noty/layouts/topRight.js")); ?>'></script>
<script type='text/javascript' src='<?php echo e(asset("backend/assets/js/plugins/noty/themes/default.js")); ?>'></script>
<script type='text/javascript' src='<?php echo e(asset("backend/assets/js/plugins/blueimp/jquery.blueimp-gallery.min.js")); ?>'></script>
<script type="text/javascript" src='<?php echo e(asset("backend/assets/js/plugins/tagsinput/jquery.tagsinput.min.js")); ?>'></script>
<script type="text/javascript" src="<?php echo e(asset("plugins/blockui/jquery.blockUI.js")); ?>"></script>
<!-- Init Common functions - public/js/default -->
<script type="text/javascript" src="<?php echo e(asset('backend/assets/js/default/init_functions.js')); ?>"></script>
<!-- START TEMPLATE -->
<script type="text/javascript" src="<?php echo e(asset("backend/assets/js/settings.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("backend/assets/js/plugins.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("backend/assets/js/actions.js")); ?>"></script>

<!-- END TEMPLATE -->
<!-- END SCRIPTS -->

<script type="text/javascript" src="<?php echo e(asset("backend/assets/js/plugins/morris/morris.min.js")); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset("backend/assets/js/demo_charts_morris.js")); ?>"></script>

<script src="<?php echo e(asset("backend/assets/js/sweetalert.min.js")); ?>"></script>

</body>
</html>
