<?php $__env->startSection('content'); ?>
    <!--bootstrap select with search-->
    <?php echo e(Html::style('css/salenow/layout/select-search/bootstrap-select.min.css')); ?>

    <!--END css-->
<div class="container cont-bg">
    <section>
        <div class="row ">
            <div class="post-form">
                <div class="col-md-2 hidden-sm hidden-xs"> <!-- required for floating -->
                    <div class="menulist2 ">
                        <?php if( !empty($categories)): ?>
                            <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <?php if(!empty($data)): ?>
                                    <a href="/selectcat?type=<?php echo e($ad_type); ?>#select_<?php echo e($data->category_code); ?>"
                                       data-tooltip="<?php echo e($data->category_name); ?>">
                                        <div class="menu-item2 menu-item-selected2 <?php echo e(($data->id == $category_id)? 'selected':''); ?>">
                                            <div class="menu-item-green-bar"></div>
                                            <div class="menu-item-icon">
                                                <span class="<?php echo e($data->icon); ?> cat-ico adicon"></span>
                                            </div>
                                            <div class="menu-item-select-arrow">
                                            </div>
                                        </div>
                                    </a>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        <?php endif; ?>
                    </div>
                </div>
                <?php echo $__env->yieldContent('content_post_ad'); ?>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo e(asset("js/location/district_city.js")); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.saleme-shortfooter', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>