
<div class="budget-div">
    <p class="section-title">Budget</p>
    
    
    
    
    
    
    
    

    
    
    
    
    
    
    
    <div class="col-md-6 col-lg-6 pd-0-xs pd-r-5">
        <label class="text-left price-input-lables pull-left">From</label>
        <input type="number" name="f_price" id="f_price"
               value="<?php echo e((!empty($querydata['from_price']))?$querydata['from_price']:'1'); ?>" placeholder="" min="1"
               class="form-control price-input-field">
        <div class="clear-fix"></div>
    </div>
    
    <div class="col-md-6 pd-0-xs col-lg-6 pd-l-5">
        <label class="text-left price-input-lables pull-left">To</label>
        <input type="number" name="t_price" id="t_price"
               value="<?php echo e((!empty($querydata['to_price']))?$querydata['to_price']:''); ?>" placeholder="" min="1"
               class="form-control price-input-field">
        <div class="clear-fix"></div>
    </div>
    <div class="clear-fix"></div>
    <button type="button" id="btn_price_range" class="btn btn-warning pull-right">Filter</button>
    <div class="clear-fix"></div>
    <hr class="fade-line">
</div>

