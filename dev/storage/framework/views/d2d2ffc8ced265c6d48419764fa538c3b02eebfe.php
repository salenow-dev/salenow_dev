<?php $__env->startSection('content'); ?>
    <div class="row">
        <?php if (Auth::check() && Auth::user()->hasRole('superadmin')): ?>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>User</strong> Registration</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <?php echo Form::open(['url' => '/register','files'=>true,'class'=>'form-horizontal','autocomplete' => 'off','id'=>'formSubmit']); ?>

                    <?php echo $__env->make('auth.form-user',['editMode' => false], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <div class="panel-footer">
                        <button class="btn btn-default" type="reset">Clear Form</button>
                        <button type="submit" class="btn btn-danger pull-right">Register</button>
                    </div>
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>