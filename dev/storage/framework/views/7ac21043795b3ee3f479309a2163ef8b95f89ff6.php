<?php if(!empty($addtypelist)): ?>
<div class="condition-div">
    <p class="section-title">Ad Type</p>
    
   <?php $__currentLoopData = $addtypelist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $addtype): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?> 
    <label><input type="radio" value="<?php echo e($addtype['typeref']); ?>" <?php if($querydata['addtype'] == $addtype['typeref']): ?> <?php echo e('checked'); ?> <?php endif; ?> name="adtype"/>
        <?php echo e($addtype['typename']); ?></label>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    <hr class="fade-line">
</div>
<?php endif; ?>