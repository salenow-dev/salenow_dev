<?php echo $__env->make('main.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldContent('content'); ?>
<div class="hidden-xs">
    <?php echo $__env->make('main.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>
