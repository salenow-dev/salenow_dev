<?php if($errors->any()): ?>
<div class=" col-md-12 alert alert-danger" id="error-div" role="alert">
    <i class="fa fa-exclamation-triangle"></i>&nbsp;
    <span class="error-title">Alert! The following errors were found.</span>
    <hr>
    <div id="error-message">
        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <li><?php echo e($error); ?></li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </div>
</div>
<?php endif; ?>