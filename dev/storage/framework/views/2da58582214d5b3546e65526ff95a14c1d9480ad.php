<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <style type="text/css" rel="stylesheet" media="all">
        /* Media Queries */
        @media  only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>
</head>
<?php

$style = [
    /* Layout ------------------------------ */
    'body' => 'margin: 0; padding: 0; width: 100%; background-color: #F2F4F6;',
    'email-wrapper' => 'width: 100%; margin: 0; padding: 0; background-color: #F2F4F6;',
    /* Masthead ----------------------- */
    'email-masthead' => 'padding: 25px 0; text-align: center;',
    'email-masthead_name' => 'font-size: 16px; font-weight: bold; color: #2F3133; text-decoration: none; text-shadow: 0 1px 0 white;',
    'email-body' => 'width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; border-bottom: 1px solid #EDEFF2; background-color: #FFF;',
    'email-body_inner' => 'width: auto; max-width: 570px; margin: 0 auto; padding: 0;',
    'email-body_cell' => 'padding: 35px;',
    'email-footer' => 'width: auto; max-width: 570px; margin: 0 auto; padding: 0; text-align: center;',
    'email-footer_cell' => 'color: #AEAEAE; padding: 35px; text-align: center;',
    /* Body ------------------------------ */
    'body_action' => ' margin: 30px auto; padding: 0; text-align: center;',
    'body_sub' => 'margin-top: 25px; padding-top: 25px; border-top: 1px solid #EDEFF2;',
    /* Type ------------------------------ */
    'anchor' => 'color: #3869D4;',
    'header-1' => 'margin-top: 0; color: #2F3133; font-size: 19px; font-weight: bold; text-align: left;',
    'paragraph' => 'margin-top: 0; color: #74787E; font-size: 14px; line-height: 1.5em;',
    'paragraph-sub' => 'margin-top: 0; color: #74787E; font-size: 12px; line-height: 1.5em;',
    'paragraph-center' => 'text-align: center;',
    /* Buttons ------------------------------ */
    'button' => 'display: block; display: inline-block; width: 200px; min-height: 20px; padding: 10px;
                 background-color: #3869D4; border-radius: 3px; color: #ffffff; font-size: 15px; line-height: 25px;
                 text-align: center; text-decoration: none; -webkit-text-size-adjust: none;',
    'button--green' => 'background-color: #22BC66;',
    'button--red' => 'background-color: #dc4d2f;',
    'button--blue' => 'background-color: #3869D4;',
    'tablebg' => '    margin: auto; max-width: 600px; font-size: 12px;line-height: 19px;
                    padding: 20px 0 5px 0;background: url() repeat #f2f4f6;color: #444444; text-align: center;',
    'btn-edit' => ' font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;display: block;display: inline-block;width: 122px;min-height: 20px;padding: 7px;
                    border-radius: 0px;color: #fff;font-size: 15px; line-height: 25px;text-align: center;border: 1px solid #eee;text-decoration: none;
                    -webkit-text-size-adjust: none;background-color: #ffa70e;',
    'btn-live' => 'font-family: Arial, "Helvetica Neue", Helvetica, sans-serif; display: block; display: inline-block;width: 122px;min-height: 20px;padding: 7px;background-color: #3869D4;border-radius: 0px;
                        color: #ffffff;font-size: 15px;line-height: 25px;text-align: center;text-decoration: none;-webkit-text-size-adjust: none; background-color: #ffa70e;',
    'btn-spam' => 'font-family: Arial, "Helvetica Neue", Helvetica, sans-serif; display: block; display: inline-block;width: 122px;min-height: 20px;padding: 7px;background-color: #3869D4;border-radius: 0px;
                        color: #ffffff;font-size: 15px;line-height: 25px;text-align: center;text-decoration: none;-webkit-text-size-adjust: none; background-color: #c13e3e;',

];
?>

<?php $fontFamily = 'font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;'; ?>

<body style="<?php echo e($style['body']); ?>">
<table style="<?php echo e($style['tablebg']); ?>" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td style="<?php echo e($style['email-wrapper']); ?>" align="center">
            <table width="100%" cellpadding="0" cellspacing="0">

                <!-- Logo -->
                <tr>
                </tr>
                <tr>
                    <td>
                        <div style="float:left"><a href="http://dev.salemelk.com/">
                                <img src="https://res.cloudinary.com/wfx/image/upload/v1499862015/saleme-logo_wxhphw.png"
                                     width="115" alt="" class="CToWUd">
                            </a></div>

                        <div style="float:right;text-align:center;margin-top:3px">
                            <a href=""
                               style="width:125px;height:30px;color:#000;border:1px solid #000;display:block;text-decoration:none;font-size:13px;border-radius:3px;line-height:30px"
                               target="_blank" data-saferedirecturl="">Post Free Ad</a>
                        </div>
                    </td>
                </tr>
                <!-- Email Body -->
                <tr>
                    <td style="<?php echo e($style['email-body']); ?>" width="100%">
                        <table style="<?php echo e($style['email-body_inner']); ?>" align="center" width="570" cellpadding="0"
                               cellspacing="0">
                            <tr>
                                <td style="<?php echo e($fontFamily); ?> <?php echo e($style['email-body_cell']); ?>">
                                    <?php if(!empty($statusforTitle)): ?>
                                        <?php
                                        $icon = '';
                                        $iconTitle = '';
                                        switch ($statusforTitle) {
                                            case 'pending':
                                                $icon = 'https://res.cloudinary.com/saleme-lk/image/upload/v1500281919/notepad_nv919f.png';
                                                $iconTitle = 'Your ad is now on review!';
                                                break;
                                            case 'confirm':
                                                $icon = 'https://res.cloudinary.com/saleme-lk/image/upload/v1500282147/like-thumb-up_tt1brz.png';
                                                $iconTitle = 'Your ad is live Now!';
                                                break;
                                            case 'cancel':
                                                $icon = 'https://res.cloudinary.com/saleme-lk/image/upload/v1500288339/exclamation-mark-inside-a-circle_h4sc5u.png';
                                                $iconTitle = 'Your ad moved to Spam!';
                                                break;
                                            case 'expired':
                                                $icon = 'https://ci5.googleusercontent.com/proxy/gbrzmALIzVmnGCF6Q1T2fD1mIKpEUzFkJub_8HVqw8sBDt1tp6NTa2bEu6NoqtZGFmDj0MpbbRSppg=s0-d-e1-ft#http://teja1.kuikr.com/images/ad.png';
                                                $iconTitle = 'Your ad Expired!';
                                                break;
                                            case 'activation':
                                                $icon = 'https://ci5.googleusercontent.com/proxy/gbrzmALIzVmnGCF6Q1T2fD1mIKpEUzFkJub_8HVqw8sBDt1tp6NTa2bEu6NoqtZGFmDj0MpbbRSppg=s0-d-e1-ft#http://teja1.kuikr.com/images/ad.png';
                                                $iconTitle = 'Activate Your Account!';
                                                break;
                                        }
                                        ?>
                                        <p style="text-align:center;margin:0 0 2px 0;padding:0">
                                            <img src="<?php echo e($icon); ?>"
                                                 width="75" height="75" alt="" class="CToWUd">
                                        </p>
                                        <div style="font-size:25px;padding:5px 0;text-align:center;color:#000000;margin-bottom:30px">
                                            <p style="margin:0;padding:0;line-height:25px"><?php echo e($iconTitle); ?></p>
                                            <div style="    width: 140px;height: 3px;background: #ffa70e;margin: auto;margin-top: 15px;"></div>
                                        </div>
                                    <?php endif; ?>
                                    <br>
                                    <h1 style="<?php echo e($style['header-1']); ?>"><?php echo e($greating); ?></h1>
                                    <!-- Intro -->
                                    <?php $__currentLoopData = $introLines; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $line): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <p style="<?php echo e($style['paragraph']); ?>">
                                            <?php echo e($line); ?>

                                        </p>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>

                                    <?php if(!empty($spamReasons)): ?>
                                            <p style="<?php echo e($style['paragraph']); ?>"> Reasons for moved to spam</p>
                                        <?php $__currentLoopData = $spamReasons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spam): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                            <ul>
                                                <p style="<?php echo e($style['paragraph']); ?>">
                                                <li><?php echo e($spam->reason); ?></li>
                                                </p>
                                            </ul>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    <?php endif; ?>

                                <!-- Action Button -->
                                    <?php if(isset($buttons) && $statusforTitle != 'pending'): ?>
                                        <table style="<?php echo e($style['body_action']); ?>" align="center" width="63%"
                                               cellpadding="0" cellspacing="0">
                                            <tr>
                                                <?php 
                                                    if($statusforTitle == 'cancel'){
                                                            unset($buttons['view']);
                                                    }
                                                 ?>
                                                <?php $__currentLoopData = $buttons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $text => $slug): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                    <td align="center">
                                                        <a href="<?php echo e($slug); ?>"
                                                           style="<?php echo e(($text == 'view')? $style['btn-live']: ($text == 'edit')? $style['btn-edit']: $style['btn-spam']); ?>"
                                                           target="_blank">
                                                            <?php if($text=='view'): ?>
                                                                Go to your Ad
                                                            <?php elseif($text=='activate'): ?>
                                                                Activate
                                                            <?php else: ?>
                                                                Edit your Ad
                                                            <?php endif; ?>
                                                        </a>
                                                    </td>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                            </tr>
                                        </table>
                                    <?php endif; ?>
                                <!-- Outro -->
                                    <?php $__currentLoopData = $outroLines; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $line): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <p style="<?php echo e($style['paragraph']); ?>">
                                            <?php echo e($line); ?>

                                        </p>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                <!-- Salutation -->
                                    <div style="width:93%;font-size: 14px;float:left;padding:20px 3.5% 20px 0">
                                        Thank you,<br>
                                        <strong>Team <span class="il">Saleme.lk</span></strong>
                                    </div>
                                    <!-- Sub Copy -->
                                    <?php if(isset($buttons) && $statusforTitle != 'pending'): ?>
                                        <table style="<?php echo e($style['body_sub']); ?>">
                                            <tr>
                                                <td style="<?php echo e($fontFamily); ?>">
                                                    <p style="<?php echo e($style['paragraph-sub']); ?>">
                                                        If you’re having trouble clicking the button(s),
                                                        copy and paste the URL below into your web browser:
                                                    </p>
                                                    <?php $__currentLoopData = $buttons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $text => $slug): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                        <p style="<?php echo e($style['paragraph-sub']); ?>">
                                                            <span>
                                                                <?php if($text=='view'): ?>
                                                                    Go to your Ad
                                                                <?php elseif($text=='activate'): ?>
                                                                    Activate your account
                                                                <?php else: ?>
                                                                    Edit your Ad
                                                                <?php endif; ?>
                                                            </span>
                                                            <a style="<?php echo e($style['anchor']); ?>" href="<?php echo e($slug); ?>"
                                                               target="_blank">
                                                                <?php echo e($slug); ?>

                                                            </a>
                                                        </p>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                                </td>
                                            </tr>
                                        </table>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- Footer -->
                <tr>
                    <td>
                        <table style="margin-top: 20px; margin-bottom: -20px;" cellpadding="0" cellspacing="0"
                               align="center" border="0">
                            <tbody>
                            <tr>
                                <td width="27">
                                    <a href="" target="_blank" data-saferedirecturl="" width="27" height="27"
                                       alt="Facebook" class="CToWUd">
                                        <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1500274188/unnamed_4_zq4trt.gif"
                                             width="27" height="27" alt="Google Plus" class="CToWUd">
                                    </a>
                                </td>
                                <td width="6">
                                <td width="27">
                                    <a href="" target="_blank" data-saferedirecturl="" width="27" height="27"
                                       alt="Twitter" class="CToWUd">
                                        <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1500274609/unnamed_5_pjqgvk.gif"
                                             width="27" height="27" alt="Google Plus" class="CToWUd">
                                    </a>
                                </td>
                                <td width="6">

                                </td>
                                <td width="27">
                                    <a href="" target="_blank"
                                       data-saferedirecturl="">
                                        <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1500274188/unnamed_dntuvv.gif"
                                             width="27" height="27" alt="Google Plus" class="CToWUd">
                                    </a>
                                </td>
                                <td width="6"></td>
                                <td width="27">
                                    <a href="" target="_blank"
                                       data-saferedirecturl="">
                                        <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1500274189/unnamed_3_iwunsm.gif"
                                             width="27" height="27" alt="Linkedin" class="CToWUd">
                                    </a>
                                </td>
                                <td width="6">

                                </td>
                                <td width="27">
                                    <a href="" target="_blank"
                                       data-saferedirecturl="">
                                        <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1500274189/unnamed_2_c00ycp.gif"
                                             width="27" height="27" alt="Youtube" class="CToWUd">
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <table style="<?php echo e($style['email-footer']); ?>" align="center" width="570" cellpadding="0"
                               cellspacing="0">
                            <tr>
                                <td style="<?php echo e($fontFamily); ?> <?php echo e($style['email-footer_cell']); ?>">
                                    <p style="<?php echo e($style['paragraph-sub']); ?>">
                                        &copy; <?php echo e(date('Y')); ?>

                                        <a style="<?php echo e($style['anchor']); ?>" href="<?php echo e(url('/')); ?>"
                                           target="_blank"><?php echo e(config('app.name')); ?></a>.
                                        All rights reserved.
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
