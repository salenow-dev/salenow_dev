<footer id="footer" class="clearfix">
    <div class="footer-bottom">
        <div class="container">
            <p class="pull-left"> &copy; <?php echo e(date("Y")); ?> Saleme.lk (Pvt) Ltd. All right reserved. </p>
        </div>
    </div>
</footer>
<!--back to top-->
<div class="scroll-top-wrapper ">
    <span class="scroll-top-inner">
        <i class="ion-ios-arrow-up"></i>
    </span>
</div>
<!--end back to top-->

</body>
<?php echo e(Html::script('js/salenow/layout/saleme-custom.js')); ?>

<script src="<?php echo e(asset('plugins/default/platform.js')); ?>" async defer></script>
<script type="text/javascript">
    $(document).ready(function () {
        //alert fade out
        setTimeout(function () {
            $('.alert').fadeOut('fast');
        }, 6000);
    })
    function blocbtn(msg = null) {
        $('.blockui').block({message: msg});
    }
    function unblocbtn() {
        $('.blockui').unblock();
    }

    //search box
    $('#more_query').click(function () {
        $('#search_form').submit();
    });
    $('#more_query1').click(function () {
        $('#search_form1').submit();
    });


    //FAQ
    function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading').find(".more-less").toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
    //
    //$('.selectpicker').selectpicker()

    window.___gcfg = {
        lang: 'en-US',
        parsetags: 'onload'
    };

    // Need Help float toggle
    $(".callcenterBox").click(function (e) {
        $(this).toggleClass('active');
    });

    //START Sticky Header
//    $(window).scroll(function () {
//        if ($(window).scrollTop() >= 300) {
//            $('.navbar').addClass('fixed-header');
//            $('.sticky-seachbar').addClass('fixed-search');
//        }
//        else {
//            $('.navbar').removeClass('fixed-header');
//            $('.sticky-seachbar').removeClass('fixed-search');
//        }
//    });
    //END Sticky Header
</script>
</html>










<?php echo $__env->make('default.popup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>