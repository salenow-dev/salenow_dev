<?php $__env->startSection('title', (!empty($seo))? $seo: 'All Ads '.(!empty($location_slug) ? 'in '.ucfirst(str_replace('_',' ',$location_slug)) :'')); ?>
<?php $__env->startSection('og-title', 'All Ads | Saleme.lk'); ?>
<?php $__env->startSection('og-description', 'All ads | Saleme.lk'); ?>
<?php $__env->startSection('content'); ?>

    <div class="col-md-7 col-sm-8 cont-bg cont-bg-m all-ads-cont">
        <div class="row">
            <div class="item-content-block tags"> <?php if(!empty($scategory)): ?> <span><?php echo e($scategory); ?><a
                            href="<?php echo e(asset('')); ?>ads/<?php echo e(!empty($location_slug) ? $location_slug :'sri-lanka'); ?>"
                            class="fa fa-times pull-right"></a> </span> <?php else: ?> <?php if(!empty($category)): ?> <span><?php echo e($category); ?>

                    <a href="<?php echo e(asset('')); ?>ads/<?php echo e(!empty($location_slug) ? $location_slug :'sri-lanka'); ?>"
                       class="fa fa-times pull-right"></a></span> <?php endif; ?> <?php endif; ?> <?php if(!empty($location_slug) && $location_slug !='sri-lanka'): ?>
                    <span><?php echo e(ucfirst(str_replace('_',' ',$location_slug))); ?><a
                                href="<?php echo e(asset('')); ?>ads/sri-lanka/<?php echo e(!empty($scategory_slug) ? $scategory_slug :!empty($category_slug) ? $category_slug:''); ?><?php if(!empty($REDIRECT_QUERY_STRING)): ?>?<?php echo e($REDIRECT_QUERY_STRING); ?><?php endif; ?>"
                                class="fa fa-times pull-right"></a></span> <?php endif; ?> </div>
            <div class="col-md-12 breadcrumb-div hidden-xs">
                <p class="breadcrumb-text"><a href="<?php echo e(URL::to('/')); ?>"><i class="ion-ios-home-outline"></i> </a>&nbsp;/&nbsp; <?php if(!empty($location_slug) || !empty($category) || !empty($scategory) || !empty($scategory)): ?><?php echo e(!empty($location_slug) ? ucfirst(str_replace('_',' ',$location_slug.'&nbsp;&nbsp;/&nbsp;')) :''); ?><?php if(!empty($category)): ?><?php echo e(!empty($category) ? $category.'&nbsp;&nbsp;/&nbsp;' :''); ?><?php elseif(!empty($scategory)): ?><?php echo e(!empty($scategory) ? $scategory.'&nbsp;&nbsp;/&nbsp;' :''); ?><?php else: ?><?php echo e(!empty($scategory) ? $scategory.'&nbsp;&nbsp;/&nbsp;' :''); ?><?php endif; ?><?php echo e(' All Ads'); ?><?php else: ?><?php echo e(' All Ads in Sri Lanka'); ?><?php endif; ?>
                </p>
            </div>
        </div><?php if(!empty($addlist)): ?>
            <div class="row cont-bg">
                <div class="col-xs-12 breadcrumb-div">
                    <p class="search-count"><b>We found&nbsp;<?php echo e($addlist->total()); ?>&nbsp;result(s)</b>
                        showing&nbsp;<?php echo e($addlist->count()+((!empty($boostAdCount))? $boostAdCount:0)); ?>&nbsp;Ads</p>
                </div>
            </div>
            <?php if(!empty($allBoostAds)): ?>
                <?php $__currentLoopData = $allBoostAds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $datas): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <?php $cnt = 1; $publishedTime = timeForUser($datas->published_at); ?>
                    <a href="<?php echo e(asset('')); ?>ad/<?php echo e($datas->slug); ?>">
                        <div class="row search-list-item pinadbg pin<?php echo e($cnt); ?>"> <?php if($datas->ad_boost_status !='none'): ?>
                                <div class="label-special pinned"><i class="ion-star"></i>Top Ad</div><?php endif; ?>
                            <div class="col-md-4 col-sm-4 col-xs-4 thumb-img-div"> <?php if($datas->featuredimage!=''): ?> <img
                                        class="img-responsive pull-right"
                                        src="<?php echo e(asset('')); ?>saleme/images/uploads/<?php echo e($datas->adid); ?>/thumb/<?php echo e($datas->featuredimage); ?>"
                                        alt="<?php echo e($datas->subcategory.' - '.strtolower($datas->adtitle) .' in '.$datas->city); ?>"> <?php else: ?>
                                    <img class="img-responsive pull-left" src="<?php echo e(asset('')); ?>images/saleme/no_image.jpg"
                                         alt="<?php echo e($datas->subcategory.' - '.strtolower($datas->adtitle) .' in '.$datas->city); ?>"> <?php endif; ?>
                            </div>
                            <div class="col-md-8 col-xs-9 col-sm-8 all-ads-details hidden-xs">
                                <h3 class="item-title"><?php echo e($datas->adtitle); ?></h3>
                                <p class="item-loc-cat"><i class="ion-clock"></i>&nbsp;<?php echo e($publishedTime); ?>&nbsp;|&nbsp;<i
                                            class="ion-location"></i><?php echo e($datas->city); ?>&nbsp;|&nbsp;<i
                                            class="ion-ios-pricetag"></i><?php echo e($datas->subcategory); ?></p>
                                <?php if(!empty($datas->mileage)): ?>
                                    <p class="vehicle-mileage"><i
                                                class="ion-speedometer"></i>&nbsp;<?php echo e($datas->mileage > 0 ? number_format($datas->mileage) : '0'); ?>

                                        &nbsp;KM</p><?php endif; ?>
                                <h4 class="item-price">
                                    Rs&nbsp;<?php echo e($datas->price > 0 ? number_format($datas->price) : '0'); ?>

                                </h4>
                                <div class="pri-adimages-div"> <?php $__currentLoopData = $datas->adimages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $imgs_addi): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?> <img
                                            class="small_image"
                                            src="<?php echo e(asset('')); ?>saleme/images/uploads/<?php echo e($datas->adid); ?>/thumb/<?php echo e($imgs_addi->imagename); ?>"
                                            alt="<?php echo e($datas->subcategory.' - '.strtolower($datas->adtitle) .' in '.$datas->city); ?>"> <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                </div>
                                <?php if(!empty($datas->member_type) AND $datas->member_type === 'premium'): ?>
                                    <span class="label label-warning primemember-badge">Premium Member</span>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-9 col-xs-8 all-ads-details visible-xs pd-l-5 pd-r-0-xs">
                                <h3 class="item-title item-m"><?php echo e($datas->adtitle); ?></h3>
                                <p class="item-loc-cat"><?php echo e($datas->subcategory); ?>,&nbsp;<?php echo e($datas->city); ?></p>
                                <?php if(!empty($datas->mileage)): ?>
                                    <p class="vehicle-mileage"><?php echo e($datas->mileage > 0 ? number_format($datas->mileage) : '0'); ?>

                                        &nbsp;KM</p><?php endif; ?>
                                <h4 class="item-price"><?php echo e($datas->price > 0 ? 'Rs '.number_format($datas->price) : ''); ?></h4>
                                <?php if(!empty($datas->member_type) AND $datas->member_type === 'premium'): ?>
                                    <span class="label label-warning primemember-badge">Premium Member</span>
                                <?php endif; ?>
                            </div>
                            <div class="open-in-new-tab visible-xs ">
                                <p class="item-time"><?php echo e($publishedTime); ?></p>
                            </div>

                        </div>
                    </a>
                    <?php $cnt++; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            <?php endif; ?>
            <?php $__currentLoopData = $addlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $datas): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                <?php $publishedTime = timeForUser($datas->published_at); ?>
                <a href="<?php echo e(asset('')); ?>ad/<?php echo e($datas->slug); ?>">
                    <div class="row search-list-item">
                        <div class="col-md-4 col-sm-4 col-xs-4 thumb-img-div"> <?php if($datas->featuredimage!=''): ?> <img
                                    class="img-responsive pull-right lazy" src="<?php echo e(asset('')); ?>images/loading-image.gif"
                                    data-original="<?php echo e(asset('')); ?>saleme/images/uploads/<?php echo e($datas->adid); ?>/thumb/<?php echo e($datas->featuredimage); ?>"
                                    alt="<?php echo e($datas->subcategory.' - '.strtolower($datas->adtitle) .' in '.$datas->city); ?>"> <?php else: ?>
                                <img class="img-responsive pull-left" src="<?php echo e(asset('')); ?>images/saleme/no_image.jpg"
                                     alt="<?php echo e($datas->subcategory.' - '.strtolower($datas->adtitle) .' in '.$datas->city); ?>"> <?php endif; ?>
                        </div>
                        
                        <div class="col-md-8 col-xs-9 col-sm-8 all-ads-details hidden-xs">
                            <h3 class="item-title"><?php echo e($datas->adtitle); ?></h3>
                            <p class="item-loc-cat "><i class="ion-clock"></i>&nbsp;<?php echo e($publishedTime); ?>&nbsp;|&nbsp; <i
                                        class="ion-ios-location-outline"></i>&nbsp;<?php echo e($datas->city); ?>&nbsp;|&nbsp; <i
                                        class="ion-ios-pricetags-outline"></i>&nbsp;<?php echo e($datas->subcategory); ?>

                            </p><?php if(!empty($datas->mileage)): ?>
                                <p class="vehicle-mileage"><i
                                            class="ion-speedometer"></i>&nbsp;<?php echo e($datas->mileage > 0 ? number_format($datas->mileage): '0'); ?>

                                    &nbsp;KM</p><?php endif; ?>
                            <?php if($datas->subcategory != 'Jobs in Sri Lanka'): ?>
                                <h4 class="item-price">
                                    <?php echo e(($datas->price > 0 AND $datas->price != 0 )? 'RS '. number_format($datas->price) : 'Negotiable'); ?></h4>
                            <?php endif; ?>
                            <?php if(!empty($datas->member_type) AND $datas->member_type === 'premium'): ?>
                                <span class="label label-warning primemember-badge">Premium Member</span>

                            <?php endif; ?>
                        </div>
                        
                        <div class="col-md-9 col-xs-8 all-ads-details visible-xs pd-l-5 pd-r-0-xs">
                            <h3 class="item-title item-m"><?php echo e($datas->adtitle); ?></h3>
                            <p class="item-loc-cat"><?php echo e($datas->subcategory); ?>,&nbsp;<?php echo e($datas->city); ?></p>
                            <?php if(!empty($datas->mileage)): ?>
                                <p class="vehicle-mileage"><?php echo e($datas->mileage > 0 ? number_format($datas->mileage) : '0'); ?>

                                    &nbsp;KM</p><?php endif; ?>
                            <?php if($datas->subcategory != 'Jobs in Sri Lanka'): ?>
                                <h4 class="item-price"><?php echo e(($datas->price > 0 AND $datas->price != 0 ) ? 'Rs '.number_format($datas->price) : 'Negotiable'); ?></h4>
                            <?php endif; ?>
                            <?php if(!empty($datas->member_type) AND $datas->member_type === 'premium'): ?>
                                <span class="label label-warning primemember-badge">Premium Member</span>
                            <?php endif; ?>
                        </div>

                        <div class="open-in-new-tab visible-xs ">
                            <p class="item-time"><?php echo e($publishedTime); ?></p>
                        </div>
                    </div>
                </a>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            <div class="row"><?php echo e($addlist->appends(Illuminate\Support\Facades\Input::except('page'))->links('vendor.pagination.custom')); ?></div><?php endif; ?>
    </div>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js"></script>
    <script>
        $(".lazy").lazyload({
            effect: "fadeIn",
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.filters', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>