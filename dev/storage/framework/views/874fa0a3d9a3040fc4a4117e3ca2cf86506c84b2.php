<?php if($paginator->hasPages()): ?>
    <ul class="pager">
        
        <?php if($paginator->onFirstPage()): ?>
            <li class="disabled"><span>← Previous</span></li>
        <?php else: ?>
            <li><a href="<?php echo e($paginator->previousPageUrl()); ?>" rel="prev">← Previous</a></li>
        <?php endif; ?>

        
        
        

        <?php if($paginator->currentPage() > 4): ?>
            <li class="disabled hidden-xs"><span>...</span></li>
        <?php endif; ?>
        <?php $__currentLoopData = range(1, $paginator->lastPage()); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <?php if($i >= $paginator->currentPage() - 2 && $i <= $paginator->currentPage() + 2): ?>
                <?php if($i == $paginator->currentPage()): ?>
                    <li class="active hidden-xs"><span><?php echo e($i); ?></span></li>
                <?php else: ?>
                    <li class="hidden-xs"><a href="<?php echo e($paginator->url($i)); ?>"><?php echo e($i); ?></a></li>
                <?php endif; ?>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        <?php if($paginator->currentPage() < $paginator->lastPage() - 3): ?>
            <li class="disabled hidden-xs"><span>...</span></li>
        <?php endif; ?>

        
        
        

        
        <?php if($paginator->hasMorePages()): ?>
            <li><a href="<?php echo e($paginator->nextPageUrl()); ?>" rel="next">Next →</a></li>
        <?php else: ?>
            <li class="disabled"><span>&raquo;</span></li>
        <?php endif; ?>
    </ul>
<?php endif; ?>