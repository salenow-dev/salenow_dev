<?php $__env->startSection('title', '| Main Category'); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <?php echo Form::open(['url' => 'settings/category','id'=>'frmRegister','class'=>'form-horizontal','autocomplete' => 'off']); ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Main Category</strong> | Add New Record</h3>
                    <div class="pull-right">
                        <a href="category/categoryorder" class="btn btn-primary">Change Categoty Order</a>
                    </div>
                </div>
                <div class="panel-body">
                    <?php echo $__env->make('backend.category.form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
                <div class="panel-footer">
                    <button class="btn btn-default">Clear Form</button>
                    <button class="btn btn-primary pull-right" type="submit" id="aaa">Submit</button>
                </div>
            </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Manage</strong> | Main Categories</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table datatable  table-striped table-bordered" id="example">
                                <thead>
                                <tr>
                                    <th>Category Name</th>
                                    <th>Category Order</th>
                                    <th>Referance</th>
                                    <th>Icon</th>
                                    <th>Status</th>
                                    
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__empty_1 = true; $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); $__empty_1 = false; ?>
                                    <tr id="<?php echo e($category->id); ?>">
                                        <td><?php echo e($category->category_name); ?></td>
                                        <td><?php echo e($category->sequence); ?></td>
                                        <td><?php echo e($category->category_code); ?></td>
                                        <td><?php echo e($category->icon); ?></td>
                                        <td><?php echo e($category->status); ?></td>
                                        
                                            
                                            
                                               
                                        
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); if ($__empty_1): ?>
                                    <tr>
                                        <td colspan="5">No Records</td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>



    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>