@extends('layouts.app')
@section('title', '| Main Category')
@section('content')
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['url' => 'settings/category','id'=>'frmRegister','class'=>'form-horizontal','autocomplete' => 'off']) !!}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Main Category</strong> | Add New Record</h3>
                    <div class="pull-right">
                        <a href="category/categoryorder" class="btn btn-primary">Change Categoty Order</a>
                    </div>
                </div>
                <div class="panel-body">
                    @include('backend.category.form')
                </div>
                <div class="panel-footer">
                    <button class="btn btn-default">Clear Form</button>
                    <button class="btn btn-primary pull-right" type="submit" id="aaa">Submit</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Manage</strong> | Main Categories</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table datatable  table-striped table-bordered" id="example">
                                <thead>
                                <tr>
                                    <th>Category Name</th>
                                    <th>Category Order</th>
                                    <th>Referance</th>
                                    <th>Icon</th>
                                    <th>Status</th>
                                    {{--<th>Action</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($categories as $category)
                                    <tr id="{{$category->id}}">
                                        <td>{{$category->category_name}}</td>
                                        <td>{{$category->sequence}}</td>
                                        <td>{{$category->category_code}}</td>
                                        <td>{{$category->icon}}</td>
                                        <td>{{$category->status}}</td>
                                        {{--<td class="action-col"><a href="/settings/category/{{$category->id}}/edit" class="edit unline"><span class="lnr lnr-pencil"></span></a>--}}
                                            {{--|--}}
                                            {{--<a class="delete unline"--}}
                                               {{--uid="{{$category->id}}" tag="category"><span class="lnr lnr-trash"></span></a>--}}
                                        {{--</td>--}}
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">No Records</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>



    {{--    <script src="{{ asset('backend/assets/js/default/commonajax.js') }}"></script>--}}
@endsection