@extends('layouts.app')
@section('title', '| Main Category')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Main Category</strong> | Change Category Order by Drag & Drop</h3>
                </div>
                <div class="panel-body">
                    <div class="dd col-md-3">
                        <ul class="dd-list" id="sortable">
                            @foreach($categories as $category)
                                <li id="item-{{$category->id}}" class="dd-item">
                                    <div class="dd-handle dd3-handle">Drag</div>
                                    <div class="dd3-content">{{$category->category_name}}</div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('ul').sortable({
                axis: 'y',
                stop: function (event, ui) {
                    var data = $(this).sortable('serialize');
                    $.ajaxSetup({
                        headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')}
                    });
                    $.ajax({
                        data: data,
                        type: 'POST',
                        url: 'order',
                        success: function (res) {
                            if (res.response.message != '') {
                                noty({
                                    text: res.response.message,
                                    layout: 'topRight',
                                    type: 'success',
                                    timeout: 3000,
                                });
                                $(this).closest("tr").hide('slow');
                            } else {
                                noty({
                                    text: res.response.error,
                                    layout: 'topRight',
                                    type: 'error',
                                    timeout: 3000,
                                });
                            }
                        }
                    });
                }
            });
        });
    </script>
    {{--    <script src="{{ asset('backend/assets/js/default/commonajax.js') }}"></script>--}}
@endsection