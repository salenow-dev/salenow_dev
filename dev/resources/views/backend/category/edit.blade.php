@extends('layouts.app')
@section('title', '| Main Category')
@section('content')
    <div class="row">
        <div class="col-md-12">
            {!! Form::model($category, ['url'=>'settings/category/'.$category->id,'class'=>'form-horizontal','autocomplete' => 'off']) !!}
                        {{method_field('PATCH')}}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Main-Category</strong> | Update Record</h3>
                </div>
                <div class="panel-body">
                    @include('backend.category.form')
                </div>
                <div class="panel-footer">
                    <button class="btn btn-default">Clear Form</button>
                    <button class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {{--{!! Form::open(['url'=>'/settings/category/changesubcategory','class'=>'form-horizontal','autocomplete' => 'off']) !!}--}}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Change Sub-Category</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-md-4">
                            <ul class="dd-list ui-sortable" id="sortable">
                                @forelse($category->subcategories as $subcategory)
                                    <li id="item-1" class="dd-item ui-sortable-handle">
                                        <div class="dd-handle dd3-handle">Drag</div>
                                        <div class="dd3-content">
                                            {{$subcategory->sub_category_name}}
                                            <div class="col-md-4  pull-right">
                                                {{--{!! Form::select('adtype[]', $mainCategories, null, ['class' => 'form-control','multiple' ]) !!}--}}
                                                {!! Form::select('changeMainCat', array_merge(['0'=>'Change'],$mainCategories) , null, ['class' => 'form-control changeMainCat','sub_cat'=>$subcategory->id ]) !!}
                                            </div>

                                        </div>

                                    </li>
                                @empty
                                    <li id="item-1" class="dd-item ui-sortable-handle">
                                        <div class="dd3-content">NO Sub-Categories Assigned</div>
                                    </li>
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            {{--            {!! Form::close() !!}--}}
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('.changeMainCat').change(function () {
                var mainCategory = this.value;
                var sub_cat_id = $(this).attr('sub_cat');

                $.ajaxSetup({
                    headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')}
                });
                $.ajax({
                    type: "POST",
                    url: "/settings/category/changesubcategory",
                    data: {"category_id": mainCategory, 'sub_cat_id': sub_cat_id},
                    success: function (data) {
                        if (data.response.message != '') {
                            noty({
                                text: data.response.message,
                                layout: 'topRight',
                                type: 'success',
                                timeout: 3000,
                            });
                            $(this).closest("li").hide('slow');
                        } else {
                            noty({
                                text: 'Sorry! Try again.)',
                                layout: 'topRight',
                                type: 'error',
                                timeout: 3000,
                            });
                        }
                    }
                });
            });
        });
    </script>
@endsection