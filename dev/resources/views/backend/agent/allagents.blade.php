@extends('layouts.app')
@section('title', '| Member Requests for Become Premium')
@section('content')

    @if(isset($agent))
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3><span class="fa fa-mail-forward"></span> All Agents Manage</h3>

                        <div class="table-responsive">

                            <table class="table datatable table-striped table-bordered" id="table">
                                <thead>
                                <tr>

                                    <th>Name</th>
                                    <th width="180">Date</th>
                                    <th width="50">actions</th>
                                    <th width="50">Performance</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($agent))
                                    @foreach($agent as $user)

                                        {{--{{dd($users->company_name)}}--}}

                                        <tr>
                                            <td class="">{{$user->company_name}}</td>
                                            <td class=text-center">
                                                {{$user->created_at->todatestring()}}
                                            </td>

                                            <td class=text-center">
                                                <form  method="post" action="">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="id" value="{{$user->id}}" >
                                                    @if($user->status == 'active')
                                                        <button type="submit" href="" value="" name="deactive" class="btn btn-danger">Deactive <span class="badge"></span></button>

                                                    @else
                                                        <button type="submit" href="" value="" name="active" class="btn btn-danger">Active <span class="badge"></span></button>
                                                    @endif

                                                </form>
                                            </td>
                                            <td class=text-center">
                                                <form  method="post" action="/agents/agentsingle">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="id" value="{{$user->id}}" >
                                                    <button class="btn btn-info" type="submit" >View Page</button>
                                                </form>
                                            </td>
                                    @endforeach
                                @endif


                                </tbody>

                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @endif

    <script>


    </script>

@endsection