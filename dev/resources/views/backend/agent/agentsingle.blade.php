@extends('backend.backend_dashboard_layout')
@section('title', 'Dashboard')
@section('content')
    <!-- START WIDGETS -->
    <div class="row">
        <div class="col-md-3">
            <div class="panel-body profile bg-primary">

                <div class="profile-image">
                    <img src="{{asset('/salenow/images/prime-members/'.$member->member_id.'/'.$member->profile_image)}}"  alt="" style="width: 50px; border: 2px solid #fff;">
                </div>

                <div class="profile-data">
                    <div class="profile-data-name">{{$member->company_name}}</div>
                    <div class="profile-data-title">{{$member->address}}</div>
                </div>
                <div class="profile-controls">

                    <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-external-link"></span></a>
                </div>

            </div>
        </div>
        <div class="col-md-3">
            <!-- START WIDGET SLIDER -->
            <div class="widget widget-default widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-bullhorn"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count">
                        {{--{{$memberCount}}--}}
                    </div>
                    <div class="widget-title">Today Ads</div>
                    <div class="col-md-6">
                    <div style="font-size: 40px">{{$vehicle+$property}}</div>
                    </div>
                    <div class="col-md-3">
                        <span class="fa fa-car" style="font-size: 20px"></span>
                        <h3>{{$vehicle}}</h3>
                    </div>
                    <div class="col-md-3">
                        <span class="fa fa-home" style="font-size: 20px"></span>
                        <h3>{{$property}}</h3>
                    </div>

                </div>

            </div>
            <!-- END WIDGET SLIDER -->
        </div>
        <div class="col-md-3">
            <!-- START WIDGET SLIDER -->
            <div class="widget widget-default widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-bell"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count">
                        {{--{{$memberCount}}--}}
                    </div>
                    <div class="widget-title">Monthly Ads</div>
                    <div class="col-md-6">
                        <div style="font-size: 40px">{{$vehiclemonth+$propertymonth}}</div>
                    </div>
                    <div class="col-md-3">
                        <span class="fa fa-car" style="font-size: 20px"></span>
                        <h3>{{$vehiclemonth}}</h3>
                    </div>
                    <div class="col-md-3">
                        <span class="fa fa-home" style="font-size: 20px"></span>
                        <h3>{{$propertymonth}}</h3>
                    </div>

                </div>

            </div>
            <!-- END WIDGET SLIDER -->
        </div>
        <div class="col-md-3">
            <!-- START WIDGET REGISTRED -->
            <div class="widget widget-info widget-no-subtitle">
                <div class="widget-big-int">Rs <span class="num-count">{{$earning}}</span></div>
                <div class="widget-subtitle"><b>Total Earnings This Month</b></div>

            </div>
            <!-- END WIDGET REGISTRED -->
        </div>

    </div>
    <!-- END WIDGETS -->
    <div class="row">
        <div class="col-md-4">
            <!-- START PROJECTS BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Ads Count Analysis</h3>
                        <span>Last Two Weeks</span>
                    </div>

                </div>
                <div class="panel-body panel-body-table">
                    <div class="table-responsive">
                        <table class="table table-condensed table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="50%">Date</th>
                                <th width="20%">Ads Count</th>
                                {{--<th width="20%">Member Count</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($pastWeekAds))
                                {{--{{dd($pastWeekAds)}}--}}
                                @foreach($pastWeekAds as $item)
                                    <?php $myDate = new \Carbon\Carbon($item->date); ?>
                                    {{--$item->created_at->toFormattedDateString()--}}
                                    <tr>
                                        <td><strong>{{$myDate->format('M d, Y l')}}</strong></td>
                                        <td><span class="label label-danger">{{$item->count}}</span></td>
                                        {{--<td><span class="label label-primary">{{$item->member_count}}</span></td>--}}
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END PROJECTS BLOCK -->
        </div>
        <div class="col-md-6">

            <!-- START LINE CHART -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Ads Count Graph</h3>
                        <span>Last Two Weeks</span>
                    </div>
                </div>
                <div class="panel-body">
                    <div id="morris-line-example" style="height: 400px;"></div>
                </div>
            </div>
            <!-- END LINE CHART -->

        </div>
        <div class="col-md-2">


                <a href="/agents/makepayment/{{$member->member_id}}" target="_blank"><button  class="btn btn-danger action-btns col-md-12 btn-lg" ><i class="fa fa-print"></i>Make Payment</button></a>


        </div>

    </div>

    <script type="text/javascript" src="{{ asset("backend/assets/js/plugins/morris/raphael-min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("backend/assets/js/plugins/morris/morris.min.js") }}"></script>
    <script type="text/javascript">
        var morrisCharts = function() {

            Morris.Line({
                element: 'morris-line-example',
                data: [
                        @foreach($pastWeekAds as $item)
                    { y: '{{$item->date}}', a: '{{$item->count}}'},
                    @endforeach
                ],
                xkey: 'y',
                ykeys: ['a'],
                labels: ['Ads'],
                resize: true,
                lineColors: ['#33414E']
            });
        }();
    </script>

@endsection
