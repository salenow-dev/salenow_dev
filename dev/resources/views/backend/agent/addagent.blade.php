@extends('layouts.app')
@section('title', '| Member Requests for Become Premium')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">

                    <h3><span class="fa fa-mail-forward"></span> Add New Agent</h3>
                    <p style="color: #940600">
                        @if (isset($message))
                        <div class="alert {{$alert}}" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                            <strong></strong>  {{$message}}
                        </div>

                        @endif
                    </p>
                    <form  method="post" action="/agents/searchmember">
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="col-md-6">

                                    <input class="form-control" name="email" type="email"  id="" placeholder="Email Address" required>
                            </div>
                        </div> <!-- form-group// -->
                        <div class="col-md-2">
                            <div class="form-group">
                                <button class="btn btn-success btn-block"><span class="fa fa-plus"></span> Add new Agent</button>
                            </div>

                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>
@if(isset($users))
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3><span class="fa fa-mail-forward"></span>All Premium Members in - {{$email}}  </h3>

                                        <div class="table-responsive">

                        <table class="table table-bordered table-striped table-actions">
                            <thead>
                            <tr>

                                <th>Name</th>
                                <th width="130">Date</th>
                                <th width="130">Status</th>
                                <th width="50">actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            {{--{{dd($users)}}--}}
                            @if(count($users))
                                @foreach($users as $user)
                                    {{--{{dd($users->company_name)}}--}}

                                    <tr>
                                        <td class="">{{$user->company_name}}</td>
                                        <td class=text-center">
                                            {{$user->created_at->todatestring()}}
                                        </td>
                                        <td class=text-center">
                                            @if($user->verified)
                                                <span class="label label-success label-form">Active</span>

                                            @else
                                                <span class="label label-danger label-form">Deactive</span>
                                            @endif
                                        </td>
                                        <td class=text-center">
                                            <form  method="post" action="/agents/newagent">
                                                {{csrf_field()}}
                                                <input type="hidden" name="id" value="{{$user->id}}" >
                                            <button type="submit" href="" value="" name="deactivate" class="btn btn-danger">Add to Agent <span class="badge"></span></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif


                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @endif

    <script>


    </script>

@endsection