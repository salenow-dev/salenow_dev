<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Advertismenttypes Type Name</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            {!! Form::text('typename', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Type Referance</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            {!! Form::text('typeref', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Status</label>
    <div class="col-md-6 col-xs-12">
        {!! Form::select('status', array('active'=>'Active','inactive'=>'Inactive'), 'active', ['class' => 'form-control' ]) !!}
    </div>
</div>