@extends('layouts.app')
@section('title', '| Registered Member List')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Manage</strong> | Registered Premium Members List</h3>
                        <div class="col-md-3 pull-right">
                            <a href="{{asset('')}}admin/member/premium/export" target="_blank">
                                <button class="btn btn-info pull-right"><span class="fa fa-download"></span> Export PDF
                                </button>
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table datatable table-striped table-bordered" id="table">
                                <thead>
                                <tr>
                                    <th style="width: 250px !important;">Company</th>
                                    <th style="width: 200px !important;">Created</th>
                                    <th style="width: 200px !important;">Period</th>
                                    <th style="width: 200px !important;">Expired on</th>
                                    {{--<th style="width: 200px !important;">View</th>--}}
                                    <th style="width: 200px !important;">Action</th>
                                    {{--<th>created_at</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($members))
                                    @foreach($members as $item)
                                        <tr id="{{$item->id}}">
                                            <td>
                                                <b>Name : </b>{{$item->company_name}}<br>
                                                <b>Email : </b>{{$item->email}}<br>
                                                <b>Mobile : </b>@foreach($item->mobile as $mob)
                                                    {{$mob. ' / '}}
                                                @endforeach
                                                <br>
                                                <a href="{{asset('')}}{{$item->slug}}" target="_blank">View Account</a>
                                            </td>
                                            <td>{{$item->created_at->toFormattedDateString()}}</td>
                                            <td>
                                                <select name="" id="" class="form-control period"
                                                        premiummember="{{$item->id}}">
                                                    <option value="" {{is_null($item->period)?'selected':''}}>Select
                                                        Period
                                                    </option>
                                                    <option value="1" {{($item->period)== 1 ?'selected':''}}>1 Month
                                                    </option>
                                                    <option value="3" {{($item->period)== 3 ?'selected':''}}>3 Months
                                                    </option>
                                                    <option value="6" {{($item->period)== 6 ?'selected':''}}>6 Months
                                                    </option>
                                                    <option value="12" {{($item->period)== 12 ?'selected':''}}>12
                                                        Months
                                                    </option>
                                                    <option value="0" {{is_null($item->period)?'selected':''}}>Life Time
                                                    </option>
                                                </select>
                                            </td>
                                            <td><p id="enddate_{{$item->id}}">
                                                    {{--{{$item->created_at->toFormattedDateString()}}--}}
                                                    @if(!is_null($item->period))
                                                        {{$endDate = $item->created_at->addMonths($item->period)->toFormattedDateString()}}

                                                    @else
                                                        Select Valid Period
                                                    @endif

                                                </p>
                                                <p id="expire_{{$item->id}}"
                                                   class="text-success {{($item->created_at->addMonths($item->period)->gt(\Carbon\Carbon::now()))?'text-success':'text-danger'}}">                                              {{--{{dd($item->created_at->addMonths($item->period))}}--}}
                                                    @if($item->created_at->addMonths($item->period)->gt(\Carbon\Carbon::now()))
                                                        @if($item->verified && is_null($item->expire_on) && $item->period == 0)
                                                            life time
                                                        @else
                                                            Expire Within
                                                            <b>{{$item->created_at->addMonths($item->period)->diffInDays(\Carbon\Carbon::now())}}
                                                                Day(s)</b>
                                                        @endif

                                                    @else
                                                        Account has Expired
                                                        <b>{{\Carbon\Carbon::now()->diffInDays($item->created_at->addMonths($item->period))}}
                                                            Day(s) ago</b>
                                                    @endif
                                                </p>
                                                <?php $arr = explode(' ', $item->company_name);
                                                $today = \Carbon\Carbon::now();
                                                ?>
                                                @if( ($item->created_at->addMonths($item->period)->gte(\Carbon\Carbon::now())) AND is_null($item->sms_sent_on) )
                                                    @if($item->created_at->addMonths($item->period)->diffInDays(\Carbon\Carbon::now()) < 7)
                                                        <button type="submit" value="" name="deactivate"
                                                                class="btn btn-info send_sms_btn"
                                                                message="Dear {{$arr[0]}}, Your Premium Account on SaleMe.lk will expired on {{$endDate = $item->created_at->addMonths($item->period)->toFormattedDateString()}}. Please Renew the account for more benefits. For more information call 0763818188. Thanks "
                                                                number="{{(!empty($item->mobile) AND count($item->mobile))?$item->mobile[0]:''}}"
                                                                premiummemberid="{{$item->id}}">
                                                            Send SMS <span
                                                                    class="badge"></span></button>
                                                    @endif
                                                @endif
                                            </td>
                                            {{--<td></td>--}}
                                            <td>
                                                Status : <b>{{($item->verified)?'Activated':'Deactivated'}}</b>
                                                <form class="form-horizontal" action="/admin/premiummember/deactive"
                                                      method="post">
                                                    {{csrf_field()}}
                                                    <div class="">
                                                        <input type="hidden" value="{{$item->id}}" name="id">
                                                        @if($item->verified == 1)
                                                            <button type="submit" value="deactivate" name="deactivate"
                                                                    class="btn btn-danger">Deactivate <span
                                                                        class="badge"></span></button>
                                                        @else
                                                            <button type="submit"
                                                                    class="btn btn-success" value="activate"
                                                                    name="activate">Activate <span
                                                                        class="badge"></span></button>
                                                        @endif
                                                    </div>

                                                </form>

                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            <div class="row">{{$members->links('vendor.pagination.custom')}}</div>
                        </div>
                    </div>
                </div>
            </div>
            {{--<a href="javascript:;" onclick="facebookPost()" class="btn btn-success">Post Offer to Facebook</a>--}}
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#table').dataTable({
                /* No ordering applied by DataTables during initialisation */
                "order": [],
                "bPaginate": false,
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
        })

        function facebookPost() {
            $.ajax({
                type: "POST",
                url: '/ads-manage/postfacebookoffer',
                data: {},
                success: function (data) {

                }
            });
        }


    </script>
    <script>
        $("select.period").change(function () {
            period = $(this).val();
            premiumid = $(this).attr('premiummember');
//            alert(premiumid);
            if (period != '' || period ==0) {
                var data = {'premiumid': premiumid, 'period': period}
                $.ajax({
                    type: "POST",
                    url: '/admin/member/premium/change-period/' + premiumid,
                    data: data,
                    success: function (res) {
                        var enddateID = '#enddate_' + premiumid;
                        var expireDate = '#expire_' + premiumid;
//                        alert(enddateID);
                        if (res.status == 'success') {
                            setTimeout(function () {
                                location.reload();
                            }, 2000)
                            $(enddateID).html(res.end_date);
                            $(expireDate).html(res.to_expire).removeClass('text-success').removeClass('text-danger').addClass(res.class);
                            noty({
                                text: res.message,
                                layout: 'topRight',
                                type: 'success',
                                timeout: 3000
                            });

                        } else {
                            noty({
                                text: res.message,
                                layout: 'topRight',
                                type: 'error',
                                timeout: 3000
                            });
                        }
                    }
                });
            }
        });

        //        send sms

        $(".send_sms_btn").click(function () {
            var message = $(this).attr('message');
            var number = $(this).attr('number');
            var premiummemberid = $(this).attr('premiummemberid');
            if (number != '') {
                var data = {'message': message, 'number': number, 'premiummemberid': premiummemberid}
                $.ajax({
                    type: "POST",
                    url: '/admin/member/premium/send-renew-sms',
                    data: data,
                    success: function (res) {
                        if (res.status) {
                            $(this).hide();
                            noty({
                                text: res.message,
                                layout: 'topRight',
                                type: 'success',
                                timeout: 3000
                            });
                            setTimeout(function () {
                                location.reload();
                            }, 1000);
                        } else {
                            noty({
                                text: res.message,
                                layout: 'topRight',
                                type: 'error',
                                timeout: 3000
                            });
                        }
                    }
                });
            }
        });
    </script>
    {{--    <script src="{{ asset('backend/assets/js/default/commonajax.js') }}"></script>--}}
@endsection