@extends('layouts.app')
@section('title', '| Member Requests for Become Premium')
@section('content')
    <?php
    $msg1=" You have won a SaleMe.lk T-shirt from Kokis Challenge. Subha aluth auruddak wewa! Post your Free Ad on SaleMe.lk. Share with your friends";
    $msg2=" You have won 200 LKR cash reload from SaleMe.lk Kokis Challenge. Subha aluth auruddak wewa! Post your Free Ad on SaleMe.lk. Share with your friends";
    $msg3=" You have won 100 LKR cash reload from SaleMe.lk Kokis Challenge. Subha aluth auruddak wewa! Post your Free Ad on SaleMe.lk. Share with your friends";
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Manage</strong> | Member Requests for Become Premium</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table datatable table-striped table-bordered" id="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>phone</th>
                                <th>Score</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            @if(count($lead))
                                <?php $x=1; ?>
                                @foreach($lead as $item)
                                    <?php $firstname=explode(" ", $item->name) ?>
                                    <tr id="{{$item->id}}">
                                        <td>{{$x}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->phone}}</td>
                                        <td>{{$item->score}}</td>
                                        <td>
                                            <form id="{{$item->id}}"
                                                  action="/ads-manage/game/sendsms" method="post">
                                                {{csrf_field()}}
                                                <textarea name="message" id="" cols="30" rows="5">Congratulations {{ucwords($firstname[0])}}@if($x==1){{$msg1}}@elseif($x>1 AND $x<=10){{$msg2}}@else{{$msg3}}@endif</textarea>
                                                <input type="hidden" name="phone" value="{{$item->phone}}">

                                                <button type="submit" class="btn btn-success">
                                                    <span class="badge badge-success">Send SMS</span>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                    <?php $x++; ?>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <a href="javascript:;" onclick="facebookPost()" class="btn btn-success">Post Offer to Facebook</a>
        </div>
    </div>

    {{--    <script src="{{ asset('backend/assets/js/default/commonajax.js') }}"></script>--}}
@endsection