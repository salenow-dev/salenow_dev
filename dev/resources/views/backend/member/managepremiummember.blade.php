@extends('layouts.app')
@section('title', '| Member Requests for Become Premium')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Manage Premium Members</strong></h3>
                </div>
                <div class="panel-body">

                    <form id="searchMember" class="form-horizontal" role="form" method="post"
                          action="/admin/member/searchmember">
                        {{csrf_field()}}
                        <div class="form-group col-md-4">
                            <input type="text" name="first_name" class="form-control" placeholder="Member Name"
                                   value="">
                        </div>
                        {{--<button type="submit" name="search" value="searchAll" class="btn btn-primary">Search Member&nbsp;<i--}}
                        {{--class="fa fa-search"></i></button>--}}
                        <button type="submit" name="search" value="searchMemAll" class="btn btn-primary">Search Premium
                            Member&nbsp;<i
                                    class="fa fa-search"></i></button>
                    </form>
                    @if(!empty($members))
                        <div class="panel-body list-group list-group-contacts">
                            @foreach($members as $member)
                                @php
                                    if ($member->member_type === 'private'){
                                    $mobile = \App\Models\Membercontact::where('member_id', $member->id)->first();
                                    }else{
                                    $mobile1 = \App\Models\Memberpremium::select('mobile')
                                    ->where('id', $member->id)->first();

                                    }

                                @endphp
                                <span class="list-group-item">
                                            <style>
                                                .sellerName .avatar {
                                                    background: #fea502;
                                                    width: 40px;
                                                    height: 40px;
                                                    float: left;
                                                    border-radius: 50%;
                                                    text-align: center;
                                                    color: #fafafa;
                                                    font-size: 30px;
                                                    text-transform: capitalize;
                                                    position: relative;
                                                    line-height: 38px;
                                                    margin-right: 10px;
                                                }
                                            </style>
                                            <div class="outter">
                                                @if(!empty($member->avatar))
                                                    <img src="{{$member->avatar}}" class="pull-left" alt="#">
                                                @else
                                                    <div class="sellerName ">
                                                        <span class="avatar pull-left">{{ucfirst (substr($member->company_name, 0, 1))}}</span>
                                                    </div>
                                                @endif
                                            </div>

                                        <span class="contacts-title">{{$member->company_name}}</span>
                                        <p>{{$member->email}}
                                            {{--<span class="label label-default label-form">District: {{($member->district)?$member->district->district_name:''}}</span>--}}
                                            {{--<span class="label label-default label-form">City: {{($member->city)?$member->city->city_name:''}}</span>--}}
                                            @if($member->verified == 1)

                                                <span class="label label-default label-form">Status: Confirmed</span>
                                            @else
                                                <span class="label label-danger label-form">Status: Pending</span>
                                            @endif
                                            @php
                                                $numbers = array_slice($member->mobile, 0,2);

                                            @endphp
                                            @foreach($numbers as $number)
                                                <span class="label label-success label-form">Mobile: {{$number}}</span>
                                            @endforeach

                                            </p>
                                                 <form class="form-horizontal" action="/admin/premiummember/deactive"
                                                       method="post">
                                                     {{csrf_field()}}
                                                     <div class="list-group-controls">
                                                          <input type="hidden" value="{{$member->id}}" name="id">
                                                         <button type="button" class="btn btn-primary">Send Message <span
                                                        class="badge"></span></button>
                                                         @if($member->verified == 1)
                                                             <button type="submit" value="deactivate" name="deactivate"
                                                                     class="btn btn-danger">Deactivate <span
                                                                         class="badge"></span></button>
                                                         @else
                                                             <button type="submit"
                                                                     class="btn btn-success" value="activate"
                                                                     name="activate">Activate <span
                                                                         class="badge"></span></button>
                                                     @endif

                                                    </form>
                                                    <form class="form-horizontal" action="/admin/premiummember/premiumdelete"
                                                          method="post">
                                                    {{csrf_field()}}
                                                        <input type="hidden" name="memberid" value="{{$member->id}}">
                                                        <button type="submit" value="" name="deactivate"
                                                                class="btn btn-danger">Delete Member <span
                                                                    class="badge"></span></button>
                                                    </form>
                        </div>


                        </span>
                        @endforeach
                        {{--<div class="row">{{$members->links()}}</div>--}}
                </div>
                @else
                    <p>No Members Found!</p>
                @endif
            </div>
        </div>
    </div>

    </div>

@endsection