@extends('layouts.app')
@section('title', '| Registered Member List')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <a type="button" class="btn btn-primary" href="/admin/agent"><i class="fa fa-hand-o-left"></i>&nbsp;Assign Agent</a>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Create New Agent</h3>
                </div>
                <div class="panel-body">
                    <form id="searchAgent" class="form-horizontal" role="form" method="post" action="/admin/agent/create">
                        {{csrf_field()}}
                        <div class="form-group col-md-4">
                            <label class="sr-only">Login</label>
                            <input type="text" name="email" class="form-control" placeholder="Member email">
                        </div>
                        <button type="submit" name="search" class="btn btn-primary">Search &nbsp;<i class="fa fa-search"></i></button>
                    </form>
                    <form id="assignAgent" class="form-horizontal" role="form" method="post"
                          action="/admin/agent/assign">
                        {{csrf_field()}}
                        @if(!empty($member))
                            <div class="">
                                <label class="col-md-3 col-xs-12 control-label">Shop Name</label>
                                <div class="col-md-4 col-xs-12">
                                    {!! Form::select('shop_id', [''=>'Select']+$shopmembers, '', ['class' => 'form-control' ]) !!}
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-body list-group list-group-contacts">
                                    @foreach($member as $member)
                                        <button type="submit" name="assign" class="btn btn-primary">Assign</button>
                                        <a class="list-group-item" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseOne_{{$member->id}}" aria-expanded="true"
                                           aria-controls="collapseOne">
                                            <style>
                                                .sellerName .avatar {
                                                    background: #fea502;
                                                    width: 40px;
                                                    height: 40px;
                                                    float: left;
                                                    border-radius: 50%;
                                                    text-align: center;
                                                    color: #fafafa;
                                                    font-size: 30px;
                                                    text-transform: capitalize;
                                                    position: relative;
                                                    line-height: 38px;
                                                    margin-right: 10px;
                                                }
                                            </style>
                                            <div class="outter">
                                                @if(!empty($member->avatar))
                                                    <img src="{{$member->avatar}}" class="pull-left" alt="#">
                                                @else
                                                    <div class="sellerName ">
                                                        <span class="avatar pull-left">{{ucfirst (substr($member->first_name, 0, 1))}}</span>
                                                    </div>
                                                @endif
                                            </div>
                                            <span class="contacts-title">{{$member->first_name}} {{$member->last_name}}</span>
                                            <p>{{$member->email}}
                                                <span class="label label-default label-form">Gender: {{$member->sex}}</span>
                                                <span class="label label-default label-form">District: {{($member->district)?$member->district->district_name:''}}</span>
                                                <span class="label label-default label-form">City: {{($member->city)?$member->city->city_name:''}}</span>
                                                <span class="label label-default label-form">Status: {{$member->status}}</span>
                                            </p>
                                            <div class="list-group-controls">
                                                <input type="hidden" name="agent_id" value="{{$member->id}}">

                                                {{--<button type="submit" class="btn btn-primary">Assign &nbsp;<i--}}
                                                {{--class="fa fa-hand-o-left"></i></button>--}}
                                            </div>
                                        </a>
                                        {{--<div id="collapseOne" class="collapse">--}}
                                        {{--<p>--}}
                                        {{--<span class="label label-default label-form">Gender: {{$member->sex}}</span>--}}
                                        {{--<span class="label label-default label-form">District: {{$member->district->district_name}}</span>--}}
                                        {{--<span class="label label-default label-form">City: {{$member->city->city_name}}</span>--}}
                                        {{--<span class="label label-default label-form">Status: {{$member->status}}</span>--}}
                                        {{--</p>--}}
                                        {{--</div>--}}
                                    @endforeach
                                </div>
                            </div>
                        @else
                            <p>No Members Found!</p>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#table').dataTable({
                /* No ordering applied by DataTables during initialisation */
                "order": [],
                "bPaginate": false,
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
        })

        function facebookPost() {
            $.ajax({
                type: "POST",
                url: '/ads-manage/postfacebookoffer',
                data: {},
                success: function (data) {

                }
            });
        }
    </script>
    {{--    <script src="{{ asset('backend/assets/js/default/commonajax.js') }}"></script>--}}
@endsection