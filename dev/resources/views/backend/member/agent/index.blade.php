@extends('layouts.app')
@section('title', '| Registered Member List')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Assign or Create New Agent</h3>
                </div>
                <div class="panel-body">
                    <h5>Search Member by E-mail Address</h5>
                    <form id="searchAgent" class="form-horizontal" role="form" method="post" action="/admin/agent">
                        {{csrf_field()}}
                        <div class="form-group col-md-4">
                            <label class="sr-only">Login</label>
                            <input type="text" name="email" class="form-control" placeholder="Member email"
                                   value="{{ old('email') }}">
                        </div>
                        <button type="submit" name="search" value="searchAll" class="btn btn-primary">Search All&nbsp;<i
                                    class="fa fa-search"></i></button>
                        <button type="submit" name="search" value="searchAgents" class="btn btn-primary">Search Agents&nbsp;<i
                                    class="fa fa-search"></i></button>
                    </form>

                    @if(!empty($members))
                        <div class="panel panel-default">
                            <form id="assignAgent" class="form-horizontal" role="form" method="post" action="">
                                {{csrf_field()}}
                                <div class="">
                                    <label class="col-md-3 col-xs-12 control-label">Select Shop Name</label>
                                    <div class="col-md-4 col-xs-12">
                                        {!! Form::select('shop_id', [''=>'Select']+$shopmembers, '', ['class' => 'form-control', 'id'=>'shop_id']) !!}
                                    </div>
                                </div>
                                <div class="panel-body list-group list-group-contacts">
                                    @foreach($members as $member)
                                        @php
                                            $agent = \App\Models\Member::find($member->agent_id);
                                        @endphp
                                        <span class="list-group-item">
                                            <style>
                                                .sellerName .avatar {
                                                    background: #fea502;
                                                    width: 40px;
                                                    height: 40px;
                                                    float: left;
                                                    border-radius: 50%;
                                                    text-align: center;
                                                    color: #fafafa;
                                                    font-size: 30px;
                                                    text-transform: capitalize;
                                                    position: relative;
                                                    line-height: 38px;
                                                    margin-right: 10px;
                                                }
                                            </style>
                                            <div class="outter">
                                                @if(!empty($member->avatar))
                                                    <img src="{{$member->avatar}}" class="pull-left" alt="#">
                                                @else
                                                    <div class="sellerName ">
                                                        <span class="avatar pull-left">{{ucfirst (substr($member->first_name, 0, 1))}}</span>
                                                    </div>
                                                @endif
                                            </div>
                                            <span class="contacts-title">{{$member->first_name}} {{$member->last_name}}</span>
                                            <p>{{$member->email}}
                                                <span class="label label-default label-form">Gender: {{$member->sex}}</span>
                                                <span class="label label-default label-form">District: {{($member->district)?$member->district->district_name:''}}</span>
                                                <span class="label label-default label-form">City: {{($member->city)?$member->city->city_name:''}}</span>
                                                <span class="label label-default label-form">Status: {{$member->status}}</span>
                                            </p>
                                            <input type="hidden" id="agent_id_{{$member->id}}" name="agent_id"
                                                   value="{{$member->id}}">
                                            <div class="list-group-controls">
                                                 <span id="checkIfAgent_{{$member->id}}">
                                                    @if($member->member_type == 'agent')
                                                         <span class="label label-default label-form">Agent &nbsp;<i
                                                                     class="fa fa-male"></i></span>
                                                     @endif
                                                </span>
                                                @if($member->member_type != 'agent')
                                                    @if(!$member->agent_id)
                                                        <button type="button" id="member_{{$member->id}}"
                                                                class="btn btn-primary makeAgent"
                                                                member="{{$member->first_name}} {{$member->last_name}}"
                                                                member_id="{{$member->id}}"><i
                                                                    class="fa fa-check-circle"></i>&nbsp;Create Agent</button>
                                                    @endif
                                                @else
                                                    <button type="submit" id="assign_{{$member->id}}"
                                                            name="assign" agent_id="{{$member->id}}"
                                                            class="btn btn-primary assign">
                                                                <i class="fa fa-chain"></i>&nbsp;Assign Me</button>
                                                @endif
                                                <span id="agentAssignSpan_{{$member->id}}">
                                                    @if($member->agent_id)
                                                        <span class="label label-success label-form">
                                                                {{--Agent: {{$member->agent->company_name}}--}}
                                                            Agent name: {{$agent->first_name}}
                                                            <i class="fa fa-check-circle-o"></i>
                                                            </span>
                                                        <a onclick="assignRemove({{$member->id}})"
                                                           id="assignRemove"
                                                           name="assign" class="btn btn-danger"> <i
                                                                    class="fa fa-trash-o"></i>&nbsp;Remove Agent</a>
                                                    @endif
                                                </span>
                                            </div>
                                        </span>
                                    @endforeach
                                    <div class="row">{{$members->links()}}</div>
                                </div>
                            </form>
                        </div>
                    @else
                        <p>No Members Found!</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#table').dataTable({
                /* No ordering applied by DataTables during initialisation */
                "order": [],
                "bPaginate": false,
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })

            $("#assignAgent").validate({
                rules: {
                    shop_id: {
                        required: true,
                    }
                }
            });
            $(".assign").click(function (e) {
                e.preventDefault();
                if (!$("#assignAgent").valid()) {
                    return;
                }
                var self = $(this);
                var agent_id = self.attr("agent_id");
                var shop_id = $("#shop_id").val();
                $.ajax({
                    type: "POST",
                    url: '/admin/agent/assign',
                    data: {agent_id: agent_id, shop_id: shop_id},
                    success: function (res) {
                        if (res.response == 'success') {
//                            var html = '<span class="label label-success label-form">Agent: ' + res.company + '<i class="fa fa-check-circle-o"></i></span>&nbsp;';
//                            html += '<a onclick="assignRemove(' + shop_id + ')" id="assignRemove" name="assign"class="btn btn-danger"> <i class="fa fa-trash-o"></i>&nbsp;Remove</a>';
//                            var resetOldAgent = '<button type="submit" id="assign_' + res.oldAgent + '"name="assign" agent_id="' + res.oldAgent + '"class="btn btn-primary assign"> ' +
//                                '<i class="fa fa-chain"></i>&nbsp;Assign</button>';
//
//                            $("#agentAssignSpan_" + res.oldAgent).html('');
//                            $("#agentAssignSpan_" + res.oldAgent).html(resetOldAgent);
//
//                            $("#agentAssignSpan_" + agent_id).html('');
//                            $("#agentAssignSpan_" + agent_id).html(html);
                            swal("Good job!", res.message, "success");
                            window.setTimeout(function () {
                                myRedirect("/admin/agent", res.email);
                            }, 1000);
                        } else {
                            swal("Oh Snap!", "Selected Member is Not an Agent! Create Agent first and Assign.", "error");
                        }
                    }
                });
            });

            //Create an Agent
            $(".makeAgent").click(function () {
                var memberId = $(this).attr('member_id');
                var memberName = $(this).attr('member');
                swal({
                    title: "Assign " + memberName + " as a New Agent of SaleMe.lk",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((value) => {
                    if (value) {
                        $.ajax({
                            type: "POST",
                            url: '/admin/agent/store',
                            data: {id: memberId},
                            success: function (res) {
                                if (res.response == 'success') {
                                    $("#member_" + memberId).toggle();
                                    var html = '<span class="label label-default label-form">Agent &nbsp;<iclass="fa fa-male"></i></span>';
                                    $("#checkIfAgent_" + memberId).toggle(html);
                                    swal(memberName + " Became a Approved Agent!", {
                                        icon: "success",
                                    });
                                } else {
                                    swal("Something went wrong! Please Contact the Administrator.");
                                }
                            }
                        });
                    }
                });
            })
        })

        //remove assigned agent
        function assignRemove($id) {
            $.ajax({
                type: "POST",
                url: '/admin/agent/assign',
                data: {shop_id: $id, status: 'remove'},
                success: function (res) {
                    if (res.response == 'success') {
                        $("#member_" + $id).toggle();
                        swal("Agent removed!", {
                            icon: "success",
                        });
                        window.setTimeout(function () {
                            window.location = "/admin/agent";
                        }, 1000);
                    } else {
                        swal("Something went wrong! Please Contact the Administrator.");
                    }
                }
            });
        }

        var myRedirect = function (redirectUrl, value) {
            var form = $('<form action="' + redirectUrl + '" method="post">' +
                '<input type="hidden" name="_token" value="' + $('meta[name="csrf-token"]').attr('content') + '"></input>' +
                '<input type="hidden" name="email" value="' + value + '"></input>' + '</form>');
            $('body').append(form);
            $(form).submit();
        };
    </script>
    {{--    <script src="{{ asset('backend/assets/js/default/commonajax.js') }}"></script>--}}
@endsection