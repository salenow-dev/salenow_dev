@extends('layouts.app')
@section('title', '| Member Requests for Become Premium')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Manage</strong> | Member Requests for Become Premium</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table datatable table-striped table-bordered" id="table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>company_name</th>
                                <th>address</th>
                                <th>website</th>
                                <th>Phone</th>
                                <th>verified</th>
                                <th>created_at</th>
                                <th>Action</th>
				<th>Send Bank info</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($premiumRequests))
                                @foreach($premiumRequests as $item)
                                    <tr id="{{$item->id}}">
                                        <td>{{$item->member->first_name}} {{$item->member->last_name}}</td>
                                        <td>{{$item->company_name}}</td>
                                        <td>{{$item->address}}</td>
                                        <td>{{$item->website}}</td>
                                        <td>
                                            @if(count($item->telephone))
                                                @foreach($item->telephone as $phone)
                                                    {{$phone}}
                                                @endforeach
                                            @endif
                                            @if(count($item->mobile))
                                                @foreach($item->mobile as $phone)
                                                    {{$phone}}
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>{{$item->verified}}</td>
                                        <td>{{$item->created_at}}</td>
                                        <td>
                                            <form id="{{$item->id}}"
                                                  action="/admin/approvepremiumrequest" method="post">
                                                {{csrf_field()}}
                                                <input type="hidden" name="memberid" value="{{$item->member->id}}">
                                                <button type="submit" class="btn btn-success">
                                                    <span class="badge badge-success">Approve</span>
                                                </button>
                                            </form>
                                            <form id="{{$item->id}}"
                                                  action="/admin/deletepremiumrequest" method="post">
                                                {{csrf_field()}}
                                                <input type="hidden" name="memberid" value="{{$item->member->id}}">
                                                <button type="submit" class="btn btn-danger">
                                                    <span class="badge badge-success">Delete</span>
                                                </button>
                                            </form>
                                        </td>
      					<td>
                                            <form id="{{$item->id}}"
                                                  action="/admin/sendsmspremiummebmer" method="post">
                                                {{csrf_field()}}
                                                <input type="hidden" name="memberid" value="{{$item->member->id}}">
                                                <button type="submit" class="btn btn-success">
                                                    <span class="badge badge-success">Send SMS</span>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        <div class="row">{{$premiumRequests->links('vendor.pagination.custom')}}</div>
                    </div>
                </div>
            </div>
            <a href="javascript:;" onclick="facebookPost()" class="btn btn-success">Post Offer to Facebook</a>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#table').dataTable({
                /* No ordering applied by DataTables during initialisation */
                "order": [],
                "bPaginate": false,
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
        })

        function facebookPost() {
            $.ajax({
                type: "POST",
                url: '/ads-manage/postfacebookoffer',
                data: {},
                success: function (data) {

                }
            });
        }
    </script>
    {{--    <script src="{{ asset('backend/assets/js/default/commonajax.js') }}"></script>--}}
@endsection