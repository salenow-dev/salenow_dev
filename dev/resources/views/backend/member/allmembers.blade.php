@extends('layouts.app')
@section('title', '| Registered Member List')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Manage</strong> | Registered Member List</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table datatable table-striped table-bordered" id="table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>email</th>
                                    <th>member_type</th>
                                    <th>created_at</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($members))
                                    @foreach($members as $item)
                                        <tr id="{{$item->id}}">
                                            <td>{{$item->first_name}} {{$item->last_name}}</td>
                                            <td>{{$item->email}}</td>
                                            <td>{{$item->member_type}}</td>
                                            <td>{{$item->created_at->toFormattedDateString()}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            <div class="row">{{$members->links('vendor.pagination.custom')}}</div>
                        </div>
                    </div>
                </div>
            </form>
            <a href="javascript:;" onclick="facebookPost()" class="btn btn-success">Post Offer to Facebook</a>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#table').dataTable({
                /* No ordering applied by DataTables during initialisation */
                "order": [],
                "bPaginate": false,
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
        })

        function facebookPost() {
            $.ajax({
                type: "POST",
                url: '/ads-manage/postfacebookoffer',
                data: {},
                success: function (data) {

                }
            });
        }
    </script>
    {{--    <script src="{{ asset('backend/assets/js/default/commonajax.js') }}"></script>--}}
@endsection