<html>
<header>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <title>Export Premium Members</title>
    <!------ Include the above in your HEAD tag ---------->

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->
</header>
<body onload="window.print()">
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="invoice-title">
                <h4>Premium Members Details<span class="pull-right"
                                                 style="font-size: 12px;">As at <b>{{\Carbon\Carbon::now()->toFormattedDateString()}}</b>
                        <br>
                        by <b>{{\Illuminate\Support\Facades\Auth::user()->name}}</b></span></h4>
            </div>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed">
                            <thead>
                            <tr>
                                <th width="10">#</th>
                                <th width="33%"><strong>Details</strong></th>
                                <th width="33%"><strong>Contact</strong></th>
                                <th width="32%"><strong>Dates</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $x = 1; ?>
                            @foreach($members as $member)
                                <tr>
                                    <td>{{$x}}</td>
                                    <td>
                                        Name : <b>{{$member->company_name}}</b><br>
                                        Website Title : <b>{{$member->webtitle}}</b><br>
                                        Url : <b>{{asset('')}}{{$member->slug}}</b><br>
                                        Facebook : <b>{{$member->facebook_page}}</b><br>

                                    </td>
                                    <td>
                                        Email : <b>{{$member->email}}</b><br>
                                        Telephone : <b>
                                            @if(count($member->telephone))
                                                @foreach($member->telephone as $tel)
                                                    {{$tel}} /
                                                @endforeach
                                            @else
                                                {{ "----" }}
                                            @endif
                                        </b><br>
                                        Mobile : <b>
                                            @if(count($member->mobile))
                                                @foreach($member->mobile as $tel)
                                                    {{$tel}} /
                                                @endforeach
                                            @else
                                                {{ "----" }}
                                            @endif

                                        </b><br>
                                        Address : <b>{{$member->address}}</b><br>
                                    </td>
                                    <td>
                                        Created On : <b>{{$member->created_at->toFormattedDateString()}}</b><br>
                                        Valid Period : <b>{{$member->period}} Months</b><br>
                                        Expire On : <b>{{$member->created_at->addMonths($member->period)->toFormattedDateString()}}</b><br>
                                        @if($member->created_at->addMonths($member->period)->gt(\Carbon\Carbon::now()))
                                            <span style="color: #229922">Expire Within <b>{{$member->created_at->addMonths($member->period)->diffInDays(\Carbon\Carbon::now())}} Day(s)</b></span>
                                        @else
                                            <span style="color: #ee1e2d">Account has Expired <b>{{\Carbon\Carbon::now()->diffInDays($member->created_at->addMonths($member->period))}} Day(s) ago</b></span>
                                        @endif
                                    </td>
                                </tr>
                                <?php $x++; ?>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>