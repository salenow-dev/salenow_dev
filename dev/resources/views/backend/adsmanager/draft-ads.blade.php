@extends('layouts.app')
@section('title', '| Review Draft Advertisement')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Search Published Ads</h3>
                    <form id="searchAgent" class="form-inline" role="form" method="get"
                          action="/ads-manage/publish-ads/search">
                        <div class="form-group col-md-4">
                            <div class="input-group col-md-12">
                                <input type="text" name="ad_info" value="{{ old('ad_info') }}" class="form-control" placeholder="Ad Title, Ref No, Location or Date">
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="input-group col-md-12">
                                <label class="sr-only">Status</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="confirm"  {{ (old("status") == 'confirm' ? "selected":"") }} selected>Publish</option>
                                    <option value="pending" {{ (old("status") == 'pending' ? "selected":"") }}>Pending</option>
                                    <option value="cancel" {{ (old("status") == 'cancel' ? "selected":"") }}>Cancel</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Search &nbsp;<i
                                    class="fa fa-search"></i></button>
                    </form>
                </div>
            </div>
            <form class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Manage</strong> | All Published Advertisement</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table datatable table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th width="100">Featured</th>
                                    <th width="300">Ad Title</th>
                                    <th>Category</th>
                                    <th>Location</th>
                                    <th>Price</th>
                                    <th width="150">Views</th>
                                    <th>Review</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($allPendingVehicleAds))
                                    @foreach($allPendingVehicleAds as $item)
                                        <tr id="{{$item->id}}">
                                            <td>
                                                @if($item->featuredimage != '')
                                                    <img class="img-responsive pull-left" width="80"
                                                         src="{{asset('')}}salenow/images/uploads/{{$item->id}}/thumb/{{$item->featuredimage}}">
                                                @else
                                                    <img class="img-responsive pull-left" width="80"
                                                         src="{{asset('')}}images/salenow/no_image.jpg">
                                                @endif
                                            </td>
                                            <td>Title: <strong>{{$item->adtitle}}</strong><br>
                                                Ref: <strong>{{$item->ad_referance}}</strong><br>
                                                Created at: <strong>{{$item->created_at}}</strong><br>
                                                Boost: <strong>{{$item->ad_boost_status}}</strong>
                                            </td>
                                            {{--<td>{{$item->ad_referance}}</td>--}}
                                            <td>{{$item->category}}</td>
                                            <td>{{$item->district_name}} > {{$item->city}}</td>
                                            <td>{{number_format($item->price)}}</td>
                                            {{--<td></td>--}}
                                            <td>Views : <strong>{{$item->views}}</strong><br>
                                                Clicks : <strong>{{$item->clicks}}</strong></td>
                                            <td>
                                                <a href="/ads-manage/ad-review/{{$item->id}}/edit"
                                                   class="edit unline btn btn-xs btn-primary" style="color: #fff"><span class="lnr lnr-pencil"></span> EDIT</a>
                                                {{--<a href="/ads-manage/ad-review/{{$item->id}}" class="edit unline"><span--}}
                                                {{--class="lnr lnr-eye"></span></a>--}}
                                                <a type="button" class="btn btn-xs btn-success review_status"
                                                   ad-id="{{$item->id}}" id="publish" status="confirm">
                                                    <span class="lnr lnr-rocket"></span> &nbsp; Re-Publish
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            <div class="row">{{$allPendingVehicleAds->links('vendor.pagination.custom-backend')}}</div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#table').dataTable({
                /* No ordering applied by DataTables during initialisation */
                "order": [],
                "bPaginate": false,
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // $('#btn_submit_signup').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
            $('#loading').hide();

            $(".review_status").click(function (e) {
                e.preventDefault();
                $.blockUI();
                var status = $(this).attr('status');
                var id = $(this).attr('ad-id');
                var data = {'id': id, 'status': status};
                $.ajax({
                    type: "PATCH",
                    url: '/ads-manage/ad-review/' + id,
                    data: data,
                    success: function (res) {
                        $.unblockUI();
                        $('#loading').hide();
                        if (typeof res.message != 'undefined' && res.url != '') {
                            $('#error-div').remove();
                            noty({
                                text: res.message,
                                layout: 'topRight',
                                type: 'success',
                                timeout: 3000,
                            });
                        }
                    }
                });
            });
        })
    </script>
@endsection