@extends('layouts.app')
@section('title', '| Review Pending Advertisement')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Review
                            <strong>
                                @if($allinfo->status == 'confirm')
                                    Published
                                @elseif($allinfo->status == 'pending')
                                    Pending
                                @elseif($allinfo->status == 'cancel')
                                    Spam
                                @else
                                    Blocked
                                @endif
                            </strong>
                            Advertisement
                        </h3>
                        @role('superadmin')
                        <form id="login_form" action="" method="post" role="form">
                            <a name="login_superadmin" username="{{$allinfo->email}}"
                               password="salemeqwe123@#2017"
                               class="btn btn-md btn-primary login_superadmin pull-right"><i
                                        class="fa fa-cutlery"></i>&nbsp; Login to Profile</a>
                        </form>
                        @endrole
                    </div>
                </div>
                <div class="col-md-12">
                    @include('errors.flash')
                    @include('errors.list')
                    <div class="alert alert-danger hide" id="error-div" role="alert">
                        <i class="fa fa-exclamation-triangle"></i>&nbsp;
                        <span class="error-title">Alert! The following errors were found.</span>
                        <hr>
                        <div id="error-message"></div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel ">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h2 class="">{{$allinfo->adtitle}} - <span class="pricetag">
                                        <span class="currency">Rs</span> {{number_format($allinfo->price)}}</span>
                                </h2>

                                <ul class="devider-list vap-chicklet1">
                                    <li>
                                        <i class="ion-ios-stopwatch-outline ico2"></i>
                                        {{$allinfo->created_at->formatLocalized('%A %d %B %Y') }}
                                        (<strong>{{$allinfo->updated_at->diffForHumans()}}</strong>)
                                    </li>
                                    <li><strong> <i class="lnr lnr-pushpin ico2"></i> Ad ID :
                                        </strong>{{$allinfo->adid}}
                                    </li>
                                    <li><strong> <i class="lnr lnr-pushpin ico2"></i> Referance :
                                        </strong>{{$allinfo->ad_referance}}
                                    </li>
                                    <li><strong> Status : </strong>
                                        <span class="badge badge-{{$allinfo->status == 'confirm'?'success':'danger'}}">
                                            {{$allinfo->status}}
                                        </span>
                                        <span class="badge badge-primary">
                                            @if(!empty($allinfo->published_at))
                                                updated
                                            @else
                                                new
                                            @endif
                                        </span>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right review-btns">
                                <form id="ad-review" action="" role="form">
                                    {{method_field('PATCH')}}
                                    <img src="{{asset('')}}/images/loadingnew.gif" id="loading" width="20">
                                    <div class="blockui">
                                        @if($allinfo->status != 'confirm')
                                            <a type="button" class="btn btn-lg btn-success review_status"
                                               ad-id="{{$allinfo->adid}}" id="publish" status="confirm">
                                                <span class="lnr lnr-rocket"></span> &nbsp; Publish
                                            </a>
                                        @endif
                                        @if($allinfo->status != 'blocked' && $allinfo->status != 'cancel')
                                            <a type="button" class="btn btn-lg btn-primary review_status"
                                               ad-id="{{$allinfo->adid}}" id="blocked" status="blocked">
                                                <span class="lnr lnr-warning"></span> &nbsp; Block
                                            </a>
                                        @endif
                                        @role('superadmin')
                                        @if($allinfo->status != 'pending' AND $allinfo->status != 'blocked')
                                            <a type="button" class="btn btn-lg btn-danger review_status"
                                               ad-id="{{$allinfo->adid}}"
                                               status="pending">
                                                <span class="lnr lnr-sad"></span>&nbsp; Pending
                                            </a>
                                        @endif
                                        @endrole
                                        @if($allinfo->status != 'cancel')
                                            <div class="btn-group open">
                                                <a type="button" class="btn btn-lg btn-danger"
                                                   ad-id="{{$allinfo->adid}}" id="spam" data-toggle="modal"
                                                   data-target="#modal_no_head" status="spam">
                                                    <span class="lnr lnr-sad"></span>&nbsp; Spam
                                                </a>
                                            </div>
                                        @endif
                                        @if($allinfo->status == 'cancel' OR $allinfo->status == 'blocked')
                                            <a type="button" class="btn btn-lg btn-danger" ad-id="{{$allinfo->adid}}"
                                               id="deletae" status="delete"
                                               onclick="notyConfirmBackend({{$allinfo->adid}},'{{$allinfo->cat_code}}')">
                                                <span class="lnr lnr-trash"></span>&nbsp; Delete
                                            </a>
                                        @endif
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        @if(!empty($db_spamreasons))
                            <fieldset class="custom-fieldset">
                                <legend>
                                    <i class="lnr lnr-pencil ico2"></i> Spam Reasons
                                </legend>
                                @foreach($db_spamreasons as $spam)
                                    <li class="badge badge-primary">{{$spam->reason}}</li>
                                @endforeach
                            </fieldset>
                        @endif
                        <div class="panel panel-default">
                            <div class="panel-heading user-dtl">
                                <div class="custom-fieldset">
                                    <span class="lnr lnr-user ico3"></span>
                                    &nbsp; {{$allinfo->first_name}} {{$allinfo->last_name}}<br>
                                    <small class="user-veri-txt"> Verified User <i
                                                class="ion-ios-checkmark verified"></i>
                                    </small>
                                    <br>
                                    @if(count($member_contacts))
                                        @foreach($member_contacts as $member_contact)
                                            <span class="badge badge-primary">{{$member_contact->contactnumber}}</span>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="panel-body list-group">
                                <a href="#" class="list-group-item"><span class="fa fa-inbox"></span>Contact Number
                                    @if(!empty($allinfo->contact))
                                        @foreach($allinfo->contact as $number)
                                            <span class="badge badge-primary">{{$number}}</span>
                                        @endforeach
                                    @endif
                                </a>
                                <a href="#" class="list-group-item"><span class="fa fa-envelope-o"></span> Sent Mail
                                    <span class="badge badge-success">{{$allinfo->email}}</span></a>
                                <a href="#" class="list-group-item"><span class="fa fa-envelope-o"></span> Member Type
                                    <span class="badge badge-success">{{$allinfo->member_type}}</span></a>
                            </div>
                            <div class="panel-body">
                                <div class="gallery" id="links">
                                    @foreach($images as $image)
                                        <a class="gallery-item"
                                           href="{{ url('/salenow/images/uploads/'.$allinfo->adid.'/'.$image->imagename.'') }}"
                                           title="{{$image->imagename}}" data-gallery="">
                                            <div class="image">
                                                <img src="{{ url('/salenow/images/uploads/'.$allinfo->adid.'/thumb/'.$image->imagename.'') }}">
                                            </div>
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="col-sm-8">
                                    @if(count($duplicateAds))
                                        {{--{{dd($duplicateAds)}}--}}
                                        <div style="background: #ffe9e7">
                                            <h6 style="padding: 10px 10px 0px; color: #ff0000">Duplicated Ads</h6>
                                            <p style="color: #ff0000; padding: 0px 10px;">System found
                                                <strong>{{count($duplicateAds)}}</strong> ad(s) related this
                                                information. <br>
                                                Please review below ads before posting this ad</p>
                                            <ul class="list-group border-bottom">
                                                @foreach($duplicateAds as $duplicateAd)
                                                    <li class="list-group-item">
                                                        <a
                                                                @if($duplicateAd->status ==='confirm')
                                                                href="/ad/{{$duplicateAd->slug}}"
                                                                @else
                                                                href="/ads-manage/ad-review/{{$duplicateAd->id}}/edit"
                                                                @endif

                                                                target="_blank">{{$duplicateAd->adtitle}} <span
                                                                    class="badge badge-primary pull-right">{{$duplicateAd->status}}</span></a>
                                                        <p>Slug : {{$duplicateAd->slug}}</p>
                                                        <p>Description
                                                            : {!! str_limit($duplicateAd->description,100) !!}</p>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <br>
                                    @endif
                                    <ul class="list-group border-bottom">
                                        <li class="list-group-item">Ad Type <span
                                                    class="badge"> {!!$allinfo->ad_type  !!}</span></li>
                                        <li class="list-group-item">Category <span class="badge">{!!$allinfo->category !!}
                                                > {!!$allinfo->subcategory  !!}</span></li>
                                        @if($allinfo->itemtype)
                                            <li class="list-group-item">Type <span
                                                        class="badge"> {!! $allinfo->itemtype !!}</span></li>
                                        @endif
                                        @if($allinfo->itemcondition)
                                            <li class="list-group-item">Condition<span
                                                        class="badge"> {!! $allinfo->itemcondition !!}</span></li>
                                        @endif
                                        <li class="list-group-item">Location <span class="badge">{!!$allinfo->district  !!}
                                                > {!!$allinfo->city  !!}</span></li>
                                    </ul>
                                    <fieldset class="custom-fieldset">
                                        <legend>
                                            <i class="lnr lnr-pencil ico2"></i> Description
                                        </legend>
                                        @if($allinfo->status == "cancel")
                                            <div class="">
                                                <form action="{{asset('')}}ads-manage/spam-ad/details-edit/save">
                                                    <input type="hidden" name="id" value="{{$allinfo->adid}}">
                                                    {{csrf_field()}}
                                                    <textarea name="description" id="" cols="30" rows="10" class="form-control">{!!$allinfo->description  !!}</textarea>
                                                    <br>
                                                    <input type="submit" class="btn btn-primary pull-right" value="save">
                                                </form>

                                            </div>
                                            @else
                                            <div class="">
                                                {!!$allinfo->description  !!}
                                            </div>
                                            @endif

                                    </fieldset>
                                </div>
                                <div class="col-sm-4">
                                    @if(!empty($primaryDetails))
                                        <fieldset class="custom-fieldset">
                                            <legend>
                                                <i class="ion-ios-speedometer-outline ico2"></i> Primary Details
                                            </legend>
                                            <ul class="car-details-list">
                                                @foreach($primaryDetails as $key => $primaryDetail)
                                                    @if(!empty($primaryDetail))
                                                        <li>
                                                            <span class="details-left-lbl pull-left">{{$key}}</span>
                                                            <span class="details-right-lbl pull-right text-right">{{$primaryDetail}}
                                                        </span>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </fieldset>
                                    @endif
                                    @if(!empty($features))
                                        <fieldset class="custom-fieldset">
                                            <legend>
                                                <i class="ion-ios-star-outline ico2"></i> Value Added Features
                                            </legend>
                                            <ul class="car-details-list">
                                                @foreach($features as $key=>$feature)
                                                    <li>
                                                        <span class="col-xs-8">{{$feature}}</span>
                                                        <span class="col-xs-4"><i
                                                                    class="ion-ios-checkmark-outline ico"></i></span>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </fieldset>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                    {{--@if(count($similarAds))--}}
                        {{--<div class="col-md-12">--}}
                            {{--START similar ads--}}
                            {{--<h5>Similar Ads</h5>--}}
                            {{--<ul class="list-group border-bottom">--}}

                                {{--@foreach($similarAds as $similarAd)--}}
                                    {{--<li class="list-group-item">--}}
                                        {{--<a href="/ads-manage/ad-review/{{$similarAd->id}}/edit">{{$similarAd->adtitle}}</a>--}}
                                        {{--<p>{{$similarAd->slug}}</p>--}}
                                        {{--<p>{!! str_limit($similarAd->description,100) !!}</p>--}}
                                    {{--</li>--}}
                                {{--@endforeach--}}
                            {{--</ul>--}}
                            {{--<br>--}}

                            {{--END similar ads--}}
                        {{--</div>--}}
                    {{--@endif--}}
                </div>
            </form>
        </div>
    </div>
    <!-- BLUEIMP GALLERY -->
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
    <div class="modal" id="modal_no_head" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <h3>Check the Spam Reason ?</h3>
                    <div id="message" class="alert" role="alert">
                    </div>
                    <form id="form_spam" role="form" class="form-material">
                        <input type="hidden" name="status" id="status" value="spam">
                        <input type="hidden" name="ad_id" id="ad_id" value="{{$allinfo->adid}}">
                        <div class="form-group">
                            @foreach($spamreasons as $spam)
                                <div class="checkbox-material">
                                    <input type="checkbox" name="spamreason_id[]" id="{{$spam->id}}"
                                           value="{{$spam->id}}">
                                    <label for="{{$spam->id}}">{{$spam->reason}}</label>
                                </div>
                            @endforeach
                        </div>
                        <input type="button" class="btn btn-md btn-block btn-danger spam" value="Send to spam">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        document.getElementById('links').onclick = function (event) {
            event = event || window.event;
            var target = event.target || event.srcElement;
            var link = target.src ? target.parentNode : target;
            var options = {
                index: link, event: event, onclosed: function () {
                    setTimeout(function () {
                        $("body").css("overflow", "");
                    }, 200);
                }
            };
            var links = this.getElementsByTagName('a');
            blueimp.Gallery(links, options);
        };
    </script>
    <!-- END BLUEIMP GALLERY -->
    <script type="text/javascript" src="{{ asset("backend/assets/js/post-ad/review-ad.js") }}"></script>
@endsection
