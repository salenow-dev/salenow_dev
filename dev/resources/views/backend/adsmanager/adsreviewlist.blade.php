@extends('layouts.app')
@section('title', '| Review Pending Advertisement')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Manage</strong> | Review Pending Advertisement</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table datatable table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Ad Title</th>
                                    <th>Category</th>
                                    <th>Location</th>
                                    <th>Price</th>
                                    <th>Created at</th>
                                    <th>Status</th>
                                    <th>Review</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($allPendingVehicleAds))
                                    @foreach($allPendingVehicleAds as $item)
                                        <tr id="{{$item->id}}">
                                            <td>{{$item->adtitle}}</td>
                                            <td>{{$item->category}}</td>
                                            <td>{{$item->district_name}} > {{$item->city}}</td>
                                            <td>{{$item->price}}</td>
                                            <td>{{$item->created_at}}</td>
                                            <td>
                                                {{$item->status}}
                                            </td>
                                            <td>
                                                <a href="/settings/adtypes/{{$item->id}}/edit" class="edit unline"><span class="lnr lnr-pencil"></span></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--    <script src="{{ asset('backend/assets/js/default/commonajax.js') }}"></script>--}}
@endsection