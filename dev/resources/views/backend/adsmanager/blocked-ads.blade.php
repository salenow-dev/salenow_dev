@extends('layouts.app')
@section('title', '| Review Spam Advertisement')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Manage</strong> | All Blocked Advertisement</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table datatable table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th width="100">Featured</th>
                                    <th width="300">Ad Title</th>
                                    <th>Ref</th>
                                    <th>Category</th>
                                    <th>Location</th>
                                    <th>Price</th>
                                    <th>Cancled at</th>
                                    <th>Review</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($allAds))
                                    @foreach($allAds as $item)
                                        <tr id="{{$item->id}}">
                                            <td>
                                                @if($item->featuredimage != '')
                                                    <img class="img-responsive pull-left" width="80"
                                                         src="{{asset('')}}salenow/images/uploads/{{$item->id}}/thumb/{{$item->featuredimage}}">
                                                @else
                                                    <img class="img-responsive pull-left" width="80"
                                                         src="{{asset('')}}images/salenow/no_image.jpg">
                                                @endif
                                            </td>
                                            <td>{{$item->adtitle}}</td>
                                            <td>{{$item->ad_referance}}</td>
                                            <td>{{$item->category}}</td>
                                            <td>{{$item->district_name}} > {{$item->city}}</td>
                                            <td>{{number_format($item->price)}}</td>
                                            <td>{{  date("d M Y", strtotime($item->updated_at)) }}</td>
                                            <td>
                                                <a class="btn btn-primary btn-condensed" href="/ads-manage/ad-review/{{$item->id}}/edit"><i class="fa fa-edit"></i>Edit</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--    <script src="{{ asset('backend/assets/js/default/commonajax.js') }}"></script>--}}
@endsection