@extends('layouts.app')
@section('title', '| Review Pending Advertisement')
@section('content')
    <div class="row">
        <div class="col-md-12">
            {{--<div class="form-group">--}}
            {{--<a type="button" class="btn btn-primary" href="/ads-manage/ad-review/blocked-ads">Blocked Ads</a>--}}
            {{--<a type="button" class="btn btn-primary" href="/ads-manage/ad-review/pending-otp">--}}
            {{--Pending OTP &nbsp;--}}
            {{--<span class="label label-danger">{{$todayOtpCount}}</span>--}}
            {{--</a>--}}
            {{--</div>--}}
            <form class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Manage</strong> | Review Pending Advertisement</h3>
                        <a href="{{asset('')}}ads-manage/delete-all-deleted-ads-with-images">

                            <button class="btn btn-danger btn-sm pull-right">Permenantly Delete 500 ads from Deleted
                                ads
                            </button>
                        </a>


                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table datatable table-striped table-bordered" id="table">
                                <thead>
                                <tr>
                                    <th width="100">Featured</th>
                                    <th width="300">Ad Title</th>
                                    <th width="200">Category</th>
                                    <th width="200">Ref</th>
                                    <th>Type</th>
                                    <th>Location</th>
                                    <th>Price</th>
                                    <th>Created at</th>
                                    <th>Review</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($allPendingVehicleAds))
                                    @foreach($allPendingVehicleAds as $item)
                                        <tr id="{{$item->id}}">
                                            <td>
                                                @if($item->featuredimage!='')
                                                    <img class="img-responsive pull-left" width="80"
                                                         src="{{asset('')}}salenow/images/uploads/{{$item->id}}/thumb/{{$item->featuredimage}}">
                                                @else
                                                    <img class="img-responsive pull-left" width="80"
                                                         src="{{asset('')}}images/salenow/no_image.jpg">
                                                @endif
                                            </td>
                                            <td class="collapsable">{{$item->adtitle}}</td>
                                            <td class="collapsable">{{$item->category}}</td>
                                            <td>{{$item->ad_referance}}</td>
                                            <td>{{$item->ad_type}}</td>
                                            <td>{{$item->district_name}} > {{$item->city}}</td>
                                            <td>{{$item->price}}</td>
                                            <td>{{$item->created_at}}</td>
                                            <td>
                                                <a href="/ads-manage/ad-review/{{$item->id}}/edit"
                                                   class="edit unline btn btn-xs btn-primary" style="color: #fff"><span
                                                            class="lnr lnr-pencil"></span> EDIT</a>
                                                {{--<a href="/ads-manage/ad-review/{{$item->id}}" class="edit unline"><span--}}
                                                {{--class="lnr lnr-eye"></span></a>--}}
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
            <a href="javascript:;" onclick="facebookPost()" class="btn btn-success">Post Offer to Facebook</a>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#table').dataTable({
                /* No ordering applied by DataTables during initialisation */
                "order": []
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
        })

        function facebookPost() {
            $.ajax({
                type: "POST",
                url: '/ads-manage/postfacebookoffer',
                data: {},
                success: function (data) {

                }
            });
        }
    </script>
    {{--    <script src="{{ asset('backend/assets/js/default/commonajax.js') }}"></script>--}}
@endsection