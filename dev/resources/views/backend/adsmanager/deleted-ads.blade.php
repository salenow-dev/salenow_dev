@extends('layouts.app')
@section('title', '| Review Spam Advertisement')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Manage</strong> | All Deleted Advertisement</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table datatable table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th width="100">Featured</th>
                                    <th width="300">Ad Title</th>
                                    <th>Delete Reason</th>
                                    <th>Review</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($allAds))
                                    @foreach($allAds as $item)
                                        <tr id="{{$item->id}}">
                                            <td>
                                                @if($item->featuredimage != '')
                                                    <img class="img-responsive pull-left" width="80"
                                                         src="{{asset('')}}salenow/images/uploads/{{$item->id}}/thumb/{{$item->featuredimage}}">
                                                @else
                                                    <img class="img-responsive pull-left" width="80"
                                                         src="{{asset('')}}images/salenow/no_image.jpg">
                                                @endif
                                            </td>
                                            <td>Title : <strong>{{$item->adtitle}}</strong><br>
                                                Ref : <strong>{{$item->ad_referance}}</strong><br>
                                                Price : <strong>{{number_format($item->price)}}</strong><br>
                                                Category : <strong>{{$item->category}}</strong><br>
                                                Location : <strong>{{$item->district_name}}
                                                    > {{$item->city}}</strong><br>
                                                Deleted at : <strong>{{  date("d M Y", strtotime($item->updated_at)) }}</strong>
                                            </td>
                                            <td>{{$item->del_reason}}</td>
                                            <td>
                                                <a class="btn btn-primary btn-condensed"
                                                   href="/ads-manage/ad-review/{{$item->id}}/edit"><i
                                                            class="fa fa-edit"></i>Edit</a>
                                                <a class="btn btn-success"
                                                   href="#" onclick="notyRepublishBackend({{$item->id}})"><i
                                                            class="fa fa-rocket"></i>Republish</a>
                                                <a class="btn btn-danger"
                                                   href="#" onclick="notyConfirmBackend({{$item->id}},'{{$item->cat_code}}')"><i
                                                            class="fa fa-trash-o"></i>Delete Permanatly</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
<<<<<<< HEAD
                                <div class="row">{{$allAds->links('vendor.pagination.custom-backend')}}</div>
=======
<div class="row">{{$allAds->links('vendor.pagination.custom-backend')}}</div>
>>>>>>> 44c017f80180aa9001b9c3ace4c8c75a2a4f68cf
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset("backend/assets/js/post-ad/delete-ad.js") }}"></script>

@endsection