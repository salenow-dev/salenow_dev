@extends('layouts.app')
@section('title', '| Pending OTP')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <a type="button" class="btn btn-primary" href="/ads-manage/ad-review">Pending Ads</a>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Manage</strong> | Review Pending OTP</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive hidden-xs">
                        <table class=" table datatable table-striped table-bordered" id="table">
                            <thead>
                            <tr>
                                <th width="50">ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Number</th>
                                <th>PIN</th>
                                <th>Count</th>
                                <th>Created at</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($otpNumbers))
                                @foreach($otpNumbers as $item)
                                    <tr id="{{(!empty($item->member->id))?$item->member->id:''}}">
                                        <td>{{(!empty($item->member->id))?$item->member->id:''}}</td>
                                        <td>{{(!empty($item->member->first_name) && !empty($item->member->last_name))?$item->member->first_name.' '. $item->member->last_name:''}}</td>
                                        <td>{{(!empty($item->member->email))?$item->member->email:''}}</td>
                                        <td>{{$item->number}}</td>
                                        <td>{{$item->code}}</td>
                                        <td>{{$item->count}}</td>
                                        <td>{{$item->created_at}}</td>
                                        <td>
                                            <form id="otpConfirm" class="form-inline" role="form" method="post"
                                                  action="/ads-manage/ad-review/confirmotp">
                                                {!! Form::token() !!}
                                                <input type="hidden" name="otpId" value="{{$item->id}}">
                                                <input type="submit" value="Confirm" class="btn btn-primary btn-xs">
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        <div class="row">{{$otpNumbers->links('vendor.pagination.custom-backend')}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#table').dataTable({
                /* No ordering applied by DataTables during initialisation */
                "order": []
            });
//            $.ajaxSetup({
//                headers: {
//                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                }
//            })
        })

//        function facebookPost() {
//            $.ajax({
//                type: "POST",
//                url: '/ads-manage/postfacebookoffer',
//                data: {},
//                success: function (data) {
//
//                }
//            });
//        }
    </script>
    {{--    <script src="{{ asset('backend/assets/js/default/commonajax.js') }}"></script>--}}
@endsection