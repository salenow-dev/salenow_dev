@extends('layouts.app')
@section('title', '| Review Pin-to-Top List')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Manage</strong> | Boost Ads - Pin To Top</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table datatable table-striped table-bordered" id="table">
                            <thead>
                            <tr>
                                <th>Ad Title</th>
                                <th width="300">Member</th>
                                <th>Boost Type</th>
                                <th>Accepted On</th>
                                <th width="160">Expire On</th>
                                <th width="260">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            {{dd($pintotopList)}}
                            @if(!empty($pintotopList))
                                @foreach($pintotopList as $rqst)
                                    <?php
                                    $expire = new \Carbon\Carbon($rqst->allads->boost_expire_on);
                                    $now = \Carbon\Carbon::now();
                                    $difference = '';
                                    if ($expire < $now) {
                                        $difference = ($expire->diffInHours($now) > 0) ? $expire->diffForHumans($now) : '0';
                                    }
                                    ?>
                                    <tr id="{{$rqst->id}}">
                                        <td>
                                            <a href="{{asset('')}}ad/{{$rqst->allads->slug}}">{{$rqst->allads->adtitle}}</a>
                                        </td>
                                        <td>{{$rqst->allads->member->first_name.' '.$rqst->allads->member->last_name}}
                                            <span class="badge badge-primary">{{$rqst->allads->member->member_type}}</span>
                                        </td>
                                        <td>{{$rqst->allads->ad_boost_status}}</td>
                                        <td>{{ date("d M Y", strtotime($rqst->created_at)) }}</td>
                                        <td>{{date("d M Y", strtotime($rqst->allads->boost_expire_on))}}
                                            @if($difference)
                                                <span class="badge badge-warning">{{$difference}}</span>
                                            @else
                                                <span class="badge badge-info">valid</span>
                                            @endif
                                        </td>
                                        <td>
                                            <form id="delete{{$rqst->id}}"
                                                  action="/ads-manage/boost/pintotop/remove/{{$rqst->id}}"
                                                  method="post">
                                                {{csrf_field()}}
                                                <button type="submit" name="remove" class="btn btn-warning" value="remove">
                                                    Remove
                                                </button>
                                                @if($difference)
                                                    <button type="submit" name="reactive" class="btn btn-success" value="reactive">
                                                        Re-Active
                                                    </button>
                                                @endif
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Manage</strong> | Removed Boost Ads</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table datatable table-striped table-bordered" id="table">
                            <thead>
                            <tr>
                                <th>Ad Title</th>
                                <th width="300">Member</th>
                                <th>Boost Type</th>
                                <th width="160">Expired On</th>
                                <th>Removed On</th>
                                <th width="180">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($pintotopListDeleted))
                                @foreach($pintotopListDeleted as $rqst)
                                    <tr id="{{$rqst->id}}">
                                        <td>
                                            <a href="{{asset('')}}ad/{{$rqst->allads->slug}}">{{$rqst->allads->adtitle}}</a>
                                        </td>
                                        <td>{{$rqst->allads->member->first_name.' '.$rqst->allads->member->last_name}}
                                            <span class="badge badge-primary">{{$rqst->allads->member->member_type}}</span>
                                        </td>
                                        <td>{{$rqst->allads->ad_boost_status}}</td>
                                        <td>{{date("d M Y", strtotime($rqst->allads->boost_expire_on))}}</td>
                                        <td>{{ date("d M Y", strtotime($rqst->updated_at)) }}</td>
                                        <td>
                                            <form id="delete{{$rqst->id}}"
                                                  action="/ads-manage/boost/pintotop/remove/{{$rqst->id}}"
                                                  method="post">
                                                {{csrf_field()}}
                                                <button type="submit" name="delete" class="btn btn-danger">
                                                    Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--    <script src="{{ asset('backend/assets/js/default/commonajax.js') }}"></script>--}}
    <script>
        $(document).ready(function () {
            $('#table').dataTable({
                /* No ordering applied by DataTables during initialisation */
                "order": []
            });
        })
    </script>
@endsection