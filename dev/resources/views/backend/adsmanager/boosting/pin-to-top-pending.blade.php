@extends('layouts.app')
@section('title', '| Review Pin-to-Top Pending Request')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <a type="button" class="btn btn-primary" href="/ads-manage/boost/pintotop">Boost Ads</a>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Manage</strong> | Boost Pending Requests - Ad Packs</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table datatable table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Ads Pack</th>
                                <th width="230">Invoice</th>
                                <th>Price</th>
                                <th>Member</th>
                                <th>Boost Type</th>
                                <th>Requested</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($adPackRequests))
                                @foreach($adPackRequests as $rqst)
                                    <tr id="{{$rqst->id}}">
                                        <td>{{$rqst->boostpackname}}</td>
                                        <td>{{$rqst->boostpackinvoiceno}}</td>
                                        <td>{{number_format($rqst->price)}}</td>
                                        <td>{{$rqst->first_name.' '.$rqst->last_name}}
                                            <span class="badge badge-primary">{{$rqst->member_type}}</span>
                                        </td>
                                        <td>{{$rqst->boosttype}}</td>
                                        <td>{{ date("d M Y", strtotime($rqst->created_at)) }}</td>
                                        <td>{{$rqst->status}}</td>
                                        <td>
                                            <form id="{{$rqst->boostpackinvoiceno}}"
                                                  action="/ads-manage/activate/pin-to-top" method="post">
                                                {{csrf_field()}}
                                                <input type="hidden" name="boostpackinvoiceno"
                                                       value="{{$rqst->boostpackinvoiceno}}">
                                                <input type="hidden" name="member_id" value="{{$rqst->member_id}}">
                                                <input type="hidden" name="boosttype" value="{{$rqst->boosttype}}">
                                                <button type="submit" class="btn btn-success">
                                                    <span class="badge badge-success">Approve</span>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Manage</strong> | Boost Pending Requests - Single Ads</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table datatable table-striped table-bordered" id="table">
                            <thead>
                            <tr>
                                <th>Ads Pack</th>
                                <th width="230">Invoice</th>
                                <th>Price</th>
                                <th>Member</th>
                                <th>Boost Type</th>
                                <th>Requested</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($adPackRequestsSingle))
                                @foreach($adPackRequestsSingle as $rqst)
                                    <tr id="{{$rqst->id}}">
                                        <td>
                                            <a href="{{asset('')}}ad/{{$rqst->slug}}">{{$rqst->adtitle}}</a>
                                        </td>
                                        <td>{{$rqst->boostpackinvoiceno}}</td>
                                        <td>{{number_format($rqst->price)}}</td>
                                        <td>{{$rqst->first_name.' '.$rqst->last_name}}
                                            <span class="badge badge-primary">{{$rqst->member_type}}</span>
                                        </td>
                                        <td>{{$rqst->boosttype}}</td>
                                        <td>{{ date("d M Y", strtotime($rqst->created_at)) }}</td>
                                        <td>{{$rqst->status}}</td>
                                        <td>
                                            <form id="{{$rqst->boostpackinvoiceno}}"
                                                  action="/ads-manage/activate/{{$rqst->boosttype}}/{{$rqst->vouchercode_id}}" method="post">
                                                {{csrf_field()}}
                                                <input type="hidden" name="ad_referance" value="{{$rqst->ad_referance}}">
                                                <input type="hidden" name="adid" value="{{$rqst->adid}}">
                                                <input type="hidden" name="boostpackinvoiceno"
                                                       value="{{$rqst->boostpackinvoiceno}}">
                                                <input type="hidden" name="member_id" value="{{$rqst->member_id}}">
                                                <input type="hidden" name="boosttype" value="{{$rqst->boosttype}}">
                                                <input type="hidden" name="vouchercode_id" value="{{$rqst->vouchercode_id}}">
                                                <button type="submit" class="btn btn-success">
                                                    <span class="badge badge-success">Approve</span>
                                                </button>
                                            </form>
{{--                                            {{dd($rqst)}}--}}
                                            <form id="delete{{$rqst->boostpackinvoiceno}}"
                                                  action="/ads-manage/delete/{{$rqst->boosttype}}/{{$rqst->id}}"  method="post">
                                                {{csrf_field()}}
                                                <input type="hidden" name="boostpackinvoiceno"
                                                       value="{{$rqst->boostpackinvoiceno}}">
                                                <input type="hidden" name="member_id" value="{{$rqst->member_id}}">
                                                <input type="hidden" name="boosttype" value="{{$rqst->boosttype}}">
                                                <button type="submit" class="btn btn-danger">
                                                    <span class="badge badge-success">Delete</span>
                                                </button>
                                            </form>
                                        </td>
     					<td>
                                            <form id="{{$rqst->member_id}}"
                                                  action="/ads-manage/sendpintopsms" method="post">
                                                {{csrf_field()}}
                                                  
                      				<input type="hidden" name="member_type" value="{{$rqst->member_type}}">
                                                <input type="hidden" name="member_id" value="{{$rqst->member_id}}">
                                                <button type="submit" class="btn btn-success">
                                                    <span class="badge badge-success">SMS</span>
                                                </button>
                                            </form>
                                        </td>

                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--    <script src="{{ asset('backend/assets/js/default/commonajax.js') }}"></script>--}}
    <script>
        $(document).ready(function () {
            $('#table').dataTable({
                /* No ordering applied by DataTables during initialisation */
                "order": []
            });
        })
    </script>
@endsection