@extends('layouts.app')
@section('title', '| Pending OTP')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Manage</strong> | Assign Ads Packs</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class=" table datatable table-striped table-bordered" id="table">
                            <thead>
                            <tr>
                                <th width="200">ID</th>
                                <th>Details</th>
                                <th>Phones</th>
                                <th>Period</th>
                                <th>Pin To Tops</th>
                                <th></th>
                                {{--<th>Count</th>--}}
                                {{--<th>Created at</th>--}}
                                {{--<th>Action</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            {{--{{dd($member_premiums)}}--}}
                            @if(!empty($member_premiums))
                                @foreach($member_premiums as $item)
                                    {{--{{dd($item)}}--}}
                                    <tr id="{{(!empty($item->member_id))?$item->member_id:''}}">
                                        <td>
                                            Member ID : <b>{{(!empty($item->member_id))?$item->member_id:''}}</b><br>
                                            Premium Member ID : <b>{{(!empty($item->id))?$item->id:''}}</b>
                                        </td>
                                        <td>
                                            Website : <b>{{(!empty($item->webtitle))?$item->webtitle:'-'}}</b><br>
                                            Company :
                                            <b>{{(!empty($item->company_name))?$item->company_name:'-'}}</b><br>
                                            Slug : <b>{{(!empty($item->slug))?$item->slug:''}}</b><br>
                                        </td>
                                        <td>
                                            {{--Telephone : {{(!empty($item->telephone))?$item->telephone:''}}<br>--}}
                                            {{--Mobile : {{(!empty($item->mobile))?$item->mobile:''}}<br>--}}
                                        </td>
                                        <td>
                                            {{(!empty($item->period))?$item->period.' Months':'-'}}
                                        </td>
                                        <td>
                                            @if(!empty($item->period))
                                                @if ($item->period == 3)
                                                    {{$item->period.' Pin to Tops'}}
                                                @elseif ($item->period == 6)
                                                    {{'5 Pin to Tops'}}
                                                @else
                                                    {{'10 Pin to Tops'}}
                                                @endif
                                            @else
                                                {{'-'}}
                                            @endif
                                        </td>
                                        <td>
                                            @if(!$item->pintotopactivated)
                                                <form id="otpConfirm" class="form-inline" role="form" method="post"
                                                      action="/ads-manage/assign-ads-pack/assign">
                                                    {!! Form::token() !!}
                                                    <input type="hidden" name="otpId" value="{{$item->id}}">
                                                    <select name="period" id="period" class="form-control" required>
                                                        <option value="">Select Period</option>
                                                        <option value="3">3 Months</option>
                                                        <option value="6">6 Months</option>
                                                        <option value="12">12 Months</option>
                                                    </select>
                                                    <input type="hidden" name="ads_pack" value="true">
                                                    <input type="hidden" name="member_id" value="{{$item->member_id}}">
                                                    <input type="hidden" name="premium_id" value="{{$item->id}}">
                                                    <input type="submit" value="Confirm" class="btn btn-primary btn-xs">
                                                </form>
                                            @else
                                                <p>already offered</p>
                                            @endif
                                        </td>
                                        {{--<td>{{(!empty($item->member->first_name) && !empty($item->member->last_name))?$item->member->first_name.' '. $item->member->last_name:''}}</td>--}}
                                        {{--<td>{{(!empty($item->member->email))?$item->member->email:''}}</td>--}}
                                        {{--<td>{{$item->number}}</td>--}}
                                        {{--<td>{{$item->code}}</td>--}}
                                        {{--<td>{{$item->count}}</td>--}}
                                        {{--<td>{{$item->created_at}}</td>--}}
                                        {{--<td>--}}
                                        {{--<form id="otpConfirm" class="form-inline" role="form" method="post"--}}
                                        {{--action="/ads-manage/ad-review/confirmotp">--}}
                                        {{--{!! Form::token() !!}--}}
                                        {{--<input type="hidden" name="otpId" value="{{$item->id}}">--}}
                                        {{--<input type="submit" value="Confirm" class="btn btn-primary btn-xs">--}}
                                        {{--</form>--}}
                                        {{--</td>--}}
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        {{--                        <div class="row">{{$otpNumbers->links('vendor.pagination.custom-backend')}}</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#table').dataTable({
                /* No ordering applied by DataTables during initialisation */
                "order": []
            });
//            $.ajaxSetup({
//                headers: {
//                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                }
//            })
        })

        //        function facebookPost() {
        //            $.ajax({
        //                type: "POST",
        //                url: '/ads-manage/postfacebookoffer',
        //                data: {},
        //                success: function (data) {
        //
        //                }
        //            });
        //        }
    </script>
    {{--    <script src="{{ asset('backend/assets/js/default/commonajax.js') }}"></script>--}}
@endsection