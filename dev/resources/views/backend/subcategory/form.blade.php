<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Main Category</label>
    <div class="col-md-6 col-xs-12">
        {!! Form::select('category_id', $categories, null, ['class' => 'form-control' ]) !!}
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Sub Category Name</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            {!! Form::text('sub_category_name', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Referance</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            {!! Form::text('sub_category_code', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Icon</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            {!! Form::text('icon', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Widget Id</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            {!! Form::text('widget_id', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Advertisement Types</label>
    <div class="col-md-6 col-xs-12">
        {!! Form::select('adtype[]', $adtypes, null, ['class' => 'form-control select','multiple' ]) !!}
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Filters</label>
    <div class="col-md-6 col-xs-12">
        {!! Form::select('categoryfilter_id[]', $categoryfilters, null, ['class' => 'form-control select','multiple' ]) !!}
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Status</label>
    <div class="col-md-6 col-xs-12">
        {!! Form::select('status', array('active'=>'Active','inactive'=>'Inactive'), 'active', ['class' => 'form-control' ]) !!}
    </div>
</div>