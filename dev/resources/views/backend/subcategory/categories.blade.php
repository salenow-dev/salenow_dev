@extends('layouts.app')
@section('title', '| Sub-Category')
@section('content')
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['url' => 'settings/subcategory','id'=>'frmRegister','class'=>'form-horizontal','autocomplete' => 'off']) !!}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Sub-Category</strong> | Add New Record</h3>
                </div>
                <div class="panel-body">
                    @include('backend.subcategory.form')
                </div>
                <div class="panel-footer">
                    <button class="btn btn-default">Clear Form</button>
                    <button class="btn btn-primary pull-right" type="submit" id="aaa">Submit</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Manage</strong> | Sub-Categories</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table datatable table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Category Name</th>
                                    <th>Sub Category Name</th>
                                    <th>Referance</th>
                                    <th>Icon</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($subcategories as $category)
                                    <tr id="{{$category->id}}">
                                        <td>{{$category->category->category_name}}</td>
                                        <td>{{$category->sub_category_name}}</td>
                                        <td>{{$category->sub_category_code}}</td>
                                        <td>{{$category->icon}}</td>
                                        <td>{{$category->status}}</td>
                                        <td class="action-col"><a href="/settings/subcategory/{{$category->id}}/edit" class="edit unline"><span class="lnr lnr-pencil"></span></a> |
                                            <a class="delete unline"
                                               uid="{{$category->id}}" tag="subcategory"><span class="lnr lnr-trash"></span></a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">No Records</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['url' => 'settings/subcategory/assign-brands','id'=>'frmRegister','class'=>'form-horizontal','autocomplete' => 'off']) !!}
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Assign</strong> | Brands to Sub-Categories</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Main Category</label>
                            <div class="col-md-6 col-xs-12">
                                {!! Form::select('subcategory_id', $allSubCategories+['0'=>'Select'], '0', ['id'=>'subcategory_id','class' => 'form-control select','data-live-search'=>'true' ]) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Brands</label>
                            <div class="col-md-6 col-xs-12" id="brands-div">
                                {{--<input type="checkbox" name="brand_id[]" value="1">--}}
{{--                                {!! Form::select('brand_id[]', $allBrands, null, ['class' => 'form-control select','multiple','id'=>'brand_id',]) !!}--}}
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-default">Clear Form</button>
                            <button class="btn btn-primary pull-right" type="submit" id="aaa">Submit</button>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    <script src="{{ asset('backend/assets/js/subcategories/assignbrands.js') }}"></script>
@endsection