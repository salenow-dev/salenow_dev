@extends('layouts.app')
@section('title', '| Sub-Category')
@section('content')
    <div class="row">
        <div class="col-md-12">
            {!! Form::model($subcategory, ['url'=>'settings/subcategory/'.$subcategory->id,'class'=>'form-horizontal','autocomplete' => 'off']) !!}
            {{method_field('PATCH')}}
            <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Sub-Category</strong> | Update Record</h3>
                    </div>
                    <div class="panel-body">
                        @include('backend.subcategory.form')
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-default">Clear Form</button>
                        <button class="btn btn-primary pull-right">Submit</button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection