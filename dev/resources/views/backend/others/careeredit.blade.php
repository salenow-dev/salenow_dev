@extends('layouts.app')
@section('title', '| Member Requests for Become Premium')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Edit Vacancy</strong></h3>
                </div>
                <div class="container">
                        @if(count($careerlist))
                            <table class="table career-table ">
                                <thead>
                                <tr>
                                    <th>Job Title</th>
                                    <th>Experience level</th>
                                    <th class="text-right"></th>
                                    <th></th>
                                </tr>
                                </thead>
                                @foreach($careerlist as $career)
                                    <tr>
                                        <td>{{$career->title}}</td>
                                        <td>{{$career->experiencelevel}}</td>
                                        <td>
                                            <form id="apply"
                                                  action="/admin/member/career/edit/job"
                                                  method="post">
                                                {{csrf_field()}}
                                                <input type="hidden" name="jobno" value={{$career->id}}>
                                                <button type="submit"  class="btn btn-success">
                                                    Edit
                                                </button>
                                            </form>
                                        </td>
                                        <td>
                                            <form action="/admin/member/career/delete/job" method="post" id="delte">
                                                {{csrf_field()}}
                                                <input type="hidden" name="jobno" value={{$career->id}}>
                                                <button type="submit"  class="btn btn-success" >Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        @else
                            <p>No More Vacancies </p>
                        @endif
                        <div class="mar-80-top"></div>
                        <div class="mar-80-top"></div>
                </div>
            </div>

        </div>
    </div>

    </div>

@endsection