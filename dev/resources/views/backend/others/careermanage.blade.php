@extends('layouts.app')
@section('title', '| Member Requests for Become Premium')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Add a Vacancy</strong></h3>
                </div>
                <div class="panel-body">
                    <form id="addjob" class="form-horizontal" role="form" method="post"
                          action="/admin/member/career/addjob">
                        {{csrf_field()}}
                        <div class=" col-md-6">
                            <div class="form-group">
                                <label class="lable_cstm" for="pwd">Job Title</label>
                                <input type="text" name="title" class="form-control" placeholder="Ex : Marketing Excecutive"
                                       value="" required>
                            </div>
                            <div class="form-group">
                                <label class="lable_cstm" for="pwd">Job Position</label>
                                <input type="text" name="position" class="form-control" placeholder="Ex : Marketing Excecutive"
                                       value="" required>
                            </div>
                            <div class="form-group">
                                <label class="lable_cstm" for="pwd">Job Description</label>
                            <textarea name="description" class="form-control" rows="5" placeholder="Description"
                                      required></textarea>
                            </div>
                            <div class="form-group">
                                <label class="lable_cstm" for="pwd">Job Salary</label>
                                <input type="number" name="salary" class="form-control" placeholder="Ex : 20000">
                            </div>
                            <div class="form-group">
                                <label class="lable_cstm" for="pwd">Closing Date</label>
                                <input type="date" name="date" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="lable_cstm" for="pwd">Experience Level</label>
                                <select class="form-control" name="experiencelevel" required>
                                    <option value="Intern">Intern</option>
                                    <option value="Trainee">Trainee</option>
                                    <option value="Junior">Junior</option>
                                    <option value="Mid-Level">Mid-Level</option>
                                    <option value="Senior">Senior</option>
                                    <option value="Lead">Lead</option>
                                    <option value="Manager">Manager</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="lable_cstm" for="pwd">Requirements</label>
                            <textarea name="requirements" class="form-control" rows="5" placeholder="Requirements"
                                      required></textarea>
                            </div>
                            <div class="form-group">
                                <label class="lable_cstm" for="pwd">Benefits</label>
                            <textarea name="benefits" class="form-control" rows="5" placeholder="Benefits"
                                      required></textarea>
                            </div>
                            <div class="form-group">

                                <button type="submit" value="addNewjob" class="btn btn-primary">Add Job&nbsp;<i
                                            class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <div class="col-md-6"></div>

                    </form>

                </div>
            </div>
        </div>

    </div>

@endsection