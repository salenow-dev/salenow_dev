@extends('layouts.app')
@section('title', '| Candidates')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Manage</strong> | Applied Candidate Details</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table datatable table-striped table-bordered" id="table">
                              <thead>
                            <tr>
                                <th>Name</th>
                                <th>Position</th>
                                <th>Gender</th>
                                <th>Age</th>
                                <th>Marital Status</th>
                                <th>Notice Period</th>
                                <th>Applied Date</th>
                                <th>CV</th>
                                <th>Delete</th>
                            </tr>
                              </thead>
                            <tbody>
                            @if(count($candidatelist))
                                @foreach($candidatelist as $item)
                                    <tr id="{{$item->id}}">
                                        <td>{{$item->first_name}} {{$item->last_name}}</td>
                                        <td>{{$item->position}}</td>
                                        pi<td>{{$item->gender}}</td>
                                        <td>{{$item->birthday}}</td>
                                        <td>{{$item->created_at}}</td>
                                        <td> {{$item->date}} {{$item->month}}</td>
                                        <td> {{$item->date}} {{$item->month}}</td>
                                        <td>
                                            <a target="_blank" href="{{ asset('/pdf_files/'.$item->attachment)}}"> <i style="font-size: 25px" class="ion-arrow-down-a"></i></a>
                                        </td>
                                        <td><a href="/admin/member/careerview/{$id}">Delete CV</a></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        {{--<div class="row">{{$premiumRequests->links('vendor.pagination.custom')}}</div>--}}
                    </div>
                </div>
            </div>

        </div>
    </div>
    <script>

    </script>
    {{--    <script src="{{ asset('backend/assets/js/default/commonajax.js') }}"></script>--}}
@endsection