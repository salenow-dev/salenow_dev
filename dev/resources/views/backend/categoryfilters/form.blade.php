<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Filter Name</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            {!! Form::text('filtername', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Filter Referance</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            {!! Form::text('filterref', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Filter Sort Order</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            {!! Form::text('sortby', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Filter Values</label>
    <div class="col-md-6 col-xs-12">
            {!! Form::text('filtervalue', $filterValues, ['class' => 'form-control tagsinput']) !!}
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Status</label>
    <div class="col-md-6 col-xs-12">
        {!! Form::select('status', array('active'=>'Active','inactive'=>'Inactive'), 'active', ['class' => 'form-control' ]) !!}
    </div>
</div>