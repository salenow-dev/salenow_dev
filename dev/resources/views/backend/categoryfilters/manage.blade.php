@extends('layouts.app')
@section('title', '| Category Filters')
@section('content')
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['url' => 'settings/filters','id'=>'frmRegister','class'=>'form-horizontal','autocomplete' => 'on']) !!}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Category Filters</strong> | Add New Record</h3>
                </div>
                <div class="panel-body">
                    @include('backend.categoryfilters.form')
                </div>
                <div class="panel-footer">
                    <button class="btn btn-default">Clear Form</button>
                    <button class="btn btn-primary pull-right" type="submit" id="aaa">Submit</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Manage</strong> | Category Filters</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table datatable table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Filter Name</th>
                                    <th>Filter Ref</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($allList))
                                    @foreach($allList as $item)
                                        <tr id="{{$item->id}}">
                                            <td>{{$item->filtername}}</td>
                                            <td>{{$item->filterref}}</td>
                                            <td>{{$item->status}}</td>
                                            <td>
                                                <a href="/settings/filters/{{$item->id}}/edit" class="edit unline"><span class="lnr lnr-pencil"></span>
                                                |
                                                <a class="delete unline"
                                                   uid="{{$item->id}}" tag="Categoryfilter"><span class="lnr lnr-trash"></span></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--    <script src="{{ asset('backend/assets/js/default/commonajax.js') }}"></script>--}}
@endsection