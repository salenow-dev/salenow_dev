@extends('layouts.app')
@section('title', '| Review Pin-to-Top List')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>PlayZone</strong> | Add Extra Questions</h3>
                </div>
                <div class="panel-body">

                    {!! Form::open(['url' => '/playzone/extra-questions/save','id'=>'frmRegister','class'=>'form-horizontal','autocomplete' => 'off','enctype'=>'multipart/form-data']) !!}
                    {!! csrf_field() !!}
                    @include('backend.playzone.question-form');
                    <div class="panel-footer">
                        <button class="btn btn-default" type="reset">Clear Form</button>
                        <button type="submit" class="btn btn-danger pull-right">Register</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#table').dataTable({
                /* No ordering applied by DataTables during initialisation */
                "order": []
            });
        })
    </script>
@endsection