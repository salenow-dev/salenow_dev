@extends('layouts.app')
@section('title', '| Review Pin-to-Top List')
@section('content')
    <style>
        .badge {
            background: #fea502 !important;
            color: #111 !important;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>PlayZone</strong> | Items Buy Requests | {{str_replace('_',' ',$status)}}</h3>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{{asset('')}}playzone/prizes-buy-requests/list/all">
                        <button class="btn btn-primary">All</button>
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{{asset('')}}playzone/prizes-buy-requests/list/ship_qued">
                        <button class="btn btn-primary">Shipping Queue &nbsp;&nbsp;<span
                                    class="badge">{{$qued_count}}</span></button>
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{{asset('')}}playzone/prizes-buy-requests/list/pending">
                        <button class="btn btn-warning">Pending Items &nbsp;&nbsp;<span
                                    class="badge">{{$pending_count}}</span></button>
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{{asset('')}}playzone/prizes-buy-requests/list/ignored">
                        <button class="btn btn-danger">Ignored Items &nbsp;&nbsp;<span
                                    class="badge">{{$ignore_count}}</span></button>
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{{asset('')}}playzone/prizes-buy-requests/list/shiped">
                        <button class="btn btn-success">Shiped Items &nbsp;&nbsp;<span
                                    class="badge">{{$shiped_count}}</span></button>
                    </a>

                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table datatable table-striped table-bordered" id="table">
                            <thead>
                            <tr>
                                <th width="50">#</th>
                                <th width="300">User Details</th>
                                <th width="300">Prize Details</th>
                                <th>Requested</th>
                                <th width="250">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($prizes))
                                <?php $x = 1; ?>
                                @foreach($prizes as $rqst)
                                    <tr id="{{$x}}">
                                        <td>{{$x}}</td>
                                        <td>
                                            Name : <b>{{$rqst->first_name}} {{$rqst->last_name}}</b><br>
                                            Address : <b>{{$rqst->address}}</b><br>
                                            Postal Code : <b>{{$rqst->post_code}}</b><br>
                                            Email : <b>{{$rqst->email}}</b><br>
                                            Mobile : <b>{{$rqst->mobile}}</b><br>

                                            <div class="print_{{$x}}" style="display: none">
                                                <div style="width: 50%;float: left">
                                                    From:-<br>
                                                    <img src="{{asset('')}}images/salenow/saleme-logo.png" alt="" width="80px"><br>
                                                    SaleMe.lk Pvt Ltd <br>
                                                    Anagarika Dharmapala Mawatha <br>
                                                    Kandy. <br>
                                                    20000<br>
                                                    0763818188
                                                </div>
                                                <div style="width: 50%;float: right">
                                                    To:- <br>
                                                    {{$rqst->first_name}} {{$rqst->last_name}}<br>
                                                    {{$rqst->address}}<br>
                                                    {{$rqst->post_code}}<br>
                                                    {{$rqst->mobile}}<br>
                                                </div>

                                            </div>
                                        </td>
                                        <td>Image : <img src="{{asset('')}}gamezone/img/store/{{$rqst->image}}" alt=""
                                                         height="75px"><br>
                                            Title : <b>{{$rqst->title}} </b><br>
                                            Available QTY : <b>{{$rqst->qty}}</b>
                                        </td>
                                        <td>
                                            on <b>{{$rqst->created_at}} </b><br><br>
                                            as {!!($rqst->is_prize)?'<span class="label label-default">Redeem Prize</span>' : '<span class="label label-warning">Buy</span>' !!}
                                            <br>
                                            <br>
                                            @if($rqst->buy_status == 'ignored')
                                                On <span class="label label-danger">Ignored</span> Status
                                            @elseif($rqst->buy_status == 'shiped')
                                                On <span class="label label-success">Shiped</span> Status
                                            @elseif($rqst->buy_status == 'ship_qued')
                                                On <span class="label label-primary">Ship Queued</span> Status
                                            @elseif($rqst->buy_status == 'delivered')
                                                On <span class="label label-success">Delivered</span> Status
                                            @else
                                                On <span class="label label-info">Pending</span> Status
                                            @endif
                                        </td>
                                        <td>
                                            @if($rqst->buy_status == 'pending' OR $rqst->buy_status == 'ignored')
                                                <button class="btn btn-success"
                                                        onclick="item_status_noty({{$rqst->id}},'ship_qued')">Accept
                                                </button>
                                            @endif
                                            @if($rqst->buy_status == 'pending')
                                                <button class="btn btn-warning"
                                                        onclick="item_status_noty({{$rqst->id}},'ignored')">Ignore
                                                </button>
                                            @endif
                                            @if($rqst->buy_status == 'ship_qued' AND !is_null($rqst->shiped_date))
                                                Queued On <span
                                                        class="label label-primary">{{date('g:ia \o\n l jS F Y', strtotime($rqst->shiped_date))}}</span>
                                                <br>
                                                <button class="btn btn-success btn-block" style="margin-top: 15px;"
                                                        onclick="item_status_noty({{$rqst->id}},'shiped')">Proceed
                                                    Shipping
                                                </button>
                                                <button class="btn btn-default btn-block" style="margin-top: 15px;" onclick="print('print_{{$x}}')">
                                                    <i class="fa fa-print"></i>  Print Address
                                                </button>
                                            @endif
                                            @if($rqst->buy_status == 'shiped' AND !is_null($rqst->shiped_date))
                                                Shipped On <span
                                                        class="label label-success">{{date('g:ia \o\n l jS F Y', strtotime($rqst->shiped_date))}}</span>
                                            @endif

                                        </td>

                                    </tr>
                                    <?php $x++; ?>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#table').dataTable({
                /* No ordering applied by DataTables during initialisation */
                "order": []
            });
        })

        function print(elem)
        {
            var mywindow = window.open('', 'PRINT', 'height=400,width=600');

            mywindow.document.write('<html>');
            mywindow.document.write($('.'+elem).html());
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10*/

            mywindow.print();
            mywindow.close();

            return true;
        }

        //        Change status
        function item_status_noty(id, status) {
            noty({
                text: 'Do you want to do this?',
                layout: 'topRight',
                buttons: [{
                    addClass: 'btn btn-success btn-clean', text: "Sure, Do it", onClick: function ($noty) {
                        change_status(id, status);
                        $noty.close();
                    }
                },
                    {
                        addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function ($noty) {
                        $noty.close();
                    }
                    }
                ]
            })
        }
        function change_status(id, status) {
//            alert(id + status);
            var data = {'id': id, 'status': status};
            $.ajax({
                type: "post",
                url: '/playzone/prizes-buy-requests/change-status/' + id,
                data: data,
                success: function (res) {
                    noty({
                        text: res.message,
                        layout: 'topRight',
                        type: res.status,
                    });
                    setTimeout(function () {
                        location.reload();
                    }, 1000)
                }
            });
        }
        //        END change status

        //        Delete
        function delete_noty(id) {
            noty({
                text: 'Do you want to Deactivate?',
                layout: 'topRight',
                buttons: [{
                    addClass: 'btn btn-success btn-clean', text: 'Delete', onClick: function ($noty) {
                        delete_que(id);
                        $noty.close();
                    }
                },
                    {
                        addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function ($noty) {
                        $noty.close();
                    }
                    }
                ]
            })
        }
        function delete_que(id) {
            var data = {'id': id};
            $.ajax({
                type: "post",
                url: '/playzone/store/delete/' + id,
                data: data,
                success: function (res) {
                    noty({
                        text: res.message,
                        layout: 'topRight',
                        type: res.status,
                    });
                    setTimeout(function () {
                        location.reload();
                    }, 1000)
                }
            });
        }
        //        END delete
    </script>
@endsection