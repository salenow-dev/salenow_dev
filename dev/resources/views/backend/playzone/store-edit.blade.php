@extends('layouts.app')
@section('title', '| Review Pin-to-Top List')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>PlayZone</strong> | Edit Store</h3>
                </div>
                <div class="panel-body">
                    {!! Form::model($store, ['url'=>'/playzone/prizes/'.$id.'/update/','class'=>'form-horizontal','autocomplete' => 'off','enctype'=>'multipart/form-data']) !!}
                    {!! method_field('PATCH') !!}
                    {!! csrf_field() !!}
                    @include('backend.playzone.store-form');
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Current Image</label>
                        <div class="col-md-6 col-xs-12">
                            <p style="color: #ff0000">Select an image to above to replace this image</p>
                            <img src="{{asset('')}}/gamezone/img/store/{{$store->image}}" alt="" class="img-responsive">
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-default" type="reset">Clear Form</button>
                        <button type="submit" class="btn btn-danger pull-right">Register</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#table').dataTable({
                /* No ordering applied by DataTables during initialisation */
                "order": []
            });
        })
    </script>
@endsection