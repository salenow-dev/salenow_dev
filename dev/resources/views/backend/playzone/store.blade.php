@extends('layouts.app')
@section('title', '| Review Pin-to-Top List')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>PlayZone</strong> | Extra Questions</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table datatable table-striped table-bordered" id="table">
                            <thead>
                            <tr>
                                <th width="50">#</th>
                                <th width="150">Image</th>
                                <th>Title</th>
                                <th width="200">Description</th>
                                <th width="200">Price</th>
                                <th>Is Prize</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($questions))
                                @foreach($questions as $rqst)
                                    <tr id="{{$rqst->id}}">
                                        <td>{{$rqst->id}}</td>
                                        <td><img src="{{asset('')}}gamezone/img/store/{{$rqst->image}}" alt=""
                                                 width="150px"></td>
                                        <td>{{$rqst->title}}</td>
                                        <td>{{$rqst->description}}</td>
                                        <td>QTY : <b>{{$rqst->qty}}</b><br>
                                            Price : <b>{{$rqst->price}}</b><br>
                                            Min Points : <b>{{$rqst->min_point}}</b></td>
                                        <td>
                                            @if($rqst->is_prize)
                                                <p class="text-center text-black" style="color: #229922">
                                                    <b>Yes</b></p>
                                                <button class="btn btn-danger btn-block"
                                                        onclick="make_prize({{$rqst->id}},0)">Remove
                                                </button></td>
                                        @else
                                            <p class="text-center" style="color: #ff0000">
                                                <b>No</b></p>
                                            <button class="btn btn-success btn-block"
                                                    onclick="make_prize({{$rqst->id}},1)">Make Prize
                                            </button></td>
                                        @endif
                                        <td>
                                            @if($rqst->status == "active")
                                                <p class="text-center text-black" style="color: #229922">
                                                    <b>{{ucwords($rqst->status)}}</b></p>
                                                <button class="btn btn-warning btn-block"
                                                        onclick="deactive_noty({{$rqst->id}},'inactive')">Inactive
                                                </button></td>
                                        @else
                                            <p class="text-center" style="color: #ff0000">
                                                <b>{{ucwords($rqst->status)}}</b></p>
                                            <button class="btn btn-success btn-block"
                                                    onclick="deactive_noty({{$rqst->id}},'active')">Active
                                            </button></td>
                                        @endif
                                        <td>
                                            <a href="{{asset('')}}/playzone/prizes/{{$rqst->id}}/edit/" class="btn btn-primary btn-block">
                                                Edit
                                            </a>
                                            <button class="btn btn-danger btn-block"
                                                    onclick="delete_noty({{$rqst->id}})">Delete
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#table').dataTable({
                /* No ordering applied by DataTables during initialisation */
                "order": []
            });
        })

        //     make prize
        function make_prize(id, status) {
            var data = {'id': id, 'status': status};
            $.ajax({
                type: "post",
                url: '/playzone/store/make-prize/' + id,
                data: data,
                success: function (res) {
                    noty({
                        text: res.message,
                        layout: 'topRight',
                        type: res.status,
                    });
                    setTimeout(function () {
                        location.reload();
                    }, 1000)
                }
            });
        }
        //        Change status
        function deactive_noty(id, status) {
            noty({
                text: 'Do you want to Deactivate?',
                layout: 'topRight',
                buttons: [{
                    addClass: 'btn btn-success btn-clean', text: status, onClick: function ($noty) {
                        change_status(id, status);
                        $noty.close();
                    }
                },
                    {
                        addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function ($noty) {
                        $noty.close();
                    }
                    }
                ]
            })
        }
        function change_status(id, status) {
            var data = {'id': id, 'status': status};
            $.ajax({
                type: "post",
                url: '/playzone/store/change-status/' + id,
                data: data,
                success: function (res) {
                    noty({
                        text: res.message,
                        layout: 'topRight',
                        type: res.status,
                    });
                    setTimeout(function () {
                        location.reload();
                    }, 1000)
                }
            });
        }
        //        END change status

        //        Delete
        function delete_noty(id) {
            noty({
                text: 'Do you want to Deactivate?',
                layout: 'topRight',
                buttons: [{
                    addClass: 'btn btn-success btn-clean', text: 'Delete', onClick: function ($noty) {
                        delete_que(id);
                        $noty.close();
                    }
                },
                    {
                        addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function ($noty) {
                        $noty.close();
                    }
                    }
                ]
            })
        }
        function delete_que(id) {
            var data = {'id': id};
            $.ajax({
                type: "post",
                url: '/playzone/store/delete/' + id,
                data: data,
                success: function (res) {
                    noty({
                        text: res.message,
                        layout: 'topRight',
                        type: res.status,
                    });
                    setTimeout(function () {
                        location.reload();
                    }, 1000)
                }
            });
        }
        //        END delete
    </script>
@endsection