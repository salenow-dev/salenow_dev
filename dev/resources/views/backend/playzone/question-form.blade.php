<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Title</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Answer 01</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-dot-circle-o"></span></span>
            {{--<input class="form-control" required="" name="answer1" type="text">--}}
            {!! Form::text('answer1', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Answer 02</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-dot-circle-o"></span></span>
            {{--<input class="form-control" required="" name="answer2" type="text">--}}
            {!! Form::text('answer2', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Answer 03</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-dot-circle-o"></span></span>
            {{--<input class="form-control" required="" name="answer3" type="text">--}}
            {!! Form::text('answer3', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Image</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-image"></span></span>
            <input class="form-control" {{(!isset($id)?'required':'')}} name="image_file" type="file">
        </div>
    </div>
</div>