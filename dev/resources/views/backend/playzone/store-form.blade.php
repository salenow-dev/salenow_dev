<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Title</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            {!! Form::text('title', null, ['class' => 'form-control','required'=>'required']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Description</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-dot-circle-o"></span></span>
            {{--<input class="form-control" required="" name="answer1" type="text">--}}
            {!! Form::textarea('description', null, ['class' => 'form-control' ,'rows'=>4,'required'=>'required']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Quantity</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-dot-circle-o"></span></span>
            {{--<input class="form-control" required="" name="answer1" type="text">--}}
            {!! Form::number('qty', null, ['class' => 'form-control','min'=> 0,'required'=>'required']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Minimum Points</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-dot-circle-o"></span></span>
            {{--<input class="form-control" required="" name="answer1" type="text">--}}
            {!! Form::number('min_point', null, ['class' => 'form-control','min'=> 0]) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Prize</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-dot-circle-o"></span></span>
            {{--<input class="form-control" required="" name="answer1" type="text">--}}
            {!! Form::number('price', null, ['class' => 'form-control','min'=> 0,'required'=>'required']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Make as Price</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            {{--<span class="input-group-addon"><span class="fa fa-dot-circle-o"></span></span>--}}
            <div class="input-group">
                @if(isset($store) AND $store->is_prize)
                    {!! Form::checkbox('is_prize', 1, true, ['id'=>'is_prize']) !!}
                @else
                    {!! Form::checkbox('is_prize', 1, null,['id'=>'is_prize']) !!}
                @endif
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Image</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-image"></span></span>
            <input class="form-control" {{(!isset($id)?'required':'')}} name="image_file" type="file">
        </div>
    </div>
</div>