@extends('layouts.app')
@section('title', '| Review Pin-to-Top List')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>PlayZone</strong> | Playzone Players List</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table datatable table-striped table-bordered" id="table">
                            <thead>
                            <tr>
                                <th width="50">#</th>
                                <th width="150">Details</th>
                                <th width="250">playzone missions</th>
                                <th width="250">Marks</th>
                                {{--<th>Answer 01</th>--}}
                                {{--<th>Answer 02</th>--}}
                                {{--<th>Answer 03</th>--}}
                                {{--<th>Status</th>--}}
                                {{--<th>Action</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($players))
                                <?php $x=1; ?>
                                @foreach($players as $player)
                                    <tr id="{{$player->id}}">
                                        <td>{{$x}}</td>
                                        <td>
                                            Name : <b>{{$player->first_name}}</b><br>
                                            Email : <b>{{$player->email}}</b><br>
                                        </td>
                                        <td>
                                            Daily Point : <b>{{!is_null($player->daily_coin_last_update_at)?$player->daily_coin_last_update_at:'Not Competed'}}</b><br>
                                            Daily Question : <b>{{!is_null($player->daily_question_last_update_at)?$player->daily_question_last_update_at:'Not Competed'}}</b><br>
                                            Extra Question : <b>{{!is_null($player->extra_question_last_update_at)?$player->extra_question_last_update_at:'Not Competed'}}</b><br>
                                            Facebook Page share : <b>{{($player->facebook_page_share)?'Done':'Not Competed'}}</b><br>
                                            FB 1st post Share : <b>{{($player->facebook_page_first_post_share)?'Done':'Not Competed'}}</b><br>
                                            FB 2nd post Share : <b>{{($player->facebook_page_second_post_share)?'Done':'Not Competed'}}</b><br>
                                            G-plus follow : <b>{{($player->google_plus_follow)?'Done':'Not Competed'}}</b><br>
                                            G-plus Share : <b>{{($player->google_plus_share)?'Done':'Not Competed'}}</b><br>
                                            Youtube Subscribe : <b>{{($player->youtube_subscribe)?'Done':'Not Competed'}}</b><br>
                                            Twitter Follow : <b>{{($player->twitter_follow)?'Done':'Not Competed'}}</b><br>
                                            Instagram Follow : <b>{{($player->instagram_follow)?'Done':'Not Competed'}}</b><br>
                                            Linkedin Follow : <b>{{($player->linkedin_follow)?'Done':'Not Competed'}}</b><br>
                                            Kokis Challage Last Play : <b>{{!is_null($player->game_01_last_played_at)?$player->game_01_last_played_at:'Not Competed'}}</b><br>
                                            Kokis Challage Play times : <b>{{($player->game_01_palyed_times)?$player->game_01_palyed_times:'Not Competed'}}</b><br>
                                            Cube Champ Last Play : <b>{{!is_null($player->game_02_last_played_at)?$player->game_02_last_played_at:'Not Competed'}}</b><br>
                                            Cube Camp Play time : <b>{{($player->game_02_palyed_times)?$player->game_02_palyed_times:'Not Competed'}}</b><br>
                                        </td>
                                        <td>
                                            Current Score : <b>{{$player->current_score}}</b><br>
                                            Final Score : <b>{{$player->final_score}}</b><br>
                                        </td>

                                    </tr>
                                    <?php $x++; ?>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#table').dataTable({
                /* No ordering applied by DataTables during initialisation */
                "order": []
            });
        })

//        Change status
        function deactive_noty(id,status) {
            noty({
                text: 'Do you want to Deactivate?',
                layout: 'topRight',
                buttons: [{
                    addClass: 'btn btn-success btn-clean', text: status, onClick: function ($noty) {
                        change_status(id,status);
                        $noty.close();
                    }
                },
                    {
                        addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function ($noty) {
                        $noty.close();
                    }
                    }
                ]
            })
        }
        function change_status(id,status){
            var data = {'id': id, 'status': status};
            $.ajax({
                type: "post",
                url: '/playzone/daily-questions/change-status/' + id,
                data: data,
                success: function (res) {
                    noty({
                        text: res.message,
                        layout: 'topRight',
                        type: res.status,
                    });
                    setTimeout(function () {
                        location.reload();
                    },1000)
                }
            });
        }
//        END change status

//        Delete
        function delete_noty(id) {
            noty({
                text: 'Do you want to Deactivate?',
                layout: 'topRight',
                buttons: [{
                    addClass: 'btn btn-success btn-clean', text: 'Delete', onClick: function ($noty) {
                        delete_que(id);
                        $noty.close();
                    }
                },
                    {
                        addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function ($noty) {
                        $noty.close();
                    }
                    }
                ]
            })
        }
        function delete_que(id) {
            var data = {'id': id};
            $.ajax({
                type: "post",
                url: '/playzone/daily-questions/delete/' + id,
                data: data,
                success: function (res) {
                    noty({
                        text: res.message,
                        layout: 'topRight',
                        type: res.status,
                    });
                    setTimeout(function () {
                        location.reload();
                    },1000)
                }
            });
        }
//        END delete
    </script>
@endsection