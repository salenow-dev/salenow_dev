@extends('layouts.gamezone')
@section('title', 'SaleMe PlayZone | Facebook Missions | Play Share Win')
@section('content')
    <style>
        #demo {
            text-align: left;
            color: #111;
            float: right;
            font-size: 20px;
            margin-bottom: 15px;
        }

        .ending {
            text-align: left;
            color: #111;
            float: left;
            margin: 0px;
            font-size: 20px;
            margin-bottom: 15px;
        }

        .team-player img {
            width: 100%;
            margin-bottom: 10px;
        }

        .team-player i {
            background: #fff;
            padding: 5px;
        }

        .iframe {
            min-height: 500px;
            width: 100%;
            margin-bottom: 10px;
        }

        .iframediv {
            background: #eee;
        }

        .title-1 {
            margin-top: 15px;
            font-size: 23px;
            font-weight: 400;
        }

        .earn-txt {
            color: #fea502;
            font-weight: 700;
            font-size: 16px;
            margin: 0;
        }

        .earnd-txt {
            color: #64c136;
            font-weight: 700;
            font-size: 16px;
            margin: 0;
        }

        .radio-btn-div {
            padding-left: 25px;
        }
        /*---------------*/
        .modal-header {
            background: #333;
            padding: 15px 0px !important;
        }

        .modal-title {
            display: block;
            width: 100%;
            margin: 0px;
            color: #eee;
            font-size: 20px;
        }

        .modal-content p {
            margin: 0;
            padding: 10px;
            color: #af2f2f;
            font-weight: 600;
        }

        .modal-body {
            padding: 5px 5px 10px !important;
        }

        .modal-backdrop {
            background: rgb(216, 110, 48) !important;
        }
    </style>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0&appId=1054163521396174&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <div class="wrapper">
        <div class="page-header page-header-small price-header">
            <div class="page-header-image" data-parallax="true" style="background-image: url({{asset('')}}gamezone/img/facebook-bg.jpg);">
            </div>
            <div class="container">
                <div class="content-center">
                    <h3>SaleMe.lk Facebook Missions</h3>
                    <h5>Play - Share - Win</h5>

                </div>
            </div>
        </div>
        <div class="section section-notifications" id="notifications">
            <div class="alert alert_display" role="alert">
                <div class="container">
                    <div class="alert-icon">
                        <i class="now-ui-icons ui-2_like icon"></i>
                    </div>
                    <span class="msg"></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">
                                <i class="now-ui-icons ui-1_simple-remove"></i>
                            </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="section section-team text-center">
            <div class="container">
                <!--<h2 class="title">Here is our team</h2>-->
                <div class="team">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="team-player">
                                {{ Html::image('images/salenow/banner.gif', 'saleme.lk', array('class' => 'img-responsive')) }}
                                <br>
                                {{--like fan page--}}
                                <div class="card">
                                    <ul class="nav nav-tabs nav-tabs-neutral bg-info" role="tablist"
                                        style="background: #3b5999 !important;"
                                        data-background-color="orange">
                                        <span class="navbar-brand"><i class="ion-thumbsup"
                                                                      style="background: transparent !important;"></i>  Like SaleMe.lk Facebook Fan Page</span>

                                    </ul>
                                    <div class="card-body">
                                        <!-- Tab panes -->
                                        <div class="tab-content text-center">
                                            <div class="tab-pane active">
                                                @if(!is_null($missiondata) AND $missiondata->facebook_page_like)
                                                    <p class="pull-right earnd-txt">
                                                        <i class="ion-ios-checkmark-outline"></i> Points Earned
                                                    </p>
                                                    <span class="btn btn-success btn-lg">
                                                        <i class="ion-ios-checkmark-outline"
                                                           style="background: transparent !important;"></i>
                                                        Liked SaleMe.lk fan page
                                                    </span>
                                                @else
                                                    <p class="pull-right earn-txt"><i class="ion-help-buoy"></i> Earn 5
                                                        Points</p>
                                                    <button type="button" class="btn btn-info btn-lg facebooklike"
                                                            style="background: #3b5999 !important;">
                                                        <i class="ion-social-facebook"
                                                           style="background: transparent !important;"></i>
                                                        Like SaleMe.lk fan page
                                                    </button>
                                                @endif

                                                <div id="Div1">
                                                </div>
                                            </div>
                                            <!-- Modal -->
                                            <div id="myModal" class="modal fade" role="dialog">
                                                <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Like SaleMe.lk Facebook page</h4>
                                                        </div>
                                                        <p id="mycounter"></p>
                                                        <div class="modal-body">
                                                            <div class="fb-page"
                                                                 data-href="https://www.facebook.com/salemeclassified/"
                                                                 data-tabs="timeline" data-small-header="false"
                                                                 data-adapt-container-width="false"
                                                                 data-hide-cover="false" data-show-facepile="true">
                                                                <blockquote
                                                                        cite="https://www.facebook.com/salemeclassified/"
                                                                        class="fb-xfbml-parse-ignore"><a
                                                                            href="https://www.facebook.com/salemeclassified/">SaleMe.lk</a>
                                                                </blockquote>
                                                            </div>
                                                        </div>
                                                        {{--<div class="modal-footer">--}}
                                                            {{--<button type="button" class="btn btn-default"--}}
                                                                    {{--data-dismiss="modal">Close--}}
                                                            {{--</button>--}}
                                                        {{--</div>--}}
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <script>
                                    $('.facebooklike').click(function () {
                                        $("#myModal").modal({backdrop: 'static', keyboard: false});
                                        onTimer();
                                    });
                                </script>
                                <script>
                                    i = 30;
                                    function onTimer() {
                                        document.getElementById('mycounter').innerHTML = 'Wait ' + i + ' Seconds';
                                        i--;
                                        if (i > 0) {
                                            setTimeout(onTimer, 1000);
                                        }
                                        if (i == 1) {
                                            window.location = "/playzone/mission/savefacebooklike/like";
                                            return true;
                                        }
                                    }
                                </script>
                                {{--END fb like--}}

                                {{--fb Share--}}
                                {{--like fan page--}}
                                <div class="card">
                                    <ul class="nav nav-tabs nav-tabs-neutral bg-info" role="tablist"
                                        style="background: #3b5999 !important;"
                                        data-background-color="orange">
                                        <span class="navbar-brand">
                                            <i class="ion-share" style="background: transparent !important;"></i>  Share SaleMe.lk Facebook Fan Page</span>

                                    </ul>
                                    <div class="card-body">
                                        <!-- Tab panes -->
                                        <div class="tab-content text-center">
                                            <div class="tab-pane active">
                                                @if(!is_null($missiondata) AND $missiondata->facebook_page_share)
                                                    <p class="pull-right earnd-txt">
                                                        <i class="ion-ios-checkmark-outline"></i> Points Earned
                                                    </p>
                                                    <span class="btn btn-success btn-lg">
                                                        <i class="ion-ios-checkmark-outline"
                                                           style="background: transparent !important;"></i>
                                                        Shared SaleMe.lk fan page
                                                    </span>
                                                @else
                                                    <p class="pull-right earn-txt"><i class="ion-help-buoy"></i> Earn 5
                                                        Points</p>
                                                    <button type="button" class="btn btn-info btn-lg fbshare"
                                                            style="background: #3b5999 !important;">
                                                        <i class="ion-social-facebook"
                                                           style="background: transparent !important;"></i>
                                                        Share SaleMe.lk fan page
                                                    </button>
                                                @endif

                                                <div id="Div1">
                                                </div>
                                            </div>
                                            <!-- Modal -->
                                            <div id="share" class="modal fade" role="dialog">
                                                <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Share SaleMe.lk Facebook page</h4>
                                                        </div>
                                                        <p id="sharecount"></p>
                                                        <div class="modal-body">
                                                            <div class="fb-share-button" data-href="https://www.facebook.com/salemeclassified/" data-layout="button_count" data-size="large" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.facebook.com%2Fsalemeclassified%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>
                                                        </div>
                                                        {{--<div class="modal-footer">--}}
                                                            {{--<button type="button" class="btn btn-default"--}}
                                                                    {{--data-dismiss="modal">Close--}}
                                                            {{--</button>--}}
                                                        {{--</div>--}}
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <script>
                                    $('.fbshare').click(function () {
                                        $("#share").modal({backdrop: 'static', keyboard: false});
                                        onTimer2();
                                    });
                                </script>
                                <script>
                                    i = 30;
                                    function onTimer2() {
                                        document.getElementById('sharecount').innerHTML = 'Wait ' + i + ' Seconds';
                                        i--;
                                        if (i > 0) {
                                            setTimeout(onTimer2, 1000);
                                        }
                                        if (i == 1) {
                                            window.location = "/playzone/mission/savefacebooklike/share";
                                            return true;
                                        }
                                    }
                                </script>
                                {{--END fb share--}}

                                {{--share post 1--}}
                                {{--<div class="card">--}}
                                    {{--<ul class="nav nav-tabs nav-tabs-neutral bg-info" role="tablist"--}}
                                        {{--style="background: #3b5999 !important;"--}}
                                        {{--data-background-color="orange">--}}
                                        {{--<span class="navbar-brand">--}}
                                            {{--<i class="ion-share" style="background: transparent !important;"></i>  Share first post on Facebook Fan Page</span>--}}

                                    {{--</ul>--}}
                                    {{--<div class="card-body">--}}
                                        {{--<!-- Tab panes -->--}}
                                        {{--<div class="tab-content text-center">--}}
                                            {{--<div class="tab-pane active">--}}
                                                {{--@if($missiondata->facebook_page_first_post_share)--}}
                                                    {{--<p class="pull-right earnd-txt">--}}
                                                        {{--<i class="ion-ios-checkmark-outline"></i> Points Earned--}}
                                                    {{--</p>--}}
                                                    {{--<span class="btn btn-success btn-lg">--}}
                                                        {{--<i class="ion-ios-checkmark-outline"--}}
                                                           {{--style="background: transparent !important;"></i>--}}
                                                        {{--Shared SaleMe.lk fan page--}}
                                                    {{--</span>--}}
                                                {{--@else--}}
                                                    {{--<p class="pull-right earn-txt"><i class="ion-help-buoy"></i> Earn 5--}}
                                                        {{--Points</p>--}}
                                                    {{--<button type="button" class="btn btn-info btn-lg share_post_1"--}}
                                                            {{--style="background: #3b5999 !important;">--}}
                                                        {{--<i class="ion-social-facebook"--}}
                                                           {{--style="background: transparent !important;"></i>--}}
                                                        {{--Share Share first post--}}
                                                    {{--</button>--}}
                                                {{--@endif--}}

                                                {{--<div id="Div1">--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<!-- Modal -->--}}
                                            {{--<div id="share_post_1" class="modal fade" role="dialog">--}}
                                                {{--<div class="modal-dialog">--}}

                                                    {{--<!-- Modal content-->--}}
                                                    {{--<div class="modal-content">--}}
                                                        {{--<div class="modal-header">--}}
                                                            {{--<button type="button" class="close" data-dismiss="modal">--}}
                                                                {{--&times;--}}
                                                            {{--</button>--}}
                                                            {{--<h4 class="modal-title">Share SaleMe.lk Facebook page posts</h4>--}}
                                                            {{--<p id="share_post_1_count"></p>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="modal-body">--}}
                                                            {{--View the post here--}}
                                                        {{--</div>--}}
                                                        {{--<div class="modal-footer">--}}
                                                            {{--<button type="button" class="btn btn-default"--}}
                                                                    {{--data-dismiss="modal">Close--}}
                                                            {{--</button>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}

                                                {{--</div>--}}
                                            {{--</div>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<script>--}}
                                    {{--$('.share_post_1').click(function () {--}}
                                        {{--$("#share_post_1").modal({backdrop: 'static', keyboard: false});--}}
                                        {{--onTimer2();--}}
                                    {{--});--}}
                                {{--</script>--}}
                                {{--<script>--}}
                                    {{--j = 30;--}}
                                    {{--function onTimer2() {--}}
                                        {{--document.getElementById('share_post_1_count').innerHTML = 'Wait ' + j + ' Seconds';--}}
                                        {{--j--;--}}
                                        {{--if (j > 0) {--}}
                                            {{--setTimeout(onTimer2, 1000);--}}
                                        {{--}--}}
                                        {{--if (j == 1) {--}}
                                            {{--window.location = "/playzone/mission/savefacebooklike/share";--}}
                                            {{--return true;--}}
                                        {{--}--}}
                                    {{--}--}}
                                {{--</script>--}}
                                {{--END share post 1--}}
                            </div>
                        </div>
                        <div class="col-md-4 iframediv">
                            <h4 class="title-1">See advertisments on SaleMe.lk</h4>
                            <iframe src="https://saleme.lk/blog/getadstoblog" frameborder="0" class="iframe"></iframe>
                            <br>
                            {{ Html::image('images/salenow/banners/saleme-mobile-banner-gif.gif', 'saleme.lk', array('class' => 'img-responsive visible-xs')) }}
                        </div>
                    </div>

                </div>
            </div>
        </div>

@endsection