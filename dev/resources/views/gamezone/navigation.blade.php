<nav class="navbar navbar-expand-lg bg-primary fixed-top navbar-transparent " color-on-scroll="400">
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand" href="{{asset('')}}" rel="tooltip">
                <img src="{{asset('images/salenow/saleme-logo.png')}}" alt="">
            </a>
            <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
                    aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse justify-content-end" id="navigation"
             data-nav-image="../assets/img/blurred-image-1.jpg">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{asset('')}}playzone/home">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{asset('')}}playzone/play">Play Zone</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{asset('')}}playzone/prizes">Prizes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{asset('')}}playzone/store">Store</a>
                </li>
                <li class="nav-item dropdown user-dropdown">
                    <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown">
                        <i class=" now-ui-icons users_single-02" aria-hidden="true"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-header text-muted">{{$memberdetails->first_name}} {{$memberdetails->last_name}}</a>
                        <a class="dropdown-item" href="#">SaleMe Coins <span
                                    class="pull-right">{{(isset($missiondata)AND !is_null($missiondata))?$missiondata->current_score : 0}}</span></a>
                        <a class="dropdown-item" href="{{asset('')}}playzone/rankings" title="{{$level['currentlevel']}}">Level<span class="pull-right">{{$level['currentlevel']}}
                                &nbsp;&nbsp;&nbsp;<img src="{{asset('')}}gamezone/img/ranks/{{strtolower($level['currentlevel'])}}.jpg"
                                        alt="" style="width: 20px" class="pull-right rounded-circle"></span></a>
                        <a class="dropdown-item" href="{{asset('')}}playzone/rankings" title="{{$level['nextLevel']}}">Next Level<span class="pull-right text-muted">{{$level['nextLevel']}}
                                &nbsp;&nbsp;&nbsp;<img src="{{asset('')}}gamezone/img/ranks/{{strtolower($level['nextLevel'])}}.jpg"
                                     alt="" style="width: 20px" class="pull-right rounded-circle"></span></a>
                        <a class="dropdown-item text-right" href="{{asset('')}}playzone/rankings">Goto Rankings page</a>
                        {{--<a class="dropdown-item" href="#">Referral Invites<span class="pull-right">222</span></a>--}}
                        <div class="divider"></div>
                        <a class="dropdown-item" href="{{asset('')}}playzone/profile">
                            User Profile<img
                                    src="{{empty($memberdetails->avatar)? asset('').'images/user.png':$memberdetails->avatar}}"
                                    alt="" style="width: 20px" class="pull-right rounded-circle">
                        </a>
                        <a class="dropdown-item" href="/member_logout_playzone">Logout</a>
                    </div>
                </li>
            </ul>

        </div>
    </div>
</nav>