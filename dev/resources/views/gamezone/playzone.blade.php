@extends('layouts.gamezone')
@section('title', 'SaleMe PlayZone | PlayZone | Play Share Win')
@section('content')
    <?php
    $daily_coin = true;
    $daily_que = true;
    $extra_que = true;
    if (!is_null($missiondata)) {
        if ($missiondata->daily_coin_last_update_at != date('Y-m-d')) {
            $daily_coin = true;
        } else {
            $daily_coin = false;
        }
        if ($missiondata->daily_question_last_update_at != date('Y-m-d')) {
            $daily_que = true;
        } else {
            $daily_que = false;
        }
        if (!is_null($missiondata->extra_question_last_update_at)){
            if (date('Y-m-d', strtotime($missiondata->extra_question_last_update_at . ' + 2 days')) <= date('Y-m-d')) {
                $extra_que = true;
            } else {
                $extra_que = false;
            }
        }else{
            $extra_que = true;
        }
    }
    ?>

    <div class="wrapper">
        <div class="page-header page-header-small play-header">
            <div class="page-header-image" data-parallax="true" style="background-image: url({{asset('')}}images/homepage/main-banner/{{$image->filename}});">
            </div>
            <div class="container">
                <div class="content-center">
                    <h3>SaleMe.lk Play Zone</h3>
                    <h5>Play - Share - Win</h5>

                </div>
            </div>
        </div>
        <div class="section section-notifications" id="notifications">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <div class="alert alert-{{ $msg }}" role="alert">
                        <div class="container">
                            <div class="alert-icon">
                                <i class="now-ui-icons ui-2_like icon"></i>
                            </div>
                            <span class="msg">{{ Session::get('alert-' . $msg) }} </span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">
                                <i class="now-ui-icons ui-1_simple-remove"></i>
                            </span>
                            </button>
                        </div>
                    </div>
                @endif
            @endforeach
            <div class="alert alert_display" role="alert">
                <div class="container">
                    <div class="alert-icon">
                        <i class="now-ui-icons ui-2_like icon"></i>
                    </div>
                    <span class="msg"></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">
                                <i class="now-ui-icons ui-1_simple-remove"></i>
                            </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="section section-team text-center">
            <div class="container">
                <!--<h2 class="title">Here is our team</h2>-->
                <div class="team">
                    <h3 class="text-center">Daily Missions</h3>
                    <div class="clear-fix"></div>
                    <div class="row">
                        <div class="col-md-4">
                            {!! ($daily_coin)?'<a href="#" id="dailypoint">':'' !!}
                            <div class="social-mission collect-daily-mission">
                                <p class="text-center">
                                    {!! ($daily_coin)?'<i class="ion-help-buoy"></i>':'<i class="ion-ios-checkmark-outline"></i>' !!}
                                </p>
                                @if($daily_coin)
                                    <h4 class="text-center ">Collect Daily Coin</h4>
                                @else
                                    <p id="coin" class="demo"></p>
                                    <h4 class="text-center daily_point_added point_added">SaleMe Daily Point Added</h4>
                                @endif
                            </div>
                            {!! ($daily_coin)?'</a>':'' !!}
                        </div>
                        <div class="col-md-4">
                            {!! ($daily_que)?'<a href="'.asset('').'playzone/daily-question" id="dailypoint">':'' !!}
                            <div class="social-mission extra-mission">
                                <p class="text-center">
                                    {!! ($daily_coin)?'<i class="ion-help-buoy"></i>':'<i class="ion-ios-checkmark-outline"></i>' !!}
                                </p>
                                @if($daily_que)
                                    <h4 class="text-center ">Daily Question</h4>
                                @else
                                    <p id="que" class="demo"></p>
                                    <h4 class="text-center daily_point_added point_added">Daily Question Answerd</h4>
                                @endif
                            </div>
                            {!! ($daily_que)?'</a>':'' !!}
                        </div>
                        <div class="col-md-4">
                            {!! ($extra_que)?'<a href="'.asset('').'playzone/extra-question" id="dailypoint">':'' !!}
                            <div class="social-mission daily-mission">
                                <p class="text-center">
                                    {!! ($extra_que)?'<i class="ion-ios-star"></i>':'<i class="ion-ios-checkmark-outline"></i>' !!}
                                </p>
                                @if($extra_que)
                                    <h4 class="text-center ">Extra Question</h4>
                                @else
                                    <p id="extra" class="demo"></p>
                                    <h4 class="text-center daily_point_added point_added">SaleMe Extra Point Added</h4>
                                @endif
                            </div>
                            {!! ($extra_que)?'</a>':'' !!}
                        </div>
                    </div>
                </div>
                <div class="team">
                    <h3 class="text-center">Social Missions</h3>
                    <div class="clear-fix"></div>
                    <div class="row">
                        <div class="col-md-4">
                            <a href="{{asset('')}}playzone/facebook-missions">
                                <div class="social-mission facebook-mission">
                                    <p class="text-center">
                                        <i class="ion-social-facebook"></i>
                                    </p>
                                    <h4 class="text-center">Facebook Missions</h4>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-4">
                            <a href="{{asset('')}}playzone/google-missions">
                                <div class="social-mission google-mission">
                                    <p class="text-center">
                                        <i class="ion-social-googleplus"></i>
                                    </p>
                                    <h4 class="text-center">Google+ Missions</h4>
                                </div>
                            </a>
                        </div>
                        {{--<div class="col-md-3">--}}
                        {{--<a href="#">--}}
                        {{--<div class="social-mission twitter-mission">--}}
                        {{--<p class="text-center">--}}
                        {{--<i class="ion-social-twitter"></i>--}}
                        {{--</p>--}}
                        {{--<h4 class="text-center">Twitter Missions</h4>--}}
                        {{--</div>--}}
                        {{--</a>--}}
                        {{--</div>--}}
                        <div class="col-md-4">
                            <a href="{{asset('')}}playzone/other-social-missions">
                                <div class="social-mission insta-mission">
                                    <p class="text-center">
                                        <i class="ion-social-instagram"></i>
                                    </p>
                                    <h4 class="text-center">Other Social Missions</h4>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="team">
                    <h3 class="text-center">Game Zone</h3>
                    <div class="clear-fix"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="{{asset('')}}playzone/games/cube-champ/play" target="_blank">
                                <div class="social-mission game3-mission text-center">
                                    <img src="{{asset('')}}gamezone/img/cubechamp final.png" alt="" class="img-responsive" style="margin: 0px;width: 250px;">
                                    <h4 class="text-center" style="margin: 0px">Cube Champ</h4>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6">
                            <a href="{{asset('')}}playzone/games/kokis-challenge/play" target="_blank">
                                <div class="social-mission game1-mission text-center">
                                    <img src="{{asset('')}}gamezone/img/banner.png" alt="" class="img-responsive" style="margin: 0px;width: 250px;">
                                    <h4 class="text-center" style="margin: 0px">කොකිස් Challenge</h4>
                                </div>
                            </a>
                        </div>
                        {{--<div class="col-md-6">--}}
                            {{--<a href="#">--}}
                                {{--<div class="social-mission google-mission">--}}
                                    {{--<p class="text-center">--}}
                                        {{--<i class="ion-social-googleplus"></i>--}}
                                    {{--</p>--}}
                                    {{--<h4 class="text-center">Google Plus Missions</h4>--}}
                                {{--</div>--}}
                            {{--</a>--}}
                        {{--</div>--}}

                    </div>
                </div>
            </div>
        </div>
        <!--<div class="section section-contact-us text-center">-->
        <!--<div class="container">-->
        <!--<h2 class="title">Want to work with us?</h2>-->
        <!--<p class="description">Your project is very important to us.</p>-->
        <!--<div class="row">-->
        <!--<div class="col-lg-6 text-center col-md-8 ml-auto mr-auto">-->
        <!--<div class="input-group input-lg">-->
        <!--<span class="input-group-addon">-->
        <!--<i class="now-ui-icons users_circle-08"></i>-->
        <!--</span>-->
        <!--<input type="text" class="form-control" placeholder="First Name...">-->
        <!--</div>-->
        <!--<div class="input-group input-lg">-->
        <!--<span class="input-group-addon">-->
        <!--<i class="now-ui-icons ui-1_email-85"></i>-->
        <!--</span>-->
        <!--<input type="text" class="form-control" placeholder="Email...">-->
        <!--</div>-->
        <!--<div class="textarea-container">-->
        <!--<textarea class="form-control" name="name" rows="4" cols="80" placeholder="Type a message..."></textarea>-->
        <!--</div>-->
        <!--<div class="send-button">-->
        <!--<a href="#pablo" class="btn btn-primary btn-round btn-block btn-lg">Send Message</a>-->
        <!--</div>-->
        <!--</div>-->
        <!--</div>-->
        <!--</div>-->
        <!--</div>-->

@endsection