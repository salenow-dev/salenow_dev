@extends('layouts.gamezone')
@section('title', 'SaleMe PlayZone | Other Social Missions | Play Share Win')
@section('content')
    <style>
        #demo {
            text-align: left;
            color: #111;
            float: right;
            font-size: 20px;
            margin-bottom: 15px;
        }

        .ending {
            text-align: left;
            color: #111;
            float: left;
            margin: 0px;
            font-size: 20px;
            margin-bottom: 15px;
        }

        .team-player img {
            width: 100%;
            margin-bottom: 10px;
        }

        .team-player i {
            background: #fff;
            padding: 5px;
        }

        .iframe {
            min-height: 500px;
            width: 100%;
            margin-bottom: 10px;
        }

        .iframediv {
            background: #eee;
        }

        .title-1 {
            margin-top: 15px;
            font-size: 23px;
            font-weight: 400;
        }

        .earn-txt {
            color: #fea502;
            font-weight: 700;
            font-size: 16px;
            margin: 0;
        }

        .earnd-txt {
            color: #64c136;
            font-weight: 700;
            font-size: 16px;
            margin: 0;
        }

        .radio-btn-div {
            padding-left: 25px;
        }

        /*---------------*/
        .modal-header {
            background: #333;
            padding: 15px 0px !important;
        }

        .modal-title {
            display: block;
            width: 100%;
            margin: 0px;
            color: #eee;
            font-size: 20px;
        }

        .modal-content p {
            margin: 0;
            padding: 10px;
            color: #af2f2f;
            font-weight: 600;
        }

        .modal-body {
            padding: 5px 5px 10px !important;
        }

        .modal-backdrop {
            background: rgb(216, 110, 48) !important;
        }
    </style>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0&appId=1054163521396174&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <div class="wrapper">
        <div class="page-header page-header-small price-header">
            <div class="page-header-image" data-parallax="true" style="background-image: url({{asset('')}}gamezone/img/other-social-bg.jpg);">
            </div>
            <div class="container">
                <div class="content-center">
                    <h3>SaleMe.lk Other Social Missions</h3>
                    <h5>Play - Share - Win</h5>

                </div>
            </div>
        </div>
        <div class="section section-notifications" id="notifications">
            <div class="alert alert_display" role="alert">
                <div class="container">
                    <div class="alert-icon">
                        <i class="now-ui-icons ui-2_like icon"></i>
                    </div>
                    <span class="msg"></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">
                                <i class="now-ui-icons ui-1_simple-remove"></i>
                            </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="section section-team text-center">
            <div class="container">
                <!--<h2 class="title">Here is our team</h2>-->
                <div class="team">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="team-player">
                                {{ Html::image('images/salenow/banner.gif', 'saleme.lk', array('class' => 'img-responsive')) }}
                                <br>
                                {{--like fan page--}}
                                <div class="card">
                                    <ul class="nav nav-tabs nav-tabs-neutral bg-info" role="tablist"
                                        style="background: #55acee !important;"
                                        data-background-color="orange">
                                        <span class="navbar-brand"><i class="ion-social-twitter-outline"
                                                                      style="background: transparent !important;"></i>  Follow SaleMe.lk on Twitter</span>

                                    </ul>
                                    <div class="card-body">
                                        <!-- Tab panes -->
                                        <div class="tab-content text-center">
                                            <div class="tab-pane active">
                                                @if(!is_null($missiondata) AND $missiondata->twitter_follow)
                                                    <p class="pull-right earnd-txt">
                                                        <i class="ion-ios-checkmark-outline"></i> Points Earned
                                                    </p>
                                                    <span class="btn btn-success btn-lg">
                                                        <i class="ion-ios-checkmark-outline"
                                                           style="background: transparent !important;"></i>
                                                        Followed
                                                    </span>
                                                @else
                                                    <p class="pull-right earn-txt"><i class="ion-help-buoy"></i> Earn 5
                                                        Points</p>
                                                    <button type="button" class="btn btn-info btn-lg facebooklike"
                                                            style="background: #55acee !important;">
                                                        <i class="ion-social-twitter"
                                                           style="background: transparent !important;"></i>
                                                        Follow SaleMe.lk google plus
                                                    </button>
                                                @endif

                                                <div id="Div1">
                                                </div>
                                            </div>
                                            <!-- Modal -->
                                            <div id="myModal" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Follow SaleMe.lk on Twitter
                                                                Page</h4>
                                                        </div>
                                                        <p id="mycounter"></p>
                                                        <div class="modal-body">
                                                            <a class="twitter-follow-button btn" style="background: #55acee"
                                                               href="https://twitter.com/SaleMeLKads" target="_blank"
                                                               data-size="large">
                                                                Follow @SaleMe.lk</a>
                                                            <img src="{{asset('')}}gamezone/img/twitter.PNG" alt="" class="img-responsive">
                                                        </div>
                                                        {{--<div class="modal-footer">--}}
                                                        {{--<button type="button" class="btn btn-default pull-right"--}}
                                                        {{--data-dismiss="modal">Close--}}
                                                        {{--</button>--}}
                                                        {{--</div>--}}
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <script>
                                    $('.facebooklike').click(function () {
                                        $("#myModal").modal({backdrop: 'static', keyboard: false});
                                        onTimer();
                                    });
                                </script>
                                <script>
                                    i = 30;
                                    function onTimer() {
                                        document.getElementById('mycounter').innerHTML = 'Wait ' + i + ' Seconds';
                                        i--;
                                        if (i > 0) {
                                            setTimeout(onTimer, 1000);
                                        }
                                        if (i == 1) {
                                            window.location = "/playzone/mission/saveothersocial/twitterfollow";
                                            return true;
                                        }
                                    }
                                </script>
                                {{--END fb like--}}

                                {{--fb Share--}}
                                {{--like fan page--}}
                                <div class="card">
                                    <ul class="nav nav-tabs nav-tabs-neutral bg-info" role="tablist"
                                        style="background: #e4405f !important;"
                                        data-background-color="orange">
                                        <span class="navbar-brand">
                                            <i class="ion-social-instagram" style="background: transparent !important;"></i>  Follow SaleMe.lk on Instagram</span>

                                    </ul>
                                    <div class="card-body">
                                        <!-- Tab panes -->
                                        <div class="tab-content text-center">
                                            <div class="tab-pane active">
                                                @if(!is_null($missiondata) AND $missiondata->instagram_follow)
                                                    <p class="pull-right earnd-txt">
                                                        <i class="ion-ios-checkmark-outline"></i> Points Earned
                                                    </p>
                                                    <span class="btn btn-success btn-lg">
                                                        <i class="ion-ios-checkmark-outline"
                                                           style="background: transparent !important;"></i>
                                                        Shared
                                                    </span>
                                                @else
                                                    <p class="pull-right earn-txt"><i class="ion-help-buoy"></i> Earn 5
                                                        Points</p>
                                                    <button type="button" class="btn btn-info btn-lg fbshare"
                                                            style="background: #e4405f !important;">
                                                        <i class="ion-social-instagram-outline"
                                                           style="background: transparent !important;"></i>
                                                        Follow SaleMe.lk
                                                    </button>
                                                @endif

                                                <div id="Div1">
                                                </div>
                                            </div>
                                            <!-- Modal -->
                                            <div id="share" class="modal fade" role="dialog">
                                                <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Follow SaleMe.lk on Instagram</h4>
                                                        </div>
                                                        <p id="sharecount"></p>
                                                        <div class="modal-body">
                                                            <a href="https://www.instagram.com/saleme.lk/" target="_blank">
                                                                <button class="btn btn-lg" style="background: #e4405f !important;margin-bottom:15px">
                                                                    <i class="ion-social-instagram-outline" style="background: transparent !important;"></i>  Follow
                                                                </button>
                                                                <img src="{{asset('')}}gamezone/img/insta.PNG" alt="" class="img-responsive">
                                                            </a>
                                                        </div>
                                                        <div class="modal-footer">
                                                            {{--<button type="button" class="btn btn-default"--}}
                                                            {{--data-dismiss="modal">Close--}}
                                                            {{--</button>--}}
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <script>
                                    $('.fbshare').click(function () {
                                        $("#share").modal({backdrop: 'static', keyboard: false});
                                        onTimer2();
                                    });
                                </script>
                                <script>
                                    j = 30;
                                    function onTimer2() {
                                        document.getElementById('sharecount').innerHTML = 'Wait ' + j + ' Seconds';
                                        j--;
                                        if (j > 0) {
                                            setTimeout(onTimer2, 1000);
                                        }
                                        if (j == 1) {
                                            window.location = "/playzone/mission/saveothersocial/instagramfollow";
                                            return true;
                                        }
                                    }
                                </script>
                                {{--END fb share--}}

                                {{--share post 1--}}
                                <div class="card">
                                    <ul class="nav nav-tabs nav-tabs-neutral bg-info" role="tablist"
                                        style="background: #0077B5 !important;"
                                        data-background-color="orange">
                                <span class="navbar-brand">
                                <i class="ion-social-linkedin-outline" style="background: transparent !important;"></i>  Follow SaleMe.lk on Linkedin</span>

                                    </ul>
                                    <div class="card-body">
                                        <!-- Tab panes -->
                                        <div class="tab-content text-center">
                                            <div class="tab-pane active">
                                                @if(!is_null($missiondata) AND $missiondata->linkedin_follow)
                                                    <p class="pull-right earnd-txt">
                                                        <i class="ion-ios-checkmark-outline"></i> Points Earned
                                                    </p>
                                                    <span class="btn btn-success btn-lg">
                                                        <i class="ion-ios-checkmark-outline"
                                                           style="background: transparent !important;"></i>
                                                        Followed
                                                        </span>
                                                @else
                                                    <p class="pull-right earn-txt"><i class="ion-help-buoy"></i> Earn 5
                                                        Points</p>
                                                    <button type="button" class="btn btn-info btn-lg youtube"
                                                            style="background: #0077B5 !important;">
                                                        <i class="ion-social-linkedin"
                                                           style="background: transparent !important;"></i>
                                                        Follow SaleMe.lk
                                                    </button>
                                                @endif

                                                <div id="Div1">
                                                </div>
                                            </div>
                                            <!-- Modal -->
                                            <div id="youtube_sub" class="modal fade" role="dialog">
                                                <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Follow SaleMe.lk on Linkedin
                                                                posts</h4>
                                                        </div>
                                                        <p id="youtube"></p>
                                                        <div class="modal-body">
                                                            <script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
                                                            <script type="IN/FollowCompany" data-id="18223121"></script>
                                                            <br>
                                                            <img src="{{asset('')}}gamezone/img/linkedin.PNG" alt="" class="img-responsive">
                                                        </div>
                                                        {{--<div class="modal-footer">--}}
                                                        {{--<button type="button" class="btn btn-default"--}}
                                                        {{--data-dismiss="modal">Close--}}
                                                        {{--</button>--}}
                                                        {{--</div>--}}
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <script>
                                    $('.youtube').click(function () {
                                        $("#youtube_sub").modal({backdrop: 'static', keyboard: false});
                                        onTimer3();
                                    });
                                </script>
                                <script>
                                    i = 30;
                                    function onTimer3() {

                                        document.getElementById('youtube').innerHTML = 'Wait ' + i + ' Seconds';
                                        i--;
                                        if (i > 0) {
                                            setTimeout(onTimer3, 1000);
                                        }
                                        if (i == 1) {
                                            window.location = "/playzone/mission/saveothersocial/linkedin";
                                            return true;
                                        }
                                    }
                                </script>
                                {{--END share post 1--}}
                            </div>
                        </div>
                        <div class="col-md-4 iframediv">
                            <h4 class="title-1">See advertisments on SaleMe.lk</h4>
                            <iframe src="https://saleme.lk/blog/getadstoblog" frameborder="0" class="iframe"></iframe>
                            <br>
                            {{ Html::image('images/salenow/banners/saleme-mobile-banner-gif.gif', 'saleme.lk', array('class' => 'img-responsive visible-xs')) }}
                        </div>
                    </div>

                </div>
            </div>
        </div>

@endsection