<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <link rel="icon" type="image/png" href="{{asset('')}}images/salenow/favicon-16x16.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>SaleMe PlayZone | Enter | profile | Play Share Win | {{ config('app.name') }}</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <!-- CSS Files -->
    <link href="{{ asset("gamezone/css/bootstrap.min.css") }}" rel="stylesheet"/>
    <link href="{{ asset("gamezone/css/now-ui-kit.css") }}" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{ asset("gamezone/css/demo.css") }}" rel="stylesheet"/>
    <link href="{{ asset("gamezone/css/play.css") }}" rel="stylesheet"/>
    {{ Html::style('css/salenow/layout/ionicons/css/ionicons.min.css') }}
    {{ Html::style('css/salenow/layout/linericons/style.min.css') }}
    {{ Html::style('css/salenow/layout/font-awesome-4.7.0/css/font-awesome.min.css') }}
    {{ Html::script('gamezone/js/core/jquery.3.2.1.min.js') }}
    {{ Html::script('gamezone/js/core/popper.min.js') }}
    {{ Html::script('gamezone/js/core/bootstrap.min.js') }}
    <style>
        .input-group {
            border: 0px !important;
        }

        .btn.btn-warning {
            border-radius: 25px 8px 8px 25px;
            font-size: 17px;
        }

        .login-page .link {
            font-size: 16px;
            text-align: center;
        }

        .error {
            color: #e88888;
            display: none;
        }
    </style>
</head>
<body class="login-page sidebar-collapse">
<!-- Navbar -->
<nav class="navbar navbar-expand-lg bg-primary fixed-top navbar-transparent " color-on-scroll="400">
    <div class="container">

        <div class="navbar-translate">
            <a class="navbar-brand" href="{{asset('')}}/ads"
               title="SaleMe Playzone" data-placement="bottom" target="_blank">
                <img class="img-logo" src="{{asset('')}}/gamezone/img/saleme-logo.png" alt="">
            </a>
            <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
                    aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse justify-content-end" id="navigation"
             data-nav-image="{{asset('')}}gamezone/img/blurred-image-1.jpg">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{asset('')}}">Back to SaleMe.lk</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{asset('')}}ads">All Ads</a>
                </li>

            </ul>
        </div>
        {{--<div class="dropdown button-dropdown">--}}
        {{--<a href="#pablo" class="dropdown-toggle" id="navbarDropdown" data-toggle="dropdown">--}}
        {{--<span class="button-bar"></span>--}}
        {{--<span class="button-bar"></span>--}}
        {{--<span class="button-bar"></span>--}}
        {{--</a>--}}
        {{--<div class="dropdown-menu" aria-labelledby="navbarDropdown">--}}
        {{--<a class="dropdown-header">Dropdown header</a>--}}
        {{--<a class="dropdown-item" href="#">Action</a>--}}
        {{--<a class="dropdown-item" href="#">Another action</a>--}}
        {{--<a class="dropdown-item" href="#">Something else here</a>--}}
        {{--<div class="dropdown-divider"></div>--}}
        {{--<a class="dropdown-item" href="#">Separated link</a>--}}
        {{--<div class="dropdown-divider"></div>--}}
        {{--<a class="dropdown-item" href="#">One more separated link</a>--}}
        {{--</div>--}}
        {{--</div>--}}
    </div>
</nav>
<!-- End Navbar -->
<div class="page-header" filter-color="">
    <div class="page-header-image"
         style="background-image:url({{asset('')}}images/homepage/main-banner/{{$image->filename}})"></div>
    <div class="container">
        <div class="row" style="    top: 150px;    ">
            <div class="col-md-6 login-area">
                <div class="card card-login card-plain">
                    {{--<h5 >To play and win with SaleMe, Register Here</h5>--}}
                    <form id="register_form" method="post" role="form" action="/memberregister" class="form">
                        {{csrf_field()}}
                        <input type="hidden" name="location" value="playzone">
                        <div class="content">
                            <div class="input-group form-group-no-border input-lg">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons users_circle-08"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="Name *" name="signup_username"
                                       id="signup_username">
                            </div>
                            <label id="signup_username-error" class="error" for="signup_username">Please enter
                                username</label>
                            <div class="input-group form-group-no-border input-lg">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons ui-1_email-85"></i>
                                </span>
                                <input type="email" placeholder="Email" class="form-control" name="signup_email"
                                       id="signup_email"/>
                            </div>
                            <label id="signup_email-error" class="error" for="signup_email">Please enter
                                username</label>
                            <div class="input-group form-group-no-border input-lg">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons ui-1_lock-circle-open"></i>
                                </span>
                                <input type="password" placeholder="Password*" class="form-control" name="signup_pass"
                                       id="signup_pass"/>
                            </div>
                            <label id="signup_pass-error" class="error" for="signup_pass">Please enter username</label>
                            <div class="input-group form-group-no-border input-lg">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons ui-1_lock-circle-open"></i>
                                </span>
                                <input type="password" placeholder="Confirm Password*" class="form-control"
                                       name="pass_confirmation" id="pass_confirmation"/>
                            </div>
                            <label id="pass_confirmation-error" class="error" for="pass_confirmation">Please enter
                                username</label>
                            <input type="submit" value="Register" class="btn btn-warning btn-block" name="login-submit"
                                   id="btn_submit_signup"/>

                        </div>
                        <div class="footer text-center">

                        </div>
                        <div class="pull-right">
                            <h6>
                                <a href="{{asset('')}}playzone/start/" class="link">Login here</a>
                            </h6>
                        </div>

                    </form>
                </div>
            </div>
        </div>


    </div>
    <footer class="footer">
        <div class="container">
            <nav>
                <ul>
                    <li>
                        <a href="{{asset('')}}" target="_blank">
                            Back to SaleMe.lk
                        </a>
                    </li>
                    <li>
                        <a href="{{asset('')}}ads/" target="_blank">
                            All Ads
                        </a>
                    </li>
                    <li>
                        <a href="{{asset('')}}terms-conditions" target="_blank">
                            Terms & Conditions
                        </a>
                    </li>
                    <li>
                        <a href="{{asset('')}}privacy-policy" target="_blank">
                            Privacy & Policies
                        </a>
                    </li>
                </ul>
            </nav>
            <div class="copyright">
                © 2018 <a href="{{asset('')}}" target="_blank">SaleMe.lk (Pvt) Ltd</a>. All right reserved.
            </div>
        </div>
    </footer>
</div>
</body>

{{ Html::script('gamezone/js/plugins/bootstrap-switch.js') }}
{{ Html::script('gamezone/js/plugins/nouislider.min.js') }}
{{ Html::script('gamezone/js/plugins/bootstrap-datepicker.js') }}
{{ Html::script('gamezone/js/now-ui-kit.js') }}
{{ Html::script('gamezone/js/gamezone-custom.js') }}
{{ Html::script('js/salenow/plugin/jquery.validate.js') }}
<script>
    // login form
    $("#memberlogin_form").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
            }
        }
    });

    //member register
    $("#register_form").validate({
        rules: {
            signup_username: {
                required: true,
            },
            signup_phone: {
                number: true,
                required: true,
            },
            signup_email: {
                required: true,
                email: true
            },
            signup_pass: {
                required: true,
            },
            pass_confirmation: {
                required: true,
                equalTo: "#signup_pass"
            }
        },
        messages: {
            signup_username: {
                required: "Please enter username",
            },
            signup_phone: {
                required: "Please enter mobile number",
            },
            signup_email: {
                required: "Please enter email",
            },
            signup_pass: {
                required: "Please enter password",
            },
            pass_confirmation: {
                equalTo: "does not match password",
            }
        }
    });

</script>

</html>