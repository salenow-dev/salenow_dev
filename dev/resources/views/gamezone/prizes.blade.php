@extends('layouts.gamezone')
@section('title', 'SaleMe PlayZone | SaleMe Prizes | Play Share Win')
@section('content')
    <style>
        .team .row{
            margin-bottom: 25px;
            border-bottom: 1px solid #eee;
            padding-bottom: 15px;
        }
    </style>
    <div class="wrapper">
        <div class="page-header page-header-small price-header">
            <div class="page-header-image" data-parallax="true" style="background-image: url({{asset('')}}gamezone/img/prizes.jpg);">
            </div>
            <div class="container">
                <div class="content-center">
                    <h3>SaleMe.lk Prizes</h3>
                    <h5>Play - Share - Win</h5>

                </div>
            </div>
        </div>

        <div class="section section-team text-center">
            <div class="container">
                <!--<h2 class="title">Here is our team</h2>-->
                <div class="team">
                    @if(count($prizes))
                        @foreach(array_chunk($prizes, 4) as $price_row)
                        <div class="row">
                            @foreach($price_row as $price)
                            <div class="col-md-3">
                                <div class="team-player">
                                    <img src="{{asset('')}}gamezone/img/store/{{$price['image']}}" alt="Thumbnail Image" class="">
                                    <h4 class="title">{{$price['title']}}</h4>
                                    <p class="category text-primary">{{($price['min_point'])?$price['min_point'].' Coins' : 'LKR '.$price['price']}}</p>
                                    <a href="{{asset('')}}playzone/item/{{$price['id']}}">
                                        <button class="btn btn-primary btn-round" type="button">Redeem Now</button>
                                    </a>
                                </div>
                            </div>
                            @endforeach

                        </div>
                        @endforeach
                    @endif

                </div>
            </div>
        </div>
        <!--<div class="section section-contact-us text-center">-->
        <!--<div class="container">-->
        <!--<h2 class="title">Want to work with us?</h2>-->
        <!--<p class="description">Your project is very important to us.</p>-->
        <!--<div class="row">-->
        <!--<div class="col-lg-6 text-center col-md-8 ml-auto mr-auto">-->
        <!--<div class="input-group input-lg">-->
        <!--<span class="input-group-addon">-->
        <!--<i class="now-ui-icons users_circle-08"></i>-->
        <!--</span>-->
        <!--<input type="text" class="form-control" placeholder="First Name...">-->
        <!--</div>-->
        <!--<div class="input-group input-lg">-->
        <!--<span class="input-group-addon">-->
        <!--<i class="now-ui-icons ui-1_email-85"></i>-->
        <!--</span>-->
        <!--<input type="text" class="form-control" placeholder="Email...">-->
        <!--</div>-->
        <!--<div class="textarea-container">-->
        <!--<textarea class="form-control" name="name" rows="4" cols="80" placeholder="Type a message..."></textarea>-->
        <!--</div>-->
        <!--<div class="send-button">-->
        <!--<a href="#pablo" class="btn btn-primary btn-round btn-block btn-lg">Send Message</a>-->
        <!--</div>-->
        <!--</div>-->
        <!--</div>-->
        <!--</div>-->
        <!--</div>-->

@endsection