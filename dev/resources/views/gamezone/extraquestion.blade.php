@extends('layouts.gamezone')
@section('title', 'SaleMe PlayZone | Extra Question | Play Share Win')
@section('content')
    <style>
        #demo {
            text-align: left;
            color: #111;
            float: right;
            font-size: 20px;
            margin-bottom: 15px;
        }

        .ending {
            text-align: left;
            color: #111;
            float: left;
            margin: 0px;
            font-size: 20px;
            margin-bottom: 15px;
        }

        .team-player img {
            width: 100%;
            margin-bottom: 10px;
        }

        .team-player i {
            background: #eee;
            padding: 5px 13px;
        }

        .iframe {
            min-height: 500px;
            width: 100%;
            margin-bottom: 10px;
        }

        .iframediv {
            background: #eee;
        }

        .title-1 {
            margin-top: 15px;
            font-size: 23px;
            font-weight: 400;
        }

        .radio-btn-div {
            padding-left: 25px;
        }
    </style>
    <div class="wrapper">
        <div class="page-header page-header-small price-header">
            <div class="page-header-image" data-parallax="true" style="background-image: url({{asset('')}}gamezone/img/question1.jpg);">
            </div>
            <div class="container">
                <div class="content-center">
                    <h3>SaleMe.lk Extra Questions</h3>
                    <h5>Play - Share - Win</h5>

                </div>
            </div>
        </div>
        <div class="section section-notifications" id="notifications">
            <div class="alert alert_display" role="alert">
                <div class="container">
                    <div class="alert-icon">
                        <i class="now-ui-icons ui-2_like icon"></i>
                    </div>
                    <span class="msg"></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">
                                <i class="now-ui-icons ui-1_simple-remove"></i>
                            </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="section section-team text-center">
            <div class="container">
                <!--<h2 class="title">Here is our team</h2>-->
                <div class="team">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="team-player">
                                <p class="ending">
                                    <i class="ion-social-buffer-outline"></i>&nbsp;Mission Ending in :</p>
                                <p id="coin" class="demo text-info"></p>
                                {{ Html::image('images/salenow/banner.gif', 'saleme.lk', array('class' => 'img-responsive')) }}
                                <br>
                                <img src="{{asset('')}}gamezone/img/extraquestions/{{$question->image}}"
                                     alt="Thumbnail Image"
                                     class="img-responsive">
                                <h3 class="title-1 text-left">{{$question->title}}</h3>
                                <div class="text-left radio-btn-div">
                                    @if(!empty($question->answer1))
                                        <div class="radio">
                                            <input type="radio" name="radio1" id="radio1" value="option1" checked="">
                                            <label for="radio1">
                                                <b>{{$question->answer1}}</b>
                                            </label>
                                        </div>
                                    @endif
                                    @if(!empty($question->answer2))
                                        <div class="radio">
                                            <input type="radio" name="radio1" id="radio2" value="option1">
                                            <label for="radio2">
                                                <b>{{$question->answer2}}</b>
                                            </label>
                                        </div>
                                    @endif
                                    @if(!empty($question->answer3))
                                        <div class="radio">
                                            <input type="radio" name="radio1" id="radio3" value="option1">
                                            <label for="radio3">
                                                <b>{{$question->answer3}}</b>
                                            </label>
                                        </div>
                                    @endif
                                    <a href="#" class="btn btn-primary btn-round" id="answer_btn_extra">Answer</a>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4 iframediv">
                            <h4 class="title-1">See advertisments on SaleMe.lk</h4>
                            <iframe src="https://saleme.lk/blog/getadstoblog" frameborder="0" class="iframe"></iframe>
                            <br>
                            {{ Html::image('images/salenow/banners/saleme-mobile-banner-gif.gif', 'saleme.lk', array('class' => 'img-responsive visible-xs')) }}
                        </div>
                    </div>

                </div>
            </div>
        </div>

@endsection