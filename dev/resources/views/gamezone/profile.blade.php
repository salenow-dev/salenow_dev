@extends('layouts.gamezone')
@section('title', 'SaleMe PlayZone | '.$memberdetails->first_name.' profile | Play Share Win')
@section('content')
    <style>
        .error {
            display: none;
            font-size: 12px;
            color: #e42e2e;
        }

        #edit_details .col-sm-9 {
            margin-bottom: 15px;
            text-align: left;
        }

        #edit_details .input-group {
            margin-bottom: 0px !important;
        }

        .clear-fix {
            clear: both;
        }

        .status {
            position: absolute;
            top: 0;
            right: 15px;
        }
    </style>

    <div class="wrapper">
        <div class="page-header page-header-small" filter-color="orange">
            <div class="page-header-image" data-parallax="true"
                 style="background-image: url({{asset('')}}images/homepage/main-banner/{{$image->filename}});">
            </div>
            <div class="container">
                <div class="content-center">
                    <div class="photo-container">
                        <img src="{{empty($memberdetails->avatar)? asset('').'images/user.png':$memberdetails->avatar}}"
                             alt="">
                    </div>
                    <h3 class="title">{{$memberdetails->first_name}} {{$memberdetails->last_name}}</h3>
                    <!--<p class="category">Photographer</p>-->
                    <div class="content">
                        <div class="social-description">
                            <h2>
                                <i class="ion-help-buoy home-head-icon"></i> {{!is_null($missiondata)?$missiondata->current_score : 0}}
                            </h2>
                            <p>SaleMe Coins</p>
                        </div>
                        <div class="social-description">
                            <h2 style="color: #fea502;">{{$level['currentlevel']}}</h2>
                            <p>Level</p>
                        </div>
                        <div class="social-description">
                            <h4 class="text-muted"><i class="ion-ios-arrow-thin-right"></i> {{$level['nextLevel']}}</h4>
                            <p>Next Level</p>
                        </div>
                        {{--<div class="social-description">--}}
                        {{--<h2><i class="ion-ios-chatbubble-outline"></i> 26</h2>--}}
                        {{--<p>Referral Invites</p>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
        <div class="section-notifications" id="notifications">
            <div class="alert alert_display" role="alert">
                <div class="container">
                    <div class="alert-icon">
                        <i class="now-ui-icons ui-2_like icon"></i>
                    </div>
                    <span class="msg"></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">
                                <i class="now-ui-icons ui-1_simple-remove"></i>
                            </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="section section-tabs">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12 ml-auto mr-auto">
                        <!--<p class="category">Tabs with Icons on Card</p>-->
                        <!-- Nav tabs -->
                        <div class="card">
                            <ul class="nav nav-tabs justify-content-center" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#home" role="tab">
                                        <i class="ion-person"></i> About Me
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#profile" role="tab">
                                        <i class="ion-edit"></i> Edit Details
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#messages" role="tab">
                                        <i class="now-ui-icons shopping_cart-simple"></i> Purchase History
                                    </a>
                                </li>
                                <!--<li class="nav-item">-->
                                <!--<a class="nav-link" data-toggle="tab" href="#settings" role="tab">-->
                                <!--<i class="now-ui-icons ui-2_settings-90"></i> Settings-->
                                <!--</a>-->
                                <!--</li>-->
                            </ul>
                            <div class="card-body">
                                <!-- Tab panes -->
                                <div class="tab-content text-center">
                                    <div class="tab-pane active" id="home" role="tabpanel">
                                        <div class="col-md-4 ml-auto mr-auto">
                                            {{--<div class="nav-align-center">--}}
                                            <img src="{{empty($memberdetails->avatar)? asset('').'images/user.png':$memberdetails->avatar}}"
                                                 alt="" class="rounded-circle img-responsive" width="120px">
                                            {{--</div>--}}
                                        </div>
                                        <div class="row" style="background: #eee; margin-top: 18px; padding-top: 15px;">
                                            <div class="col-md-5">
                                                <h5 class="" style="margin: 0px">Current Level</h5>
                                                {{--<br>--}}
                                                <img class="rounded-circle img-responsive"
                                                     src="{{asset('')}}gamezone/img/ranks/{{strtolower($level['currentlevel'])}}.jpg"
                                                     alt="" style="width: 100px">
                                                <br>
                                                <h5 style="margin-top: 10px;margin-bottom: 0px"><span
                                                            class="badge badge-success">{{$level['currentlevel']}}</span>
                                                </h5><br>

                                            </div>
                                            <div class="col-md-2">
                                                <img class="img-responsive"
                                                     src="{{asset('')}}gamezone/img/ranks/arrow.png"
                                                     alt="" style="width: 100px">
                                                <br>
                                                <p class="text-info" style="font-size: 14px; margin-top: 20px;">
                                                    <b>
                                                        <a href="{{asset('')}}playzone/rankings" class="text-primary">Goto
                                                            Ranking Page</a>
                                                    </b>
                                                </p>


                                            </div>
                                            <div class="col-md-5">
                                                <h5 class="" style="margin: 0px">Next Level</h5>
                                                <img class="rounded-circle img-responsive"
                                                     src="{{asset('')}}gamezone/img/ranks/{{strtolower($level['nextLevel'])}}.jpg"
                                                     alt="" style="width: 100px">
                                                <br>
                                                <h5 style="margin-top: 10px;margin-bottom: 0px"><span
                                                            class="badge badge-info">{{$level['nextLevel']}}</span>
                                                </h5><br>
                                            </div>
                                        </div>
                                        <h5 class="name">
                                            {{$memberdetails->first_name}} {{$memberdetails->last_name}}
                                        </h5>

                                        <h5 class="address">
                                            <span class="text-muted">Email : </span> <br>
                                            <span>{{$memberdetails->email}}</span>
                                        </h5>
                                        @if(!is_null($playzone_member))
                                            <h5 class="address">
                                                <span class="text-muted">Address : </span> <br>
                                                <span>{{$playzone_member->address}}</span> <br>
                                                <span>{{$playzone_member->post_code}}</span> <br>
                                            </h5>
                                        @endif
                                        @if(!is_null($playzone_member))
                                            <h5 class="address">
                                                <span class="text-muted">Phone : </span> <br>
                                                <span>{{($playzone_member->mobile)?$playzone_member->mobile :''}}</span>
                                                <br>
                                            </h5>
                                        @endif
                                        @if(!is_null($playzone_member))
                                            <h5 class="address">
                                                <span class="text-muted">Gender :</span> {{ucwords($playzone_member->gender)}}
                                            </h5>
                                        @endif

                                        @if(!is_null($playzone_member))
                                            <h5 class="address">
                                                <span class="text-muted">Date Of Birth : </span>
                                                <span>{{date('l jS F Y', strtotime($playzone_member->dob))}}</span>
                                            </h5>
                                        @endif


                                        {{--<h5 class="address">--}}
                                        {{--<span class="text-muted">T Shirt Size : </span>--}}
                                        {{--<span>Extra Large</span>--}}
                                        {{--</h5>--}}

                                        <button class="btn btn-primary btn-simple btn-round" type="button"
                                                data-toggle="tab"
                                                href="#profile" role="tab">Edit My Details
                                        </button>


                                    </div>
                                    <div class="tab-pane" id="profile" role="tabpanel">
                                        <div class="col-md-6 ml-auto mr-auto">
                                            <form id="edit_details">
                                                <div class="row">
                                                    <div class="col-sm-3 col-lg-3 col-xs-12"
                                                         style="    line-height: 2.5;">
                                                        Name
                                                    </div>
                                                    <div class="col-sm-9 col-lg-9 col-xs-12">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="name"
                                                                   name="name" placeholder="Name"
                                                                   value="{{ !is_null($playzone_member)?$playzone_member->member->first_name : '' }}">
                                                        </div>
                                                        <label id="name-error" class="error" for="name"></label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-3 col-lg-3 col-xs-12"
                                                         style="    line-height: 2.5;">
                                                        E Mail
                                                    </div>
                                                    <div class="col-sm-9 col-lg-9 col-xs-12">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control"
                                                                   value="{{ !is_null($playzone_member)?$playzone_member->email : '' }}"
                                                                   placeholder="email" readonly>
                                                        </div>
                                                        <label id="email-error" class="error" for="email"></label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-3 col-lg-3 col-xs-12"
                                                         style="    line-height: 2.5;">
                                                        Phone
                                                    </div>
                                                    <div class="col-sm-9 col-lg-9 col-xs-12">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="mobile"
                                                                   name="mobile"
                                                                   value="{{ !is_null($playzone_member)?$playzone_member->mobile : '' }}"
                                                                   placeholder="Name">
                                                        </div>
                                                        <label id="mobile-error" class="error" for="mobile"></label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-3 col-lg-3 col-xs-12"
                                                         style="    line-height: 2.5;">
                                                        Gender
                                                    </div>
                                                    <div class="col-sm-9 col-lg-9 col-xs-12">
                                                        <div class="input-group" style="border: 0px">
                                                            <div class="radio">
                                                                <input type="radio" name="gender" id="radio1"
                                                                       value="male" {{ (is_null($playzone_member))? 'checked':(!is_null($playzone_member) AND $playzone_member->gender =='male')? 'checked' : '' }}>
                                                                <label for="radio1">
                                                                    Male
                                                                </label>
                                                            </div>
                                                            <div class="radio">
                                                                <input type="radio" name="gender" id="radio2"
                                                                       value="female" {{ (!is_null($playzone_member) AND $playzone_member->gender =='female')? 'checked' : '' }}>
                                                                <label for="radio2">
                                                                    Female
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-3 col-lg-3 col-xs-12"
                                                         style="    line-height: 2.5;">
                                                        Address
                                                    </div>
                                                    <div class="col-sm-9 col-lg-9 col-xs-12">
                                                        <div class="input-group">
                                                                <textarea class="form-control" name="address"
                                                                          id="address"
                                                                          placeholder="Address">{{ !is_null($playzone_member)?$playzone_member->address : '' }}</textarea>
                                                        </div>
                                                        <label id="address-error" class="error" for="address"></label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-3 col-lg-3 col-xs-12"
                                                         style="    line-height: 2.5;">
                                                        Postal Code
                                                    </div>
                                                    <div class="col-sm-9 col-lg-9 col-xs-12">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" name="post_code"
                                                                   id="post_code" placeholder="Postal Code"
                                                                   value="{{ !is_null($playzone_member)?$playzone_member->post_code : '' }}">
                                                        </div>
                                                        <label id="post_code-error" class="error"
                                                               for="post_code"></label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-3 col-lg-3 col-xs-12"
                                                         style="    line-height: 2.5;">
                                                        Date of Birth
                                                    </div>
                                                    <div class="col-sm-9 col-lg-9 col-xs-12">
                                                        <div class="input-group">
                                                            <input type="date" class="form-control" name="dob" id="dob"
                                                                   value="{{ (!is_null($playzone_member) AND !is_null($playzone_member->dob))?$playzone_member->dob : date('Y-m-d') }}">
                                                            {{--<input type="text" class="form-control date-picker"--}}
                                                            {{--data-datepicker-color="primary">--}}
                                                        </div>
                                                        <label id="dob-error" class="error" for="dob"></label>
                                                    </div>
                                                </div>
                                                {{--</div>--}}

                                                {{--<div class="row">--}}
                                                {{--<div class="col-sm-3 col-lg-3 col-xs-12"--}}
                                                {{--style="    line-height: 2.5;">--}}
                                                {{--T Shirt Size--}}
                                                {{--</div>--}}
                                                {{--<div class="col-sm-9 col-lg-9 col-xs-12">--}}
                                                {{--<div class="input-group">--}}
                                                {{--<select name="" id="" class="form-control">--}}
                                                {{--<option value="">Select</option>--}}
                                                {{--</select>--}}

                                                {{--</div>--}}
                                                {{--</div>--}}
                                                {{--</div>--}}
                                                <div class="row">
                                                    <div class="col-sm-3 col-lg-3 col-xs-12"
                                                         style="    line-height: 2.5;">

                                                    </div>
                                                    <div class="col-sm-9 col-lg-9 col-xs-12">
                                                        <input type="submit" value="Save"
                                                               class="btn btn-warning btn-round pull-right">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="tab-pane " id="messages" role="tabpanel">
                                        <ul class="my-toggle">
                                            @if(count($purchase))
                                                @foreach($purchase as $item)
                                                    <li>
                                                        <a class="view">
                                                            <div class="row">
                                                                <div class="col-md-2 col-sm-2 col-lg-2">
                                                                    <img src="{{asset('')}}gamezone/img/store/{{$item->image}}"
                                                                         alt="Rounded Image"
                                                                         class="rounded">
                                                                </div>
                                                                <div class="col-md-10 col-sm-10 col-lg-10">
                                                                    <h3 class="toggle-title">{{$item->title}}</h3>
                                                                    @if(!$item->is_prize)
                                                                        <p class="text-left">Price : LKR
                                                                            <b>{{$item->price}}</b> <span
                                                                                    class="badge badge-info">Purchase</span>
                                                                        </p>
                                                                    @else
                                                                        <p class="text-left">Redeem Point :
                                                                            <b>{{$item->min_point}}</b> SaleMe Coins
                                                                            <span class="badge badge-primary">Redeem</span>
                                                                        </p>
                                                                    @endif
                                                                    <p class="text-left">Orded Date :
                                                                        <b>{{date('l jS F Y', strtotime($item->created_at))}}</b>
                                                                    </p>
                                                                    <p class="text-left"><b>Click To show Shipping
                                                                            Details</b></p>
                                                                    @if($item->status == 'ignored')
                                                                        <span class="badge badge-danger status">Danger</span>
                                                                    @elseif($item->status == 'shiped')
                                                                        <span class="badge badge-success status">Shipped</span>
                                                                    @elseif($item->status == 'ship_qued')
                                                                        <span class="badge badge-primary status">Shipping Queue</span>
                                                                    @elseif($item->status == 'delivered')
                                                                        <span class="badge badge-info status">Delivered</span>
                                                                    @elseif($item->status == 'pending')
                                                                        <span class="badge badge-warning status">Pending</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <div class="detail">
                                                            <div class="row">
                                                                <!--<div class="col-sm-12">-->
                                                                <!--<div class="toggle-wrapper">-->
                                                                <div class="col-md-6">
                                                                    <p class="text-left margin-bottom-0">Address : </p>
                                                                    <p class="text-left">{{$item->address}}</p>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    @if($item->status == 'shiped')
                                                                        <p class="text-left margin-bottom-0">Shipped on
                                                                            : </p>
                                                                        <p class="text-left">{{date('l jS F Y', strtotime($item->shiped_date))}}</p>
                                                                    @endif
                                                                </div>

                                                                <!--</div>-->
                                                                <!--</div>-->
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            @else
                                                <li>
                                                    <div class="view">
                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12 col-lg-12">
                                                                <h2 class="toggle-title text-center">No Purchase history
                                                                    to show</h2>
                                                                <a href="{{asset('')}}playzone/store">
                                                                    <button class="btn btn-primary">Keep Purchasing
                                                                    </button>

                                                                </a>
                                                                <div class="clear-fix"></div>
                                                            </div>
                                                            <div class="clear-fix"></div>
                                                        </div>
                                                        <div class="clear-fix"></div>
                                                    </div>

                                                </li>
                                            @endif

                                        </ul>
                                    </div>
                                    <!--<div class="tab-pane" id="settings" role="tabpanel">-->
                                    <!--<p>-->
                                    <!--"I will be the leader of a company that ends up being worth billions of dollars, because I got the answers. I understand culture. I am the nucleus. I think that’s a responsibility that I have, to push possibilities, to show people, this is the level that things could be at."-->
                                    <!--</p>-->
                                    <!--</div>-->
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <script type="text/javascript" src="{{ asset("plugins/blockui/jquery.blockUI.js")}}"></script>
        {{ Html::script('js/salenow/plugin/jquery.validate.js') }}

        <script>
            $(document).ready(function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $("#edit_details").validate({
                    ignore: ":hidden",
                    rules: {
                        name: {
                            required: true,
                        },
                        email: {
                            required: true,
                        },
                        mobile: {
                            required: true,
                            number: true,
                            maxlength: 10,
                            minlength: 10
                        },
                        address: {
                            required: true,
                        },
                        gender: {
                            required: true,
                        },
                        dob: {
                            required: true,
                        },
                        post_code: {
                            required: true,
                            number: true,
                            maxlength: 5,
                            minlength: 5
                        }
                    },
                    messages: {
                        name: {
                            required: "Please fill your Name",
                        },
                        email: {
                            required: "Please fill your Email address",
                        },
                        mobile: {
                            required: "Please fill your mobile Number",
                        },
                        address: {
                            required: "Please fill Address",
                        },
                        post_code: {
                            required: "Please fill Postal Code",
                        },
                        dob: {
                            required: "Please fill date of birth",
                        }
                    },
                    submitHandler: function (form) {
                        var formdata = $("#edit_details").serializeArray();
                        $.ajax({
                            type: 'POST',
                            url: '/playzone/user-details/save/{{$memberdetails->memberid}}',
                            data: formdata,
                            beforeSend: function (d) {
                                $.blockUI({
                                    message: '<img src="{{asset('')}}gamezone/img/animat-lightbulb-color.gif" width="100px"/><h1 style="font-size:20px !important;"> Just a moment...</h1>',
                                    css: {
                                        backgroundColor: '#fff',
                                        color: '#222',
                                        border: '0px',
                                        'border-radius': '5px',
                                        'opacity': 1
                                    }
                                });
                            },
                            success: function (res) {
                                $.unblockUI();
                                $('.alert_display').show().addClass(res.css);
                                $('.icon').addClass(res.icon);
                                $('.alert_display .msg').html(res.message);
                                setTimeout(function () {
                                    $('.redeem-now-btn').attr('disabled', 'disabled');
                                }, 500);
                                setTimeout(function () {
                                    window.scrollTo(0, 0);
                                }, 1000);
                                setTimeout(function () {
                                    location.reload();
                                }, 3000);
                            }
                        });
                    }
                });


            });
        </script>
@endsection