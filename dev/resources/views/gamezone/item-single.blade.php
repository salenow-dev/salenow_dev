@extends('layouts.gamezone')
@section('title', 'SaleMe PlayZone | '.$item->title.' | Play Share Win')
@section('content')
    <style>
        .buy-now-div {
            background-color: #eee;
            width: 100%;
            padding: 15px;
        }

        .buy-now-div .qty {
            background: #fff;
            padding: 10px;
            margin-left: 15px;
            border: 1px solid #ddd;
            border-radius: 5px;
        }

        .redeem-points {
            margin: 0px;
            padding-top: 15px;
        }

        .redeem-points b {
            color: #fea502;
        }

        .redeem-price {
            margin: 0px;
            padding-top: 15px;
            color: #c15701;
            font-weight: 700;
            font-size: 16px;
        }

        .section {
            padding: 35px 0 70px;
        }

        .clear-fix {
            clear: both;
        }

        #redeem-form {
            display: none;
        }

        .redeem-class {
            animation: colorchange 2s;
        }

        .redeem-class-remove {
            animation: colorchange 2s;
        }

        @keyframes colorchange {
            0% {
                border: 5px solid #229922;
            }
            100% {
                border: 5px solid #eee;
            }
        }

        .error {
            display: none;
            font-size: 12px;
            color: #e42e2e;
        }

        #redeemform .col-sm-9 {
            margin-bottom: 15px;
        }

        #redeemform .input-group {
            margin-bottom: 0px !important;
        }

        #alert_display {
            display: block;
        }

        .outofstock {
            background: #db3f3f;
            padding: 10px;
            border-radius: 5px;
            color: #fff;
        }
    </style>
    <div class="wrapper">
        <div class="page-header page-header-small store-header">
            <div class="page-header-image" data-parallax="true" style="background-image: url({{asset('')}}gamezone/img/prizes.jpg);">
            </div>
            <div class="container">
                <div class="content-center">
                    <h3>SaleMe.lk Online Store</h3>
                    <h5>Play - Share - Win</h5>

                </div>
            </div>
        </div>
        <div class="section-notifications" id="notifications">
            <div class="alert alert_display" role="alert">
                <div class="container">
                    <div class="alert-icon">
                        <i class="now-ui-icons ui-2_like icon"></i>
                    </div>
                    <span class="msg"></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">
                                <i class="now-ui-icons ui-1_simple-remove"></i>
                            </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="section-sponser section-team text-center">
            <img src="{{asset('')}}images/salenow/banner.gif" class="img-responsive" alt="saleme.lk">
        </div>

        <div class="section section-team text-center">
            <div class="container">
                <!--<h2 class="title">Here is our team</h2>-->
                <div class="team">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="team-player">
                                <img src="{{asset('')}}gamezone/img/store/{{$item->image}}" alt="Thumbnail Image"
                                     class="img-responsive" style="width: 100%">

                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="team-player text-left">
                                <h4 class="title text-left">{{$item->title}}</h4>
                                <p class="category text-primary text-left">{{$item->description}}</p>

                                <div class="buy-now-div">
                                    <div>
                                        <lable>Quantity :</lable>
                                        <input type="number" class="qty" min="1" value="1" max="{{$item->qty}}">
                                        <span> / {{$item->qty}} Available</span>
                                        <p class="redeem-price">Buy for LKR <b>{{$item->price}}.00</b></p>
                                        @if($item->is_prize)
                                            <p class="redeem-points">Redeem for <b>{{$item->min_point}}</b> Saleme Coins
                                            </p>
                                        @endif

                                    </div>
                                    <br>
                                    @if($item->qty > 0)
                                        @if($missiondata->current_score < $item->min_point )
                                            <div class="text-danger">Not enough SaleMe Coins. Play and collect more coins</div>
                                        @endif
                                        {{--<button class="btn btn-primary pull-right btn-success" type="button">Buy Now--}}
                                        {{--</button>--}}
                                        @if($item->is_prize)
                                            <button class="btn btn-primary btn-warning pull-right redeem-now-btn"
                                                    type="button"
                                                    {{($missiondata->current_score >= $item->min_point )?'onclick=redeemPrize('.$item->id.')':'disabled'}} >
                                                Redeem Now
                                            </button>
                                        @endif
                                    @else
                                        <div class="outofstock">Out of Stock. Please wait for next restock</div>
                                    @endif
                                    <div class="clear-fix"></div>
                                </div>
                                @if($item->is_prize)
                                    @if($missiondata->current_score >= $item->min_point)
                                        {{--order div--}}
                                        <div>
                                            <br>
                                            <div class="buy-now-div" id="redeem-form">
                                                <div>
                                                    <p>Fill your contact Details bellow</p>
                                                    <form id="redeemform"
                                                          method="post">
                                                        {!! csrf_field() !!}
                                                        <div class="row">
                                                            <div class="col-sm-3 col-lg-3 col-xs-12"
                                                                 style="    line-height: 2.5;">
                                                                Name
                                                            </div>
                                                            <div class="col-sm-9 col-lg-9 col-xs-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="name"
                                                                           id="name"
                                                                           placeholder="Name"
                                                                           value="{{ count($memberdetails)?$memberdetails->first_name :'' }}">
                                                                </div>
                                                                <label id="name-error" class="error" for="name"></label>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-3 col-lg-3 col-xs-12"
                                                                 style="    line-height: 2.5;">
                                                                E Mail
                                                            </div>
                                                            <div class="col-sm-9 col-lg-9 col-xs-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="email"
                                                                           id="email"
                                                                           placeholder="Email"
                                                                           value="{{ count($memberdetails)?$memberdetails->email :'' }}">
                                                                </div>
                                                                <label id="email-error" class="error"
                                                                       for="email"></label>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-3 col-lg-3 col-xs-12"
                                                                 style="    line-height: 2.5;">
                                                                Address
                                                            </div>
                                                            <div class="col-sm-9 col-lg-9 col-xs-12">
                                                                <div class="input-group">
                                                            <textarea class="form-control" name="address" id="address"
                                                                      placeholder="Address">{{ (isset($playzonemember) AND !is_null($playzonemember->address))?$playzonemember->address:''}}</textarea>
                                                                </div>
                                                                <label id="address-error" class="error"
                                                                       for="address"></label>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-3 col-lg-3 col-xs-12"
                                                                 style="    line-height: 2.5;">
                                                                Post Code
                                                            </div>
                                                            <div class="col-sm-9 col-lg-9 col-xs-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control"
                                                                           name="post_code"
                                                                           id="post_code"
                                                                           placeholder="Post Code"
                                                                           value="{{ (isset($playzonemember) AND !is_null($playzonemember->post_code))?$playzonemember->post_code:''}}">
                                                                </div>
                                                                <label id="post_code-error" class="error"
                                                                       for="post_code"></label>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-3 col-lg-3 col-xs-12"
                                                                 style="    line-height: 2.5;">
                                                                Mobile
                                                            </div>
                                                            <div class="col-sm-9 col-lg-9 col-xs-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control"
                                                                           name="mobile"
                                                                           id="mobile"
                                                                           placeholder="Mobile"
                                                                           value="{{ (isset($playzonemember) AND !is_null($playzonemember->mobile))?$playzonemember->mobile:''}}">
                                                                </div>
                                                                <label id="mobile-error" class="error"
                                                                       for="mobile"></label>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-3 col-lg-3 col-xs-12"
                                                                 style="    line-height: 2.5;">

                                                            </div>
                                                            <div class="col-sm-9 col-lg-9 col-xs-12">
                                                                <input type="submit" class="btn btn-success pull-right"
                                                                       value="Continue" id="redeem-btn">
                                                            </div>
                                                        </div>
                                                    </form>

                                                </div>

                                                <div class="clear-fix"></div>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                                {{--END order div--}}

                            </div>
                        </div>
                        <div class="col-md-4">
                            <img src="{{asset('')}}images/salenow/banners/squre-desktop.jpg" class="" alt="saleme.lk">
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            {{--<div class="team-player">--}}
                            {{--<img src="{{asset('')}}gamezone/img/store/{{$item->image}}" alt="Thumbnail Image"--}}
                            {{--class="img-responsive" style="width: 100%">--}}

                            {{--</div>--}}
                        </div>
                        <div class="col-md-9">
                            <div class="standerd-pro-info1">
                                <ul class="details-ul">
                                    <li>
                                        <label><b>Pickup @ SaleMe.lk Office</b></label>
                                    </li>
                                    <li>
                                        <label>Kandy Branch</label>
                                        <span> : &nbsp;&nbsp;Available Weekdays 8.30am to 5.00pm</span>
                                    </li>
                                    <li>
                                        <label>Kegalle Branch</label>
                                        <span> : &nbsp;&nbsp;Available Weekdays 8.30am to 5.00pm</span>
                                    </li>
                                    <li>
                                        <label>Delivery</label> <span> : &nbsp;&nbsp;	7-10 Working Days</span>
                                    </li>
                                    <li>
                                        <label>Delivery Area</label>
                                        <span> : &nbsp;&nbsp; All Island Delivery Available</span>
                                    </li>
                                </ul>
                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </div>
        <script type="text/javascript" src="{{ asset("plugins/blockui/jquery.blockUI.js")}}"></script>
        {{ Html::script('js/salenow/plugin/jquery.validate.js') }}
        @if($item->is_prize)
            <script>
                function redeemPrize(id) {
                    $.blockUI({
                        message: '<img src="{{asset('')}}gamezone/img/animat-lightbulb-color.gif" width="100px"/><h1 style="font-size:20px !important;"> Just a moment...</h1>',
                        css: {
                            backgroundColor: '#fff',
                            color: '#222',
                            border: '0px',
                            'border-radius': '5px',
                            'opacity': 1
                        }
                    });
                    setTimeout(function () {
                        $.unblockUI();
                        $('#redeem-form').show().addClass('redeem-class');
                    }, 2000);

                }
                //form sumbission
                $(document).ready(function () {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $("#redeemform").validate({
                        ignore: ":hidden",
                        rules: {
                            name: {
                                required: true,
                            },
                            email: {
                                required: true,
                            },
                            mobile: {
                                required: true,
                                number: true,
                                maxlength: 10,
                                minlength: 10
                            },
                            address: {
                                required: true,
                            },
                            post_code: {
                                required: true,
                                number: true,
                                maxlength: 5,
                                minlength: 5
                            }
                        },
                        messages: {
                            name: {
                                required: "Please fill your Name",
                            },
                            email: {
                                required: "Please fill your Email address",
                            },
                            mobile: {
                                required: "Please fill your mobile Number",
                            },
                            address: {
                                required: "Please fill Address",
                            },
                            post_code: {
                                required: "Please fill Postal Code",
                            }
                        },
                        submitHandler: function (form) {
                            var formdata = $("#redeemform").serializeArray();
                            $.ajax({
                                type: 'POST',
                                url: '/playzone/prize/redeem/{{$memberdetails->memberid}}/{{$item->id}}',
                                data: formdata,
                                beforeSend: function (d) {
                                    $.blockUI({
                                        message: '<img src="{{asset('')}}gamezone/img/animat-lightbulb-color.gif" width="100px"/><h1 style="font-size:20px !important;"> Just a moment...</h1>',
                                        css: {
                                            backgroundColor: '#fff',
                                            color: '#222',
                                            border: '0px',
                                            'border-radius': '5px',
                                            'opacity': 1
                                        }
                                    });
                                },
                                success: function (res) {
                                    $.unblockUI();
                                    $('.alert_display').show().addClass(res.css);
                                    $('.icon').addClass(res.icon);
                                    $('.alert_display .msg').html(res.message);
//                                    $('#responce').show();
                                    $('#redeem-form').show().addClass('redeem-class');
                                    setTimeout(function () {
                                        $('#redeem-form').hide().addClass('redeem-class-remove');
                                        $('.redeem-now-btn').attr('disabled', 'disabled');
                                    }, 500);
                                    setTimeout(function () {
                                        window.scrollTo(0, 0);
//                                        location.reload();
                                    }, 1000);
//                                    setTimeout(function () {
//                                        location.reload();
//                                    }, 3000);
                                }
                            });
                        }
                    });


                });

            </script>
    @endif
@endsection