@extends('layouts.gamezone')
@section('title', 'SaleMe PlayZone Play Share Win')
@section('content')
    {{--{{dd(date('Y-m-d', strtotime($missiondata->extra_question_last_update_at. ' + 2 days')))}}--}}

    <?php
    $daily_coin = true;
    $daily_que = true;
    $extra_que = true;
    if (!is_null($missiondata)) {
        if ($missiondata->daily_coin_last_update_at != date('Y-m-d')) {
            $daily_coin = true;
        } else {
            $daily_coin = false;
        }
        if ($missiondata->daily_question_last_update_at != date('Y-m-d')) {
            $daily_que = true;
        } else {
            $daily_que = false;
        }
        if (!is_null($missiondata->extra_question_last_update_at)){
            if (date('Y-m-d', strtotime($missiondata->extra_question_last_update_at . ' + 2 days')) <= date('Y-m-d')) {
                $extra_que = true;
            } else {
                $extra_que = false;
            }
        }else{
            $extra_que = true;
        }

    }
    ?>
    <div class="wrapper">
        <div class="page-header page-header-small" filter-color="orange">
            <div class="page-header-image" data-parallax="true" style="background-image: url({{asset('')}}images/homepage/main-banner/{{$image->filename}});">
            </div>
            <div class="container">
                <div class="content-center">
                    <div class="photo-container">
                        <img src="{{empty($memberdetails->avatar)? asset('').'images/user.png':$memberdetails->avatar}}"
                             alt="">
                    </div>
                    <h3 class="title">{{$memberdetails->first_name}} {{$memberdetails->last_name}}</h3>
                    <!--<p class="category">Photographer</p>-->
                    <div class="content">
                        <div class="social-description">
                            <h2><i class="ion-help-buoy home-head-icon"></i> {{!is_null($missiondata)?$missiondata->current_score : 0}}</h2>
                            <p>SaleMe Coins</p>
                        </div>
                        <div class="social-description">
                            <h2 style="color: #fea502;">{{$level['currentlevel']}}</h2>
                            <p>Level</p>
                        </div>
                        <div class="social-description">
                            <h4 class="text-muted"><i class="ion-ios-arrow-thin-right"></i> {{$level['nextLevel']}}</h4>
                            <p>Next Level</p>
                        </div>
                        {{--<div class="social-description">--}}
                        {{--<h2><i class="ion-ios-chatbubble-outline"></i> 26</h2>--}}
                        {{--<p>Referral Invites</p>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
        <div class="section section-notifications" id="notifications">
            <div class="alert alert_display" role="alert">
                <div class="container">
                    <div class="alert-icon">
                        <i class="now-ui-icons ui-2_like icon"></i>
                    </div>
                    <span class="msg"></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">
                                <i class="now-ui-icons ui-1_simple-remove"></i>
                            </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 ml-auto mr-auto">
                        <div class="nav-align-center">
                            <ul class="nav nav-pills nav-pills-primary" role="tablist">

                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#profile" role="tablist"
                                       rel="tooltip"
                                       title="Daily Missions" data-placement="top">
                                        <i class="ion-ios-calendar-outline"></i>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " data-toggle="tab" href="#home" role="tablist" rel="tooltip"
                                       title="Social Missions" data-placement="top">
                                        <i class="ion-social-facebook-outline"></i>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#messages" role="tablist" rel="tooltip"
                                       title="Game Zone" data-placement="top">
                                        <i class="ion-ios-game-controller-b-outline"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Tab panes -->
                    <div class="tab-content gallery">

                        <div class="tab-pane active" id="profile" role="tabpanel">
                            <div class="col-md-10 ml-auto mr-auto">
                                <h3 class="text-center">Daily Missions</h3>
                                <div class="clear-fix"></div>
                                <div class="row collections">
                                    <div class="col-md-6">
                                        {!! ($daily_coin)?'<a href="#" id="dailypoint">':'' !!}
                                        <div class="social-mission collect-daily-mission">
                                            <p class="text-center">
                                                {!! ($daily_coin)?'<i class="ion-help-buoy"></i>':'<i class="ion-ios-checkmark-outline"></i>' !!}
                                            </p>
                                            @if($daily_coin)
                                                <h4 class="text-center ">Collect Daily Coin</h4>
                                            @else
                                                <p id="coin" class="demo"></p>
                                                <h4 class="text-center daily_point_added point_added">SaleMe Daily Point
                                                    Added</h4>
                                            @endif
                                        </div>
                                        {!! ($daily_coin)?'</a>':'' !!}

                                        {!! ($extra_que)?'<a href="'.asset('').'playzone/extra-question" id="dailypoint">':'' !!}
                                        <div class="social-mission daily-mission">
                                            <p class="text-center">
                                                {!! ($extra_que)?'<i class="ion-ios-star"></i>':'<i class="ion-ios-checkmark-outline"></i>' !!}
                                            </p>
                                            @if($extra_que)
                                                <h4 class="text-center ">Extra Question</h4>
                                            @else
                                                <p id="extra" class="demo"></p>
                                                <h4 class="text-center daily_point_added point_added">SaleMe Extra Point
                                                    Added</h4>
                                            @endif
                                        </div>
                                        {!! ($extra_que)?'</a>':'' !!}

                                    </div>
                                    <div class="col-md-6">
                                        {!! ($daily_que)?'<a href="'.asset('').'playzone/daily-question" id="dailypoint">':'' !!}
                                        <div class="social-mission extra-mission">
                                            <p class="text-center">
                                                {!! ($daily_que)?'<i class="ion-ios-calendar-outline"></i>':'<i class="ion-ios-checkmark-outline"></i>' !!}
                                            </p>
                                            @if($daily_que)
                                                <h4 class="text-center ">Daily Question</h4>
                                            @else
                                                <p id="que" class="demo"></p>
                                                <h4 class="text-center daily_point_added point_added">SaleMe Daily Question
                                                    Answerd</h4>
                                            @endif
                                        </div>
                                        {!! ($daily_que)?'</a>':'' !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane " id="home" role="tabpanel">
                            <div class="col-md-10 ml-auto mr-auto">
                                <h3 class="text-center">Social Missions</h3>
                                <div class="clear-fix"></div>
                                <div class="row collections">
                                    <div class="col-md-6">
                                        <a href="{{asset('')}}playzone/facebook-missions">
                                            <div class="social-mission facebook-mission">
                                                <p class="text-center">
                                                    <i class="ion-social-facebook"></i>
                                                </p>
                                                <h4 class="text-center">Facebook Missions</h4>
                                            </div>
                                        </a>
                                        {{--<a href="#">--}}
                                        {{--<div class="social-mission twitter-mission">--}}
                                        {{--<p class="text-center">--}}
                                        {{--<i class="ion-social-twitter"></i>--}}
                                        {{--</p>--}}
                                        {{--<h4 class="text-center">Twitter Missions</h4>--}}
                                        {{--</div>--}}
                                        {{--</a>--}}
                                        <a href="{{asset('')}}playzone/other-social-missions">
                                            <div class="social-mission insta-mission">
                                                <p class="text-center">
                                                    <i class="ion-social-instagram"></i>
                                                </p>
                                                <h4 class="text-center">Other Social Missions</h4>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{asset('')}}playzone/google-missions">
                                            <div class="social-mission google-mission">
                                                <p class="text-center">
                                                    <i class="ion-social-googleplus"></i>
                                                </p>
                                                <h4 class="text-center">Google Plus Missions</h4>
                                            </div>
                                        </a>

                                        <!--<img src="../assets/img/bg7.jpg" alt="" class="img-raised">-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="messages" role="tabpanel">
                            <div class="col-md-10 ml-auto mr-auto">
                                <h3 class="text-center">Game Zone</h3>
                                <div class="clear-fix"></div>
                                <div class="row collections">
                                    <div class="col-md-6">
                                        <a href="{{asset('')}}playzone/games/cube-champ/play" target="_blank">
                                            <div class="social-mission game3-mission text-center">
                                                <img src="{{asset('')}}gamezone/img/cubechamp final.png" alt="" class="img-responsive" style="margin: 0px;width: 250px;">
                                                <h4 class="text-center" style="margin: 0px">Cube Champ</h4>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{asset('')}}playzone/games/kokis-challenge/play" target="_blank">
                                            <div class="social-mission game1-mission text-center">
                                                <img src="{{asset('')}}gamezone/img/banner.png" alt="" class="img-responsive" style="margin: 0px;width: 250px;">
                                                <h4 class="text-center" style="margin: 0px">කොකිස් Challenge</h4>
                                            </div>
                                        </a>
                                    </div>
                                    {{--<div class="col-md-6">--}}
                                        {{--<a href="#">--}}
                                            {{--<div class="social-mission game2-mission">--}}
                                                {{--<p class="text-center">--}}
                                                    {{--<i class="ion-social-googleplus"></i>--}}
                                                {{--</p>--}}
                                                {{--<h4 class="text-center">Google Plus Missions</h4>--}}
                                            {{--</div>--}}
                                        {{--</a>--}}
                                        {{--<!--<a href="#">-->--}}
                                        {{--<!--<div class="social-mission insta-mission">-->--}}
                                        {{--<!--<p class="text-center">-->--}}
                                        {{--<!--<i class="ion-social-instagram"></i>-->--}}
                                        {{--<!--</p>-->--}}
                                        {{--<!--<h4 class="text-center">Instagram Missions</h4>-->--}}
                                        {{--<!--</div>-->--}}
                                        {{--<!--</a>-->--}}
                                        {{--<!--<img src="../assets/img/bg7.jpg" alt="" class="img-raised">-->--}}
                                    {{--</div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection