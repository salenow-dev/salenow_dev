@extends('layouts.gamezone')
@section('title', 'SaleMe PlayZone | Twitter Missions | Play Share Win')
@section('content')
    <style>
        #demo {
            text-align: left;
            color: #111;
            float: right;
            font-size: 20px;
            margin-bottom: 15px;
        }

        .ending {
            text-align: left;
            color: #111;
            float: left;
            margin: 0px;
            font-size: 20px;
            margin-bottom: 15px;
        }

        .team-player img {
            width: 100%;
            margin-bottom: 10px;
        }

        .team-player i {
            background: #fff;
            padding: 5px;
        }

        .iframe {
            min-height: 500px;
            width: 100%;
            margin-bottom: 10px;
        }

        .iframediv {
            background: #eee;
        }

        .title-1 {
            margin-top: 15px;
            font-size: 23px;
            font-weight: 400;
        }

        .earn-txt {
            color: #fea502;
            font-weight: 700;
            font-size: 16px;
            margin: 0;
        }

        .radio-btn-div {
            padding-left: 25px;
        }
    </style>
    <div class="wrapper">
        <div class="page-header page-header-small price-header">
            <div class="page-header-image" data-parallax="true" style="background-image: url('../assets/img/bg6.jpg');">
            </div>
            <div class="container">
                <div class="content-center">
                    <h3>SaleMe.lk Facebook Missions</h3>
                    <h5>Play - Share - Win</h5>

                </div>
            </div>
        </div>
        <div class="section section-notifications" id="notifications">
            <div class="alert alert_display" role="alert">
                <div class="container">
                    <div class="alert-icon">
                        <i class="now-ui-icons ui-2_like icon"></i>
                    </div>
                    <span class="msg"></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">
                                <i class="now-ui-icons ui-1_simple-remove"></i>
                            </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="section section-team text-center">
            <div class="container">
                <!--<h2 class="title">Here is our team</h2>-->
                <div class="team">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="team-player">
                                {{ Html::image('images/salenow/banner.gif', 'saleme.lk', array('class' => 'img-responsive')) }}
                                <br>
                                {{--like fan page--}}
                                <div class="card">
                                    <ul class="nav nav-tabs nav-tabs-neutral bg-info" role="tablist"
                                        data-background-color="orange">
                                        <span class="navbar-brand">Like SaleMe.lk Facebook Fan Page</span>

                                    </ul>
                                    <div class="card-body">
                                        <!-- Tab panes -->
                                        <div class="tab-content text-center">
                                            <div class="tab-pane active" id="home1" role="tabpanel">
                                                <p class="pull-right earn-txt"><i class="ion-help-buoy"></i> Earn 10
                                                    Points</p>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                {{--like fan page--}}
                                <div class="card">
                                    <ul class="nav nav-tabs nav-tabs-neutral bg-info" role="tablist"
                                        data-background-color="orange">
                                        <span class="navbar-brand">Like SaleMe.lk Facebook Fan Page</span>

                                    </ul>
                                    <div class="card-body">
                                        <!-- Tab panes -->
                                        <div class="tab-content text-center">
                                            <div class="tab-pane active" id="home1" role="tabpanel">
                                                <p class="pull-right earn-txt"><i class="ion-help-buoy"></i> Earn 10
                                                    Points</p>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                {{--like fan page--}}
                                <div class="card">
                                    <ul class="nav nav-tabs nav-tabs-neutral bg-info" role="tablist"
                                        data-background-color="orange">
                                        <span class="navbar-brand">Like SaleMe.lk Facebook Fan Page</span>

                                    </ul>
                                    <div class="card-body">
                                        <!-- Tab panes -->
                                        <div class="tab-content text-center">
                                            <div class="tab-pane active" id="home1" role="tabpanel">
                                                <p class="pull-right earn-txt"><i class="ion-help-buoy"></i> Earn 10
                                                    Points</p>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="col-md-4 iframediv">
                            <h4 class="title-1">See advertisments on SaleMe.lk</h4>
                            <iframe src="https://saleme.lk/blog/getadstoblog" frameborder="0" class="iframe"></iframe>
                            <br>
                            {{ Html::image('images/salenow/banners/saleme-mobile-banner-gif.gif', 'saleme.lk', array('class' => 'img-responsive visible-xs')) }}
                        </div>
                    </div>

                </div>
            </div>
        </div>

@endsection