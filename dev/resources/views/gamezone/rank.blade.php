@extends('layouts.gamezone')
@section('title', 'SaleMe PlayZone | Rankings | Play Share Win')
@section('content')
    <style>
        .error {
            display: none;
            font-size: 12px;
            color: #e42e2e;
        }

        #edit_details .col-sm-9 {
            margin-bottom: 15px;
            text-align: left;
        }

        #edit_details .input-group {
            margin-bottom: 0px !important;
        }

        .timeline {
            position: relative;
            padding: 4px 0 0 0;
            margin-top: 22px;
            list-style: none;
        }

        .timeline > li:nth-child(even) {
            position: relative;
            margin-bottom: 50px;
            height: 180px;
            right: -100px;
        }

        .timeline > li:nth-child(odd) {
            position: relative;
            margin-bottom: 50px;
            height: 180px;
            left: -100px;
        }

        .timeline > li:before,
        .timeline > li:after {
            content: " ";
            display: table;
        }

        .timeline > li:after {
            clear: both;
            min-height: 170px;
        }

        .timeline > li .timeline-panel {
            position: relative;
            float: left;
            width: 41%;
            padding: 30px 20px 20px 30px;
            text-align: right;
        }

        .timeline > li .timeline-panel:before {
            right: auto;
            left: -15px;
            border-right-width: 15px;
            border-left-width: 0;
        }

        .timeline > li .timeline-panel:after {
            right: auto;
            left: -14px;
            border-right-width: 14px;
            border-left-width: 0;
        }

        .timeline > li .timeline-image {
            z-index: 100;
            position: absolute;
            left: 50%;
            border: 7px solid #3b5998;
            border-radius: 100%;
            background-color: #3b5998;
            box-shadow: 0 0 5px #4582ec;
            width: 200px;
            height: 200px;
            margin-left: -100px;
        }

        .timeline > li .timeline-image h4 {
            margin-top: 12px;
            font-size: 10px;
            line-height: 14px;
        }

        .timeline > li.timeline-inverted > .timeline-panel {
            float: right;
            padding: 30px 30px 20px 20px;
            text-align: left;
        }

        .timeline > li.timeline-inverted > .timeline-panel:before {
            right: auto;
            left: -15px;
            border-right-width: 15px;
            border-left-width: 0;
        }

        .timeline > li.timeline-inverted > .timeline-panel:after {
            right: auto;
            left: -14px;
            border-right-width: 14px;
            border-left-width: 0;
        }

        .timeline > li:last-child {
            margin-bottom: 0;
        }

        .timeline .timeline-heading h4 {
            margin-top: 22px;
            margin-bottom: 4px;
            padding: 0;
            color: #111;
            font-size: 37px;
            line-height:1.1;
        }

        .timeline .timeline-heading h4.subheading {
            margin: 0;
            padding: 0;
            text-transform: none;
            font-size: 18px;
            color: #111;
        }

        .timeline .timeline-heading h4.subheading2 {
            margin: 0;
            padding: 0;
            text-transform: none;
            font-size: 16px;
            color: #fea502;
        }

        .timeline .timeline-body > p,
        .timeline .timeline-body > ul {
            margin-bottom: 0;
            color: #808080;
        }

        /*Style for even div.line*/
        .timeline > li:nth-child(odd) .line:before {
            content: "";
            position: absolute;
            top: 60px;
            bottom: 0;
            left: 690px;
            width: 4px;
            height: 340px;
            background-color: #3b5998;
            -ms-transform: rotate(-44deg); /* IE 9 */
            -webkit-transform: rotate(-44deg); /* Safari */
            transform: rotate(-44deg);
            box-shadow: 0 0 5px #4582ec;
        }

        /*Style for odd div.line*/
        .timeline > li:nth-child(even) .line:before {
            content: "";
            position: absolute;
            top: 60px;
            bottom: 0;
            left: 450px;
            width: 4px;
            height: 340px;
            background-color: #3b5998;
            -ms-transform: rotate(44deg); /* IE 9 */
            -webkit-transform: rotate(44deg); /* Safari */
            transform: rotate(44deg);
            box-shadow: 0 0 5px #4582ec;
        }

        .timeline > li .timeline-image.current {
            z-index: 100;
            position: absolute;
            left: 50%;
            border: 21px solid #fea502;
            border-radius: 100%;
            background-color: #fea502;
            box-shadow: 0 0 5px #855600;
            width: 200px;
            height: 200px;
            margin-left: -100px;
        }

        .timeline > li .timeline-image.next {
            z-index: 100;
            position: absolute;
            left: 50%;
            border: 21px solid #c75732;
            border-radius: 100%;
            background-color: #fea502;
            box-shadow: 0 0 5px #855600;
            width: 200px;
            height: 200px;
            margin-left: -100px;
        }

        .timeline > li .subheading3 {
            font-size: 88px;
            position: absolute;
            top: 0px;
            left: 0px;
            right: 15px;
            z-index: 1;
            opacity: 0.2;
            color: #6f4800;
        }
        .timeline > li .subheading4 {
            font-size: 88px;
            position: absolute;
            top: 0px;
            left: 12px;
            right: 15px;
            z-index: 1;
            opacity: 0.2;
            color: #d0572f;
        }

        .timeline-heading {
            background: transparent;
            z-index: 999;
        }

        /* Medium Devices, .visible-md-* */
        @media (min-width: 992px) and (max-width: 1199px) {
            .timeline > li:nth-child(even) {
                margin-bottom: 0px;
                min-height: 0px;
                right: 0px;
            }

            .timeline > li:nth-child(odd) {
                margin-bottom: 0px;
                min-height: 0px;
                left: 0px;
            }

            .timeline > li:nth-child(even) .timeline-image {
                left: 0;
                margin-left: 0px;
            }

            .timeline > li:nth-child(odd) .timeline-image {
                left: 690px;
                margin-left: 0px;
            }

            .timeline > li:nth-child(even) .timeline-panel {
                width: 76%;
                padding: 0 0 20px 0px;
                text-align: left;
            }

            .timeline > li:nth-child(odd) .timeline-panel {
                width: 70%;
                padding: 0 0 20px 0px;
                text-align: right;
            }

            .timeline > li .line {
                display: none;
            }
        }

        /* Small Devices, Tablets */
        @media (min-width: 768px) and (max-width: 991px) {
            .timeline > li:nth-child(even) {
                margin-bottom: 0px;
                min-height: 0px;
                right: 0px;
            }

            .timeline > li:nth-child(odd) {
                margin-bottom: 0px;
                min-height: 0px;
                left: 0px;
            }

            .timeline > li:nth-child(even) .timeline-image {
                left: 0;
                margin-left: 0px;
            }

            .timeline > li:nth-child(odd) .timeline-image {
                left: 520px;
                margin-left: 0px;
            }

            .timeline > li:nth-child(even) .timeline-panel {
                width: 70%;
                padding: 0 0 20px 0px;
                text-align: left;
            }

            .timeline > li:nth-child(odd) .timeline-panel {
                width: 70%;
                padding: 0 0 20px 0px;
                text-align: right;
            }

            .timeline > li .line {
                display: none;
            }
        }

        /* Custom, iPhone Retina */
        @media only screen and (max-width: 767px) {
            .timeline > li:nth-child(even) {
                margin-bottom: 0px;
                min-height: 0px;
                right: 0px;
            }

            .timeline > li:nth-child(odd) {
                margin-bottom: 0px;
                min-height: 0px;
                left: 0px;
            }

            .timeline > li .timeline-image {
                position: static;
                width: 150px;
                height: 150px;
                margin-bottom: 0px;
            }

            .timeline > li:nth-child(even) .timeline-image {
                left: 0;
                margin-left: 0;
            }

            .timeline > li:nth-child(odd) .timeline-image {
                float: right;
                left: 0px;
                margin-left: 0;
            }

            .timeline > li:nth-child(even) .timeline-panel {
                width: 100%;
                padding: 0 0 20px 14px;
            }

            .timeline > li:nth-child(odd) .timeline-panel {
                width: 100%;
                padding: 0 14px 20px 0px;
            }

            .timeline > li .line {
                display: none;
            }
        }
    </style>
    <div class="wrapper">
        <div class="page-header page-header-small" filter-color="orange">
            <div class="page-header-image" data-parallax="true" style="background-image: url({{asset('')}}images/homepage/main-banner/{{$image->filename}});">
            </div>
            <div class="container">
                <div class="content-center">
                    <div class="photo-container">
                        <img src="{{empty($memberdetails->avatar)? asset('').'images/user.png':$memberdetails->avatar}}"
                             alt="">
                    </div>
                    <h3 class="title">{{$memberdetails->first_name}} {{$memberdetails->last_name}}</h3>
                    <!--<p class="category">Photographer</p>-->
                    <div class="content">
                        <div class="social-description">
                            <h2>
                                <i class="ion-help-buoy home-head-icon"></i> {{!is_null($missiondata)?$missiondata->current_score : 0}}
                            </h2>
                            <p>SaleMe Coins</p>
                        </div>
                        <div class="social-description">
                            <h2 style="color: #fea502;">{{$level['currentlevel']}}</h2>
                            <p>Level</p>
                        </div>
                        <div class="social-description">
                            <h4 class="text-muted"><i class="ion-ios-arrow-thin-right"></i> {{$level['nextLevel']}}</h4>
                            <p>Next Level</p>
                        </div>
                        {{--<div class="social-description">--}}
                        {{--<h2><i class="ion-ios-chatbubble-outline"></i> 26</h2>--}}
                        {{--<p>Referral Invites</p>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
        <div class="section-notifications" id="notifications">
            <div class="alert alert_display" role="alert">
                <div class="container">
                    <div class="alert-icon">
                        <i class="now-ui-icons ui-2_like icon"></i>
                    </div>
                    <span class="msg"></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">
                                <i class="now-ui-icons ui-1_simple-remove"></i>
                            </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="section section-tabs">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="timeline">
                            <li>
                                <div class="timeline-image {{($level['current'] == 1)?'current' :''}}">
                                    <img class="rounded-circle img-responsive "
                                         src="{{asset('')}}gamezone/img/ranks/elite.jpg" alt="">
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-heading">
                                        <h4>Elite</h4>
                                        <h4 class="subheading">Level I</h4>
                                        <h4 class="subheading2">Upto <b>150</b> SaleMe Coins</h4>

                                    </div>
                                    @if($level['current'] == 1)
                                        <h4 class="subheading3">CURRENT</h4>
                                    @endif
                                </div>
                                <div class="line"></div>
                            </li>
                            <li class="timeline-inverted">
                                <div class="timeline-image {{($level['current'] == 2)?'current' :''}} {{($level['next'] == 2)?'next' :''}}">
                                    <img class="rounded-circle img-responsive"
                                         src="{{asset('')}}gamezone/img/ranks/knight.jpg" alt="">
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-heading">
                                        <h4>Knight</h4>
                                        <h4 class="subheading">Level II</h4>
                                        <h4 class="subheading2"><b>150</b> - <b>250</b> SaleMe Coins</h4>
                                    </div>
                                    @if($level['current'] == 2)
                                        <h4 class="subheading3">CURRENT</h4>
                                    @endif
                                    @if($level['next'] == 2)
                                        <h4 class="subheading4">NEXT</h4>
                                    @endif
                                </div>
                                <div class="line"></div>
                            </li>
                            <li>
                                <div class="timeline-image {{($level['current'] == 3)?'current' :''}} {{($level['next'] == 3)?'next' :''}}">
                                    <img class="rounded-circle img-responsive"
                                         src="{{asset('')}}gamezone/img/ranks/champion.jpg" alt="">
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-heading">
                                        <h4>Champion</h4>
                                        <h4 class="subheading">Level III</h4>
                                        <h4 class="subheading2"><b>250</b> - <b>400</b> SaleMe Coins</h4>
                                    </div>@if($level['current'] == 3)
                                        <h4 class="subheading3">CURRENT</h4>
                                    @endif
                                    @if($level['next'] == 3)
                                        <h4 class="subheading4">NEXT</h4>
                                    @endif
                                </div>
                                <div class="line"></div>
                            </li>
                            <li class="timeline-inverted">
                                <div class="timeline-image {{($level['current'] == 4)?'current' :''}} {{($level['next'] == 4)?'next' :''}}">
                                    <img class="rounded-circle img-responsive"
                                         src="{{asset('')}}gamezone/img/ranks/legend.jpg" alt="">
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-heading">
                                        <h4>Legend</h4>
                                        <h4 class="subheading">Level IV</h4>
                                        <h4 class="subheading2"><b>400</b> - <b>650</b> SaleMe Coins</h4>
                                    </div>
                                    @if($level['current'] == 4)
                                        <h4 class="subheading3">CURRENT</h4>
                                    @endif
                                    @if($level['next'] == 4)
                                        <h4 class="subheading4">NEXT</h4>
                                    @endif
                                </div>
                                <div class="line"></div>
                            </li>
                            <li>
                                <div class="timeline-image {{($level['current'] == 5)?'current' :''}} {{($level['next'] == 5)?'next' :''}}">
                                    <img class="rounded-circle img-responsive"
                                         src="{{asset('')}}gamezone/img/ranks/king.jpg" alt="">
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-heading">
                                        <h4>King</h4>
                                        <h4 class="subheading">Level V</h4>
                                        <h4 class="subheading2"><b>650</b> - <b>1000</b> SaleMe Coins</h4>
                                    </div>
                                    @if($level['current'] == 5)
                                        <h4 class="subheading3">CURRENT</h4>
                                    @endif
                                    @if($level['next'] == 5)
                                        <h4 class="subheading4">NEXT</h4>
                                    @endif
                                </div>
                                <div class="line"></div>
                            </li>
                            <li class="timeline-inverted">
                                <div class="timeline-image {{($level['current'] == 6)?'current' :''}} {{($level['next'] == 6)?'next' :''}}">
                                    <img class="rounded-circle img-responsive"
                                         src="{{asset('')}}gamezone/img/ranks/emperor.jpg" alt="">
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-heading">
                                        <h4>Emperor</h4>
                                        <h4 class="subheading">Level VI</h4>
                                        <h4 class="subheading2"><b>1000</b> - <b>1400</b> SaleMe Coins</h4>
                                    </div>
                                    @if($level['current'] == 6)
                                        <h4 class="subheading3">CURRENT</h4>
                                    @endif
                                    @if($level['next'] == 6)
                                        <h4 class="subheading4">NEXT</h4>
                                    @endif
                                </div>
                                <div class="line"></div>
                            </li>
                            <li>
                                <div class="timeline-image {{($level['current'] == 7)?'current' :''}} {{($level['next'] == 7)?'next' :''}}">
                                    <img class="rounded-circle img-responsive"
                                         src="{{asset('')}}gamezone/img/ranks/god.jpg" alt="">
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-heading">
                                        <h4>God</h4>
                                        <h4 class="subheading">Level VII</h4>
                                        <h4 class="subheading2"><b>1400</b> - <b>2000</b> SaleMe Coins</h4>
                                    </div>
                                    @if($level['current'] == 7)
                                        <h4 class="subheading3">CURRENT</h4>
                                    @endif
                                    @if($level['next'] == 7)
                                        <h4 class="subheading4">NEXT</h4>
                                    @endif
                                </div>
                                <div class="line"></div>
                            </li>
                            <li class="timeline-inverted">
                                <div class="timeline-image {{($level['current'] == 8)?'current' :''}} {{($level['next'] == 8)?'next' :''}}">
                                    <img class="rounded-circle img-responsive"
                                         src="{{asset('')}}gamezone/img/ranks/titan.jpg" alt="">
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-heading">
                                        <h4>Titan</h4>
                                        <h4 class="subheading">Final Level</h4>
                                        <h4 class="subheading2">More than <b>2000</b> SaleMe Coins</h4>
                                    </div>
                                </div>
                                @if($level['current'] == 8)
                                    <h4 class="subheading3">CURRENT</h4>
                                @endif
                                @if($level['next'] == 8)
                                    <h4 class="subheading4">NEXT</h4>
                                @endif
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>

@endsection