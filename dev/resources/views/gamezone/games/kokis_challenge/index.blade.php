<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="text/html; charset=utf-8" http-equiv=Content-Type>{{----}}
    <meta content="{{ csrf_token() }}" name=csrf-token>
    <title>SaleMe.lk කොකිස් Challenge | SaleMe.lk Kokis Challenge </title>
    <meta content="width=device-width,initial-scale=1" name=viewport>
    <meta content="කොකිස් Challenge, kokis Challenge, awurudu game, free t-shirt, free reloads"
          name=keywords>
    <meta content="SaleMe.lk හඳුන්වාදෙන 'කොකිස් Challenge'. මේ අවුරුදු කාලේ කොකිස් Challenge එක Play කර, වැඩිම ලකුණු ලබාගෙන වටිනා T-Shirt සහ නොමිලේ Reload දිනා ගන්න. SaleMe.lk වෙත පිවිසෙන්න, නැත්නම් https://goo.gl/AgqfpY click කරන්න. කොකිස් වැඩි ප්‍රමාණයක් රැස්කර යාලුවෝ 5දෙනෙක් tag කරලා share කරන්න #KokisChallenge #Awurudu2K18 #SaleMe #WinTshirts #Reloads"
          name=description>
    <meta content=#fea502 name=theme-color>
    <link href="{{asset('/images/salenow/favicon-16x16.png')}}" rel="shortcut icon" type=image/x-icon>
    <link href="{{asset('/images/salenow/favicon-16x16.png')}}" rel=icon type=image/x-icon>
    <meta content={{Request::url()}} property=og:url>
    <meta content=www.saleme.lk property=og:site_name>
    <meta content=product property=og:type>
    <meta content="SaleMe.lk කොකිස් Challenge | SaleMe.lk Kokis Challenge " property=og:title>
    <meta content="SaleMe.lk හඳුන්වාදෙන 'කොකිස් Challenge'. මේ අවුරුදු කාලේ කොකිස් Challenge එක Play කර, වැඩිම ලකුණු ලබාගෙන වටිනා T-Shirt සහ නොමිලේ Reload දිනා ගන්න. SaleMe.lk වෙත පිවිසෙන්න, නැත්නම් https://goo.gl/AgqfpY click කරන්න. කොකිස් වැඩි ප්‍රමාණයක් රැස්කර යාලුවෝ 5දෙනෙක් tag කරලා share කරන්න #KokisChallenge #Awurudu2K18 #SaleMe #WinTshirts #Reloads"
          property=og:description>
    <meta content=600 property=og:image:width>
    <meta content=315 property=og:image:height>
    <meta content="{{asset('')}}game_assets/banner.jpg" property=og:image>
    <meta content=1867918153484824 property=fb:app_id>

    {{ Html::style('css/salenow/layout/bootstrap-3.3.7/css/bootstrap.min.css') }}
    {{ Html::style('game_assets/css/style.css') }}
    {{ Html::style('css/salenow/layout/ionicons/css/ionicons.min.css') }}
    <link href="https://fonts.googleapis.com/css?family=Hi+Melody" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
    <script>
        !function (e, t, a, n, r) {
            e[n] = e[n] || [], e[n].push({
                "gtm.start": (new Date).getTime(),
                event: "gtm.js"
            });
            var s = t.getElementsByTagName("script")[0],
                g = t.createElement("script");
            g.async = !0, g.src = "https://www.googletagmanager.com/gtm.js?id=GTM-PQQWX8C", s.parentNode.insertBefore(g, s)
        }(window, document, 0, "dataLayer")
    </script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-834902320"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments)
        }
        gtag("js", new Date), gtag("config", "AW-834902320")
    </script>

    <style>
        .nopadding {
            padding: 0px;
        }

        #yes,
        #start,
        #start_btn {
            font-size: 20px;
            padding: 10px 25px;
            border: 0;
            border-radius: 3px;
            color: #fff;
            background: #fea502;
            outline: none;
        }

        .start_btn_1 {
            font-size: 20px;
            padding: 10px 16px;
            border: 0;
            border-radius: 3px;
            color: #fff;
            background: #02b6fe;
            outline: none;
        }

        #domMessage h1 {
            margin-top: 0px;
        }

        .top-score {
            text-align: right;
            padding-right: 15px;
            font-family: 'Hi Melody', cursive;
            margin: 0;
            font-size: 25px;
            line-height: 0.5;
        }

        .media-head {
            font-family: 'Hi Melody', cursive;
            font-size: 24px;
            padding: 0;
            margin: 0;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        .top-ten {
            margin: 15px 0 5px;
            text-align: center;
            font-size: 25px;
            color: #fff;
            font-family: 'Pacifico', cursive;
        }
        .plyed {
            margin-bottom: 10px;
            display: block;
            text-align: center;
            font-size: 25px;
            color: #fff;
            font-family: 'Hi Melody', cursive;
        }
        #myModal {
            z-index: 99999;
        }

        .banner-div img {
            width: 100%;
        }

        .banner-div .close {
            position: absolute;
            right: 0;
            color: #fff;
            opacity: 0.8;
            padding: 10px;
        }

        .banner-div .close i {
            font-size: 41px;
        }

        .scoreboard-div {
            padding: 13px;
            background: #77600e;
            color: #fff;
            font-size: 18px;
            font-family: 'Pacifico', cursive;
        }
.win{
paddding:10px 0px;
text-align:center;
color:#fff;
}
    </style>
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 nopadding">

            <div class="game">
                <div class="header-game">
                    <div class="counter" id="counter"><span class='remain'>තත්පර</span> 60
                        <br>

                    </div>
                    {{--<div>secnsa</div>--}}
                    {{--<div class="score">--}}
                    {{--කොකිස් 0--}}
                    {{--</div>--}}
                    <div class="sound-area">
                        <img class="mute-btn-class" src="{{asset('')}}game_assets/sound.png" width="50px">
                        {{--<img class="mute-btn-class" src="../game_assets/mute.png" width="75px">--}}

                        {{--<button id="mute-btn" onclick="mute()"><i class="ion-volume-high" id="mute-btn-class"></i>--}}
                        {{--</button>--}}

                    </div>
                </div>
                {{--<div class="counter" id="counter">00s</div>--}}
                <div class="score score-1">
                    කොකිස් 0
                </div>

            </div>
        </div>

    </div>
</div>
<audio id="bgs">
    <source src="{{asset('')}}game_assets/bgsound.mp3" type="audio/mpeg">
</audio>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            {{--<div class="modal-header">--}}
            {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
            {{--<h4 class="modal-title">Modal Header</h4>--}}
            {{--</div>--}}
            <div class="banner-div">
                <button type="button" class="close" data-dismiss="modal"><i class="ion-close-circled"></i></button>
                <img src="{{asset('')}}game_assets/banner-long.jpg" alt="" class="img-responsive">
            </div>
            <div class="modal-body">
                <h4>Game Rules & How to play</h4>
                <ol>
                    <li>When Catch 'Kokis' marks increment by one.</li>
                    <li>When Catch 'Fire cracker' marks deduct by one.</li>
                    <li>Catch 'Kokis' as much as you can.</li>
                    <li>Then share your marks on Facebook by tagging 5 friends of you.</li>
                    <li>After share process you can fill your contact details. Then you are registered for
                        competition.
                    </li>
                    <li>The competition will be evaluated Every day at 5.00 pm</li>
                    <li>If you are a winner, SaleMe.lk will contact you</li>
                </ol>
                <hr>
                <h4><b>තරග නීතිරීති සහ ක්‍රීඩා කිරීම</b></h4>
                <ol>
                    <li>'කොකිස්' අල්ලගන්න විට ඔබට එක ලකුණක් හිමිවේ.</li>
                    <li>'රතිඤ්ඤා' අල්ලගන්න විට ඔබට එක ලකුණක් අහිමිවේ.</li>
                    <li>ඔබට හැකි තරම් කොකිස් අල්ලන්න.</li>
                    <li>එවිට ඔබේ මිතුරන් 5 ක් Tag කර Facebook එකෙහි Share කරන්න.</li>
                    <li>Share කිරීමෙන් පසු ඔබේ සම්බන්ධතා විස්තර පුරවන්න. එවිට ඔබ තරග සඳහා ලියාපදිංචි වී ඇත.</li>
                    <li>තරගය සෑම දිනකම පස්වරු 5.00 ට ඇගයීමට ලක් කෙරේ.</li>
                    <li>ඔබ ජයග්රාහකයෙක් නම්, SaleMe.lk ඔබව සම්බන්ධ කර ගනීවි.</li>
                </ol>
            </div>

        </div>

    </div>
</div>
{{Html::script('js/salenow/jquery.min.js')}}
{{Html::script('js/salenow/bootstrap.min.js')}}
<script type="text/javascript" src="{{ asset("plugins/blockui/jquery.blockUI.js")}}"></script>
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
        $.blockUI({message: $('#start_game'), css: {
            backgroundColor: '#fff',
            color: '#222',
            border: '0px',
            'border-radius': '5px',
            'opacity': 1
        }});


        $('#start_btn').click(function () {
            $.unblockUI();
            var score = 0;
            var color = "blue";
            var ismute = 1;
            var beepOne = document.getElementById("bgs");
            beepOne.play();

            $('.mute-btn-class').click(function () {
                if (ismute) {
                    $('.mute-btn-class').attr("src", "{{asset('')}}game_assets/mute.png");
                    beepOne.pause();
                    return ismute = 0;

                } else {
                    $('.mute-btn-class').attr("src", "{{asset('')}}game_assets/sound.png");
                    beepOne.play();
                    return ismute = 1;
                }
            });


            function random(min, max) {
                return Math.round(Math.random() * (max - min) + min);
            }

            function setBG() {
                if (Math.round(Math.random())) {
                    return "{{asset('')}}game_assets/kokis.png";
                } else {
                    return "{{asset('')}}game_assets/ratinna.png";
                }
            }


            function dropBox() {
                var length = random(100, ($(".game").width() - 100));
                var velocity = random(850, 10000);
                var size = random(50, 150);
                var thisBox = $("<div/>", {
                    class: "box",
                    style: "width:" + size + "px; height:" + size + "px; left:" + length + "px; transition: transform " + velocity + "ms linear;"
                });

                //set data and bg based on data
                thisBox.data("test", Math.round(Math.random()));
                if (thisBox.data("test")) {
                    thisBox.css({
                        "background": "url('{{asset('')}}game_assets/kokis.png')",
                        "background-size": "contain"
                    });
                } else {
                    thisBox.css({
                        "background": "url('{{asset('')}}game_assets/ratinna.png')",
                        "background-size": "contain"
                    });
                }


                //insert gift element
                $(".game").append(thisBox);

                //random start for animation
                setTimeout(function () {
                    thisBox.addClass("move");
                }, random(0, 5000));

                //remove this object when animation is over
                thisBox.one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend",
                    function (event) {
                        $(this).remove();
                    });
            }

            for (i = 0; i < 10; i++) {
                dropBox();
            }


            $(document).on('click', '.box', function () {
                if ($(this).data("test")) {
                    score += 1;
                    if (ismute) {
                        PlaySound('{{asset('')}}game_assets/crispy_slow.mp3');
                    }
//            PlaySound('../game_assets/crispy_slow.mp3');
                } else {
                    if (score > 0) {
                        score -= 1;
                    }
                    if (ismute) {
                        PlaySound('{{asset('')}}game_assets/haha_don.mp3');
                    }
                }

                $(".score").html("කොකිස් " + score);
                $(this).remove();
            });

            var runGame = setInterval(function () {
                for (i = 0; i < 10; i++) {
                    dropBox();
                }
            }, 5000);

            function countdown() {
                var seconds = 5;

                function tick() {
                    var counter = document.getElementById("counter");
                    seconds--;
                    counter.innerHTML = "<span class='remain'>තව තත්පර</span>" + (seconds < 10 ? "0" : "") + String(seconds);
                    if (seconds > 0) {
                        setTimeout(tick, 1000);
                        // draw();
                        // update();
                    } else {
                        if (score <= 0) {
                            $.blockUI({message: $('#domMessage'), css: {
                                backgroundColor: '#fff',
                                color: '#222',
                                border: '0px',
                                'border-radius': '5px',
                                'opacity': 1
                            }});
                            $('#yes').click(function () {
                                // update the block message
                                $.blockUI({message: "<h1>Restarting Game...</h1>"});
                                document.location.href = '/playzone/games/kokis-challenge/play';

                            });

                        } else {
                            $.blockUI();
                            var rand = Math.floor((Math.random() * 10000000) + 1);
                            var data = {"score": score, "_token": "{{ csrf_token() }}"};
                            $.ajax({
                                type: "post",
                                url: '/playzone/games/kokis-challenge/score/' + rand,
                                data: data,
                                success: function (res) {
                                    $.unblockUI();
                                    document.location.href = '/playzone/games/kokis-challenge/share/' + rand;
//
                                }
                            });
                            // alert("Game over");
                            clearInterval(runGame);
                        }
                    }
                }

                tick();
            }

            countdown();

            function PlaySound(path) {
                var audioElement = document.createElement('audio');
                audioElement.setAttribute('src', path);
                audioElement.play();
            }


        });
    });
</script>
<div id="domMessage" style="display:none;">
    <img src="{{asset('')}}gamezone/img/animat-search-color.gif" width="100px"/>
    <h1 style="font-size:20px !important;">Your Score is 0</h1>
    <button id="yes"><i class="ion-refresh"></i>&nbsp;&nbsp;Replay Game</button>
    {{--<input type="button"  value="Replay Game"/>--}}
</div>
<div id="start_game" style="display:none;">
    <img src="{{asset('')}}game_assets/banner.jpg" alt="" class="img-responsive hidden-xs" width="100%">
    <h1 style="font-size:20px !important;background: #eee; padding: 10px; border-radius: 10px;" class="hidden-xs">Start Game Here</h1>
    <button id="start_btn" title="Start Game"><i class="ion-arrow-right-b"></i>&nbsp;&nbsp;Start</button>
    
</div>

</body>

</html>
