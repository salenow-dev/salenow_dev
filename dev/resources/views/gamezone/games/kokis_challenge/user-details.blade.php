<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="text/html; charset=utf-8" http-equiv=Content-Type>{{----}}
    <meta content="{{ csrf_token() }}" name=csrf-token>
    <title>SaleMe.lk කොකිස් Challenge | SaleMe.lk Kokis Challenge </title>
    <meta content="width=device-width,initial-scale=1" name=viewport>
    <meta content="කොකිස් Challenge, kokis Challenge, awurudu game, free t-shirt, free reloads"
          name=keywords>
    <meta content="SaleMe.lk හඳුන්වාදෙන 'කොකිස් Challenge'. මේ අවුරුදු කාලේ කොකිස් Challenge එක Play කර, වැඩිම ලකුණු ලබාගෙන වටිනා T-Shirt සහ නොමිලේ Reload දිනා ගන්න. SaleMe.lk වෙත පිවිසෙන්න, නැත්නම් https://goo.gl/AgqfpY click කරන්න. කොකිස් වැඩි ප්‍රමාණයක් රැස්කර යාලුවෝ 5දෙනෙක් tag කරලා share කරන්න #KokisChallenge #Awurudu2K18 #SaleMe #WinTshirts #Reloads"
          name=description>
    <meta content=#fea502 name=theme-color>
    <link href="{{asset('/images/salenow/favicon-16x16.png')}}" rel="shortcut icon" type=image/x-icon>
    <link href="{{asset('/images/salenow/favicon-16x16.png')}}" rel=icon type=image/x-icon>
    <meta content={{Request::url()}} property=og:url>
    <meta content=www.saleme.lk property=og:site_name>
    <meta content=product property=og:type>
    <meta content="SaleMe.lk කොකිස් Challenge | SaleMe.lk Kokis Challenge " property=og:title>
    <meta content="SaleMe.lk හඳුන්වාදෙන 'කොකිස් Challenge'. මේ අවුරුදු කාලේ කොකිස් Challenge එක Play කර, වැඩිම ලකුණු ලබාගෙන වටිනා T-Shirt සහ නොමිලේ Reload දිනා ගන්න. SaleMe.lk වෙත පිවිසෙන්න, නැත්නම් https://goo.gl/AgqfpY click කරන්න. කොකිස් වැඩි ප්‍රමාණයක් රැස්කර යාලුවෝ 5දෙනෙක් tag කරලා share කරන්න #KokisChallenge #Awurudu2K18 #SaleMe #WinTshirts #Reloads"
          property=og:description>
    <meta content=600 property=og:image:width>
    <meta content=315 property=og:image:height>
    <meta content="{{asset('')}}game_assets/banner.jpg" property=og:image>
    <meta content=1867918153484824 property=fb:app_id>

    {{ Html::style('css/salenow/layout/bootstrap-3.3.7/css/bootstrap.min.css') }}
    {{Html::script('js/salenow/jquery.min.js')}}
    {{Html::script('js/salenow/bootstrap.min.js')}}
    {{ Html::style('css/salenow/layout/ionicons/css/ionicons.min.css') }}
    {{--spark--}}
    {{ Html::style('game_assets/spark/css/style.css') }}
    <script>
        window.fbAsyncInit = function () {
            FB.init({
                appId: '1867918153484824',
                autoLogAppEvents: true,
                xfbml: true,
                version: 'v2.12'
            });
        };
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <style>
        @import url(https://fonts.googleapis.com/css?family=Roboto:400,300,600,400italic);

        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-font-smoothing: antialiased;
            -moz-font-smoothing: antialiased;
            -o-font-smoothing: antialiased;
            font-smoothing: antialiased;
            text-rendering: optimizeLegibility;
        }

        body {
            font-family: "Roboto", Helvetica, Arial, sans-serif;
            font-weight: 100;
            font-size: 12px;
            line-height: 30px;
            color: #777;
            background: #fea502;
        }

        .container {
            max-width: 400px;
            width: 100%;
            margin: 0 auto;
            position: relative;
        }

        #contact input[type="text"],
        #contact input[type="email"],
        #contact input[type="tel"],
        #contact input[type="url"],
        #contact textarea,
        #contact button[type="submit"] {
            font: 400 12px/16px "Roboto", Helvetica, Arial, sans-serif;
        }

        #contact,
        #conatct2,
        #contact3 {
            background: #F9F9F9;
            padding: 25px;
            margin: 25px 0;
            box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        }

        #contact h3 {
            display: block;
            font-size: 30px;
            font-weight: 300;
            margin-bottom: 10px;
            margin-top: 0px;
        }

        #contact h4 {
            margin: 5px 0 15px;
            display: block;
            font-size: 13px;
            font-weight: 400;
        }

        fieldset {
            border: medium none !important;
            margin: 0 0 10px;
            min-width: 100%;
            padding: 0;
            width: 100%;
        }

        #contact input[type="text"],
        #contact input[type="email"],
        #contact input[type="tel"],
        #contact input[type="url"],
        #contact textarea {
            width: 100%;
            border: 1px solid #ccc;
            background: #FFF;
            margin: 0 0 5px;
            padding: 10px;
        }

        #contact input[type="text"]:hover,
        #contact input[type="email"]:hover,
        #contact input[type="tel"]:hover,
        #contact input[type="url"]:hover,
        #contact textarea:hover {
            -webkit-transition: border-color 0.3s ease-in-out;
            -moz-transition: border-color 0.3s ease-in-out;
            transition: border-color 0.3s ease-in-out;
            border: 1px solid #aaa;
        }

        #contact textarea {
            height: 100px;
            max-width: 100%;
            resize: none;
        }

        #contact button[type="submit"] {
            cursor: pointer;
            width: 100%;
            border: none;
            background: #4CAF50;
            color: #FFF;
            margin: 0 0 5px;
            padding: 10px;
            font-size: 15px;
        }

        #contact button[type="submit"]:hover {
            background: #43A047;
            -webkit-transition: background 0.3s ease-in-out;
            -moz-transition: background 0.3s ease-in-out;
            transition: background-color 0.3s ease-in-out;
        }

        #contact button[type="submit"]:active {
            box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.5);
        }

        .copyright {
            text-align: center;
        }

        #contact input:focus,
        #contact textarea:focus {
            outline: 0;
            border: 1px solid #aaa;
        }

        ::-webkit-input-placeholder {
            color: #888;
        }

        :-moz-placeholder {
            color: #888;
        }

        ::-moz-placeholder {
            color: #888;
        }

        :-ms-input-placeholder {
            color: #888;
        }

        #shareBtn {
            background: #3B5998;
            color: white;
        }

        .share-txt {
            font-size: 16px;
            text-align: center;
        }

        .score {
            background-color: #fff;
            width: 150px;
            height: 150px;
            border-radius: 50% 50%;
            -moz-border-radius: 50% 50%;
            -webkit-border-radius: 50% 50%;
            -o-border-radius: 50% 50%;
            -ms-border-radius: 50% 50%;
            border: 1px solid #fea502;
            color: #fea502;
            font-size: 55px;
            line-height: 140px;
            text-align: center;
            margin: 10px auto;
        }

        .head-txt {
            margin: 0;
            padding-bottom: 5px;
            font-weight: 300;
            font-size: 22px;
            color: #222;
        }

        .replybtn {
            margin-top: 15px;
            padding: 10px;
            background: #fea502;
            color: #fff;
            font-size: 17px;
        }

        .userdetailsform {
            padding-top: 25px;
        }

        .alert {
            font-size: 16px;
            font-weight: 400;
        }

        .banner-div {
            margin-top: 26px;
            padding: 0 0 10px;
        }

        .error {
            font-size: 12px;
            color: #f13737;
            margin: 0;
            line-height: 0.2;
            font-weight: 300;
        }

        .details-add-success {
            margin-top: 20px;
        }

        #basic-addon1 {
            /* padding: 0px 12px; */
            padding-top: 0px;
            padding-bottom: 0px;
            line-height: 0.2;
            border: 0;
            background: transparent;
            vertical-align: unset;
            /*height: -6px;*/
        }
    </style>
</head>

<body>

<div class="container sharediv">
    <div class="row">
        <div class="col-md-12 banner-div">
            <img src="{{asset('')}}game_assets/banner.jpg" alt="" class="img-responsive">
        </div>

        <div id="conatct2">
            <h2 class="text-center head-txt">Your Score Is</h2>
            <h3 class="text-center score">{{$score}}</h3>
            <p class="share-txt">Share on Facebook to Continue</p>
            <p class="share-txt"><b>යාලුවෝ 5 දෙනෙක් Tag කර Facebook එකේ share කර තරගයට ඇතුලත් වන්න </b></p>
            <div id="shareBtn" class="btn btn-block btn-social btn-lg btn-facebook"><i class="ion-social-facebook"></i>&nbsp;&nbsp;Share
                on Facebook
            </div>

        </div>
    </div>
    <div class="alert alert-danger alert-dismissible error-share" style="display: none">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error!</strong> <br>Share process not completed. Please share to continue
        <br>ඉදිරියට යාම සදහා Facebook එකේ Share කරන්න. යාලුවෝ 5දෙනෙක් Tag කරලා share කරන්න
    </div>

</div>

<div class="container userdetailsform" style="display: none">
    <div class="alert alert-success alert-dismissible success-share" style="display: none">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> Share process is completed. Please enter contat details to continue
        <br>Share කිරීම සාර්ථකයි. ඔබව සම්බන්ධ කරගතහැකි තොරතුරු ඇතුලත් කරන්න
    </div>
    <div class="alert alert-danger alert-dismissible" style="display: none">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error!</strong> <br>User Details not added. Please try again
        <br>නැවත උත්සහ කරන්න
    </div>
</div>
<div class="container userdetailscompleted" style="display: none">

    <div class="alert alert-success alert-dismissible details-add-success" style="display: none;">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> You have entered to competition. Stay on play
        <br>ඔබගේ තොරතුරු ඇතුලත් කිරීම සාර්ථකයි. වැඩි ලකුණු ප්‍රමානයක් ලබාගැනීමට නැවත ක්‍රීඩා කරන්න
    </div>
    <div class="col-md-12 banner-div">
        <img src="{{asset('')}}game_assets/banner.jpg" alt="" class="img-responsive">
    </div>
    <div id="contact3">
        <h2 class="text-center head-txt">Want Play Again?</h2>
        <a href="{{asset('')}}game/play">
            <button class="btn btn-block replybtn"><i class="ion-refresh"></i>&nbsp;&nbsp;Reply game</button>
        </a>
    </div>
    <div class="alert alert-success alert-dismissible details-add-success2" style="display: none;">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> You have entered to competition. Stay on play
        <br>ඔබගේ තොරතුරු ඇතුලත් කිරීම සාර්ථකයි. වැඩි ලකුණු ප්‍රමානයක් ලබාගැනීමට නැවත ක්‍රීඩා කරන්න
    </div>

    <div class="alert alert-danger alert-dismissible unsuccess-msg2" style="display: none;">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> You have entered to competition. Stay on play
        <br>ඔබගේ තොරතුරු ඇතුලත් කිරීම සාර්ථකයි. වැඩි ලකුණු ප්‍රමානයක් ලබාගැනීමට නැවත ක්‍රීඩා කරන්න
    </div>

</div>


</body>
<script type="text/javascript" src="{{ asset("plugins/blockui/jquery.blockUI.js")}}"></script>
{{ Html::script('js/salenow/plugin/jquery.validate.js') }}
<script>

    $(".user_details").hide();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    document.getElementById('shareBtn').onclick = function () {
        FB.ui({
            method: 'share',
            display: 'popup',
            hashtag: '#SaleMe_KokisChallenge',
            quote: 'SaleMe.lk හඳුන්වාදෙන "කොකිස් Challenge". SaleMe.lk විසින් හදුන්වා දෙන නවතම Playzone එක වෙත පිවිස විවිධ තරග වලට සහභාගී වී ලකුණු රැස් කර SaleMe මගින් නවතම ත්‍යාග රැසක් ලබාගන්න. SaleMe.lk වෙත පිවිස Log/Register වී SaleMe playZone වෙත පිවිසෙන්න, නැත්නම් https://goo.gl/AgqfpY click කරන්න. ',
            href: '{{asset('')}}playzone/home',
        }, function (response) {
//            alert(response);
            var response = true;
            if (response) {
                $(".userdetailsform").show();
                $(".success-share").show();
                $(".sharediv").hide();
                $("#canvas").hide();

                var score = {{$score}};
                var marks = Math.floor(score / 10);
                var member_id = {{getPrivetMemberId()}}
                //                 alert(member_id);
                var data = {'member_id': member_id, 'marks': marks}
                $.ajax({
                    type: "POST",
                    url: '/playzone/games/kokis-challenge/save-score/' + member_id,
                    data: data,
                    success: function (res) {
//                        alert(res.success);
                        if (res.success) {
                            $.unblockUI();
                            $('.userdetailscompleted').show();
                            $('.details-add-success').show();
                            $('.details-add-success2').show().html(res.message);
                            $('.userdetailsform').hide();
                            setTimeout(function () {
                                window.location='/playzone/play';
                            },1500)
                        } else {
                            window.location='/playzone/games/error/kokis-challage/101';
                        }

                    }
                });
            } else {
                $(".error-share").show();
            }
        });

    }

</script>
{{Html::script('game_assets/spark/js/index.js')}}

</html>
