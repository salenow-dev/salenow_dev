@extends('layouts.gamezone')
@section('title', 'SaleMe PlayZone | Error | Play Share Win')
@section('content')
    <style>
        #demo {
            text-align: left;
            color: #111;
            float: right;
            font-size: 20px;
            margin-bottom: 15px;
        }

        .ending {
            text-align: left;
            color: #111;
            float: left;
            margin: 0px;
            font-size: 20px;
            margin-bottom: 15px;
        }

        .team-player img {
            width: 100%;
            margin-bottom: 10px;
        }

        .team-player i {
            background: #eee;
            padding: 5px 13px;
        }

        .iframe {
            min-height: 500px;
            width: 100%;
            margin-bottom: 10px;
        }

        .iframediv {
            background: #eee;
        }

        .title-1 {
            margin-top: 15px;
            font-size: 23px;
            font-weight: 400;
        }

        .radio-btn-div {
            padding-left: 25px;
        }
        .clear-fix{
            clear: both;
        }
        .error ul li{
            background: #e4afaf;
            padding: 5px 10px;
            margin-bottom: 5px;
        }
        .error ul{
            text-align: left;
            list-style: none;
        }
    </style>
    <div class="wrapper">
        <div class="page-header page-header-small price-header" style="min-height: 75px !important;">
            <div class="page-header-image" data-parallax="true" style="background-image: url('../assets/img/bg6.jpg');">
            </div>

        </div>
        <div class="section section-notifications" id="notifications">
            <div class="alert alert_display" role="alert">
                <div class="container">
                    <div class="alert-icon">
                        <i class="now-ui-icons ui-2_like icon"></i>
                    </div>
                    <span class="msg"></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">
                                <i class="now-ui-icons ui-1_simple-remove"></i>
                            </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="section section-team text-center">
            <div class="container">
                <!--<h2 class="title">Here is our team</h2>-->
                <div class="team">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="team-player">
                                <p id="coin" class="demo text-info"></p>
                                {{ Html::image('images/salenow/banner.gif', 'saleme.lk', array('class' => 'img-responsive')) }}
                                <br>
                                <p class="ending">
                                    <i class="ion-ios-close-outline"></i>&nbsp;Following errors found in {{str_replace('-',' ',$game)}}</p>
                                <div class="clear-fix"></div>
                                <div class="error">
                                    <ul>
                                        <li>{{$error}}</li>
                                    </ul>
                                </div>


                            </div>
                        </div>
                        <div class="col-md-4 iframediv">
                            <h4 class="title-1">See advertisments on SaleMe.lk</h4>
                            <iframe src="https://saleme.lk/blog/getadstoblog" frameborder="0" class="iframe"></iframe>
                            <br>
                            {{ Html::image('images/salenow/banners/saleme-mobile-banner-gif.gif', 'saleme.lk', array('class' => 'img-responsive visible-xs')) }}
                        </div>
                    </div>

                </div>
            </div>
        </div>
@endsection