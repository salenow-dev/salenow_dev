<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>SaleMe Cube Champ</title>

  <link href="{{asset('')}}gamezone/game_assests/2048/style/main.css" rel="stylesheet" type="text/css">
  <link rel="shortcut icon" href="favicon.ico">
  <link rel="apple-touch-icon" href="meta/apple-touch-icon.png">
  <link rel="apple-touch-startup-image" href="meta/apple-touch-startup-image-640x1096.png" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)"> <!-- iPhone 5+ -->
  <link rel="apple-touch-startup-image" href="meta/apple-touch-startup-image-640x920.png"  media="(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 2)"> <!-- iPhone, retina -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
  <meta name="viewport" content="width=device-width, target-densitydpi=160dpi, initial-scale=1.0, maximum-scale=1, user-scalable=no, minimal-ui">
</head>
<body>
  <div class="container">
      {{ Html::image('images/salenow/banner.gif', 'saleme.lk', array('class' => 'img-responsive','style'=>'max-width:100%')) }}
    <div class="heading">

      <h1 class="title">
          <a class="navbar-brand" href="{{asset('')}}" rel="tooltip">
              <img src="{{asset('images/salenow/saleme-logo.png')}}" alt="" class="saleme-logo">
          </a>CUBE CHAMP</h1>

    </div>

    <div class="above-game">
        <div class="scores-container">
            <div class="score-container">0</div>
            <div class="best-container">0</div>
            <a class="restart-button">New Game</a>
        </div>
    </div>
      {{--<p>Join the numbers and get to the <strong>2048 tile!</strong></p>--}}
      {{--<div class="above-game">--}}
      {{--<p class="game-intro">Join the numbers and get to the <strong>2048 tile!</strong></p>--}}
    {{--</div>--}}

    <div class="game-container">
      <div class="game-message">
        <p></p>
        <div class="lower">
	        <a class="keep-playing-button">Keep going</a>
          <a class="retry-button">Try again</a>
          <a class="end-now">End Now</a>
        </div>
      </div>

      <div class="grid-container">
        <div class="grid-row">
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
        </div>
        <div class="grid-row">
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
        </div>
        <div class="grid-row">
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
        </div>
        <div class="grid-row">
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
          <div class="grid-cell"></div>
        </div>
      </div>

      <div class="tile-container">

      </div>
    </div>
      <div style="margin-top: 10px;">
          {{ Html::image('images/salenow/banner.gif', 'saleme.lk', array('class' => 'img-responsive','style'=>'max-width:100%')) }}
      </div>
  </div>

  <footer class="footer footer-default">
      <div class="container">
          <nav>
              <ul>
                  <li>
                      <a href="https://saleme.lk/" target="_blank">
                          Back to SaleMe.lk
                      </a>
                  </li>
                  <li>
                      <a href="https://saleme.lk/ads/" target="_blank">
                          All Ads
                      </a>
                  </li>
                  <li>
                      <a href="https://saleme.lk/terms-conditions" target="_blank">
                          Terms
                      </a>
                  </li>
                  <li>
                      <a href="https://saleme.lk/privacy-policy" target="_blank">
                          Privacy
                      </a>
                  </li>
              </ul>
          </nav>
          <div class="copyright">
              © 2018 <a href="http://www.invisionapp.com" target="_blank">Saleme.lk (Pvt) Ltd</a>. All right reserved.

          </div>
      </div>
  </footer>
  {{ Html::script('gamezone/js/core/jquery.3.2.1.min.js') }}
  <script>
      $(document).ready(function () {

          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
      });
  </script>


  <script src="{{asset('')}}gamezone/game_assests/2048/js/bind_polyfill.js"></script>
  <script src="{{asset('')}}gamezone/game_assests/2048/js/classlist_polyfill.js"></script>
  <script src="{{asset('')}}gamezone/game_assests/2048/js/animframe_polyfill.js"></script>
  <script src="{{asset('')}}gamezone/game_assests/2048/js/keyboard_input_manager.js"></script>
  <script src="{{asset('')}}gamezone/game_assests/2048/js/html_actuator.js"></script>
  <script src="{{asset('')}}gamezone/game_assests/2048/js/grid.js"></script>
  <script src="{{asset('')}}gamezone/game_assests/2048/js/tile.js"></script>
  <script src="{{asset('')}}gamezone/game_assests/2048/js/local_storage_manager.js"></script>
  <script src="{{asset('')}}gamezone/game_assests/2048/js/game_manager.js"></script>
  <script src="{{asset('')}}gamezone/game_assests/2048/js/application.js"></script>
</body>
</html>
