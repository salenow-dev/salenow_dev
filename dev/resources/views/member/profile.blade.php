@extends('layouts.member.member_layout')
@section('title', 'Member profile | SaleMe.lk')
@section('content')
    {{ Html::style('css/salenow/layout/select-search/bootstrap-select.min.css') }}
    {{ Html::style('css/salenow/layout/my-account-custom-css.css') }}
    <div class="row login_box visible-xs">
        {{--mobile--}}
        <div class="col-md-12 col-xs-12" align="center">
            <a href="#" class="btn edit-btn pull-right visible-xs edit-btn-mobile"><span
                        class="fa fa-pencil"></span></a>
            <div class="line">
                <h3 class="user-name-xs">{{!empty($memberdetails->first_name) ? ucfirst($memberdetails->first_name) : '' }}   {{!empty($memberdetails->last_name) ? ucfirst($memberdetails->last_name) : '' }}</h3>
                <hr>
            </div>
            <div class="outter">
                @if(!empty($memberdetails->avatar))
                    <img src="{{$memberdetails->avatar}}" class="img-circle">
                @else
                    <div class="sellerName ">
                        <span class="avatar">{{ucfirst (substr($memberdetails->first_name, 0, 1))}}</span>
                    </div>
                @endif
            </div>
            <p class="m-0 white-font-xs">&nbsp;&nbsp;{{!empty($memberdetails->email) ? $memberdetails->email : '' }}</p>
            @foreach($member->membercontacts as $contacts)
                <p class="m-0 white-font-xs">&nbsp;&nbsp;{{$contacts->contactnumber}}</p>
            @endforeach

            @if(!empty($memberdetails->district_name) && !empty($memberdetails->cityname))
                <p class="m-0 white-font-xs">&nbsp;&nbsp;
                    {{!empty($memberdetails->district_name) ? $memberdetails->district_name : '' }}
                    {{!empty($memberdetails->cityname) ? ' - '.$memberdetails->cityname : '' }}</p>
            @endif
        </div>
        {{--desktop--}}
        <div class="col-md-6 col-xs-4 col-sm-8 follow line {{(Request::segment(1))==='myprofile' ? 'active-item-mobile' : ''}}"
             align="center">
            <a href="/myprofile" class="">My Account</a>
        </div>
        <div class="col-md-6 col-xs-4 follow line {{(Request::segment(1))==='my_ads' ? 'active-item-mobile' : ''}}"
             align="center">
            {{--<a href="/my_ads" class=""><span></span></a>--}}
            <div class="dropdown">
                <span class="dropdown-toggle" data-toggle="dropdown">My Ads<span class="caret"></span></span>
                <ul class="dropdown-menu">
                    <li><a href="{{asset('')}}my_ads">All Ads</a></li>
                    <li class="divider"></li>
                    <li><a href="{{asset('')}}my_ads?state=confirm">Published Ads</a></li>
                    <li><a href="{{asset('')}}my_ads?state=pending">Pending Ads</a></li>
                    <li><a href="{{asset('')}}my_ads?state=cancel">Canceled Ads</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-6 col-xs-4 follow line" align="center">
            <a href="javascript:void(0);" class="verify-toggle">Verify Number</a>
        </div>
    </div>
    <section class="profile-content">
        <div class="col-md-12 m-b-10 pd-0-xs pd-l-0 pd-r-0">
            <div class=" user-details-section hidden-xs">
                <div class="cont-bg user-details-div">
                    <div class="pull-left my-acc-pro-pic-div col-lg-1 col-sm-1 no-padding">
                        @if(!empty($memberdetails->avatar))
                            <img src="{{$memberdetails->avatar}}" class="img-circle img-responsive">
                        @else
                            {{--<img src="images/user.png" class="img-circle img-responsive">--}}
                            <div class="sellerName ">
                                <span class="avatar">{{ucfirst (substr($memberdetails->first_name, 0, 1))}}</span>
                            </div>
                        @endif
                    </div>
                    <div class="pull-left pd-lr-10 pd-r-0-xs my-acc-details-div col-lg-5 col-sm-8">
                        <a href="#" class="btn btn-managead edit-btn pull-right visible-xs"><span
                                    class="lnr lnr-pencil"></span></a>
                        <h4 class="m-b-0 user-name">
                            {{!empty($memberdetails->first_name) ? ucfirst($memberdetails->first_name) : '' }}   {{!empty($memberdetails->last_name) ? ucfirst($memberdetails->last_name) : '' }}
                        </h4>
                        <div class="sellerName ">
                            <span class="sellerbk3 color-fff">
                                <i class="ion-ios-email-outline"></i>
                            </span>
                        </div>
                        <p class="m-0 color-fff">&nbsp;&nbsp;{{!empty($memberdetails->email) ? $memberdetails->email : '' }}
                            @if($memberdetails->status == 'active')
                                <i class="ion-checkmark-circled verify-icon" title="Verified"></i>
                            @else
                                <i class="ion-android-warning warnig-icon " title="Not verified"></i>
                            @endif
                        </p>
                        @if(!empty($memberdetails->district_name) && !empty($memberdetails->cityname))
                            <div class="sellerName ">
                            <span class="sellerbk2 color-fff">
                                <i class="ion-ios-location-outline"></i>
                            </span>
                            </div>
                            <p class="m-0 line-2-3 color-fff">&nbsp;&nbsp;
                                {{--{{dd($memberdetails)}}--}}
                                {{!empty($memberdetails->district_name) ? $memberdetails->district_name : '' }}
                                {{!empty($memberdetails->cityname) ? ' - '.$memberdetails->cityname : '' }}</p>
                        @endif
                        <div class="visible-sm">
                            @foreach($member->membercontacts as $contacts)
                                <div class="sellerName ">
                            <span class="sellerbk3 color-fff">
                                <i class="ion-ios-telephone-outline"></i>
                            </span>
                                </div>
                                <p class="m-0 color-fff">&nbsp;&nbsp;{{$contacts->contactnumber}}<i
                                            class="ion-checkmark-circled verify-icon"></i></p>
                            @endforeach
                        </div>
                    </div>
                    <div class="pull-left pd-lr-10 col-lg-4 col-sm-8 hidden-sm">
                        <h4 class="m-t-0">&nbsp;</h4>
                        @foreach($member->membercontacts as $contacts)
                            <div class="sellerName ">
                            <span class="sellerbk3 color-fff">
                                <i class="ion-ios-telephone-outline"></i>
                            </span>
                            </div>
                            <p class="m-0 color-fff">&nbsp;
                                {{'0'.substr($contacts->contactnumber, 2, 2).'-'.substr($contacts->contactnumber, 4, 3).'-'.substr($contacts->contactnumber,7)}}
                                <i class="ion-checkmark-circled verify-icon"></i></p>
                        @endforeach
                    </div>
                    <div class="pull-right hidden-xs">
                        <button type="button" class="btn btn-managead green-btn edit-btn"> Edit&nbsp;<span
                                    class="lnr lnr-pencil"></span></button>
                    </div>
                    <div class="clear-fix"></div>
                </div>
            </div>
            <div class=" edit-section m-b-10">
                <div class="cont-bg user-details-div-2">
                    <div class="pull-left my-acc-pro-pic-div">
                        @if(!empty($memberdetails->image_link))
                            <img src="{{$memberdetails->image_link}}" class="img-circle">
                        @endif
                    </div>
                    <form id="update_form" action="" method="post" role="form">

                        <div class="col-lg-4">

                            <div class="form-group">
                                <p class="edit-user-title">Edit User Details<i
                                            class="ion-ios-close-outline pull-right edit-close-btn"></i></p>
                            </div>
                            <div class="form-group">
                                <label class="lable-form" for="name">First Name</label>
                                <input type="text"
                                       value="{{!empty($memberdetails->first_name) ? $memberdetails->first_name : '' }}"
                                       class="form-control" id="first_name" name="first_name" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <label class="lable-form" for="name">Last Name</label>
                                <input type="text"
                                       value="{{!empty($memberdetails->last_name) ? $memberdetails->last_name : '' }}"
                                       class="form-control" id="last_name" name="last_name" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <label class="lable-form" for="address">Gender</label>
                                <select name="gender" class="selectpicker show-tick form-control"
                                        data-live-search="false">
                                    <option @if(!empty($memberdetails->sex) && $memberdetails->sex =='male') selected
                                            @endif value="male">Male
                                    </option>
                                    <option @if(!empty($memberdetails->sex) && $memberdetails->sex =='female') selected
                                            @endif value="female">Female
                                    </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="lable-form" for="address">Your City</label>
                                <div class="row hover-eee">
                                    <div class="col-md-6 col-sm-6 pd-r-5">
                                        <select id="district_id" name="district_id"
                                                class="selectpicker show-tick form-control" data-live-search="true">
                                            <option default selected value=""> Select location</option>
                                            @foreach ($locationdistrictslist as $district)
                                                <option value="{{$district->id}}" {{($member->district_id && $district->id == $member->district_id)? 'selected':''}}
                                                        {{((!empty($editAd->district_id)) && $district->id == $editAd->district_id)? 'selected':''}}>
                                                    {{$district->district_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <label class="error" for="district_id" generated="true"
                                               style="display: none;"></label>
                                    </div>
                                    <div class="col-md-6 col-sm-6 pd-l-5 ">
                                        <select id="city_id" name="city_id"
                                                class="selectpicker show-tick form-control"
                                                data-live-search="true">
                                            {{$locationcities}}
                                            @if(!empty($locationcities))
                                                @foreach ($locationcities as $city)
                                                    <option value="{{$city->id}}" {{($member->city_id && $city->id == $member->city_id)? 'selected':''}}
                                                            {{((!empty($editAd->city_id)) && $city->id == $editAd->city_id)? 'selected':''}}>
                                                        {{$city->city_name }}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <div class="clear-fix"></div>
                                        <label class="error" for="city_id" generated="true"
                                               style="display: none;"></label>
                                    </div>

                                </div>
                            </div>
                            <br>
                            <div class="messager"></div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="lable-form">Phone</label>
                                <div id="contact_nombers">
                                    @foreach($member->membercontacts as $contacts)
                                        <p class="m-0 mr-b-5" id="contact_{{$contacts->id}}">
                                            <span class="label label-default">
                                                {{'0'.substr($contacts->contactnumber, 2, 2).'-'.substr($contacts->contactnumber, 4, 3).'-'.substr($contacts->contactnumber,7)}}
                                            </span>
                                            <i class="ion-checkmark-circled verify-icon" title="Verified"></i>

                                            <a href="javascript:;" href="#" uid="{{$contacts->id}}" class="removephone"
                                               title="Delete"><i class="ion-trash-b del-icon"></i></a>
                                        </p>
                                    @endforeach

                                    {{--veryfi pendin numbers--}}
                                    @if(count($member->otpnumbers))
                                        <p>Pending Numbers</p>
                                        @foreach($member->otpnumbers as $contacts)
                                            <p class="m-0 mr-b-5" id="contact_{{$contacts->id}}">
                                            <span class="label label-default">
                                                {{'0'.substr($contacts->number, 2, 2).'-'.substr($contacts->number, 4, 3).'-'.substr($contacts->number,7)}}
                                            </span>
                                                <a href="javascript:;" href="#" uid="{{$contacts->id}}"
                                                   onclick="removePendingOPT({{$contacts->id}})"
                                                   title="Delete"><i class="ion-ios-trash-outline del-icon"></i></a>
                                            </p>
                                        @endforeach
                                    @endif
                                    {{--END veryfi pendin numbers--}}
                                </div>
                                <a href="#" class="add-more-link" data-toggle="modal" data-target=".add-phone-number">+
                                    Add new number</a>
                            </div>
                            <div class="form-group email-address-show">
                                <label class="lable-form">Email Address</label>
                                <p class="m-0">
                                    <span class="label label-default">{{$memberdetails->email}}</span>
                                    @if($memberdetails->status == 'active')
                                        <i class="ion-checkmark-circled verify-icon" title="Verified"></i>
                                    @else
                                        <i class="ion-android-warning warnig-icon " title="Not verified"></i>
                                    @endif

                                </p>
                            </div>
                        </div>
                    </form>

                    <div class="col-md-12 col-xs-12 text-right btn-div">
                        <button type="button" id="memberupdate" class="btn btn-managead green-btn">Update</button>
                        <button type="button" class="btn btn-managead green-btn" data-toggle="modal"
                                data-target=".change-password">Change Password
                        </button>
                        <button type="button" class="btn btn-managead green-btn edit-close-btn" id="close_btn_edit">
                            Cancel
                        </button>
                    </div>
                    <div class="clear-fix"></div>
                </div>
            </div>
            <div class="clear-fix"></div>
            {{--verify phone--}}
            @if(count($member->otpnumbers))
                <div id="message" class="alert alert-info" style="display: none"></div>
                <div class="verify-phone-section cont-bg">
                    <form id="otpVerify" method="post" action="member/verifynumberMember">
                        <div class="col-md-3 col-xs-12">
                            <div class="form-group verify-otp-txt">
                                <label class="lable-form verify-lable" for="address">Verify Number</label>
                            </div>
                        </div>
                        <div class="col-md-5 col-xs-7">
                            <div class="form-group verify-otp-txt">
                                <input type="hidden" value="{{getMemberId()}}" name="member_id">
                                <input type="text" value="" class="form-control valid verify-otp-txt-field"
                                       required="required"
                                       name="number_verify" id="number_verify" placeholder="Enter Pin Code*"
                                       aria-required="true"
                                       aria-invalid="false">
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-5">
                            <div class="form-group verify-otp-txt  pd-l-0-xs    ">
                                <input type="submit" value="Verify"
                                       class="btn btn-managead btn-block btn-success btn-verify"
                                       placeholder="Name" aria-required="true" aria-invalid="false">
                            </div>
                        </div>
                        <div class="col-md-1 col-xs-2 btn-close-verify">
                            <p><i class="ion-ios-close-outline pull-right verify-no-close"></i></p>
                        </div>

                    </form>
                    <div class="clear-fix"></div>
                </div>
            @endif
            <div class="clear-fix"></div>
            {{--verify phone--}}
            <div class=" adbox-section">
                <div class="col-md-6 col-xs-6 pd-0-xs mr-b-10-xs pd-l-0">
                    <div class="card blue-card">
                        <div class="card-content">
                            <div class="card-header-blue">
                                <h4 class="card-heading">My Free Ads</h4>
                            </div>
                            <div class="card-body">
                                <div class="adnumb">
                                    <p>{{!empty($adscount['0']->addcount)?$adscount['0']->addcount : '0'}}</p>
                                </div>
                                <p class="ad-numbers">My Active Ads<span
                                            class="pull-right">{{!empty($adscount['0']->addcount)?$adscount['0']->addcount : '0'}}</span>
                                </p>
                                <p class="ad-numbers">My Spamed Ads<span
                                            class="pull-right">{{!empty($spamedadscount['0']->addcount)?$spamedadscount['0']->addcount : '0'}}</span>
                                </p>


                                <nav class="nav-tabs">
                                    <a href="{{asset('')}}my_ads">
                                        <button type="button" class="btn btn-block btn-managead blue-btn">Manage My All
                                            Ads
                                        </button>
                                    </a>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-6 pd-0-xs mr-b-10-xs pd-r-0">
                    <div class="card green-card">
                        <div class="card-content">
                            <div class="card-header-green">
                                <h4 class="card-heading">My Premium Ads</h4>
                            </div>
                            <div class="card-body">
                                <div class="adnumb">
                                    <p>0</p>
                                </div>
                                <p class="ad-numbers">Pin To Top Ads<span class="pull-right">
                                        {{(!empty($boostedcount))?$boostedcount[0]->addcount:'0'}}
                                    </span></p>
                                <p class="ad-numbers">Bump To Top Ads<span class="pull-right">0</span></p>


                                <nav class="nav-tabs">
                                    <a href="#" class="disabled-link">
                                        <button type="button" class="disabled btn btn-block btn-managead green-btn-2">
                                            Manage
                                            Premium Ads
                                        </button>
                                    </a>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if( !getMember()->premiummember)
                <div class="col-md-12 col-xs-12 pd-0-xs ">
                    <div class="banner-audience">
                        <div class="audience-details text-center">
                            <span class="q-audi-logo hidden-xs"></span>
                            <p>Reach audiences across 180+ categories <br>with high purchase intent</p>
                            <a href="/premium/create" class="btn btn-primary">Be a Premium</a>
                        </div>
                    </div>
                </div>
            @else
                @if( !getMember()->premiummember->verified)
                    <div class="col-md-12 col-xs-12 pd-0-xs hidden-sm">
                        <div class="banner-audience">
                            <div class="audience-details text-center">
                                <span class="q-audi-logo hidden-xs"></span>
                                <strong>
                                    <p>Your request for Become <br>Premium Member is in Progress! <br>We will get back
                                        to you
                                        <br>as soon as possible.</p>
                                </strong>
                            </div>
                        </div>
                    </div>
                @endif
            @endif
        </div>
    </section>
    <!--bootstap select with search-->
    {{ Html::script('js/salenow/plugin/select-search/bootstrap-select.min.js') }}
    {{ Html::script('js/salenow/plugin/jquery.validate.js') }}
    {{ Html::script('js/location/district_city.js') }}
    {{ Html::script('js/memeber/memeber.js') }}
    <script>
        function removePendingOPT(id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var data = {'id': id};
            $.ajax({
                type: "POST",
                url: '/delete_otp/' + id,
                data: data,
                success: function (res) {
                    if (res.message == 'success') {
                        $('#contact_' + id).toggle(400);
                        return false;
                    }
                }
            });
        }

        $(document).ready(function () {
            $("#otpVerify").validate({
                rules: {
                    number_verify: {
                        required: true,
                        number: true
                    },
                }, submitHandler: function (form) {
                    $.ajax({
                        type: "POST",
                        url: '/verifynumberMember',
                        data: $("#otpVerify").serialize(),
                        success: function (res) {
                            if (res.response.message == 'verified') {
                                $("#message").html('Number Verified! Thank You.');
                                setTimeout("window.location.href='myprofile';", 1000);
                            }
                            if (res.response.error != '') {
                                $("#message").html(res.response.error)
                                $("#message").toggle();
                            }
                        }
                    });
                }
            });
        });
    </script>

@endsection

