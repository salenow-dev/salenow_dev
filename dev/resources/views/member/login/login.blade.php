@extends('layouts.adddetails')
@section('title', 'Member Login')
@section('content')
    {{ Html::style('css/salenow/layout/login-page.css') }}
    <div class="container">
        <div class="modal-dialog modal-md omb_login login-register">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center"><span class="no-1"><i
                                    class="lnr lnr-mustache linier-icon"></i> </span> &nbsp; Login / Register</h4>
                </div>
                <div class="col-md-12 pd-0-xs">
                    <div class="panel panel-login">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-6">
                                    <a href="{{asset('')}}member-login" class="active">Login</a>
                                    {{--<a href="#" class="active" id="login-form-link">Login</a>--}}

                                </div>
                                <div class="col-xs-6">
                                    <a href="{{asset('')}}member/register">Register</a>
                                    {{--<a href="#" id="register-form-link">Register</a>--}}
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="login-form">
                                        @if($errors->any())
                                            <div class="alert alert-danger">
                                                @foreach($errors->all() as $error)
                                                    <i class="fa fa-exclamation-triangle"></i>&nbsp;{{$error}}
                                                @endforeach
                                            </div>
                                        @endif
                                        <div class="message"></div>
                                        <div class="omb_row-sm-offset-6 omb_socialButtons">
                                            <form action="/facebook/authorize" target="" method="get"
                                                  id="facebook_form">
                                                <div class="col-xs-6 col-sm-6 margin-0">
                                                    <button type="submit" class="btn btn-lg btn-block omb_btn-facebook">
                                                        <i class="fa fa-facebook visible-xs"></i>
                                                        <span class="">Facebook</span>
                                                    </button>
                                                </div>
                                            </form>
                                            <form action="/google/authorize" target="" method="get" id="google_form">
                                                <div class="col-xs-6 col-sm-6 margin-0">
                                                    <button type="submit" class="btn btn-lg btn-block omb_btn-google">
                                                        <i class="fa fa-google-plus visible-xs"></i>
                                                        <span class="">Google+</span>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="clear-fix"></div>
                                        <div class="omb_loginOr">
                                            <div class="col-xs-12 col-sm-12 margin-0">
                                                <hr class="omb_hrOr">
                                                <span class="omb_spanOr">or</span>
                                            </div>
                                        </div>
                                        <form id="memberlogin_form" action="/memberlogin" method="post" role="form">
                                            {{csrf_field()}}
                                            <div class="form-group">
                                                <input type="text" name="email" id="email" tabindex="1"
                                                       class="form-control" placeholder="Email"
                                                       value="{{ old('email') }}">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" name="password" id="password"
                                                       tabindex="2" class="form-control" placeholder="Password">
                                            </div>
                                            <div class="form-group remember-me-div">
                                                <div class="checkbox checkbox-primary">
                                                    <input id="checkbox2" type="checkbox">
                                                    <label for="checkbox2">&nbsp;Remember Me</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-6 pull-right margin-0">
                                                        <input type="submit" name="login-submit" id="login-submit"
                                                               tabindex="4" class="form-control btn btn-login"
                                                               value="Log In">
                                                    </div>
                                                    <div class="col-sm-6 col-xs-6 pull-left forget-pass">
                                                        <div class="">
                                                            <a href="/member/password/reset" tabindex="5"
                                                               class="forgot-password">Forgot Password?</a>
                                                        </div>
                                                    </div>
                                                    <div class="clear-fix"></div>
                                                    <div class="col-md-12">
                                                        <p class="agree-txt-1">
                                                            By login you agree to
                                                            <a href="{{asset('')}}terms-conditions"
                                                               title="View Terms & Conditions"
                                                               target="_blank">Terms-Conditions</a>
                                                            and <a href="{{asset('')}}privacy-policy"
                                                                   title="View Privacy Policy"
                                                                   target="_blank">Privacy-Policy</a>

                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--<div class="col-md-12 pd-0-xs bg-yellow loging-bottom">--}}
                    {{--<h4 id="log-reg-title" class="text-center">Login with Saleme.lk</h4>--}}
                    {{--<div class="col-md-4 col-xs-4 text-center">--}}
                        {{--<img src="{{asset('')}}/images/manage.png" class="img-responsive log-reg-bottom-img"--}}
                             {{--alt="Start post your ads">--}}
                        {{--<p class="text-center">Start post your ads.</p>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-4 col-xs-4 text-center">--}}
                        {{--<img src="{{asset('')}}/images/target.png" class="img-responsive log-reg-bottom-img"--}}
                             {{--alt="Boost your ads">--}}
                        {{--<p class="text-center">Boost your ads.</p>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-4 col-xs-4 text-center">--}}
                        {{--<img src="{{asset('')}}/images/tags.png" class="img-responsive log-reg-bottom-img"--}}
                             {{--alt="View, manage your ads">--}}
                        {{--<p class="text-center">View, manage your ads</p>--}}
                    {{--</div>--}}
                    {{--<div class="clear-fix"></div>--}}
                {{--</div>--}}
                <div class="clear-fix"></div>
            </div>
        </div>
    </div>
    {{ Html::script('js/salenow/plugin/jquery.validate.js') }}
    <script>
        // login form
        $("#memberlogin_form").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                }
            }
        });

        //member register
        $("#register_form").validate({
            rules: {
                signup_username: {
                    required: true,
                },
                signup_phone: {
                    number: true,
                    required: true,
                },
                signup_email: {
                    required: true,
                    email: true
                },
                signup_pass: {
                    required: true,
                },
                pass_confirmation: {
                    required: true,
                    equalTo: "#signup_pass"
                }
            },
            messages: {
                signup_username: {
                    required: "Please enter username",
                },
                signup_phone: {
                    required: "Please enter mobile number",
                },
                signup_email: {
                    required: "Please enter email",
                },
                signup_pass: {
                    required: "Please enter password",
                },
                pass_confirmation: {
                    equalTo: "does not match password",
                }
            }
        });

    </script>
@endsection