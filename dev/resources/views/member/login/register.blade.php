@extends('layouts.adddetails')
@section('title', 'Member Register')
@section('content')
    {{ Html::style('css/salenow/layout/login-page.css') }}
    <div class="container">
        <div class="modal-dialog modal-md omb_login login-register">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center"><span class="no-1"><i
                                    class="lnr lnr-mustache linier-icon"></i> </span> &nbsp; Login / Register</h4>
                </div>
                <div class="col-md-12 pd-0-xs">
                    <div class="panel panel-login">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-6">
                                    <a href="{{asset('')}}member-login">Login</a>
                                    {{--<a href="#" class="active" id="login-form-link">Login</a>--}}
                                </div>
                                <div class="col-xs-6">
                                    <a href="{{asset('')}}member/register" class="active">Register</a>
                                    {{--<a href="#" id="register-form-link">Register</a>--}}

                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">

                                    {{--register--}}
                                    <div id="register-form">
                                        <div class="col-md-12 reg-success-thumb">
                                            <p class="text-center">
                                                <i class="lnr lnr-thumbs-up thumb-up-image" aria-hidden="true"></i>
                                            </p>
                                        </div>
                                        <div class="messager"></div>
                                        <form id="register_form" method="post" role="form" action="/memberregister">
                                            {{csrf_field()}}
                                            <div class="form-group">
                                                <input type="text" name="signup_username" id="signup_username"
                                                       tabindex="1"
                                                       class="form-control" placeholder="Name *" value="{{ old('signup_username') }}">
                                            </div>
                                            <div class="form-group">
                                                <input type="email" name="signup_email" id="signup_email" tabindex="1"
                                                       class="form-control" placeholder="Email Address *" value="{{ old('email') }}">
                                            </div>
                                            <div class="form-group position-relative">
                                                <input type="password" name="signup_pass" id="signup_pass" tabindex="2"
                                                       class="form-control" placeholder="Password *">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" name="pass_confirmation" id="pass_confirmation"
                                                       tabindex="2" class="form-control"
                                                       placeholder="Confirm Password *">
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-6 pull-right">
                                                        <input type="submit" name="login-submit" id="btn_submit_signup"
                                                               tabindex="4" class="form-control btn btn-login"
                                                               value="REGISTER">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear-fix"></div>
                                            <div class="col-md-12">
                                                <p class="agree-txt-1">
                                                    By login you agree to
                                                    <a href="{{asset('')}}terms-conditions"
                                                       title="View Terms & Conditions"
                                                       target="_blank">Terms-Conditions</a>
                                                    and <a href="{{asset('')}}privacy-policy"
                                                           title="View Privacy Policy"
                                                           target="_blank">Privacy-Policy</a>

                                                </p>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--<div class="col-md-12 pd-0-xs bg-yellow loging-bottom">--}}
                    {{--<h4 id="log-reg-title" class="text-center">Login with Saleme.lk</h4>--}}
                    {{--<div class="col-md-4 col-xs-4 text-center">--}}
                        {{--<img src="{{asset('')}}/images/manage.png" class="img-responsive log-reg-bottom-img"--}}
                             {{--alt="Start post your ads">--}}
                        {{--<p class="text-center">Start post your ads.</p>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-4 col-xs-4 text-center">--}}
                        {{--<img src="{{asset('')}}/images/target.png" class="img-responsive log-reg-bottom-img"--}}
                             {{--alt="Boost your ads">--}}
                        {{--<p class="text-center">Boost your ads.</p>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-4 col-xs-4 text-center">--}}
                        {{--<img src="{{asset('')}}/images/tags.png" class="img-responsive log-reg-bottom-img"--}}
                             {{--alt="View, manage your ads">--}}
                        {{--<p class="text-center">View, manage your ads</p>--}}
                    {{--</div>--}}
                    {{--<div class="clear-fix"></div>--}}
                {{--</div>--}}
                <div class="clear-fix"></div>
            </div>
        </div>
    </div>
    {{ Html::script('js/salenow/plugin/jquery.validate.js') }}
    <script>
        // login form
        $("#memberlogin_form").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                }
            }
        });

        //member register
        $("#register_form").validate({
            rules: {
                signup_username: {
                    required: true,
                },
                signup_phone: {
                    number: true,
                    required: true,
                },
                signup_email: {
                    required: true,
                    email: true
                },
                signup_pass: {
                    required: true,
                },
                pass_confirmation: {
                    required: true,
                    equalTo: "#signup_pass"
                }
            },
            messages: {
                signup_username: {
                    required: "Please enter username",
                },
                signup_phone: {
                    required: "Please enter mobile number",
                },
                signup_email: {
                    required: "Please enter email",
                },
                signup_pass: {
                    required: "Please enter password",
                },
                pass_confirmation: {
                    equalTo: "does not match password",
                }
            }
        });

    </script>
@endsection