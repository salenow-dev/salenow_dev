@extends('layouts.member.member_layout')
@section('title', 'My Ads | Saleme.lk')
@section('content')
    {{ Html::style('css/salenow/layout/my-account-custom-css.css') }}
    {{--mobile--}}
    <div class="row login_box visible-xs">
        <div class="col-md-12 col-xs-12" align="center">
            <div class="line">
                <h3 class="user-name-xs">{{!empty($memberdetails->first_name) ? ucfirst($memberdetails->first_name) : '' }}   {{!empty($memberdetails->last_name) ? ucfirst($memberdetails->last_name) : '' }}</h3>
                <hr>
            </div>
            <div class="outter">
                @if(!empty($memberdetails->avatar))
                    <img src="{{$memberdetails->avatar}}" class="img-circle">
                @else
                    <div class="sellerName ">
                        <span class="avatar">{{ucfirst (substr($memberdetails->first_name, 0, 1))}}</span>
                    </div>
                @endif
            </div>
            <p class="m-0 white-font-xs">&nbsp;&nbsp;{{!empty($memberdetails->email) ? $memberdetails->email : '' }}</p>
            @foreach($member->membercontacts as $contacts)
                <p class="m-0 white-font-xs">&nbsp;&nbsp;{{$contacts->contactnumber}}</p>
            @endforeach
            @if(!empty($memberdetails->district_name) && !empty($memberdetails->cityname))
                <p class="m-0 white-font-xs">&nbsp;&nbsp;
                    {{!empty($memberdetails->district_name) ? $memberdetails->district_name : '' }}
                    {{!empty($memberdetails->cityname) ? ' - '.$memberdetails->cityname : '' }}</p>
            @endif
        </div>
        <div class="col-md-6 col-xs-4 follow line {{(Request::segment(1))==='myprofile' ? 'active-item-mobile' : ''}}"
             align="center">
            <a href="/myprofile" class="">My Account</a>
        </div>
        <div class="col-md-6 col-xs-4 follow line {{(Request::segment(1))==='my_ads' ? 'active-item-mobile' : ''}}"
             align="center">
            {{--<a href="/my_ads" class=""><span></span></a>--}}
            <div class="dropdown">
                <span class="dropdown-toggle" data-toggle="dropdown">My Ads<span class="caret"></span></span>
                <ul class="dropdown-menu">
                    <li><a href="{{asset('')}}my_ads">All Ads</a></li>
                    <li class="divider"></li>
                    <li><a href="{{asset('')}}my_ads?state=confirm">Published Ads</a></li>
                    <li><a href="{{asset('')}}my_ads?state=pending">Pending Ads</a></li>
                    <li><a href="{{asset('')}}my_ads?state=cancel">Canceled Ads</a></li>

                </ul>
            </div>
        </div>
        <div class="col-md-6 col-xs-4 follow line" align="center">
            <a href="#" class=" ">Payments</a>
        </div>
    </div>
    <div class="">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Search Ads</h3>
                </div>
                <div class="panel-body">
                    <p>Search any advertisement by its Title or Category, you posted on SaleMe.lk .</p>
                    <div class="col-md-6">
                        <form id="searchAgent" class="form-inline" role="form" method="get" action="/myads/search">
                            <div class="form-group ">
                                <div class="input-group stylish-input-group">
                                    <input type="text" name="ad_info" value="{{ old('ad_info') }}" class="form-control"
                                           placeholder="Ad Title, Ref No, Location or Date">
                                    <span class="input-group-addon ">
                                    <button type="submit">
                                    <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                    </span>
                                </div>
                            </div>
                            <div class="clear-fix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="">
        <section class="profile-content">
            <div class="col-lg-12 col-xs-12 pd-0-sm">
                <div class="user-adz-div panel panel-default">
                    @if(count($allads)==0)
                        @if(empty($state))
                            <br><br>
                            <div class="col-lg-12 top-mar-20">
                                <div class="text-center">
                                    <h4 class="promo-head2">You have not posted any Ads yet</h4>
                                    <h4 class="promo-head2">Click "<strong>Post Free Ad</strong>" button to post your ad
                                    </h4>
                                    <br>
                                    <a href="addtype">
                                        {{ Html::image('images/salenow/place-Ad-01-01.png', 'saleme.lk logo', array('class' => 'img-responsive center-block plce-ad')) }}
                                    </a>
                                </div>
                            </div>
                            <div class="clear-fix"></div>
                        @else
                            <br><br>
                            <div class="col-lg-12 top-mar-20">
                                <div class="text-center">
                                    <h4 class="promo-head2">You have not {{ucwords($state)}} Ads.</h4>
                                    <a href="/my_ads" class="goto-link">Go to my all ads</a>
                                    <h4 class="display-block">Or</h4>
                                    <h4 class="promo-head2">Click "<strong>Post Free Ad</strong>" button to post your ad
                                    </h4>
                                    <br>
                                    <a href="addtype">
                                        {{ Html::image('images/salenow/place-Ad-01-01.png', 'saleme.lk logo', array('class' => 'img-responsive center-block plce-ad')) }}
                                    </a>
                                </div>
                            </div>
                            <div class="clear-fix"></div>
                        @endif
                    @else
                        <h2 class="my-ad-h">&nbsp;
                            @if(count($allads)!=0)
                                <span class="lnr my-ad-ico lnr-rocket"></span>
                                {{!(empty($state))? ucwords($state) : 'All'}} Ads
                            @endif
                        </h2>
                        <hr>
                        <div class="panel-body">
                            <!--My add single item-->
                            @foreach($allads as $datas)
                                <a href="{{($datas->status != 'cancel' && $datas->status !='deleted')? asset('').'ad/'.$datas->slug:
                                ($datas->status == 'draft' || $datas->status == 'cancel' || $datas->status =='deleted')?'/ad/edit/'.$datas->slug:'javascript:;'}}"
                                   id="{{$datas->adid}}" class="">
                                    <div class="row myadds-list-item div-{{$datas->adid}}">
                                        <div class="col-md-2 col-xs-3 thumb-img-div">
                                            <p class="label visible-xs
                                            @if($datas->status === 'pending')
                                            {{'label-warning'}}
                                            @elseif($datas->status === 'confirm')
                                            {{'label-success'}}
                                            @elseif($datas->status === 'cancel' || $datas->status === 'draft' || $datas->status === 'deleted')
                                            {{'label-danger'}}
                                            @endif ">{{($datas->status=='confirm')? 'Published' : ucwords($datas->status)}}</p>
                                            {{-- START Featured Image--}}
                                            @if($datas->featuredimage!='')
                                                <img class="img-responsive pull-left"
                                                     src="{{asset('')}}salenow/images/uploads/{{$datas->adid}}/thumb/{{$datas->featuredimage}}">
                                            @else
                                                <img class="img-responsive pull-left"
                                                     src="{{asset('')}}images/salenow/no_image.jpg">
                                            @endif
                                            {{-- END Featured Image--}}
                                            @if($datas->ad_boost_status != 'none')
                                                <p class="pined-to-top-badge-xs visible-xs">Pined to top</p>
                                            @endif
                                            @if(!is_null($datas->vouchercode_status) && !$datas->vouchercode_status)
                                                <p class="pined-pending-badge-xs visible-xs">Boost Pending</p>
                                            @endif
                                        </div>
                                        <div class="col-md-6 col-xs-9 pull-left ad-des-div">
                                            <h3 class="myadd-title">{{$datas->adtitle}}
                                                {{--<small class=" hidden-xs ">Ad Id : ({{$datas->ad_referance}})</small>--}}
                                                <span class="label status-lable pull-right hidden-xs
                                                    @if($datas->status === 'pending')
                                                {{'label-warning'}}
                                                @elseif($datas->status === 'confirm')
                                                {{'label-success'}}
                                                @elseif($datas->status === 'cancel' || $datas->status === 'draft' || $datas->status === 'deleted')
                                                {{'label-danger'}}
                                                @endif ">{{($datas->status=='confirm')? 'Published' : ucwords($datas->status)}}</span>
                                            </h3>
                                            <p class="item-loc-cat">
                                                <span class="item-price">Rs {{$datas->price > 0 ? number_format($datas->price) : '' }}</span>
                                            </p>
                                            <p class="item-loc-cat">
                                                <i class="ion-location"></i> {{$datas->district_name}}
                                                , {{$datas->city}} &nbsp; | &nbsp;
                                                <i class="ion-ios-pricetag"></i> {{$datas->category}}</p>
                                            <p class="item-loc-cat">
                                                <span class="item-added-date">Posted on:</span> {{ date('F d, Y', strtotime($datas->created_at))}}
                                                &nbsp;
                                                @if($datas->status === 'confirm')
                                                    {{--                                                    {!!' | &nbsp;<span class="item-added-date"> Active till:</span> '.date('F d, Y', strtotime($datas->updated_at)).'</p>'!!}--}}
                                                @elseif($datas->status === 'cancel')
                                                    {!!' | &nbsp;<span class="item-added-date"> Canceled On:</span> '.date('F d, Y', strtotime($datas->updated_at))!!}
                                                @endif
                                            </p>
                                            @if($datas->status == 'draft')
                                                <p class="">
                                                        <span class="col-md-12 col-xs-12 label label-danger"
                                                              style="font-weight: 300 !important;white-space: normal !important;">
                                                            This ad not yet Published. Add Phone Numbers & Post
                                                        </span>
                                                </p>
                                            @endif
                                            @if($datas->status == 'confirm' )
                                                @if(is_null($datas->vouchercode_status))
                                                    <a type="button"
                                                       href="/myprofile/{{$datas->slug}}/pin-to-top"
                                                       class="btn visible-xs visible-sm pin-btn btn-sm">Promote Ad
                                                    </a>
                                                @endif
                                            @endif
                                            @if(!empty($datas->spams))
                                                <p><a data-toggle="collapse"
                                                      href="#collapseExample_{{$datas->adid}}"
                                                      class="label status-lable label-danger">Spam Reasons&nbsp;<i
                                                                class="fa fa-hand-o-down"></i>&nbsp;</a>
                                                </p>
                                                <div class="collapse" id="collapseExample_{{$datas->adid}}">
                                                    <ul class="list-tags">
                                                        @foreach($datas->spams as $spam)
                                                            <li><a href="#" class="spam-reason"><i
                                                                            class="fa fa-caret-right"></i>&nbsp;<span>{{$spam->reason}}</span>
                                                                </a></li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <style>
                                                .spam-reason{
                                                    background-color: #fddbdb;
                                                    color: #630000;
                                                    padding: 3px 8px;
                                                    font-size: 11px;
                                                    border-radius: 4px;
                                                }
                                            </style>
                                            {{--{{$datas->reason}}--}}
                                        </div>
                                        <div class="col-md-4 col-xs-12 ad-des-bottom">
                                            @php
                                                $disabled = '';
                                                    if($datas->status === 'pending'){
                                                        $disabled = 'disabled-link';
                                                    }
                                            @endphp
                                            {{--desktop--}}
                                            <div class="edit-delete pull-right {{$disabled}} hidden-xs hidden-sm">
                                                <ul>
                                                    <li><a href="/ad/edit/{{$datas->slug}}">Edit</a></li>
                                                    <li>|</li>
                                                    <li>
                                                        <a ad-id="{{$datas->adid}}" id="delete_{{$datas->adid}}"
                                                           status="delete" class="delete-record"
                                                           onClick="notyConfirm({{$datas->adid}})">Delete
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            {{--mobile--}}
                                            <div class="edit-delete pull-right {{$disabled}} visible-xs visible-sm">
                                                <ul>
                                                    <li><a href="/ad/edit/{{$datas->slug}}">Edit</a></li>
                                                    <li>|</li>
                                                    <li>
                                                        <a ad-id="{{$datas->adid}}" id="delete_{{$datas->adid}}"
                                                           status="delete" class="delete-record"
                                                           onClick="notyConfirm({{$datas->adid}})">Delete
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="clear-fix"></div>
                                            @if($datas->status == 'confirm' )
                                                @if(is_null($datas->vouchercode_status))
                                                    <div class="pin-btn-set {{$disabled}} hidden-xs hidden-sm">
                                                        <ul>
                                                            <li>
                                                                <a type="button"
                                                                   href="/myprofile/{{$datas->slug}}/pin-to-top"
                                                                   class="btn  pin-btn btn-sm">Promote Ad
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                @endif
                                            @endif
                                            <div class="pin-btn-set {{$disabled}} hidden-xs hidden-sm">
                                                <ul>
                                                    @if($datas->ad_boost_status != 'none')
                                                        <li class="pd-t-30">
                                                            <span class="boosted-badge hidden-xs "><img
                                                                        src="{{asset('')}}images/salenow/pined.png"></span>
                                                        </li>
                                                    @endif
                                                    @if(!is_null($datas->vouchercode_status) && !$datas->vouchercode_status)
                                                        <li class="pd-t-30">
                                                            <span class="boost-pending-badge hidden-xs">Boost Pending</span>
                                                        </li>
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </a>
                            @endforeach
                        </div>
                        <div class="row">{{$allads->appends(Illuminate\Support\Facades\Input::except('page'))->links('vendor.pagination.custom')}}</div>
                    @endif
                </div>
            </div>
        </section>
    </div>
    <!--category icons-->
    <div class="modal fade deleresons" id="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="ion-ios-close-empty"></i>
                    </button>
                    <h4 class="modal-title text-center"><span class="no-1"><i class="ion-ios-trash-outline"></i> </span>
                        &nbsp; Select Delete Reason</h4>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <ul>
                            @if(!empty($delete_resons))
                                @foreach($delete_resons as $reson)
                                    <li><label><input value="{{$reson->id}}" name="delete_reson_id"
                                                      type="radio" {{($reson->id ==1)? 'checked' : ''}}>
                                            &nbsp;{{$reson->reason}}
                                        </label></li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn primary " data-dismiss="modal">Cancel</button>
                    <button class="btn  delete-c" id="delete_btn1">Delete Ad</button>
                </div>
                <div class="clear-fix"></div>
            </div>
        </div>
    </div>
@endsection
{{--<script type="text/javascript" src="{{ asset("js/member/ad-delete.js") }}"></script>--}}
<style>
    .stylish-input-group .input-group-addon {
        background: white !important;
    }

    .stylish-input-group .form-control {
        border-right: 0;
        box-shadow: 0 0 0;
        border-color: #ccc;
    }

    .stylish-input-group button {
        border: 0;
        background: transparent;
    }

    /*css*/
    .deleresons .modal-header {
        background: #821900 !important;
    }

    .delete-c {
        background: #821900 !important;
        color: #fff !important;

    }
</style>