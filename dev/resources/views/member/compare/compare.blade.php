@extends('layouts.saleme')
@section('content')
    <style>
        .compare-div h3 {
            margin: 0px;
            padding: 10px;
            font-size: 15px;
            text-align: center;
        }

        .compare-div h2 {
            background: #fff;
            padding: 10px;
            font-size: 15px;
            margin-top: 5px;
            margin-bottom: 5px;
            font-weight: 300;
        }

        .compare-div {
            background: #f5f5f5;
            border: 1px solid #ddd;
            padding-bottom: 15px;
            min-height: 500px;
        }

        .img-div-inner {
            padding-top: 15px;
            padding-bottom: 15px;
        }

        .search-compare {
            height: 40px !important;
            font-size: 15px !important;
            background: #fff;
            border-radius: 0;
            font-family: 'Work Sans', sans-serif !important;
            font-weight: 300;
            color: #000 !important;
            border: 1px solid #eee;
            z-index: 0 !important;
        }

        .search-compare-button {
            height: 40px;
            width: 54px;
            font-size: 17px;
            padding-top: 9px;
            background: #fea502;
            color: #fff;
            font-weight: 300;
            border: 1px solid #fea502;
            border-radius: 0px;
        }

        .div-search {
            margin-top: 3px;
        }

        .change-btn {
            border-radius: 0px;
            margin-top: 10px;
            font-size: 15px;
            background: #fea502;
            color: #fff;
            font-weight: 600;
        }

        /*----*/
        .autocomplete {
            /*the container must be positioned relative:*/
            position: relative;
            display: inline-block;
        }

        input {
            border: 1px solid transparent;
            background-color: #f1f1f1;
            padding: 10px;
            font-size: 16px;
        }

        input[type=text] {
            /*background-color: #f1f1f1;*/
            width: 100%;
        }

        input[type=submit] {
            background-color: DodgerBlue;
            color: #fff;
            cursor: pointer;
        }

        .autocomplete-items {
            position: absolute;
            border: 1px solid #d4d4d4;
            border-bottom: none;
            border-top: none;
            z-index: 99;
            /*position the autocomplete items to be the same width as the container:*/
            top: 100%;
            left: 0;
            right: 0;
        }

        .autocomplete-items div {
            padding: 10px;
            cursor: pointer;
            background-color: #fff;
            border-bottom: 1px solid #d4d4d4;
        }

        .autocomplete-items div:hover {
            /*when hovering an item:*/
            background-color: #e9e9e9;
        }

        .autocomplete-active {
            /*when navigating through the items using the arrow keys:*/
            background-color: DodgerBlue !important;
            color: #ffffff;
        }
    </style>
    <div class="container text-center visible-md visible-lg">
        <div class="image-div">
            <!-- /21634329919/slm_leaderboard_970x90_atf -->
            <div id='div-gpt-ad-1511940698204-1'>
                <script>
                    googletag.cmd.push(function () {
                        googletag.display('div-gpt-ad-1511940698204-1');
                    });
                </script>
            </div>
        </div>
    </div>
    <div class="container ">
        <section>
            <div class="row hidden-xs">
                <div class="col-md-12">
                    <p class="breadcrumb-text">
                        <a href="{{URL::to('/')}}"><i class="ion-ios-home-outline"></i> </a>/&nbsp;&nbsp;
                        Compare Ads
                    </p>
                </div>
            </div>
            <div class="row cont-bg" style="padding: 15px;">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <h3 class="product-title">Compare ads</h3>
                    <p class="text-muted">Select products to compare</p>
                </div>
                <div class="clear-fix"></div>
                <div class="col-lg-6 compare-div">
                    <h3>Select product 1</h3>
                    <div class="input-group div-search">
                        <input class="form-control search-compare" placeholder="Search Product Here" type="text"
                               id="search1" name="search2">
                        <span class="input-group-btn">
                            <button class="btn search-compare-button" type="button"><span
                                        class="lnr lnr-magnifier"></span></button>
                        </span>
                    </div>
                </div>

                <div class="col-lg-6 compare-div">
                    <h3>Select product 2</h3>
                    <h2>Title : <b>Hyundai Getz 2007</b></h2>
                    <div class="col-md-12 cont-bg img-div-inner">
                        <img src="/salenow/images/uploads/6054/saleme_5a38e24cc1f76.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="clear-fix"></div>
                    <h2>Brand : <b>Hyundai Getz 2007</b></h2>
                    <h2>Model : <b>Hyundai Getz 2007</b></h2>
                    <h2>Condition : <b>Hyundai Getz 2007</b></h2>
                    <h2>Transmission : <b>Hyundai Getz 2007</b></h2>
                    <h2>Fuel Type : <b>Hyundai Getz 2007</b></h2>
                    <h2>Mileage : <b>Hyundai Getz 2007</b></h2>
                    <h2>Engine Capacity : <b>Hyundai Getz 2007</b></h2>
                    <button class="btn btn-lg change-btn pull-right">Change Product</button>
                </div>

            </div>
        </section>
    </div>
    <script>
        function autocomplete(inp, arr) {
            /*the autocomplete function takes two arguments,
             the text field element and an array of possible autocompleted values:*/
            var currentFocus;
            /*execute a function when someone writes in the text field:*/
            inp.addEventListener("input", function (e) {
                var a, b, i, val = this.value;
                /*close any already open lists of autocompleted values*/
                closeAllLists();
                if (!val) {
                    return false;
                }
                currentFocus = -1;
                /*create a DIV element that will contain the items (values):*/
                a = document.createElement("DIV");
                a.setAttribute("id", this.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items");
                /*append the DIV element as a child of the autocomplete container:*/
                this.parentNode.appendChild(a);
                /*for each item in the array...*/
                for (i = 0; i < arr.length; i++) {
                    /*check if the item starts with the same letters as the text field value:*/
                    if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                        /*create a DIV element for each matching element:*/
                        b = document.createElement("DIV");
                        /*make the matching letters bold:*/
                        b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                        b.innerHTML += arr[i].substr(val.length);
                        /*insert a input field that will hold the current array item's value:*/
                        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                        /*execute a function when someone clicks on the item value (DIV element):*/
                        b.addEventListener("click", function (e) {
                            /*insert the value for the autocomplete text field:*/
                            inp.value = this.getElementsByTagName("input")[0].value;
                            /*close the list of autocompleted values,
                             (or any other open lists of autocompleted values:*/
                            closeAllLists();
                        });
                        a.appendChild(b);
                    }
                }
            });
            /*execute a function presses a key on the keyboard:*/
            inp.addEventListener("keydown", function (e) {
                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {
                    /*If the arrow DOWN key is pressed,
                     increase the currentFocus variable:*/
                    currentFocus++;
                    /*and and make the current item more visible:*/
                    addActive(x);
                } else if (e.keyCode == 38) { //up
                    /*If the arrow UP key is pressed,
                     decrease the currentFocus variable:*/
                    currentFocus--;
                    /*and and make the current item more visible:*/
                    addActive(x);
                } else if (e.keyCode == 13) {
                    /*If the ENTER key is pressed, prevent the form from being submitted,*/
                    e.preventDefault();
                    if (currentFocus > -1) {
                        /*and simulate a click on the "active" item:*/
                        if (x) x[currentFocus].click();
                    }
                }
            });
            function addActive(x) {
                /*a function to classify an item as "active":*/
                if (!x) return false;
                /*start by removing the "active" class on all items:*/
                removeActive(x);
                if (currentFocus >= x.length) currentFocus = 0;
                if (currentFocus < 0) currentFocus = (x.length - 1);
                /*add class "autocomplete-active":*/
                x[currentFocus].classList.add("autocomplete-active");
            }

            function removeActive(x) {
                /*a function to remove the "active" class from all autocomplete items:*/
                for (var i = 0; i < x.length; i++) {
                    x[i].classList.remove("autocomplete-active");
                }
            }

            function closeAllLists(elmnt) {
                /*close all autocomplete lists in the document,
                 except the one passed as an argument:*/
                var x = document.getElementsByClassName("autocomplete-items");
                for (var i = 0; i < x.length; i++) {
                    if (elmnt != x[i] && elmnt != inp) {
                        x[i].parentNode.removeChild(x[i]);
                    }
                }
            }
            /*execute a function when someone clicks in the document:*/
            document.addEventListener("click", function (e) {
                closeAllLists(e.target);
            });
        }
//-------------------------------------------------------------
        $(document).ready(function () { 

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
        
        $('#search1').keyup(function () {
            var keyword= $('#search1').val();
            if (keyword.length == 4){
                var data = {'keyword': keyword};
                $.ajax({
                    type: "post",
                    url: '/get/search-ajax-data/' + keyword,
                    data: data,
                    success: function (res) {
                        var adtitles= res;
//                    alert(res);
//                    alert(res);
//                    $('.deleresons').modal('hide');
//                    $(classname).css('opacity', 0.5);
//                    noty({text: 'Ad deleted Successfully!', layout: 'topRight', type: 'success', timeout: 3000,});
//                    var delay = 5000;
//                    setTimeout(function () {
//                        window.location = '/my_ads';
//                    }, delay);
                        autocomplete(document.getElementById("search1"), adtitles);
                    }
                });
            }

        });

//        var countries = ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua & Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia & Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central Arfrican Republic", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica", "Cote D Ivoire", "Croatia", "Cuba", "Curacao", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauro", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "North Korea", "Norway", "Oman", "Pakistan", "Palau", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre & Miquelon", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Korea", "South Sudan", "Spain", "Sri Lanka", "St Kitts & Nevis", "St Lucia", "St Vincent", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad & Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks & Caicos", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States of America", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"];

        /*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/

    </script>

@endsection