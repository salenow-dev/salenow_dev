@extends('layouts.saleme-shortfooter')
@section('title', 'Select Category | SaleMe.lk')
@section('content')
    <?php $addtype = !empty($_GET['type']) ? $_GET['type'] : ''; ?>
    <div class="container cont-bg min-height-mobile min-height-desk">
        <div class="col-md-12 no-padding selectcat-block">
            <ul class="adstype adtype-inner">
                <a href="?type=sell">
                    <li class="col-md-2 col-xs-6">
                        <input id="want_sell_vehicle" name="vehicle-ad-type"
                               <?php echo ($addtype == 'sell') ? 'checked' : ''; ?> class="i-want"
                               type="radio"
                               onclick="goToURL('parent','selectcat/?type=sell');return document.returnValue">
                        <label for="want_sell"><i class="ion-ios-checkmark-outline active-icon"></i>I want
                            to sell</label>
                    </li>
                </a>
                <a href="?type=buy">
                    <li class="col-md-2 col-xs-6">
                        <input id="want_buy_vehicle" name="vehicle-ad-type"
                               <?php echo ($addtype == 'buy') ? 'checked' : ''; ?> class="i-want" type="radio"
                               onclick="goToURL('parent','selectcat/?type=buy');return document.returnValue">
                        <label for="want_buy"><i class="ion-ios-checkmark-outline active-icon"></i>I want to
                            buy</label>
                    </li>
                </a>
                <a href="?type=rent">
                    <li class="col-md-2 col-xs-6">
                        <input id="want_rent_vehicle" name="vehicle-ad-type" class="i-Offer"
                               <?php echo ($addtype == 'rent') ? 'checked' : ''; ?> type="radio"
                               onclick="goToURL('parent','selectcat/?type=rent');return document.returnValue">
                        <label for="want_rent"><i class="ion-ios-checkmark-outline active-icon"></i>I Offer
                            for rent</label>
                    </li>
                </a>
                <a href="?type=lookforrent">
                    <li class="col-md-2 col-xs-6">
                        <input id="look_rent_vehicle" name="vehicle-ad-type" class="i-Offer"
                               <?php echo ($addtype == 'lookforrent') ? 'checked' : ''; ?> type="radio"
                               onclick="goToURL('parent','selectcat/?type=lookforrent');return document.returnValue">
                        <label for="look_rent"><i class="ion-ios-checkmark-outline active-icon"></i>I Look
                            for rent</label>
                    </li>
                </a>
                <a href="?type=jobs">
                    <li class="col-md-4 col-xs-6">
                        <input id="jobs" name="vehicle-ad-type" class="i-Offer"
                               <?php echo ($addtype == 'jobs') ? 'checked' : ''; ?> type="radio"
                               onclick="window.location.href='/selectcat?type=jobs#select_job';">
                        {{--onclick="goToURL('parent','/selectcat?type=jobs#select_job');">--}}
                        <label for="jobs"><i class="ion-ios-checkmark-outline active-icon"></i>Post a Job</label>
                    </li>
                </a>

            </ul>
        </div>
        <div class="clear-fix"></div>
        <div class="row top-mar-50">

            <div class="col-lg-3 pd-r-0 select-cat-left-bar-div-main col-xs-12 col-sm-4">
                <p class="sel-cat-title visible-xs ">Select Category</p>
                <ul class="nav nav-tabs tabs-left sideways select-cat-left-bar">
                    <li class="active hidden-xs">
                        <a href="#select-category" data-toggle="tab">Select Category</a>
                    </li>
                    @if($categories != '')
                        @foreach($categories as $data)
                            <li>
                                <a href="#select_{{ $data->category_code }}" data-toggle="tab">
                                    <i class="{{ $data->icon }} hidden-xs"></i> {{ $data->category_name }}
                                </a>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
            <div class="col-lg-9 select-cat-right-bar col-xs-7 hide-on-xs-visible-click col-sm-8">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="select-category">
                        <h2 class="hidden-xs">Post Free Ad </h2>
                        <img src="images/salenow/postbanner.png" class="img-responsive hidden-xs">
                    </div>
                    @if($alldata != '')
                        @foreach($alldata as $datas=>$value)

                            <div class="tab-pane" id="select_{{$value[0]->category_code}}">
                                <h2 class="hidden-xs">Post Free Ad </h2>
                                <div class="clear-fix"></div>
                                <p class="sel-cat-title">Select Subcategory</p>
                                {{--<div class="list-group sub-category">--}}
                                <ul class="subcat-ul">
                                    @if($value != '')
                                        @foreach($value as $svalue)
                                            @if($svalue->sub_category_code== 'jobs-in-sl')
                                                <li>
                                                    <a href="/post-job/jobs-in-sl/new-job?type=jobs"
                                                       class="">{{$svalue->sub_category_name}}</a>
                                                </li>
                                            @else
                                                <li>
                                                    <a href="/post-ad/{{$svalue->catid}}/{{$svalue->sub_category_code}}/{{$addtype}}"
                                                       class="">{{$svalue->sub_category_name}}</a>
                                                </li>
                                            @endif
                                        @endforeach
                                    @endif
                                </ul>
                                {{--</div>--}}
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <div class="top-mar-50"></div>
    </div>
    <!-- select sub-category tabs-->
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.tabs').tab();
            $('.select-cat-left-bar a').click(function () {
                $('.select-cat-left-bar-div-main').removeClass('col-xs-12').addClass('col-xs-5');
                $('.hide-on-xs-visible-click').show(400);
            });
        });

        var activeTab = "";
        var url = window.location.href;
        activeTab = url.substring(url.indexOf("#") + 1);
        if (activeTab != "") {
            $("#" + activeTab).addClass("active in");
            $('a[href="#' + activeTab + '"]').tab('show');
        }
    </script>
    <!--tabs-->
@endsection