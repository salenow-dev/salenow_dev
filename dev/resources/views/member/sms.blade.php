@extends('layouts.app')
@section('title', '| Member Requests for Become Premium')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Send Messages</strong></h3>
                </div>
                <div class="panel-body">
                    <form id="searchMember" class="form-horizontal" role="form" method="post"
                          action="/admin/member/sendmesseage">
                        {{csrf_field()}}
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="number" id="number" placeholder="94 XXXXXXX">
                        </div><div class="row"></div>


                        <div class="form-group col-md-4">

                            <label for="comment">Your Message:</label>
                            <textarea class="form-control text-count" rows="5" name="message" id="message"></textarea>
                        </div>
                        {{--<button type="submit" name="search" value="searchAll" class="btn btn-primary">Search Member&nbsp;<i--}}
                        {{--class="fa fa-search"></i></button>--}}
                        <div id="counter"></div><div style="margin-top: 20px"></div>
                        <p style="color: red">* 320 Maximum Characters </p>
                        <button type="submit" name="search" value="searchMemAll" class="btn btn-primary">Send SMS&nbsp;</button>
                    </form>

                </div>
            </div>
        </div>

    </div>
    <script>
        function count(){
            var txtVal = $('.text-count').val();
            var words = txtVal.trim().replace(/\s+/gi, ' ').split(' ').length;
            var chars = txtVal.length;
            if(chars===0){words=0;}
            $('#counter').html('<br>'+words+' words and '+ chars +' characters');
        }
        count();

        $('textarea').on('keyup propertychange paste', function(){
            count();
        });
    </script>

@endsection