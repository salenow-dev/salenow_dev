@extends('layouts.member.premium')
@section('premium_content')
    {{ Html::style('premium_assets/frontend/dist/css/lightgallery.css') }}
    <style type="text/css">

        .demo-gallery > ul {
            margin-bottom: 0;
        }

        .demo-gallery > ul > li {
            /*float: left;*/
            margin-bottom: 15px;
            /*margin-right: 20px;*/
            /*width: 200px;*/
        }

        .demo-gallery > ul > li a {
            border: 3px solid #FFF;
            border-radius: 3px;
            display: block;
            overflow: hidden;
            position: relative;
            float: left;
        }

        .demo-gallery > ul > li a > img {
            -webkit-transition: -webkit-transform 0.15s ease 0s;
            -moz-transition: -moz-transform 0.15s ease 0s;
            -o-transition: -o-transform 0.15s ease 0s;
            transition: transform 0.15s ease 0s;
            -webkit-transform: scale3d(1, 1, 1);
            transform: scale3d(1, 1, 1);
            height: 100%;
            width: 100%;
        }

        .demo-gallery > ul > li a:hover > img {
            -webkit-transform: scale3d(1.1, 1.1, 1.1);
            transform: scale3d(1.1, 1.1, 1.1);
        }

        .demo-gallery > ul > li a:hover .demo-gallery-poster > img {
            opacity: 1;
        }

        .demo-gallery > ul > li a .demo-gallery-poster {
            background-color: rgba(0, 0, 0, 0.1);
            bottom: 0;
            left: 0;
            position: absolute;
            right: 0;
            top: 0;
            -webkit-transition: background-color 0.15s ease 0s;
            -o-transition: background-color 0.15s ease 0s;
            transition: background-color 0.15s ease 0s;
        }

        .demo-gallery > ul > li a .demo-gallery-poster > img {
            left: 50%;
            margin-left: -10px;
            margin-top: -10px;
            opacity: 0;
            position: absolute;
            top: 50%;
            -webkit-transition: opacity 0.3s ease 0s;
            -o-transition: opacity 0.3s ease 0s;
            transition: opacity 0.3s ease 0s;
        }

        .demo-gallery > ul > li a:hover .demo-gallery-poster {
            background-color: rgba(0, 0, 0, 0.5);
        }

        .demo-gallery .justified-gallery > a > img {
            -webkit-transition: -webkit-transform 0.15s ease 0s;
            -moz-transition: -moz-transform 0.15s ease 0s;
            -o-transition: -o-transform 0.15s ease 0s;
            transition: transform 0.15s ease 0s;
            -webkit-transform: scale3d(1, 1, 1);
            transform: scale3d(1, 1, 1);
            height: 100%;
            width: 100%;
        }

        .demo-gallery .justified-gallery > a:hover > img {
            -webkit-transform: scale3d(1.1, 1.1, 1.1);
            transform: scale3d(1.1, 1.1, 1.1);
        }

        .demo-gallery .justified-gallery > a:hover .demo-gallery-poster > img {
            opacity: 1;
        }

        .demo-gallery .justified-gallery > a .demo-gallery-poster {
            background-color: rgba(0, 0, 0, 0.1);
            bottom: 0;
            left: 0;
            position: absolute;
            right: 0;
            top: 0;
            -webkit-transition: background-color 0.15s ease 0s;
            -o-transition: background-color 0.15s ease 0s;
            transition: background-color 0.15s ease 0s;
        }

        .demo-gallery .justified-gallery > a .demo-gallery-poster > img {
            left: 50%;
            margin-left: -10px;
            margin-top: -10px;
            opacity: 0;
            position: absolute;
            top: 50%;
            -webkit-transition: opacity 0.3s ease 0s;
            -o-transition: opacity 0.3s ease 0s;
            transition: opacity 0.3s ease 0s;
        }

        .demo-gallery .justified-gallery > a:hover .demo-gallery-poster {
            background-color: rgba(0, 0, 0, 0.5);
        }

        .demo-gallery .video .demo-gallery-poster img {
            height: 48px;
            margin-left: -24px;
            margin-top: -24px;
            opacity: 0.8;
            width: 48px;
        }

        .demo-gallery.dark > ul > li a {
            border: 3px solid #04070a;
        }

        .home .demo-gallery {
            padding-bottom: 80px;
        }
    </style>
    {{--<section class="slider-container hidden-xs"--}}
             {{--style="background-image: url({{asset('')}}salenow/images/prime-members/{{$memberCompany->cover_image}});"></section>--}}
    <section class="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-9">
                        <h1>Image Gallery</h1>

                    </div>
                    <div class="col-sm-3">
                        {{--<h4 class="text-muted text-right"><strong>{{count($ads)}} Ads</strong> From--}}
                            {{--<strong>{{$ads->total()}}</strong> Ads</h4>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="blog">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="blog-posts">
                        <div class="demo-gallery">
                            @if($images)
                                <ul id="lightgallery" class="list-unstyled row">
                                    @foreach($images as $img)
                                        <li class="col-xs-6 col-sm-4 col-md-4"
                                            data-src="{{asset('')}}salenow/images/prime-members/{{$memberCompany->id}}/{{$img->image}}"
                                            data-sub-html="<h4>{{title_case($memberCompany->company_name)}}</h4>">
                                            <a href="">
                                                <img class="img-responsive"
                                                     src="{{asset('')}}salenow/images/prime-members/{{$memberCompany->id}}/{{$img->image}}">
                                            </a>
                                        </li>
                                    @endforeach

                                </ul>
                            @else
                                <div class="min-height-240">
                                    <div class="col-md-12 pd-0-xs">
                                        <h4 class="">No Images Posted
                                            By {{title_case($memberCompany->company_name)}}</h4>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="footer-widgets">
        <div class="container">
            <div class="row footer-contact">
                <div class="col-sm-4">
                    <h5>Address</h5>
                    <p>
                        {{$memberCompany->company_name}}<br/>
                        {!! nl2br($memberCompany->address) !!}
                    </p>
                </div>
                <div class="col-sm-4 contact-div">
                    <h5>Contact Infomation</h5>
                    @if($memberCompany->email)
                        <div class="col-md-1 padding-0"><i class="entypo-mail"></i></div>
                        <div class="col-md-11 padding-0">{{$memberCompany->email}}</div>
                    @endif
                    @if($memberCompany->telephone)
                        <div class="col-md-1 padding-0"><i class="entypo-phone"></i></div>
                        <div class="col-md-11 padding-0 phone-number">
                            @foreach($memberCompany->telephone as $com_con)
                                {!!$com_con.'<br>'!!}
                            @endforeach
                        </div>
                    @endif
                    @if($memberCompany->mobile)
                        <div class="col-md-1 padding-0"><i class="entypo-phone"></i></div>
                        <div class="col-md-11 padding-0 phone-number">
                            @foreach($memberCompany->mobile as $mobi)
                                {!!$mobi.'<br>'!!}
                            @endforeach
                        </div>
                    @endif
                    @if($memberCompany->website)
                        <div class="col-md-1 padding-0"><i class="entypo-globe"></i></div>
                        <div class="col-md-11 padding-0"><a href="{{$memberCompany->website}}" target="_blank">Visit
                                Website</a></div>
                    @endif
                    @if($memberCompany->facebook_page)
                        <div class="col-md-1 padding-0"><i class="entypo-facebook"></i></div>
                        <div class="col-md-11 padding-0"><a href="{{$memberCompany->facebook_page}}" target="_blank">Visit facebook
                                page</a></div>
                    @endif
                    <div class="clear-fix"></div>
                </div>
                <div class="col-sm-4">
                    <h5>Direction</h5>
                    <div id="map" class="map_footer"></div>
                </div>
            </div>
        </div>
    </section>
@endsection