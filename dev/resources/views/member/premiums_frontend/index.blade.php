@extends('layouts.member.premium')
@section('premium_content')
    @if($memberCompany->cover_image ==='saleme-default-cover.jpg' OR is_null($memberCompany->cover_image))
        <section class="slider-container hidden-xs"
        style="background-image: url({{asset('')}}salenow/images/prime-members/saleme-default-cover.jpg);"></section>
    @else
        <section class="slider-container hidden-xs"
        style="background-image: url({{asset('')}}salenow/images/prime-members/{{$memberCompany->member_id.'/'.$memberCompany->cover_image}});"></section>
    @endif
    <section class="about-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h1 class="x">{{title_case($memberCompany->company_name)}}</h1>
                </div>
                <div class="col-sm-9 about-des more">
                    {{$memberCompany->description}}
                    {{--                    {{truncateText($memberCompany->description,200)}}--}}
                </div>
                @if(!empty($memberCompany->profile_image))
                    <div class="col-sm-3 hidden-xs">
                        @if($memberCompany->profile_image ==='saleme-default-profile.jpg' OR is_null($memberCompany->profile_image))
                            <img src="{{asset('')}}salenow/images/prime-members/premium_default.jpg"
                                 class="about-logo">
                        @else
                            <img src="{{asset('')}}salenow/images/prime-members/{{$memberCompany->member_id.'/'.$memberCompany->profile_image}}"
                                 class="about-logo">
                        @endif
                    </div>
                @endif
            </div>
        </div>
    </section>
    <section class="blog">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="all-ads-title">Recent ads from
                        <strong>{{title_case($memberCompany->company_name)}}</strong></h4>
                    <hr>
                    <div class="blog-posts">
                        @if(count($ads))
                            @foreach($ads as $datas)
                                <?php
                                $publishedTime = timeForUser($datas->published_at);
                                ?>
                                <a href="{{asset('')}}ad/{{$datas->slug}}" target="_blank">
                                    <div class="blog-post">
                                        <div class="col-md-12 pd-0-xs visible-xs">
                                            <h4 class="mobi-title">{{title_case($datas->adtitle)}}</h4>
                                        </div>
                                        <div class="post-thumb col-md-3 col-xs-4 pd-0-xs">
                                            @if($datas->featuredimage!='')
                                                <img src="{{asset('')}}salenow/images/uploads/{{$datas->id}}/thumb/{{$datas->featuredimage}}"
                                                     class="img-responsive"/> <span class="hover-zoom"></span>
                                            @else
                                                <img class="img-responsive pull-left"
                                                     src="{{asset('')}}images/salenow/no_image.jpg">
                                            @endif
                                        </div>
                                        <div class="post-details col-md-9 col-xs-8">
                                            <div class="visible-xs mobile-des">
                                                <span><i class="entypo-tag"></i>{{$datas->subcategory->sub_category_name}}</span>
                                                <span><i class="entypo-location"></i>{{$datas->city->city_name}}</span>
                                                <span><i class="entypo-calendar"></i> {{$publishedTime}}</span>
                                                <p class="vehicle-mileage">{{$datas->mileage > 0 ? number_format($datas->mileage).'KM' : '' }}</p>
                                                <h4 class="price">{{$datas->price > 0 ? 'Rs.'. number_format($datas->price) : '' }}</h4>
                                            </div>
                                            <h2 class="hidden-xs">{{title_case($datas->adtitle)}}</h2>
                                            <div class="post-meta hidden-xs">
                                                <div class="post-meta">
                                                    <i class="entypo-tag"></i> {{$datas->category->category_name}}
                                                    > {{$datas->subcategory->sub_category_name}}</div>
                                            </div>
                                            <div class="post-meta hidden-xs">
                                                <div class="meta-info"><i
                                                            class="entypo-location"></i> {{$datas->district->district_name}}
                                                    > {{$datas->city->city_name}}</div>
                                                <div class="meta-info"><i
                                                            class="entypo-calendar"></i> {{$publishedTime}}
                                                </div>
                                            </div>
                                            <h3 class="hidden-xs">{{$datas->price > 0 ? 'Rs.'. number_format($datas->price) : '' }}</h3>
                                            <p class="hidden-xs">{{strip_tags(truncateText($datas->description ,200))}}</p>
                                        </div>
                                    </div>
                                </a>
                            @endforeach
                            <p class="text-right more-ads-link">
                                <a href="/{{$memberCompany->slug}}/ads">More Ads from
                                    {{title_case($memberCompany->company_name)}} &rarr;</a>
                            </p>
                        @else
                            <div class=" min-height-240">
                                <div class="col-md-12 pd-0-xs">
                                    <h4 class="">No Ads Posted By {{title_case($memberCompany->company_name)}}</h4>
                                </div>
                            </div>

                        @endif

                    </div>
                </div>

            </div>
        </div>
    </section>
    @if(count($services))
        <section class="our-service-section">
            <div class="container">
                <div class="col-md-12"><h3>Our <strong>Services</strong></h3> <br></div>
                <div class="row">
                    <?php
                    $serviceCount = count($services);
                    $rows = '3';
                    if ($serviceCount == '3') {
                        $rows = '4';
                    }
                    if ($serviceCount == '2') {
                        $rows = '6';
                    }
                    if ($serviceCount == '1') {
                        $rows = '12';
                    }
                    ?>
                    @foreach($services as $service)
                        <div class="col-sm-{{$rows}}">
                            <div class="text-center">
                                <a class="image" href="{{asset('')}}{{$memberCompany->slug}}/services">
                                    @if(!empty($service->image))
                                        <img src="{{asset('')}}salenow/images/prime-members/{{$memberCompany->id}}/{{$service->image}}"
                                             class="img-circle">
                                    @else
                                        <img src="{{asset('')}}images/user.png" class="img-circle">
                                    @endif
                                </a>
                                <h4>{{$service->servicetitle}}</h4>
                                <p>
                                    <small>{{$service->service}}</small>
                                </p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif

    @if(count($images))
        <section class="">
            <div class="container-fluid">
                <div class="row">
                    @foreach($images as $img)
                        <a href="{{asset('')}}{{$memberCompany->slug}}/gallery" title="View More images ">
                            <div class="col-sm-3 col-xs-6 col-md-3 padding-0">
                                <img class="img-responsive"
                                     src="{{asset('')}}salenow/images/prime-members/{{$memberCompany->id}}/{{$img->image}}">
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        </section>
    @endif

    @if($memberCompany->teampublic AND count($memberemployees))
        <section class="our-service-section">
            <div class="container">
                <div class="col-md-12"><h3>Our <strong>Staff</strong></h3> <br></div>
                @foreach($memberemployees->chunk(4) as $row)
                    <div class="row">
                        @foreach($row as $employee)
                            <div class="col-sm-3">
                                <div class="staff-member">
                                    <a class="image" href="#">
                                        @if(!empty($employee->member->avatar))
                                            <img src="{{$employee->member->avatar}}"
                                                 class="img-circle">
                                        @else
                                            <img src="{{asset('')}}images/user.png" class="img-circle">
                                        @endif
                                    </a>
                                    <h4>{{$employee->member->first_name}}
                                        <small>{{$employee->designation}}</small>
                                    </h4>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </section>
    @endif
    {{--    {{dd($memberCompany)}}--}}
    <section class="footer-widgets">
        <div class="container">
            <div class="row footer-contact">
                <div class="col-sm-4">
                    <h5>Address</h5>
                    <p>
                        {{$memberCompany->company_name}}<br/>
                        {!! nl2br($memberCompany->address) !!}
                    </p>
                </div>
                <div class="col-sm-4 contact-div">
                    <h5>Contact Infomation</h5>
                    @if($memberCompany->email)
                        <div class="col-md-1 padding-0"><i class="entypo-mail"></i></div>
                        <div class="col-md-11 padding-0">{{$memberCompany->email}}</div>
                    @endif
                    @if($memberCompany->telephone)
                        <div class="col-md-1 padding-0"><i class="entypo-phone"></i></div>
                        <div class="col-md-11 padding-0 phone-number">
                            @foreach($memberCompany->telephone as $com_con)
                                {!!$com_con.'<br>'!!}
                            @endforeach
                        </div>
                    @endif
                    @if($memberCompany->mobile)
                        <div class="col-md-1 padding-0"><i class="entypo-phone"></i></div>
                        <div class="col-md-11 padding-0 phone-number">
                            @foreach($memberCompany->mobile as $mobi)
                                {!!$mobi.'<br>'!!}
                            @endforeach
                        </div>
                    @endif
                    @if($memberCompany->website)
                        <div class="col-md-1 padding-0"><i class="entypo-globe"></i></div>
                        <div class="col-md-11 padding-0"><a href="{{$memberCompany->website}}" target="_blank">Visit
                                Website</a></div>
                    @endif
                    @if($memberCompany->facebook_page)
                        <div class="col-md-1 padding-0"><i class="entypo-facebook"></i></div>
                        <div class="col-md-11 padding-0"><a href="{{$memberCompany->facebook_page}}" target="_blank">Visit facebook
                                page</a></div>
                    @endif
                    <div class="clear-fix"></div>
                </div>
                <div class="col-sm-4">
                    <h5>Direction</h5>
                    <div id="map" class="map_footer"></div>
                </div>
            </div>
        </div>
    </section>

    <script>
        $(document).ready(function () {
            // Configure/customize these variables.
            var showChar = 600;  // How many characters are shown by default
            var ellipsestext = "...";
            var moretext = "Show More";
            var lesstext = "Show Less";


            $('.more').each(function () {
                var content = $(this).html();

                if (content.length > showChar) {

                    var c = content.substr(0, showChar);
                    var h = content.substr(showChar, content.length - showChar);

                    var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

                    $(this).html(html);
                }

            });

            $(".morelink").click(function () {
                if ($(this).hasClass("less")) {
                    $(this).removeClass("less");
                    $(this).html(moretext);
                } else {
                    $(this).addClass("less");
                    $(this).html(lesstext);
                }
                $(this).parent().prev().toggle();
                $(this).prev().toggle();
                return false;
            });
        });
    </script>
@endsection