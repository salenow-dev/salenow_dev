@extends('layouts.member.premium')
@section('premium_content')
    <section class="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-9">
                        <h1>Services of <strong>{{title_case($memberCompany->company_name)}}</strong></h1>

                    </div>
                    <div class="col-sm-3">
                        <h4 class="text-muted text-right"><strong>{{count($services)}} Services</strong></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br>
    <section class="blog">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="blog-posts">
                        @if(count($services))
                            @foreach($services as $datas)
                                <?php
                                $publishedTime = timeForUser($datas->published_at);
                                ?>
                                <div class="blog-post">
                                    <div class="col-md-12 pd-0-xs visible-xs">
                                        <h4 class="mobi-title">{{title_case($datas->servicetitle)}}</h4>
                                    </div>
                                    <div class="post-thumb col-md-3 col-xs-4 pd-0-xs">
                                        @if($datas->image!='')
                                            <img src="{{asset('')}}salenow/images/prime-members/{{$memberCompany->id}}/{{$datas->image}}"
                                                 class="img-responsive"/> <span class="hover-zoom"></span>
                                        @else
                                            <img class="img-responsive pull-left"
                                                 src="{{asset('')}}images/salenow/no_image.jpg">
                                        @endif
                                    </div>
                                    <div class="post-details col-md-9 col-xs-8">
                                        <div class="visible-xs mobile-des">
                                            <span>{{$datas->service}}</span>
                                        </div>
                                        <h2 class="hidden-xs">{{title_case($datas->servicetitle)}}</h2>
                                        <div class="post-meta hidden-xs">
                                            <div class="">{{$datas->service}}</div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="min-height-240">
                                <div class="col-md-12 pd-0-xs">
                                    <h4 class="">No Services Posted By {{title_case($memberCompany->company_name)}}</h4>
                                </div>
                            </div>

                        @endif
                        <div class="text-center">
                            {{--{{$ads->links('vendor.pagination.custom')}}--}}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="footer-widgets">
        <div class="container">
            <div class="row footer-contact">
                <div class="col-sm-4">
                    <h5>Address</h5>
                    <p>
                        {{$memberCompany->company_name}}<br/>
                        {!! nl2br($memberCompany->address) !!}
                    </p>
                </div>
                <div class="col-sm-4 contact-div">
                    <h5>Contact Infomation</h5>
                    @if($memberCompany->email)
                        <div class="col-md-1 padding-0"><i class="entypo-mail"></i></div>
                        <div class="col-md-11 padding-0">{{$memberCompany->email}}</div>
                    @endif
                    @if($memberCompany->telephone)
                        <div class="col-md-1 padding-0"><i class="entypo-phone"></i></div>
                        <div class="col-md-11 padding-0 phone-number">
                            @foreach($memberCompany->telephone as $com_con)
                                {!!$com_con.'<br>'!!}
                            @endforeach
                        </div>
                    @endif
                    @if($memberCompany->mobile)
                        <div class="col-md-1 padding-0"><i class="entypo-phone"></i></div>
                        <div class="col-md-11 padding-0 phone-number">
                            @foreach($memberCompany->mobile as $mobi)
                                {!!$mobi.'<br>'!!}
                            @endforeach
                        </div>
                    @endif
                    @if($memberCompany->website)
                        <div class="col-md-1 padding-0"><i class="entypo-globe"></i></div>
                        <div class="col-md-11 padding-0"><a href="{{$memberCompany->website}}" target="_blank">Visit
                                Website</a></div>
                    @endif
                    @if($memberCompany->facebook_page)
                        <div class="col-md-1 padding-0"><i class="entypo-facebook"></i></div>
                        <div class="col-md-11 padding-0"><a href="{{$memberCompany->facebook_page}}" target="_blank">Visit facebook
                                page</a></div>
                    @endif
                    <div class="clear-fix"></div>
                </div>
                <div class="col-sm-4">
                    <h5>Direction</h5>
                    <div id="map" class="map_footer"></div>
                </div>
            </div>
        </div>
    </section>
@endsection