@extends('layouts.member.premium_theme_01')
@section('premium_content')
</div>
</div>

<!-- Main -->
<div id="main-wrapper">
    <div class="container">

        <div class="row">
            <div class="12u">
            <?php
            $class = "6u 12u(mobile)";
            if (count($services) == 1) {
                $class = '6u 12u(mobile) service-items-inner';
            }
            ?>
            <!-- Blog -->
                <section>
                    <header class="major">
                        <h2 class="inner-title">Our Services</h2>
                        <p><b>{{count($services)}}</b> Service(s)</p>
                    </header>
                    <div class="row services">
                        @if(count($services))
                            @foreach($services as $datas)
                                <?php
                                $publishedTime = timeForUser($datas->published_at);
                                ?>
                                <div class="{{$class}}">
                                    <a href="#">
                                        <section class="box">
                                            <div class="image featured">
                                                @if($datas->image!='')
                                                    <img src="{{asset('')}}salenow/images/prime-members/{{$memberCompany->id}}/{{$datas->image}}" alt=""/>
                                                @else
                                                    <img src="{{asset('')}}images/salenow/no_image.jpg" alt=""/>
                                                @endif

                                            </div>
                                            <header>
                                                <h3 class="text-center">{{title_case($datas->servicetitle)}}</h3>
                                                <p class="text-center">{{$datas->service}}</p>
                                            </header>
                                        </section>
                                    </a>
                                </div>
                            @endforeach
                        @endif

                    </div>
                </section>

            </div>
        </div>

    </div>
</div>
@endsection