@extends('layouts.member.premium_theme_01')
@section('premium_content')

</div>
</div>

<!-- Main -->
<div id="main-wrapper">
    <div class="container">
        <div class="row">
            <div class="12u">

                <!-- Portfolio -->
                <section>
                    <header class="major">
                        <h2 class="inner-title">All Ads From {{title_case($memberCompany->company_name)}}</h2>
                        <p><b>{{count($ads)}} Ads</b> From <b>{{$ads->total()}} Ads</b></p>
                    </header>
                    <div class="row ads">
                        @if(count($ads))
                            @foreach($ads as $datas)
                                <?php
                                $publishedTime = timeForUser($datas->published_at);
                                ?>
                                <div class="4u 12u(mobile)">
                                    <a href="{{asset('')}}ad/{{$datas->slug}}" target="_blank">
                                        <section class="box">
                                            <div class="image featured" style="height: 275px;
                                                    background: url({{($datas->featuredimage!='')?asset('').'salenow/images/uploads/'.$datas->adid.'/'.$datas->featuredimage :asset('').'images/salenow/no_image.jpg'}});background-size: cover;">
                                                {{--@if($datas->featuredimage!='')--}}
                                                    {{--<img src="{{asset('')}}salenow/images/uploads/{{$datas->adid}}/{{$datas->featuredimage}}"--}}
                                                         {{--alt=""/>--}}
                                                {{--@else--}}
                                                    {{--<img src="{{asset('')}}images/salenow/no_image.jpg" alt=""/>--}}
                                                {{--@endif--}}

                                            </div>
                                            <header>
                                                <h3 class="title">{{title_case($datas->adtitle)}}</h3>
                                                <p class="location">
                                                    <i class="ion-ios-location"></i>&nbsp;&nbsp;&nbsp;{{$datas->city}}
                                                </p>

                                                <h3 class="price">{{$datas->price > 0 ? 'Rs.'. number_format($datas->price) : 'Negotiable' }}</h3>
                                            </header>
                                            <p class="des-para">{{strip_tags(truncateText($datas->description ,30))}}</p>
                                            <footer>

                                                <p style="text-align: right; color: #666"><i class="ion-ios-time"></i>&nbsp;&nbsp;&nbsp;{{$publishedTime}}
                                                </p>
                                            </footer>
                                        </section>
                                    </a>
                                </div>
                            @endforeach
                        @else
                            <div class="12u 12u(mobile)">
                                <section class="box">
                                    <header>
                                        <h3 class="title text-center">No Ads Posted
                                            By {{title_case($memberCompany->company_name)}}</h3>

                                    </header>

                                    <footer class="text-center">
                                        <a href="{{asset('')}}ads" class="emptyads">
                                            {{ Html::image('premium_assets/frontend/images/saleme-logo.png', 'saleme.lk logo', array('class' => 'footer-saleme')) }}
                                            <p>Back To SaleMe.lk</p>
                                        </a>
                                    </footer>
                                </section>
                            </div>
                        @endif
                    </div>
                    <div class="row">
                        <div class="12u">
                            <div class="text-center">
                                {{$ads->links('vendor.pagination.custom')}}
                            </div>
                        </div>
                    </div>

                </section>


            </div>
        </div>


    </div>
</div>
@endsection
