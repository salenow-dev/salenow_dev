@extends('layouts.member.premium_theme_01')
@section('premium_content')

    <!-- Banner -->
    <div class="hidden-xs">
    @if($memberCompany->cover_image ==='saleme-default-cover.jpg' OR is_null($memberCompany->cover_image))
        <section id="banner"
                 style="background-image: url({{asset('')}}salenow/images/prime-members/saleme-default-cover.jpg);"></section>

    @else
        <section id="banner"
                 style="background-image: url({{asset('')}}salenow/images/prime-members/{{$memberCompany->member_id.'/'.$memberCompany->cover_image}});"></section>

    @endif
    </div>

    <!-- Intro -->

    <div class="twPc-div visible-xs">
        @if($memberCompany->cover_image ==='saleme-default-cover.jpg' OR is_null($memberCompany->cover_image))
        <a class="twPc-bg twPc-block"
           style="background-image: url({{asset('')}}salenow/images/prime-members/saleme-default-cover.jpg);"></a>
        @else
        <a class="twPc-bg twPc-block"
           style="background-image: url({{asset('')}}salenow/images/prime-members/{{$memberCompany->member_id.'/'.$memberCompany->cover_image}});"></a>
        <div>
        @endif

            @if($memberCompany->facebook_page)
                <div class="twPc-button">
                    <a href="{{$memberCompany->facebook_page}}" target="_blank" title="Facebook" class="btn btn-facebook btn-lg"><i class="fa fa-facebook fa-fw"></i> Follow Us</a>
                </div>
            @endif


            <a  href="" class="twPc-avatarLink">
                @if(!empty($memberCompany->profile_image))
                    @if($memberCompany->profile_image ==='saleme-default-profile.jpg' OR is_null($memberCompany->profile_image))
                        <img src="{{asset('')}}salenow/images/prime-members/premium_default.jpg" class="twPc-avatarImg">
                    @else
                        <img src="{{asset('')}}salenow/images/prime-members/{{$memberCompany->member_id.'/'.$memberCompany->profile_image}}" class="twPc-avatarImg">
                    @endif
                @endif
            </a>

            <div class="twPc-divUser">
                <div class="twPc-divName">
                    {{$memberCompany->company_name}}
                </div>
                <p class="addres-top">
				{{str_replace('\n',' ',$memberCompany->address )}}
			</p>
            </div>

        </div>
    </div>

    <section id="intro" class="container">
        <div class="row">
            <div class="9u 12u(mobile)">
                <section class="first">
                    <p class="more">{{$memberCompany->description}}</p>
                </section>
            </div>
            <div class="3u 12u(mobile)">
                <section class="middle hidden-xs">
                    <div class="image featured ">
                        @if(!empty($memberCompany->profile_image))
                            @if($memberCompany->profile_image ==='saleme-default-profile.jpg' OR is_null($memberCompany->profile_image))
                                <img src="{{asset('')}}salenow/images/prime-members/premium_default.jpg">
                            @else
                                <img src="{{asset('')}}salenow/images/prime-members/{{$memberCompany->member_id.'/'.$memberCompany->profile_image}}">
                            @endif
                        @endif
                    </div>

                </section>
            </div>

        </div>

    </section>

    </div>
    </div>

    <!-- Main -->
    <div id="main-wrapper">
        <div class="container">
            @if(count($ads))
                <div class="row">
                    <div class="12u">
                        <!-- Portfolio -->
                        <section>
                            <header class="major">
                                <h2>Recent Ads</h2>
                            </header>
                            <div class="row ads">
                                @foreach($ads as $datas)
                                    <?php
                                    $publishedTime = timeForUser($datas->published_at);
                                    ?>
                                    <div class="4u 12u(mobile)">
                                        <a href="{{asset('')}}ad/{{$datas->slug}}" target="_blank">
                                            <section class="box">
                                                <div class="image featured" style="height: 275px;
                                                        background: url({{($datas->featuredimage!='')?asset('').'salenow/images/uploads/'.$datas->id.'/'.$datas->featuredimage :asset('').'images/salenow/no_image.jpg'}});    background-size: cover;">
                                                    {{--@if($datas->featuredimage!='')--}}
                                                        {{--<img src="{{asset('')}}salenow/images/uploads/{{$datas->id}}/{{$datas->featuredimage}}"--}}
                                                             {{--alt=""/>--}}
                                                    {{--@else--}}
                                                        {{--<img src="{{asset('')}}images/salenow/no_image.jpg" alt=""/>--}}
                                                    {{--@endif--}}
                                                </div>
                                                <header>
                                                    <h3 class="title">{{title_case($datas->adtitle)}}</h3>
                                                    <p class="location">
                                                        <i class="ion-ios-location"></i>&nbsp;&nbsp;&nbsp;
                                                        {{$datas->district->district_name}}
                                                        &nbsp;>&nbsp;{{$datas->city->city_name}}
                                                    </p>

                                                    <h3 class="price">{{$datas->price > 0 ? 'Rs.'. number_format($datas->price) : 'Negotiable' }}</h3>
                                                </header>
                                                <p class="des-para">{{strip_tags(truncateText($datas->description ,30))}}</p>
                                                <footer>

                                                    <p style="text-align: right; color: #666"><i
                                                                class="ion-ios-time"></i>&nbsp;&nbsp;&nbsp;{{$publishedTime}}
                                                    </p>
                                                </footer>
                                            </section>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </section>
                    </div>
                </div>
            @endif
            @if(count($services))
                <div class="row">
                    <div class="12u">
                        <!-- Blog -->
                        <section>
                            <header class="major">
                                <h2>Our Services</h2>
                            </header>
                            <?php
                            $serviceCount = count($services);
                            $rows = '3u 12u(mobile) ';
                            if ($serviceCount == '3') {
                                $rows = '4u 12u(mobile)';
                            }
                            if ($serviceCount == '2') {
                                $rows = '6u 12u(mobile) ';
                            }
                            if ($serviceCount == '1') {
                                $rows = '6u 12u(mobile) service-items';
                            }
                            ?>
                            <div class="row services">
                                @foreach($services as $service)
                                    <div class="{{$rows}}">
                                        <a class="" href="{{asset('')}}{{$memberCompany->slug}}/services">
                                            <section class="box">
                                                @if(!empty($service->image))
                                                    <div class="image featured"><img
                                                                src="{{asset('')}}salenow/images/prime-members/{{$memberCompany->id}}/{{$service->image}}"
                                                                alt=""/></div>
                                                @else
                                                    <div class="image featured"><img src="{{asset('')}}images/user.png"
                                                                                     alt=""/></div>
                                                @endif

                                                <header>
                                                    <h3 class="text-center">{{$service->servicetitle}}</h3>
                                                    <p class="text-center">
                                                        {{strip_tags(truncateText($service->service ,65))}}</p>
                                                    <p style="font-style: normal;font-weight: 600;text-align: right;margin: 0;padding: 0px;color: #fea502;">
                                                        View More ></p>
                                                </header>
                                            </section>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </section>

                    </div>
                </div>
            @endif

            @if($memberCompany->teampublic AND count($memberemployees))
                <div class="row">
                    <div class="12u">
                        <!-- Blog -->
                        <?php
                        $class = '3u 12u(mobile) staff-item';
                        if (count($memberemployees) == 1) {
                            $class = '3u 12u(mobile) staff-item single-staff';
                        }
                        ?>
                        <section>
                            <header class="major">
                                <h2>Our Staff</h2>
                            </header>
                            @foreach($memberemployees->chunk(4) as $row)
                                <div class="row staff">
                                    @foreach($row as $employee)
                                        <div class="{{$class}}">
                                            <a href="#">
                                                <section class="box">
                                                    @if(!empty($employee->member->avatar))
                                                        <div class="image featured"><img
                                                                    src="{{$employee->member->avatar}}"
                                                                    alt=""/></div>
                                                    @else
                                                        <div class="image featured"><img
                                                                    src="{{asset('')}}images/user.png"
                                                                    alt=""/></div>
                                                    @endif
                                                    <header>
                                                        <h3 class="title text-center">{{$employee->member->first_name}}</h3>
                                                        <p class="text-center">{{($employee->designation="N/A" ?'Team Member':$employee->designation)}}</p>
                                                    </header>
                                                </section>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </section>

                    </div>
                </div>
            @endif

            @if(count($images))
                <div class="row">
                    <div class="12u">
                        <!-- Blog -->
                        <?php
                        $class = '3u 12u(mobile) staff-item';
                        if (count($images) == 1) {
                            $class = '4u 12u(mobile) single-img';
                        }
                        ?>
                        <section>
                            <header class="major">
                                <h2>Gallery</h2>
                            </header>
                            <div class="row">
                                @foreach($images as $img)
                                    <div class="{{$class}}">
                                        <article class="">
                                            <a href="{{asset('')}}{{$memberCompany->slug}}/gallery"
                                               title="View More images " class="image featured">
                                                <img src="{{asset('')}}salenow/images/prime-members/{{$memberCompany->id}}/{{$img->image}}"
                                                     alt=""/>
                                            </a>
                                        </article>
                                    </div>
                                @endforeach
                            </div>
                        </section>

                    </div>
                </div>
            @endif

        </div>
    </div>
    <script>
        $(document).ready(function () {
            // Configure/customize these variables.
            var showChar = 500;  // How many characters are shown by default
            var ellipsestext = "...";
            var moretext = "Show More";
            var lesstext = "Show Less";


            $('.more').each(function () {
                var content = $(this).html();

                if (content.length > showChar) {

                    var c = content.substr(0, showChar);
                    var h = content.substr(showChar, content.length - showChar);

                    var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

                    $(this).html(html);
                }

            });

            $(".morelink").click(function () {
                if ($(this).hasClass("less")) {
                    $(this).removeClass("less");
                    $(this).html(moretext);
                } else {
                    $(this).addClass("less");
                    $(this).html(lesstext);
                }
                $(this).parent().prev().toggle();
                $(this).prev().toggle();
                return false;
            });
        });
    </script>
@endsection