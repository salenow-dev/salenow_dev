@extends('layouts.member.premium_theme_01')
@section('premium_content')
</div>
</div>

<!-- Main -->
<div id="main-wrapper">
    <div class="container">


        <div class="row">
            <div class="12u">
                <!-- Blog -->
                <section>
                    <header class="major">
                        <h2 class="inner-title">Contact Us</h2>
                    </header>
                    <article class="" style="padding: 8px; background: #fff">
                        <div style="height: 350px; width: 100%; background-color: #eee" id="map"></div>
                    </article>
                    <div class="row ">

                        <div class="6u 12u(mobile)">
                            <section class="box">
                                <header>
                                    <h3>Make an Inquiry</h3>
                                </header>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="alert alert-success alert-dismissable" id="responce">
                                        <strong>Message Sent successfully.</strong>
                                    </div>
                                    <form class="" action="" method="post" id="contact_form">
                                        <input type="hidden" name="id" readonly value="{{$memberCompany->id}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <fieldset>
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <input name="name" id="name" placeholder="Your Name"
                                                           class="form-control"
                                                           type="text">
                                                </div>
                                            </div>
                                            <!-- Text input-->
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input name="email" name="email" placeholder="Your E-Mail"
                                                           class="form-control"
                                                           type="text">
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input name="phone" id="phone" placeholder="Your Phone Number"
                                                           class="form-control"
                                                           type="text">
                                                </div>
                                            </div>
                                            <!-- Text input-->
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <input name="subject" id="subject" placeholder="Subject"
                                                           class="form-control"
                                                           type="text">
                                                </div>
                                            </div>
                                            <!-- Text input-->
                                            <div class="form-group">
                                                <div class="col-md-12 inputGroupContainer">
																<textarea class="form-control" name="message"
                                                                          id="message"
                                                                          placeholder="Type your message here.."
                                                                          rows="3"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-warning pull-right"
                                                            id="send_inquiry">Send
                                                        Inquiry
                                                    </button>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </section>

                        </div>


                        <div class="6u 12u(mobile)">
                            <section class="box">
                                <header>
                                    <h3>Contact Infomation</h3>
                                </header>
                                <section>

                                    <ul class="contact contact-page">
                                        @if($memberCompany->company_name OR $memberCompany->address)

                                            <li>
                                                <h3>Address</h3>
                                                <p>
                                                    {{$memberCompany->company_name}}<br>
                                                    {{str_replace('\n',' ',$memberCompany->address )}}
                                                </p>
                                            </li>
                                        @endif
                                        @if($memberCompany->email)
                                            <li>
                                                <h3>E-Mail</h3>
                                                <p>{{$memberCompany->email}}</p>
                                            </li>
                                        @endif
                                        @if($memberCompany->telephone)
                                            <li>
                                                <h3>Telephone</h3>
                                                <p>
                                                    @foreach($memberCompany->telephone as $com_con)
                                                        {{$com_con.'&nbsp;/&nbsp;'}}
                                                    @endforeach
                                                </p>
                                            </li>
                                        @endif
                                        @if($memberCompany->mobile)
                                            <li>
                                                <h3>Mobile</h3>
                                                <p>
                                                    @foreach($memberCompany->mobile as $mobi)
                                                        {{$mobi.'&nbsp;/&nbsp;'}}
                                                    @endforeach
                                                </p>
                                            </li>
                                        @endif
                                        @if($memberCompany->website)

                                            <li>
                                                <h3>Website</h3>
                                                <p><a href="{{$memberCompany->website}}"  target="_blank"
                                                      style="color: #111 !important; text-decoration: underline;">Visit
                                                        Website</a></p>
                                            </li>
                                        @endif
                                        @if($memberCompany->facebook_page)
                                            <li>
                                                <h3>Facebook</h3>
                                                <p>
                                                    <a href="{{$memberCompany->facebook_page}}" target="_blank"  style="color: #111 !important; text-decoration: underline;">Visit
                                                        Facebook page</a>
                                                </p>
                                            </li>
                                        @endif


                                    </ul>
                                    {{--<ul class="social">--}}
                                    {{--<li><a class="icon fa-facebook" href="#"><span class="label">Facebook</span></a>--}}
                                    {{--</li>--}}
                                    {{--<li><a class="icon fa-twitter" href="#"><span class="label">Twitter</span></a>--}}
                                    {{--</li>--}}
                                    {{--<li><a class="icon fa-dribbble" href="#"><span class="label">Dribbble</span></a>--}}
                                    {{--</li>--}}
                                    {{--<li><a class="icon fa-linkedin" href="#"><span class="label">LinkedIn</span></a>--}}
                                    {{--</li>--}}
                                    {{--<li><a class="icon fa-google-plus" href="#"><span--}}
                                    {{--class="label">Google+</span></a></li>--}}
                                    {{--</ul>--}}
                                </section>

                            </section>

                        </div>


                    </div>
                </section>

            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
    // price mask
    $('.phone').text(function (i, text) {
        return text.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
    });

    $('.mobile').text(function (i, text) {
        return text.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
    });
    //        $('.phone').mask("#,##0", {reverse: true});
</script>
{{ Html::script('plugins/blockui/jquery.blockUI.js') }}
{{ Html::script('js/salenow/plugin/jquery.validate.js') }}
{{ Html::script('premium_assets/frontend/js/validate.js') }}
@endsection