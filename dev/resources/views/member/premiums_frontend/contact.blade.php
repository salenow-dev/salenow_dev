@extends('layouts.member.premium')
@section('premium_content')
    <section class="map-container" id="map"
             style="background-image: url({{asset('')}}salenow/images/prime-members/{{$memberCompany->cover_image}});"></section>
    <section class="contact-form-div" id="">
        <div class="site-header-container container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h2>Make an Inquiry</h2>
                    <hr>
                    <div class="alert alert-success alert-dismissable" id="responce">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong>Message Sent successfully.</strong>
                    </div>
                    <form class="form-horizontal" action="" method="post" id="contact_form">
                        {{--<form class="form-horizontal" method="post" id="contact_form" action="/premiummember/sendinquiryemail">--}}
                        <input type="hidden" name="id" readonly value="{{$memberCompany->id}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        {{--                        {!! csrf_field() !!}--}}
                        <fieldset>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input name="name" id="name" placeholder="Your Name" class="form-control"
                                           type="text">
                                </div>
                            </div>
                            <!-- Text input-->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input name="email" name="email" placeholder="Your E-Mail" class="form-control"
                                           type="text">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <input name="phone" id="phone" placeholder="Your Phone Number" class="form-control"
                                           type="text">
                                </div>
                            </div>
                            <!-- Text input-->
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input name="subject" id="subject" placeholder="Subject" class="form-control"
                                           type="text">
                                </div>
                            </div>
                            <!-- Text input-->
                            <div class="form-group">
                                <div class="col-md-12 inputGroupContainer">
                                    <textarea class="form-control" name="message" id="message"
                                              placeholder="Type your message here.." rows="5"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-warning pull-right" id="send_inquiry">Send
                                        Inquiry
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="contact-page-info">
                        <h2>Contact Infomation</h2>
                        <hr>
                        @if($memberCompany->company_name OR $memberCompany->address)
                            <div class="col-md-1 col-xs-2 padding-0"><i class="entypo-location"></i></div>
                            <div class="col-md-11 col-xs-10 padding-0 con-details">
                                {{$memberCompany->company_name}}<br/>
                                {!! nl2br($memberCompany->address) !!}
                            </div>
                            <div class="clear-fix"></div>
                            <br>
                        @endif
                        @if($memberCompany->email)
                            <div class="col-md-1 col-xs-2 padding-0"><i class="entypo-mail"></i></div>
                            <div class="col-md-11 col-xs-10 padding-0 con-details">{{$memberCompany->email}}</div>
                            <div class="clear-fix"></div>
                            <br>
                        @endif
                        @if(!empty($memberCompany->telephone) OR !empty($memberCompany->mobile) )
                            <div class="col-md-1 col-xs-2 padding-0"><i class="entypo-phone"></i></div>
                            <div class="col-md-11 col-xs-10 padding-0 phone-number con-details">
                                @if(!empty($memberCompany->telephone))
                                    @foreach($memberCompany->telephone as $fixed)
                                        <span class="phone">{{$fixed}}</span><br/>
                                    @endforeach
                                    <br>
                                @endif
                                @if(!empty($memberCompany->mobile))
                                    @foreach($memberCompany->mobile as $mobile)
                                        <span class="mobile">{{$mobile}}</span><br/>
                                    @endforeach
                                @endif
                            </div>
                            <div class="clear-fix"></div>
                            <br>
                        @endif
                        @if($memberCompany->website)
                            <div class="col-md-1 col-xs-2 padding-0"><i class="entypo-globe"></i></div>
                            <div class="col-md-11 col-xs-10 padding-0 con-details"><a href="{{$memberCompany->website}}"
                                                                                      target="_blank">Visit Website</a>
                            </div>
                            <div class="clear-fix"></div>
                            <br>
                        @endif
                        @if($memberCompany->facebook_page)
                            <div class="col-md-1 col-xs-2 padding-0"><i class="entypo-facebook"></i></div>
                            <div class="col-md-11 col-xs-10 padding-0 con-details"><a href="{{$memberCompany->facebook_page}}"
                                                                                      target="_blank">Visit facebook page</a>
                            </div>
                            <div class="clear-fix"></div>
                        @endif

                        <div class="clear-fix"></div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- footer -->
    <script type="text/javascript">
        // price mask
        $('.phone').text(function (i, text) {
            return text.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
        });

        $('.mobile').text(function (i, text) {
            return text.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
        });
        //        $('.phone').mask("#,##0", {reverse: true});
    </script>
    {{ Html::script('plugins/blockui/jquery.blockUI.js') }}
    {{ Html::script('js/salenow/plugin/jquery.validate.js') }}
    {{ Html::script('premium_assets/frontend/js/validate.js') }}
@endsection