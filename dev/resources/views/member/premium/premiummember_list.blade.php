@extends('layouts.saleme')
@section('title','Premium members on SaleMe.lk')
@section('og-title', 'Premium members on SaleMe.lk')
@section('og-description', 'Premium members on SaleMe.lk | Saleme.lk')
@section('content')
    <div class="container">
        <div class="row">

            <div class="col-lg-12 col-xs-12 pd-0-sm">
                <div class="panel">
                    <h2 class="my-ad-h">&nbsp;
                        <span class="my-ad-ico ion-trophy"></span>
                        Premium Members on SaleMe.lk
                    </h2>
                    <hr>
                    <div class="single-ad">
                        <!--My add single item-->
                        @foreach($premiummembers as $datas)
                            <a href="{{asset('')}}{{$datas->slug}}">
                                <div class="media">
                                    <div class="col-md-12 no-padding ">
                                        {{-- START Featured Image--}}
                                        <div class="col-md-4 col-xs-6 pull-left">
                                            @if($datas->profile_image !='' AND $datas->profile_image !='saleme-default-profile.jpg')
                                                <img class="media-object img-responsive" style="margin: 0 auto"
                                                     alt="64x64"
                                                     src="{{asset('')}}salenow/images/prime-members/{{$datas->member_id}}/{{$datas->profile_image}}">
                                            @else
                                                <img class="media-object img-responsive" style="margin: 0 auto"
                                                     alt="64x64"
                                                     src="{{asset('')}}salenow/images/prime-members/premium_default.jpg">
                                            @endif
                                        </div>
                                        {{-- END Featured Image--}}

                                        {{--details body--}}
                                        <div class="media-body">
                                            {{--desktop--}}
                                            <div class="desktop hidden-xs hidden-sm">
                                                <h4 class="media-heading">
                                                    {{$datas->company_name}}
                                                </h4>
                                                <p class="item-loc-cat">
                                                    <span class="item-price">{{$datas->email}}</span>
                                                </p>
                                                <p class="item-loc-cat">
                                                    <i class="ion-location"></i> <span
                                                            class="hidden-xs">{{$datas->category}}</span>
                                                </p>
                                                <p class="item-loc-cat">
                                                    <i class="ion-location"></i> <span
                                                            class="hidden-xs"> {{$datas->address}}</span>
                                                </p>
                                                @if(count(unserialize($datas->telephone)))
                                                    <p class="item-loc-cat">
                                                        <i class="ion-location"></i> <span class="hidden-xs">
                                                @foreach(unserialize($datas->telephone) as $tel)
                                                                {{$tel}}&nbsp;/
                                                            @endforeach
                                                </span>
                                                    </p>
                                                @endif
                                                @if(count(unserialize($datas->mobile)))
                                                    <p class="item-loc-cat">
                                                        <i class="ion-location"></i> <span class="hidden-xs">
                                                        @foreach(unserialize($datas->mobile) as $tel)
                                                                {{$tel}}&nbsp;/
                                                            @endforeach
                                                    </span>
                                                    </p>
                                                @endif

                                                @if(!empty($datas->website))
                                                    <p class="item-loc-cat">
                                                        <i class="ion-location"></i> <span class="hidden-xs">
                                                        <a href="{{$datas->website}}">Visit Website</a>
                                                    </span>
                                                    </p>
                                                @endif

                                                <div class="clear-fix"></div>
                                                {{--END Draft ad notification--}}

                                            </div>
                                            {{--mobile--}}
                                            <div class="mobile visible-xs visible-sm">
                                                <p class="item-title item-m">{{$datas->company_name}}</p>
                                                <p class="item-loc-cat">{{$datas->email}}</p>
                                                <p class="item-loc-cat">{{$datas->category}}</p>
                                                <p class="item-loc-cat">{{$datas->address}}</p>
                                                @if(count(unserialize($datas->telephone)))
                                                    <p class="item-loc-cat">
                                                        @foreach(unserialize($datas->telephone) as $tel)
                                                            {{$tel}}&nbsp;/
                                                        @endforeach</p>
                                                @endif
                                                @if(count(unserialize($datas->mobile)))
                                                    <p class="item-loc-cat">
                                                        @foreach(unserialize($datas->mobile) as $mobi)
                                                            {{$mobi}}&nbsp;/
                                                        @endforeach</p>
                                                @endif
                                                @if(!empty($datas->website))
                                                    <p class="item-loc-cat">
                                                        <a href="{{$datas->website}}">Visit Website</a>
                                                    </p>
                                                @endif

                                            </div>

                                        </div>
                                        {{--END details body--}}

                                    </div>
                                    {{--single add end--}}
                                </div>
                            </a>
                            <hr>
                        @endforeach
                    </div>
                    <div class="row">
                        {{$premiummembers->appends(Illuminate\Support\Facades\Input::except('page'))->links('vendor.pagination.custom')}}
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection