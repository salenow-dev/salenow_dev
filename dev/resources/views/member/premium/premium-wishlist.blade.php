{{--@section('title', 'SaleMe | Member profile')--}}
{{--@include('main.header')--}}
{{--{{dd($member->membercontacts)}}--}}
@extends('layouts.member.premium_admin')
@section('title', 'My Ads | Saleme.lk')
@section('content')
    <style>
        .fav{
            color: red;
        }
    </style>
    <div class="row">
        <div class="col-md-9 col-sm-12">
            <div class="card">
                <div class="tab-content clearfix">
                    <div class="content" id="">
                        <h4 class="title"><i class="ion-ios-heart"></i> &nbsp;Wish List</h4>
                        <div class="clear-fix"></div>
                        @if(count($allads)==0)
                            <div class="col-md-12">
                                <br><br>
                                <div class="text-center">
                                    <h4 class="promo-head2">You have no Ads In wish list.</h4>
                                    <a href="/my_ads" class="goto-link">Go to my all ads</a>

                                    <br>
                                    {{--<a href="addtype">--}}
                                    {{--{{ Html::image('images/salenow/place-Ad-01-01.png', 'saleme.lk logo', array('class' => 'img-responsive center-block plce-ad')) }}--}}
                                    {{--</a>--}}
                                </div>
                                <div class="clear-fix"></div>
                            </div>
                            <div class="clear-fix"></div>

                        @else
                            @foreach($allads as $datas)
                                @if(!is_null($datas->adtitle) AND !is_null($datas->featuredimage))
                                    <div class="row single-ad">
                                        <a href="{{asset('').'ad/'.$datas->slug}}"
                                           id="{{$datas->adid}}" class="">
                                            <div class="col-md-3">
                                                {{-- START Featured Image----------------------------------------------------------------------------------}}
                                                @if($datas->featuredimage!='')
                                                    <img class="img-responsive pull-left"
                                                         src="{{asset('')}}salenow/images/uploads/{{$datas->adid}}/thumb/{{$datas->featuredimage}}">
                                                @else
                                                    <img class="img-responsive pull-left"
                                                         src="{{asset('')}}images/salenow/no_image.jpg">
                                                @endif
                                                {{-- END Featured Image-------------------------------------------------------------------------------------}}
                                            </div>
                                            <div class="col-md-9 listing-title">
                                                <span class="title-member-listing"> {{$datas->adtitle}}</span>

                                                <p class="item-added-date">
                                                    <span>Posted on: {{ date('F d, Y', strtotime($datas->created_at))}}</span>
                                                </p>
                                                <p class="item-price">
                                                    <span>Rs {{$datas->price > 0 ? number_format($datas->price) : '' }}</span>
                                                </p>

                                                <p class="location-item">
                                                    <i class="ion-ios-pricetag"></i>&nbsp; {{$datas->category}} &nbsp; |
                                                    <i class="ion-location"></i>&nbsp; {{$datas->district_name}}
                                                    , {{$datas->city}}
                                                    &nbsp;

                                                </p>
                                                @if(getMemberId())
                                                    <p class="text-right">
                                                        <a href="javascript:void(0);" ad-id="{{$datas->adid}}"
                                                           class="fav">
                                                            <i class="ion-ios-trash-outline"></i>&nbsp;&nbsp;Remove from
                                                            Wish List</a>
                                                    </p>

                                                @endif

                                            </div>
                                        </a>
                                    </div>
                                @endif
                            @endforeach
                        @endif
                        {{$allads->links('vendor.pagination.custom')}}
                        <div class="clear-fix"></div>

                    </div>


                </div>
            </div>
        </div>

        <div class="col-md-3 hidden-sm">
            <div class="card">
                <div class="">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="list-group right-list">
                                <a href="/{{$member->premiummember->slug}}" target="_blank" class="list-group-item"><i
                                            class="ion-log-in"></i> &nbsp;View profile as public</a>
                                <li class="list-group-item">
                                    <small class="block">Business Category</small>
                                    <i class="ion-ios-pricetags-outline"></i>
                                    &nbsp;{{$member->premiummember->category->category_name}}</li>
                                <li class="list-group-item">
                                    <small class="block">Address</small>
                                    <i class="ion-navigate"></i>
                                    &nbsp;{{$member->premiummember->address}}</li>
                                <li class="list-group-item">
                                    <small class="block">Email Address</small>
                                    <i class="ion-ios-email-outline"></i>
                                    &nbsp;{{$member->premiummember->email}}</li>
                                <li class="list-group-item">
                                    <small class="block">Phone Numbers</small>
                                    @if(!empty($member->premiummember->telephone))
                                        @foreach($member->premiummember->telephone as $telephone)
                                            <p class="tel-no" id="contact_{{str_replace(' ','_', $telephone)}}">
                                                <i class="ion-ios-telephone"></i>
                                                <span class="">{{$telephone}}</span>
                                                {{--<a href="javascript:;" href="#" uid="{{$telephone}}"--}}
                                                {{--class="removephone pull-right"--}}
                                                {{--title="Delete {{$telephone}}"><i class="ion-ios-trash del-icon"></i></a>--}}
                                            </p>
                                        @endforeach
                                    @endif

                                    @if(!empty($member->premiummember->mobile))
                                        @foreach($member->premiummember->mobile as $mobile)
                                            <p class="tel-no" id="contact_{{$mobile}}">
                                                <i class="ion-android-phone-portrait"></i>
                                                <span class="">&nbsp;{{$mobile}} </span>
                                                {{--<a href="javascript:;" href="#" uid="{{$mobile}}"--}}
                                                {{--class="removephone pull-right"--}}
                                                {{--title="Delete {{$mobile}}"><i class="ion-ios-trash del-icon"></i></a>--}}
                                            </p>
                                        @endforeach
                                    @endif
                                </li>
                                <li class="list-group-item">
                                    <small class="block">Business Title</small>
                                    <i class="ion-navigate"></i>
                                    &nbsp;{{$member->premiummember->webtitle}}
                                </li>
                                <li class="list-group-item">
                                    <small class="block">Website Title</small>
                                    <input type="text" value="{{$member->premiummember->website}}"
                                           class="website-display" readonly="readonly">
                                </li>
                                @if(isEmployeeAdmin())
                                    <li class="list-group-item edit-li">
                                        <div class="stats">
                                            <a href="/premium/{{str_random(3).$member->premiummember->id}}/edit">
                                                <i class="ion-compose"></i> Update Information
                                            </a>
                                        </div>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .right-list .list-group-item {
            border-left: 0px;
            border-right: 0px;
            border: 1px solid #f1f1f1 !important;
        }

        .title-member-listing {
            font-size: 18px;
            font-weight: 600;
        }

        .listing-title p {
            font-size: 13px;
        }

        .item-price {
            font-size: 16px;
            font-weight: 500;
        }

        img {
            /*width: 100%;*/
        }

        .bg-grey {
            background-color: #f2f2f2;
        }

        a {
            color: #111;
        }

        a:hover {
            color: #222;
        }

        a:focus {
            color: #111;
            text-decoration: none;
        }

        .listing-number {
            width: 100%;
        }

        .listing-number li {
            display: inline-block;
            list-style-type: none;
            overflow: hidden;
            position: relative;
            width: 50%;
            float: left;
            color: #636363;
        }

        .listing-enquiry-sec {
            width: 100%;
            overflow: hidden;
            position: relative;
            box-sizing: border-box;
        }

        .listing-enquiry-sec ul {
            padding: 0pt;

        }

        .listing-enquiry-sec ul li {
            margin: 0in;
            float: left;
            display: inline-block;
            /*line-height: 18pt;*/
            color: #343c42;
            font-weight: 400;
            list-style-type: none;
            width: 98%;
            box-sizing: border-box;

        }

        .listing-enquiry-sec ul li {
            margin-right: .052083333in;
            color: #3e4c56;
            /*padding: 3.75pt;*/
            text-align: center;
            font-weight: 300;
            border-radius: .020833333in;
            margin-left: 3pt;
            margin-right: 3pt;
            margin-top: 3pt;
            margin-bottom: 5pt;
            box-sizing: border-box;
            display: block;
            text-align: center;
            border-image: none;
            border-color: #d6d1d1;
            border-style: solid;
            border-top-width: .75pt;
            border-bottom-width: .75pt;
            border-left-width: 1pt;
            border-right-width: .75pt;
            outline: none;
            transition: all .5s ease;
            padding: 4px;
            font-size: 12px;
        }

        .listing-enquiry-sec ul li a:hover {
            color: #333;
            list-style-type: none;
        }

        .listing-title h3 {
            color: #000;
            font-weight: 700;
        }

        .item-listing {
            margin-top: 15px;
        }

        .item-listing:hover {
            background-color: #fff;
            border-radius: 2px;
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
            transition: box-shadow .25s;
        }

        .promo-head2 {
            font-weight: 300 !important;
            margin: 10px;
        }

        .display-block {
            font-weight: 300 !important;
            margin: 10px;
        }

        .single-ad {
            padding: 20px 15px;
            border-bottom: 1px solid #dadada;
        }

        .single-ad:hover {
            border-top: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            box-shadow: 0 0 10px 4px #eee;
        }

        .pintotop_li {
            background: #fea502;
            border: 1px solid #fea502 !important;
        }

        .pintotop_li:hover {
            color: #fea502 !important;
            font-weight: 600;
            background: #222 !important;
            border: 1px solid #222 !important;
        }

        .pintotop_btn {
            color: #fff;
            font-weight: 600;
        }

        .pintotop_btn:hover {
            color: #fea502 !important;
        }

        .edit_btn:hover {
            color: #e67e22 !important;
        }

        .delete_btn:hover {
            color: #e74c3c !important;
        }

        .disable {
            opacity: 0.3;
        }

        .disable a {
            pointer-events: none;
        }

        .card .title {
            padding: 6px 4px 10px;
        }

        .item-added-date {
            color: #a3a2a2;
            font-size: 11px;
            font-weight: 300;
        }

        .item-added-date span {
            font-size: 12px;
            font-weight: 300;
        }

        .location-item {
            font-size: 12px !important;
            font-weight: 300;
            margin: 5px 0px;
        }

        .item-price {
            margin: 10px 0px;
        }

        .item-price span {
            font-size: 16px !important;
            font-weight: 600;
        }

        .single-ad img {
            width: 100%;
        }

        @media screen and ( min-width: 600px ) {

            .listing-enquiry-sec ul li {
                width: 25%;
            }
        }
    </style>
    <script>
        $(document).ready(function () {
            //  remove  mobile nomber
            $('.removephone').click(function () {
                var self = $(this);
                var parent = self.parent().parent();
                if (confirm("Do you really want to delete?")) {
                    $.ajax({
                        type: 'POST',
                        url: '/deletecontactno',
                        data: 'uid=' + self.attr('uid'),
                        dataType: 'json',
                        success: function (msg) {
                            if (msg.success) {
                                $('#msg').html(msg.response);
                                $("#contact_" + self.attr('uid')).remove();
                            } else {
                                $('#msg').html(msg.errors, 'errors');
                            }
                            return false;
                        }
                    });
                }
                return false;
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('.fav').click(function (e) {
                e.preventDefault();
                var element = $(this);
                var id = $(this).attr('ad-id');
                var data = {'id': id};

                $.ajax({
                    type: "post",
                    url: '/myads/favorite/' + id,
                    data: data,
                    success: function (res) {
                        if (res.response.message == 'delete_success') {
                            $(element).closest('.single-ad').hide();
                            noty({
                                text: 'Removed from Wish list successfully',
                                layout: 'topRight',
                            })
                        }
                    }
                });
            })
        });
    </script>
@endsection
{{--inquiry model--}}
<div class="modal fade send-mail" tabindex="-1" role="dialog" id="modalinquiry" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="ion-ios-close-empty"></i></button>
                <h4 class="modal-title text-center"><span class="no-1"><i class="ion-paper-airplane"></i> </span> &nbsp;Make
                    an Inquiry</h4>
            </div>
            <div class="col-lg-12">
                <form role="form" id="inquiry-form" class="inquiry-form">
                    <input type="hidden" id="ad_id" name="ad_id"
                           value="">
                    <div class="">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" autocomplete="off" id="name"
                                       placeholder="Name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="mobile" autocomplete="off" id="mobile"
                                       placeholder="Mobile Number">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" autocomplete="off" id="email"
                                       placeholder="Your E-mail Address">
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="col-md-12 mg-b-10">
                            <div class="form-group">
                                <textarea class="form-control textarea" rows="3" name="message" id="message"
                                          placeholder="Message"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="col-md-12 top-mar-20">
                            <button type="button" id="inquiry_post_ad" class="btn main-btn pull-right">Send a message
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{--inquiry model--}}

{{--contact model--}}
<div class="modal fade contact" tabindex="-1" role="dialog" id="modalinquiry" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="ion-ios-close-empty"></i></button>
                <h4 class="modal-title text-center"><span class="no-1"><i class="ion-paper-airplane"></i> </span> &nbsp;Contact
                    Information</h4>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-4 pd-t-8 prime-member-right pd-r-0">
                    <div class="col-md-12 pd-r-0 pd-l-0 category-name">
                        <p>
                            <i class="ion-ios-pricetags-outline"></i> {{ucwords(str_replace('_',' ',$member->premiummember->member_category))}}
                        </p>
                    </div>
                    {{--<div class="col-md-12 pd-r-0 pd-l-0 open-time">--}}
                    {{--<span class="open">Open Now</span>--}}
                    {{--<span class="closed">Closed Now</span>--}}
                    {{--<p><i class="ion-ios-time-outline"></i>Open today: 8:00 am – 6:00 pm</p>--}}
                    {{--</div>--}}

                    <div class="col-md-12 pd-r-0 pd-l-0 address">
                        <p><i class="ion-ios-navigate-outline"></i>{{$member->premiummember->address}}</p>
                    </div>
                    <div class="col-md-12 pd-r-0 pd-l-0 category-name email-send-btn">
                        <p><i class="ion-ios-email-outline"></i> salinda@saleme.lk</p>
                    </div>
                    <div class="col-md-12 pd-r-0 pd-l-0 category-name">
                        @foreach(($member->membercontacts) as $key=> $contact)
                            <p><i class="ion-ios-telephone-outline"></i>{{$contact->contactnumber}}</p>
                        @endforeach

                    </div>

                    <div class="col-md-12 pd-r-0 pd-l-0 category-name">
                        <a href="{{$member->premiummember->website}}" target="_blank"><p><i
                                        class="ion-android-globe"></i>
                                Visit Website</p></a>
                    </div>
                    <div class="col-md-12 pd-r-0 pd-l-0 category-name">
                        <a href="{{$member->premiummember->website}}" target="_blank"><p><i
                                        class="ion-android-globe"></i>
                                Visit Website</p></a>
                    </div>
                    {{--<div class="col-md-12 pd-r-0 pd-l-0 category-name">--}}
                    {{--<p class="text-justify"><i class="ion-ios-information-outline"></i>  We at ‘FIRST TEAM’ highly appreciate talent and we continuously work to explore personal potential and to reward outstanding achievements.</p>--}}
                    {{--</div>--}}


                </div>
            </div>
        </div>
    </div>
</div>
{{--contact model--}}
<div class="modal fade change-password" id="password_change" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="ion-ios-close-empty"></i></button>
                <h4 class="modal-title text-center"><span class="no-1"><i class="ion-android-unlock"></i> </span> &nbsp;
                    Change Password</h4>
            </div>
            <div class="col-lg-12">
                <form id="updatepass_form" action="" method="post" class="change-password-form" role="form">
                    <div class="messager"></div>
                    <div class="form-group">
                        <label class="lable-form" for="old-pwd">Old Password</label>
                        <input type="password" name="old_password" class="form-control" placeholder="Old Password"
                               id="old_password"/>
                    </div>
                    <div class="form-group">
                        <label class="lable-form" for="new-pwd">New Password</label>
                        <input type="password" name="new_password" class="form-control" placeholder="New Password"
                               id="new_password"/>
                    </div>
                    <div class="form-group">
                        <label class="lable-form" for="confirm-pwd">Confirm Password</label>
                        <input type="password" name="confirm_password" class="form-control"
                               placeholder="Confirm Password" id="" confirm_password"/>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-managead blue-btn pull-right">Change Password</button>
                        <div class="clear-fix"></div>
                    </div>
                </form>
            </div>
            <div class="clear-fix"></div>
        </div>
    </div>
</div>
{{--delete resons--}}
<div class="modal fade deleresons" id="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="ion-ios-close-empty"></i>
                </button>
                <h4 class="modal-title text-center"><span class="no-1"><i class="ion-ios-trash-outline"></i> </span>
                    &nbsp; Select Delete Reason</h4>
            </div>

            <div class="modal-body">
                <div class="col-lg-12">
                    <ul>
                        @if(!empty($delete_resons))
                            @foreach($delete_resons as $reson)
                                <li><label><input value="{{$reson->id}}" name="delete_reson_id"
                                                  type="radio" {{($reson->id ==1)? 'checked' : ''}}>
                                        &nbsp;{{$reson->reason}}
                                    </label></li>
                            @endforeach
                        @endif
                    </ul>
                </div>
                <div class="clear-fix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn primary " data-dismiss="modal">Cancel</button>
                <button class="btn  delete-c" id="delete_btn1">Delete Ad</button>
            </div>
            <div class="clear-fix"></div>
        </div>
    </div>
</div>
{{--@include('main.footer')--}}