{{--@section('title', 'SaleMe | Member profile')--}}
{{--@include('main.header')--}}
@extends('layouts.member.premium_admin')
@section('title', 'Team | Saleme.lk')
@section('content')
    <section>
        <div class=" prime-container prime-shop-container">
            <div class="row main-row">
                <div class="col-md-12">
                    <div class="clear-fix"></div>
                    @include('errors.list')
                    <form class="form-horizontal" name="services" action="/premium/services/store" method="post"
                          enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><strong>Add New </strong> Service</h3>
                            </div>
                            <div class="panel-body">
                                @include('member.premium.form-services')
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="clear-fix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h4 class="title"><strong>Your</strong> Services</h4>
                            <p class="category">Here is a list of services your company offer</p>
                        </div>
                        <div class="content table-responsive table-full-width">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Featured</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($services as $service)
                                    <tr>
                                        <td>
                                            <div class="col-xs-3">
                                                <div class="avatar">
                                                    <img src="{{asset('')}}salenow/images/prime-members/{{$service->memberpremium_id}}/{{$service->image}}"
                                                         alt="Circle Image"
                                                         class="img-circle img-no-padding img-responsive">
                                                </div>
                                            </div>
                                        </td>
                                        <td>{{$service->servicetitle}}</td>
                                        <td>{{($service->featured)?'Yes':'No'}}</td>
                                        <td>{{str_limit($service->service,100)}}</td>
                                        <td>
                                            <a href="/premium/services/{{$service->id}}/edit"
                                               class="edit unline">Edit</a>
                                            <a service-id="{{$service->id}}" id="delete_service_{{$service->id}}"
                                               status="delete" class="delete-record delete_btn"
                                               onClick="notyServicedDelete({{$service->id}})"><i
                                                        class="fa fa-trash"
                                                        aria-hidden="true"></i>
                                                Delete</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>No Record Found</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection