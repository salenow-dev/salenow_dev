{{--@section('title', 'SaleMe | Member profile')--}}
{{--@include('main.header')--}}
{{--{{dd($searchMembers->membercontacts)}}--}}
@extends('layouts.member.premium_admin')
@section('title', 'Edit Location | Saleme.lk')
@section('content')
    <section>
        <style>
            .theme-id-container-active .theme-name {
                margin: 0;
                color: #fff;
                font-size: 15px;
                line-height: 1.8;
            }

            .theme-id-container-active .theme-name span {
                margin: 0;
                color: #ccc;
            }

            .theme-id-container .theme-name {
                margin: 0;
                color: #111;
                font-size: 15px;
                line-height: 1.8;
            }

            .active .theme-screenshot {
                border-top: 4px solid #229922;
                border-left: 4px solid #229922;
                border-right: 4px solid #229922;
                background: #229922;
                padding: 0px;
            }

            .theme-screenshot {
                padding: 5px;
                background: #ddd;
            }

            .active .theme-screenshot img {
                opacity: 0.7;
            }

            .theme-id-container-active {
                background: #333;
                padding: 13px 8px 12px;
            }

            .theme-id-container {
                background: #ddd;
                padding: 8px;
            }

            .apply-theme-btn {
                padding: 7px 10px;
                border: 0;
                outline: none;
                background: #008ec2;
                color: #fff;
                font-weight: 700;
            }

            .applied-theme {
                background: #25c56e;
                padding: 5px 10px;
                color: #fff;
                font-weight: 700;
            }

            /*------drag reorder-------------*/
            .connected, .sortable, .exclude, .handles {
                margin: auto;
                padding: 0;
                width: 100px;
                -webkit-touch-callout: none;
                -webkit-user-select: none;
                -khtml-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            .sortable.grid {
                overflow: hidden;
            }

            .connected li, .sortable li, .exclude li {
                color: #313135;
                list-style: none;
                border: 1px solid #e8e8e8;
                background: #fff;
                font-family: "Tahoma";
                margin: 5px;
                padding: 10px 6px;
                box-shadow: 1px 2px 4px #d6d6d6;
                cursor: pointer;
            }

            .handles li {
                color: #fea502;
                list-style: none;
                border: 1px solid #e8e8e8;
                background: #fff;
                font-family: "Tahoma";
                margin: 5px;
                padding: 10px 6px;
                box-shadow: 1px 2px 4px #d6d6d6;
                cursor: pointer;
            }

            .no2 > li {
                color: #fff;
                background: #56b5e3;
                border: 1px solid #56b5e3;
            }

            .handles span {
                cursor: move;
            }

            .sortable.grid li {
                line-height: 80px;
                float: left;
                width: 80px;
                height: 80px;
                text-align: center;
            }

            #connected {
                width: 100%;
                overflow: hidden;
                margin: auto;
            }

            .connected {
                float: left;
                width: 100%;
                /*min-height: 289px;*/
                background: #fff;
                margin: 5px;
            }

            .connected.no2 {
                float: right;
            }

            .tik {
                display: none;
            }

            .telno .trash {
                color: rgb(209, 91, 71);
            }

            .telno .checkbox {
                padding-left: 0px !important;
            }

            .telno .flag {
                color: rgb(248, 148, 6);
            }

            .telno .panel-body {
                padding: 0px;
            }

            .telno .panel-footer .pagination {
                margin: 0;
            }

            .telno .panel .glyphicon, .list-group-item .glyphicon {
                margin-right: 5px;
            }

            .telno .panel-body .radio, .checkbox {
                display: inline-block;
                margin: 0px;
            }

            .telno .panel-body input[type=checkbox]:checked + label {
                text-decoration: line-through;
                color: rgb(128, 144, 160);
            }

            .telno .list-group-item:hover, a.list-group-item:focus {
                text-decoration: none;
                background-color: rgb(245, 245, 245);
            }

            .telno .list-group {
                margin-bottom: 0px;
            }
        </style>
        <div class="prime-container prime-shop-container">
            <div class="row main-row">
                {{--</div>--}}
                <div class="col-md-12">
                    <div class="responce"></div>

                    <div class="clear-fix"></div>

                    <div class="col-md-8">
                        <div class="card about-card">
                            <div class="header">
                                <h4 class="title">Select Theme</h4>
                            </div>
                            <br>

                            <div class=" col-md-6 theme {{($member->premiummember->theme =='default')?'active' :''}}">
                                <div class="theme-screenshot">
                                    <img src="{{asset('')}}premium_assets/default.jpg"
                                         alt="" class="img-responsive">
                                </div>
                                @if($member->premiummember->theme =='default')
                                    <div class="theme-id-container-active">
                                        <h4 class="theme-name pull-left">
                                            <span>Active:</span>
                                            Default Theme</h4>
                                        <div class="theme-actions pull-right">
                                            <div class="applied-theme">
                                                <i class="ion-checkmark-round"></i> Applied
                                            </div>
                                        </div>
                                        <div class="clear-fix"></div>
                                    </div>
                                @else
                                    <div class="theme-id-container">
                                        <h4 class="theme-name pull-left">
                                            Default Theme</h4>
                                        <div class="theme-actions pull-right">
                                            <button class="apply-theme-btn"
                                                    onclick="notyConfirmtheme('default',{{$memberid}})">Apply Theme
                                            </button>
                                        </div>
                                        <div class="clear-fix"></div>
                                    </div>
                                @endif
                            </div>


                            <div class=" col-md-6 theme {{($member->premiummember->theme =='full-navigation')?'active' :''}}">
                                <div class="theme-screenshot">
                                    <img src="{{asset('')}}premium_assets/full-navigation.jpg"
                                         alt="" class="img-responsive">
                                </div>
                                {{--{{dd($member->premiummember->theme)}}--}}
                                @if($member->premiummember->theme =='full-navigation')
                                    <div class="theme-id-container-active">
                                        <h4 class="theme-name pull-left">
                                            <span>Active:</span>
                                            Full Navigation Theme</h4>
                                        <div class="theme-actions pull-right">
                                            <div class="applied-theme">
                                                <i class="ion-checkmark-round"></i> Applied
                                            </div>
                                        </div>
                                        <div class="clear-fix"></div>
                                    </div>
                                @else
                                    <div class="theme-id-container">
                                        <h4 class="theme-name pull-left">
                                            Full Navigation Theme</h4>
                                        <div class="theme-actions pull-right">
                                            <button class="apply-theme-btn"
                                                    onclick="notyConfirmtheme('full-navigation',{{$memberid}})">Apply
                                                Theme
                                            </button>
                                        </div>
                                        <div class="clear-fix"></div>
                                    </div>
                                @endif
                            </div>

                            <div class="clear-fix"></div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="card about-card">
                            <div class="header">
                                <h4 class="title">About Us</h4>
                            </div>
                            <div class="content pd-b-25 text-justify">
                                <textarea name="" id="description_textarea" cols="30" rows="10" class="form-control">{!! $member->premiummember->description !!}</textarea>
                                {{--<div class="col-lg-12">--}}
                                {{--{{$member->premiummember->description}}--}}
                                {{--</div>--}}
                                @if(isEmployeeAdmin())
                                    <div class="">
                                        {{--<a href="/premium/{{str_random(3).$member->premiummember->id}}/edit">--}}
                                            <button class="btn btn-sm btn-save-img" id="description_save_btn" premiumid="{{$member->premiummember->id}}">Save Description</button>
                                        {{--</a>--}}
                                    </div>
                                @endif
                                <div class="clear-fix"></div>
                            </div>

                            <div class="clear-fix"></div>
                        </div>

                        <div class="col-md-5">
                            <div class="card card-user about-card">
                                <div class="header">
                                    <h4 class="title">Profile Image</h4>
                                    <span class="text-danger"><small>Image size must be 200px X 200px</small></span>
                                </div>
                                <div class="content">
                                    {{--<div class="col-lg-12">--}}
                                    {{--<p>Select cover image</p>--}}
                                    <form action="{{asset('')}}premium/profile_image/update/{{$member->premiummember->id}}"
                                          enctype="multipart/form-data" method="post">
                                        {{ csrf_field() }}

                                        @if($member->premiummember->profile_image === 'saleme-default-profile.jpg')
                                            <img id="pro_preview" class="img-responsive"
                                                 src="{{asset('')}}salenow/images/prime-members/saleme-default-profile.jpg"/>
                                        @else
                                            <img id="pro_preview" class="img-responsive"
                                                 src="{{asset('')}}salenow/images/prime-members/{{$member->premiummember->member_id.'/'.$member->premiummember->profile_image}}"/>
                                        @endif
                                        <div class="clear-fix"></div>
                                        <input type="file" name="profile_image" onchange="readURL2(this);"
                                               class="upload-pic">
                                        <input type="submit" class="btn btn-sm btn-save-img" value="Change Image">
                                        <div class="clear-fix"></div>
                                    </form>
                                    {{--</div>--}}
                                </div>

                                <div class="clear-fix"></div>
                            </div>
                            <div class="clear-fix"></div>
                        </div>

                        <div class="col-md-7">
                            <div class="card card-user about-card">
                                <div class="header">
                                    <h4 class="title">Cover Image</h4>
                                    <span class="text-danger"><small>Image size must be 1400px X 250px</small></span>
                                </div>
                                <div class="content">
                                    <form action="{{asset('')}}premium/cover/update/{{$member->premiummember->id}}"
                                          enctype="multipart/form-data"
                                          method="post">
                                        {{ csrf_field() }}
                                        @if($member->premiummember->cover_image === 'saleme-default-cover.jpg')
                                            <img id="cover_preview" class="img-responsive"
                                                 src="{{asset('')}}salenow/images/prime-members/default_cover.jpg"/>
                                        @else
                                            <img id="cover_preview" class="img-responsive"
                                                 src="{{asset('')}}salenow/images/prime-members/{{$member->premiummember->member_id.'/'.$member->premiummember->cover_image}}"/>
                                        @endif
                                        <div class="clear-fix"></div>
                                        <div class="">
                                            <input type='file' onchange="readURL(this);" name="cover"
                                                   class="upload-cvr"/>
                                            <input type="submit" class="btn btn-sm btn-save-img" value="Change Image">
                                        </div>
                                        <div class="clear-fix"></div>
                                    </form>
                                </div>

                                <div class="clear-fix"></div>
                            </div>
                            <div class="clear-fix"></div>
                        </div>
                        <div class="clear-fix"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-user about-card">
                            <div class="header">
                                <h4 class="title">Reorder Phone numbers</h4>
                                <small class="text-danger">Drag section up and down to change order</small>
                            </div>
                            <div class="content">
                                <form action="{{asset('')}}premium/contact/reorder/{{$member->premiummember->member_id}}"
                                      enctype="multipart/form-data"
                                      method="post">
                                    {{ csrf_field() }}
                                    @if(!empty($membercontacts) && count($membercontacts))
                                        <section id="connected">
                                            <ul class="connected list">
                                                @foreach($membercontacts as $contacts)
                                                    <li>
                                                        <input type="hidden" name="contactnumber[]"
                                                               value="{{$contacts->contactnumber}}">
                                                        <label for="box-{{$contacts->id}}"> {{'0'.substr($contacts->contactnumber, 2, 2).'-'.substr($contacts->contactnumber, 4, 3).'-'.substr($contacts->contactnumber,7)}}</label>
                                                        <i class="ion-arrow-move pull-right"
                                                           style="font-size: 22px;color: #bbb;"></i>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </section>
                                    @endif
                                    <div class="clear-fix"></div>
                                    <div class="">

                                        <input type="submit" class="btn btn-sm btn-save-img" value="Save Order">
                                    </div>
                                    <div class="clear-fix"></div>
                                </form>
                            </div>

                            <div class="clear-fix"></div>
                        </div>

                        <div class="card card-user about-card">
                            <div class="header">
                                <h4 class="title">Edit / Delete Phone numbers</h4>
                                {{--<small class="text-danger">Drag section up and down to change order</small>--}}
                            </div>
                            <div class="content">
                                <form action="{{asset('')}}premium/contact/edit-save/{{$member->premiummember->member_id}}"
                                      enctype="multipart/form-data"
                                      method="post" id="editphonenumbersform">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="{{$member->premiummember->id}}" name="premium_id">
                                    <span>Telephone : </span>
                                    @if(!empty($member->premiummember->telephone) && count($member->premiummember->telephone))
                                        <section class="telno">
                                            <div class="panel-body">
                                                <ul class="list-group">
                                                    @foreach($member->premiummember->telephone as $tel)
                                                        <li class="list-group-item">
                                                            <input type="hidden" name="telephone[]"
                                                                   value="{{$tel}}">
                                                            <div class="checkbox">
                                                                <label for="checkbox">
                                                                    {{$tel}}
                                                                </label>
                                                            </div>
                                                            <div class="pull-right action-buttons">
                                                                <a href="#"><span
                                                                            class="glyphicon glyphicon-pencil"></span></a>
                                                                <a href="#" class="trash deletetelno"
                                                                   member="{{$member->premiummember->member_id}}"
                                                                   contact="{{$tel}}"><span
                                                                            class="glyphicon glyphicon-trash"></span></a>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </section>
                                    @else
                                        <p>No Telephone numbers to show</p>
                                    @endif
                                    <br>
                                    <span>Mobile : </span>
                                    @if(!empty($member->premiummember->mobile) && count($member->premiummember->mobile))
                                        <section class="telno">
                                            <div class="panel-body">
                                                <ul class="list-group">
                                                    @foreach($member->premiummember->mobile as $tel)
                                                        <li class="list-group-item">
                                                            <input type="hidden" name="mobile[]"
                                                                   value="{{$tel}}">
                                                            <div class="checkbox">
                                                                <label for="checkbox">
                                                                    {{$tel}}
                                                                </label>
                                                            </div>
                                                            @if(count($member->premiummember->mobile) > 1)
                                                                <div class="pull-right action-buttons">
                                                                    <a href="#"><span
                                                                                class="glyphicon glyphicon-pencil"></span></a>
                                                                    <a href="#" class="trash deletetelno"
                                                                       member="{{$member->premiummember->member_id}}"
                                                                       contact="{{$tel}}"><span
                                                                                class="glyphicon glyphicon-trash"></span></a>
                                                                </div>
                                                            @endif
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </section>
                                    @else
                                        <p>No mobile numbers to show</p>
                                    @endif
                                    <div class="clear-fix"></div>
                                    <div class="">


                                        <input type="submit" class="btn btn-sm btn-save-img" value="Save Order">
                                    </div>
                                    <div class="clear-fix"></div>
                                </form>
                            </div>

                            <div class="clear-fix"></div>
                        </div>

                        <div class="card about-card">
                            {{--<div class="header">--}}
                            {{--<h4 class="title">Facebook Page</h4>--}}
                            {{--</div>--}}
                            <div class="content pd-b-10 text-justify">
                                <p class="text-primary text-left">Company Name</p>
                                <form action="/company-name/save" method="post">
                                    {{csrf_field()}}
                                    <input type="text" class="form-control" placeholder="Paste facebook link here"
                                           required name="company_name" id="comapny_name"
                                           value="{{($member->premiummember->company_name)?$member->premiummember->company_name:''}}"><br>
                                    <p class="text-primary text-left">Company Slug</p>
                                    <input type="text" class="form-control" placeholder="Slug" id="slug"
                                           value="{{($member->premiummember->slug)?$member->premiummember->slug:''}}"
                                           readonly>
                                    <button class="btn btn-sm btn-save-img" type="submit">Save</button>
                                </form>
                                <div class="clear-fix"></div>
                            </div>
                        </div>
                        <div class="card about-card">
                            <div class="content pd-b-10 text-justify">
                                <p class="text-primary text-left">Paste facebook page link below</p>
                                <form action="/facebookpage/save" method="post">
                                    {{csrf_field()}}
                                    <input type="text" class="form-control" placeholder="Paste facebook link here"
                                           required name="facebook_page"
                                           value="{{($member->premiummember->facebook_page)?$member->premiummember->facebook_page:''}}">
                                    <button class="btn btn-sm btn-save-img" type="submit">Save</button>
                                </form>
                                <div class="clear-fix"></div>
                            </div>
                        </div>
                        <div class="card about-card">
                            <div class="content pd-b-10 text-justify">
                                <p class="text-primary text-left">Paste existing website URL below</p>
                                <form action="/website/save" method="post">
                                    {{csrf_field()}}
                                    <input type="text" class="form-control" placeholder="Paste facebook link here"
                                           required name="website"
                                           value="{{($member->premiummember->website)?$member->premiummember->website:'http://'}}">
                                    <button class="btn btn-sm btn-save-img" type="submit">Save</button>
                                </form>
                                <div class="clear-fix"></div>
                            </div>

                            <div class="clear-fix"></div>
                        </div>
                        <div class="clear-fix"></div>
                    </div>
                    <div class="clear-fix"></div>

                </div>
            </div>
            <div class="clear-fix"></div>
        </div>
        {{--{{dd()}}--}}
    </section>

    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    {{--order telphone--}}
    <script>
        (function ($) {
            var dragging, placeholders = $();
            $.fn.sortable = function (options) {
                var method = String(options);
                options = $.extend({
                    connectWith: false
                }, options);
                return this.each(function () {
                    if (/^enable|disable|destroy$/.test(method)) {
                        var items = $(this).children($(this).data('items')).attr('draggable', method == 'enable');
                        if (method == 'destroy') {
                            items.add(this).removeData('connectWith items')
                                .off('dragstart.h5s dragend.h5s selectstart.h5s dragover.h5s dragenter.h5s drop.h5s');
                        }
                        return;
                    }
                    var isHandle, index, items = $(this).children(options.items);
                    var placeholder = $('<' + (/^ul|ol$/i.test(this.tagName) ? 'li' : 'div') + ' class="sortable-placeholder">');
                    items.find(options.handle).mousedown(function () {
                        isHandle = true;
                    }).mouseup(function () {
                        isHandle = false;
                    });
                    $(this).data('items', options.items)
                    placeholders = placeholders.add(placeholder);
                    if (options.connectWith) {
                        $(options.connectWith).add(this).data('connectWith', options.connectWith);
                    }
                    items.attr('draggable', 'true').on('dragstart.h5s', function (e) {
                        if (options.handle && !isHandle) {
                            return false;
                        }
                        isHandle = false;
                        var dt = e.originalEvent.dataTransfer;
                        dt.effectAllowed = 'move';
                        dt.setData('Text', 'dummy');
                        index = (dragging = $(this)).addClass('sortable-dragging').index();
                    }).on('dragend.h5s', function () {
                        if (!dragging) {
                            return;
                        }
                        dragging.removeClass('sortable-dragging').show();
                        placeholders.detach();
                        if (index != dragging.index()) {
                            dragging.parent().trigger('sortupdate', {item: dragging});
                        }
                        dragging = null;
                    }).not('a[href], img').on('selectstart.h5s', function () {
                        this.dragDrop && this.dragDrop();
                        return false;
                    }).end().add([this, placeholder]).on('dragover.h5s dragenter.h5s drop.h5s', function (e) {
                        if (!items.is(dragging) && options.connectWith !== $(dragging).parent().data('connectWith')) {
                            return true;
                        }
                        if (e.type == 'drop') {
                            e.stopPropagation();
                            placeholders.filter(':visible').after(dragging);
                            dragging.trigger('dragend.h5s');
                            return false;
                        }
                        e.preventDefault();
                        e.originalEvent.dataTransfer.dropEffect = 'move';
                        if (items.is(this)) {
                            if (options.forcePlaceholderSize) {
                                placeholder.height(dragging.outerHeight());
                            }
                            dragging.hide();
                            $(this)[placeholder.index() < $(this).index() ? 'after' : 'before'](placeholder);
                            placeholders.not(placeholder).detach();
                        } else if (!placeholders.is(this) && !$(this).children(options.items).length) {
                            placeholders.detach();
                            $(this).append(placeholder);
                        }
                        return false;
                    });
                });
            };
        })(jQuery);


        $(function () {
            $('.sortable').sortable();
            $('.handles').sortable({
                handle: 'span'
            });
            $('.connected').sortable({
                connectWith: '.connected'
            });
            $('.exclude').sortable({
                items: ':not(.disabled)'
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#comapny_name').keyup(function () {
                var text = $(this).val();
                var slug1 = text.replace(/[#_*.,/+!@%^&()$]/g, "");
                var slug = slug1.replace(/[ ]/g, "-").toLowerCase();
                $('#slug').val(slug);
//               alert(text.replace(" ", "-").toLowerCase().replace(/[#_ -*.,/]/g, ""))
            });

            $('.deletetelno').click(function () {
                var contactnumber = $(this).attr('contact');
                var member_id = $(this).attr('member')
                var data = {'contactnumber': contactnumber, 'member_id': member_id};
                $(this).parents('li').remove();
                $.ajax({
                    type: "post",
                    url: '/premium/contact/delete-single/' + member_id,
                    data: data,
                    success: function (res) {
                        if (res.success) {
                            var formdata = $('#editphonenumbersform').serialize();
//                            alert(formdata);
                            $.ajax({
                                type: "post",
                                url: '/premium/contact/edit-save/' + member_id,
                                data: formdata,
                                success: function (res) {
                                    if (res.success) {
                                        noty({
                                            text: "Contact Number deleted successfully",
                                            layout: 'topRight',
                                            type: 'success',
                                            timeout: 3000
                                        });
                                        location.reload();
                                    } else {
                                        noty({
                                            text: "Contact Number not deleted",
                                            layout: 'topRight',
                                            type: 'error',
                                            timeout: 3000
                                        });
                                    }
                                }
                            });
                        } else {
                            noty({
                                text: "Something went wrong",
                                layout: 'topRight',
                                type: 'error',
                                timeout: 3000
                            });
                        }
                    }
                });
            });

            $('#description_save_btn').click(function () {
                var description = $('#description_textarea').val();
                var premiumid = $(this).attr('premiumid');
                var data = {'description':description , 'premiumid':premiumid}
                $.ajax({
                    type: "post",
                    url: '/premium/description/save/' + premiumid,
                    data: data,
                    success: function (res) {
                        noty({
                            text: res.message,
                            layout: 'topRight',
                            type: res.status,
                            timeout: 3000
                        });
                        setTimeout(function () {
                            window.location = '/my_ads';
                        }, delay);
                    }
                });
            });
        });
    </script>
@endsection
{{--END map locations--}}
{{--@include('main.footer')--}}