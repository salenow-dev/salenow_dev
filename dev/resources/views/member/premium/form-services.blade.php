<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Service Title</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            {!! Form::text('servicetitle', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Service Description</label>
    <div class="col-md-6 col-xs-12">
        {!! Form::textarea('service', null, ['size' => '5x5','class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Image</label>
    <div class="col-md-6 col-xs-12">
        @if(!empty($service->image))
            <div class="col-xs-3">
                <div class="avatar">
                    <img src="{{asset('')}}salenow/images/prime-members/{{$service->memberpremium_id}}/{{$service->image}}" alt="Circle Image"
                         class="img-circle img-no-padding img-responsive">
                </div>
            </div>
            {!! Form::hidden('dbimage', $service->image, ['class' => 'form-control']) !!}
        @endif
        {!! Form::file('file', null, ['class' => 'fileinput btn-primary']) !!}
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Featured</label>
    <div class="col-md-6 col-xs-12">
        {{ Form::checkbox('featured') }}
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label"></label>
    <div class="col-md-6 col-xs-12">
        {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
    </div>
</div>