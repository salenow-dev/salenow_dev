{{--@section('title', 'SaleMe | Member profile')--}}
{{--@include('main.header')--}}
@extends('layouts.member.premium_admin')
@section('title', 'Team | Saleme.lk')
@section('content')
    <section>
        <div class=" prime-container prime-shop-container">

            <div class="row main-row">
                <div class="col-md-12">
                    <div class="clear-fix"></div>
                    @include('errors.flash')
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Meet Our Team</h4>
                        </div>
                        <style>
                            body {
                                margin-top: 20px;
                            }

                            .glyphicon {
                                margin-right: 5px;
                            }

                            .section-box h2 {
                                margin-top: 0px;
                            }

                            .section-box h2 a {
                                font-size: 15px;
                            }

                            .glyphicon-heart {
                                color: #e74c3c;
                            }

                            .glyphicon-comment {
                                color: #27ae60;
                            }

                            .separator {
                                padding-right: 5px;
                                padding-left: 5px;
                            }

                            .section-box hr {
                                margin-top: 0;
                                margin-bottom: 5px;
                                border: 0;
                                border-top: 1px solid rgb(199, 199, 199);
                            }
                        </style>
                        {{--<div class="row">--}}

                        @foreach($employees->chunk(2) as $row)
                            <div class="row">
                                <div class="col-md-12">
                                    @foreach($row as $employee)
                                        <div class="col-md-6 col-xs-12 col-sm-6">
                                            <div class="card card-user">
                                                <div class="image">
                                                </div>
                                                <div class="content">
                                                    <div class="author">
                                                        <img class="avatar border-white"
                                                             src="{{(!empty($employee->member->avatar))?$employee->member->avatar:asset('/images/user.png')}}"
                                                             alt="...">
                                                        <h4 class="title">{{$employee->member->first_name}} {{$employee->member->last_name}}
                                                            <br>
                                                            <small>@if($employee->designation){{$employee->designation}}@endif</small>
                                                        </h4>
                                                    </div>

                                                    @if($member->premiummember)
                                                        <p class="description text-center">
                                                            {{$member->premiummember->company_name}}
                                                        </p>
                                                    @endif
                                                    @if($employee->email)<h5
                                                            class="text-center">{{$employee->email}}</h5>@endif


                                                </div>

                                            </div>

                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                        {{--</div>--}}
                        <div class="clear-fix"></div>
                        {{--</div>--}}
                    </div>
                </div>
            </div>
            <div class="clear-fix"></div>
        </div>
    </section>
    <script>

    </script>
    {{--@include('main.footer')--}}
@endsection