{{--@section('title', 'SaleMe | Member profile')--}}
{{--@include('main.header')--}}
{{--{{dd($searchMembers->membercontacts)}}--}}
@extends('layouts.member.premium_admin')
@section('title', 'Edit Location | Saleme.lk')
@section('content')
<section>
    <style>
        .sellerName .avatar {
            background: #fea502;
            width: 40px;
            height: 40px;
            float: left;
            border-radius: 50%;
            text-align: center;
            color: #fafafa;
            font-size: 30px;
            text-transform: capitalize;
            position: relative;
            line-height: 38px;
            margin-right: 10px;
        }

        .avatar-photo {
            border: 2px solid #F5F5F5;
            -moz-border-radius: 50%;
            -webkit-border-radius: 50%;
            border-radius: 50%;
            width: 40px;
            margin-right: 10px;
        }

        /* BOOTSTRAP DATEPICKER */
        .datepicker {
            padding: 10px;
            -moz-box-shadow: 0px 2px 1px 0px rgba(0, 0, 0, 0.1);
            -webkit-box-shadow: 0px 2px 1px 0px rgba(0, 0, 0, 0.1);
            box-shadow: 0px 2px 1px 0px rgba(0, 0, 0, 0.1);
            min-width: 250px;
        }

        .datepicker > div {
            display: none;
        }

        .datepicker table {
            width: 100%;
            margin: 0;
        }

        .datepicker td,
        .datepicker th {
            text-align: center;
            width: 20px;
            height: 20px;
        }

        .datepicker td.day:hover {
            background: #fea502;
            border-radius: 5px;
            color: #fff;
            cursor: pointer;
        }

        .datepicker td.day.disabled {
            color: #CCC;
        }

        .datepicker td.old,
        .datepicker td.new {
            color: #999;
        }

        .datepicker td.active,
        .datepicker td.active:hover {
            background: #fea502;
            color: #fff;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            border-radius: 3px;
        }

        .datepicker td span {
            display: block;
            width: 31%;
            height: 54px;
            line-height: 54px;
            float: left;
            margin: 2px;
            cursor: pointer;
        }

        .datepicker td span:hover {
            background: #F5F5F5;
        }

        .datepicker td span.active {
            background: #fea502;
            color: #fff;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            border-radius: 3px;
        }

        .datepicker td span.old {
            color: #999;
        }

        .datepicker th.switch {
            width: 145px;
        }

        .datepicker th.next,
        .datepicker th.prev {
            font-size: 12px;
        }

        .datepicker thead tr:first-child th {
            cursor: pointer;
            padding: 8px 0px;
        }

        .datepicker thead tr:first-child th:hover {
            background: #F5F5F5;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            border-radius: 3px;
        }

        .input-append.date .add-on i,
        .input-prepend.date .add-on i {
            display: block;
            cursor: pointer;
            width: 16px;
            height: 16px;
        }

        .datepicker.dropdown-menu:after,
        .datepicker.dropdown-menu:before {
            left: 16px;
        }

        .datepicker.datepicker-orient-left.dropdown-menu:after,
        .datepicker.datepicker-orient-left.dropdown-menu:before {
            left: auto;
            right: 16px;
        }

        /* EOF BOOTSTRAP DATEPICKER */
        /* BOOTSTRAP TIMEPICKER */

        .pac-card {
            margin: 0 0 15px 0;
            border-radius: 0px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            outline: none;
            /*box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);*/
            background-color: #fff;
            border: 1px solid #dadada;
            padding-bottom: 15px;
            font-family: Roboto;
        }

        #pac-container {
            /*padding-bottom: 12px;*/
            /*margin-right: 12px;*/
            margin-top: 12px !important;
        }

        .pac-controls {
            display: inline-block;
            padding: 5px 11px;
        }

        .pac-controls label {
            font-family: Roboto;
            font-size: 13px;
            font-weight: 300;
        }

        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            /*margin-left: 12px;*/
            padding: 5px;
            text-overflow: ellipsis;
            width: 100%;
            outline: none;
            border: 1px solid #ccc;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }

        #title span {
            font-size: 11px;
            color: #eee;
        }

        #title {
            color: #fff;
            background-color: #4d90fe;
            font-size: 20px;
            font-weight: 500;
            padding: 6px 12px;
            margin-bottom: 10px;

        }

        .txt-box {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            /*margin-left: 12px;*/
            padding: 5px;
            text-overflow: ellipsis;
            width: 100%;
            outline: none;
            border: 1px solid #ccc;
        }

        .save-loc {
            padding: 6px 0px;
            margin-top: 24px;
            /*background-color: #fea502;*/
            /*color: #fff !important;*/
            font-weight: 400;
            text-transform: capitalize;
            /*border: 0px;*/
            /*border-radius: 0px;*/
            font-size: 14px;
            width: 100%;
        }
        #register-form__map{
            width:100%;
            height:400px;
        }
    </style>


    <div class="prime-container prime-shop-container">
        <div class="row main-row">
            <div class="col-md-12">
                <div class="clear-fix"></div>
                <div class="card">
                    <div class="header">
                        <h4 class="title">Mark your Company Location <small>Drag the pin to mark location</small></h4>
                    </div>
                    <div class="panel-body">
                        <div class="pac-card" id="pac-card">
                            <div id="pac-container" class="col-md-4 col-sm-4 col-xs-12">
                                <lable>Search Location</lable>
                                <input id="pac-input" type="text"
                                       placeholder="Enter a location" class="register-form__location-holder"
                                       value="">
                            </div>
                            {!! Form::open(['url' => 'premium/location/update/'. $memberid,'files' => true]) !!}
                            <div class="col-md-6 col-sm-6 col-xs-12 lat-lon-div">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <lable>Latitude :</lable>
                                    <input name="lat" value="{{$lat}}"
                                           class="register-form__latitude-holder txt-box">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <lable>Longitude :</lable>
                                    <input name="lng" value="{{$lng}}"
                                           class="register-form__longitude-holder txt-box">
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <button class="pull-right btn btn-primary save-loc" type="submit">Save Location</button>
                            </div>
                            {!! Form::close() !!}
                            <div class="clear-fix"></div>
                        </div>


                        {{--<input type="text" id="user_location" name="user_location"--}}
                        {{--class="register-form__location-holder"> <a href="#"--}}
                        {{--class="button button--small register-wizard__map-search-button">Search--}}
                        {{--adr.</a>--}}
                        <div id="register-form__map"
                             class="register-form__map register-form__map--user"></div>


                        <div class="clear-fix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear-fix"></div>
    </div>
</section>

{{--map locations--}}
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMZ1lBPKgmEWvAP6uvBoEv7zouf_73FLw&libraries=places"></script>
<!-- <script src="https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDMZ1lBPKgmEWvAP6uvBoEv7zouf_73FLw&address=kandy" ></script> -->
<script>
    $(document).ready(function (e) {
        // init map
        function initMap(lat, long) {

            var card = document.getElementById('pac-card');
            var input = document.getElementById('pac-input');


            var center = new google.maps.LatLng(parseFloat(lat), long);
            var mapOptions = {center: center, zoom: 16, scrollwheel: true};
            map = new google.maps.Map(document.getElementById("register-form__map"), mapOptions);
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, long),
                draggable: true,
                map: map,
                title: 'Drag pin to select location'
            });

//            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);

            google.maps.event.addListener(marker, 'dragend', function (event) {
                var lat = this.getPosition().lat();
                var long = this.getPosition().lng();
                // initMap(lat, long);
                $('.register-form__latitude-holder').val(lat);
                $('.register-form__longitude-holder').val(long);
            });

            autocomplete.addListener('place_changed', function () {
                // infowindow.close();
                marker.setVisible(true);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                    map.setZoom(15);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);  // Why 17? Because it looks good.
                }
                map.setCenter(place.geometry.location);
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                var lat = marker.getPosition().lat();
                var long = marker.getPosition().lng();
                // initMap(lat, long);
                $('.register-form__latitude-holder').val(lat);
                $('.register-form__longitude-holder').val(long);
                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }


            });
        }

        var lat = $('.register-form__latitude-holder').val();
        var long = $('.register-form__longitude-holder').val();
        initMap(lat, long);
    });

</script>
@endsection
{{--END map locations--}}
{{--@include('main.footer')--}}