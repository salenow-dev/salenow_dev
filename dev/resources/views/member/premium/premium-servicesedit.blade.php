{{--@section('title', 'SaleMe | Member profile')--}}
{{--@include('main.header')--}}
@extends('layouts.member.premium_admin')
@section('title', 'Team | Saleme.lk')
@section('content')
    <section>
        <div class=" prime-container prime-shop-container">
            <div class="row main-row">
                <div class="col-md-12">
                    <div class="clear-fix"></div>
                    @include('errors.list')
                    {!! Form::model($service, ['url'=>'/premium/services/'.$service->id,'class'=>'form-horizontal','autocomplete' => 'off','files' => true]) !!}
                        {{csrf_field()}}
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><strong>Add New </strong> Service</h3>
                            </div>
                            <div class="panel-body">
                                @include('member.premium.form-services')
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="clear-fix"></div>
        </div>
    </section>
@endsection