@section('title', 'SaleMe | Become Premium Member')
@include('main.header')
<section class="mar-top-20">
    <div class="pin-bg">
    <div class="container  min-height-1200">
        <div class="">
        @include('errors.list')
        @include('errors.flash')
        <div class="col-md-6">
            <div class="pintop-content">
                <h4>Become a Premium Member</h4>
                <p>Premium membership profile is specifically designed for<span> Expand
                    your Business </span> , increase your sales and to give unmatched publicity to company
                    or your brand through SaleMe.lk. </p>
                        <strong>Benefits</strong><br>
                        <ul>
                            <li><i class="ion-android-globe"></i><span><strong>Create Own Website </strong>Brand new experience for the people to have their <br>Own Web
                    Site </></li>
                            <li><i class="ion-ios-speedometer-outline"></i><span><strong>Own control panel</strong>Fully Manageble Dashboard</span></li>
                            <li><i class="ion-ios-people"></i><span><strong>Manage Employees </strong> allocate the members for post ads by
your own.</span></li>
                            <li><i class="ion-navigate"></i><span><strong>Directions</strong> To Business Location</span></li>
                            <li><i class="ion-ribbon-b"></i><span><strong>25% off for the promote top ads. </strong> Reach With premium benifits</span></li>
                            <li><i class="ion-arrow-graph-up-right"></i><span><strong>Manage Ads With Statistics</strong> All of your bulk ads in one place.</span></li>
                            <li><i class="ion-trophy"></i><span><strong>Priority </strong> On Related Ads</span></li>
                        </ul>
            </div>
        </div>
        <div class="col-md-6">
            <div class="nextadform">
            {!! Form::open(['url' => 'premium' ,'files' => true,'autocomplete' => 'off']) !!}
            @include('member.premium.form')
            <div class="row  mg-b-10 hover-eee ">
                <div class="blockui col-md-12 ">
                <div class=" pull-right ">
                    <input type="submit" name="formsubmit" value="Continue" class="vehicel_btn btn post-btn-bottom  btn-lg blockui">
                </div>
                </div>
                <div class="clear-fix"></div>
            </div>
            {!! Form::close() !!}
        </div>
        </div>
    </div>
    </div>
    </div>
</section>
<div class="mar-b-25">
</div>
@include('main.shortfooter')