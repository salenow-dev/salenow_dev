{{--@section('title', 'SaleMe | Member profile')--}}
{{--@include('main.header')--}}
{{--{{dd($searchMembers->membercontacts)}}--}}
@extends('layouts.member.premium_admin')
@section('title', 'Manage Company | Saleme.lk')
@section('content')
    <section>
        <style>
            .sellerName .avatar {
                background: #fea502;
                width: 40px;
                height: 40px;
                float: left;
                border-radius: 50%;
                text-align: center;
                color: #fafafa;
                font-size: 30px;
                text-transform: capitalize;
                position: relative;
                line-height: 38px;
                margin-right: 10px;
            }

            .avatar-photo {
                border: 2px solid #F5F5F5;
                -moz-border-radius: 50%;
                -webkit-border-radius: 50%;
                border-radius: 50%;
                width: 40px;
                margin-right: 10px;
            }
        </style>
        <div class="prime-container prime-shop-container">
            <div class="row main-row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Search Member by E-mail Address</h4>
                        </div>
                        <div class="content">
                            <form id="searchAgent" class="form-horizontal" role="form" method="post"
                                  action="/company/manage">
                                {{csrf_field()}}
                                <div class="col-md-6 col-sm-6 col-xs-7">
                                    <div class="">
                                        <input type="text" name="email" class="form-control"
                                               placeholder="Member email"
                                               value="{{ old('email') }}">
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-5">
                                    <div class="">
                                        <button type="submit" name="search" value="searchAll"
                                                class="btn btn-primary">Search<i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="">
                                    <div class="switch-wrap">
                                        <div class="check-div">
                                            <h5>Make Team Public</h5>
                                            <input id="toggleLights" type="checkbox" {{($member->premiummember->teampublic)? 'checked':''}}/>
                                            <label for="toggleLights"><i class="fa fa-check"></i></label>

                                            {{--<input type="checkbox" value="1" id="input_1" checked>--}}
                                            {{--<label for='input_1' tabindex="-1">--}}
                                                {{--<span class="check"></span>--}}
                                                {{--Checked--}}
                                            {{--</label>--}}
                                        </div>
                                        {{--<input id="toggleLights" type="checkbox" />--}}
                                        {{--<label for="toggleLights" class="switch">--}}
                                            {{--<div class="slide-wrap">--}}
                                                {{--<div class="slide">--}}
                                                    {{--<div class="slider"></div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</label>--}}
                                    </div>
                                </div>
                                <div class="clear-fix"></div>
                                <div id="massage"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    @if(!empty($searchMembers))
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Search Result</h4>
                            </div>
                            <div class="content">
                                <form id="assignMember" class="form-horizontal" role="form" method="post"
                                      action="/premium/memberassign">
                                    {{csrf_field()}}

                                    <div class="row">
                                        <div class="col-xs-3 col-md-1 col-sm-2">
                                            <div class="avatar">
                                                <img src="{{(!empty($searchMembers->avatar))?$searchMembers->avatar:asset('/images/user.png')}}"
                                                     alt="Circle Image"
                                                     class="img-circle img-no-padding img-responsive">
                                            </div>
                                        </div>
                                        <div class="col-xs-9 col-md-6 col-sm-5">
                                            {{$searchMembers->first_name}} {{$searchMembers->last_name}}
                                            <br/>
                                            <span class="text-success"><small>{{$searchMembers->email}}</small></span>
                                        </div>
                                        <input type="hidden" id="company_id" name="company_id"
                                               value="{{$member->premiummember->member_id}}">
                                        <input type="hidden" id="member_id" name="member_id"
                                               value="{{$searchMembers->id}}">

                                        <div class="col-xs-12 col-md-5 col-sm-5 text-right control-div">
                                            {{--<div class="list-group-controls">--}}
                                            @if(!$searchMembers->parent_id)

                                                <div class="form-inline">
                                                    <div class="col-xs-6 col-md-9">
                                                    {!! Form::select('membergroup_id', $employeeTypes, null, ['class' => 'form-control border-input' ]) !!}
                                                    </div>
                                                    <div class="col-xs-6 col-md-3">
                                                        <input type="submit" class="btn btn-primary"
                                                           value="Assign">
                                                    </div>
                                                    {{--<button type="button" id="assignEmployee" class="btn btn-primary">--}}
                                                    {{--<i class="fa fa-check-circle"></i>&nbsp;Assign Employee</button>--}}

                                                </div>
                                            @else
                                                @if($member->parent_id == $searchMembers->parent_id)
                                                    @if($searchMembers->premiummember)
                                                        <span class="label label-default label-form">Already Assigned to: &nbsp;{{$searchMembers->premiummember->company_name}}
                                                            <i class="fa fa-male"></i></span>
                                                    @endif
                                                @endif
                                            @endif
                                            {{--</div>--}}
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    @else
                        @if($searchMembers === null)
                            <div class="card">
                                <div class="header">
                                    <h4 class="title">Search Result</h4>
                                </div>
                                <div class="content">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <p>No member found. Please check E-mail address</p>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        @endif
                    @endif
                    @if(!empty($employees))
                    <!-- CONTACTS WITH CONTROLS -->
                        <style>
                            /* LIST GROUP CONTACTS */
                            .list-group-contacts .list-group-item {
                                padding: 10px;
                            }

                            .list-group-contacts .list-group-item img {
                                border: 2px solid #F5F5F5;
                                -moz-border-radius: 50%;
                                -webkit-border-radius: 50%;
                                border-radius: 50%;
                                width: 40px;
                                margin-right: 10px;
                            }

                            .list-group-contacts .list-group-item .contacts-title {
                                font-size: 13px;
                                font-weight: 600;
                                line-height: 20px;
                            }

                            .list-group-contacts .list-group-item p {
                                margin-bottom: 0px;
                                line-height: 20px;
                            }

                            .list-group-contacts .list-group-item.active {
                                background: #F5F5F5;
                                border-color: #E5E5E5;
                                color: #656d78;
                            }

                            .list-group-controls {
                                position: absolute;
                                right: 10px;
                                top: 15px;
                            }

                            .list-group-status {
                                float: left;
                                margin-right: 10px;
                                height: 40px;
                                width: 10px;
                                position: relative;
                            }

                            .list-group-status:after {
                                position: absolute;
                                left: 0px;
                                top: 15px;
                                width: 10px;
                                height: 10px;
                                border: 2px solid #CCC;
                                -moz-border-radius: 50%;
                                -webkit-border-radius: 50%;
                                border-radius: 50%;
                                content: " ";
                                background: transparent;
                            }

                            .list-group-status.status-online:after {
                                border-color: #95b75d;
                            }

                            .list-group-status.status-away:after {
                                border-color: #fea223;
                            }

                            .list-group-status.status-offline:after {
                                border-color: #CCC;
                            }

                            /* END LIST GROUP CONTACTS */
                            /* LIST GROUP CONTACTS */
                            .list-group-contacts .list-group-item {
                                padding: 10px;
                            }

                            .list-group-contacts .list-group-item img {
                                border: 2px solid #F5F5F5;
                                -moz-border-radius: 50%;
                                -webkit-border-radius: 50%;
                                border-radius: 50%;
                                width: 90px;
                                margin-right: 10px;
                            }

                            .list-group-contacts .list-group-item .contacts-title {
                                font-size: 13px;
                                font-weight: 600;
                                line-height: 20px;
                            }

                            .list-group-contacts .list-group-item p {
                                margin-bottom: 0px;
                                line-height: 20px;
                            }

                            .list-group-contacts .list-group-item.active {
                                background: #F5F5F5;
                                border-color: #E5E5E5;
                                color: #656d78;
                            }

                            .list-group-controls {
                                position: absolute;
                                right: 10px;
                                top: 15px;
                            }

                            .list-group-status {
                                float: left;
                                margin-right: 10px;
                                height: 40px;
                                width: 10px;
                                position: relative;
                            }

                            .list-group-status:after {
                                position: absolute;
                                left: 0px;
                                top: 15px;
                                width: 10px;
                                height: 10px;
                                border: 2px solid #CCC;
                                -moz-border-radius: 50%;
                                -webkit-border-radius: 50%;
                                border-radius: 50%;
                                content: " ";
                                background: transparent;
                            }

                            .list-group-status.status-online:after {
                                border-color: #95b75d;
                            }

                            .list-group-status.status-away:after {
                                border-color: #fea223;
                            }

                            .list-group-status.status-offline:after {
                                border-color: #CCC;
                            }

                            /* END LIST GROUP CONTACTS */
                        </style>

                        <div class="card">
                            <div class="header">
                                <h4 class="title">Employees With Controls</h4>
                            </div>
                            <div class="content">
                                <ul class="list-unstyled team-members">
                                    @foreach($employees as $employee)
                                        <li class="col-md-6 col-sm-12 col-xs-12">
                                            {{--<div class="row">--}}
                                            <div class="col-xs-3 col-md-2 col-sm-2">
                                                <div class="avatar">
                                                    <img src="{{(!empty($employee->member->avatar))?$employee->member->avatar:asset('/images/user.png')}}"
                                                         alt="Circle Image"
                                                         class="img-circle img-no-padding img-responsive">
                                                </div>
                                            </div>
                                            <div class="col-xs-7 col-md-8 col-sm-8">
                                                {{$employee->member->first_name}} {{$employee->member->last_name}}
                                                <br/>
                                                <span class="text-success">
                                                        <small>{{$employee->member->email}}
                                                            | Type: {{$employee->employeegroup->name}}</small>
                                                    </span>
                                            </div>

                                            <div class="col-xs-2 col-md-2 col-sm-2 text-right">
                                                <a href="/employee/{{$employee->id}}/edit">
                                                    <btn class="btn btn-sm btn-info btn-icon"><i
                                                                class="fa fa-pencil"></i></btn>
                                                </a>
                                            </div>
                                            {{--</div>--}}
                                        </li>
                                    @endforeach
                                    <div class="clear-fix"></div>

                                </ul>
                            </div>
                        </div>

                        <!-- END CONTACTS WITH CONTROLS -->
                    @endif
                    @if(count($pendingEmployees))
                    <!-- CONTACTS WITH CONTROLS -->
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Pending Employee Approvals</h4>
                            </div>
                            <div class="content">
                                <ul class="list-unstyled team-members">
                                    @foreach($pendingEmployees as $employee)
                                        <li class="col-md-12">
                                            {{--<div class="row">--}}
                                            <div class="col-xs-3 col-md-1 col-sm-2">
                                                <div class="avatar">
                                                    <img src="{{(!empty($employee->member->avatar))?$employee->member->avatar:asset('/images/user.png')}}"
                                                         alt="Circle Image"
                                                         class="img-circle img-no-padding img-responsive">
                                                </div>
                                            </div>
                                            <div class="col-xs-8  col-md-8 col-sm-8">
                                                {{$employee->member->first_name}} {{$employee->member->last_name}}
                                                <br/>
                                                <span class="text-success">
                                                        <small>{{$employee->member->email}}</small>
                                                    </span><br>
                                                <span class="text-info">
                                                        <small>Requested Access Type -> {{$employee->employeegroup->name}}</small>
                                                    </span>
                                            </div>

                                            <div class="col-xs-12  col-md-3 col-sm-2 text-right">
                                                {!! Form::model($employee, ['url'=>'/employee/update/'.$employee->id,'class'=>'form-horizontal']) !!}
                                                <input type="hidden" name="approveEmployee">
                                                {{csrf_field()}}
                                                <button type="submit" class="btn btn-primary btn-rounded">Approve
                                                </button>
                                                {!! Form::close() !!}
                                            </div>
                                            {{--</div>--}}
                                        </li>
                                    @endforeach
                                    <div class="clear-fix"></div>

                                </ul>
                            </div>
                        </div>

                        <!-- END CONTACTS WITH CONTROLS -->
                    @endif
                    @if(count($resignedEmployees))
                    <!-- CONTACTS WITH CONTROLS -->

                        <div class="card">
                            <div class="header">
                                <h4 class="title">Resigned Employees</h4>
                            </div>
                            <div class="content">
                                <ul class="list-unstyled team-members">
                                    @foreach($resignedEmployees as $employee)
                                        <li class="col-md-12">
                                            {{--<div class="row">--}}
                                            <div class="col-xs-3 col-md-2 col-sm-2">
                                                <div class="avatar">
                                                    <img src="{{(!empty($employee->member->avatar))?$employee->member->avatar:asset('/images/user.png')}}"
                                                         alt="Circle Image"
                                                         class="img-circle img-no-padding img-responsive">
                                                </div>
                                            </div>
                                            <div class="col-xs-9 col-md-10 col-sm-10">
                                                {{$employee->member->first_name}} {{$employee->member->last_name}}
                                                <br/>
                                                <span class="text-success">
                                                        <small>{{$employee->member->email}}
                                                            | Type: {{$employee->employeegroup->name}}</small>
                                                    </span>
                                            </div>
                                        </li>
                                    @endforeach
                                    <div class="clear-fix"></div>

                                </ul>
                            </div>
                        </div>
                        <!-- END CONTACTS WITH CONTROLS -->
                    @endif
                </div>

            </div>
            <div class="clear-fix"></div>
        </div>
    </section>
    <script>
        $('.expan-click').click(function () {
            $('.tele-nos').slideToggle('slow');
            $('.expan-click').hide();
        });
        $('.email-send-btn').click(function () {
            $('.readmore').modal('hide');
        });
        $('.sitelink').click(function () {
//        window.location.href = "http://stackoverflow.com";
            var siteurl = $(".sitelink").attr("url");
            window.open(
                siteurl,
                '_blank' // <- This is what makes it open in a new window.
            );
        });

//        togle make team public
        $('#toggleLights').on('click', function(e) {
            if(this.checked){
                makePublic({{$member->parent_id}},1);
            }else{
                makePublic({{$member->parent_id}},0);
            }
        });

        function makePublic(id,state) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var data = {'member_id': id, 'teampublic':state};
            $.ajax({
                type: "POST",
                url: '/makepublic/' + id,
                data: data,
                success: function (res) {
                    if (res.status == 'success') {
                        $('#massage').show().addClass('alert alert-success').html(res.message);
                        setTimeout(function () {
                            $("#massage").slideUp()
                        },2000);
                        return false;
                    }
                }
            });
        }
    </script>
    {{--@include('main.footer')--}}
@endsection