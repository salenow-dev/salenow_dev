@section('title', 'SaleMe | Member profile')
@include('main.header')
{{--{{dd(Session::get('login_member_59ba36addc2b2f9401580f014c7f58ea4e30989d'))}}--}}
<div class="container cont-bg">
    <section>
        <div class="row">
            <div class="post-form-1">
                <div class="col-md-12 min-height-1200">
                    @include('errors.list')
                    @include('errors.flash')
                    <form action="/become-premium-member/save" method="post">
                        <div class="row">
                            <div class="col-md-2">
                                <h4 class="category-titles">Member Type</h4>
                            </div>
                            <div class="col-md-10">

                                <div class="col-md-4 prime-package text-center member-types-div pd-r-0 pd-l-0">
                                    <h4>Premium Member</h4>
                                    <p>First month free banner ads </p>
                                    <p>First month free banner ads </p>
                                    <p>First month free banner ads </p>
                                    <p>First month free banner ads </p>
                                    <label class="btn btn-primary">
                                        <input type="radio" name="mem_type" id="" value="member"> Become Premium
                                    </label>
                                </div>
                                <div class="col-md-4 silver-package text-center member-types-div pd-r-0 pd-l-0">
                                    <h4>Silver Member</h4>
                                    <p>First month free banner ads </p>
                                    <p>First month free banner ads </p>
                                    <p>First month free banner ads </p>
                                    <p>First month free banner ads </p>
                                    <label class="btn btn-primary">
                                        <input type="radio" name="mem_type" id="" value="silver"> Become Silver
                                    </label>
                                </div>
                                <div class="col-md-4 gold-package text-center member-types-div pd-r-0 pd-l-0">
                                    <h4>Gold Member</h4>
                                    <p>First month free banner ads </p>
                                    <p>First month free banner ads </p>
                                    <p>First month free banner ads </p>
                                    <p>First month free banner ads </p>
                                    <label class="btn btn-primary">
                                        <input type="radio" name="mem_type" id="" value="gold"> Become Gold
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="company-details">

                    </form>

                    <div class="member-details">

                    </div>

                </div>
                <div class="clear-fix"></div>
            </div>
        </div>
    </section>
</div>

<!--verify phone popup-->
<div class="modal fade add-phone-number" id="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="ion-ios-close-empty"></i>
                </button>
                <h4 class="modal-title text-center"><span class="no-1"><i
                                class="ion-ios-telephone-outline"></i> </span> &nbsp; Verify Phone Number</h4>
            </div>
            <div class="col-lg-12">
                <p>
                <h3>Please enter your mobile phone number</h3>
                If you haven't verified it before, we will send you an SMS with a PIN code to make sure that we can
                get in touch with you.</p>
                <form id="formNumberVerify" class="verify-number-form">
                    <div id="message"></div>
                    <div class="inline-group" id="newnumber">
                        <input type="text" name="number" id="number" class="form-control inline-item enter-phone"
                               required="required" placeholder="eg:07xxxxxxxx"/>
                        <input type="button" name="numberSubmit" id="numberSubmit"
                               class="btn btn-primary inline-item verify-btn"
                               value="Add Number"/>
                    </div>
                    <div class="inline-group" id="verifynumber">
                        <input type="text" name="number_verify" class="form-control inline-item enter-phone"
                               required="required" placeholder="Enter Pin Code"/>
                        <input type="button" name="verify" id="verify"
                               class="btn btn-primary inline-item verify-btn"
                               value="Verify"/>
                    </div>
                </form>
            </div>
            <div class="clear-fix"></div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset("js/post-ad/number-verify.js") }}"></script>
<script>
    $(document).ready(function () {
        $("input[name$='mem_type']").click(function () {
            var type = $(this).val();
            if (type == "member") {
                $('.company-details').html('<div class="row mg-b-10 hover-eee"> <div class="col-md-2"> <h4 class="category-titles">BusinessName</h4> </div><div class="col-md-6"> <div class="form-group"> <input class="form-control" placeholder="Company Name" id="company_name" name="company_name" type="text"> </div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee"> <div class="col-md-2"> <h4 class="category-titles">Address</h4> </div><div class="col-md-6"> <div class="form-group"> <textarea class="form-control" rows="5" name="address"></textarea> </div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee"> <div class="col-md-2"> <h4 class="category-titles">Phone Number</h4> </div><div class="col-md-4"> <div class="form-group"> <input class="form-control" placeholder="Company Name" id="company_name" name="company_name" type="text"> </div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee"> <div class="col-md-2"> <h4 class="category-titles">Email Address</h4> </div><div class="col-md-6"> <div class="form-group"> <input class="form-control" placeholder="Email Address" id="email" name="email" type="email"> </div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee "> <div class="col-md-2"> <h4 class="category-titles">BR / NIC Copy</h4> </div><div class="col-md-4"> <div class="form-group"> <input class="form-control" placeholder="Company Name" id="company_name" name="company_name" type="file"> </div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee"> <div class="col-md-2"> <h4 class="category-titles">Member Category</h4> </div><div class="col-md-4"> <div class="form-group">' +
                    '<select class="form-control show-tick">' +
                        @foreach($categorylist as $key=> $cat)
                            '<option value="{{$cat->category_name}}">{{$cat->category_name}}</option>' +
                        @endforeach
                            '</select></div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee"> <div class="col-md-2"> <h4 class="category-titles">Website</h4> </div><div class="col-md-4"> <div class="form-group"> <input class="form-control" placeholder="Company Name" id="company_name" name="WebSite URL" type="text"> </div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee"> <div class="col-md-4 pull-right"> <div class="form-group"> <input class="btn" type="submit" value="Submit"> </div></div><div class="clear-fix"></div></div>');

            } else if (type == "silver") {
                $('.company-details').html('<div class="row mg-b-10 hover-eee"> <div class="col-md-2"> <h4 class="category-titles">Company Name</h4> </div><div class="col-md-6"> <div class="form-group"> <input class="form-control" placeholder="Company Name" id="company_name" name="company_name" type="text"> </div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee"> <div class="col-md-2"> <h4 class="category-titles">Company Registration No</h4> </div><div class="col-md-4"> <div class="form-group"> <input class="form-control" placeholder="Company Name" id="company_name" name="company_name" type="text"> </div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee"> <div class="col-md-2"> <h4 class="category-titles">Address</h4> </div><div class="col-md-6"> <div class="form-group"> <textarea class="form-control" rows="5"></textarea> </div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee"> <div class="col-md-2"> <h4 class="category-titles">Phone Number</h4> </div><div class="col-md-4"> <div class="form-group"> <input class="form-control" placeholder="Company Name" id="company_name" name="company_name" type="text"> </div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee"> <div class="col-md-2"> <h4 class="category-titles">Email Address</h4> </div><div class="col-md-6"> <div class="form-group"> <input class="form-control" placeholder="Email Address" id="email" name="email" type="email"> </div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee "> <div class="col-md-2"> <h4 class="category-titles">BR Copy</h4> </div><div class="col-md-4"> <div class="form-group"> <input class="form-control" placeholder="Company Name" id="company_name" name="company_name" type="file"> </div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee"> <div class="col-md-2"> <h4 class="category-titles">Member Category</h4> </div><div class="col-md-4"> <div class="form-group">' +
                    '<select class="form-control show-tick">' +
                        @foreach($categorylist as $key=> $cat)
                            '<option value="{{$cat->category_name}}">{{$cat->category_name}}</option>' +
                        @endforeach
                            '</select></div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee"> <div class="col-md-2"> <h4 class="category-titles">Website</h4> </div><div class="col-md-4"> <div class="form-group"> <input class="form-control" placeholder="Company Name" id="company_name" name="WebSite URL" type="text"> </div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee"> <div class="col-md-4 pull-right"> <div class="form-group"> <input class="btn" type="submit" value="Submit"> </div></div><div class="clear-fix"></div></div>');

            } else {
                $('.company-details').html('<div class="row mg-b-10 hover-eee"> <div class="col-md-2"> <h4 class="category-titles">Company Name</h4> </div><div class="col-md-6"> <div class="form-group"> <input class="form-control" placeholder="Company Name" id="company_name" name="company_name" type="text"> </div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee"> <div class="col-md-2"> <h4 class="category-titles">Company Registration No</h4> </div><div class="col-md-4"> <div class="form-group"> <input class="form-control" placeholder="Company Name" id="company_name" name="company_name" type="text"> </div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee"> <div class="col-md-2"> <h4 class="category-titles">Address</h4> </div><div class="col-md-6"> <div class="form-group"> <textarea class="form-control" rows="5"></textarea> </div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee"> <div class="col-md-2"> <h4 class="category-titles">Phone Number</h4> </div><div class="col-md-4"> <div class="form-group"> <input class="form-control" placeholder="Company Name" id="company_name" name="company_name" type="text"> </div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee"> <div class="col-md-2"> <h4 class="category-titles">Email Address</h4> </div><div class="col-md-6"> <div class="form-group"> <input class="form-control" placeholder="Email Address" id="email" name="email" type="email"> </div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee "> <div class="col-md-2"> <h4 class="category-titles">BR Copy</h4> </div><div class="col-md-4"> <div class="form-group"> <input class="form-control" placeholder="Company Name" id="company_name" name="company_name" type="file"> </div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee"> <div class="col-md-2"> <h4 class="category-titles">Member Category</h4> </div><div class="col-md-4"> <div class="form-group">' +
                    '<select class="form-control show-tick">' +
                        @foreach($categorylist as $key=> $cat)
                            '<option value="{{$cat->category_name}}">{{$cat->category_name}}</option>' +
                        @endforeach
                            '</select></div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee"> <div class="col-md-2"> <h4 class="category-titles">Website</h4> </div><div class="col-md-4"> <div class="form-group"> <input class="form-control" placeholder="Company Name" id="company_name" name="WebSite URL" type="text"> </div></div><div class="clear-fix"></div></div><div class="row mg-b-10 hover-eee"> <div class="col-md-4 pull-right"> <div class="form-group"> <input class="btn" type="submit" value="Submit"> </div></div><div class="clear-fix"></div></div>');

            }

        });
    });
</script>

@include('main.shortfooter')