{{--@section('title', 'SaleMe | Member profile')--}}
{{--@include('main.header')--}}
{{--{{dd($searchMembers->membercontacts)}}--}}
@extends('layouts.member.premium_admin')
@section('title', 'Manage Employees | Saleme.lk')
@section('content')
    <section>
        <style>
            .sellerName .avatar {
                background: #fea502;
                width: 40px;
                height: 40px;
                float: left;
                border-radius: 50%;
                text-align: center;
                color: #fafafa;
                font-size: 30px;
                text-transform: capitalize;
                position: relative;
                line-height: 38px;
                margin-right: 10px;
            }

            .avatar-photo {
                border: 2px solid #F5F5F5;
                -moz-border-radius: 50%;
                -webkit-border-radius: 50%;
                border-radius: 50%;
                width: 40px;
                margin-right: 10px;
            }

            /* BOOTSTRAP DATEPICKER */
            .datepicker {
                padding: 10px;
                -moz-box-shadow: 0px 2px 1px 0px rgba(0, 0, 0, 0.1);
                -webkit-box-shadow: 0px 2px 1px 0px rgba(0, 0, 0, 0.1);
                box-shadow: 0px 2px 1px 0px rgba(0, 0, 0, 0.1);
                min-width: 250px;
            }

            .datepicker > div {
                display: none;
            }

            .datepicker table {
                width: 100%;
                margin: 0;
            }

            .datepicker td,
            .datepicker th {
                text-align: center;
                width: 20px;
                height: 20px;
            }

            .datepicker td.day:hover {
                background: #fea502;
                border-radius: 5px;
                color: #fff;
                cursor: pointer;
            }

            .datepicker td.day.disabled {
                color: #CCC;
            }

            .datepicker td.old,
            .datepicker td.new {
                color: #999;
            }

            .datepicker td.active,
            .datepicker td.active:hover {
                background: #fea502;
                color: #fff;
                -moz-border-radius: 3px;
                -webkit-border-radius: 3px;
                border-radius: 3px;
            }

            .datepicker td span {
                display: block;
                width: 31%;
                height: 54px;
                line-height: 54px;
                float: left;
                margin: 2px;
                cursor: pointer;
            }

            .datepicker td span:hover {
                background: #F5F5F5;
            }

            .datepicker td span.active {
                background: #fea502;
                color: #fff;
                -moz-border-radius: 3px;
                -webkit-border-radius: 3px;
                border-radius: 3px;
            }

            .datepicker td span.old {
                color: #999;
            }

            .datepicker th.switch {
                width: 145px;
            }

            .datepicker th.next,
            .datepicker th.prev {
                font-size: 12px;
            }

            .datepicker thead tr:first-child th {
                cursor: pointer;
                padding: 8px 0px;
            }

            .datepicker thead tr:first-child th:hover {
                background: #F5F5F5;
                -moz-border-radius: 3px;
                -webkit-border-radius: 3px;
                border-radius: 3px;
            }

            .input-append.date .add-on i,
            .input-prepend.date .add-on i {
                display: block;
                cursor: pointer;
                width: 16px;
                height: 16px;
            }

            .datepicker.dropdown-menu:after,
            .datepicker.dropdown-menu:before {
                left: 16px;
            }

            .datepicker.datepicker-orient-left.dropdown-menu:after,
            .datepicker.datepicker-orient-left.dropdown-menu:before {
                left: auto;
                right: 16px;
            }

            /* EOF BOOTSTRAP DATEPICKER */
            /* BOOTSTRAP TIMEPICKER */
        </style>
        <div class="prime-container prime-shop-container">

            <div class="row main-row">
                {{--<div class=" col-md-2 profiles">--}}
                {{--<div class="col-md-12 thumbnail ">--}}
                {{--<div class="col-xs-12">--}}
                {{--<img src="{{asset('')}}salenow/images/prime-members/{{$member->premiummember->profile_image}}"--}}
                {{--class="img-responsive"/>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="clear-fix"></div>--}}
                {{--<h3 class="profile-heading-shop">--}}
                {{--{{$member->premiummember->company_name}}--}}
                {{--</h3>--}}
                {{--<p class="profile-tag">{{'@'.$member->premiummember->category->category_name}}</p>--}}
                {{--<ul class="nav nav-pills nav-stacked">--}}
                {{--<li role="presentation"><a href="/{{$member->premiummember->slug}}/manage"> Back to--}}
                {{--Manage </a></li>--}}
                {{--<li role="presentation"><a href="/myprofile"> Company Account </a></li>--}}
                {{--<li role="presentation"><a href="/my_ads?type=private"><span class="label label-primary label-form">Visit Private Profile</span>--}}
                {{--</a></li>--}}
                {{--</ul>--}}
                {{--<div class="clear-fix"></div>--}}
                {{--</div>--}}
                <div class="col-md-12">
                    <div class="clear-fix"></div>
                    @include('errors.flash')
                    <div class="card">
                        <div class="header">
                            <h3 class="title">Update Employee: {{$employee->member->first_name}}</h3>
                            <p>Modify employee's current company profile information.</p>
                        </div>
                        {!! Form::model($employee, ['url'=>'/employee/update/'.$employee->id,'class'=>'form-horizontal','autocomplete' => 'on']) !!}
                        {{--<form id="searchAgent" class="form-horizontal" role="form" method="post" action="/company/manage">--}}
                        {{csrf_field()}}
                        <div class="content">

                            <div class="form-group"></div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">First Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" value="{{$employee->member->first_name}}"
                                           readonly>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Last Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <input type="text" class="form-control" value="{{$employee->member->last_name}}"
                                           readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Designation</label>
                                <div class="col-md-6 col-xs-12">
                                    {{ Form::text('designation',$employee->designation, array('class' => 'form-control','placeholder'=>'Designation',  (!isEmployeeAdmin())?'readonly':'')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Nic</label>
                                <div class="col-md-6 col-xs-12">
                                    {{ Form::text('nic',$employee->nic, array('class' => 'form-control','placeholder'=>'NIC')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Company E-mail</label>
                                <div class="col-md-6 col-xs-12">
                                    {{ Form::text('email',$employee->email, array('class' => 'form-control','placeholder'=>'Company E-mail')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Date of birth</label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group date">
                                        {{ Form::text('dob',null, array('class' => 'form-control','placeholder'=>'Select Date of Birth','id'=>'dp-3')) }}
                                        <span class="input-group-addon"><span
                                                    class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Join Date</label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group date">
                                        {{ Form::text('joindate',$employee->joindate, array('class' => 'form-control datepicker','placeholder'=>'Join Date')) }}
                                        <span class="input-group-addon"><span
                                                    class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                            @if(isEmployeeAdmin())
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Member Group</label>
                                    <div class="col-md-6 col-xs-12">
                                        {{ Form::select('membergroup_id', $employeeTypes, null, ['class' => 'form-control select']) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Public</label>
                                    <div class="col-md-6 col-xs-12">
                                        {{ Form::checkbox('teampublic', $employee->teampublic, null, ['class' => '']) }}
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label"></label>
                            <div class="col-md-3 col-xs-12">
                                <button type="submit" name="save" value="searchAll" class="btn btn-primary">Update
                                    Employee&nbsp;
                                    <i class="fa fa-search"></i></button>
                            </div>
                            @if(isEmployeeAdmin())
                                <button type="submit" name="remove_employee" class="btn btn-primary assign">
                                    <i class="fa fa-chain"></i>&nbsp;Remove
                                </button>
                            @endif
                        </div>
                        <div class="clear-fix"></div>
                        {{--</form>--}}
                        {!! Form::close() !!}
                        <div class="clear-fix"></div>
                    </div>
                </div>
                <div class="clear-fix"></div>
            </div>
            <div class="clear-fix"></div>
        </div>
    </section>

    <script type='text/javascript'
            src='{{ asset("backend/assets/js/plugins/bootstrap/bootstrap-datepicker.js") }}'></script>
    <script>
        $('.datepicker').datepicker({format: 'yyyy-mm-dd', startView: 3});
        $('#dp-3').datepicker({format: 'yyyy-mm-dd', startView: 2});

        $('.expan-click').click(function () {
            $('.tele-nos').slideToggle('slow');
            $('.expan-click').hide();
        });
        $('.email-send-btn').click(function () {
            $('.readmore').modal('hide');
        });
        $('.sitelink').click(function () {
//        window.location.href = "http://stackoverflow.com";
            var siteurl = $(".sitelink").attr("url");
            window.open(
                siteurl,
                '_blank' // <- This is what makes it open in a new window.
            );
        });

    </script>
    {{--@include('main.footer')--}}
@endsection