{{Form::hidden('member_id',getMemberId())}}

<div class="col-md-12   hover-eee ">
    <h4 class="category-titles2">Company Name</h4>
    <div class="form-group">
        {{Form::text('company_name',  old('company_name'),['class' => 'form-control', 'placeholder'=>'Company Name','id'=>'company_name'])}}
    </div>
    <div class="clear-fix"></div>
</div>
<div class="col-md-12  hover-eee ">
    <h4 class="category-titles2">Website Title</h4>
    <div class="form-group">
        {{Form::text('webtitle',  old('webtitle'),['class' => 'form-control', 'placeholder'=>'Website Title','id'=>'webtitle'])}}
    </div>
    <div class="clear-fix"></div>
</div>
<div class="col-md-12  hover-eee ">
    <h4 class="category-titles2">Password</h4>
    <div class="form-group">
        {{ Form::password('password', array('class' => 'form-control', 'placeholder'=>'Enter a Password for company profile login','id'=>'password')) }}
    </div>
    <div class="clear-fix"></div>
</div>
<div class="col-md-12  hover-eee ">
    <h4 class="category-titles2">Company Category</h4>
    <div class="form-group">
        {{Form::select('category_id', $categorylist, NULL ,['class' =>'selectpicker show-tick form-control','placeholder'=>'Member Category'])}}
    </div>
    <div class="clear-fix"></div>
</div>
<div class="col-md-12 hover-eee ">
    <h4 class="category-titles2">Address</h4>
    <div class="form-group">
        {{Form::textarea('address', old('address'),['class' => 'form-control', 'placeholder'=>'Address','id'=>'address','size' => '1x3'])}}
    </div>
    <div class="clear-fix"></div>
</div>
@if(empty($premiumMember))
    <div class="col-md-12  hover-eee ">
        <h4 class="category-titles2">Email Address</h4>
        <div class="form-group">
            {{Form::text('email', old('email'),['class' => 'form-control', 'placeholder'=>'Company Email','id'=>'email'])}}
        </div>
        <div class="clear-fix"></div>
    </div>
@endif
<div class="col-md-12 mg-b-10 hover-eee ">
    <h4 class="category-titles2">Contact Numbers</h4>
    <div class="row">
        <div class="col-md-6" id="dynamic_tel">
            <div class="form-group">
                <div class="input-group date">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></span>
                    <input class="form-control" placeholder="011 xxx xxxx" id="telephone" name="telephone[]"
                           type="text">
                    {{--                        {{Form::text('telephone[]', old('telephone'),['class' => 'form-control', 'placeholder'=>'011 xxx xxxx','id'=>'telephone'])}}--}}
                    <span class="input-group-addon" id="addtelephone"><i
                                class="fa fa-plus-circle"></i> Add more</span>
                </div>

            </div>
        </div>
        <div class="col-md-6" id="dynamic_mob">
            <div class="form-group">
                <div class="input-group date">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-phone"></span></span>
                    <input class="form-control" placeholder="07x xxx xxxx" id="mobile" name="mobile[]" type="text" required>
                    {{--                        {{Form::text('mobile[]', old('mobile'),['class' => 'form-control', 'placeholder'=>'07x xxx xxxx','id'=>'mobile'])}}--}}
                    <span class="input-group-addon" id="addmobile"><i class="fa fa-plus-circle"></i> Add more</span>
                </div>
            </div>
        </div>
    </div>

    @if(!empty($contacts))
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {{--all contacts--}}
                    @if(!empty($contacts))
                        @foreach($contacts as $tel)
                            <div class="form-group" id="row{{$tel->id}}">
                                <div class="input-group date">
                                            <span class="input-group-addon"><span
                                                        class="glyphicon glyphicon-phone-alt"></span></span>
                                    <input class="form-control" placeholder="07x xxx xxxx" id="mobile"
                                           readonly name="telephone[]" type="text" value="{{substr($tel->contactnumber, 2)}}">
                                    <span class="input-group-addon btn btn-danger btn_delete" id="{{$tel->id}}"><i
                                                class="fa fa-minus-circle"></i></span>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>

            {{--<div class="col-md-12">--}}
                {{--<div class="form-group">--}}
                    {{--@if(!empty($premiumMember->telephone ))--}}
                        {{--@foreach($premiumMember->telephone as $tel)--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="input-group date">--}}
                                            {{--<span class="input-group-addon"><span--}}
                                                        {{--class="glyphicon glyphicon-phone-alt"></span></span>--}}
                                    {{--<input class="form-control" placeholder="07x xxx xxxx" id="mobile"--}}
                                           {{--disabled name="telephone[]" type="text" value="{{$tel->contactnumber}}">--}}
                                    {{--<span class="input-group-addon btn btn-danger btn_delete" id="{{$tel->id}}"><i--}}
                                                {{--class="fa fa-minus-circle"></i></span>--}}
                                {{--</div>--}}

                                {{--<div class="input-group date">--}}
                                            {{--<span class="input-group-addon"><span--}}
                                                        {{--class="glyphicon glyphicon-phone-alt"></span></span>--}}
                                    {{--<input class="form-control" placeholder="07x xxx xxxx" id="mobile"--}}
                                           {{--name="telephone[]" type="text" value="{{$tel}}">--}}
                                    {{--<span class="input-group-addon" id="addmobile"><i--}}
                                                {{--class="fa fa-trash"></i></span>--}}
                                    {{--<span class="input-group-addon btn btn-danger btn_delete" id="{{$tel->id}}"><i--}}
                                                {{--class="fa fa-minus-circle"></i></span>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<li class="list-group-item"><span class="glyphicon glyphicon-phone-alt"></span></span> {{$tel}}--}}
                            {{--<span class="btn btn-danger" id=""><i class="fa fa-trash"></i></span>--}}
                            {{--</li>--}}
                        {{--@endforeach--}}
                    {{--@endif--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-12">--}}
                {{--<div class="form-group">--}}
                    {{--@if(!empty($premiumMember->mobile ))--}}
                        {{--@foreach($premiumMember->mobile as $mob)--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="input-group date">--}}
                                            {{--<span class="input-group-addon"><span--}}
                                                        {{--class="glyphicon glyphicon-phone"></span></span>--}}
                                    {{--<input class="form-control" placeholder="07x xxx xxxx" id="mobile"--}}
                                           {{--name="mobile[]" type="text" value="{{$mob}}">--}}
                                    {{--<span class="input-group-addon" id="addmobile"><i--}}
                                                {{--class="fa fa-trash"></i></span>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<li class="list-group-item"><span class="glyphicon glyphicon-phone"></span> {{$mob}}--}}
                            {{--<span class="btn btn-danger" id=""><i class="fa fa-trash"></i></span>--}}
                            {{--</li>--}}
                        {{--@endforeach--}}
                    {{--@endif--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    @endif
</div>
<div class="clear-fix"></div>

<div class="row mg-b-10 hover-eee ">
    <div class="col-md-12">
        <div class="col-md-12">
            <h4 class="category-titles2">Website URL</h4>
            <div class="form-group">
                {{Form::text('website', old('website'),['class' => 'form-control', 'placeholder'=>'Website URL','id'=>'website'])}}
            </div>
        </div>
    </div>
    <div class="clear-fix"></div>
</div>
<div class="row  mg-b-10 hover-eee ">
    <div class="col-md-12">
        <div class="col-md-12">
            <h4 class="category-titles2 ">Description</h4>
            <div class="form-group">
                {{Form::textarea('description', old('description'),['class' => 'form-control', 'placeholder'=>'Description','id'=>'description'])}}
            </div>
        </div>
        <div class="clear-fix"></div>
    </div>
</div>

{{--<div class="row  mg-b-10 hover-eee ">--}}
{{--<div class="col-md-3">--}}
{{--<h4 class="category-titles">Location</h4>--}}

{{--</div>--}}
{{--<div class="col-md-6">--}}
{{--<div class="form-group">--}}
{{--{{Form::text('title', old('description'),['class' => 'form-control', 'placeholder'=>'Type Place','id'=>'title'])}}--}}
{{--</div>--}}
{{--<div class="form-group">--}}
{{--{{Form::text('lat',NULL,['class' => 'form-control', 'placeholder'=>'Latitude','id'=>'lat'])}}--}}
{{--</div>--}}
{{--<div class="form-group">--}}
{{--{{Form::text('lng',NULL,['class' => 'form-control', 'placeholder'=>'Longitude','id'=>'lng'])}}--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="clear-fix"></div>--}}
{{--</div>--}}

{{ Html::script('js/salenow/plugin/maskedinput/jquery.maskedinput.min.js') }}
<script type="text/javascript">
    $(document).ready(function () {
        var i = 1;
        var j = 1;

        $('#addmobile').click(function (e) {
            e.preventDefault();
            i++;
            var html = '<div class="col-md-12 no-padding" id="row' + i + '"><div class="form-group"> ' +
                '<div class="input-group date"> ' +
                '<span class="input-group-addon"><span class="glyphicon glyphicon-phone"></span></span> ' +
                '<input class="form-control" placeholder="07x xxx xxxx" id="mobile' + i + '" name="mobile[]" type="text"> ' +
                '<span class="input-group-addon btn btn-danger btn_remove" id="' + i + '"><i class="fa fa-minus-circle"></i></span>' +
                '</div>' +
                '</div></div>';
            $('#dynamic_mob').append(html);
        });

        $('#addtelephone').click(function (e) {
            e.preventDefault();
            j++;
            var html = '<div class="col-md-12 no-padding" id="row' + j + '"><div class="form-group"> ' +
                '<div class="input-group date"> ' +
                '<span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></span> ' +
                '<input class="form-control" placeholder="011 xxx xxxx" id="telephone' + j + '" name="telephone[]" type="text"> ' +
                '<span class="input-group-addon btn btn-danger btn_remove" id="' + j + '"><i class="fa fa-minus-circle"></i></span>' +
                '</div>' +
                '</div></div>';
            $('#dynamic_tel').append(html);
        });

        $(document).on('click', '.btn_remove', function (e) {
            e.preventDefault();
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        //delete a contact number
        $(document).on('click', '.btn_delete', function (e) {
            e.preventDefault();
            var button_id = $(this).attr("id");
            $.ajax({
                type: "POST",
                url: '/premium/contact/delete/' + button_id,
                data: {id: button_id},
                beforeSend: function () {
                    //$.blockUI({message: '<h1>Just a moment...</h1>'});
                },
                success: function (data) {
                    if (data.response.message == 'success') {
                        $('#row' + button_id + '').fadeOut("slow");
                        $('#row' + button_id + '').remove();
                    }
                }
            });
        });

    });

</script>
<style>
    .btn-danger:hover {
        color: #fff;
        background-color: #c9302c !important;
        border-color: #ac2925;
    }

    .btn-danger {
        color: #fff;
        background-color: #c9302c !important;
        border-color: #ac2925;
    }
</style>