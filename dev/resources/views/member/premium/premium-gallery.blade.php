{{--@section('title', 'SaleMe | Member profile')--}}
{{--@include('main.header')--}}
@extends('layouts.member.premium_admin')
@section('title', 'Team | Saleme.lk')
@section('content')
    <section>
        <div class=" prime-container prime-shop-container">
            <div class="row main-row">
                <div class="col-md-12">
                    <div class="clear-fix"></div>
                    @include('errors.list')
                    <form class="form-horizontal" name="services" action="/premium/gallery-add/store" method="post"
                          enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><strong>Add New </strong> Images to Gallery
                                    <span class="pull-right">Remaining images <b>{{12 - count($images)}}</b> out of 12</span>
                                </h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Select Multiple Images</label>
                                    <div class="col-md-3 col-xs-12">
                                        <input type="file" name="file[]" multiple class="fileinput"/>
                                    </div>
                                    <div class="col-md-3 col-xs-12">
                                        {{ Form::submit('Add Images to Gallery', ['class' => 'btn btn-primary']) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="clear-fix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h4 class="title"><strong>Gallery</strong> Images
                                <span class="pull-right"
                                      style="    font-size: 14px;">Remaining images <b>{{12 - count($images)}}</b> out of 12</span>
                            </h4>
                            <p class="category">Here is a list of Gallery Images</p>

                        </div>
                        <div class="content table-responsive table-full-width">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Added Date</th>
                                    <th>Featured</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($images as $img)
                                    <tr>
                                        <td>
                                            <div class="col-xs-3">
                                                <div class="" style="border-radius: 0px; width: 200px;">
                                                    <img src="{{asset('')}}salenow/images/prime-members/{{$img->memberpremium_id}}/{{$img->image}}"
                                                         alt="Circle Image"
                                                         class=" img-no-padding img-responsive">
                                                </div>
                                            </div>
                                        </td>
                                        <td>{{timeForUser($img->created_at)}}</td>
                                        <td>
                                            <input type="checkbox" value="1" member_id="{{$img->memberpremium_id}}"
                                                   image_id="{{$img->id}}"
                                                   {{($img->featured)?'checked featured=0':'featured=1'}} class="change_featued">
                                        </td>
                                        <td>

                                            <a image-id="{{$img->id}}" id="delete_image_{{$img->id}}"
                                               status="delete" class="delete-record delete_btn"
                                               onClick="notyImageDelete({{$img->id}})"><i
                                                        class="fa fa-trash"
                                                        aria-hidden="true"></i>
                                                Delete</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>No Images Found</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        $(document).ready(function () {
            $('.change_featued').click(function () {
                var image_id = $(this).attr('image_id');
                var member_id = $(this).attr('member_id');
                var featured = $(this).attr('featured');
                var data = {'image_id': image_id, 'member_id': member_id, 'featured':featured};
                $.ajax({
                    type: "post",
                    url: '/setfeaturedimage/' + image_id,
                    data: data,
                    success: function (res) {
                        noty({
                            text: res.message,
                            layout: 'topRight',
                            type: res.class,
                        });
                        setTimeout(function () {
                            location.reload();
                        },1000);
                    }
                });
            });
        });
    </script>
@endsection