@extends('layouts.saleme')
@section('title', 'Boost your Ad')
@section('content')
    <style>
        .plans {
            padding: 0 !important;
        }

        .plans h5 {
            padding: 20px 10px;
            color: #100f0f;
            font-size: 22px;
            text-transform: uppercase;
            letter-spacing: 1px;
            font-weight: bold;
        }

        .panel-pricing {
            -moz-transition: all .3s ease;
            -o-transition: all .3s ease;
            -webkit-transition: all .3s ease;
        }

        .panel-pricing:hover {
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);
        }

        .panel-pricing .panel-heading {
            padding: 13px 10px;
            color: #fdfdfd;
            /*background-color: #ed1c24;*/
            /*border-color: #ed1c24;*/
        }

        .panel-pricing .panel-heading .icon {
            margin-top: 0px;
            font-size: 30px;
            line-height: 1;
        }

        .panel {
            border: 1px solid #eee !important;
        }

        .panel-pricing .panel-body {
            font-size: 40px;
            padding: 10px;
            margin: 0px;
            border-bottom: 1px solid #cfcfcf;
        }

        .panel-footer {
            border: 0px solid !important;
            background-color: #fff !important;
            padding: 20px 0px;
        }

        .panel-pricing .panel-heading h3 {
            margin: 0;
            padding: 10px;
        }

        p.p-title {
            font-size: 18px;
            text-align: center;
            text-transform: capitalize;
        }

        p.p-time {
            font-size: 18px;
            text-align: center;
            text-transform: capitalize;
        }

        p.p-price {
            font-size: 18px;
            text-align: center;
            text-transform: capitalize;
            color: #000;
            font-weight: bold;
        }

        p.p-tax {
            font-size: 18px;
            text-align: center;
            text-transform: capitalize;
        }

        .btn-lg{
            font-family: 'Work Sans', sans-serif !important;
        }
    </style>
    <div class="container cont-bg">
        <div class="plans col-md-12 col-sm-12 col-xs-12 text-center">
            <h5>Select ad pack Type</h5>
        </div>
        <div class="row">
            @if(!empty($adPacks))
                @foreach($adPacks as $adPack)
                    <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                        <div class="panel panel-pricing">
                            <div class="panel-heading {{($adPack->referance == 'gold')?'gold-package':'silver-package'}}">
                                <i class="ion-pricetags icon"></i>
                                <h4>{{$adPack->boostpackname}}</h4>
                            </div>
                            <div class="panel-body text-center">
                                <p class="p-title">Pin to Top ads </p>
                                <p class="p-time">{{$adPack->pin_to_top}}</p>
                            </div>
                            {{--<div class="panel-body text-center">--}}
                                {{--<p class="p-title">Bump Up ads</p>--}}
                                {{--<p class="p-time">{{$adPack->bump_up}}</p>--}}
                            {{--</div>--}}
                            <div class="panel-body text-center">
                                <p class="p-title">Duration</p>
                                <p class="p-time">7 days</p>
                            </div>
                            <div class="panel-body text-center">
                                <p class="p-title">Validity Period</p>
                                <p class="p-time">{{$adPack->validitymonths}}
                                    Months</p>
                            </div>
                            <div class="panel-body text-center">
                                <p class="p-tax">Price</p><!--p-tax close-->
                                <p class="p-price">LKR: {{number_format($adPack->price)}}</p><!--p-price close-->
                            </div><!--panel-body text-center close-->
                            <div class="panel-footer">
                                <form action="/myprofile/pin-to-top" method="post" name="pinToTopFrm" id="pinToTopFrm" class="has-validation-callback">
                                    {{csrf_field()}}
                                    <input type="hidden" name="ads_pack" value="true">
                                    <input type="hidden" value="{{$adPack->id}}" name="boostpack_id" id="">
                                    <input type="hidden" name="member_id" value="{{getMemberId()}}">

                                    <input type="hidden" name="pintotop_count" value="{{$adPack->pin_to_top}}" id="">
                                    <input type="hidden" name="bump_up_count" value="{{$adPack->bump_up}}" id="">
                                    <input type="hidden" name="price" value="{{$adPack->price}}">
                                    <input type="hidden" name="validitymonths" value="{{$adPack->validitymonths}}">

                                    <input type="submit" class="btn btn-primary btn-lg" value="Select">
                                </form>
                            </div>
                        </div><!--panel panel-pricing close-->
                    </div><!--col-md-4 col-sm-4 col-xs-12 text-center close-->
            @endforeach
        @endif

        </div><!--row close-->
    </div><!--container close-->

@endsection
<!--verify phone popup-->
