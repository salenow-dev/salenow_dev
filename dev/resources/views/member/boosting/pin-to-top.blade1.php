@extends('layouts.saleme')
@section('title', 'Boost your Ad')
@section('content')
    <div class="pin-bg ">
        <div class="container ">
            <div class="row top-mar-50">
                <div class="mar-80">
                    <div class="col-lg-7 col-sm-6">

                        <div class="pintop-content">
                            <h4>Pin To Top</h4>
                            <p>Guaranteed placement in the <br><span>Top 2 Ads</span> <br>on search pages.</p>
                            <strong>Benefits</strong>
                            <ul>
                                <li><i class="lnr lnr-pushpin"></i><span>Ad pinned among <strong>top 2 slots</strong>, for 7 days</span>
                                </li>
                                <li><i class="lnr lnr-users"></i><span>Get <small>10X</small> more leads </span></li>
                                <li><i class="lnr lnr-bullhorn"></i><span><strong>Highlight</strong> yourself to become the <strong>top service provider</strong> in your city</span>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div class="col-lg-5 col-sm-6">
                        <div class="nextadform">
                            <h5>Next Ad Slot</h5>
                            <em class="no-ad-msg hide">Sorry! No Ad slots available for the next 4 weeks. Please try
                                again later.</em>
                            <div class="row">
                                <div class="col-xs-6 ">
                                    <small>Available on</small>
                                    <span class="expire-date">{{\Carbon\Carbon::now()->format('F j, Y')}}</span>
                                </div>
                                <div class="col-xs-6 ">
                                    <small>Expires on</small>
                                    <span class="expire-date">{{\Carbon\Carbon::now()->addDays(7)->format('F j, Y')}}</span>
                                </div>
                            </div>
                            <div class="adinfo-2">
                                <label class="pintotop-lable">Ad Location</label><br>
                                Kandy > Kadugannawa
                            </div>

                            <div class="adinfo-2">
                                <label class="pintotop-lable">Ad Category</label><br>
                                {{$info->category->category_name}}
                                > {{$subCategory->sub_category_name}}</div>
                            <form action="/myprofile/pin-to-top" method="post" name="pinToTopFrm" id="pinToTopFrm"
                                  class="has-validation-callback">
                                {{csrf_field()}}
                                <input type="hidden" name="adid" value="{{$info->id}}">
                                <input type="hidden" name="member_id" value="{{getMemberId()}}">
                                <input type="hidden" name="boosttype" value="{{$boosttype}}">
                                <input type="hidden" name="price" id="price" value="{{$adPacks->price}}">
                                <input type="hidden" name="validitymonths" value="{{$adPacks->validitymonths}}">
                                <input type="hidden" name="ad_referance" value="{{$info->ad_referance}}">
                                @if(!empty($pinStatus))
                                    <span>Already Pinned</span>
                                @else
                                    {{--boost vouchers--}}
                                    @if(!empty($voucherCodes))
                                        <div class="adinfo-2">
                                            <label class="pintotop-lable">Select Voucher</label>
                                            <select name="vouchercode" id="vouchercode"
                                                    class="show-tick form-control selectpicker">
                                                @foreach($voucherCodes as $voucher)
                                                    <option value="{{$voucher->id}}">{{(!empty($voucher->vouchercode))?$voucher->boosttype.' '.$voucher->id :''}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <input name="Submit" type="submit" onclick="createGa();setpaymentpage();"
                                               class="btn btn-warning btn-lg" value="Pay Now">
                                    @else
                                        @if(!count($checkPendinBoostForReleventAd))
                                            <p>Amount payable: <span id="totalPrice">{{number_format($adPacks->price)}}</span></p>
                                            <input name="Submit" type="submit" onclick="createGa();setpaymentpage();"
                                                   class="btn btn-warning btn-lg" value="Pay Now">
                                        @else
                                            <span>Pending Request</span>
                                        @endif
                                    @endif
                                @endif
                            </form>
                            <span style="color:red;" id="errormsg"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="top-mar-50"></div>
        </div>
    </div>
    <script src="http://saleme.me/js/location/district_city.js"></script>
    <script type="text/javascript">
        $("#adpack").change(function () {
            var option = $('option:selected', this).attr('packprice');
            if (typeof option === "undefined") {
                $('#price').val('');
                $('#totalPrice').html('');
            } else {
                $('#price').val(option);
                $('#totalPrice').html('LKR ' + option);
            }

        });
    </script>
@endsection
<!--verify phone popup-->
