@extends('layouts.saleme')
@section('title', 'Boost your Ad')
@section('content')
    <div class="pin-bg ">
        <div class="container ">
            <div class="row top-mar-50">
                <div class="mar-80">
                    <div class="col-lg-7 col-sm-6">

                        <div class="pintop-content">
                            <h4>Pin To Top</h4>
                            <p>Guaranteed placement in the <br><span>Top 2 Ads</span> <br>on search pages (Ads listing
                                page).</p>
                            <strong>Benefits</strong>
                            <ul>
                                <li><i class="lnr lnr-pushpin"></i><span>Ad pinned among <strong>top 2 slots</strong>Bigger than regular ads and highlighted <br>in yellow <b>for 7 days</b> </span>
                                </li>
                                <li><i class="lnr lnr-users"></i><span>You will get chances of<br> more than  <small>10X</small> times of <b>displaying</b> your add.</span>
                                </li>
                                <li><i class="lnr lnr-bullhorn"></i><span><strong>Highlight</strong> yourself to become the <strong>top service provider</strong> in your city</span>
                                </li>
                            </ul>
                            <p><a data-toggle="modal" data-target=".pintotop-modal" class="change-cat pull-right">Read
                                    More&nbsp;<i class="ion-ios-arrow-thin-right ico2"></i></a></p>
                        </div>

                    </div>
                    <div class="col-lg-5 col-sm-6">
                        <div class="nextadform">
                            <h5>Next Ad Slot</h5>
                            <em class="no-ad-msg hide">Sorry! No Ad slots available for the next 4 weeks. Please try
                                again later.</em>
                            <div class="row">
                                <div class="col-xs-6 ">
                                    <small>Available on</small>
                                    <span class="expire-date">{{\Carbon\Carbon::now()->format('F j, Y')}}</span>
                                </div>
                                <div class="col-xs-6 ">
                                    <small>Expires on</small>
                                    <span class="expire-date">{{\Carbon\Carbon::now()->addDays(7)->format('F j, Y')}}</span>
                                </div>
                            </div>
                            <div class="adinfo-2">
                                <label class="pintotop-lable">Ad Location</label><br>
                                Kandy > Kadugannawa
                            </div>

                            <div class="adinfo-2">
                                <label class="pintotop-lable">Ad Category</label><br>
                                {{$info->category->category_name}}
                                > {{$subCategory->sub_category_name}}</div>
                            <form action="/myprofile/pin-to-top" method="post" name="pinToTopFrm" id="pinToTopFrm"
                                  class="has-validation-callback">
                                {{csrf_field()}}


                                <input type="hidden" name="adid" value="{{$info->id}}">
                                <input type="hidden" name="member_id" value="{{$info->member_id}}">
                                <input type="hidden" name="boosttype" value="{{$boosttype}}">
                                <input type="hidden" name="price" id="price" value="{{$adPacks->price}}">
                                <input type="hidden" name="validitymonths" value="{{$adPacks->validitymonths}}">
                                <input type="hidden" name="ad_referance" value="{{$info->ad_referance}}">
                                @if(!empty($pinStatus))
                                    <span>Already Pinned</span>
                                @else
                                    {{--boost vouchers--}}
                                    @if(!empty($voucherCodes))
                                        <div class="adinfo-2">
                                            <label class="pintotop-lable">Select Voucher</label>
                                            <select name="vouchercode" id="vouchercode"
                                                    class="show-tick form-control selectpicker">
                                                @foreach($voucherCodes as $voucher)
                                                    <option value="{{$voucher->id}}">{{(!empty($voucher->vouchercode))?$voucher->boosttype.' '.$voucher->id :''}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <input name="Submit" type="submit" onclick="createGa();setpaymentpage();"
                                               class="btn btn-warning btn-lg" value="Pay Now">
                                    @else
                                        @if(!count($checkPendinBoostForReleventAd))
                                            <p>Amount payable: <span

                                                        id="totalPrice"> LKR {{number_format($adPacks->price)}}</span>
                                            </p>
                                            <a href="" target="_blank" data-toggle="modal"
                                               data-target="#myModal"><img src="https://www.payhere.lk/downloads/images/payhere_long_banner.png" alt="PayHere" width="340" /></a>
                                            <div class="paymentoption">

                                                {{--<a href="" class="" id="visa" data-toggle="modal"--}}
                                                   {{--data-target="#myModal"><i class=" visa fa fa-cc-visa "></i></a>--}}
                                                {{--<a href="" data-toggle="modal"--}}
                                                   {{--data-target="#myModal"><i class="fa fa-cc-mastercard master"--}}
                                                    {{--></i></a>--}}
                                                {{--<a href="" data-toggle="modal"--}}
                                                   {{--data-target="#myModal"> <i class="fa fa-cc-amex amari" aria-hidden="true"--}}
                                                    {{--></i></a>--}}
                                                <input name="Submit" type="submit"
                                                       onclick="createGa();setpaymentpage();"
                                                       class="btn btn-warning btn-lg" value="Pay on Cash">
                                            </div>

                                                        id="totalPrice">{{number_format($adPacks->price)}}</span></p>
                                            <input name="Submit" type="submit" onclick="createGa();setpaymentpage();"
                                                   class="btn btn-warning btn-lg" value="Pay Now">

                                        @else
                                            <span>Pending Request</span>
                                        @endif
                                    @endif
                                @endif
                            </form>
                            <form>

                            </form>
                            <span style="color:red;" id="errormsg"></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="top-mar-50"></div>
        </div>
    </div>
    <script src="http://saleme.me/js/location/district_city.js"></script>
    <script type="text/javascript">
        $("#adpack").change(function () {
            var option = $('option:selected', this).attr('packprice');
            if (typeof option === "undefined") {
                $('#price').val('');
                $('#totalPrice').html('');
            } else {
                $('#price').val(option);
                $('#totalPrice').html('LKR ' + option);
            }
        });

$('#proceedBtn').click(function (e) {
    e.preventDefault();
    var data = {
        order_id:document.getElementsByName("order_id")[0].value,
        items:document.getElementsByName("items")[0].value,
        currency:document.getElementsByName("currency")[0].value,
        first_name:document.getElementsByName("first_name")[0].value,
        last_name:document.getElementsByName("last_name")[0].value,
        email:document.getElementsByName("email")[0].value,
        address:document.getElementsByName("address")[0].value,
        city:document.getElementsByName("city")[0].value,
        country:document.getElementsByName("country")[0].value,
    };
    $.ajax({
        type: "POST",
        cache: false,
        url: "/member/payment_detals",
        data: data,
        success: function (res) {
            if(res.success){
                $('#visapaymentform').submit();
            }else{
                $('#errormsg').show().html('jdfdbhfb');
            }
        }
    });
})
    </script>
@endsection
<!--verify phone popup-->
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Payment Method Option</h4>
            </div>
            {{--{{dd($info)}}--}}
            <div class="modal-body">
                <p>Are you sure you want to proceed card payment</p>
                <form action="https://sandbox.payhere.lk/pay/checkout" method="post" id="visapaymentform">
                {{--<form action="" method="post" onsubmit="return Validate(this);" >--}}
                    {{csrf_field()}}
                    <input type="hidden" name="merchant_id" value="1211009">
                    <input type="hidden" name="return_url" value="http://158.69.250.36/member/payment_detals">
                    <input type="hidden" name="cancel_url" value="http://saleme.ca/member/paymentsuccess">
                    <input type="hidden" name="notify_url" value="http://158.69.250.36/member/payment_detals">
                    {{--<br><br>Item Details<br>--}}
                    <input type="hidden" name="order_id" value={{$info->ad_referance}}>
                    <input type="hidden" name="items" value="Pin to Top payment ">
                    <input type="hidden" name="currency" value="LKR">
                    <input type="hidden" name="amount" value="1000">
                    {{--<br><br>Customer Details<br>--}}
                    <input type="hidden" name="first_name" value="{{$info->member->first_name}}">
                    <input type="hidden" name="last_name" value="{{$info->member->last_name}}">
                    <input type="hidden" name="email" value="{{$info->member->email}}">
                    <input type="hidden" name="phone" value="{{$member_contacts->contactnumber}}">
                    <input type="hidden" name="address" value="{{$info->city->city_name}}">
                    <input type="hidden" name="city" value="{{$info->city->city_name}}">
                    <input type="hidden" name="country" value="Sri Lanka">
                    <div style="float: right">
                    <input type="submit" value="Proceed" class="btn btn-success" id="proceedBtn">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>


<!--select category popup-->
<div class="modal fade pintotop-modal select-cat-modal" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="ion-ios-close-empty"></i></button>
                <h4 class="modal-title text-center"><span class="lnr lnr-pushpin"></span> &nbsp;
                    Pin To Top </h4>
            </div>
            <div class="mar-80">
                <h4>Pin To Top Ads</h4>
                <div class="top-mar-10"></div>
                <p>
                    Pin to top method of displaying ads is the most suitable and value addition system of publishing an
                    advertisement on saleMe.lk. There are two spots reserved on top of the ads listing page which can be
                    used to promote your ad and to view more and more times rather than the other usual display.
                    used to promote your ad and to view more and more times rather than the other usual display.

                </p>
                <h4>Benifits</h4>
                <div class="top-mar-10"></div>
                <p>
                    By using pin to top method you will get chances of more than 10 times of displaying your add. The
                    pin to top spots also bigger than regular ads and highlighted in yellow and clearly mark as top ad
                    batch
                </p>
                <h4>Durations</h4>
                <div class="top-mar-10"></div>
                <p>

                    Pin to top ad will display continuously <b>7 days</b> as a pin to top ad
                </p>
                <h4>Charge</h4>
                <div class="top-mar-10"></div>
                <p>

                    To display as a pin to , a sum of <b>Rs. 1000/- ( One Thousand Only)</b> will be charged per ad per
                    time.
                </p>
                <div class="top-mar-20"></div>
            </div>
        </div>
    </div>
</div>
<!--end select category popup-->