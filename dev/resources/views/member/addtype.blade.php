@extends('layouts.saleme-shortfooter')
@section('title', 'Select Ad Type | Saleme.lk')
@section('content')
    <style>
        .post-job{
            float: none;
            margin-left: auto;
            margin-right: auto;
            margin-top: 20px;
            background: #eee;
            padding: 20px;
            font-size: 20px;
            color: #111;
            border: 1px solid #ccc;
        }
        .post-job:hover{
            background: #010002;
            color: #fff;
        }
        .post-job i{
            color: #fea502;
        }
       .or{
           font-size: 20px;
           text-align: center;
           margin: 15px 0px 0px;
       }
    </style>
    <div class="container cont-bg min-height add-type-page">
        <section>
            <div class="row">
                <div class="mar-90">
                    <div class="text-center">
                        <div class="top-mar-50 top-mar-50-xs"></div>
                        <h2 class="review-headings">Welcome
                            <strong>{{(checkCompany())?(temporyPrivateLogin())?$membername:getMember()->premiummember->company_name:$membername}}</strong>
                            Let's post an ad. <br>Choose any option
                            below </h2>
                        <div class="top-mar-50 top-mar-50-xs"></div>

                        {{--<h2 class="review-headings">--}}
                            {{--Ad Posting is temporary unavailable. We will back Soon--}}
                        {{--</h2>--}}
                        <ul class="adstype adtype-main">
                            <a href="selectcat/?type=sell">
                                <li class="col-md-3 col-xs-6">
                                    <input id="want_sell_vehicle" name="vehic e-ad-type" checked="checked"
                                           class="i-want" type="radio"
                                           onClick="goToURL('parent','selectcat/?type=sell');return document.returnValue">
                                    <label for="want_sell"><i class="ion-ios-checkmark-outline active-icon"></i>I want
                                        to sell</label>
                                </li>
                            </a>
                            <a href="selectcat/?type=buy">
                                <li class="col-md-3 col-xs-6">
                                    <input id="want_buy_vehicle" name="vehicle-ad-type" class="i-want" type="radio"
                                           onClick="goToURL('parent','selectcat/?type=buy');return document.returnValue">
                                    <label for="want_buy"><i class="ion-ios-checkmark-outline active-icon"></i>I want to
                                        buy</label>
                                </li>
                            </a>
                            <a href="selectcat/?type=rent">
                                <li class="col-md-3 col-xs-6">
                                    <input id="want_rent_vehicle" name="vehicle-ad-type" class="i-Offer" type="radio"
                                           onClick="goToURL('parent','selectcat/?type=rent');return document.returnValue">
                                    <label for="want_rent"><i class="ion-ios-checkmark-outline active-icon"></i>I Offer
                                        for rent</label>
                                </li>
                            </a>

                            <a href="selectcat/?type=lookforrent">
                                <li class="col-md-3 col-xs-6">
                                    <input id="look_rent" name="vehicle-ad-type" class="i-Offer" type="radio"
                                           onClick="goToURL('parent','selectcat/?type=lookforrent');return document.returnValue">
                                    <label for="look_rent"><i class="ion-ios-checkmark-outline active-icon"></i>I Look
                                        for rent</label>
                                </li>
                            </a>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 margin-0">
                   <p class="or">Or</p>
                </div>
                <div class="clear-fix"></div>
                <div class="mar-90">

                    <div class="text-center">

                        {{--<a href="/post-job/jobs-in-sl/new-job?type=jobs">--}}
                        <a href="/post-job/jobs-in-sl/new-job?type=jobs">
                        <a href="/selectcat?type=jobs#select_job">
                            <div class="col-md-4 col-xs-12 post-job">
                                <i class="ion-briefcase"></i> &nbsp;Post a Job
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </section>
        <div class="top-mar-50 top-mar-50-xs"></div>
        <div class="text-center">
        </div>
        <div class="top-mar-50 top-mar-50-xs"></div>
    </div>
    <div class="container  add-type-page">
        <div class="top-mar-50 top-mar-50-xs"></div>
        <div class="">
            <h3 class="panel-head text-center">Ads posted on Saleme.lk you are agreed with our rules:</h3><br>
            <div class="col-md-6 col-sm-6 hints-top pd-r-5-sm">
                <div class="boxed-area bg-green">
                    <div class="box-title  bg-green">
                        <span class="lnr lnr-smile lnr-smile-style "></span>
                    </div>
                    <p class="pannel-para"><i class="ion-ios-checkmark-outline"></i> Post Your Ad in the Correct
                        Category is essential.</p>
                    <p class="pannel-para"><i class="ion-ios-checkmark-outline"></i> Make Sure the used Languages and
                        Symbols are correct.
                    </p>
                    <p class="pannel-para"><i class="ion-ios-checkmark-outline"></i> You must enter a valid mobile
                        number.</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 top-mar-50-xs pd-l-5-sm">
                <div class="boxed-area bg-red">
                    <div class="box-title bg-red">
                        <span class="lnr lnr-sad lnr-sad-style"></span>
                    </div>
                    <p class="pannel-para"><i class="ion-ios-close-outline"></i> No pictures with water marks are
                        allowed.
                    </p>
                    <p class="pannel-para"><i class="ion-ios-close-outline"></i> The ads containing multiple items will
                        be rejected.</p>
                    <p class="pannel-para"><i class="ion-ios-close-outline"></i> You must enter your phone number only
                        on given field.</p>
                </div>
            </div>
        </div>
        <div class="top-mar-50 top-mar-50-xs clear-fix"></div>
        <br class="visible-sm visible-lg">
        <br class="visible-sm visible-lg">
        <br class="visible-lg">
    </div>
    <script language="JavaScript">
        <!--
        function goToURL() { //v3.0
            var i, args = goToURL.arguments;
            document.returnValue = false;
            for (i = 0; i < (args.length - 1); i += 2) eval(args[i] + ".location='" + args[i + 1] + "'");
        }
        //-->
    </script>
    <!--category icons-->
@endsection