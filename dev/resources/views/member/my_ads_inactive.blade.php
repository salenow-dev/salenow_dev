@include('main.header')
{{ Html::style('css/salenow/layout/my-account-custom-css.css') }}
<section class="hidden-xs">
    <div class="container ">
        <div class="row">
            <div class="col-md-12">
                <p class="breadcrumb-text">
                    <i class="ion-ios-home-outline"></i> / My Account
                </p>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container ">
        <div class="row search-results no-margin-top-mobile">
            {{ Html::style('css/salenow/layout/my-account-custom-css.css') }}
            {{--mobile--}}
            <div class="col-lg-12 col-xs-12">
                <div class="cont-bg user-adz-div ">
                    <br><br>
                    <div class="col-lg-12 top-mar-20">
                        <div class="text-center">
                            <h2 class="">Your account is currently inactive! </h2>
                            <br>
                            <h5 class="promo-head2">Please Activate the account using Activation email we sent you. Thank you.</h5>
                        </div>
                    </div>
                    <div class="clear-fix"></div>
                </div>
            </div>
            <!--category icons-->
        </div>
    </div>
</section>
<!--verify phone popup-->

<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/noty/jquery.noty.js")}}'></script>
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/noty/layouts/topCenter.js")}}'></script>
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/noty/layouts/topLeft.js")}}'></script>
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/noty/layouts/topRight.js")}}'></script>
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/noty/themes/default.js")}}'></script>

<script type="text/javascript" src="{{ asset("js/member/ad-delete.js") }}"></script>
<script type="text/javascript" src="{{ asset("js/memeber/number-verify.js") }}"></script>
<script>
    function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".more-less")
            .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
<script>
    jQuery(document).ready(function () {
        $('.edit-btn').click(function (event) {
            $('.edit-section').slideToggle('show');
            $('.user-details-section').slideToggle('hide');
        });
        $('.edit-close-btn').click(function (event) {
            $('.edit-section').slideToggle('hide');
            $('.user-details-section').slideToggle('show');
        });
    });
</script>
@include('main.footer')