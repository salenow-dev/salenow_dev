@extends('layouts.member.member_layout')
@section('title', 'My Ads | Saleme.lk')
@section('content')
    {{ Html::style('css/salenow/layout/my-account-custom-css.css') }}
    {{--mobile--}}
    <div class="row login_box visible-xs">
        <div class="col-md-12 col-xs-12" align="center">
            <div class="line">
                <h3 class="user-name-xs">{{!empty($memberdetails->first_name) ? ucfirst($memberdetails->first_name) : '' }}   {{!empty($memberdetails->last_name) ? ucfirst($memberdetails->last_name) : '' }}</h3>
                <hr>
            </div>
            <div class="outter">
                @if(!empty($memberdetails->avatar))
                    <img src="{{$memberdetails->avatar}}" class="img-circle">
                @else
                    <div class="sellerName ">
                        <span class="avatar">{{ucfirst (substr($memberdetails->first_name, 0, 1))}}</span>
                    </div>
                @endif
            </div>
            <p class="m-0 white-font-xs">&nbsp;&nbsp;{{!empty($memberdetails->email) ? $memberdetails->email : '' }}</p>
            @foreach($member->membercontacts as $contacts)
                <p class="m-0 white-font-xs">&nbsp;&nbsp;{{$contacts->contactnumber}}</p>
            @endforeach
            @if(!empty($memberdetails->district_name) && !empty($memberdetails->cityname))
                <p class="m-0 white-font-xs">&nbsp;&nbsp;
                    {{!empty($memberdetails->district_name) ? $memberdetails->district_name : '' }}
                    {{!empty($memberdetails->cityname) ? ' - '.$memberdetails->cityname : '' }}</p>
            @endif
        </div>
        <div class="col-md-6 col-xs-4 follow line {{(Request::segment(1))==='myprofile' ? 'active-item-mobile' : ''}}"
             align="center">
            <a href="/myprofile" class="">My Account</a>
        </div>
        <div class="col-md-6 col-xs-4 follow line {{(Request::segment(1))==='my_ads' ? 'active-item-mobile' : ''}}"
             align="center">
            {{--<a href="/my_ads" class=""><span></span></a>--}}
            <div class="dropdown">
                <span class="dropdown-toggle" data-toggle="dropdown">My Ads<span class="caret"></span></span>
                <ul class="dropdown-menu">
                    <li><a href="{{asset('')}}my_ads">All Ads</a></li>
                    <li class="divider"></li>
                    <li><a href="{{asset('')}}my_ads?state=confirm">Published Ads</a></li>
                    <li><a href="{{asset('')}}my_ads?state=pending">Pending Ads</a></li>
                    <li><a href="{{asset('')}}my_ads?state=cancel">Canceled Ads</a></li>

                </ul>
            </div>
        </div>
        {{--<div class="col-md-6 col-xs-4 follow line" align="center">--}}
        {{--<a href="#" class=" ">Payments</a>--}}
        {{--</div>--}}
    </div>
    <div class="">
        <section class="profile-content">
            <div class="col-lg-12 col-xs-12 pd-0-sm">
                <div class="panel">
                    @if(count($allads)==0)
                        <br><br>
                        <div class="col-lg-12 top-mar-20">
                            <div class="text-center">
                                <h4 class="promo-head2">You have not Favorite ads yet</h4>
                                {{--<h4 class="promo-head2">Click "<strong>Post Free Ad</strong>" button to post your ad--}}
                                {{--</h4>--}}
                                <br>
                                <a href="addtype">
                                    {{ Html::image('images/salenow/place-Ad-01-01.png', 'saleme.lk logo', array('class' => 'img-responsive center-block plce-ad')) }}
                                </a>
                            </div>
                        </div>
                        <div class="clear-fix"></div>
                    @else
                        <h2 class="my-ad-h">&nbsp;
                            @if(count($allads)!=0)
                                <span class="my-ad-ico ion-ios-heart"></span> Wish List
                            @endif
                        </h2>
                        <hr>
                        <div class="single-ad">
                            <!--My add single item-->
                            @if(count($allads)==0)
                                <div class="media">
                                    <div class="col-md-12 no-padding ">
                                        No Favorite ads
                                    </div>
                                </div>
                            @else
                                @foreach($allads as $datas)
                                    @if(!is_null($datas->adtitle))
                                        <a href="{{asset('').'ad/'.$datas->slug}}"
                                           id="{{$datas->adid}}" class="">

                                            <div class="media">
                                                <div class="col-md-12 no-padding ">
                                                    {{-- START Featured Image--}}
                                                    <div class="col-md-4 col-xs-6 pull-left">

                                                        @if($datas->featuredimage!='')
                                                            <img class="media-object img-responsive"
                                                                 style="margin: 0 auto"
                                                                 alt="64x64"
                                                                 src="{{asset('')}}salenow/images/uploads/{{$datas->adid}}/thumb/{{$datas->featuredimage}}">
                                                        @else
                                                            <img class="img-responsive" style="margin: 0 auto"
                                                                 src="{{asset('')}}images/salenow/no_image.jpg">
                                                        @endif
                                                    </div>
                                                    {{-- END Featured Image--}}

                                                    {{--details body--}}
                                                    <div class="media-body">
                                                        {{--desktop--}}
                                                        <div class="desktop hidden-xs hidden-sm">
                                                            <h4 class="media-heading">
                                                                {{$datas->adtitle}}
                                                            </h4>
                                                            <p class="item-loc-cat">
                                                                <span class="item-price">Rs {{$datas->price > 0 ? number_format($datas->price) : '' }}</span>
                                                            </p>
                                                            <p class="item-loc-cat">
                                                                <i class="ion-location"></i> <span class="hidden-xs">{{$datas->district_name}}
                                                                    ,</span>
                                                                {{$datas->city}} &nbsp; | &nbsp;
                                                                <i class="ion-ios-pricetag"></i> {{$datas->category}}
                                                            </p>
                                                            <p class="item-loc-cat">
                                                                <span class="item-added-date">Posted on:</span> {{ date('F d, Y', strtotime($datas->created_at))}}
                                                            </p>
                                                            @if(getMemberId())
                                                                <a href="javascript:void(0);" ad-id="{{$datas->adid}}"
                                                                   class="fav ">
                                                                    <i class="ion-ios-trash-outline"></i>&nbsp;Remove
                                                                    from
                                                                    Wish List

                                                                </a>
                                                            @endif
                                                        </div>
                                                        {{--mobile--}}
                                                        <div class="mobile visible-xs visible-sm">
                                                            <p class="item-title item-m">{{$datas->adtitle}}</p>
                                                            <p class="item-loc-cat">{{$datas->subcategory}}
                                                                ,&nbsp;{{$datas->city}}</p>
                                                            @if(!empty($datas->mileage))
                                                                <p class="vehicle-mileage">{{$datas->mileage > 0 ? number_format($datas->mileage) : '0'}}
                                                                    &nbsp;KM</p>@endif
                                                            <p class="item-price">{{$datas->price > 0 ? 'Rs '.number_format($datas->price) : ''}}</p>
                                                            @if(getMemberId())
                                                                <a href="javascript:void(0);" ad-id="{{$datas->adid}}"
                                                                   class="fav ">
                                                                    <i class="ion-ios-trash-outline"></i>&nbsp;Remove
                                                                    from
                                                                    Wish List

                                                                </a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                {{--single add end--}}
                                            </div>
                                            <hr>
                                        </a>

                                    @endif
                                @endforeach
                            @endif
                        </div>
                        <div class="row">
                            {{$allads->appends(Illuminate\Support\Facades\Input::except('page'))->links('vendor.pagination.custom')}}
                        </div>
                    @endif
                </div>
            </div>
        </section>
    </div>

    <script>
        $(document).ready(function () {
            $('.fav').click(function (e) {
                e.preventDefault();
                var element = $(this);
                var id = $(this).attr('ad-id');
                var data = {'id': id};

                $.ajax({
                    type: "post",
                    url: '/myads/favorite/' + id,
                    data: data,
                    success: function (res) {
                        if (res.response.message == 'delete_success') {
                            $(element).closest('.media').hide();
                            noty({
                                text: 'Removed from Wish list successfully',
                                layout: 'topRight',
                            })
                        }
                    }
                });
            })
        });
    </script>
@endsection

{{--<script type="text/javascript" src="{{ asset("js/member/ad-delete.js") }}"></script>--}}
<style>
    p {
        margin: 0 0 5px !important;
    }

    .stylish-input-group .input-group-addon {
        background: white !important;
    }

    .stylish-input-group .form-control {
        border-right: 0;
        box-shadow: 0 0 0;
        border-color: #ccc;
    }

    .stylish-input-group button {
        border: 0;
        background: transparent;
    }

    /*.media, .media-body {*/
    /*overflow: inherit !important;*/
    /*zoom: 1;*/
    /*}*/

    /*css*/
    .deleresons .modal-header {
        background: #821900 !important;
    }

    .top-10 {
        margin-top: 10px !important;
    }

    .label-danger[href]:focus, .label-danger[href]:hover {
        background-color: #ff0600;
        color: #fff !important;
    }

    .delete-c {
        background: #821900 !important;
        color: #fff !important;

    }

    .my-ad-devide {
        margin-top: 16px !important;
        margin-bottom: 16px !important;
    }

    .single-ad:hover {
        box-shadow: 0 0 9px 3px #eee;

    }

    .single-ad {
        padding-top: 8px;

    }

    .promo-tx {
        font-weight: 600;
    }

    .promo-btn {
        margin-right: 4px;
        color: #fff;
        font-weight: 600;
        background-color: #fea502;
        border-radius: 2px;
        padding: 3px 9px;
        margin-bottom: 14px !important;
    }

    .promo-btn-m {
        color: #fff;
        font-weight: 600;
        background-color: #fea502;
        border-radius: 2px;
        padding: 3px 9px;
    }

    .spam-reason {
        background-color: #fddbdb;
        color: #630000;
        padding: 3px 8px;
        font-size: 11px;
        border-radius: 4px;
    }

    .pinned-status {
        padding-left: 7px;
        width: 67px;
        opacity: 0.9;
        color: #ffffff;
    }

    /*mobile viewmy ads*/
    .item-m {
        margin-bottom: 4px !important;
        margin-top: 0px;
        font-size: 16px;
        font-weight: 500;
    }

    .m-promo-box {
        border-top: 1px solid #fea502;
        border-bottom: 1px solid #dfdfdf;
        margin-top: 8px;
        margin-bottom: 16px;
        padding-top: 6px !important;
        padding-bottom: 10px !important;
        font-size: 13px;
        background: #ffe5b4;
    }

    .item-price {
        font-size: 13px;
        font-weight: 500;
        color: #fea502;

    }

    /*mobile views end my ads*/


</style>