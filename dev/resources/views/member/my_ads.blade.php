@extends('layouts.member.member_layout')
@section('title', 'My Ads | Saleme.lk')
@section('content')
    {{ Html::style('css/salenow/layout/my-account-custom-css.css') }}
    {{--mobile--}}
    <div class="row login_box visible-xs">
        <div class="col-md-12 col-xs-12" align="center">
            <div class="line">
                <h3 class="user-name-xs">{{!empty($memberdetails->first_name) ? ucfirst($memberdetails->first_name) : '' }}   {{!empty($memberdetails->last_name) ? ucfirst($memberdetails->last_name) : '' }}</h3>
                <hr>
            </div>
            <div class="outter">
                @if(!empty($memberdetails->avatar))
                    <img src="{{$memberdetails->avatar}}" class="img-circle">
                @else
                    <div class="sellerName ">
                        <span class="avatar">{{ucfirst (substr($memberdetails->first_name, 0, 1))}}</span>
                    </div>
                @endif
            </div>
            <p class="m-0 white-font-xs">&nbsp;&nbsp;{{!empty($memberdetails->email) ? $memberdetails->email : '' }}</p>
            @foreach($member->membercontacts as $contacts)
                <p class="m-0 white-font-xs">&nbsp;&nbsp;{{$contacts->contactnumber}}</p>
            @endforeach
            @if(!empty($memberdetails->district_name) && !empty($memberdetails->cityname))
                <p class="m-0 white-font-xs">&nbsp;&nbsp;
                    {{!empty($memberdetails->district_name) ? $memberdetails->district_name : '' }}
                    {{!empty($memberdetails->cityname) ? ' - '.$memberdetails->cityname : '' }}</p>
            @endif
        </div>
        <div class="col-md-6 col-xs-4 follow line {{(Request::segment(1))==='myprofile' ? 'active-item-mobile' : ''}}"
             align="center">
            <a href="/myprofile" class="">My Account</a>
        </div>
        <div class="col-md-6 col-xs-4 follow line {{(Request::segment(1))==='my_ads' ? 'active-item-mobile' : ''}}"
             align="center">
            {{--<a href="/my_ads" class=""><span></span></a>--}}
            <div class="dropdown">
                <span class="dropdown-toggle" data-toggle="dropdown">My Ads<span class="caret"></span></span>
                <ul class="dropdown-menu">
                    <li><a href="{{asset('')}}my_ads">All Ads</a></li>
                    <li class="divider"></li>
                    <li><a href="{{asset('')}}my_ads?state=confirm">Published Ads</a></li>
                    <li><a href="{{asset('')}}my_ads?state=pending">Pending Ads</a></li>
                    <li><a href="{{asset('')}}my_ads?state=cancel">Canceled Ads</a></li>

                </ul>
            </div>
        </div>
        <div class="col-md-6 col-xs-4 follow line" align="center">
            <a href="#" class=" ">Payments</a>
        </div>
    </div>
    <div class="">
        <div class="col-md-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Search Ads</h3>
                </div>
                <div class="panel-body">
                    <p>Search any advertisement by its Title or Category, you posted on SaleMe.lk .</p>
                    <div class="col-md-6">
                        <form id="searchAgent" class="form-inline" role="form" method="get" action="/myads/search">
                            <div class="form-group ">
                                <div class="input-group stylish-input-group">
                                    <input type="text" name="ad_info" value="{{ old('ad_info') }}" class="form-control"
                                           placeholder="Ad Title, Ref No, Location or Date">
                                    <span class="input-group-addon ">
                                    <button type="submit">
                                    <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                    </span>
                                </div>
                            </div>
                            <div class="clear-fix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="">
        <section class="profile-content">
            <div class="col-lg-12 col-xs-12 pd-0-sm">
                <div class="panel">
                    @if(count($allads)==0)
                        @if(empty($state))
                            <br><br>
                            <div class="col-lg-12 top-mar-20">
                                <div class="text-center">
                                    <h4 class="promo-head2">You have not posted any Ads yet</h4>
                                    <h4 class="promo-head2">Click "<strong>Post Free Ad</strong>" button to post your ad
                                    </h4>
                                    <br>
                                    <a href="addtype">
                                        {{ Html::image('images/salenow/place-Ad-01-01.png', 'saleme.lk logo', array('class' => 'img-responsive center-block plce-ad')) }}
                                    </a>
                                </div>
                            </div>
                            <div class="clear-fix"></div>
                        @else
                            <br><br>
                            <div class="col-lg-12 top-mar-20">
                                <div class="text-center">
                                    <h4 class="promo-head2">You have not {{ucwords($state)}} Ads.</h4>
                                    <a href="/my_ads" class="goto-link">Go to my all ads</a>
                                    <h4 class="display-block">Or</h4>
                                    <h4 class="promo-head2">Click "<strong>Post Free Ad</strong>" button to post your ad
                                    </h4>
                                    <br>
                                    <a href="addtype">
                                        {{ Html::image('images/salenow/place-Ad-01-01.png', 'saleme.lk logo', array('class' => 'img-responsive center-block plce-ad')) }}
                                    </a>
                                </div>
                            </div>
                            <div class="clear-fix"></div>
                        @endif
                    @else
                        <h2 class="my-ad-h">&nbsp;
                            @if(count($allads)!=0)
                                <span class="lnr my-ad-ico lnr-rocket"></span>
                                {{!(empty($state))? ucwords($state) : 'All'}} Ads
                            @endif
                        </h2>
                        <hr>
                        <div class="single-ad">
                            <!--My add single item-->
                            @foreach($allads as $datas)
                                <a href="{{($datas->status =='confirm')? 'ad/'.$datas->slug: '/ad/edit/'.$datas->slug}}"
                                   id="{{$datas->adid}}" class="">

                                    <div class="media">
                                        <div class="col-md-12 col-xs-12 visible-xs">
                                            @if($datas->status == 'draft')
                                                <div class="col-md-12 col-xs-12 label label-danger "
                                                     style="font-weight: 300 !important;white-space: normal !important;">
                                                    Add Phone Numbers & Complete.
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-md-12 no-padding ">
                                            {{-- START Featured Image--}}
                                            <div class="col-md-4 col-xs-6 pull-left">
                                                <div class="label label-special pinned-status
                                                    @if($datas->status === 'pending')
                                                {{'label-warning'}}
                                                @elseif($datas->status === 'confirm')
                                                {{'label-success'}}
                                                @elseif($datas->status === 'cancel' || $datas->status === 'draft')
                                                {{'label-danger'}}
                                                @endif">{{($datas->status=='confirm')? 'Published' : ucwords($datas->status)}}
                                                </div>
                                                @if($datas->featuredimage!='')
                                                    <img class="media-object img-responsive" style="margin: 0 auto"
                                                         alt="64x64"
                                                         src="{{asset('')}}salenow/images/uploads/{{$datas->adid}}/thumb/{{$datas->featuredimage}}">
                                                @else
                                                    <img class="img-responsive" style="margin: 0 auto"
                                                         src="{{asset('')}}images/salenow/no_image.jpg">
                                                @endif
                                            </div>
                                            {{-- END Featured Image--}}

                                            {{--details body--}}
                                            <div class="media-body">
                                                {{--desktop--}}
                                                <div class="desktop hidden-xs hidden-sm">
                                                    <h4 class="media-heading">
                                                        {{$datas->adtitle}}
                                                    </h4>
                                                    <p class="item-loc-cat">
                                                        <span class="item-price">Rs {{$datas->price > 0 ? number_format($datas->price) : '' }}</span>
                                                    </p>
                                                    <p class="item-loc-cat">
                                                        <i class="ion-location"></i> <span class="hidden-xs">{{$datas->district_name}}
                                                            ,</span>
                                                        {{$datas->city}} &nbsp; | &nbsp;
                                                        <i class="ion-ios-pricetag"></i> {{$datas->category}}</p>
                                                    <p class="item-loc-cat">
                                                        <span class="item-added-date">Posted on:</span> {{ date('F d, Y', strtotime($datas->created_at))}}
                                                        &nbsp;
                                                        @if($datas->status === 'confirm')
                                                            {{--                                                    {!!' | &nbsp;<span class="item-added-date"> Active till:</span> '.date('F d, Y', strtotime($datas->updated_at)).'</p>'!!}--}}
                                                        @elseif($datas->status === 'cancel')
                                                            {!!' | &nbsp;<span class="item-added-date"> Canceled On:</span> '.date('F d, Y', strtotime($datas->updated_at))!!}
                                                        @endif
                                                    </p>

                                                    {{--START desktop edit & delete buttons--}}
                                                    <a class="hidden-xs" href="/ad/edit/{{$datas->slug}}">
                                                        <button class="btn  btn-default btn-sm "><i
                                                                    class="lnr lnr-pencil"></i> Edit
                                                        </button>

                                                    </a>
                                                    <a ad-id="{{$datas->adid}}" id="delete_{{$datas->adid}}"
                                                       status="delete" class="hidden-xs delete-record"
                                                       onClick="notyConfirm({{$datas->adid}})">
                                                        <button class="btn  btn-default btn-sm"><i
                                                                    class="lnr lnr-trash"></i> Delete
                                                        </button>
                                                    </a>
                                                    {{--END desktop edit & delete buttons--}}

                                                    {{--START Spam Reasons--}}
                                                    @if(!empty($datas->spams))
                                                        <p class="top-10"><a data-toggle="collapse"
                                                                             href="#collapseExample_{{$datas->adid}}"
                                                                             class="label status-lable label-danger">Spam
                                                                Reasons&nbsp;<i
                                                                        class="fa fa-hand-o-down"></i>&nbsp;</a>
                                                        </p>
                                                        <div class="collapse" id="collapseExample_{{$datas->adid}}">
                                                            <ul class="list-tags">
                                                                @foreach($datas->spams as $spam)
                                                                    <li><a href="#" class="spam-reason"><i
                                                                                    class="fa fa-caret-right"></i>&nbsp;<span>{{$spam->reason}}</span>
                                                                        </a></li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    @endif
                                                    {{--END Spam Reasons--}}

                                                    {{--START Draft ad notification--}}
                                                    @if($datas->status == 'draft')
                                                        <p class=" hidden-xs my-ad-devide">
                                                        <span class="label label-danger"
                                                              style="font-weight: 300 !important;white-space: normal !important;">
                                                            This ad not yet Published. Add Phone Numbers & Post
                                                        </span>
                                                        </p>
                                                    @endif
                                                    <div class="clear-fix"></div>
                                                    {{--END Draft ad notification--}}

                                                    {{--START promot ad--}}
                                                    @if($datas->status == 'confirm')
                                                        <hr class="my-ad-devide">
                                                        <div class=" col-md-9 promo-tx">
                                                            <i class="ion-flash"></i> Reach up to 10x more people by
                                                            promoting your ad.
                                                        </div>
                                                        <div class="col-md-3">
                                                            <a type="button"
                                                               href="/myprofile/{{$datas->slug}}/pin-to-top"
                                                               class="btn promo-btn btn-md">Promote Ad
                                                            </a>
                                                        </div>
                                                    @endif
                                                    {{--END promot ad--}}
                                                </div>
                                                {{--mobile--}}
                                                <div class="mobile visible-xs visible-sm">
                                                    <p class="item-title item-m">{{$datas->adtitle}}</p>
                                                    <p class="item-loc-cat">{{$datas->subcategory}}
                                                        ,&nbsp;{{$datas->city}}</p>
                                                    @if(!empty($datas->mileage))
                                                        <p class="vehicle-mileage">{{$datas->mileage > 0 ? number_format($datas->mileage) : '0'}}
                                                            &nbsp;KM</p>@endif
                                                    <p class="item-price">{{$datas->price > 0 ? 'Rs '.number_format($datas->price) : ''}}</p>
                                                </div>
                                                <div class=" visible-xs visible-sm  ">
                                                    <div class="col-xs-4 no-padding">
                                                        <a class="" href="/ad/edit/{{$datas->slug}}">
                                                            <button class="btn  btn-default btn-xs"><i
                                                                        class="lnr lnr-pencil"></i> Edit
                                                            </button>
                                                        </a>
                                                    </div>

                                                    <div class="col-xs-6 no-padding pull-left">
                                                        <a ad-id="{{$datas->adid}}" id="delete_{{$datas->adid}}"
                                                           status="delete" class="delete-record"
                                                           onClick="notyConfirm({{$datas->adid}})">
                                                            <button class="btn  btn-default btn-xs"><i
                                                                        class="lnr lnr-trash"></i> Delete
                                                            </button>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--END details body--}}
                                            @if($datas->status == 'confirm')
                                                <div class="col-xs-12 m-promo-box no-padding visible-xs visible-sm">
                                                    <div class="col-xs-7 promo-tx" style="    margin-top: 6px;">
                                                        <i class="ion-flash"></i> Reach up to 10x.
                                                    </div>
                                                    <div class="col-xs-5 pull-right no-padding">
                                                        <a type="button"
                                                           href="/myprofile/{{$datas->slug}}/pin-to-top"
                                                           class="btn  promo-btn-m btn-sm pull-right">Promote Ad
                                                        </a>
                                                    </div>
                                                    <hr>
                                                </div>
                                            @endif
                                        </div>
                                        {{--single add end--}}
                                    </div>
                                </a>
                                <hr>
                            @endforeach
                        </div>
                        <div class="row">
                            {{$allads->appends(Illuminate\Support\Facades\Input::except('page'))->links('vendor.pagination.custom')}}
                        </div>
                    @endif
                </div>
            </div>
        </section>
    </div>
    <!--category icons-->
    <div class="modal fade deleresons" id="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="ion-ios-close-empty"></i>
                    </button>
                    <h4 class="modal-title text-center"><span class="no-1"><i class="ion-ios-trash-outline"></i> </span>
                        &nbsp; Select Delete Reason</h4>
                </div>

                <div class="modal-body">
                    <div class="col-lg-12">
                        <ul>
                            @if(!empty($delete_resons))
                                @foreach($delete_resons as $reson)
                                    <li><label><input value="{{$reson->id}}" name="delete_reson_id"
                                                      type="radio" {{($reson->id ==1)? 'checked' : ''}}>
                                            &nbsp;{{$reson->reason}}
                                        </label></li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                    <div class="clear-fix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn primary " data-dismiss="modal">Cancel</button>
                    <button class="btn  delete-c" id="delete_btn1">Delete Ad</button>
                </div>
                <div class="clear-fix"></div>
            </div>
        </div>
    </div>
@endsection
{{--<script type="text/javascript" src="{{ asset("js/member/ad-delete.js") }}"></script>--}}
<style>
    p {
        margin: 0 0 5px !important;
    }

    .stylish-input-group .input-group-addon {
        background: white !important;
    }

    .stylish-input-group .form-control {
        border-right: 0;
        box-shadow: 0 0 0;
        border-color: #ccc;
    }

    .stylish-input-group button {
        border: 0;
        background: transparent;
    }

    /*.media, .media-body {*/
    /*overflow: inherit !important;*/
    /*zoom: 1;*/
    /*}*/

    /*css*/
    .deleresons .modal-header {
        background: #821900 !important;
    }

    .top-10 {
        margin-top: 10px !important;
    }

    .label-danger[href]:focus, .label-danger[href]:hover {
        background-color: #ff0600;
        color: #fff !important;
    }

    .delete-c {
        background: #821900 !important;
        color: #fff !important;

    }

    .my-ad-devide {
        margin-top: 16px !important;
        margin-bottom: 16px !important;
    }

    .single-ad:hover {
        box-shadow: 0 0 9px 3px #eee;

    }

    .single-ad {
        padding-top: 8px;

    }

    .promo-tx {
        font-weight: 600;
    }

    .promo-btn {
        margin-right: 4px;
        color: #fff;
        font-weight: 600;
        background-color: #fea502;
        border-radius: 2px;
        padding: 3px 9px;
        margin-bottom: 14px !important;
    }

    .promo-btn-m {
        color: #fff;
        font-weight: 600;
        background-color: #fea502;
        border-radius: 2px;
        padding: 3px 9px;
    }

    .spam-reason {
        background-color: #fddbdb;
        color: #630000;
        padding: 3px 8px;
        font-size: 11px;
        border-radius: 4px;
    }

    .pinned-status {
        padding-left: 7px;
        width: 67px;
        opacity: 0.9;
        color: #ffffff;
    }

    /*mobile viewmy ads*/
    .item-m {
        margin-bottom: 4px !important;
        margin-top: 0px;
        font-size: 16px;
        font-weight: 500;
    }

    .m-promo-box {
        border-top: 1px solid #fea502;
        border-bottom: 1px solid #dfdfdf;
        margin-top: 8px;
        margin-bottom: 16px;
        padding-top: 6px !important;
        padding-bottom: 10px !important;
        font-size: 13px;
        background: #ffe5b4;
    }

    .item-price {
        font-size: 13px;
        font-weight: 500;
        color: #fea502;

    }

    /*mobile views end my ads*/


</style>