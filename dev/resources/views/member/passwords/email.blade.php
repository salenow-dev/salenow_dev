@include('main.header')
        <!DOCTYPE html>
<html lang="en" class="body-full-height">
<head>
    <!-- META SECTION -->
    <title>Saleme.lk | Password Reset</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link href="{{ asset("backend/assets/css/theme-default.css") }}" rel="stylesheet">
    <!-- EOF CSS INCLUDE -->
</head>
<body>

<div class="registration-container pin-bg">

    <div class="registration-box animated fadeInDown">
        <div class="registration-body">
            <div class="registration-title"><strong>Forgot</strong> Password?</div>
            <div class="registration-subtitle">We will send a link on your registered email to reset your password.
            </div>
            @if (session('status'))
                <div class="alert alert-success saleme-success">
                    {{ session('status') }}
                </div>
            @endif
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/member/password/email') }}">
                {{ csrf_field() }}
                @if (!session('status'))
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="row">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                                   placeholder="email@domain.com" required>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary">
                                Send Password Reset Link
                            </button>
                        </div>
                    </div>
                @endif
            </form>
        </div>
        <div class="registration-footer">
            <div class="pull-left">
                &copy; {{date('Y')}} {{ config('app.name') }}
            </div>

        </div>
    </div>
</div>
<div class="clear-fix"></div>
@include('default.popup')

</body>
</html>
