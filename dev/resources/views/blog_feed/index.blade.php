<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{ Html::style('css/salenow/layout/bootstrap-3.3.7/css/bootstrap.min.css') }}
    {{ Html::style('plugins/blog_feed_assests/css/simple.min.css') }}
    {{ Html::style('css/salenow/layout/ionicons/css/ionicons.min.css') }}
    {{ Html::script('js/salenow/jquery.min.js') }}
    {{ Html::script('plugins/blog_feed_assests/js/jquery.marquee.js') }}
    {{ Html::style('plugins/blog_feed_assests/css/blog-custom.css') }}

    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i"
          rel="stylesheet">
</head>
<body style="width:306px;overflow: hidden;">

<div class="block-hdnews" style="width:306px; margin:0px auto; background-color:#fff;min-width:306px;">

    <div class="list-wrpaaer" style="height:500px">
        <ul class="list-aggregate" id="marquee-vertical">

            @foreach($adslist as $ad)
                <?php $publishedTime = timeForUser($ad->published_at); ?>
                <li>
                    <a href="{{asset('')}}ad/{{$ad->slug}}" title="{{$ad->adtitle}}" target="_blank">
                        <div class="width-100">
                            <div class="image-div">
                                <img src="{{asset('')}}salenow/images/uploads/{{$ad->id}}/thumb/{{$ad->featuredimage}}"
                                     alt="{{$ad->adtitle}}"
                                     class="img-responsive">
                            </div>
                            <div class="des-div">
                                <h2 class="ad-title">{{$ad->adtitle}} </h2>
                                @if(!empty($ad->mileage))
                                    <p class="prime-features">{{$ad->mileage}} Km</p>
                                @elseif(!empty($ad->land_size))
                                    <p class="prime-features">{{$ad->land_size}}</p>
                                @elseif(!empty($ad->prop_size))
                                    <p class="prime-features">{{$ad->prop_size}}</p>
                                @endif

                                <div class="clear-fix"></div>
                                @if(($ad->price) >0)
                                    <h3 class="item-price">Rs. {{$ad->price}} </h3>
                                @endif
                                <p class="other-details">
                                    <span><i class="ion-ios-location-outline icon"></i>&nbsp; {{$ad->city_name}}</span>
                                    &nbsp;&nbsp;&nbsp;
                                    <span><i class="ion-ios-location-outline icon"></i>&nbsp; {{$ad->category_name}}</span>
                                </p>
                                <p class="other-details">
                                    <i class="ion-ios-stopwatch-outline"></i>&nbsp;{{$publishedTime}}</span>
                                </p>
                                <div class="clear-fix"></div>
                            </div>
                            <div class="clear-fix"></div>
                        </div>
                    </a>
                </li>
            @endforeach

        </ul>
    </div><!-- list-wrpaaer -->

</div> <!-- block-hdnews -->
<script type="text/javascript">
    $(function () {
        $('#marquee-vertical').marquee();
        $('#marquee-horizontal').marquee({direction: 'horizontal', delay: 0, timing: 50});
    });
</script>
</body>
</html>