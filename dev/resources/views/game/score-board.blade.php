<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="text/html; charset=utf-8" http-equiv=Content-Type>{{----}}
    <meta content="{{ csrf_token() }}" name=csrf-token>
    <title>SaleMe.lk කොකිස් Challenge | SaleMe.lk Kokis Challenge </title>
    <meta content="width=device-width,initial-scale=1" name=viewport>
    <meta content="කොකිස් Challenge, kokis Challenge, awurudu game, free t-shirt, free reloads"
          name=keywords>
    <meta content="SaleMe.lk හඳුන්වාදෙන 'කොකිස් Challenge'. මේ අවුරුදු කාලේ කොකිස් Challenge එක Play කර, වැඩිම ලකුණු ලබාගෙන වටිනා T-Shirt සහ නොමිලේ Reload දිනා ගන්න. SaleMe.lk වෙත පිවිසෙන්න, නැත්නම් https://goo.gl/AgqfpY click කරන්න. කොකිස් වැඩි ප්‍රමාණයක් රැස්කර යාලුවෝ 5දෙනෙක් tag කරලා share කරන්න #KokisChallenge #Awurudu2K18 #SaleMe #WinTshirts #Reloads"
          name=description>
    <meta content=#fea502 name=theme-color>
    <link href="{{asset('/images/salenow/favicon-16x16.png')}}" rel="shortcut icon" type=image/x-icon>
    <link href="{{asset('/images/salenow/favicon-16x16.png')}}" rel=icon type=image/x-icon>
    <meta content={{Request::url()}} property=og:url>
    <meta content=www.saleme.lk property=og:site_name>
    <meta content=product property=og:type>
    <meta content="SaleMe.lk කොකිස් Challenge | SaleMe.lk Kokis Challenge " property=og:title>
    <meta content="SaleMe.lk හඳුන්වාදෙන 'කොකිස් Challenge'. මේ අවුරුදු කාලේ කොකිස් Challenge එක Play කර, වැඩිම ලකුණු ලබාගෙන වටිනා T-Shirt සහ නොමිලේ Reload දිනා ගන්න. SaleMe.lk වෙත පිවිසෙන්න, නැත්නම් https://goo.gl/AgqfpY click කරන්න. කොකිස් වැඩි ප්‍රමාණයක් රැස්කර යාලුවෝ 5දෙනෙක් tag කරලා share කරන්න #KokisChallenge #Awurudu2K18 #SaleMe #WinTshirts #Reloads"
          property=og:description>
    <meta content=600 property=og:image:width>
    <meta content=315 property=og:image:height>
    <meta content="{{asset('')}}game_assets/banner.jpg" property=og:image>
    <meta content=1867918153484824 property=fb:app_id>

    {{ Html::style('css/salenow/layout/bootstrap-3.3.7/css/bootstrap.min.css') }}
    {{Html::script('js/salenow/jquery.min.js')}}
    {{Html::script('js/salenow/bootstrap.min.js')}}
    {{ Html::style('game_assets/css/style.css') }}
    {{ Html::style('css/salenow/layout/ionicons/css/ionicons.min.css') }}
    <link href="https://fonts.googleapis.com/css?family=Hi+Melody" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
    {{--spark--}}
    {{ Html::style('game_assets/spark/css/style.css') }}
    <script>
        window.fbAsyncInit = function () {
            FB.init({
                appId: '1867918153484824',
                autoLogAppEvents: true,
                xfbml: true,
                version: 'v2.12'
            });
        };
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <style>
        @import url(https://fonts.googleapis.com/css?family=Roboto:400,300,600,400italic);

        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-font-smoothing: antialiased;
            -moz-font-smoothing: antialiased;
            -o-font-smoothing: antialiased;
            font-smoothing: antialiased;
            text-rendering: optimizeLegibility;
        }

        body {
            font-family: "Roboto", Helvetica, Arial, sans-serif;
            font-weight: 100;
            font-size: 12px;
            line-height: 30px;
            color: #777;
            background: #fea502;
        }

        .container {
            max-width: 400px;
            width: 100%;
            margin: 0 auto;
            position: relative;
        }

        #contact input[type="text"],
        #contact input[type="email"],
        #contact input[type="tel"],
        #contact input[type="url"],
        #contact textarea,
        #contact button[type="submit"] {
            font: 400 12px/16px "Roboto", Helvetica, Arial, sans-serif;
        }

        #contact,
        #conatct2,
        #contact3 {
            background: #F9F9F9;
            padding: 25px;
            margin: 25px 0;
            box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        }

        #contact h3 {
            display: block;
            font-size: 30px;
            font-weight: 300;
            margin-bottom: 10px;
            margin-top: 0px;
        }

        #contact h4 {
            margin: 5px 0 15px;
            display: block;
            font-size: 13px;
            font-weight: 400;
        }

        fieldset {
            border: medium none !important;
            margin: 0 0 10px;
            min-width: 100%;
            padding: 0;
            width: 100%;
        }

        #contact input[type="text"],
        #contact input[type="email"],
        #contact input[type="tel"],
        #contact input[type="url"],
        #contact textarea {
            width: 100%;
            border: 1px solid #ccc;
            background: #FFF;
            margin: 0 0 5px;
            padding: 10px;
        }

        #contact input[type="text"]:hover,
        #contact input[type="email"]:hover,
        #contact input[type="tel"]:hover,
        #contact input[type="url"]:hover,
        #contact textarea:hover {
            -webkit-transition: border-color 0.3s ease-in-out;
            -moz-transition: border-color 0.3s ease-in-out;
            transition: border-color 0.3s ease-in-out;
            border: 1px solid #aaa;
        }

        #contact textarea {
            height: 100px;
            max-width: 100%;
            resize: none;
        }

        #contact button[type="submit"] {
            cursor: pointer;
            width: 100%;
            border: none;
            background: #4CAF50;
            color: #FFF;
            margin: 0 0 5px;
            padding: 10px;
            font-size: 15px;
        }

        #contact button[type="submit"]:hover {
            background: #43A047;
            -webkit-transition: background 0.3s ease-in-out;
            -moz-transition: background 0.3s ease-in-out;
            transition: background-color 0.3s ease-in-out;
        }

        #contact button[type="submit"]:active {
            box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.5);
        }

        .copyright {
            text-align: center;
        }

        #contact input:focus,
        #contact textarea:focus {
            outline: 0;
            border: 1px solid #aaa;
        }

        ::-webkit-input-placeholder {
            color: #888;
        }

        :-moz-placeholder {
            color: #888;
        }

        ::-moz-placeholder {
            color: #888;
        }

        :-ms-input-placeholder {
            color: #888;
        }

        .banner-div {
            /*margin-top: 26px;*/
            padding: 0 0 10px;
        }

        .top-score {
            text-align: right;
            padding-right: 15px;
            font-family: 'Hi Melody', cursive;
            margin: 0;
            font-size: 25px;
            line-height: 0.5;
            color: #222;
        }

        .media-head {
            font-family: 'Hi Melody', cursive;
            font-size: 24px;
            padding: 0;
            margin: 0;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            color: #222;
            text-align: left;
        }

        .top-ten {
            margin: 15px 0;
            text-align: center;
            font-size: 25px;
            color: #fff;
            font-family: 'Pacifico', cursive;
        }
        .scorebord{
            padding: 0px;
            background: transparent;
        }
    </style>
</head>

<body>
<canvas id="canvas" style="width: 100%  !important;"></canvas>
<div class="container sharediv">
    <div class="row">
        <div class="col-md-12 banner-div">
            <img src="../game_assets/lead.jpg" alt="" class="img-responsive">
        </div>
        <div class="col-md-12 scorebord ">
            <p class="top-ten">
                <a href="{{asset('')}}game/play/" style="    color: #fff;"><i class="ion-arrow-left-b pull-left"><span style="font-size:13px"> Back</span></i></a> Today Top Score</p>
            @if(count($leadboard))
                <?php $x = 1; ?>
                @foreach($leadboard as $item)
                    <div class="item">
                        <div class="media item-m">
                            <div class="media-left">
                                <img src="../game_assets/{{($x<4)?$x.'.png':'def.png'}}" class="media-object"
                                     style="width:50px">
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading media-head">{{$x}}. {{ucwords($item->name)}}</h4>
                                <p class="top-score">{!!($item->score)?'Score <b>'.$item->score.'</b>':'Already Rewarded'!!}</p>
                            </div>
                        </div>
                    </div>
                    <?php $x++; ?>
                @endforeach
            @else
                <div class="item">
                    <h4>No records available</h4>
                    <span></span>
                </div>
            @endif
        </div>

    </div>

</div>

</body>
<script type="text/javascript" src="{{ asset("plugins/blockui/jquery.blockUI.js")}}"></script>
{{ Html::script('js/salenow/plugin/jquery.validate.js') }}
{{Html::script('game_assets/spark/js/index.js')}}


</html>
