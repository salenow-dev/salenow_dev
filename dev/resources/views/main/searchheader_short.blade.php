@include('main.header')

{{--<section class="leader-board-top">--}}
{{--<div class="container visible-md visible-lg">--}}
{{--<div class="image-div">--}}
{{--<!-- Google Analytics -->--}}
{{--<script>--}}
{{--(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){--}}
{{--(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),--}}
{{--m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)--}}
{{--})(window,document,'script','https://www.google-analytics.com/analytics_debug.js','ga');--}}

{{--ga('create', 'UA-XXXXX-Y', 'auto');--}}
{{--ga('send', 'pageview');--}}
{{--</script>--}}
{{--<!-- End Google Analytics -->--}}
{{--<a href="http://saleme.dev/ads"--}}
{{--onClick="_gaq.push(['_trackEvent', 'Banner', 'Click', 'BANNERNAME',1.00,true]);">--}}
{{--<img--}}
{{--style="border: 0px" src="{{asset('/images/salenow/advertisments/leader-board-desktop.jpg')}}"--}}
{{--alt="SOMEALTEXT"/></a>--}}
{{--<img width=0 height=0--}}
{{--src="{{asset('/images/salenow/advertisments/leader-board-desktop.jpg')}}"--}}
{{--onload="_gaq.push(['_trackEvent', 'Banner', 'Impression', 'BANNERNAME',2.00,true]);" style="display: none"/>--}}
{{--            <img src="{{asset('/images/salenow/advertisments/leader-board-desktop.jpg')}}" class="">--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="image-div-xs visible-xs">--}}
{{--<img src="{{asset('/images/salenow/advertisments/leader-board-mobile.jpg')}}" class="">--}}
{{--</div>--}}
{{--</section>--}}
<section class="inner-search hidden-xs">
    <div class="search-page-short">
        <div class="container">
            <div class="mar-70 text-center">
                <form id="search_form" method="get" action="{{asset('')}}ads">
                    <div class="col-xs-12  search-panel2-short">
                        <div class="input-group home-search1">
                            <div class="input-group-btn ">
                                <button type="button" class="btn location-btn " data-toggle="modal"
                                        data-target=".select-city-modal">
                                    <span class="lnr lnr-pointer-down"></span><span
                                            id="">&nbsp;{{!empty($location_slug) ? ucfirst(str_replace('_',' ',$location_slug)) :'Select City'}}</span>
                                </button>
                                <button type="button" class="btn cat-btn" data-toggle="modal"
                                        data-target=".select-category-modal">
                                    @if(!empty($category))
                                        <span class="lnr lnr-tag"></span><span
                                                id="">&nbsp;{{!empty($category) ? $category :'All Categories'}}</span>
                                    @elseif(!empty($scategory))
                                        <span class="lnr lnr-tag"></span><span
                                                id="">&nbsp;{{!empty($scategory) ? $scategory :'All Categories'}}</span>
                                    @else
                                        <span class="lnr lnr-tag"></span><span
                                                id="">&nbsp;{{!empty($scategory) ? $scategory :'All Categories'}}</span>
                                    @endif
                                </button>
                            </div>
                            <input class="form-control search-txt" name="query"
                                   value="@if(!empty($querydata['query'])) {{$querydata['query']}}@endif"
                                   placeholder="What you looking for..." type="text">
                            <span class="input-group-btn">
                            <button class="btn  search-btn" id="more_query" type="button"><span
                                        class="lnr lnr-magnifier"></span></button>
                        </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!--search-mobile-->
<section class="inner-search visible-xs">
    <div class="search-page-xs">
        <div class="container">
            <div class="mar-70 text-center ">
                {{--<h1 class="promo-head4">--}}
                {{--Find best deals from <b>Sri lanka’s No #<span class="no-1">1</span></b> Marketplace--}}
                {{--</h1>--}}
                <div class="col-xs-6 pd-r-5-xs">
                    <div class="input-group home-search1">
                        <div class="input-group-btn ">
                            <button type="button" class="btn location-btn " data-toggle="modal"
                                    data-target=".select-city-modal">
                                <span class="lnr lnr-pointer-down"></span> <span
                                        id="search_concept">&nbsp;{{!empty($location_slug) ? ucfirst(str_replace('_',' ',$location_slug)) :'Select City'}}</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 pd-l-5-xs">
                    <div class="input-group home-search1">
                        <div class="input-group-btn ">
                            <button type="button" class="btn cat-btn" data-toggle="modal"
                                    data-target=".select-category-modal">
                                @if(!empty($category))
                                    <span class="lnr lnr-tag"></span><span
                                            id="">&nbsp;{{!empty($category) ? $category :'All Categories'}}</span>
                                @elseif(!empty($scategory))
                                    <span class="lnr lnr-tag"></span><span
                                            id="">&nbsp;{{!empty($scategory) ? $scategory :'All Categories'}}</span>
                                @else
                                    <span class="lnr lnr-tag"></span><span
                                            id="">&nbsp;{{!empty($scategory) ? $scategory :'All Categories'}}</span>
                                @endif
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12  search-panel2 ">
                    <form id="search_form1" method="GET" action="{{asset('')}}ads">
                        <div class="input-group stylish-input-group">
                            <input type="text" class="form-control search-txt-xs" name="query"
                                   value="@if(!empty($querydata['query'])){{$querydata['query']}}@endif"
                                   placeholder="What you looking for...">
                            <span class="input-group-addon search-btn-span-xs">
                                <button type="submit" id="more_query1" class="search-btn-xs">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
