<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    {{--<!-- CSRF Token -->--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> {{ config('app.name') }} | @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Absolutely free to buy and and sell anything you have.. or even lease or rent... we help you to find better deals from all over Sri Lanka and the best deal closest to you.">
    <meta name="keywords"
          content="salenow.lk, salenow, free, absolutely free, classified, website, sri lanka, free classified">
    <meta name="theme-color" content="#fea502"/>
    {{--favicon--}}
    <link rel="shortcut icon" href="{{asset('/images/salenow/favicon-16x16.png')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('/images/salenow/favicon-16x16.png')}}" type="image/x-icon">

    {{--<!--OG-->--}}
    <meta property="og:url" content="https://salenow.lk/">
    <meta property="og:site_name" content="salenow.lk">
    <meta property="og:type" content="website" />
    <meta property="og:title" content=" @yield('og-title')">
    {{--<meta name="robots" content="noarchive,nofollow,unavailable_after:Tuesday,27-Jun-17 06:48:54 UTC">--}}
    <meta property="og:description"
          content=" @yield('og-description')">
    <meta property="og:image" content="@yield('og-image')"/>
    {{--<!--OG-->--}}

<!-- Bootstrap Core CSS -->
    {{ Html::style('css/salenow/layout/bootstrap-3.3.7/css/bootstrap-theme.min.css') }}
    {{ Html::style('css/salenow/layout/bootstrap-3.3.7/css/bootstrap.min.css') }}
    {{ Html::style('css/salenow/layout/saleme-custom.css') }}

<!--icon library-->
    {{ Html::style('css/salenow/layout/ionicons/css/ionicons.min.css') }}
    {{--{{ Html::style('css/salenow/layout/saleme-icon.css') }}--}}
    {{ Html::style('css/salenow/layout/linericons/style.css') }}
    {{ Html::style('css/salenow/layout/font-awesome-4.7.0/css/font-awesome.min.css') }}

<!-- fonts -->
    {{ Html::style('css/salenow/fonts/Catamaran.css') }}
    {{ Html::style('css/salenow/fonts/icon.css') }}
    {{ Html::style('css/salenow/fonts/icon-font.min.css') }}
    {{ Html::style('css/salenow/fonts/Khula.css') }}
    {{ Html::style('css/salenow/fonts/Raleway.css') }}
    {{ Html::style('css/salenow/fonts/work.css') }}

<!--custom css files-->
    {{ Html::style('css/salenow/layout/saleme-responsive.css') }}
    {{ Html::style('css/salenow/layout/saleme-resposive-tab.css') }}

<!--bootstrap select with search-->
    {{ Html::style('css/salenow/layout/select-search/bootstrap-select.css') }}
    {{ Html::style('css/salenow/layout/select-search/bootstrap-select.min.css') }}

<!--    custom css files -->
{{ Html::style('css/salenow/layout/saleme-responsive.css') }}

        <!--other plugins-->
<!--{{ Html::style('css/salenow/plugin/select-search/bootstrap-select.css') }}-->


    <script>
        window.Laravel = <?php
        echo json_encode([
            'csrfToken' => csrf_token(),
        ]);
        ?>
    </script>
    {{ Html::script('js/salenow/jquery.min.js') }}
    {{ Html::script('js/salenow/bootstrap.min.js') }}

    {{--google analitics--}}
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-105583065-1', 'auto');
        ga('send', 'pageview');

    </script>
    {{--google analitics--}}

</head>
<body>
<section class="leader-board-top-mobile visible-xs">
    <div class="image-div-xs ">
        <img src="{{asset('/images/salenow/advertisments/leader-board-mobile.jpg')}}" class="">
    </div>
</section>

<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=1054163521396174";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div class="navbar navbar-info hidden-xs">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target=".navbar-material-light-blue-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{URL::to('/')}}">
                {{ Html::image('images/salenow/saleme-logo.png', 'saleme.lk logo', array('class' => 'img-responsive')) }}
            </a>
        </div>
        <div class="navbar-collapse collapse navbar-material-light-blue-collapse">
            <ul class="dropdown-menu">
                <li><a href="#">Normal</a></li>
                <li class="disabled"><a href="#">Disabled</a></li>
                <li class="active"><a href="#">Active</a></li>
                <li><a href="#">Normal</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a class="all pd-t-32" href="{{asset('')}}ads">All Ads</a>
                </li>
                {{-- @if(Session::get('mname') || Session::get('mememail') && Session::get('memid'))--}}
                @if(checkMember())
                    <li>
                    <!--<a class="all pd-t-32" href="{{asset('')}}"><span class="lnr lnr-mustache"></span>&nbsp; </a>-->
                        <div class="dropdown user-drop">
                                <span class="dropdown-toggle" data-toggle="dropdown">
                                    <span class="lnr lnr-mustache"></span>&nbsp;
                                    <span class="username-md">{{!empty(getMember()->first_name) ? getMember()->first_name : getMember()->email }}</span>
                                    <span class="caret"></span>
                                </span>
                            <ul class="dropdown-menu">
                                <li><a href="{{asset('')}}myprofile"><span class="ion-person"></span>&nbsp;&nbsp;&nbsp;My
                                        Account</a></li>
                                <li><a href="{{asset('')}}my_ads"><span class="ion-ios-pricetags"></span>&nbsp;&nbsp;&nbsp;My
                                        Ads</a>
                                </li>
                                <li class="divider"></li>
                                <li><a href="{{asset('')}}member_logout"><span class="ion-power"></span>&nbsp;&nbsp;&nbsp;Logout</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                @else
                    <li>
                        <a href="#" class="login pd-t-30" data-toggle="modal" data-target=".login-register"><span
                                    class="lnr lnr-mustache"></span>&nbsp; Login / Register </a>
                    </li>
                @endif
                <li> @if(getMemberId())
                        <a href="{{asset('')}}addtype" class="post-free-ad-btn">
                            <button type="button" class="btn post  btn-lg ">Post Free Ad</button>
                        </a>
                    @else
                        <a href="#" data-toggle="modal" class="post-free-ad-btn" data-target=".login-register">
                            <button type="button" class="btn post  btn-lg ">Post Free Ad</button>
                        </a>
                    @endif
                </li>

            </ul>
        </div>
    </div>
</div>
<!--mobile menu-->
<div class="navbar navbar-info visible-xs">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{URL::to('/')}}">
                {{ Html::image('images/salenow/saleme-logo.png', 'salenow.lk logo', array('class' => 'img-responsive logo')) }}
            </a>
            @if(checkMember())
                <a href="{{asset('')}}addtype" class="post-free-ad-btn">
                    <button type="button" class="btn post  btn-lg ">Post Free Ad</button>
                </a>
            @else
                <a href="#" data-toggle="modal" class="post-free-ad-btn" data-target=".login-register">
                    <button type="button" class="btn post  btn-lg ">Post Free Ad</button>
                </a>
            @endif
            <a href="#" class="post-free-ad-btn"> </a>
        </div>
    </div>
    <div class="sub-menu-bar">
        <a class="all-ads-mo" href="{{asset('')}}ads">All ads</a>
        @if(checkMember())
            <div class="dropdown login-register-mo pull-right">
                    <span class="dropdown-toggle" data-toggle="dropdown">
                        <span class="username-xs">{{!empty(getMember()->first_name) ? getMember()->first_name : getMember()->email }}</span>
                        <span class="caret"></span>
                    </span>
                <ul class="dropdown-menu">
                    <li><a href="{{asset('')}}myprofile"><span class="ion-person"></span>&nbsp;&nbsp;&nbsp;My
                            Account</a></li>
                    <li><a href="{{asset('')}}my_ads"><span class="ion-ios-pricetags"></span>&nbsp;&nbsp;&nbsp;My
                            Ads</a></li>
                    <li class="divider"></li>
                    <li><a href="{{asset('')}}member_logout"><span class="ion-power"></span>&nbsp;&nbsp;&nbsp;Logout</a>
                    </li>
                </ul>
            </div>
        @else
            <a href="#" data-toggle="modal" data-target=".login-register" class="login-register-mo pull-right">
                <span class="ion-ios-person"></span>
            </a>
        @endif
    </div>
</div>
<!--end mobile menu-->
{{-- START CONTENT EXTEND--}}
@yield('frontend_content')
{{-- END CONTENT EXTEND--}}
<section class="pin-bg bord-top">
    <div class="container">
        <div class=" hidden-xs"></div>
        <div class="row post-ad-banner-bottom">
            <div class="col-sm-9 col-md-9 col-lg-9">
                <div class="promo_content">
                    <h3><span class="quick blink_me">Quick</span> Buy, Sell, Rent or Anything - Post your Ad on
                        salenow.lk</h3>
                </div>
            </div>
            <div class="col-sm-3 col-md-3 col-lg-3">
                <div class="pb_action">
                    @if(getMemberId())
                        <a class="btn post-btn-bottom  btn-lg " href="{{asset('')}}addtype"> Post Free Ad
                        </a>   @else
                        <a class="btn post-btn-bottom  btn-lg" data-toggle="modal" data-target=".login-register"
                           href="#">
                            Post Free Ad
                        </a>
                    @endif
                </div>
            </div>
        </div>
        <div class=" hidden-xs"></div>
    </div>
</section>
<footer id="footer" class="clearfix">
    <!-- footer-top -->
    <section class="footer-top clearfix">
        <div class="container">
            <div class="row">
                <!-- footer-widget -->
                <div class="col-sm-3">
                    <div class="footer-widget news-letter">
                        <a href=""></a>
                        {{ Html::image('images/salenow/footer-img.png', 'saleme.lk logo', array('class' => 'img-responsive footer-logo')) }}
                        <hr class="visible-xs">
                    </div>
                </div>
                <!-- footer-widget -->
                <div class="col-sm-3">
                    <div class="footer-widget">
                        <h4>Quik Links</h4>
                        <ul>
                            <li><a href="{{asset('')}}about">About Us</a></li>
                            <li><a href="{{asset('')}}contact-us">Contact Us</a></li>
                            <li><a href="{{asset('')}}members/premium">Premium Members</a></li>
                            {{--<li><a href="{{asset('')}}about">Careers</a></li>--}}
                            {{--<li><a href="{{asset('')}}help-support">Help &amp; Support</a></li>--}}
                            {{--<li><a href="{{asset('')}}about">Advertise With Us</a></li>--}}
                        </ul>
                        <hr class="visible-xs">
                    </div>
                </div><!-- footer-widget -->

                <!-- footer-widget -->
                <div class="col-sm-3">
                    <div class="footer-widget">
                        <h4>Stay Safe with salenow.lk</h4>
                        <ul>
                            <li><a href="{{asset('')}}terms-conditions">Terms & Conditions</a></li>
                            <li><a href="{{asset('')}}privacy-policy">Privacy Policy</a></li>
                            {{--<li><a href="{{asset('')}}faq">FAQ</a></li>--}}
                            {{--<li><a href="{{asset('')}}stay-safe">Stay Safe</a></li>--}}
                            {{--<li><a href="{{asset('')}}sell-fast">How to sell fast</a></li>--}}
                            {{--<li><a href="{{asset('')}}banner-advertising">Banner Advertising</a></li>--}}
                            {{--<li><a href="#">Membership</a></li>--}}
                            {{--<li><a href="#">Promote your ad</a></li>--}}
                            {{--<li><a href="#">Trade Delivers</a></li>--}}
                        </ul>
                        <hr class="visible-xs">
                    </div>
                </div><!-- footer-widget -->
                <!-- footer-widget -->
                <div class="col-sm-3">
                    <div class="footer-widget social-widget">
                        <h4>Follow us on</h4>
                        <ul>
                            <li><a href="https://www.facebook.com/salemeclassified/"
                                   target="_blank"><i class="fa fa-facebook-official"></i>Facebook</a></li>
                            <li><a href="https://www.linkedin.com/company/18223121/" target="_blank"><i
                                            class="fa fa-linkedin-square"></i>Linkedin</a></li>
                            <li><a href="https://www.youtube.com/channel/UCsgsXTkgA3bIAopAXtOrwcg" target="_blank"><i
                                            class="fa fa-youtube-square"></i>youtube</a></li>
                        </ul>
                        <hr class="visible-xs">
                    </div>
                </div><!-- footer-widget -->
            </div><!-- footer-widget -->
        </div><!-- row -->
        </div><!-- container -->
    </section><!-- footer-top -->
    <!--/.footer-bottom-->
    <div class="footer-bottom">
        <div class="container">
            <p class="pull-left"> &copy; {{date("Y")}} Saleme.lk (Pvt) Ltd. All right reserved. </p>
        </div>
    </div>
    <!--/.footer-bottom-->
</footer>
<!--back to top-->
<div class="scroll-top-wrapper ">
    <span class="scroll-top-inner">
        <i class="ion-ios-arrow-up"></i>
    </span>
</div>
<!--end back to top-->
@include('default.popup')
</body>
{{ Html::script('plugins/default/bootstrap-select.js') }}
<script type="text/javascript" src="{{ asset("plugins/blockui/jquery.blockUI.js")}}"></script>
<script type="text/javascript" src="{{ asset("js/default.js")}}"></script>
<!-- FlexSlider  inner-->
{{ Html::style('css/salenow/plugin/inner-slider/flexslider.css') }}
<!--flex slider-->
{{ Html::script('js/salenow/plugin/inner-slider/flexslider.js') }}
<!--custom js-->
{{ Html::script('js/salenow/layout/saleme-custom.js') }}

<!--end custom js-->
<script type="text/javascript">
    window.onload = function () {
        $('.selectpicker').selectpicker();
    };
</script>
{{ Html::script('js/salenow/layout/price-slider.js') }}

<!--end custom js-->

<script>
    $(window).load(function () {
        $('.flexslider').flexslider({
            animation: "slide",
            controlNav: "thumbnails"
        });
    });
</script>

<!--bootstap select with search-->
{{ Html::script('js/salenow/plugin/select-search/bootstrap-select.js') }}
<script type="text/javascript">
    $('.selectpicker').selectpicker()
</script>
<!--bootstap select with search-->

<script type="text/javascript">
    $(document).ready(function () {
        $('.vehicle-ad-type .i-want').click(function () {
            $('.hide-item').show();
        });
        $('.vehicle-ad-type .i-Offer').click(function () {
            $('.hide-item').hide();
        });
    });
</script>
{{ Html::script('plugins/default/validator.min.js') }}
<script type="text/javascript">
    $('#more_query').click(function () {
        $('#search_form').submit();
    });
    $('#more_query1').click(function () {
        $('#search_form1').submit();
    });
</script>

<script>
    function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".more-less")
            .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
{{ Html::script('js/salenow/plugin/jquery.validate.js') }}
{{ Html::script('js/memeber/memeber.js') }}
<script>
    window.___gcfg = {
        lang: 'en-US',
        parsetags: 'onload'
    };
</script>
{{--call to action slider--}}
<script>
    $(".callcenterBox").click(function (e) {
        $(this).toggleClass('active');
    });
</script>
{{--call to action slider end--}}
<script src="{{asset('plugins/default/platform.js')}}" async defer></script>
{{ Html::script('js/socialshare/index.js') }}
{{--sticky nav--}}
<script>
    $(window).scroll(function () {
        if ($(window).scrollTop() >= 300) {
            $('.navbar').addClass('fixed-header');
        }
        else {
            $('.navbar').removeClass('fixed-header');
        }
    });
</script>
</html>









