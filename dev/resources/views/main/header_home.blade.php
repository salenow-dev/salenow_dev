<!DOCTYPE html>
<html lang=en>
<meta charset=utf-8>
<meta content="text/html; charset=utf-8" http-equiv=Content-Type>{{----}}
<meta content="{{csrf_token()}}" name=csrf-token>
<title>{{config('app.name')}} - @yield('title')</title>

<meta content="width=device-width,initial-scale=1" name=viewport>@if((!empty($category) || !empty($scategory)) && is_string ($category) ) @if(!empty($scategory) )
    <meta content="Find any{{$scategory}}for sale in Sri Lanka - find the best online deals on saleme.lk, free classified sri lanka!" name=description>@else
    <meta content="Find any{{$category}}for sale in Sri Lanka - find the best online deals on saleme.lk, free classified sri lanka!" name=description>@endif @else
    <meta content="SaleMe is to buy and sell anything by online. we help you to find better deals from all over land for sale, used cars sri lanka, used phones sri lanka, used phones for sale and cars for sale." name=description>@endif
<meta content="land for sale, used cars sri lanka, mobile phones sri lanka, used phones for sale, cars for sale, laptop for sale, bikes for sale, sale me" name=keywords>
<meta content=#fea502 name=theme-color>
<link href="{{asset('/images/salenow/favicon-16x16.png')}}" rel="shortcut icon" type=image/x-icon>
<link href="{{asset('/images/salenow/favicon-16x16.png')}}" rel=icon type=image/x-icon>
<meta content={{Request::url()}}property=og:url>
<meta content=www.saleme.lk property=og:site_name>
<meta content=product property=og:type>
<meta content=" @yield('og-title')" property=og:title>
<meta content=" @yield('og-description')" property=og:description>
<meta content=600 property=og:image:width>
<meta content=315 property=og:image:height>
<meta content="@yield('og-image')" property=og:image>

<meta content=1867918153484824 property=fb:app_id>{{Html::style('css/salenow/layout/bootstrap-3.3.7/css/bootstrap-theme.min.css')}}{{Html::style('css/salenow/layout/bootstrap-3.3.7/css/bootstrap.min.css')}}{{Html::style('css/salenow/layout/saleme-custom.css')}}{{Html::style('css/salenow/layout/ionicons/css/ionicons.min.css')}}{{Html::style('css/salenow/layout/linericons/style.min.css')}}{{Html::style('css/salenow/layout/font-awesome-4.7.0/css/font-awesome.min.css')}}{{Html::style('css/salenow/fonts/work.css')}}{{Html::style('css/salenow/layout/saleme-responsive.css?ver=0.2')}}
<script>
    window.Laravel = <?php echo json_encode([ 'csrfToken'=> csrf_token(),]); ?>
</script>{{Html::script('js/salenow/jquery.min.js')}}{{Html::script('js/salenow/bootstrap.min.js')}}{{--START google AdSense account - DFP--}}
<script async src=https://www.googletagservices.com/tag/js/gpt.js></script>
<script>
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [], googletag.cmd.push(function() {
        googletag.defineSlot("/21634329919/slm_leaderboard_970x90_atf", [320, 50], "div-gpt-ad-1511940698204-0").addService(googletag.pubads()), googletag.defineSlot("/21634329919/slm_leaderboard_970x90_atf", [970, 90], "div-gpt-ad-1511940698204-1").addService(googletag.pubads()), googletag.defineSlot("/21634329919/slm_skyscraper_160x600_atf", [160, 600], "div-gpt-ad-1512040136576-0").addService(googletag.pubads()).setTargeting("subcategory-mobile", "{{(!empty($subcategory_code))?$subcategory_code:''}}"), googletag.defineSlot("/21634329919/slm_bottom_970x90", [300, 100], "div-gpt-ad-1512033969149-0").addService(googletag.pubads()), googletag.defineSlot("/21634329919/slm_bottom_970x90", [970, 90], "div-gpt-ad-1512033969149-1").addService(googletag.pubads()), googletag.defineSlot("/21634329919/slm_square_300x250_btf", [300, 250], "div-gpt-ad-1512731616849-0").addService(googletag.pubads()), googletag.enableServices()
    })
</script>{{--END google AdSense account - DFP--}}
<script>
    ! function(e, t, a, n, r) {
        e[n] = e[n] || [], e[n].push({
            "gtm.start": (new Date).getTime(),
            event: "gtm.js"
        });
        var s = t.getElementsByTagName("script")[0],
            g = t.createElement("script");
        g.async = !0, g.src = "https://www.googletagmanager.com/gtm.js?id=GTM-PQQWX8C", s.parentNode.insertBefore(g, s)
    }(window, document, 0, "dataLayer")
</script>
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-834902320"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments)
    }
    gtag("js", new Date), gtag("config", "AW-834902320")
</script>

<body>
<noscript>
    <iframe height=0 src="https://www.googletagmanager.com/ns.html?id=GTM-PQQWX8C" style="display:none;visibility:hidden"
            width=0></iframe>

</noscript>
<h1 style=display:none>land for sale</h1>
<h1 style=display:none>used cars sri lanka</h1>
<h1 style=display:none>used phones sri lanka</h1>
<h1 style=display:none>used phones for sale</h1>
<h1 style=display:none>cars for sale</h1>
<h2 style=display:none>land for sale</h2>
<h2 style=display:none>used cars sri lanka</h2>
<h2 style=display:none>used phones sri lanka</h2>
<h2 style=display:none>used phones for sale</h2>
<h2 style=display:none>cars for sale</h2>
<section class="visible-xs leader-board-top-mobile">

    <div class=image-div-xs align=center>
    {{--<img src="{{asset('/images/salenow/banners/leader-board-mobile.jpg')}}">--}}
    {{--<div id=div-gpt-ad-1511940698204-0>--}}
    {{--<script>--}}
    {{--googletag.cmd.push(function() {--}}
    {{--googletag.display("div-gpt-ad-1511940698204-0")--}}
    {{--})--}}
    {{--</script>--}}
    {{--</div>--}}
    <!-- /21634329919/saleme_mobile_leaderboard_320x50 -->
        <div id='div-gpt-ad-1527136399877-0' style='height:50px; width:320px;'>
            <script>
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1527136399877-0'); });
            </script>
        </div>
    </div>
</section>
<div id=fb-root></div>
<script>
    !function (e, t, n) {

        var c, o = e.getElementsByTagName("script")[0];
        e.getElementById(n) || ((c = e.createElement("script")).id = n, c.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=1054163521396174", o.parentNode.insertBefore(c, o))
    }(document, 0, "facebook-jssdk")
</script>
<div class="navbar navbar-info hidden-xs">
    <div class=container>
        <div class=navbar-header>
            <button class=navbar-toggle type=button data-target=.navbar-material-light-blue-collapse
                    data-toggle=collapse><span class=icon-bar></span> <span class=icon-bar></span> <span
                        class=icon-bar></span></button>
            <a href="{{URL::to('/')}}"
               class=navbar-brand>{{Html::image('images/salenow/saleme-logo.png', 'saleme.lk logo', array('class'=> 'img-responsive'))}}</a>
        </div>
        <div class="collapse navbar-collapse navbar-material-light-blue-collapse">

            <ul class=dropdown-menu>
                <li><a href=#>Normal</a>
                <li class=disabled><a href=#>Disabled</a>
                <li class=active><a href=#>Active</a>
                <li><a href=#>Normal</a>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{asset('')}}ads" class="all pd-t-32">All Ads</a></li>@if(checkMember())
                    <?php $member = getMember(); ?>@else
                    <?php $member = ''; ?>@endif @if(checkCompany())
                    <?php $checkMember = true;?>@else
                    <?php $checkMember = false;?>@endif @if(!empty($member))
                    <li>
                        <ul class="nav navbar-nav user-nav-desktop">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle user-drop2" data-toggle="dropdown"> <span
                                            class="lnr lnr-mustache"></span> {{($checkMember)?(session()->has('profile_type'))?$member->first_name:'Premium Account':$member->first_name}}
                                    <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{asset('')}}myprofile">{{($checkMember)?(temporyPrivateLogin())?'&nbsp;&nbsp;My Account':'&nbsp;&nbsp;Company Account':'&nbsp;&nbsp;My Account'}}<span
                                                    class="ion-ios-person pull-left"></span></a></li>
                                    <li class="divider"></li>
                                    @if($member->employee && !temporyPrivateLogin())
                                        <li><a href="{{asset('')}}employee/{{$member->employee->id}}/edit">&nbsp;&nbsp;User Account<span
                                                        class="ion-edit pull-left"></span></a></li>
                                        <li class="divider"></li>
                                        <li><a href="{{asset('')}}my_ads?type=private">&nbsp;&nbsp;Private Account<span
                                                        class="ion-ios-locked-outline pull-left"></span></a></li>
                                        <li class="divider"></li>
                                    @endif
                                    @if($member->employee && !$checkMember || temporyPrivateLogin())
                                        <li><a href="{{asset('')}}myprofile?type=company">&nbsp;&nbsp;Company Account<span
                                                        class="ion-flag pull-left"></span></a></li>
                                        <li class="divider"></li>
                                    @endif
                                    @if(!$checkMember || temporyPrivateLogin())
                                        <li><a href="{{asset('')}}my_ads">&nbsp;&nbsp;My Ads<span
                                                        class="ion-ios-pricetags-outline pull-left"></span></a></li>
                                        <li class="divider"></li>
                                    @endif
                                    <li><a href="{{asset('')}}wishlist">&nbsp;&nbsp;Wish List<span
                                                    class="ion-ios-heart pull-left"></span></a></li>
                                    <li class="divider"></li>
                                    {{--<li class="divider"></li>--}}
                                    <li><a href="{{asset('')}}member_logout">&nbsp;&nbsp;Logout<span
                                                    class="ion-power pull-left"></span></a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    </li>@else
                    <li><a href=/member-login class="login pd-t-30"><span class="lnr lnr-mustache"></span>Â  Login/
                            Register</a></li>@endif
                <li>
                    <a href="{{(checkMember())?asset('').'addtype':'/member-login'}}" class=post-free-ad-btn>
                        <button class="btn btn-lg post-lg" type=button>Post Free Ad</button>
                    </a>
            </ul>
        </div>
    </div>
</div>
<div class="navbar navbar-info visible-xs">
    <div class=mar-top-10>
        <div class="col-xs-4 col-md-4 pd-r-0"><a href="{{URL::to('/')}}">{{Html::image('images/salenow/saleme-logo.png', 'saleme.lk logo', array('class'=> 'img-responsive'))}}</a> </div>
        <div class="pd-l-0 col-md-5 col-xs-8 mar-top-5">
            <div class="pd-l-0 col-xs-4"><a href="{{asset('')}}ads"><span class="allads-nav pull-right">All Ads</span></a></div>
            <div class="col-xs-8 no-padding text-center">@if(!empty($member))
                    <div class=dropdown> <span class=dropdown-toggle data-toggle=dropdown><span>{{Html::image('images/user_black.png', 'saleme.lk user', array('class'=> 'user-fixed-img'))}}</span> <span class="crt-dpdwn ion-arrow-down-b"></span></span>
                        <ul class=dropdown-menu>
                            <li><a href="{{asset('')}}myprofile"><span class=ion-person></span>{{($checkMember)?(temporyPrivateLogin())?'My Account':'Company Account':'My Account'}}</a></li>@if($member->employee)
                                <li><a href="/employee/{{$member->employee->id}}/edit"><span class=ion-ios-pricetags></span> Company Profile</a> </li>@endif @if(!$checkMember || temporyPrivateLogin())
                                <li><a href="{{asset('')}}my_ads"><span class=ion-ios-pricetags></span> My Ads</a> </li>@endif
                            <li class=divider>
                            <li><a href="{{asset('')}}member_logout"><span class=ion-power></span>Â Â Â Logout</a> </ul>
                    </div>@else <a href=/member-login><span>{{Html::image('images/user_black.png', 'saleme.lk user', array('class'=> 'user-fixed-img'))}}</span> Login / Register</a> @endif </div>
        </div>
    </div>
</div>