@include('main.header')

<section class="leader-board-top hidden-xs">
    <div class="container visible-md visible-lg">
        <div class="image-div">
            <!-- /21634329919/slm_leaderboard_970x90_atf -->
            <div id='div-gpt-ad-1511940698204-1'>
                <script>
                    googletag.cmd.push(function () {
                        googletag.display('div-gpt-ad-1511940698204-1');
                    });
                </script>
            </div>
        </div>
    </div>

</section>
{{--desktop--}}
<section class="inner-search hidden-xs">
    <div class="search-page">
        <div class="container">
            <div class="mar-70 text-center">
                <h1 class="promo-head4">
                    Find Best Deals from Trending Marketplace in Sri Lanka
                </h1>
                <form id="search_form" method="get" action="{{ app('url')->full() }}">
                    <div class="col-xs-12  search-panel2 ">
                        <div class="input-group home-search1">
                            <div class="input-group-btn ">
                                <button type="button" class="btn location-btn " data-toggle="modal"
                                        data-target=".select-city-modal">
                                    <span class="lnr lnr-pointer-down"></span><span
                                            id="">&nbsp;{{!empty($location_slug) ? ucfirst(str_replace('_',' ',$location_slug)) :'Select City'}}</span>
                                </button>
                                <button type="button" class="btn cat-btn" data-toggle="modal"
                                        data-target=".select-category-modal">
                                    @if(!empty($category))
                                        <span class="lnr lnr-tag"></span><span
                                                id="">&nbsp;{{!empty($category) ? $category :'All Categories'}}</span>
                                    @elseif(!empty($scategory))
                                        <span class="lnr lnr-tag"></span><span
                                                id="">&nbsp;{{!empty($scategory) ? $scategory :'All Categories'}}</span>
                                    @else
                                        <span class="lnr lnr-tag"></span><span
                                                id="">&nbsp;{{!empty($scategory) ? $scategory :'All Categories'}}</span>
                                    @endif
                                </button>
                            </div>
                            <input class="form-control search-txt" name="query"
                                   value="@if(!empty($querydata['query'])){{$querydata['query']}}@endif"
                                   placeholder="What you looking for..."
                                   type="text">
                            <span class="input-group-btn">
                            <button class="btn  search-btn" id="more_query" type="button"><span
                                        class="lnr lnr-magnifier"></span></button>
                        </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!--search mobile-->
<section class="inner-search visible-xs">
    <div class="search-page-xs">
        {{--<div class="container">--}}
        {{--<div class="mar-70 text-center row">--}}
        {{--<div class="searchdiv">--}}
        {{--<div class="col-xs-12  search-panel2 ">--}}
        {{--<form id="search_form1" method="GET" action="{{ app('url')->full() }}">--}}
        {{--<div id="custom-search-input">--}}
        {{--<div class="input-group col-md-12">--}}
        {{--<input type="text" class="form-control input-lg" placeholder="What you looking for..."--}}
        {{--name="query"/>--}}
        {{--<span class="input-group-btn">--}}
        {{--<button class="btn btn-info btn-lg" type="button">--}}
        {{--<i class="ion-ios-search-strong"></i>--}}
        {{--</button>--}}
        {{--</span>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</form>--}}
        {{--</div>--}}
        {{--<div class="col-xs-6 pd-r-5-xs">--}}
        {{--<div class="input-group home-search1">--}}
        {{--<div class="input-group-btn ">--}}
        {{--<button type="button" class="btn mob-loc " data-toggle="modal"--}}
        {{--data-target=".select-city-modal">--}}
        {{--<span class="lnr lnr-pointer-down"></span> --}}
        {{--<span id="search_concept">--}}
        {{--<span class="ion-ios-location"></span>--}}
        {{--&nbsp;{{!empty($location_slug) ? ucfirst(str_replace('_',' ',substr($location_slug , 0, 18))) :'Select City'}}</span>--}}

        {{--</button>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-xs-6 pd-l-5-xs">--}}
        {{--<div class="input-group home-search1">--}}
        {{--<div class="input-group-btn ">--}}
        {{--<button type="button" class="btn mob-cat-btn" data-toggle="modal"--}}
        {{--data-target=".select-category-modal">--}}
        {{--@if(!empty($category))--}}
        {{--<span class="lnr lnr-tag"></span>--}}
        {{--<span id="">&nbsp;{{!empty($category) ? substr($category , 0, 18) :'All Categories'}}</span>--}}
        {{--@elseif(!empty($scategory))--}}
        {{--<span class="lnr lnr-tag"></span>--}}
        {{--<span id="">&nbsp;{{!empty($scategory) ? substr($scategory , 0, 18) :'All Categories'}}</span>--}}
        {{--@else--}}
        {{--<span class="lnr lnr-tag"></span>--}}
        {{--<span id="">&nbsp;{{!empty($scategory) ? substr($scategory , 0, 18) :'All Categories'}}</span>--}}
        {{--@endif--}}

        {{--</button>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        <div class=" searchdiv pd-10">
            <div class="row">
                <div class="col-xs-12 ">
                    <form id="search_form1" method="GET" action="{{ app('url')->full() }}">
                        <div id="custom-search-input">
                            <div class="input-group col-md-12">
                                <input type="text" class="form-control input-lg"
                                       placeholder="What you looking for..."
                                       name="query" value="@if(!empty($querydata['query'])){{$querydata['query']}}@endif"/>
                                <span class="input-group-btn">
                                        <button class="btn btn-info btn-lg" type="button" id="more_query1">
                                    <i class="ion-ios-search-strong"></i>
                                        </button>
                                 </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row mar-top-5">
                <div class="col-xs-6 pd-r-5-xs">
                    <div class="input-group home-search1">
                        <div class="input-group-btn ">
                            <button type="button" class="btn mob-loc " data-toggle="modal"
                                    data-target=".select-city-modal">
                                {{--<span class="lnr lnr-pointer-down"></span> --}}
                                <span id="search_concept">
                                    <span class="ion-ios-location"></span>
                                    &nbsp;{{!empty($location_slug) ? ucfirst(str_replace('_',' ',substr($location_slug , 0, 18))) :'Select City'}}</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 pd-l-5-xs">
                    <div class="input-group home-search1">
                        <div class="input-group-btn ">
                            <button type="button" class="btn mob-cat-btn" data-toggle="modal"
                                    data-target=".select-category-modal">
                                @if(!empty($category))
                                    {{--<span class="lnr lnr-tag"></span>--}}
                                    <span id="">&nbsp;{{!empty($category) ? substr($category , 0, 18) :'All Categories'}}</span>
                                @elseif(!empty($scategory))
                                    {{--<span class="lnr lnr-tag"></span>--}}
                                    <span id="">&nbsp;{{!empty($scategory) ? substr($scategory , 0, 18) :'All Categories'}}</span>
                                @else
                                    {{--<span class="lnr lnr-tag"></span>--}}
                                    <span id="">&nbsp;{{!empty($scategory) ? substr($scategory , 0, 18) :'All Categories'}}</span>
                                @endif
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--</div>--}}
    </div>
</section>
