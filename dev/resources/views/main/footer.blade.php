<section class="pin-bg bord-top">
    <div class="container">
        <div class=" hidden-xs"></div>
        <div class="row post-ad-banner-bottom">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                <div class="promo_content text-center">
                    <h3><span class="quick blink_me">Quick</span> Buy, Sell, Rent or Anything - Post your Ad on
                        SaleNow.co.nz</h3>
                </div>
            </div>
        </div>
        <div class=" hidden-xs"></div>
    </div>
</section>
<footer id="footer" class="clearfix">
    <!-- footer-top -->
    <section class="footer-top clearfix">
        <div class="container">
            <div class="row">
                {{--<!-- footer-widget -->--}}
                <div class="col-sm-3">
                    <div class="footer-widget news-letter">
                        <a href="{{URL::to('/')}}">
                            {{ Html::image('images/salenow/footer-img.png', 'SaleNow.co.nz logo', array('class' => 'img-responsive footer-logo')) }}
                        </a>

                        <hr class="visible-xs">
                    </div>
                </div>
                {{--<!-- footer-widget -->--}}
                <div class="col-sm-3">
                    <div class="footer-widget">
                        <h4>Quik Links</h4>
                        <ul>
 			    <li><a href="{{asset('')}}advertise-with-us">Advertise with Us</a></li>
                            <li><a href="{{asset('')}}about">About Us</a></li>
                            <li><a href="{{asset('')}}contact-us">Contact Us</a></li>
                            <li><a href="{{asset('')}}careers">Careers</a></li>
                            <li><a href="{{asset('')}}membership">Membership</a></li>
                        </ul>
                        <hr class="visible-xs">
                    </div>
                </div>
                {{--<!-- footer-widget -->--}}
                <div class="col-sm-3">
                    <div class="footer-widget">
                        <h4>Stay Safe with SaleNow.co.nz</h4>
                        <ul>
                            <li><a href="{{asset('')}}terms-conditions">Terms & Conditions</a></li>
                            <li><a href="{{asset('')}}privacy-policy">Privacy Policy</a></li>
                        </ul>
                        <hr class="visible-xs">
                    </div>
                </div>
                {{--<!-- footer-widget -->--}}
                <div class="col-sm-3">
                    <div class="footer-widget social-widget">
                        <h4>Follow us on</h4>
                        <ul>
                            <li><a href="#"
                                   target="_blank"><i class="fa fa-facebook-official"></i>Facebook</a></li>
                            <li><a href="#" target="_blank"><i
                                            class="fa fa-linkedin-square"></i>Linkedin</a></li>
                            <li><a href="#" target="_blank"><i
                                            class="fa fa-youtube-square"></i>youtube</a></li>
                            <li><a href="#" target="_blank"><i
                                            class="fa fa-twitter-square"></i>Twitter</a></li>
                        </ul>
                        <hr class="visible-xs">
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    {{--<!--/.footer-bottom-->--}}
    <div class="footer-bottom">
        <div class="container">
            <p class="pull-left"> &copy; {{date("Y")}} SaleNow.co.nz (Pvt) Ltd. All right reserved. </p>
        </div>
    </div>
    {{--<!--/.footer-bottom-->--}}
</footer>
{{--<!--back to top-->--}}
<div class="scroll-top-wrapper ">
    <span class="scroll-top-inner">
        <i class="ion-ios-arrow-up"></i>
    </span>
</div>
{{--<!--end back to top-->--}}
<!--default search-bar pop-->
@include('default.popup')
</body>
<!--Send Pulse Script-->
<script charset="UTF-8" src="//cdn.sendpulse.com/28edd3380a1c17cf65b137fe96516659/js/push/a9ed31b7aac599d7dff747b5b262479b_1.js" async></script>
<!--Send Pulse Script-->
<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function() {
            $('.alert').fadeOut('fast')
        }, 6000);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        setTimeout(function() {
            $('.alert').fadeOut('fast')
        }, 6000);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#more_query').click(function() {
            $('#search_form').submit()
        });
        $('#more_query1').click(function() {
            $('#search_form1').submit()
        });

        function toggleIcon(e) {
            $(e.target).prev('.panel-heading').find(".more-less").toggleClass('glyphicon-plus glyphicon-minus')
        }
        $('.panel-group').on('hidden.bs.collapse', toggleIcon);
        $('.panel-group').on('shown.bs.collapse', toggleIcon);
        window.___gcfg = {
            lang: 'en-US',
            parsetags: 'onload'
        };
        $(".callcenterBox").click(function(e) {
            $(this).toggleClass('active')
        })
    })

    function blocbtn(msg = null) {
        $('.blockui').block({
            message: msg
        })
    }

    function unblocbtn() {
        $('.blockui').unblock()
    }
    $('#more_query').click(function() {
        $('#search_form').submit()
    });
    $('#more_query1').click(function() {
        $('#search_form1').submit()
    });

    function toggleIcon(e) {
        $(e.target).prev('.panel-heading').find(".more-less").toggleClass('glyphicon-plus glyphicon-minus')
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
    window.___gcfg = {
        lang: 'en-US',
        parsetags: 'onload'
    };
    $(".callcenterBox").click(function(e) {
        $(this).toggleClass('active')
    });

    $(window).scroll(function() {
        if ($(window).scrollTop() >= 200) {
            $('.navbar').addClass('fixed-header');
            $('.sticky-seachbar').addClass('fixed-search')
        } else {
            $('.navbar').removeClass('fixed-header');
            $('.sticky-seachbar').removeClass('fixed-search')
        }
    });

    function scrollToTop() {
        verticalOffset = "undefined" != typeof verticalOffset ? verticalOffset : 0, element = $("body"), offset = element.offset(), offsetTop = offset.top, $("html, body").animate({
            scrollTop: offsetTop
        }, 500, "linear")
    }

    function toTitleCase(e) {
        return e.replace(/\w\S*/g, function(e) {
            return e.charAt(0).toUpperCase() + e.substr(1).toLowerCase()
        })
    }
    $(document).ready(function(e) {
        $(".search-panel .dropdown-menu").find("a").click(function(e) {
            e.preventDefault();
            var t = $(this).attr("href").replace("#", ""),
                l = $(this).text();
            $(".search-panel span#search_concept").text(l), $(".input-group #search_param").val(t)
        })
    }), $(function() {
        $(document).on("scroll", function() {
            $(window).scrollTop() > 100 ? $(".scroll-top-wrapper").addClass("show") : $(".scroll-top-wrapper").removeClass("show")
        }), $(".scroll-top-wrapper").on("click", scrollToTop)
    }), $(function() {
        $(".search-result li").hide(), $("#search").keyup(function() {
            var e = $("#search").val(),
                t = toTitleCase(e);
            $("#all-srilanka-txt").html(""), "" !== t ? ($(".search-result li").hide(), $(".search-result li").each(function() {
                var l = $(this).text();
                l.indexOf(t) >= 0 ? ($(this).show(), $(".popular-city").hide(), $("#all-srilanka-txt").html('Available Cities for "' + e + '"'), $(".res-div").hide()) : $("#all-srilanka-txt").html('Cities Available for "' + e + '"')
            })) : ($(".popular-city").show(), $(".search-result li").hide(), $("#all-srilanka-txt").html(""), $(".res-div").show())
        }), $(".select-city-modal .close").click(function() {
            $("#search").val(""), $(".popular-city").show(), $(".search-result li").hide()
        })
    }), $(function() {
        $("#login-form-link").click(function(e) {
            $(".loging-bottom").fadeOut(100), $("#login-form").delay(300).fadeIn(100), $(".loging-bottom").delay(1e3).fadeIn(100), $("#register-form").fadeOut(100), $("#register-form-link").removeClass("active"), $(this).addClass("active"), $("#log-reg-title").text("Login into SaleNow.co.nz"), e.preventDefault()
        }), $("#register-form-link").click(function(e) {
            $(".loging-bottom").fadeOut(100), $("#register-form").delay(300).fadeIn(100), $(".loging-bottom").delay(1e3).fadeIn(100), $("#login-form").fadeOut(100), $("#login-form-link").removeClass("active"), $(this).addClass("active"), $("#log-reg-title").text("Register with SaleNow.co.nz"), e.preventDefault()
        })
    });
</script>
{{--<script language="Javascript" src="http://www.bw2018.lk/stats.php?page=2018"></script>--}}
</html>









