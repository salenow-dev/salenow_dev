@extends('layouts.saleme')
@section('title', 'Careers ')
@section('content')
    <!------ Include the above in your HEAD tag ---------->
    @if(!empty(session()->get('msg')))
        <div class="col-md-8">

        </div>
        <div class="alert alert-success alert-dismissible col-md-4">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>{{session()->get('msg')}}</strong>
        </div>
        <div class="col-md-8"></div>

    @endif
    <div class="container">
        <div class="mar-80"></div>
        <div class="mar-80-top"></div>
        <h4>Available Vacancies at SaleMe.lk</h4>
        <h4>Join with our Team , you can help to create a wonderful world. </h4>
        <div class="mar-80-top"></div>
        <div class="row">
            <div class=" col-md-12">

                @if(count($careerlist))
                    <table class="table career-table ">
                        <thead>
                        <tr>
                            <th>Job Title</th>
                            <th>Experience level</th>
                            <th class="text-right">Action</th>
                        </tr>
                        </thead>
                        @foreach($careerlist as $career)
                            <tr>
                                <td>{{$career->title}}</td>
                                <td>{{$career->experiencelevel}}</td>
                                <td>
                                    <form id="apply"
                                          action="/careers/apply"
                                          method="post">
                                        {{csrf_field()}}
                                        <input type="hidden" name="jobno" value={{$career->id}}>
                                        <button type="submit" name="apply" class="btn btn-default btn-aaply">
                                            Apply
                                        </button>
                                    </form>
                                </td>

                            </tr>
                        @endforeach
                    </table>
                @else
                    <p>No More Vacancies </p>

                @endif
                <div class="mar-80-top"></div>
                <div class="mar-80-top"></div>

            </div>
        </div>
    </div>

@endsection