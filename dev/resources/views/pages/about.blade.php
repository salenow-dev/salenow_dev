@extends('layouts.saleme')
@section('title', 'About Us')
@section('content')
    <div class="container cont-bg">
        {{--<div class="mar-70 text-center">--}}
        {{--<br>--}}
        {{--<h1 class="promo-head2">Welcome to saleme.lk Help & Support page</h1>--}}
        {{--<p class="text-center promo-sub-1">Here you can find answers to our frequently asked questions and learn--}}
        {{--about saleme.lk , how to use it, how to stay safe and get in touch with us. </p>--}}

        {{--</div>--}}
        <div class="top-mar-100"></div>
        @include('pages.side-nav')
        <div class="col-md-8">
            <section>

                <h2>About Us</h2>
                <div class="top-mar-30"></div>

                <p>
                    We are <B>SaleMe.lk</B> is creating successful connections between buyers and sellers
                    online in Sri Lanka and Sri Lankans who are now in overseas. Our aim is to empower
                    every person in Sri Lanka to independently connect with buyers and sellers online
                    and have a better transaction closer to their dreams. </p>

                    <p> We  introduce  brand  new  concept  which  is  first  time  in  Sri  Lanka.  Through
                    SaleMe.lk we offer web site with the control panel which mean they can control
                    their web page with out any assistance.  We use latest technology to develop the
                    site in-house.Our targeted audience is each and every person in Sri Lanka and Sri
                    Lankans who are in overseas.
                </p>
                <div class="top-mar-30"></div>
                <h4>Vision</h4>
                <div class="top-mar-30"></div>

                <p>
                    To become the worlds’ preferred and the finest free classified platform.
                </p>
                <div class="top-mar-30"></div>


                <h4>Mission</h4>
                <div class="top-mar-30"></div>

                <p>
                    We are committed to ourselves to create an innovative, the best quality, the quickest, secured and
                    the
                    largest satisfied customer based free classified platform to buy & sell with no boundaries.
                </p>
                <div class="top-mar-30"></div>

                <h4>Objectives</h4>
                <div class="top-mar-30"></div>

                <p>
                    Our dedicated professional team continuously focuses on new technology and development of the
                    product
                    in order to
                </p>
                <li><strong>Buy & Sell</strong> articles through online advertising
                </li>
                <br>
                <li><strong>Provide services</strong> to local and foreign institutions by way of Business Process
                    Outsourcing (BPO)
                </li>
                <br>
                <li><strong>Provide and assist with professional services</strong> through online
                </li>
		<div class="top-mar-30"></div>

            </section>

        </div>
    </div>
@endsection