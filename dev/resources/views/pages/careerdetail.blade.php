@extends('layouts.saleme')
@section('title', 'Careers ')
@section('content')
    <!------ Include the above in your HEAD tag ---------->
    <div class="mar-80"></div>

    <div class="container">
        <div class="row">
            @foreach($careerlist as $career)

            @endforeach
            <h3>  {{$career->title}}</h3>
            <div class="col-md-7 div-style">
                <ul class="list-group list-style">
                    <li class="list-group-item"> Job Role :<b>{{$career->position}}</b></li>
                    <li class="list-group-item">Experience level :<b>{{$career->experiencelevel}}</b></li>
                    <span class="label  l-date">Closing Date : {{$career->date}}</span>
                    @if($career->salary =="")
                        <li class="list-group-item">Sallary :<b>LKR : Negotiable</b></li>
                    @else
                        <li class="list-group-item">Sallary :<b>LKR {{$career->salary}}</b></li>
                    @endif


                </ul>
                <h4 class="head-1"><span class="ion-clipboard i-icon-styles"></span> Job Description</h4>
                <p class="job-dis">
                    {!! $career->description !!}
                </p>
                <div class="row"></div>
                <h4><span class="ion-person i-icon-styles"></span> Job Requirements</h4>
                <p class="job-dis">{!! $career->requirements !!}</p>
                <div class="row"></div>
                <h4><span class="ion-star i-icon-styles"></span>Job Benifits</h4>
                <p class="job-dis">{!! $career->benefits !!}</p>
                <div class="row"></div>
                <p class="job-dis"><b>* Apply online or you can mail us your CV and cover letter to careers@saleme
                        .lk</b></p>
            </div>
            <div class="col-md-5">
                <div class="panel panel-style-career panel-default ">
                    <h2>Apply this Job</h2>
                    <form class="form-horizontal" action="/career/applyform" method="post" enctype="multipart/form-data"
                          id="career-form" onsubmit="return Validate(this);" name="carr">
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="lable_cstm" for="email">First Name:</label>
                                <input type="text" class="form-control" placeholder="First Name" name="first_name"
                                       required
                                >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="lable_cstm  " for="email">Last Name:</label>
                                <input type="text" class="form-control" placeholder="Last Name" name="last_name"
                                       required
                                >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="lable_cstm" for="pwd">Posistion:</label>
                                <input type="text" class="form-control"
                                       value="{{$career->position}}" disabled>
                                <input type="hidden" class="form-control"
                                       value="{{$career->position}}" name="position">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="lable_cstm" for="pwd">Gender:</label>
                                <select class="form-control" name="gender">
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="lable_cstm" for="pwd">Age:</label>
                                <input type="text" class="form-control" id="birthday" name="birthday" placeholder=" 20"
                                       required size="2">
                                <p id="b_validate" style="color: red;"></p>
                                <p id="b_validate2" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label class="lable_cstm" for="pwd">Marital Status:</label>
                                <select class="form-control" name="marital_status">
                                    <option value="single">Single</option>
                                    <option value="married">Married</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12"><label class="lable_cstm" for="email">Notification Period</label>
                            </div>
                            <div class="col-sm-6">
                                <select class="form-control" name="date">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <select class="form-control" name="month">
                                    <option value="Days">Days</option>
                                    <option value="Weeks">Weeks</option>
                                    <option value="Months">Months</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12"><label class="lable_cstm" for="pwd">Attache your CV:</label></div>
                            <div class="col-sm-10">
                                <input type="file" name="attachment" data-bv-field="cv" id="cv" class="cv" required>
                            </div>
                        </div>
                        <div id="cv-alert" class="cv-alert" style="color: red">Allow Types PDF,DOCX,DOC</div>
                        <div id="cv-alert1" class="cv-alert1" style="color: red">Max file Size 2MB</div>

                        <div class="form-group">
                            <div class=" col-sm-10">
                                <button type="submit" class="btn btn-primary" id="submit">Apply Online</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>

        var _validFileExtensions = [".pdf", ".docx", ".doc"];
        function Validate(oForm) {
            var arrInputs = oForm.getElementsByTagName("input");
            var birthday = document.getElementsByName("birthday")[0].value;
            var num = parseInt(birthday);

            if (num > 80) {
                $('#b_validate').html('You are too Old');
                document.getElementById("birthday").focus();
                return false;
            }
            if (isNaN(num)) {
                $('#b_validate2').html('Please Enter a Valid Age.Text are Not Allowed');
                return false;
            }
            var file_size = $('#cv')[0].files[0].size;
            if (file_size > 2097152) {
                $("#cv-alert1").html("File size is greater than 2MB");
                return false;
            }
            for (var i = 0; i < arrInputs.length; i++) {
                var oInput = arrInputs[i];
                if (oInput.type == "file") {
                    var sFileName = oInput.value;
                    if (sFileName.length > 0) {
                        var blnValid = false;
                        for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                blnValid = true;
                                break;
                            }
                        }
                        if (!blnValid) {
                            $('.cv-alert').html("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(","));
                            return false;
                        }
                    }
                }
            }

            return true;
        }


    </script>
@endsection