@include('main.header')
<!--slider start-->
<div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel">
    <!-- Overlay -->
    <div class="overlay"></div>

    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
        <li data-target="#bs-carousel" data-slide-to="1"></li>
        <li data-target="#bs-carousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item slides active">
            <div class="slide-1">
                <img src="{{asset('')}}images/salenow/banner.jpg" class="img-responsive">
            </div>
            <div class="hero">
            </div>
        </div>
        <div class="item slides">
            <div class="slide-2">
                <img src="{{asset('')}}images/salenow/banner1.jpg" class="img-responsive">
            </div>
            <div class="hero">
                <!--        <hgroup>
                            <h1>We are smart</h1>
                            <h3>Get start your next awesome project</h3>
                        </hgroup>
                        <button class="btn btn-hero btn-lg" role="button">See all features</button>-->
            </div>
        </div>
        <div class="item slides">
            <div class="slide-3">
                <img src="{{asset('')}}images/salenow/banner.jpg" class="img-responsive">
            </div>
            <div class="hero">


            </div>
        </div>
    </div>
</div>
<!--slider end-->
<div class="clear-fix"></div>
<div class="adds-intro">
    <div class=" mar-80">
        <h3 class="adds-head">EXPLORE THE POWER OF SALEME ADS PLATFORM</h3>
        <br>
        <div class="container ">
            <p class="adds-txt">
                Consumers don’t come to Saleme.lk for entertainment or news. They come to us with a very precise intent
                – to either buy, sell or find something – across a wide range of categories including cars, homes,
                mobiles, furniture, jobs and various services. <br>
                Armed with this powerful information, brands can target audiences
                with high intent and purchasing power and <br>ensure the maximum impact for their campaigns.
                <br>
                {{--<input name="Submit" type="submit" class="btn btn-warning btn-lg" data-toggle="modal" data-target=".ad-inquiry" value="Enquire Now">--}}
            </p>
        </div>
    </div>
</div>
<div class="clear-fix"></div>
<section class="qap-section ">
    <h3 class="">BUY WHAT YOU NEED | SELL WHAT YOU WANT</h3>
</section>
<div class="clear-fix"></div>
<section>
    <div class="one">
        <div class="container">
            <div class="col-md-4">
                <br><br><br>
                <div class="adsale-h4">
                    <h5 class="h3-group">Target audiences who have high purchase intent on our platform.</h5><br>
                    <h5 class="h3-group">Always stay top of mind with our Cookie Pool Retargeting on and outside
                        Saleme.</h5><br>
                    <h5 class="h3-group">Cut through clutter with innovations for your ad on our platform.</h5><br>
                </div>
            </div>
            <div class="col-md-4">
                <br> <br><br><br>
                <img src="{{asset('')}}images/salenow/adsale-imag-01.png" class="img-responsive ">
            </div>
            <div class="col-md-4">
                <br><br><br>
                <div class="adsale-h4">
                    <h5 class="h3-group">Reach affinity audiences across 180+ categories with our cross category
                        targeting.</h5><br>
                    <h5 class="h3-group">Smarter ways to reach out to relevant audiences with our Price, Competitor,
                        Hyperlocal Search based targeting.</h5><br>
                    <h5 class="h3-group">Your ad reaches a larger pool of audiences bypassing Ad Blockers only on
                        Saleme.</h5><br>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="clear-fix"></div>
<center>
    <section class="qap-section ">
        <h3 class="">We Are Coverd</h3>
    </section>
</center>
<br><br><br>

<div class="container">
    <div class="row">
        <div class="col-md-3 col-sm-6">
            <div class="progress blue">
                    <span class="progress-left">
                        <span class="progress-bar"></span>
                    </span>
                <span class="progress-right">
                        <span class="progress-bar"></span>
                    </span>
                <div class="progress-value"><i class="ion-location pro-font"></i></div>

            </div>
            <center><h2>Cities</h2>
                <p>1250 cities are coverd</p>
            </center>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="progress yellow">
                    <span class="progress-left">
                        <span class="progress-bar"></span>
                    </span>
                <span class="progress-right">
                        <span class="progress-bar"></span>
                    </span>
                <div class="progress-value"><i class="ion-arrow-graph-up-right pro-font"></i></div>

            </div>
            <center><h2>Market</h2>
                <p>75% market growth</p>
            </center>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="progress pink">
                <span class="progress-left">
                    <span class="progress-bar"></span>
                </span>
                <span class="progress-right">
                    <span class="progress-bar"></span>
                </span>
                <div class="progress-value"><i class="ion-ios-briefcase pro-font"></i></div>

            </div>
            <center><h2>Products</h2>
                <p>150+ product catogories</p>
            </center>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="progress green">
                <span class="progress-left">
                    <span class="progress-bar"></span>
                </span>
                <span class="progress-right">
                    <span class="progress-bar"></span>
                </span>
                <div class="progress-value"><i class="ion-pricetags pro-font"></i></div>

            </div>
            <center><h2>Deals</h2>
                <p>1000 deals per month</p>
            </center>
        </div>

    </div>
</div>

<br><br>
<div class="clear-fix"></div>
<div class="two">
    <div class="container ">
        <div class="row">


            <div class="col-md-6">
                <br>
                <h2 class="desktop-head">Sign up Saleme.lk Free</h2><br>
                <p class="desktop-para">
                    180+ Categories
                    Infinite Opportunities.Whether it is buying a new car or selling a pre-owned TV, searching for a
                    plumber or finding a job, getting information on Education or buying a new house – we are helping
                    our customers simplify their lives every single day</p>

            </div>

            <div class="col-md-6">
                <br>
                <img src="{{asset('')}}images/salenow/apple-desktop.gif" class="img-responsive adsale-img">

            </div>


        </div>
    </div>
</div>
<br>
{{--<!---Logo Slider -->--}}
{{--<div class="logo-slider">--}}
    {{--<div class="container">--}}
        {{--<center>--}}
            {{--<section class="qap-section ">--}}
                {{--<h3 class="">OUR TOP CLIENTS</h3>--}}
            {{--</section>--}}
        {{--</center>--}}
        {{--<ul class="bxslider">--}}
            {{--<li class="li-slider "><img src="assets/img/topclient/client-1-01.png"/></li>--}}
            {{--<li class="li-slider "><img src="assets/img/topclient/client-1-02-01.png"/></li>--}}
            {{--<li class="li-slider "><img src="assets/img/topclient/client-1-03-01.png"/></li>--}}
            {{--<li class="li-slider "><img src="assets/img/topclient/audi-01.png"/></li>--}}
            {{--<li class="li-slider "><img src="assets/img/topclient/client-1-01.png"/></li>--}}
            {{--<li class="li-slider "><img src="assets/img/topclient/addidas-01.png"/></li>--}}
            {{--<li class="li-slider "><img src="assets/img/topclient/client-1-02-01.png"/></li>--}}
            {{--<li class="li-slider "><img src="assets/img/topclient/client-1-03-01.png"/></li>--}}
        {{--</ul>--}}
    {{--</div>--}}
{{--</div>--}}
{{--<!--Testomonials -->--}}

@include('main.footer')