@extends('layouts.saleme')
@section('title', 'Privacy Policy')
@section('content')
<div class="container cont-bg">
    <div class="top-mar-100"></div>
    @include('pages.side-nav')
    <div class="col-md-8">
        <section>
            <h2>Privacy Policy</h2>
            <div class="top-mar-30"></div>
            <p>
                In order to provide a better service, it is important for Saleme.lk to collect, use, and share personal
                information.
            </p>
            <div class="top-mar-30"></div>
            <h4>Collection of Information</h4>
            <div class="top-mar-30"></div>
            <p>
                Upon submission of the following information to Saleme.lk website you are expressly consenting to
                transfer and storage of such information in Saleme.lks’ database.
            </p>
            <ul>
                <li>- Email address, contact information, Computer sign-on data, statistics on page views, traffic to
                    and
                    from Saleme.lk and response to advertisements
                </li>
                <li>- Other information, including users' IP address and standard web log information.</li>
            </ul>
            <div class="top-mar-30"></div>
            <h4>Use of Information</h4>
            <div class="top-mar-30"></div>
            <p>
                We use users' personal information to:
            </p>
            <ul>
                <li>- Provide our services</li>
                </li>
                <li>- Encourage safe trading and enforce our policies</li>
                <li>- Customize users experience, measure interest in our services</li>
                <li>- Improve our services and inform users about services and updates</li>
                <li>- Communicate marketing and promotional offers to you according to your preferences</li>
                <li>- Do other things for users as described when we collect the information</li>
            </ul>
            <div class="top-mar-30"></div>
            <h4>Disclosure</h4>
            <div class="top-mar-30"></div>
            <p>
                We will not sell or rent users' personal information to third parties for their marketing purposes
                without users' explicit consent. We may disclose personal information to respond to legal requirements,
                enforce our policies, respond to claims that a posting or other content violates other's rights, or
                protect anyone's rights, property, or safety.
            </p>
            <div class="top-mar-30"></div>
            <h4>Communication and email tools</h4>
            <div class="top-mar-30"></div>
            <p>
                You agree to receive marketing communications on behalf of our third party advertising partners. You may
                not use our site or communication tools to harvest addresses, send spam or otherwise breach our Terms of
                Use or Privacy Policy. If you use our tools to send content to a friend, we don't permanently store your
                friends' addresses or use or disclose them for marketing purposes. To report spam from other users,
                please contact customer support services.
            </p>
            <div class="top-mar-30"></div>
            <h4>Security</h4>
            <div class="top-mar-30"></div>
            <p>
                We have used all the possible tools (encryption, passwords, and physical security) to protect the
                personal information against unauthorized access and use them.
            </p>
            <p>
                All personal electronic details are kept safely except for those that you wish to disclose. You are
                violating the laws, rules & regulations and guilty of an offence as per this agreement, if you disclose
                others information through the Service.
            </p>
            <p>
                This website makes use of Display Advertising, and uses Remarketing technology with Google Analytics to
                advertise online. Third-party vendors, including Google, may show our ads on various websites across the
                Internet, using first-party cookies and third-party cookies together to inform, optimize, and serve ads
                based on past visits to our website.
            </p>
            <p>
                Visitors can opt-out of Google Analytics for Display Advertising and customize Google Display Network
                ads using the Ads Preferences Manager.
            </p>

            <div class="top-mar-30"></div>


        </section>

    </div>
</div>
@endsection