@include('main.header')


<div class="container cont-bg">
    {{--<div class="mar-70 text-center">--}}
    {{--<br>--}}
    {{--<h1 class="promo-head2">Welcome to saleme.lk Help & Support page</h1>--}}
    {{--<p class="text-center promo-sub-1">Here you can find answers to our frequently asked questions and learn--}}
    {{--about saleme.lk , how to use it, how to stay safe and get in touch with us. </p>--}}

    {{--</div>--}}
    <div class="top-mar-100"></div>
    <section>
        <div class="col-md-3">
            <div class="">

                <ul>
                    <li class="list-group-item">
                        <a href="#"> About us </a>
                        </l>
                    {{--<li class="list-group-item">--}}
                    {{--<a href="#"> FAQ </a>--}}
                    {{--</l>--}}

                    {{--<li class="list-group-item">--}}
                    {{--<a href="#"> How to sell fast </a>--}}
                    {{--</l>--}}

                    {{--<li class="list-group-item">--}}
                    {{--<a href="#"> Stay safe on saleme.lk </a>--}}
                    {{--</l>--}}
                    <li class="list-group-item">
                        <a href="https://saleme.lk"> Contact us </a>
                        </l>

                </ul>
            </div>
        </div>

    </section>
    <div class="col-md-8">
        <section>
            <h3>Stay safe on saleme.lk</h3>
            <p>At saleme.lk we are 100% committed to making sure that your experience on our site is as safe as
                possible.
                Here you can find advice on how to stay safe while trading on saleme.lk.</p><br>
            <div>

                <i class="fa fa-shield" aria-hidden="true" style="font-size:60px ;color:#fea502; "></i>


                <h3 class="">General safety advice.</h3>
                <li><strong>Keep things local.</strong> Meet the seller in person, check the item and make sure you are
                    satisfied with it before you make a payment. Where available, use Buy Now and have items delivered
                    to you directly.
                </li>
                <br>
                <li><strong>Exchange item and payment at the same time.</strong> Buyers – don't make any payments before
                    receiving an item. Sellers – don't send an item before receiving payment.
                </li>
                <br>
                <li><strong>Use common sense.</strong>Avoid anything that appears too good to be true, such as
                    unrealistically low prices and promises of quick money.
                </li>
                <br>
                <li><strong>Never give out financial information.</strong>This includes bank account details,
                    eBay/PayPal info, and any other information that could be misused.
                </li>
            </div>
            <br><br>

            <div>
                <i class="fa fa-exclamation-triangle" aria-hidden="true" style="font-size:60px ;color:#fea502; "></i>


                <h3 class="">Scams and frauds to watch out for</h3>

                <li><strong>Fake payment services.</strong> saleme.lk does not offer any form of payment scheme or
                    protection. Please report any emails claiming to offer such services. Avoid using online payment
                    services or escrow sites unless you are 100% sure that they are genuine.
                </li>
                <br>

                <li><strong>Fake information requests.</strong> saleme.lk never sends emails requesting your personal
                    details. If you receive an email asking
                </li>
                <br>

                <li><strong>Fake fee requests</strong>Avoid anyone that ask for extra fees to buy or sell an item or
                    service. saleme.lk never requests payments for its basic services, and doesn't allow items that are
                    not located in Sri Lanka, so import and brokerage fees should never be required.
                </li>
                <br>

                <li><strong>Requests to use money transfer services such as Western Union or MoneyGram.</strong>These
                    services are not meant for transactions between strangers and many scams are run through them. Avoid
                    requests to use these services.
                </li>
                <br>
            </div>
            <br><br>

            <div>
                <i class="fa fa-user-secret" aria-hidden="true" style="font-size:60px ;color:#fea502; "></i>


                <h3 class="">saleme.lk's safety measures</h3>

                <p>We work continuously to ensure you have a safe, enjoyable experience on saleme.lk.</p>
                <p>Our safety measures include:</p>

                <li>Hiding your email address on ads you post to protect you from spam.</li>
                <br>

                <li>Giving you the option to hide your phone number on ads you post to protect you from spam.</li>
                <br>

                <li>Making constant improvements to our technology to detect and prevent suspicious or inappropriate
                    activity behind the scenes.
                </li>
                <br>

                <li>Tracking reports of suspicious or illegal activity to prevent offenders from using the site again.
                </li>
                <br>
            </div>
            <br>

            <div>
                <i class="fa fa-comments" aria-hidden="true" style="font-size:60px ;color:#fea502; "></i>


                <h3 class="">Reporting a safety issue</h3>

                <p>If you feel that you have been the victim of a scam, please report your situation to us immediately.
                    If you have been cheated, we also recommend that you contact your local police department.</p>

                <p>saleme.lk is committed to ensuring the privacy of its users and therefore does not share information
                    about its users publicly. However, we are committed to the safety of our users and will cooperate
                    with the police department should we receive any requests for information in connection with
                    fraudulent or other criminal activity.</p>


            </div>
        </section>

    </div>
</div>


@include('main.footer')