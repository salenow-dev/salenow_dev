@extends('layouts.saleme')
@section('title', 'Terms & Conditions')
@section('content')
<div class="container cont-bg">
    {{--<div class="mar-70 text-center">--}}
    {{--<br>--}}
    {{--<h1 class="promo-head2">Welcome to saleme.lk Help & Support page</h1>--}}
    {{--<p class="text-center promo-sub-1">Here you can find answers to our frequently asked questions and learn--}}
    {{--about saleme.lk , how to use it, how to stay safe and get in touch with us. </p>--}}

    {{--</div>--}}
    <div class="top-mar-100"></div>
    @include('pages.side-nav')
    <div class="col-md-8">
        <section>
            <h2>Terms & Conditions</h2>
            <div class="top-mar-30"></div>
            <p>
                Please read the Terms & Conditions before using this website. The site holds absolute right of changes,
                modifications, additions and removals of Terms & Conditions at any time. Prior notice will not be
                provided for changes of any Terms & Conditions in this agreement.
            </p>
            <div class="top-mar-30"></div>
            <h4>Copyright</h4>
            <div class="top-mar-30"></div>
            <p>
                Advertisers grant Saleme.lk a perpetual, royalty-free, irrevocable, non-exclusive right and license to
                use, reproduce, modify, adapt, publish, translate, create derivative works from and distribute such
                content into any form, medium or technology as we use now or updated later
            </p>
            <p>
                The material (including the Content, and any other content, software or services) contained on Saleme.lk
                are the property of its owners, its subsidiaries, affiliates and/or third party licensors. Any
                intellectual property rights, such as copyright, trademarks, service marks, trade names and other
                distinguishing brands on the Web Site are the property of Saleme.lk. No material on this site may be
                copied, reproduced, republished, installed, posted, transmitted, stored or distributed without written
                consent from Saleme.lk.
            </p>
            <div class="top-mar-30"></div>
            <h4>Watermarks</h4>
            <div class="top-mar-30"></div>
            <p>
                All images on Saleme.lk will be watermarked to prevent the images to be used for other purposes, without
                the consent of the advertiser.
            </p>
            <div class="top-mar-30"></div>
            <h4>Safety & Images</h4>
            <div class="top-mar-30"></div>

            <p> Saleme.lk reserves the right to change the title of the content at any time for the advancement of the
                site and reserves the right not to publish images that are irrelevant or images that violate Saleme.lk’s
                rules. </p>
            <div class="top-mar-30"></div>

            <h4>Privacy</h4>
            <div class="top-mar-30"></div>
            <p>
                Saleme.lk is expressly authorized by the users and advertisers to disclose the information collected
                from such parties to the Affiliate companies and internal administrative purposes.
            </p>
            <div class="top-mar-30"></div>

            <h4>Email address of the users</h4>
            <div class="top-mar-30"></div>
            <p>
                It is a mandatory to submit a valid email address prior to post an advertisement. The email addresses so
                submitted are not publicly displayed in the site and other users may send emails only through Saleme.lk.
                and vice versa
            </p>
            <div class="top-mar-30"></div>
            <h4>Site availability</h4>
            <div class="top-mar-30"></div>
            <p>
                Saleme.lk is held no responsibility of the access to the website due to unavoidable circumstances arise.
                The website will be available “as is” and when only.
            </p>
            <div class="top-mar-30"></div>
            <h4>Cookies</h4>
            <div class="top-mar-30"></div>
            <p>
                Saleme.lk uses cookies, therefore you may have cookies enabled on your computer in order for all the
                functions on the site to work on a proper manner.
            </p>
            <div class="top-mar-30"></div>
            <h4>Third party websites links and services</h4>
            <div class="top-mar-30"></div>
            <p>
                Saleme.lk may contain links or references to third party websites. Saleme.lk will not be responsible for
                the contents in the third party websites which are not investigated or monitored. When the user decides
                to leave Saleme.lk and access a third party website, the user may do so at his/her own risk.
            </p>
            <p>
                Advertisers and users may be required to transmit paid content information through a third party service
                provider, which may be governed by its own terms of use. Linking to any third party service providers is
                at the users’ and advertisers’ own risk and Saleme.lk disclaims all liability related thereto. Saleme.lk
                will not be obliged to refund any payments made in respect to paid content under no circumstances.
            </p>
            <div class="top-mar-30"></div>
            <h4>Membership</h4>
            <div class="top-mar-30"></div>
            <p>
                As a part of a membership Saleme.lk creates a shop on behalf of the user. The shop is a dedicated place
                on Saleme.lk which the user has to maintain. However Saleme.lk reserves the right to remove or not to
                publish any content or part threroff if it does not comply with the Terms & Conditions.
            </p>
            <div class="top-mar-30"></div>
            <h4>General</h4>
            <div class="top-mar-30"></div>
            <p>
                Advertisers and users are responsible for ensuring that advertising content, text, images, graphics and
                video ("Content") uploaded for inclusion on saleme.lk complies with all applicable laws governed.
                Saleme.lk holds no responsibility for any illegality or any inaccuracy of the content.
            </p>
            <p>The advertisers and users guarantee that their content does not violate any copyright, intellectual
                property rights or other rights of any person or entity and agree to release Saleme.lk from all the
                obligations, liabilities and claims arising in connection with the use of the service.</p>
            <p>Saleme.lk takes no responsibility in the content of the advertisement, the products or services being
                sold through the advertisements or actual transactions through the buyers and sellers though the
                advertisements. Therefore Saleme.lk takes no responsibility on the quality, safety, truth, accuracy or
                legality of the products offered. </p>
            <p>Saleme.lk reserves the right to cooperate with authorities in case of any content violates the law. The
                identity of advertisers, users or buyers may be determined. </p>
            <p>Advertisers are allowed to post the advertisements in Saleme.lk website for free of charge. The users
                shall use the website also for free and Saleme.lk does not hold any responsibility of any payment made
                for a third party in this regard. </p>
            <div class="top-mar-30"></div>
            <h4>Disclaimer</h4>
            <div class="top-mar-30"></div>
            <p>
                Saleme.lk holds no responsibility what so ever for the use of Saleme.lk and disclaims all responsibility
                for any injury, claim, liability, or damage of any kind resulting from or arising out of or any way
                related to (a) any errors on Saleme.lk or the Content, including but not limited to technical errors and
                typographical errors, (b) any third party web sites or content directly or indirectly accessed through
                links in Saleme.lk, (c) the unavailability of Saleme.lk, (d) your use of Saleme.lk or the Content, or
                (e) your use of any equipment (or software) in connection with Saleme.lk
            </p>
            <div class="top-mar-30"></div>
            <h4>Indemnification</h4>
            <div class="top-mar-30"></div>
            <p>
                Advertisers and users agree to indemnify Saleme.lk as well as its officers, directors, employees,
                agents, from and against all losses, expenses, damages and costs, including attorney's fees, resulting
                from any violation of this Terms and Conditions (including negligent or wrongful conduct).
            </p>
            <div class="top-mar-30"></div>
            <h4>Modification</h4>
            <div class="top-mar-30"></div>
            <p>
                Saleme.lk reserves the right to modify these Terms and Conditions. Such modifications will be effective
                immediately upon posting on Saleme.lk. You are responsible for the reviewing of such modifications. Your
                continued access or use of Saleme.lk shall be deemed your acceptance of the modified terms and
                conditions.
            </p>
            <div class="top-mar-30"></div>
            <h4>Applicable law</h4>
            <div class="top-mar-30"></div>
            <p>
                Saleme.lk is operated under the laws and regulations of Sri Lanka. The courts of Sri Lanka will have the
                jurisdiction over any dispute or claim relating to the use of Saleme.lk.
            </p>
            <div class="top-mar-30"></div>
        </section>
    </div>
</div>
@endsection