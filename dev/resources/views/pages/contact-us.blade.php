@extends('layouts.saleme')
@section('title', 'Contact Us')
@section('content')
    <div class="container cont-bg">
        {{--<div class="mar-70 text-center">--}}
        {{--<br>--}}
        {{--<h1 class="promo-head2">Welcome to saleme.lk Help & Support page</h1>--}}
        {{--<p class="text-center promo-sub-1">Here you can find answers to our frequently asked questions and learn--}}
        {{--about saleme.lk , how to use it, how to stay safe and get in touch with us. </p>--}}

        {{--</div>--}}
        <div class="top-mar-100"></div>
        @include('pages.side-nav')
        <div class="col-md-8">
            <section>
                <h3 class="margin-0">Get in touch with Saleme.lk</h3>
                <div class="second-portion">
                    <div class="row">
                        <!-- Boxes de Acoes -->
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <div class="box">
                                <div class="icon">
                                    <div class="image"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                                    <div class="info">
                                        <h3 class="title">E-mail</h3>
                                        <p>
                                            <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp; support@saleme.lk

                                        </p>

                                    </div>
                                </div>
                                <div class="space"></div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <div class="box">
                                <div class="icon">
                                    <div class="image"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                    <div class="info">
                                        <h3 class="title">Hot Line</h3>
                                        <p>
                                            <i class="fa fa-mobile" aria-hidden="true"></i> &nbsp (+94)-115 81 81 88

                                        </p>

                                    </div>
                                </div>
                                <div class="space"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear-fix"></div>
                {{--<div class="col-md-12 ">--}}
                {{--<div class="row">--}}
                {{--<h4 class="margin-0">Send us a message</h4>--}}
                {{--</div>--}}
                {{--<form role="form" action="#" method="post">--}}
                {{--<div class="form-group">--}}
                {{--<div class="row">--}}
                {{--<div class="col-md-12 pd-r-0 pd-l-0">--}}
                {{--<input type="text" class="form-control input-lg" placeholder="Your Name" required=""--}}
                {{--name="isim" id="isim" value="">--}}
                {{--</div>--}}

                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                {{--<div class="row">--}}
                {{--<div class="col-md-12 pd-r-0 pd-l-0">--}}
                {{--<input type="text" class="form-control input-lg" placeholder="Your Email" required=""--}}
                {{--name="soyisim" id="soyisim" value="">--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                {{--<div class="row">--}}
                {{--<div class="col-md-6 pd-r-5 pd-l-0">--}}
                {{--<input type="email" name="email" placeholder="Phone Number" required=""--}}
                {{--class="form-control input-lg" id="email" value="">--}}
                {{--</div>--}}
                {{--<div class="col-md-6 pd-l-5 pd-r-0">--}}
                {{--<input type="tel" name="tel" placeholder="Subject" required=""--}}
                {{--class="form-control input-lg" id="tel" value="">--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                {{--<div class="row">--}}
                {{--<div class="col-md-12 pd-r-0 pd-l-0">--}}
                {{--<textarea name="mesaj" class="form-control" rows="8" cols="80"--}}
                {{--placeholder="Message"></textarea>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                {{--<div class="row">--}}
                {{--<div class="col-md-12 pd-r-0 pd-l-0">--}}
                {{--<button type="submit" class="btn btn-gonder2 btn-lg " name="gonder">Send Message</button>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--</form>--}}

                {{--</div>--}}

                <form name="submit-to-google-sheet" id="form">
                    <input name="email" type="email" placeholder="Email" required>
                    <button type="submit">Send</button>
                </form>

                <script>
                    const scriptURL = 'https://script.google.com/macros/s/AKfycbxAOcpX8ELZEGg8vCTiLd00wz5uJeUPCh56E9QeBzbjwsRRxuo/exec'
                    const form = document.forms['submit-to-google-sheet'];

                    $('#form').submit(function(e){
                        e.preventDefault();
                        $.ajax({
                            url: scriptURL,
                            data:$('#form').serialize(),
                            method:'GET',
                        }).done(function() {
                            $( this ).addClass( "done" );
                        });

                    })
                </script>
                <div class="clear-fix"></div>
            </section>
            <BR>
            <BR>
            <div class="clear-fix"></div>
        </div>
    </div>
@endsection