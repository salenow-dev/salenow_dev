@extends('main.saleme-frontend')
@section('title', 'FAQ')
@section('frontend_content')
    <div class="container cont-bg">
        <div class="top-mar-50"></div>
        @include('pages.side-nav')
        <div class="col-md-8">
            <h2>What you want to Know about SaleMe.lk </h2>
            <div class="top-mar-20"></div>
            <div class="panel-group" id="accordion">
                <div class="panel">
                    <div class="panel-heading" id="#coll">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" id="#collapse1" href="#collapse1"><i
                                        class="fa fa-plus" aria-hidden="true"></i> What is SaleMe.lk</a>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse in">
                        <div class="panel-body"><p>
                                We are <B>SaleMe.lk</B> is creating successful connections between buyers and sellers
                                online in Sri Lanka and Sri Lankans who are now in overseas. Our aim is to empower
                                every person in Sri Lanka to independently connect with buyers and sellers online
                                and have a better transaction closer to their dreams. </p>

                            <p> We introduce brand new concept which is first time in Sri Lanka. Through
                                SaleMe.lk we offer web site with the control panel which mean they can control
                                their web page with out any assistance. We use latest technology to develop the
                                site in-house.Our targeted audience is each and every person in Sri Lanka and Sri
                                Lankans who are in overseas.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="panel ">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><i class="fa fa-plus"
                                                                                                    aria-hidden="true"></i>
                                How post an Ad on SaleMe.lk</a>
                        </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p>If you are not a member on SaleMe.lk your have to register as a member on SaleMe.lk
                            you can register on SaleMe.lk by your,</p>
                            <ol>
                                <li>E - mail</li>
                                <li>Facebook Account</li>
                                <li>Google Account </li>
                            </ol>
                            <p>If you are already a member on SaleMe.lk, firstly you have to <a href="http://saleme.ca/member-login" class="active">log on</a> SaleMe.lk by using
                                your E mail and Password or your can log on by your Facebook account or your Google Account</p>

                        </div>
                    </div>
                </div>
                <div class="panel ">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" id="#coll"><i
                                        class="fa fa-plus" aria-hidden="true"></i> How Reset your Password</a>
                        </h4>
                    </div>
                    <div id="collapse3" class="panel-collapse collapse">
                        <div class="panel-body"><p>If you want to reset your password you have to complete three steps.</p>
                            <ol>
                                <li>Visit to Login Page</li>
                                <li>Click Forget Password Link</li>
                                <li>Enter your email and click SEND PASSWORD RESET BUTTON </li>
                            </ol>
                            <p>
                                When you click reset button we will send a Link(url) to your e-mail.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="panel ">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" id="#coll"><i
                                        class="fa fa-plus" aria-hidden="true"></i> Why You have to Verify Mobile Number before posting an Ad </a>
                        </h4>
                    </div>
                    <div id="collapse4" class="panel-collapse collapse">
                        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </div>
                    </div>
                </div>
                <div class="panel ">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" id="#coll"><i
                                        class="fa fa-plus" aria-hidden="true"></i> Why You cannot find your Ad</a>
                        </h4>
                    </div>
                    <div id="collapse5" class="panel-collapse collapse">
                        <div class="panel-body">There are more resons to show your ads in the list
                        </div>
                    </div>
                </div>
                <div class="panel ">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse6" id="#coll"><i
                                        class="fa fa-plus" aria-hidden="true"></i> How Reset your Password</a>
                        </h4>
                    </div>
                    <div id="collapse6" class="panel-collapse collapse">
                        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('.coll').click(function () {
            $(this).find("i").removeClass("fa fa-plus").addClass("fa fa-minus")
        });
    </script>
@endsection