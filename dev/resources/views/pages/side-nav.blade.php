<section>
    <div class="col-md-3">
        <div class="">
            <ul>
                <li class="list-group-item">
                    <a href="{{asset('')}}about"> About us </a>
                </li>
                <li class="list-group-item">
                    <a href="{{asset('')}}terms-conditions"> Terms & Conditions</a>
                </li>
                <li class="list-group-item">
                    <a href="{{asset('')}}privacy-policy"> Privacy Policy</a>
                </li>
                <li class="list-group-item">
                    <a href="{{asset('')}}contact-us"> Contact us </a>
                </li>
                <li class="list-group-item">
                    <a href="{{asset('')}}faq"> FAQ </a>
                </li>
            </ul>
        </div>
    </div>
</section>