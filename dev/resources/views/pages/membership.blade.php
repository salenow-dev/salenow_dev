@extends('layouts.saleme')
@section('title', 'Membership')
@section('content')
    <div class="container">
        <div class="clear-fix"></div>
        <div class="container mem-sec-1-back pin-bg">
            <div class=" mar-80">
                <h1 class="mem-head">Create your Premium Membership on Saleme.lk </h1>
                <div class="mem-sec-1">
                    <div class="col-md-4">
                        <img src="./images/salenow/id-card-01.png">
                        <h4><strong>Register with Saleme</strong></h4>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/salenow/promo-01.png">
                        <h4><strong>Promote your product</strong></h4>
                    </div>
                    <div class="col-md-4">
                        <img src="./images/salenow/growth-01.png">
                        <h4><strong>Grow your Business</strong></h4>
                    </div>
                    <a class="btn btn-highlight active" data-target="" href="{{asset('')}}member/register">Try
                        Your Memberspace </a>
                    <div class="mem-boxed-area ">
                        <div class="mem-box-title">
                            <h3 class="c-text mem-head-2"> Expand your business with Membership</h3>
                        </div>
                        <p class="mem-para para-box">You can sell your Brand new , Old stuff or you can advertise your
                            business free
                            of cost by submitting free classifieds advertisements. Also many peoples are will to buy
                            different items. So classifieds adverting is right place to buy and sell online.</p>
                    </div>
                </div>
            </div>
            <hr>
            <div class="">
                <div class=" pin-bg">
                    <div class="col-md-6">
                        <img src="./images/salenow/shopping-store (2).png" class="mem-img">
                        <h3 class="c-text mem-head-2"> Grow your Company </h3>
                        <p class="mem-para">Premium membership profile is specifically designed for Expand your Business
                            , increase your sales and to give unmatched publicity to company or your brand through
                            SaleMe.lk.</p>
                    </div>
                    <div class="col-md-6">
                        <img src="./images/salenow/man.png" class="mem-img">
                        <h3 class="c-text mem-head-2"> Grow your personal Business </h3>
                        <p class="mem-para">Brand new experience for the people to have their
                            Own Web Site with fully Manageble Dashboard.You can allocate the members for post ads by
                            your own.</p>
                    </div>
                    <hr>
                </div>
                <div class="clear-fix"></div>
                <div class="pin-bg">
                    <div class="mem-div-3">
                        <img src="./images/salenow/badge.png" class="mem-img-pin">
                        <h3 class="c-text mem-head-2">Pin to Top and Bump to Top</h3>
                        <p class="mem-para">Pin to Top Ad promotions* You will get chances of
                            more than 10X times of displaying your add.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection