@include('main.header')


<div class="container cont-bg">
    {{--<div class="mar-70 text-center">--}}
    {{--<br>--}}
    {{--<h1 class="promo-head2">Welcome to saleme.lk Help & Support page</h1>--}}
    {{--<p class="text-center promo-sub-1">Here you can find answers to our frequently asked questions and learn--}}
    {{--about saleme.lk , how to use it, how to stay safe and get in touch with us. </p>--}}

    {{--</div>--}}
    <div class="top-mar-100"></div>
    <section>
        <div class="col-md-3">
            <div class="">

                <ul>
                    <li class="list-group-item">
                        <a href="#"> About us </a>
                        </l>
                    {{--<li class="list-group-item">--}}
                    {{--<a href="#"> FAQ </a>--}}
                    {{--</l>--}}

                    {{--<li class="list-group-item">--}}
                    {{--<a href="#"> How to sell fast </a>--}}
                    {{--</l>--}}

                    {{--<li class="list-group-item">--}}
                    {{--<a href="#"> Stay safe on saleme.lk </a>--}}
                    {{--</l>--}}
                    <li class="list-group-item">
                        <a href="https://saleme.lk"> Contact us </a>
                        </l>

                </ul>
            </div>
        </div>

    </section>
    <div class="col-md-8">

        <section>
            <h3>How to sell fast</h3>
            <div>
                <i class="fa fa-money" aria-hidden="true" style="font-size:60px ;color:#fea502; "></i>


                <h3 class="">Pick the right price - everything sells if the price is right.</h3>
                <li>Browse similar ads and pick a competitive price.</li>
                <li>Think about how much buyers are willing to pay. The lower the price, the higher the demand.</li>
            </div>
            <br>

            <div>
                <i class="fa fa-picture-o" aria-hidden="true" style="font-size:60px ;color:#fea502; "></i>


                <h3 class="">Use great photos</h3>
                <li>Use actual photos - ads with photos of the real item get up to 10 times more views than ads with
                    catalogue images.
                </li>
                <li>Take clear photos - use good lighting and different angles.</li>
            </div>
            <br>

            <div>
                <i class="fa fa-tags" aria-hidden="true" style="font-size:60px ;color:#fea502; "></i>

                <h3 class="">Promote your ad!</h3>
                <li>Promoted ads get up to 10 times more views.More views = more interested buyers</li>
                <li>More views = more interested buyers</li>
                <li>With lots of interested buyers, you have a better chance of selling fast for the price that you
                    want.
                </li>
            </div>
            <br>

        </section>
    </div>
</div>


@include('main.footer')