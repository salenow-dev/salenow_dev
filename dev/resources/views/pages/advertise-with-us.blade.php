@extends('layouts.saleme')
@section('title', 'Advertise with Us')
@section('content')
    <section>
        <div class="">

            <div class="container container-back">
                <div class="container cont-bg">
                    <div class="advantages">
                        <div class="top-mar-50"></div>
                        <h3> The advantage of advertising on SaleMe.lk</h3>
                        <div class="top-mar-30"></div>
                        <div class="discription">
                           <p> We continuously put forward with our maximum effort to give unmatched
                                publicity to your company or brand in Sri Lanka.
                            We strive to provide ongoing services for our clients before and after the
                                sales and work together to create long term business relationship and
                                assuring the best services at all the time.
                           We take care all your business needs, increase returns and growth to your
                                brand or company.
                           If you need help in creating your banners, we can create unique and
                                attractive banner design for your brand.
                             </p>
                        </div>
                    </div>
                    <div class="top-mar-50"></div>
                    <h3>Where You Want to Advertise on SaleMe.lk </h3>
                </div>
                <div class="container cont-bg">
                    <section id="1">
                        <div class="col-md-8">
                            <div class="bannerinfo">
                                <div class="banner-inner">
                                    <h2 class="ad-type-headers">Leaderboard</h2>
                                    <hr>
                                    <p class="ad-type-1"> Advantageously positioned above the search bar the leaderboard
                                        is viewed by every user of the site.
                                    </p><br>
                                    <h4><i class="ion-plus-circled ion-information"> </i> Good for building brand
                                        awareness</h4>
                                    <h4><i class="ion-plus-circled ion-information"> </i> Available on mobile and
                                        desktop</h4>
                                    <h4><i class="ion-plus-circled ion-information"> </i> On Desktop: 970 x 90 px
                                        Dimensions on Mobile: 320 x 50 px</h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="center-block">
                                <img src="./images/salenow/banner-advertsing/LeaderBoard-01.png"
                                     class="img-responsive banner-img"><br>
                            </div>
                        </div>
                    </section>
                    <section id="2">
                        <div class="col-md-8">
                            <div class="bannerinfo">
                                <div class="banner-inner">
                                    <h2 class="ad-type-headers">Skyscraper</h2>
                                    <hr>
                                    <p class="ad-type-1">A cost efficient placement to the right of our search result
                                        page, with higher reach</p><br>
                                    <h4><i class="ion-plus-circled ion-information"> </i> Best option for brand building
                                    </h4>
                                    <h4><i class="ion-plus-circled ion-information"> </i> Available on desktop only</h4>
                                    <h4><i class="ion-plus-circled ion-information"> </i> On Desktop: 160 x 600 px </h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="center-block">
                                <img src="./images/salenow/banner-advertsing/Skyscraper-01.png"
                                     class="img-responsive banner-img"><br>
                            </div>
                        </div>
                    </section>


                    <section id="3">
                        <div class="col-md-8">
                            <div class="bannerinfo">
                                <div class="banner-inner">
                                    <h2 class="ad-type-headers">Bottom</h2>
                                    <hr>
                                    <p class="ad-type-1"> This also cost-efficient location bottom of search result
                                    page,with higher reach.
                                    </p>
                                    <br>
                                    <h4><i class="ion-plus-circled ion-information"> </i> Best option for brand building
                                    </h4>
                                    <h4><i class="ion-plus-circled ion-information"> </i> Available on mobile and
                                    desktop</h4>
                                    <h4><i class="ion-plus-circled ion-information"> </i> On Desktop: 970 x 90 px
                                    Dimensions on Mobile: 320 x 100 px</h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="center-block">
                                <img src="./images/salenow/banner-advertsing/bottom-01.png"
                                     class="img-responsive banner-img"><br>
                            </div>
                        </div>
                    </section>

                    <section id="4">
                        <div class="col-md-8">
                            <div class="bannerinfo">
                                <div class="banner-inner">
                                    <h2 class="ad-type-headers">Square</h2>
                                    <hr>
                                    <p class="ad-type-1">The Square is the prime location on each of ad pages and
                                    the best way to reach specific target audience.</p><br>
                                    <h4><i class="ion-plus-circled ion-information"> </i> Best option for target
                                    audience</h4>
                                    <h4><i class="ion-plus-circled ion-information"> </i> Available on mobile and
                                    desktop</h4>
                                    <h4><i class="ion-plus-circled ion-information"> </i> On Desktop: 300 x 250 px
                                    Dimensions on Mobile: 250 x 250 px </h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="center-block">
                                <img src="./images/salenow/banner-advertsing/details-01.png"
                                     class="img-responsive banner-img"><br>
                            </div>
                        </div>
                    </section>
           
                </div>
            </div>

@endsection