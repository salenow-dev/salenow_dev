@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Update</strong> User</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <p>This is non libero bibendum, scelerisque arcu id, placerat nunc. Integer ullamcorper rutrum dui
                        eget porta. Fusce enim dui, pulvinar a augue nec, dapibus hendrerit mauris. Praesent efficitur,
                        elit non convallis faucibus, enim sapien suscipit mi, sit amet fringilla felis arcu id sem.
                        Phasellus semper felis in odio convallis, et venenatis nisl posuere. Morbi non aliquet magna, a
                        consectetur risus. Vivamus quis tellus eros. Nulla sagittis nisi sit amet orci consectetur
                        laoreet. Vivamus volutpat erat ac vulputate laoreet. Phasellus eu ipsum massa.</p>
                </div>
                <div class="panel-body">
                    {!! Form::model($user,['url' => '/user/update/'.$user->id,'files'=>true,'class'=>'form-horizontal','autocomplete' => 'off','id'=>'formSubmit']) !!}
                    {{method_field('PUT')}}
                    @include('auth.form-user',['editMode' => true])
                    <div class="panel-footer">
                        <button class="btn btn-default" type="reset">Clear Form</button>
                        <button type="submit" class="btn btn-danger pull-right">Register</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection