<!DOCTYPE html>
<html lang="en" class="body-full-height">
<head>
    <!-- META SECTION -->
    <title>Atlant - Responsive Bootstrap Admin Template</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="css/theme-default.css"/>
    <!-- EOF CSS INCLUDE -->
</head>
<body>

<div class="registration-container">
    <div class="registration-box animated fadeInDown">
        <div class="registration-logo"></div>
        <div class="registration-body">
            <div class="registration-title"><strong>Registration</strong>, use form below</div>
            <div class="registration-subtitle">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In odio mauris,
                maximus ac sapien sit amet.
            </div>
            {!! Form::open(['url' => '/user/register','files'=>true,'class'=>'form-horizontal','autocomplete' => 'off','id'=>'formSubmit']) !!}
            <div class="panel-body">
                @include('auth.form-user')
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-danger btn-block">Register</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="registration-footer">
            <div class="pull-left">
                &copy; 2015 AppName
            </div>
            <div class="pull-right">
                <a href="#">About</a> |
                <a href="#">Privacy</a> |
                <a href="#">Contact Us</a>
            </div>
        </div>
    </div>

</div>

</body>
</html>