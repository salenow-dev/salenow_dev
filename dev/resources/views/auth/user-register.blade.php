@extends('layouts.app')

@section('content')
    <div class="row">
        @role('superadmin')
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>User</strong> Registration</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    {!! Form::open(['url' => '/register','files'=>true,'class'=>'form-horizontal','autocomplete' => 'off','id'=>'formSubmit']) !!}
                    @include('auth.form-user',['editMode' => false])
                    <div class="panel-footer">
                        <button class="btn btn-default" type="reset">Clear Form</button>
                        <button type="submit" class="btn btn-danger pull-right">Register</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    @endrole
@endsection