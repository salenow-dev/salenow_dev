<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label class="col-md-3 col-xs-12 control-label">Name</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
            {!! Form::text('name', null, ['class' => 'form-control','required']) !!}
            @if ($errors->has('name'))
                <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
            @endif
        </div>
    </div>
</div>
@if(!$editMode)
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        <label class="col-md-3 col-xs-12 control-label">User Roles</label>
        <div class="col-md-6 col-xs-12">
            @if(!empty($roles))
                @foreach($roles as $role)
                    <div>
                        <div class="checkbox checkbox-primary">
                            {!! Form::checkbox('role[]',$role->slug,!empty($user)?$user->hasRole($role->slug):null ,['id' =>$role->name]) !!}
                            <label for="{{$role->name}}"> {{$role->name}}</label>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@endif
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label class="col-md-3 col-xs-12 control-label">Email</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-envelope"></span></span>
            {!! Form::text('email', null, ['class' => 'form-control','required', ($editMode)?'readonly':'']) !!}
            @if ($errors->has('email'))
                <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
            @endif
        </div>
    </div>
</div>
<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
    <label class="col-md-3 col-xs-12 control-label">Password</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
            {!! Form::password('password', ['class' => 'form-control','required']) !!}
            @if ($errors->has('password'))
                <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
            @endif
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 col-xs-12 control-label">Confirm Password</label>
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
            {!! Form::password('password_confirmation', ['class' => 'form-control','required']) !!}
        </div>
    </div>
</div>