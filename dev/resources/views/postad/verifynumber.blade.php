@extends('layouts.saleme-shortfooter')
@section('title', 'Verify Number')
@section('content')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script type="text/javascript" src="{{ asset("plugins/blockui/jquery.blockUI.js") }}"></script>
    <script>
        $.blockUI();
    </script>
    <style>
        .connected, .sortable, .exclude, .handles {
            margin: auto;
            padding: 0;
            width: 100px;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .sortable.grid {
            overflow: hidden;
        }

        .connected li, .sortable li, .exclude li {
            color: #313135;
            list-style: none;
            border: 1px solid #e8e8e8;
            background: #fff;
            font-family: "Tahoma";
            margin: 5px;
            padding: 10px 6px;
            box-shadow: 1px 2px 4px #d6d6d6;
            cursor: pointer;
        }

        .handles li {
            color: #fea502;
            list-style: none;
            border: 1px solid #e8e8e8;
            background: #fff;
            font-family: "Tahoma";
            margin: 5px;
            padding: 10px 6px;
            box-shadow: 1px 2px 4px #d6d6d6;
            cursor: pointer;
        }

        .no2 > li {
            color: #fff;
            background: #56b5e3;
            border: 1px solid #56b5e3;
        }

        .handles span {
            cursor: move;
        }

        .sortable.grid li {
            line-height: 80px;
            float: left;
            width: 80px;
            height: 80px;
            text-align: center;
        }

        #connected {
            width: 80%;
            overflow: hidden;
            margin: auto;
        }

        .connected {
            float: left;
            width: 100%;
            /*min-height: 289px;*/
            background: #fff;
            margin: 5px;
        }

        .connected.no2 {
            float: right;
        }

        .tik {
            display: none;
        }
    </style>


    {{--if user is agent--}}
    <div class="container min650">
        <div class="row ">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <div class="step-head">
                    <p class="onemore">
                        @if(count($member->membercontacts))
                            Select Your Mobile Number
                        @else
                            Add Your Mobile Number
                        @endif
                    </p>
                </div>
                <div class="profile-content">
                    {{--select number--}}
                    <div id="s-number" class="col-sm-12 text-center">
                        <form id="verify-number-submit" action="">
                            <div class="boxes boxes-position">
                                {{--<h2 class="select-num">--}}
                                {{--@if(count($member->membercontacts))--}}
                                {{--Select Your Mobile Number--}}
                                {{--@else--}}
                                {{--Add Your Mobile Number--}}
                                {{--@endif--}}
                                {{--</h2>--}}
                                <p class="txt-select">Your Ad Saved as a Draft and Ready to Post.Please Select Your
                                    Contact Numbers from the List</p>
                                <p>Drag to change order of phone numbers</p>
                                <div class="col-md-12">
                                    <div id="error-message"></div>
                                    @if(!empty($member) && count($member->membercontacts))
                                        <section id="connected">
                                            <ul class="connected list">
                                                @foreach($member->membercontacts as $contacts)
                                                    <li>
                                                        <input type="checkbox" id="box-{{$contacts->id}}"
                                                               class="tik"
                                                               name="contactnumber[]"
                                                               {{(isset($adInfo->contact[$contacts->contactnumber]))?'checked': ''}}
                                                               {{(empty($adInfo->ad_referance))?($loop->first) ?'checked' :'':''}}
                                                               value="{{$contacts->contactnumber}}">
                                                        <label for="box-{{$contacts->id}}"> {{$contacts->contactnumber}}</label>

                                                    </li>
                                                @endforeach
                                            </ul>
                                        </section>
                                    @else
                                        <input type="checkbox" id="box" class="tik" name="nonumber"
                                               value="">
                                    @endif
                                    <input type="hidden" name="id" value="{{$adInfo->id}}">
                                    <input type="hidden" name="ad_referance"
                                           value="{{$adInfo->ad_referance}}">
                                </div>
                                <br>
                                <!-- 	add Button -->
                            </div>
                            <div class="text-center">
                                <div class="col-md-12 add-btn-div">
                                    <a href="javascript:;" class="add-button"> <span
                                                class="lnr lnr-phone-handset"></span>&nbsp; Add Number +</a>
                                    @if($is_agent)
                                        <a href="javascript:;" class="add-button2"> <span
                                                    class="lnr lnr-phone-handset"></span>&nbsp; Add Temporary Number
                                            +</a>
                                    @endif
                                </div>
                                <div class="finishbtndiv">
                                    <div class="blockui">
                                        <button type="submit"
                                                class=" btn post-btn-bottom2 finish blockui">
                                            Click to Finish <i class="fa fa-paper-plane"></i>
                                        </button>
                                    </div>
                                </div>
                                <br>
                            </div>
                        </form>
                        <div class="clear-fix"></div>
                    </div>
                    {{--END select number--}}

                    {{--START send pin--}}
                    <div id="verify-div2" class="hide">
                        <div class="subscribe">
                            <!-- 	Image:: Email Logo  -->
                            <img src="{{asset('')}}images/send-otp.png" alt="OTP">
                            <!-- 	Title Card subscribe  -->
                            <h1>ENTER TEMPORARY PHONE NUMBER</h1>
                            <!-- 	Description -->
                            <p>This is temporary phone number. This is not saved anywhere.</p>
                            <div id="message" class="hide alert"></div>
                            <form id="" class="verify-number-form1">
                                <input type="hidden" name="adid" value="{{$_GET['id']}}">
                                <!-- 	input Email -->
                                <div class="col-md-4">
                                    <select name="" id="provider" class="enter-phone">
                                        <option value="0">Select Provider</option>
                                        <option value="9470" num="070">Mobitel 070</option>
                                        <option value="9471" num="071">Mobitel 071</option>
                                        <option value="9472" num="072">Etisalat 072</option>
                                        <option value="9475" num="075">Airtel 075</option>
                                        <option value="9476" num="076">Dialog 076</option>
                                        <option value="9477" num="077">Dialog 077</option>
                                        <option value="9478" num="078" >Hutch 078</option>
                                    </select>
                                </div>
                                 <div class="col-md-8">
                                     <input type="number" name="number" id="phone_number" required="required" class="enter-phone"
                                            placeholder="XXX XXXX" autofocus autocomplete
                                            required checked min="0" max="9999999">

                                </div>
                                <div class="clear-fix"></div>
                                <p class="msg_res"></p>

                                <label id="number-error" class="error" for="number" style="display: none;">Please
                                    enter
                                    a valid number!</label>
                                <!-- 	subscribe Button -->
                                <button type="button" id="add_number_to_ad_btn"
                                        class="sub-button verify-btn">Add Number
                                </button>
                                <br>
                                <br>
                                <a id="viewnumbers2" type="" href="#">Back </a>
                            </form>
                        </div>
                    </div>
                    {{--END pin send-div---}}

                    {{------------------------}}
                    {{--START send pin--}}
                    <div id="verify-div" class="hide">
                        <div class="subscribe">
                            <!-- 	Image:: Email Logo  -->
                            <img src="{{asset('')}}images/send-otp.png" alt="OTP">
                            <!-- 	Title Card subscribe  -->
                            <h1>ENTER PHONE NUMBER</h1>
                            <!-- 	Description -->
                            <p>We will send you a one-time verification code</p>
                            <div id="message" class="hide alert"></div>
                            <form id="formNumberVerify" class="verify-number-form">
                                <input type="hidden" name="adid" value="{{$_GET['id']}}">
                                <!-- 	input Email -->
                                <input type="text" name="number" id="number" required="required" class="enter-phone"
                                       placeholder="07X XXX XXXX" autofocus autocomplete
                                       required checked>
                                <label id="number-error" class="error" for="number" style="display: none;">Please
                                    enter
                                    a valid number!</label>
                                <!-- 	subscribe Button -->
                                <button type="button" name="numberSubmit" id="numberSubmit"
                                        class="sub-button verify-btn">Add Mobile Number
                                </button>
                                <br>
                                <br>
                                <a id="viewnumbers" type="" href="#">Back </a>
                            </form>
                        </div>
                    </div>
                    {{--END pin send-div---}}

                    {{--STATR Verify Number--}}
                    <div class="col-md-12 text-center hide" id="verifynumber">
                        <form class="verify-number-form" id="veryfyotp">
                            <input type="hidden" name="adid" value="{{$_GET['id']}}">
                            <div class="inline-group ">
                                <img src="{{asset('')}}images/otp.png"
                                     class="pin-image">
                                <h2 class="text-center">Verify Your Mobile</h2>
                                <h4 style="color: #7b7b7b !important;"> We just sent you a text message to
                                    <strong><span
                                                id="otp_number"></span></strong> with a code for you enter here.
                                </h4>
                                <br>
                                <div id="otpmessage" class="hide alert"></div>
                                <!-- 	input Email -->
                                <input type="text" name="number_verify" id="number"
                                       class=""
                                       required="required" placeholder="Enter Pin Code" autofocus autocomplete
                                       required checked>
                                <!-- 	verify Button -->
                                <input type="button" name="verify" id="verify"
                                       class="btn sub-button"
                                       class="btn sub-button"
                                       value="Verify"/>
                                <div id="resend_otp">
                                    {{--Resend PIN--}}
                                    <input type="hidden" name="resend" id="resend" value="">
                                    <div id="resendpinnumber" class="hide">
                                        Did not receive PIN code?&nbsp;
                                        <input type="button" name="resendpin" id="resendpin" onclick="resendPIN()"
                                               class="btn  verify-btn"
                                               value="Resend PIN "/>
                                        <br>
                                    </div>
                                </div>
                            </div>

                        </form>
                        <br><br>
                    </div>
                    {{------------------------}}
                    <div class="clear-fix">

                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--END if user is agent--}}
    {{ Html::script('js/salenow/plugin/jquery.validate.js') }}
    <script type="text/javascript" src="{{ asset("js/post-ad/number-verify.js") }}"></script>
    <script type="text/javascript" src="{{ asset("js/post-ad/review.js") }}"></script>
    <style>
        /*-------*/
        .success{
            font-size: 14px !important;
            padding: 10px;
            background-color: #b7f5b3;
            border-radius: 4px;
            border: 1px solid #13920b;
            color: #13920b;
        }
        #phone_number{
            font-family: 'Work Sans', sans-serif !important;
            background-color: transparent;
            width: 100%;
            padding: 10px;
            font-weight: 300;
            border: none;
            outline: 0;
            margin-bottom: 1px;
            margin-top: 18px;
            font-size: 24px;
            border-bottom: 2px solid #e97322;
            letter-spacing: 8px;
        }
        #provider{
            padding: 20px 5px;
            margin-top: 13px;
            border-radius: 9px;
            border: 1px solid #eee;
            outline: none;
        }
        #provider option{
            background: #eee;
            padding: 15px;
            font-size: 14px;
        }
        /*-------*/
        .hide {
            display: none
        }

        .txt-select {
            color: #889189
        }

        .add-btn-div {
            margin-top: 50px
        }

        .show {
            display: block
        }

        .profile {
            margin: 20px 0 -9px;
            width: 100%;
            padding: 4px 14px;
            background: linear-gradient(45deg, #fead1b 5%, #e05925 99%)
        }

        .pin-image {
            width: 13%;
            margin: 0 auto
        }

        .profile .profile-image {
            float: left;
            width: 100%;
            margin: 0 0 10px;
            text-align: center
        }

        .profile .profile-image img {
            width: 55px;
            border: 3px solid #FFF;
            -moz-border-radius: 50%;
            -webkit-border-radius: 50%;
            border-radius: 50%;
            margin: 0 auto
        }

        .profile .profile-data {
            width: 100%;
            float: left;
            margin-top: 8px
        }

        .profile .profile-data .profile-data-name {
            width: 100%;
            float: left;
            font-size: 14px;
            font-weight: 500;
            color: #FFF
        }

        .profile .profile-data .profile-data-title {
            width: 100%;
            float: left;
            font-size: 11px;
            font-weight: 400;
            color: #fff
        }

        .profile .profile-controls a {
            width: 30px;
            height: 30px;
            font-size: 14px;
            color: #FFF;
            border: 2px solid #FFF;
            line-height: 25px;
            position: absolute;
            -moz-border-radius: 50%;
            -webkit-border-radius: 50%;
            border-radius: 50%;
            padding: 0;
            -webkit-transition: all .2s ease;
            -moz-transition: all .2s ease;
            -ms-transition: all .2s ease;
            -o-transition: all .2s ease;
            transition: all .2s ease
        }

        .profile .profile-controls a.profile-control-left {
            left: 15px;
            top: 53px;
            text-align: center
        }

        .profile .profile-controls a.profile-control-right {
            right: 15px;
            top: 53px;
            text-align: center
        }

        .profile .profile-controls a .fa, .profile .profile-controls a .glyphicon {
            width: auto;
            margin-right: auto
        }

        .profile .profile-controls a:hover {
            border-color: #DDD;
            color: #DDD
        }

        .subscribe, input[type=text] {
            color: #445963;
            text-align: center
        }

        .panel .panel-body.list-group {
            padding: 0
        }

        .col-item {
            border: 1px solid #E1E1E1;
            background: #FFF;
            margin-bottom: 12px
        }

        .col-item .options {
            position: absolute;
            top: 6px;
            right: 22px
        }

        .col-item .photo {
            overflow: hidden
        }

        .col-item .photo .options {
            display: none
        }

        .col-item .photo img {
            margin: 0 auto;
            width: 100%
        }

        .col-item .options-cart {
            position: absolute;
            left: 22px;
            top: 6px;
            display: none
        }

        .col-item .photo:hover .options, .col-item .photo:hover .options-cart {
            display: block;
            -webkit-animation: fadeIn .5s ease;
            -moz-animation: fadeIn .5s ease;
            -ms-animation: fadeIn .5s ease;
            -o-animation: fadeIn .5s ease;
            animation: fadeIn .5s ease
        }

        .col-item .options-cart-round {
            position: absolute;
            left: 42%;
            top: 22%;
            display: none
        }

        .col-item .options-cart-round button {
            border-radius: 50%;
            padding: 14px 16px
        }

        .col-item .options-cart-round button .fa {
            font-size: 22px
        }

        .col-item .photo:hover .options-cart-round {
            display: block;
            -webkit-animation: fadeInDown .5s ease;
            -moz-animation: fadeInDown .5s ease;
            -ms-animation: fadeInDown .5s ease;
            -o-animation: fadeInDown .5s ease;
            animation: fadeInDown .5s ease
        }

        .col-item .btn-add a, .col-item .btn-details a {
            display: inline-block !important
        }

        .col-item .info {
            padding: 10px;
            margin-top: 1px
        }

        .col-item .price-details {
            width: 100%;
            margin-top: 5px
        }

        .col-item .price-details h1 {
            font-size: 14px;
            margin-bottom: 14px;
            float: left
        }

        .col-item .price-details .details {
            margin-bottom: 0;
            font-size: 12px
        }

        .col-item .price-details span {
            float: right
        }

        .col-item .price-details .price-new {
            font-size: 16px;
            margin-top: -21px
        }

        .col-item .price-details .price-old {
            font-size: 18px;
            text-decoration: line-through
        }

        .col-item .separator {
            border-top: 1px solid #E1E1E1
        }

        .col-item .clear-left {
            clear: left
        }

        .col-item .separator a {
            text-decoration: none
        }

        .col-item .separator p {
            margin-bottom: 0;
            margin-top: 6px;
            text-align: center
        }

        .col-item .separator p i {
            margin-right: 5px
        }

        .col-item .btn-add {
            width: 60%;
            float: left;
            border-right: 1px solid #E1E1E1
        }

        .col-item .btn-details {
            width: 40%;
            float: left;
            padding-left: 10px
        }

        .col-item .btn-details a:first-child {
            margin-right: 12px
        }

        .item-title {
            margin-bottom: 15px;
            margin-top: 12px;
            font-size: 19px
        }

        .ad-div {
            background: #fff;
            margin-top: 21px
        }

        .item-sum {
            padding-top: 10px;
            padding-bottom: 10px
        }

        .item-detail {
            padding: 0 7px 0 15px;
            font-size: .8em
        }

        .profile-content {
            padding: 20px;
            background: #fff;
            min-height: 477px;
            margin-top: 0
        }

        .subscribe {
            padding: 15px 0 0;
            margin: 0 auto;
            border-radius: 20px;
            width: 80%;
            min-height: 325px;
            height: auto
        }

        .subscribe img {
            width: 13%;
            margin-top: 31px
        }

        .subscribe h2 {
            text-transform: capitalize;
            margin-bottom: 0;
            margin-top: 0;
            font-size: 27px
        }

        .subscribe p {
            font-family: 'Source Sans Pro', sans-serif;
            letter-spacing: 0;
            margin-top: 3px;
            font-size: 16px
        }

        input[type=text] {
            font-family: 'Work Sans', sans-serif !important;
            background-color: transparent;
            width: 100%;
            padding: 10px;
            font-weight: 300;
            border: none;
            outline: 0;
            margin-bottom: 30px;
            margin-top: 18px;
            font-size: 24px;
            border-bottom: 2px solid #e97322;
            letter-spacing: 8px
        }

        .add-button,
        .add-button2,
        .sub-button {
            transition: .3s linear;
            outline: 0;
            font-weight: 700
        }

        .sub-button {
            padding: 12px 25px;
            margin-top: 10px;
            border: none;
            border-radius: 21px;
            font-size: 1em;
            color: #797979;
            background: #eee
        }

        .sub-button:hover {
            cursor: pointer;
            transform: translatey(-3px);
            box-shadow: 0 0 6px #a1c3fe
        }

        .sub-button:active {
            transform: translatey(3px);
            box-shadow: 0 0 0 #a1c3fe
        }

        .add-button,
        .add-button2 {
            padding: 12px 26px 12px 24px;
            margin-top: 42px;
            border: none;
            border-radius: 45px;
            font-size: 12px;
            color: #fff;
            background: #c50000
        }

        .post-btn-bottom2, .step-head {
            background-color: #fea502
        }

        .add-button:hover {
            cursor: pointer;
            transform: translatey(-3px);
            box-shadow: 0 0 6px #a1c3fe
        }

        .add-button:active {
            transform: translatey(3px);
            box-shadow: 0 0 0 #a1c3fe
        }

        .boxes {
            margin: 50px auto auto;
            padding: 15px 23px
        }

        .boxes-position {
            margin: 0 auto
        }

        .bo-left {
            border-left: 1px solid #eee
        }

        .select-num {
            font-size: 20px;
            font-weight: 500;
            margin-bottom: 25px;
            color: #445963
        }

        .tik {
            opacity: 0
        }

        input[type=checkbox] + label {
            display: block;
            position: relative;
            padding-left: 0;
            margin-bottom: -10px;
            color: #000;
            font-size: 19px;
            margin-left: 18px;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none
        }

        input[type=checkbox] + label:last-child {
            margin-bottom: 0
        }

        input[type=checkbox] + label:before {
            content: '';
            display: block;
            width: 20px;
            height: 20px;
            border: 2px solid #e26024;
            position: absolute;
            left: 56px;
            top: 2px;
            -webkit-transition: all .12s, border-color .08s;
            transition: all .12s, border-color .08s
        }

        input[type=checkbox]:checked + label:before {
            width: 10px;
            top: 0;
            border-radius: 0;
            opacity: 1;
            border-top-color: transparent;
            border-left-color: transparent;
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg)
        }

        .post-btn-bottom2 {
            color: #fff !important;
            font-weight: 500;
            text-transform: capitalize;
            border: 0;
            border-radius: 0;
            margin-top: 48px
        }

        p {
            margin: 0
        }

        @media only screen and (max-width: 768px) and (min-width: 320px) {
            p {
                margin: 0 0 1px
            }

            .item-title {
                margin-bottom: 8px;
                margin-top: 0;
                font-size: 19px;
                padding-top: 10px
            }

            .boxes {
                margin: 0 auto auto;
                padding: 0
            }

            input[type=checkbox] + label:before {
                content: '';
                display: block;
                width: 20px;
                height: 20px;
                border: 2px solid #e26024;
                position: absolute;
                left: 0;
                top: 2px;
                -webkit-transition: all .12s, border-color .08s;
                transition: all .12s, border-color .08s
            }

            .subscribe {
                padding: 0;
                margin: 0 auto;
                border-radius: 20px;
                text-align: center;
                width: 100%;
                min-height: 325px;
                height: auto;
                color: #445963
            }

            .subscribe img {
                width: 13%;
                margin-top: 0
            }

            .subscribe p {
                font-family: 'Source Sans Pro', sans-serif;
                letter-spacing: 0;
                margin-top: 3px;
                font-size: 12px
            }
        }

        .onemore {
            margin-top: 33px;
            text-align: center;
            font-weight: 600;
            font-size: 17px;
            color: #fff;
            padding-top: 10px;
            padding-bottom: 10px
        }

        .finish {
            padding: 10px 20px;
            font-size: 18px;
            margin-top: 92px !important
        }

        .min650 {
            min-height: 700px
        }

        .error {
            font-size: 14px !important;
            padding: 10px;
            background-color: #ffdfdf;
            border-radius: 4px;
            border: 1px solid #f9c9c9;
            margin-top: 10px
        }    </style>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        (function ($) {
            var dragging, placeholders = $();
            $.fn.sortable = function (options) {
                var method = String(options);
                options = $.extend({
                    connectWith: false
                }, options);
                return this.each(function () {
                    if (/^enable|disable|destroy$/.test(method)) {
                        var items = $(this).children($(this).data('items')).attr('draggable', method == 'enable');
                        if (method == 'destroy') {
                            items.add(this).removeData('connectWith items')
                                .off('dragstart.h5s dragend.h5s selectstart.h5s dragover.h5s dragenter.h5s drop.h5s');
                        }
                        return;
                    }
                    var isHandle, index, items = $(this).children(options.items);
                    var placeholder = $('<' + (/^ul|ol$/i.test(this.tagName) ? 'li' : 'div') + ' class="sortable-placeholder">');
                    items.find(options.handle).mousedown(function () {
                        isHandle = true;
                    }).mouseup(function () {
                        isHandle = false;
                    });
                    $(this).data('items', options.items)
                    placeholders = placeholders.add(placeholder);
                    if (options.connectWith) {
                        $(options.connectWith).add(this).data('connectWith', options.connectWith);
                    }
                    items.attr('draggable', 'true').on('dragstart.h5s', function (e) {
                        if (options.handle && !isHandle) {
                            return false;
                        }
                        isHandle = false;
                        var dt = e.originalEvent.dataTransfer;
                        dt.effectAllowed = 'move';
                        dt.setData('Text', 'dummy');
                        index = (dragging = $(this)).addClass('sortable-dragging').index();
                    }).on('dragend.h5s', function () {
                        if (!dragging) {
                            return;
                        }
                        dragging.removeClass('sortable-dragging').show();
                        placeholders.detach();
                        if (index != dragging.index()) {
                            dragging.parent().trigger('sortupdate', {item: dragging});
                        }
                        dragging = null;
                    }).not('a[href], img').on('selectstart.h5s', function () {
                        this.dragDrop && this.dragDrop();
                        return false;
                    }).end().add([this, placeholder]).on('dragover.h5s dragenter.h5s drop.h5s', function (e) {
                        if (!items.is(dragging) && options.connectWith !== $(dragging).parent().data('connectWith')) {
                            return true;
                        }
                        if (e.type == 'drop') {
                            e.stopPropagation();
                            placeholders.filter(':visible').after(dragging);
                            dragging.trigger('dragend.h5s');
                            return false;
                        }
                        e.preventDefault();
                        e.originalEvent.dataTransfer.dropEffect = 'move';
                        if (items.is(this)) {
                            if (options.forcePlaceholderSize) {
                                placeholder.height(dragging.outerHeight());
                            }
                            dragging.hide();
                            $(this)[placeholder.index() < $(this).index() ? 'after' : 'before'](placeholder);
                            placeholders.not(placeholder).detach();
                        } else if (!placeholders.is(this) && !$(this).children(options.items).length) {
                            placeholders.detach();
                            $(this).append(placeholder);
                        }
                        return false;
                    });
                });
            };
        })(jQuery);


        $(function () {
            $('.sortable').sortable();
            $('.handles').sortable({
                handle: 'span'
            });
            $('.connected').sortable({
                connectWith: '.connected'
            });
            $('.exclude').sortable({
                items: ':not(.disabled)'
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $.unblockUI();
            var count = {{(!empty($numberCount))?$numberCount:'0'}};
            if (count < 1) {
                $("#verify-number-submit").addClass('hide');
                $("#verify-div").removeClass('hide');
            }

            $('.tik').prop('checked', true);

            $(".add-button").click(function (e) {
                e.preventDefault();
                $("#s-number").addClass('hide').removeClass('show');
                $("#verify-div").addClass('show', 400, "easeOutExpo").removeClass('hide');
            });
            $(".add-button2").click(function (e) {
                e.preventDefault();
                $("#s-number").addClass('hide').removeClass('show');
                $("#verify-div2").addClass('show', 400, "easeOutExpo").removeClass('hide');
            });

            $("#viewnumbers").click(function (e) {
                e.preventDefault();
                $("#s-number").addClass('show').removeClass('hide');
                $("#verify-div").addClass('hide').removeClass('show');
            });

            $("#viewnumbers2").click(function (e) {
                e.preventDefault();
                $("#s-number").addClass('show').removeClass('hide');
                $("#verify-div2").addClass('hide').removeClass('show');
            });

            $('#phone_number').keyup(function () {
                var number = $('#phone_number').val();
                if(number.length == 7){
                    $('.msg_res').addClass('success').removeClass('error').html('Valid Number')
                }else if(number.length > 7){
                    $('.msg_res').addClass('error').removeClass('success').html('Number is too long')
                }else{
                    $('.msg_res').addClass('error').removeClass('success').html('Number is too Short')
                }
//                alert(number);
            });
            $('#provider').change(function () {
                var provider = $('#provider').val();
                if(provider == 0){
                    $('.msg_res').addClass('error').removeClass('success').html('Select Provider');
                    $('#provider').css('border-color','red').focus();
                }else{
                    $('.msg_res').removeClass('error').removeClass('success').html('');
                    $('#provider').css('border-color','#eee').focus();
                }
            });

            $('#add_number_to_ad_btn').click(function () {
                var number = $('#phone_number').val();
                var provider = $('#provider').val();
                if(number.length == 7){
                    $('.msg_res').addClass('success').removeClass('error').html('Valid Number')
                }else if(number.length > 7){
                    $('.msg_res').addClass('error').removeClass('success').html('Number is too long')
                }else{
                    $('.msg_res').addClass('error').removeClass('success').html('Number is too Short')
                }
                if(number.length == 7){
                    if(provider == 0){
                        $('.msg_res').addClass('error').removeClass('success').html('Select Provider');
                        $('#provider').css('border-color','red').focus();
                    }else{
                        var code = $('#provider').find(':selected').attr('num');
                        var display_num = code + "-"+ number;
                        var save_num = provider+number;

                        $('.connected').append('<li>'+
                            '<input type="checkbox" id="box-'+number+'" class="tik" name="contactnumber[]"'+
                            'checked value="'+save_num+'">'+
                            '<label for="box-'+number+'"> '+display_num+'</label></li>');
                        $("#s-number").addClass('show').removeClass('hide');
                        $("#verify-div2").addClass('hide').removeClass('show');


                    }
                }

               });
        });
    </script>

@endsection
