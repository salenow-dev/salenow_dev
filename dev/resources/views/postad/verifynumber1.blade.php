@extends('layouts.saleme-shortfooter')
@section('title', 'Verify Number')
@section('content')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <div class="container">
        <div class="">
            <p class="onemore">You Have 1 More Step</p>
        </div>
        <div class="row ">
            <div class="col-md-2">

            </div>

            <div class="col-md-3 ad-div hidden-xs hidden-sm">
                <h3 class="item-title">Your Ad Summery</h3>
                <div class="col-item">

                    <div class="media profile media-testimonial">
                        <div class="media-left">
                            <div class="profile-image">
                                <img src="{{(!empty($member->avatar))?$member->avatar:asset('/images/user.png')}}">
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="profile-data">
                                <div class="profile-data-name">{{(!empty($member->first_name))?$member->first_name:''}} {{(!empty($member->last_name))?$member->last_name:''}}</div>
                                <div class="profile-data-title">{{(!empty($member->member_type))?ucfirst($member->member_type):''}}</div>
                            </div>
                        </div>
                    </div>

                    <div class="hidden-xs">
                        <div class="photo ">
                            <a href="#">
                                @if($adInfo->featuredimage!='')
                                    <img class="img-responsive "
                                         src="{{asset('')}}salenow/images/uploads/{{$adInfo->id}}/thumb/{{$adInfo->featuredimage}}"
                                         alt="{{strtolower($adInfo->adtitle)}}">
                                @else
                                    <img class="img-responsive " src="{{asset('')}}images/salenow/no_image.jpg"
                                         alt="{{strtolower($adInfo->adtitle)}}">
                                @endif
                            </a>
                        </div>
                        <div class="info">
                            <div class="row">
                                <div class="price-details col-md-6">
                                    <h1 class="col-xs-12 group inner list-group-item-heading" title="">
                                        {{$adInfo->adtitle}} <span
                                                class="label label-primary label-form">{{$adInfo->ad_type}}</span>
                                    </h1>
                                    <div class="item-sum">
                                        <p class="item-detail "><i
                                                    class="ion-ios-location-outline"></i> {{(!empty($member->district))?$member->district->district_name:''}}
                                            , {{(!empty($member->city))?$member->city->city_name:''}}
                                        </p>
                                        <p class="item-detail "><i class="ion-ios-pricetag-outline"></i>
                                            {{$adInfo->category->category_name}}
                                            ,{{$adInfo->subcategory->sub_category_name}}
                                        </p>
                                        {{--@if(!empty($adInfo->mileage))--}}
                                            {{--<p class="item-detail "><i class="ion-ios-speedometer-outline"></i>--}}
                                                {{--{{$adInfo->mileage > 0 ? number_format($adInfo->mileage).'KM' : '' }}--}}
                                            {{--</p>--}}
                                        {{--@endif--}}
                                        <div class="clear-fix"></div>
                                    </div>
                                    <span class="price-new"><b>Rs {{$adInfo->price > 0 ? number_format($adInfo->price) : '' }}</b></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="media visible-xs">
                        <div class="media-left">
                            @if($adInfo->featuredimage!='')
                                <img src="{{asset('')}}salenow/images/uploads/{{$adInfo->id}}/thumb/{{$adInfo->featuredimage}}"
                                     class="media-object" style="width:100px">
                            @else
                                <img src="{{asset('')}}salenow/images/uploads/{{$adInfo->id}}/thumb/{{$adInfo->featuredimage}}"
                                     class="media-object" style="width:100px">
                            @endif
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"> {{$adInfo->adtitle}}</h4>
                            <p><i
                                        class="ion-ios-location-outline"></i> {{(!empty($member->district))?$member->district->district_name:''}}
                                , {{(!empty($member->city))?$member->city->city_name:''}}
                            </p>
                            <span class="price-new"><b>Rs {{$adInfo->price > 0 ? number_format($adInfo->price) : '' }}</b></span>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-5">
                <div class="profile-content">
                    {{--select number--}}
                    <div id="s-number" class="col-sm-12 text-center">
                        <form id="verify-number-submit" action="">
                            <div class="boxes boxes-position">
                                <h2 class="select-num">
                                    @if(count($member->membercontacts))
                                        Select Your Mobile Number
                                    @else
                                        Add Your Mobile Number
                                    @endif
                                </h2>
                                <p class="txt-select">Your Ad Saved as a Draft and Ready to Post.Please Select Your
                                    Contact Numbers from the List</p>
                                <div class="col-md-12">
                                    <div id="error-message"></div>
                                    @if(!empty($member) && count($member->membercontacts))
                                        @foreach($member->membercontacts as $contacts)
                                            <input type="checkbox" id="box-{{$contacts->id}}" class="tik"
                                                   name="contactnumber[]"
                                                   {{(isset($adInfo->contact[$contacts->contactnumber]))?'checked': ''}}
                                                   {{(empty($adInfo->ad_referance))?($loop->first) ?'checked' :'':''}}
                                                   value="{{$contacts->contactnumber}}">
                                            {{--<input type="checkbox" id="box-1"--}}
                                            {{--{{(isset($adInfo->contact[$contacts->contactnumber]))?'checked': ''}}--}}
                                            {{--{{(empty($adInfo->ad_referance))?($loop->first) ?'checked' :'':''}}--}}
                                            {{--value="{{$contacts->contactnumber}}">--}}
                                            <label for="box-{{$contacts->id}}"> {{'0'.substr($contacts->contactnumber, 2, 2).'-'.substr($contacts->contactnumber, 4, 3).'-'.substr($contacts->contactnumber,7)}}</label>
                                        @endforeach
                                    @else
                                        <input type="checkbox" id="box" class="tik" name="nonumber"
                                               value="">
                                    @endif
                                    <input type="hidden" name="id" value="{{$adInfo->id}}">
                                    <input type="hidden" name="ad_referance" value="{{$adInfo->ad_referance}}">
                                </div>
                                <br>
                                <!-- 	add Button -->
                            </div>
                            <div class="text-center">
                                <div class="col-md-12 add-btn-div">
                                    <a href="javascript:;" class="add-button">Add Number +</a>
                                </div>
                                <img src="{{asset('')}}/images/loadingnew.gif" class="loading-post hide"
                                     id="loading"
                                     width="20">
                                <div class="">
                                    <div class="blockui">
                                        <button type="submit"
                                                class=" btn post-btn-bottom2  finish blockui">
                                            Finish Post Ad <i class="fa fa-paper-plane"></i>
                                        </button>
                                    </div>
                                </div>
                                <br>
                            </div>
                        </form>
                        <div class="clear-fix"></div>
                    </div>
                    {{--END select number--}}

                    {{--START send pin--}}
                    <div id="verify-div" class="hide">
                        <div class="subscribe">
                            <!-- 	Image:: Email Logo  -->
                            <img src="{{asset('')}}images/send-otp.png" alt="OTP">
                            <!-- 	Title Card subscribe  -->
                            <h2>Number Verify</h2>
                            <!-- 	Description -->
                            <p>SaleMe.lk will send a verification code via text message (SMS). Please Verify your
                                Mobile
                                Phone Number</p>
                            <div id="message" class="hide alert"></div>
                            <form id="formNumberVerify" class="verify-number-form">
                                <input type="hidden" name="adid" value="{{$_GET['id']}}">
                                <!-- 	input Email -->
                                <input type="text" name="number" id="number" required="required" class="enter-phone"
                                       placeholder="07X XXX XXXX" autofocus autocomplete
                                       required checked>
                                <label id="number-error" class="error" for="number" style="display: none;">Please enter
                                    a valid number!</label>
                                <!-- 	subscribe Button -->
                                <button type="button" name="numberSubmit" id="numberSubmit"
                                        class="sub-button verify-btn">Send PIN
                                </button>
                                <br>
                                <br>
                                <a id="viewnumbers" type="" href="#">Back </a>
                            </form>
                        </div>
                    </div>
                    {{--END pin send-div---}}

                    {{--STATR Verify Number--}}
                    <div class="col-md-12 text-center hide" id="verifynumber">
                        <form class="verify-number-form" id="veryfyotp">
                            <input type="hidden" name="adid" value="{{$_GET['id']}}">
                            <div class="inline-group ">
                                <img src="{{asset('')}}images/otp.png"
                                     class="pin-image">
                                <h3 class="text-center">Verify Your Mobile</h3>
                                <p> An OTP has been sent to your Mobile Number. Please enter the 5
                                    digit OTP below for Verify Number</p>
                                <br>
                                <div id="otpmessage" class="hide alert"></div>
                                <!-- 	input Email -->
                                <input type="text" name="number_verify" id="number"
                                       class=""
                                       required="required" placeholder="Enter Pin Code" autofocus autocomplete
                                       required checked>
                                <!-- 	verify Button -->
                                <input type="button" name="verify" id="verify"
                                       class="btn sub-button"
                                       class="btn sub-button"
                                       value="Verify"/>
                                <div id="resend_otp">
                                    {{--Resend PIN--}}
                                    <input type="hidden" name="resend" id="resend" value="">
                                    <div id="resendpinnumber" class="hide">
                                        Did not receive PIN code?&nbsp;
                                        <input type="button" name="resendpin" id="resendpin" onclick="resendPIN()"
                                               class="btn  verify-btn"
                                               value="Resend PIN "/>
                                        <br>
                                    </div>
                                </div>
                            </div>

                        </form>
                        <br><br>
                    </div>
                    {{--END Verify Number--}}
                    <div class="clear-fix">

                    </div>
                </div>
            </div>
            <div class="col-md-3 ad-div visible-xs visible-sm">
                <h3 class="item-title">Your Ad Summery</h3>
                <div class="col-item">

                    <div class="media profile media-testimonial">
                        <div class="media-left">
                            <div class="profile-image">
                                <img src="{{(!empty($member->avatar))?$member->avatar:asset('/images/user.png')}}">
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="profile-data">
                                <div class="profile-data-name">{{(!empty($member->first_name))?$member->first_name:''}} {{(!empty($member->last_name))?$member->last_name:''}}</div>
                                <div class="profile-data-title">{{(!empty($member->member_type))?ucfirst($member->member_type):''}}</div>
                            </div>
                        </div>
                    </div>

                    <div class="hidden-xs">
                        <div class="photo ">
                            <a href="#">
                                @if($adInfo->featuredimage!='')
                                    <img class="img-responsive "
                                         src="{{asset('')}}salenow/images/uploads/{{$adInfo->id}}/thumb/{{$adInfo->featuredimage}}"
                                         alt="{{strtolower($adInfo->adtitle)}}">
                                @else
                                    <img class="img-responsive " src="{{asset('')}}images/salenow/no_image.jpg"
                                         alt="{{strtolower($adInfo->adtitle)}}">
                                @endif
                            </a>
                        </div>
                        <div class="info">
                            <div class="row">
                                <div class="price-details col-md-6">
                                    <h1 class="col-xs-12 group inner list-group-item-heading" title="">
                                        {{$adInfo->adtitle}} <span
                                                class="label label-primary label-form">{{$adInfo->ad_type}}</span>
                                    </h1>
                                    <div class="item-sum">
                                        <p class="item-detail "><i
                                                    class="ion-ios-location-outline"></i> {{(!empty($member->district))?$member->district->district_name:''}}
                                            , {{(!empty($member->city))?$member->city->city_name:''}}
                                        </p>
                                        <p class="item-detail "><i class="ion-ios-pricetag-outline"></i>
                                            {{$adInfo->category->category_name}}
                                            ,{{$adInfo->subcategory->sub_category_name}}
                                        </p>
                                        {{--@if(!empty($adInfo->mileage))--}}
                                        {{--<p class="item-detail "><i class="ion-ios-speedometer-outline"></i>--}}
                                        {{--{{$adInfo->mileage > 0 ? number_format($adInfo->mileage).'KM' : '' }}--}}
                                        {{--</p>--}}
                                        {{--@endif--}}
                                        <div class="clear-fix"></div>
                                    </div>
                                    <span class="price-new"><b>Rs {{$adInfo->price > 0 ? number_format($adInfo->price) : '' }}</b></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="media visible-xs">
                        <div class="media-left">
                            @if($adInfo->featuredimage!='')
                                <img src="{{asset('')}}salenow/images/uploads/{{$adInfo->id}}/thumb/{{$adInfo->featuredimage}}"
                                     class="media-object" style="width:100px">
                            @else
                                <img src="{{asset('')}}salenow/images/uploads/{{$adInfo->id}}/thumb/{{$adInfo->featuredimage}}"
                                     class="media-object" style="width:100px">
                            @endif
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"> {{$adInfo->adtitle}}</h4>
                            <p><i
                                        class="ion-ios-location-outline"></i> {{(!empty($member->district))?$member->district->district_name:''}}
                                , {{(!empty($member->city))?$member->city->city_name:''}}
                            </p>
                            <span class="price-new"><b>Rs {{$adInfo->price > 0 ? number_format($adInfo->price) : '' }}</b></span>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    </div>

    {{ Html::script('js/salenow/plugin/jquery.validate.js') }}
    <script type="text/javascript" src="{{ asset("plugins/blockui/jquery.blockUI.js") }}"></script>
    <script type="text/javascript" src="{{ asset("js/post-ad/number-verify.js") }}"></script>
    <script type="text/javascript" src="{{ asset("js/post-ad/review.js") }}"></script>
    <style>
        .hide {
            display: none;
        }

        .txt-select {
            color: #889189;
        }

        .add-btn-div {
            margin-top: 50px;
        }

        .show {
            display: block;
        }

        /* Profile container */
        .profile {
            margin: 20px 0;
        }

        /* Profile sidebar */
        .profile {
            width: 100%;
            padding: 4px 14px;
            margin-bottom: -9px;
            background: linear-gradient(45deg, #fead1b 5%, #e05925 99%);
        }

        .pin-image {
            width: 13%;
            margin: 0 auto;
        }

        .profile .profile-image {
            float: left;
            width: 100%;
            margin: 0px 0px 10px;
            text-align: center;
        }

        .profile .profile-image img {
            width: 55px;
            border: 3px solid #FFF;
            -moz-border-radius: 50%;
            -webkit-border-radius: 50%;
            border-radius: 50%;
            margin: 0px auto;
        }

        .profile .profile-data {
            width: 100%;
            float: left;
            margin-top: 8px;
        }

        .profile .profile-data .profile-data-name {
            width: 100%;
            float: left;
            font-size: 14px;
            font-weight: 500;
            color: #FFF;
        }

        .profile .profile-data .profile-data-title {
            width: 100%;
            float: left;
            font-size: 11px;
            font-weight: 400;
            color: #fff;
        }

        .profile .profile-controls a {
            width: 30px;
            height: 30px;
            font-size: 14px;
            color: #FFF;
            border: 2px solid #FFF;
            line-height: 25px;
            position: absolute;
            -moz-border-radius: 50%;
            -webkit-border-radius: 50%;
            border-radius: 50%;
            padding: 0px;
            -webkit-transition: all 200ms ease;
            -moz-transition: all 200ms ease;
            -ms-transition: all 200ms ease;
            -o-transition: all 200ms ease;
            transition: all 200ms ease;
        }

        .profile .profile-controls a.profile-control-left {
            left: 15px;
            top: 53px;
            text-align: center;
        }

        .profile .profile-controls a.profile-control-right {
            right: 15px;
            top: 53px;
            text-align: center;
        }

        .profile .profile-controls a .fa,
        .profile .profile-controls a .glyphicon {
            width: auto;
            margin-right: auto;
        }

        .profile .profile-controls a:hover {
            border-color: #DDD;
            color: #DDD;
        }

        .panel .panel-body.list-group {
            padding: 0px;
        }

        /* Profile Content */

        /*ad draft*/
        .col-item {
            border: 1px solid #E1E1E1;
            background: #FFF;
            margin-bottom: 12px;
        }

        .col-item .options {
            position: absolute;
            top: 6px;
            right: 22px;
        }

        .col-item .photo {
            overflow: hidden;
        }

        .col-item .photo .options {
            display: none;
        }

        .col-item .photo img {
            margin: 0 auto;
            width: 100%;
        }

        .col-item .options-cart {
            position: absolute;
            left: 22px;
            top: 6px;
            display: none;
        }

        .col-item .photo:hover .options,
        .col-item .photo:hover .options-cart {
            display: block;
            -webkit-animation: fadeIn .5s ease;
            -moz-animation: fadeIn .5s ease;
            -ms-animation: fadeIn .5s ease;
            -o-animation: fadeIn .5s ease;
            animation: fadeIn .5s ease;
        }

        .col-item .options-cart-round {
            position: absolute;
            left: 42%;
            top: 22%;
            display: none;
        }

        .col-item .options-cart-round button {
            border-radius: 50%;
            padding: 14px 16px;

        }

        .col-item .options-cart-round button .fa {
            font-size: 22px;
        }

        .col-item .photo:hover .options-cart-round {
            display: block;
            -webkit-animation: fadeInDown .5s ease;
            -moz-animation: fadeInDown .5s ease;
            -ms-animation: fadeInDown .5s ease;
            -o-animation: fadeInDown .5s ease;
            animation: fadeInDown .5s ease;
        }

        .col-item .info {
            padding: 10px;
            margin-top: 1px;
        }

        .col-item .price-details {
            width: 100%;
            margin-top: 5px;
        }

        .col-item .price-details h1 {
            font-size: 14px;
            margin-bottom: 14px;
            float: left;
        }

        .col-item .price-details .details {
            margin-bottom: 0px;
            font-size: 12px;
        }

        .col-item .price-details span {
            float: right;
        }

        .col-item .price-details .price-new {
            font-size: 16px;
            margin-top: -21px;
        }

        .col-item .price-details .price-old {
            font-size: 18px;
            text-decoration: line-through;
        }

        .col-item .separator {
            border-top: 1px solid #E1E1E1;
        }

        .col-item .clear-left {
            clear: left;
        }

        .col-item .separator a {
            text-decoration: none;
        }

        .col-item .separator p {
            margin-bottom: 0;
            margin-top: 6px;
            text-align: center;
        }

        .col-item .separator p i {
            margin-right: 5px;
        }

        .col-item .btn-add {
            width: 60%;
            float: left;
        }

        .col-item .btn-add a {
            display: inline-block !important;
        }

        .col-item .btn-add {
            border-right: 1px solid #E1E1E1;
        }

        .col-item .btn-details {
            width: 40%;
            float: left;
            padding-left: 10px;
        }

        .col-item .btn-details a {
            display: inline-block !important;
        }

        .col-item .btn-details a:first-child {
            margin-right: 12px;
        }

        .item-title {
            margin-bottom: 15px;
            margin-top: 12px;
            font-size: 19px;
        }

        .ad-div {
            background: #fff;
            margin-top: 21px;
        }

        .item-sum {
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .item-detail {
            padding: 0px 7px 0px 15px;
            /* margin: 0px; */
            font-size: .8em;
        }

        /*ad draft*/
        .profile-content {
            padding: 20px;
            background: #fff;
            min-height: 477px;
            margin-top: 20px;
        }

        /*otp section*/

        /* properties of the subscribe card */
        .subscribe {
            padding: 15px 0px 0px 0px;
            margin: 0 auto;
            border-radius: 20px;
            text-align: center;
            width: 80%;
            min-height: 325px;
            height: auto;
            color: #445963;

        }

        /* Iamge:: Email Logo  */
        .subscribe img {
            width: 13%;
            margin-top: 31px;
        }

        /* Title Card subscribe  */
        .subscribe h2 {
            text-transform: capitalize;
            margin-bottom: 0;
            margin-top: 0;
            font-size: 27px;
        }

        /* Description of the "card subscribe" */
        .subscribe p {
            font-family: 'Source Sans Pro', sans-serif;
            letter-spacing: 0;
            margin-top: 3px;
            font-size: 16px;
        }

        /* number input */
        input[type=text] {
            font-family: 'Work Sans', sans-serif !important;
            background-color: transparent;
            text-align: center;
            width: 100%;
            color: #445963;
            padding: 10px;
            font-weight: 300;
            border: none;
            outline: none;
            margin-bottom: 30px;
            margin-top: 18px;
            font-size: 24px;
            border-bottom: 2px solid #e97322;
            letter-spacing: 8px;
        }

        /* Button transmitter subscribe */
        .sub-button {
            padding: 12px 25px 12px 25px;
            transition: 0.3s linear;
            margin-top: 10px;
            border: none;
            border-radius: 21px;
            outline: none;
            font-size: 1em;
            color: #797979;
            font-weight: bold;
            background: #eee;
        }

        .sub-button:hover {
            cursor: pointer;
            transform: translatey(-3px);
            box-shadow: 0 0 6px #a1c3fe;
        }

        .sub-button:active {
            transform: translatey(3px);
            box-shadow: 0 0 0px #a1c3fe;
        }

        /* add Button transmitter subscribe */
        .add-button {
            padding: 12px 26px 12px 24px;
            transition: 0.3s linear;
            margin-top: 42px;
            border: none;
            border-radius: 45px;
            outline: none;
            font-size: 12px;
            color: #5a5959;
            font-weight: bold;
            background: #eee;
        }

        .add-button:hover {
            cursor: pointer;
            transform: translatey(-3px);
            box-shadow: 0 0 6px #a1c3fe;
        }

        .add-button:active {
            transform: translatey(3px);
            box-shadow: 0 0 0px #a1c3fe;
        }

        /*otp section*/
        /*select Number section*/
        .boxes {
            margin: auto;
            margin-top: 50px;
            padding: 15px 23px;
        }

        .boxes-position {
            margin: 0 auto;

        }

        .bo-left {
            border-left: 1px solid #eee;
        }

        .select-num {
            font-size: 20px;
            font-weight: 500;
            margin-bottom: 25px;
            color: #445963;
        }

        /*Checkboxes styles*/
        .tik {
            opacity: 0;
        }

        input[type="checkbox"] + label {
            display: block;
            position: relative;
            padding-left: 0px;
            margin-bottom: -10px;
            color: #000;
            font-size: 19px;
            margin-left: 18px;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
        }

        input[type="checkbox"] + label:last-child {
            margin-bottom: 0;
        }

        input[type="checkbox"] + label:before {
            content: '';
            display: block;
            width: 20px;
            height: 20px;
            border: 2px solid #e26024;
            position: absolute;
            left: 56px;
            top: 2px;
            -webkit-transition: all .12s, border-color .08s;
            transition: all .12s, border-color .08s;
        }

        input[type="checkbox"]:checked + label:before {
            width: 10px;
            top: 0px;
            /*left: 56px;*/
            border-radius: 0;
            opacity: 1;
            border-top-color: transparent;
            border-left-color: transparent;
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg);
        }

        .post-btn-bottom2 {
            background-color: #fea502;
            color: #fff !important;
            font-weight: 500;
            text-transform: capitalize;
            border: 0px;
            border-radius: 0px;
            margin-top: 48px;
        }

        /*select Number section*/
        @media only screen and (max-width: 768px) and (min-width: 320px) {

            p {
                margin: 0 0 1px;
            }

            .item-title {
                margin-bottom: 8px;
                margin-top: 0px;
                font-size: 19px;
                padding-top: 10px;
            }

            .boxes {
                margin: auto;
                margin-top: 0px;
                padding: 0;
            }

            input[type="checkbox"] + label:before {
                content: '';
                display: block;
                width: 20px;
                height: 20px;
                border: 2px solid #e26024;
                position: absolute;
                left: 0;
                top: 2px;
                -webkit-transition: all .12s, border-color .08s;
                transition: all .12s, border-color .08s;
            }

            .subscribe {
                padding: 0;
                margin: 0 auto;
                border-radius: 20px;
                text-align: center;
                width: 100%;
                min-height: 325px;
                height: auto;
                color: #445963;
            }

            .subscribe img {
                width: 13%;
                margin-top: 0;
            }

            .subscribe p {
                font-family: 'Source Sans Pro', sans-serif;
                letter-spacing: 0;
                margin-top: 3px;
                font-size: 12px;
            }

        }
        .onemore{
            margin-top: 13px;
            text-align: center;
            font-weight: 600;
            font-size: 17px;
            color: #fea502;
        }
        .finish {
            padding: 10px 20px;
            font-size: 18px;
            margin-top: 92px !important;
        }

    </style>
    </div>

    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(document).ready(function () {

            $(".add-button").click(function (e) {
                e.preventDefault();
//                $("#s-number").animate({left: '250px'});
//                $("#s-number").addClass('col-sm-7').removeClass('col-sm-12');
//                $(".boxes").addClass('bo-left');
//                $(".boxes").removeClass('boxes-position')
//                $(".add-button").addClass('hide');
                $("#s-number").addClass('hide').removeClass('show');
                $("#verify-div").addClass('show', 400, "easeOutExpo").removeClass('hide');
            });

            $("#viewnumbers").click(function (e) {
                e.preventDefault();
                $("#s-number").addClass('show').removeClass('hide');
                $("#verify-div").addClass('hide').removeClass('show');
            });
        });
    </script>

@endsection
