@extends('layouts.filters')
@section('title', (!empty($seo))? $seo: 'All Ads '.(!empty($location_slug) ? 'in '.ucfirst(str_replace('_',' ',$location_slug)) :''))
@section('og-title', 'All Ads | Saleme.lk')
@section('og-description', 'All ads | Saleme.lk')
@section('content')

    <div class="col-md-7 col-sm-8 cont-bg cont-bg-m all-ads-cont">
        <div class="row">
            <div class="item-content-block tags"> @if(!empty($scategory)) <span>{{$scategory}}<a
                            href="{{asset('')}}ads/{{!empty($location_slug) ? $location_slug :'sri-lanka'}}"
                            class="fa fa-times pull-right"></a> </span> @else @if(!empty($category)) <span>{{$category}}
                    <a href="{{asset('')}}ads/{{!empty($location_slug) ? $location_slug :'sri-lanka'}}"
                       class="fa fa-times pull-right"></a></span> @endif @endif @if(!empty($location_slug) && $location_slug !='sri-lanka')
                    <span>{{ucfirst(str_replace('_',' ',$location_slug))}}<a
                                href="{{asset('')}}ads/sri-lanka/{{!empty($scategory_slug) ? $scategory_slug :!empty($category_slug) ? $category_slug:''}}@if(!empty($REDIRECT_QUERY_STRING))?{{$REDIRECT_QUERY_STRING}}@endif"
                                class="fa fa-times pull-right"></a></span> @endif </div>
            <div class="col-md-12 breadcrumb-div hidden-xs">
                <p class="breadcrumb-text"><a href="{{URL::to('/')}}"><i class="ion-ios-home-outline"></i> </a>&nbsp;/&nbsp; @if(!empty($location_slug) || !empty($category) || !empty($scategory) || !empty($scategory)){{!empty($location_slug) ? ucfirst(str_replace('_',' ',$location_slug.'&nbsp;&nbsp;/&nbsp;')) :''}}@if(!empty($category)){{!empty($category) ? $category.'&nbsp;&nbsp;/&nbsp;' :''}}@elseif(!empty($scategory)){{!empty($scategory) ? $scategory.'&nbsp;&nbsp;/&nbsp;' :''}}@else{{!empty($scategory) ? $scategory.'&nbsp;&nbsp;/&nbsp;' :''}}@endif{{' All Ads'}}@else{{' All Ads in Sri Lanka'}}@endif
                </p>
            </div>
        </div>@if(!empty($addlist))
            <div class="row cont-bg">
                <div class="col-xs-12 breadcrumb-div">
                    <p class="search-count"><b>We found&nbsp;{{$addlist->total()}}&nbsp;result(s)</b>
                        showing&nbsp;{{$addlist->count()+((!empty($boostAdCount))? $boostAdCount:0)}}&nbsp;Ads</p>
                </div>
            </div>
            @if(!empty($allBoostAds))
                @foreach($allBoostAds as $datas)
                    <?php $cnt = 1; $publishedTime = timeForUser($datas->published_at); ?>
                    <a href="{{asset('')}}ad/{{$datas->slug}}">
                        <div class="row search-list-item pinadbg pin{{$cnt}}"> @if($datas->ad_boost_status !='none')
                                <div class="label-special pinned"><i class="ion-star"></i>Top Ad</div>@endif
                            <div class="col-md-4 col-sm-4 col-xs-4 thumb-img-div"> @if($datas->featuredimage!='') <img
                                        class="img-responsive pull-right"
                                        src="{{asset('')}}salenow/images/uploads/{{$datas->adid}}/thumb/{{$datas->featuredimage}}"
                                        alt="{{$datas->subcategory.' - '.strtolower($datas->adtitle) .' in '.$datas->city}}"> @else
                                    <img class="img-responsive pull-left" src="{{asset('')}}images/salenow/no_image.jpg"
                                         alt="{{$datas->subcategory.' - '.strtolower($datas->adtitle) .' in '.$datas->city}}"> @endif
                            </div>
                            <div class="col-md-8 col-xs-9 col-sm-8 all-ads-details hidden-xs">
                                <h3 class="item-title">{{$datas->adtitle}}</h3>
                                <p class="item-loc-cat"><i class="ion-clock"></i>&nbsp;{{$publishedTime}}&nbsp;|&nbsp;<i
                                            class="ion-location"></i>{{$datas->city}}&nbsp;|&nbsp;<i
                                            class="ion-ios-pricetag"></i>{{$datas->subcategory}}</p>
                                @if(!empty($datas->mileage))
                                    <p class="vehicle-mileage"><i
                                                class="ion-speedometer"></i>&nbsp;{{$datas->mileage > 0 ? number_format($datas->mileage) : '0'}}
                                        &nbsp;KM</p>@endif
                                <h4 class="item-price">
                                    Rs&nbsp;{{$datas->price > 0 ? number_format($datas->price) : '0'}}
                                </h4>
                                <div class="pri-adimages-div"> @foreach($datas->adimages as $imgs_addi) <img
                                            class="small_image"
                                            src="{{asset('')}}salenow/images/uploads/{{$datas->adid}}/thumb/{{$imgs_addi->imagename}}"
                                            alt="{{$datas->subcategory.' - '.strtolower($datas->adtitle) .' in '.$datas->city}}"> @endforeach
                                </div>
                                @if(!empty($datas->member_type) AND $datas->member_type === 'premium')
                                    <span class="label label-warning primemember-badge">Premium Member</span>
                                @endif
                            </div>
                            <div class="col-md-9 col-xs-8 all-ads-details visible-xs pd-l-5 pd-r-0-xs">
                                <h3 class="item-title item-m">{{$datas->adtitle}}</h3>
                                <p class="item-loc-cat">{{$datas->subcategory}},&nbsp;{{$datas->city}}</p>
                                @if(!empty($datas->mileage))
                                    <p class="vehicle-mileage">{{$datas->mileage > 0 ? number_format($datas->mileage) : '0'}}
                                        &nbsp;KM</p>@endif
                                <h4 class="item-price">{{$datas->price > 0 ? 'Rs '.number_format($datas->price) : ''}}</h4>
                                @if(!empty($datas->member_type) AND $datas->member_type === 'premium')
                                    <span class="label label-warning primemember-badge">Premium Member</span>
                                @endif
                            </div>
                            <div class="open-in-new-tab visible-xs ">
                                <p class="item-time">{{$publishedTime}}</p>
                            </div>

                        </div>
                    </a>
                    <?php $cnt++; ?>
                @endforeach
            @endif
            @foreach($addlist as $datas)
                <?php $publishedTime = timeForUser($datas->published_at); ?>
                <a href="{{asset('')}}ad/{{$datas->slug}}">
                    <div class="row search-list-item">
                        <div class="col-md-4 col-sm-4 col-xs-4 thumb-img-div"> @if($datas->featuredimage!='') <img
                                    class="img-responsive pull-right lazy" src="{{asset('')}}images/loading-image.gif"
                                    data-original="{{asset('')}}salenow/images/uploads/{{$datas->adid}}/thumb/{{$datas->featuredimage}}"
                                    alt="{{$datas->subcategory.' - '.strtolower($datas->adtitle) .' in '.$datas->city}}"> @else
                                <img class="img-responsive pull-left" src="{{asset('')}}images/salenow/no_image.jpg"
                                     alt="{{$datas->subcategory.' - '.strtolower($datas->adtitle) .' in '.$datas->city}}"> @endif
                        </div>
                        {{--Desktop--}}
                        <div class="col-md-8 col-xs-9 col-sm-8 all-ads-details hidden-xs">
                            <h3 class="item-title">{{$datas->adtitle}}</h3>
                            <p class="item-loc-cat "><i class="ion-clock"></i>&nbsp;{{$publishedTime}}&nbsp;|&nbsp; <i
                                        class="ion-ios-location-outline"></i>&nbsp;{{$datas->city}}&nbsp;|&nbsp; <i
                                        class="ion-ios-pricetags-outline"></i>&nbsp;{{$datas->subcategory}}
                            </p>@if(!empty($datas->mileage))
                                <p class="vehicle-mileage"><i
                                            class="ion-speedometer"></i>&nbsp;{{$datas->mileage > 0 ? number_format($datas->mileage): '0'}}
                                    &nbsp;KM</p>@endif
                            @if($datas->subcategory != 'Jobs in Sri Lanka')
                                <h4 class="item-price">
                                    {{($datas->price > 0 AND $datas->price != 0 )? 'RS '. number_format($datas->price) : 'Negotiable'}}</h4>
                            @endif
                            @if(!empty($datas->member_type) AND $datas->member_type === 'premium')
                                <span class="label label-warning primemember-badge">Premium Member</span>

                            @endif
                        </div>
                        {{--mobile--}}
                        <div class="col-md-9 col-xs-8 all-ads-details visible-xs pd-l-5 pd-r-0-xs">
                            <h3 class="item-title item-m">{{$datas->adtitle}}</h3>
                            <p class="item-loc-cat">{{$datas->subcategory}},&nbsp;{{$datas->city}}</p>
                            @if(!empty($datas->mileage))
                                <p class="vehicle-mileage">{{$datas->mileage > 0 ? number_format($datas->mileage) : '0'}}
                                    &nbsp;KM</p>@endif
                            @if($datas->subcategory != 'Jobs in Sri Lanka')
                                <h4 class="item-price">{{($datas->price > 0 AND $datas->price != 0 ) ? 'Rs '.number_format($datas->price) : 'Negotiable'}}</h4>
                            @endif
                            @if(!empty($datas->member_type) AND $datas->member_type === 'premium')
                                <span class="label label-warning primemember-badge">Premium Member</span>
                            @endif
                        </div>

                        <div class="open-in-new-tab visible-xs ">
                            <p class="item-time">{{$publishedTime}}</p>
                        </div>
                    </div>
                </a>
            @endforeach
            <div class="row">{{$addlist->appends(Illuminate\Support\Facades\Input::except('page'))->links('vendor.pagination.custom')}}</div>@endif
    </div>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js"></script>
    <script>
        $(".lazy").lazyload({
            effect: "fadeIn",
        });
    </script>
@endsection