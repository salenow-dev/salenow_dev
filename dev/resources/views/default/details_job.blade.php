@extends('layouts.adddetails')
<?php
$metaDescItem = htmlspecialchars($ad_details->description, ENT_QUOTES, 'UTF-8');
$metaDescItem = preg_replace("#{(.*?)}(.*?){/(.*?)}#s", '', $metaDescItem);
$metaDescItem = strip_tags($metaDescItem);
$price_as_meal_type = "";
$meal_types = array('Only Bed', 'Bed and Breakfast', 'Half Board', 'Full Board');
?>
@section('title', $ad_details->sub_category_name .' - '.$ad_details->adtitle .' in '. $ad_details->city_name)
@section('og-title', $ad_details->adtitle .' in '. $ad_details->city_name)
@section('og-description', $ad_details->adtitle .' in '. $ad_details->city_name.' | Saleme.lk. '.$metaDescItem)
@section('og-image',asset('salenow/images/uploads/'.$ad_details->adid.'/'.$ad_details->featuredimage) )
{{TrackVisitor::trackVisitor($ad_details->adid,'view')}}
@section('content')
    {{--similer items slider--}}
    {{ Html::style('plugins/slick/slick.css') }}
    {{ Html::style('plugins/slick/slick-theme.css') }}
    {{--similer items slider end--}}
    {{ Html::style('css/salenow/layout/saleme-job-style.css') }}
    {{--{{dd($ad_details)}}--}}
    <div class="theme-layout " id="scrollup">
        <section>
            <div class="block">
                <div class="container cont-bg cont-job">
                    <div class="row">
                        <div class="col-lg-9 column">
                            <div class="job-single-sec">
                                <div class="job-single-head">
                                    <div class="job-thumb hidden-xs">
                                        @if($memeberdetails->member_type == 'premium')
                                            <a href="{{asset('')}}{{$memeberdetails->slug}}"
                                               target="_blank">
                                                <div class="label-special pinned">Premium</div>
                                                <img src="{{asset('')}}salenow/images/prime-members/{{$memeberdetails->id.'/'.$memeberdetails->profile_image}}"
                                                     alt=""/>
                                            </a>
                                        @else
                                            <div class="sellerName ">
                                                <span class="sellerbk">
                                                    {{ucfirst (substr(($ad_details->company)?$ad_details->company : $memeberdetails->first_name, 0, 1))}}
                                                </span>
                                            </div>
                                        @endif
                                    </div>


                                    <div class="job-head-info">
                                        <div class="visible-xs">
                                            <div class="col-xs-4 nopadding">
                                                @if($memeberdetails->member_type == 'premium')
                                                    <a href="{{asset('')}}{{$memeberdetails->slug}}"
                                                       target="_blank">
                                                        <div class="label-special pinned">Premium</div>
                                                        <img src="{{asset('')}}salenow/images/prime-members/{{$memeberdetails->id.'/'.$memeberdetails->profile_image}}"
                                                             alt="" class="img-responsive">
                                                    </a>
                                                @else
                                                    <div class="sellerName ">
                                                <span class="sellerbk">
                                                    {{ucfirst (substr(($ad_details->company)?$ad_details->company : $memeberdetails->first_name, 0, 1))}}
                                                </span>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-xs-8">
                                                <h4 title="{{$ad_details->adtitle}}">{{$ad_details->adtitle}}</h4>
                                                <span>{{($memeberdetails->member_type == 'premium')?($ad_details->company)?ucwords($ad_details->company) : ucwords($memeberdetails->company_name):ucwords(($ad_details->company)?ucwords($ad_details->company) : ucwords($memeberdetails->first_name)).' '. ucwords($memeberdetails->last_name)}}</span>

                                            </div>
                                        </div>
                                        <div class="hidden-xs">
                                            <h4 title="{{$ad_details->adtitle}}">{{$ad_details->adtitle}}</h4>
                                            <span>{{($memeberdetails->member_type == 'premium')?($ad_details->company)?ucwords($ad_details->company) : ucwords($memeberdetails->company_name):ucwords(($ad_details->company)?ucwords($ad_details->company) : ucwords($memeberdetails->first_name)).' '. ucwords($memeberdetails->last_name)}}</span>

                                        </div>

                                        <div class="job-single-sec visible-xs">
                                            @if(!empty($ad_details->featuredimage))
                                                <div class="job-details">
                                                    <img src="{{asset('')}}salenow/images/uploads/{{$ad_details->adid}}/{{$ad_details->featuredimage}}"
                                                         alt="" class="img-responsive"
                                                         style="width: 100%">
                                                </div>
                                            @endif

                                            @if(!empty($ad_details->description))
                                                <div class="job-details">
                                                    <h3>Job Description</h3>
                                                    <p>{!! $ad_details->description !!}</p>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="job-overview">
                                            <ul>
                                                @if($ad_details->itemtype)
                                                    <li title="{{$ad_details->itemtype}}"><i
                                                                class="ion-ios-timer-outline"></i>
                                                        <h3>Time</h3><span>{{$ad_details->itemtype}}</span></li>
                                                @endif
                                                @if($ad_details->city_name)
                                                    <li title="{{$ad_details->city_name}}"><i
                                                                class="ion-ios-location-outline"></i>
                                                        <h3>Location</h3><span>{{$ad_details->city_name}}</span></li>
                                                @endif
                                                @if($ad_details->industry)
                                                    <li title="{{$ad_details->industry}}"><i
                                                                class="ion-ios-pricetag-outline"></i>
                                                        <h3>Category</h3><span>{{$ad_details->industry}}</span>
                                                    </li>
                                                @endif
                                                @if($ad_details->closingdate)
                                                    <?php $closing = new DateTime($ad_details->closingdate);?>
                                                    <li title="{{$closing->format('l jS F Y')}}"><i
                                                                class="ion-ios-calendar-outline"></i>
                                                        <h3>Closing Date</h3>
                                                        <span>{{$closing->format('l jS F Y')}}</span>
                                                    </li>
                                                @endif
                                                @if($ad_details->salary)
                                                    <li title="{{$ad_details->salary}}"><i
                                                                class="ion-social-usd-outline"></i>
                                                        <h3>Salary Scale</h3><span>{{$ad_details->salary}}</span>
                                                    </li>
                                                @endif
                                                @if($ad_details->company)
                                                    <li title="{{$ad_details->company}}"><i
                                                                class="lnr lnr-apartment"></i>
                                                        <h3>Company</h3><span>{{$ad_details->company   }}</span>
                                                    </li>
                                                @endif

                                                {{--<li><i class="la la-line-chart "></i><h3>Qualification</h3><span>Bachelor Degree</span></li>--}}
                                            </ul>
                                        </div><!-- Job Overview -->
                                    </div>
                                </div><!-- Job Head -->
                            </div>
                        </div>
                        <div class="col-lg-3 column">
                            <a href="{{Request::url()}}/apply">
                                <button class="btn btn-warning  apply-btn margin-0">Apply Now</button>
                            </a>
                            <button class="btn btn-warning  email-to-friend margin-0" data-toggle="modal"
                                    data-target=".send-to-friend">Email to Friend
                            </button>
                            {{--<div class="clear-fix"></div>--}}
                            {{--<button class="btn btn-warning  btn-contact-email margin-0">Apply for job</button>--}}
                            <div class="share-bar">
                                <p class="margin-0"
                                   style="display: block;font-size: 12px;line-height: 1;margin-bottom: 5px;">Share
                                    On</p>
                                {{--<div class="fb-send"--}}
                                {{--data-href="{{Request::url()}}"--}}
                                {{--data-layout="button_count"--}}
                                {{--data-colorscheme="dark">--}}
                                {{--</div>--}}
                                <a href="https://www.facebook.com/sharer.php?u={{Request::url()}}" title=""
                                   class="share-fb"><i class="fa fa-facebook"></i></a>
                                <a href="https://plus.google.com/share?url={{Request::url()}}" title=""
                                   class="share-gplus"><i class="fa fa-google-plus "></i></a>
                                <a href="https://twitter.com/share?url={{Request::url()}}&amp;text= &amp;hashtags=saleme.lk"
                                   title="" class="share-linkedin"><i class="fa fa-twitter"></i></a>
                                <a href="https://www.linkedin.com/shareArticle?mini=true&url={{Request::url()}}"
                                   title="" class="share-twitter"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-9 column">
                            <div class="job-single-sec hidden-xs">
                                @if(!empty($ad_details->featuredimage))
                                    <div class="job-details">
                                        <img src="{{asset('')}}salenow/images/uploads/{{$ad_details->adid}}/{{$ad_details->featuredimage}}"
                                             alt="" class="img-responsive"
                                             style="width: 100%">
                                    </div>
                                @endif

                                @if(!empty($ad_details->description))
                                    <div class="job-details">
                                        <h3>Job Description</h3>
                                        <p>{!! $ad_details->description !!}</p>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-lg-3 hidden-xs column">
                            <div class="clear-fix"></div>
                            <br>
                            <div id='div-gpt-ad-1512040136576-0' style='height:600px; width:160px; margin: 0px auto;'>
                                <script>
                                    googletag.cmd.push(function () {
                                        googletag.display('div-gpt-ad-1512040136576-0');
                                    });
                                </script>
                            </div>
                        </div>
                        <div class="col-lg-12 column">
                            <div class="job-single-sec">

                                @if($relatedads)
                                    <div class="recent-jobs">
                                        <h4 class="section-title-1 related  hidden-xs">
                                            @if($relatedByPremium && $memeberdetails->member_type == 'premium')
                                                <span>Some Related Ads by
                                                    <a href="{{asset('')}}{{$memeberdetails->slug}}"
                                                       target="_blank">{{$memeberdetails->company_name}}</a>
                                                </span>
                                            @else
                                                Some Related Jobs
                                            @endif
                                        </h4>
                                        <hr class="related-hr  hidden-xs">
                                        @if($memeberdetails->member_type != 'premium')
                                            <div class="job-list-modern">
                                                <div class="job-listings-sec no-border">
                                                    @foreach($relatedads as $key=>$relatedad)
                                                        {{--{{dd($relatedad)}}--}}
                                                        <a href="{{asset('')}}ad/{{$relatedad->slug}}"
                                                           title="{{$relatedad->adtitle}}">
                                                            <div class="job-listing wtabs">
                                                                <div class="job-title-sec">
                                                                    <div class="c-logo"
                                                                         style="height: 100px;
                                                                         @if(!empty($relatedad->featuredimage))
                                                                                 background: url({{asset('')}}salenow/images/uploads/{{$relatedad->adid}}/{{$relatedad->featuredimage}});
                                                                         @else
                                                                                 background: url({{asset('')}}images/salenow/no_image.jpg);
                                                                         @endif
                                                                                 background-size: 100%;margin-right: 15px;">

                                                                    </div>
                                                                    <h3>{{$relatedad->adtitle}}</h3>
                                                                    @if($relatedad->industry)
                                                                        <span><i class="ion-ios-pricetag"></i> {{$relatedad->industry}}</span>
                                                                    @endif
                                                                    @if($relatedad->city)
                                                                        <div class="job-lctn">
                                                                            <i class="ion-ios-location"></i>{{$relatedad->city}}
                                                                        </div>
                                                                    @endif
                                                                    <div class="job-lctn">
                                                                        <?php $created = new DateTime($relatedad->created_at);?>
                                                                        <i class="ion-ios-stopwatch-outline"></i>
                                                                        On {{$created->format('jS F Y')}}
                                                                    </div>
                                                                </div>
                                                                <div class="job-style-bx">
                                                                    @if($relatedad->itemtype)
                                                                        <span class="job-is ft">{{$relatedad->itemtype}}</span>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                        </a>
                                                    @endforeach

                                                </div>
                                            </div>
                                        @else
                                            <div class="col-md-12 related-items hidden-xs">
                                                <div class="row">
                                                    <section class="regular slider">
                                                        @foreach($relatedads as $key=>$relatedad)
                                                            <div>
                                                                <a href="{{asset('')}}ad/{{$relatedad->slug}}">
                                                                    <div class="related-single">
                                                                        <a href="{{asset('')}}ad/{{$relatedad->slug}}"
                                                                           target="_blank"
                                                                           title="{{$relatedad->adtitle}}">
                                                                            <section class="box">
                                                                                <div class="img-featured"
                                                                                     style="background-image: url({{asset('')}}salenow/images/uploads/{{$relatedad->adid}}/{{$relatedad->featuredimage}})">

                                                                                </div>
                                                                                <header>
                                                                                    <h3 class="title">{{$relatedad->adtitle}}</h3>
                                                                                    <p class="rel-cat">
                                                                                        <i class="ion-ios-pricetag"></i>&nbsp;
                                                                                        {{$relatedad->subcategory}}
                                                                                    </p>
                                                                                    <p class="location">
                                                                                        <i class="ion-ios-location"></i>&nbsp;
                                                                                        {{$relatedad->district_name}}
                                                                                        &nbsp;&gt;&nbsp;{{$relatedad->city}}
                                                                                    </p>
                                                                                    @if($relatedad->category !="Jobs")
                                                                                        <h4 class="price">
                                                                                            {{($relatedad->price> 0)?'RS. '.$relatedad->price: 'Negotiable'}}
                                                                                        </h4>
                                                                                    @endif
                                                                                </header>
                                                                                <div class="last">
                                                                                    <p style="text-align: right; color: #666">
                                                                                        <i
                                                                                                class="ion-ios-time"></i>&nbsp;&nbsp;&nbsp;20
                                                                                        Feb 2018, 10:35am
                                                                                    </p>
                                                                                </div>
                                                                            </section>
                                                                        </a>
                                                                    </div>

                                                                </a>
                                                            </div>
                                                        @endforeach
                                                    </section>
                                                    <!--end one related item-->
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-xs-12 related-items pd-0-xs visible-xs">
                                                @if($memeberdetails->member_type == 'premium')
                                                    <h4 class="section-title-1 related">More Ads
                                                        <span>by
                                                            <a href="{{asset('')}}{{$memeberdetails->slug}}">{{$memeberdetails->company_name}}</a>
                                                        </span>
                                                    </h4>
                                                @else
                                                    <h4 class="section-title-1 related">Some Related Ads</h4>
                                                @endif
                                                <div class="container">
                                                    <div class="row">
                                                    @foreach($relatedads as $key=>$relatedad)
                                                        <!--one related item-->
                                                            <a href="{{asset('')}}ad/{{$relatedad->slug}}">
                                                                <div class="col-md-3 col-xs-12 pd-0-xs">
                                                                    <div class="item-box">
                                                                        <div class="img-hover-style js-image-click col-xs-3 pd-0-xs">
                                                                            <!--<img class="img-responsive" src="assets/img/uploads/phone.jpg">-->
                                                                            <div class="img-class-to-wrap-related pull-left"
                                                                                 style="background-image: url({{asset('')}}salenow/images/uploads/{{$relatedad->adid}}/thumb/{{$relatedad->featuredimage}});">
                                                                                <div class="overlay hidden-xs">
                                                                                    <span class="plus"> <i
                                                                                                class="ion-ios-search"></i></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="caption pull-right col-xs-9">
                                                                            <h2 class="col-xs-12 group inner list-group-item-heading"
                                                                                title="{{$relatedad->adtitle}}">
                                                                                {{$relatedad->adtitle}}
                                                                            </h2>
                                                                            <ul class="prime-features prime-xs">
                                                                                {!! $relatedad->mileage > 0 ? '<li>'.number_format($relatedad->mileage).'KM</li>' : '' !!}
                                                                                {!! !empty($relatedad->fuel_name)? '<li>'.ucwords($relatedad->fuel_name).'</li>':'' !!}
                                                                                {!! !empty($relatedad->landsize)? '<li>'.ucwords($relatedad->landsize).'</li>':''!!}
                                                                                {!! !empty($relatedad->propertysize)? '<li>'.ucwords($relatedad->propertysize).'</li>':''!!}
                                                                                {!! !empty($relatedad->beds)? '<li>'.ucwords($relatedad->beds).' Bedrooms</li>':''!!}
                                                                                {!! !empty($relatedad->baths)? '<li>'.ucwords($relatedad->baths).' Bathrooms</li>':''!!}
                                                                            </ul>
                                                                            <h3 class="item-price">
                                                                                Rs. {{$relatedad->price > 0 ? number_format($relatedad->price) : '' }}</h3>
                                                                            <p class="item-loc hidden-lg hidden-md"><i
                                                                                        class="ion-ios-location-outline"></i> {{$relatedad->city}}
                                                                                <span
                                                                                        class="pd-l-5"><i
                                                                                            class="ion-ios-stopwatch-outline"></i> {{$ad_details->created_at->formatLocalized('%d %B %Y') }}</span>
                                                                            </p>
                                                                            <div class="clear-fix"></div>
                                                                        </div>
                                                                        <div class="clear-fix"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="clear-fix"></div>
                                                            </a>
                                                            <!--end one related item-->
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>

                                        @endif
                                    </div>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>


    </div>
    <div class="clear-fix"></div>
    <!--Email to friend popup-->
    <div class="modal fade inquiry send-to-friend" tabindex="-1" role="dialog" id="modalsendtofriend"
         aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="ion-ios-close-empty"></i>
                    </button>
                    <h4 class="modal-title text-center"><span class="no-1"><i
                                    class="ion-paper-airplane"></i> </span> &nbsp;Email to Friend</h4>
                </div>
                <div class="col-lg-12 col-xs-12 pd-0-xs">
                    <form role="form" id="email-to-friend-form" class="inquiry-form">
                        <input type="hidden" id="adid" name="adid"
                               value="{{(!empty($ad_details->adid))?$ad_details->adid:''}}">
                        <input type="hidden" name="category_code" id="category_code"
                               value="{{$ad_details->category_code}}">
                        <div class="">
                            <div class="col-md-1 col-xs-2">
                                <div class="form-group">
                                    <span for="">To</span>
                                </div>
                            </div>
                            <div class="col-md-11 col-xs-10">
                                <div class="form-group">
                                    <input type="email" class="form-control" name="to_email" autocomplete="off"
                                           id="to_email"
                                           placeholder="Friend's E-mail Address">
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="sender_name" autocomplete="off"
                                           id="sender_name"
                                           value="{{(!empty(getMember())?getMember()->first_name.' '.getMember()->last_name:'')}}"
                                           placeholder="Name">
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="col-md-12 mg-b-10 col-xs-12">
                                <div class="form-group">
                                <textarea class="form-control textarea" rows="1" name="message" id="message"
                                          placeholder="Your Message"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="email-to-friend-btn">
                            <div class="col-md-12 top-mar-20">
                                <button type="button" id="email-to-friend"
                                        class="btn main-btn pull-right email-to-friend">Send
                                </button>
                            </div>
                        </div>
                        <div class="clear-fix"></div>
                    </form>
                </div>
                <div class="clear-fix"><br></div>
            </div>
        </div>
    </div>

    {{ Html::script('js/salenow/plugin/jquery.validate.js') }}
    <!-- FlexSlider Detail page inner-->
    {{ Html::style('css/salenow/plugin/inner-slider/flexslider.min.css') }}
    {{ Html::script('js/salenow/plugin/inner-slider/flexslider.js') }}
    {{ Html::script('plugins/popup-gallery/popup.js') }}
    {{--iem slider related ads start--}}
    {{ Html::script('plugins/slick/slick.js') }}
    {{--light box--}}
    {{ Html::script('plugins/lightbox_2/simple-lightbox.js') }}
    {{--Alerts--}}
    <script src="{{ asset("backend/assets/js/sweetalert.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("plugins/blockui/jquery.blockUI.js")}}"></script>
    <script src="{{asset('plugins/default/platform.js')}}" async defer></script>
    <script type="text/javascript">
        $(document).on('ready', function () {
            $(".regular").slick({
                dots: true,
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 4,
                infinite: false
            });

            $('#phone_number').click(function () {
                var _token = $('input[name="_token"]').val();
                var type = $(this).attr('visittype');
                var id = $(this).attr('adid');
                $.ajax({
                    type: 'post',
                    url: '/visitor-click',
                    data: {
                        '_token': _token,
                        'type': type,
                        'adid': id
                    }
                });
                $(this).css("display", "none");
                $('.loading_gif').show();
                setTimeout(function () {
                    $('.loading_gif').css("display", "none");
                }, 500);
                setTimeout(function () {
                    $('.show_tel').css("display", "block");
                }, 500);
            });


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $("#email-to-friend-form").validate({
                rules: {
                    to_email: {
                        required: true,
                        email: true
                    },
                    sender_name: {
                        required: true,
                    },
                },
                messages: {
                    to_email: {
                        required: "Please Enter Receiver's E-Mail Address !",
                    },
                    sender_name: {
                        required: "Please Enter Your Name!",
                    }
                },
            });
            $('#email-to-friend').click(function () {
                if ($("#email-to-friend-form").valid()) {
                    var _token = $('input[name="_token"]').val();
                    var adid = $("#adid").val();
                    var category_code = $("#category_code").val();
                    $.ajax({
                        type: 'post',
                        url: '/ad/email-to-friend',
                        data: $("#email-to-friend-form").serializeArray(),
                        beforeSend: function (d) {
                            $('#modalsendtofriend').block({
                                message: '<h5>Sending...</h5>', css: {
                                    border: 'none',
                                    padding: '15px',
                                    backgroundColor: '#000',
                                    '-webkit-border-radius': '5px',
                                    '-moz-border-radius': '5px',
                                    opacity: .5,
                                    color: '#fff'
                                }
                            });
                        },
                        success: function (res) {
                            $.unblockUI();
//                            swal("Grate!", "Suggestion Email Sent!", "success").then((value)=>{$('#modalsendtofriend').modal('toggle');});
                        }
                    });
                }
            });
            /***************************************************************************************************************************************************************
             *
             ***************************************************************************************************************************************************************/
//            $.ajaxSetup({
//                headers: {
//                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                }
//            });
//            $('#btn_submit_signup').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
//            $("#inquiry-form").validate({
//                rules: {
//                    name: "required",
//                    message: "required",
//                    email: {
//                        required: true,
//                        email: true
//                    },
//                    mobile: {
//                        required: true,
//                        number: true
//                    }
//                }
//            });
//
//            $("#inquiry_post_ad").click(function (e) {
//                e.preventDefault();
//                if($("#inquiry-form").valid()){
//                    var formdata = $("#inquiry-form").serializeArray();
//                    $.ajax({
//                        type: "POST",
//                        url: '/member/sendinquirymail',
//                        data: formdata,
//                        success: function (res) {
//                            if (res.errors) {
//                                var li = '';
//                                $.each(res.errors, function (key, value) {
//                                    li += '<li>' + value + '</li>'
//                                });
//                                $('#error-message').html(li);
//                                $('#error-div').removeClass("hide");
//                                $('html, body').animate({
//                                    scrollTop: $(".category-title").offset().top
//                                }, 1000);
//                                return false;
//                            }
//                            if (res.response.message == 'success') {
//                                $('#modalinquiry').modal('hide')
//                            }
//                        }
//                    });
//                }
//            });
        });

        $(function () {
            var $gallery = $('.gallery a').simpleLightbox();
        });

        //Details page image slider
        $(window).load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                controlNav: "thumbnails"
            });
        });

        (function ($) {
            $.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {
                e.preventDefault();
                intWidth = intWidth || '500';
                intHeight = intHeight || '400';
                strResize = (blnResize ? 'yes' : 'no');
                var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
                    strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize,
                    objWindow = window.open(this.attr('href'), strTitle, strParam).focus()
            }
            $(document).ready(function ($) {
                $('.customer.share').on("click", function (e) {
                    $(this).customerPopup(e)
                })
            })
        }(jQuery))
    </script>
    {{--light box--}}
    <script>
        gtag('event', 'page_view', {
            'send_to': 'AW-834902320',
            'dynx_itemid': '{{(!empty($ad_details->adid))?$ad_details->adid:''}}',
            'dynx_itemid2': '',
            'dynx_pagetype': 'other',
            'dynx_totalvalue': '{{(!empty($ad_details->price))?$ad_details->price:''}}'
        });

    </script>

    <script>
        $(document).ready(function () {
            $('.fav').click(function (e) {
                e.preventDefault();
                var element = $(this);
                var id = $(this).attr('ad-id');
                var data = {'id': id};
                $.ajax({
                    type: "post",
                    url: '/myads/favorite/' + id,
                    data: data,
                    success: function (res) {
                        if (res.response.message == 'add_success') {
                            $(element).html('<i class="ion-ios-heart"></i>&nbsp;Added to Wish List');

                            swal({
                                title: "Added Wish list successfully",
//                                text: "Added Wish list successfully",
                                type: "success",
                                timer: 2000
                            });
                        } else if (res.response.message == 'delete_success') {
                            $(element).html('<i class="ion-ios-heart-outline"></i>&nbsp;Add to Wish List');
                            swal({
                                title: "Removed from Wish list successfully",
//                                text: "Removed from Wish list successfully",
                                type: "success",
                                timer: 2000
                            });
                        }
                    }
                });
            })
        });
    </script>
@endsection