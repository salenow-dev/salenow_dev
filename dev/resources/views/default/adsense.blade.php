@extends('layouts.saleme')
@section('title', 'SaleMe.lk | Buy, Sell & Rent anything. Absolutely free. Find the better deals from all over Sri Lanka and the best deal closest to you.')
@section('content')
    {{--desktop--}}
    <section class="leader-board-top hidden-xs">
        <div class="container visible-md visible-lg">
            <div class="image-div">
                <!-- /21634329919/slm_leaderboard_970x90_atf -->
                <div id='div-gpt-ad-1506330575958-0'>
                    <script>
                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1506330575958-0'); });
                    </script>
                </div>
            </div>
        </div>
    </section>
    <div class="col-md-2 search-right-bar hidden-xs hidden-sm">
        <!-- /21634329919/slm_skyscraper_160x600_atf -->
        <div id='div-gpt-ad-1506330575958-1' style='height:600px; width:160px;'>
            <script>
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1506330575958-1'); });
            </script>
        </div>
    </div>

@endsection


