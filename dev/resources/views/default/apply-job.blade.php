@extends('layouts.adddetails')
<?php
$metaDescItem = htmlspecialchars($ad_details->description, ENT_QUOTES, 'UTF-8');
$metaDescItem = preg_replace("#{(.*?)}(.*?){/(.*?)}#s", '', $metaDescItem);
$metaDescItem = strip_tags($metaDescItem);
$price_as_meal_type = "";
$meal_types = array('Only Bed', 'Bed and Breakfast', 'Half Board', 'Full Board');
?>
@section('title', $ad_details->sub_category_name .' - '.$ad_details->adtitle .' in '. $ad_details->city_name)
@section('og-title', $ad_details->adtitle .' in '. $ad_details->city_name)
@section('og-description', $ad_details->adtitle .' in '. $ad_details->city_name.' | Saleme.lk. '.$metaDescItem)
@section('og-image',asset('salenow/images/uploads/'.$ad_details->adid.'/'.$ad_details->featuredimage) )
{{TrackVisitor::trackVisitor($ad_details->adid,'view')}}
@section('content')
    {{ Html::style('css/salenow/layout/saleme-job-style.css') }}
    <div class="theme-layout " id="scrollup">
        <section>
            <div class="block">
                <div class="container cont-bg cont-job">
                    {{--job header--}}
                    <div class="row">
                        <div class="col-lg-9 column hidden-xs">
                            <div class="job-single-sec">
                                <div class="job-single-head">
                                    <div class="job-thumb hidden-xs">
                                        @if($memeberdetails->member_type == 'premium')
                                            <a href="{{asset('')}}{{$memeberdetails->slug}}"
                                               target="_blank">
                                                <div class="label-special pinned">Premium</div>
                                                <img src="{{asset('')}}salenow/images/prime-members/{{$memeberdetails->id.'/'.$memeberdetails->profile_image}}"
                                                     alt=""/>
                                            </a>
                                        @else
                                            <div class="sellerName ">
                                                <span class="sellerbk">
                                                    {{ucfirst (substr(($ad_details->company)?$ad_details->company : $memeberdetails->first_name, 0, 1))}}
                                                </span>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="job-head-info">

                                        <h4 title="{{$ad_details->adtitle}}">{{$ad_details->adtitle}}</h4>
                                        <span>{{($memeberdetails->member_type == 'premium')?($ad_details->company)?ucwords($ad_details->company) : ucwords($memeberdetails->company_name):ucwords(($ad_details->company)?ucwords($ad_details->company) : ucwords($memeberdetails->first_name)).' '. ucwords($memeberdetails->last_name)}}</span>
                                        <div class="job-overview">
                                            <ul>
                                                @if($ad_details->itemtype)
                                                    <li title="{{$ad_details->itemtype}}"><i
                                                                class="ion-ios-timer-outline"></i>
                                                        <h3>Time</h3><span>{{$ad_details->itemtype}}</span></li>
                                                @endif
                                                @if($ad_details->city_name)
                                                    <li title="{{$ad_details->city_name}}"><i
                                                                class="ion-ios-location-outline"></i>
                                                        <h3>Location</h3><span>{{$ad_details->city_name}}</span></li>
                                                @endif
                                                @if($ad_details->industry)
                                                    <li title="{{$ad_details->industry}}"><i
                                                                class="ion-ios-pricetag-outline"></i>
                                                        <h3>Category</h3><span>{{$ad_details->industry}}</span>
                                                    </li>
                                                @endif
                                                @if($ad_details->closingdate)
                                                    <?php $closing = new DateTime($ad_details->closingdate);?>
                                                    <li title="{{$closing->format('l jS F Y')}}"><i
                                                                class="ion-ios-calendar-outline"></i>
                                                        <h3>Closing Date</h3>
                                                        <span>{{$closing->format('l jS F Y')}}</span>
                                                    </li>
                                                @endif
                                                @if($ad_details->salary)
                                                    <li title="{{$ad_details->salary}}"><i
                                                                class="ion-social-usd-outline"></i>
                                                        <h3>Salary Scale</h3><span>{{$ad_details->salary}}</span>
                                                    </li>
                                                @endif
                                                @if($ad_details->company)
                                                    <li title="{{$ad_details->company}}"><i
                                                                class="lnr lnr-apartment"></i>
                                                        <h3>Company</h3><span>{{$ad_details->company   }}</span>
                                                    </li>
                                                @endif

                                                {{--<li><i class="la la-line-chart "></i><h3>Qualification</h3><span>Bachelor Degree</span></li>--}}
                                            </ul>
                                        </div><!-- Job Overview -->
                                    </div>
                                </div><!-- Job Head -->
                            </div>
                            <div class="clear-fix"></div>
                        </div>
                        <div class="col-lg-3 column">
                            <a href="{{asset('')}}ad/{{$ad_details->slug}}">
                                <button class="btn btn-warning btn-block margin-0">Back to Job Advertisement</button>
                            </a>
                            <button class="btn btn-warning  email-to-friend margin-0" data-toggle="modal"
                                    data-target=".send-to-friend" style="margin-bottom: 10px;">Email to Friend
                            </button>
                            <a href="{{asset('')}}ads/sri-lanka/jobs">
                                <button class="btn btn-warning btn-block margin-0">See more Jobs</button>
                            </a>
                            <div class="share-bar">
                                <p class="margin-0"
                                   style="display: block;font-size: 12px;line-height: 1;margin-bottom: 5px;">Share
                                    On</p>
                                <a href="https://www.facebook.com/sharer.php?u=http://saleme.me/ad/wdevfbg-galenbindunuwewa"
                                   title="" class="share-fb"><i class="fa fa-facebook"></i></a>
                                <a href="https://plus.google.com/share?url=http://saleme.me/ad/wdevfbg-galenbindunuwewa"
                                   title="" class="share-gplus"><i class="fa fa-google-plus "></i></a>
                                <a href="https://twitter.com/share?url=http://saleme.me/ad/wdevfbg-galenbindunuwewa&amp;text= &amp;hashtags=saleme.lk"
                                   title="" class="share-linkedin"><i class="fa fa-twitter"></i></a>
                                <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=http://saleme.me/ad/wdevfbg-galenbindunuwewa"
                                   title="" class="share-twitter"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                        <div class="clear-fix"></div>
                    </div>
                    {{--END job header--}}
                    <div class="row">
                        <div class="col-md-8">
                            <h4 title="Apply to {{$ad_details->adtitle}}">Apply to {{$ad_details->adtitle}}</h4>
                            <div class="col-md-12">
                                <div class="alert alert- alert-dismissible" style="display: none">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <span></span>
                                </div>
                            </div>

                            <form role="form" id="apply_job_now" action="apply-now" method="post"
                                  class="apply-job-form" enctype="multipart/form-data">
                                <input type="hidden" id="slug" name="slug"
                                       value="{{(!empty($slug))?$slug:''}}">
                                <input type="hidden" name="reciveemail" id="reciveemail"
                                       value="{{(!empty($memeberdetails->premiumemail))? $memeberdetails->premiumemail : $memeberdetails->privetemail}}">
                                <div class="">

                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" autocomplete="off"
                                                   id="name" placeholder="Your Name">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email" autocomplete="off"
                                                   id="email" placeholder="Your E-mail">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-xs-12">
                                        <div class="form-group">
                                            {{--<lable>Apply For</lable>--}}
                                            <input type="text" class="form-control" name="apply_for" autocomplete="off"
                                                   id="apply_for" readonly=""
                                                   value="{{(!empty($ad_details->adtitle)?'Post of '. $ad_details->adtitle:'')}}"
                                                   placeholder="Name">
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="col-md-12 mg-b-10 col-xs-12">
                                        <div class="form-group">
                                        <textarea class="form-control textarea" rows="1" name="message" id="message"
                                                  placeholder="Your Message"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear-fix"></div>
                                <div class="col-md-12 col-xs-12">
                                    <lable>Attach your CV here</lable>
                                    <input type="file" id="apply_job_file" name="apply_job_file" class="text-center well well-sm" style="    display: inline;">
                                    <div id="cv-error"></div>

                                </div>
                                <div class="clear-fix"></div>
                                <div class="email-to-friend-btn">
                                    <div class="col-md-12 top-mar-20">
                                        <button type="submit" id="apply_now"
                                                class="btn main-btn pull-right  apply-job-btn1">Apply Now
                                        </button>
                                    </div>
                                </div>
                                <div class="clear-fix"></div>
                            </form>
                            {{ Html::script('js/salenow/plugin/jquery.validate.js') }}
                            <script type="text/javascript"
                                    src="{{ asset("plugins/blockui/jquery.blockUI.js")}}"></script>
                            <script>
                                $(document).ready(function () {
                                    $.ajaxSetup({
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        }
                                    });
                                    $("#apply_job_now").validate({
                                        ignore: ":hidden",
                                        rules: {
                                            name: {
                                                required: true,
                                            },
                                            email: {
                                                required: true,
                                            },
                                            apply_job_file: {
                                                required: true
                                            }
                                        },
                                        messages: {
                                            apply_job_file: {
                                                required: "Please upload CV"
                                            }
                                        },

                                        submitHandler: function (form) {
                                            var myform = document.getElementById("apply_job_now");
                                            var formData = new FormData(myform);

                                            var out = fileValidation('apply_job_file');
                                            if (out) {
                                                $.ajax({
                                                    url: "/applynow",
                                                    type: "post",
                                                    async: true,
                                                    cache: false,
                                                    contentType: false,
                                                    processData: false,
                                                    data: formData,
                                                    beforeSend: function () {
                                                        $.blockUI({message: '<h1>Just a moment...</h1>'});
                                                    },
                                                    complete: function () {
                                                        $.unblockUI();
                                                    },
                                                    success: function (res) {
                                                        $.unblockUI();
                                                        if (res.success) {
                                                            $('.alert').show().addClass(res.class);
                                                            $('.alert span').html(res.message);
                                                            setTimeout(function () {
//                                                                location.reload();
                                                                window.location = "{{asset('').'ad/'.$ad_details->slug}}"
                                                            }, 5000)
                                                        } else {
                                                            $('.alert').show().addClass(res.class);
                                                            $('.alert span').html(res.message);
                                                        }
                                                        return false;
                                                    }
                                                });
                                            } else {
                                                var msg = 'Please upload file having extensions .pdf , .doc or .docx only.';
                                                $('#cv-error').html(msg).css('color', 'red')
//                                                alert('');
                                            }

                                            return false;
                                        }
                                    });
                                });
                                function fileValidation(id) {
                                    var fileInput = document.getElementById(id);
                                    var filePath = fileInput.value;
                                    var allowedExtensions = /(\.pdf|\.docx|\.doc)$/i;
                                    if (!allowedExtensions.exec(filePath)) {
                                        fileInput.value = '';
//                                        fileInput.style.borderStyle = "solid";
//                                        fileInput.style.borderColor = "red";
                                        return false;
                                    } else {
                                        return true;
                                    }
                                }
                            </script>
                        </div>
                        {{--banner ad--}}
                        <div class="col-md-4">
                            <div class="squre-div visible-md visible-lg">
                                <div class="image-wrapper">
                                    <!-- /21634329919/slm_square_300x250_btf -->
                                    <div id='div-gpt-ad-1512731616849-0' style='height:250px; width:300px;'>
                                        <script>
                                            googletag.cmd.push(function () {
                                                googletag.display('div-gpt-ad-1512731616849-0');
                                            });
                                        </script>
                                    </div>
                                    {{--{{ Html::image('images/salenow/banners/squre-desktop.jpg', 'saleme.lk', array('class' => '')) }}--}}
                                </div>
                            </div>
                        </div>
                        {{--end banner ad--}}
                    </div>
                </div>
            </div>

        </section>
        <div class="clear-fix"></div>
    </div>
    <div class="clear-fix"></div>

    <!--Email to friend popup-->
    <div class="modal fade inquiry send-to-friend" tabindex="-1" role="dialog" id="modalsendtofriend"
         aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="ion-ios-close-empty"></i>
                    </button>
                    <h4 class="modal-title text-center"><span class="no-1"><i
                                    class="ion-paper-airplane"></i> </span> &nbsp;Email to Friend</h4>
                </div>
                <div class="col-lg-12 col-xs-12 pd-0-xs">
                    <form role="form" id="email-to-friend-form" class="inquiry-form">
                        <input type="hidden" id="adid" name="adid"
                               value="{{(!empty($ad_details->adid))?$ad_details->adid:''}}">
                        <input type="hidden" name="category_code" id="category_code"
                               value="{{$ad_details->category_code}}">
                        <div class="">
                            <div class="col-md-2 col-xs-12">
                                <div class="form-group">
                                    <label for="">To</label>
                                </div>
                            </div>
                            <div class="col-md-10 col-xs-12">
                                <div class="form-group">
                                    <input type="email" class="form-control" name="to_email" autocomplete="off"
                                           id="to_email"
                                           placeholder="Friend's E-mail Address">
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="sender_name" autocomplete="off"
                                           id="sender_name"
                                           value="{{(!empty(getMember())?getMember()->first_name.' '.getMember()->last_name:'')}}"
                                           placeholder="Name">
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="col-md-12 mg-b-10 col-xs-12">
                                <div class="form-group">
                                <textarea class="form-control textarea" rows="1" name="message" id="message"
                                          placeholder="Your Message"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="email-to-friend-btn">
                            <div class="col-md-12 top-mar-20">
                                <button type="button" id="email-to-friend"
                                        class="btn main-btn pull-right email-to-friend">Send
                                </button>
                            </div>
                        </div>
                        <div class="clear-fix"></div>
                    </form>
                </div>
                <div class="clear-fix"><br></div>
            </div>
        </div>
    </div>

    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $("#email-to-friend-form").validate({
            rules: {
                to_email: {
                    required: true,
                    email: true
                },
                sender_name: {
                    required: true,
                },
            },
            messages: {
                to_email: {
                    required: "Please Enter Receiver's E-Mail Address !",
                },
                sender_name: {
                    required: "Please Enter Your Name!",
                }
            },
        });
        $('#email-to-friend').click(function () {
            if ($("#email-to-friend-form").valid()) {
                var _token = $('input[name="_token"]').val();
                var adid = $("#adid").val();
                var category_code = $("#category_code").val();
                $.ajax({
                    type: 'post',
                    url: '/ad/email-to-friend',
                    data: $("#email-to-friend-form").serializeArray(),
                    beforeSend: function (d) {
                        $('#modalsendtofriend').block({
                            message: '<h5>Sending...</h5>', css: {
                                border: 'none',
                                padding: '15px',
                                backgroundColor: '#000',
                                '-webkit-border-radius': '5px',
                                '-moz-border-radius': '5px',
                                opacity: .5,
                                color: '#fff'
                            }
                        });
                    },
                    success: function (res) {
                        $.unblockUI();
//                            swal("Grate!", "Suggestion Email Sent!", "success").then((value)=>{$('#modalsendtofriend').modal('toggle');});
                    }
                });
            }
        });
    </script>
@endsection