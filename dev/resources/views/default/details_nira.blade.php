@extends('layouts.adddetails')
<?php
$metaDescItem = htmlspecialchars($ad_details->description, ENT_QUOTES, 'UTF-8');
$metaDescItem = preg_replace("#{(.*?)}(.*?){/(.*?)}#s", '', $metaDescItem);
$metaDescItem = strip_tags($metaDescItem);
?>
@section('title', $ad_details->sub_category_name .' - '.$ad_details->adtitle .' in '. $ad_details->city_name)
@section('og-title', $ad_details->adtitle .' in '. $ad_details->city_name)
@section('og-description', $ad_details->adtitle .' in '. $ad_details->city_name.' | Saleme.lk. '.$metaDescItem)
@section('og-image',asset('salenow/images/uploads/'.$ad_details->adid.'/'.$ad_details->featuredimage) )
{{TrackVisitor::trackVisitor($ad_details->adid,'view')}}
@section('content')
    {{--similer items slider--}}
    {{ Html::style('plugins/slick/slick.css') }}
    {{ Html::style('plugins/slick/slick-theme.css') }}
    {{--similer items slider end--}}
    {{--light box css--}}
    {{ Html::style('plugins/lightbox_2/demo.css') }}
    {{ Html::style('plugins/lightbox_2/simplelightbox.css') }}
    {{ Html::style('plugins/vehi-icons/styles.css') }}
    {{--light box css--}}
    <div class="container ">
        <section>
            <div class="row hidden-xs">
                <div class="col-md-12">
                    <p class="breadcrumb-text">
                        <a href="{{URL::to('/')}}"><i class="ion-ios-home-outline"></i> </a>/&nbsp;&nbsp;
                        {{$ad_details->category_name}} /&nbsp;&nbsp;
                        {{$ad_details->sub_category_name}} /&nbsp;&nbsp;
                        {{$ad_details->adtitle}} in {{$ad_details->district}} -
                        <strong>{{$ad_details->city_name}}</strong>
                    </p>
                </div>
            </div>

            {{--<div class="row visible-xs cont-bg breadcrumb-div">--}}
                {{--<div class="col-md-12">--}}
                    {{--<p class="breadcrumb-text margin-0">--}}
                        {{--<a href="{{URL::to('/')}}"><i class="ion-ios-home-outline"></i> </a>/&nbsp;&nbsp;--}}
                        {{--{{$ad_details->category_name}} /&nbsp;&nbsp;--}}
                        {{--{{$ad_details->sub_category_name}} /&nbsp;&nbsp;--}}
                        {{--{{$ad_details->adtitle}} in {{$ad_details->district}} ---}}
                        {{--<strong>{{$ad_details->city_name}}</strong>--}}
                    {{--</p>--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="mar-top-20">

            </div>
        </section>
        <?php

        $publishedTime = timeForUser($ad_details->published_at);
        ?>
        <div class="row cont-bg">
            <div class="col-lg-12 pd-l-30 visible-xs mobile-title-div">
                <h1 class="adtitle"
                    title="{{$ad_details->adtitle}}">{{$ad_details->adtitle}}</h1>
                <ul class="devider-list vap-chicklet1 adzinf-top">
                    <li>
                        {{$publishedTime}}
                    </li>
                    {{--<li><strong>--}}
                            {{--Referance : </strong>{{$ad_details->ad_referance}}--}}
                    {{--</li>--}}
                    <li><strong>Views : </strong>{{$ad_details->views}}</li>
                </ul>
            </div>
            <div class="col-lg-12 padding-top-15">
                <div class="col-md-6 col-sm-6 cont-bg border-eee img-slider-div">
                    @if(!$images->isEmpty())
                        <div class="flexslider gallery">
                            <div class="flex-viewport" style="overflow: hidden; position: relative;">
                                <ul class="slides"
                                    style="width: 1200%; transition-duration: 0s; transform: translate3d(-627px, 0px, 0px);">
                                    @foreach($images  as $img)
                                        <li data-thumb="{{asset('')}}salenow/images/uploads/{{$img->alladsmaster_id}}/thumb/{{$img->imagename}}"
                                            class="clone gallery-item" aria-hidden="true"
                                            style="width: 627px; margin-right: 0px; float: left; display: block;">
                                            <a href="{{asset('salenow/images/uploads/'.$img->alladsmaster_id.'/'.$img->imagename.'')}}"
                                               class="lumos-link" data-lumos="demo3">
                                                <img src="{{asset('salenow/images/uploads/'.$img->alladsmaster_id.'/'.$img->imagename.'')}}"
                                                     alt="{{$ad_details->sub_category_name .' - '.$ad_details->adtitle .' in '. $ad_details->city_name}}"
                                                     title="{{$ad_details->sub_category_name .' - '.$ad_details->adtitle .' in '. $ad_details->city_name}}">
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @else
                        <img class="img-responsive pull-left" src="{{asset('')}}images/salenow/no_image.jpg">
                    @endif
                </div>

                <div class="col-md-6 col-sm-6 details-div-full pd-l-15">
                    <h3 class="product-title hidden-xs"
                        title="{{$ad_details->adtitle}}">{{$ad_details->adtitle}}</h3>
                    <ul class="devider-list vap-chicklet1 hidden-xs">
                        <li>
                            <i class="ion-ios-stopwatch-outline ico2"></i>
                            {{$publishedTime}}
                        </li>
                        <li><strong> <i class="lnr lnr-pushpin ico2"></i>
                                Referance : </strong>{{$ad_details->ad_referance}}
                        </li>
                        <li><strong> <i class="ion-ios-eye-outline ico2"></i> Views :
                            </strong>{{$ad_details->views}}</li>
                    </ul>
                    <hr class="seperator hidden-xs">
                    <br class="hidden-xs">
                    <p class="product-description hidden-xs">{{ substr(str_replace('<br />', ' ',$ad_details->description), 0, 120).'...' }}
                        <span class=""> &nbsp;&nbsp;&nbsp;<a href="#read-more" class="read-more">Read More</a></span>
                    </p>

                    <div class="col-lg-12 top-mar-20 pd-l-30 price-section ad-price-div">
                        <p class=" hidden-xs"><span class="pricetag">
                            <span class="currency">Rs: </span> {{$ad_details->price > 0 ? number_format($ad_details->price) : '0' }}</span>
                            @if($ad_details->nego)
                                <span class="nigotible">Negotiable <i
                                            class="ion-ios-checkmark-outline ico-2"></i></span>
                            @endif
                        </p>
                        <p class="text-center visible-xs"><span class="pricetag-xs">
                            <span class="currency">Rs: </span> {{$ad_details->price > 0 ? number_format($ad_details->price) : '0' }}</span>
                            @if($ad_details->nego)
                                <span class="nigotible">Negotiable
                                        <i class="ion-ios-checkmark-outline ico-2"></i></span>
                            @endif
                        </p>
                    </div>
                    <hr class="seperator visible-xs">
                    <div class="seller col-md-12 pd-l-30 seller-names ad-details-user-details-div">
                        <div class="row pd-t-5">
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <div class="sellerInfo">
                                    <div class="sellerName ">
                                        <span class="sellerbk">
                                            {{ucfirst (substr($memeberdetails->first_name, 0, 1))}}
                                        </span>
                                    </div>
                                    <div class="sellerStatus">
                                        @if($memeberdetails->member_type == 'premium')
                                            <div class="uname bold-txt">
                                                <span>
                                                    <a href="{{asset('')}}member/{{$memeberdetails->slug}}"
                                                       target="_blank">
                                                        {{$memeberdetails->company_name}}
                                                    </a>
                                                </span>
                                            </div>
                                        @else
                                            <div class="uname bold-txt">
                                                <span>{{strtoupper($memeberdetails->first_name)}} {{strtoupper($memeberdetails->last_name)}}</span>
                                            </div>
                                        @endif
                                        <div class="lightgrey js-showSellerMobile">
                                            <small class="user-veri-txt"> Verified User
                                                <i class="ion-ios-checkmark verified"></i></small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear-fix"></div>
                            <div class="col-lg-6 pd-t-8 col-md-6 col-xs-6">
                                <div class="sellerInfo-location">
                                    <div class="sellerName hidden-xs">
                                        <span class="sellerbk2">
                                            <i class="ion-ios-location-outline"></i>
                                        </span>
                                    </div>
                                    <div class="sellerStatus">
                                        <div class="uname light-font">LOCATION</div>
                                        <div class="lightgrey bold-txt font-10"> {{$ad_details->city_name}}</div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-6  pd-t-8 col-md-6 col-xs-6">
                                <div class="sellerInfo-time">
                                    <div class="sellerName  hidden-xs">
                                        <span class="sellerbk2">
                                            <i class="ion-ios-clock-outline"></i>
                                        </span>
                                    </div>
                                    <div class="sellerStatus">
                                        <div class="uname light-font"> AD UPDATED</div>
                                        <div class="lightgrey bold-txt font-10"> {{$ad_details->created_at->formatLocalized('%d %B %Y') }}</div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-6 mr-t-10 pd-r-5 view-phone-number col-xs-12 ">
                                <section class="progress-demo">
                                    <button class="btn btn-warning btn-contact-num margin-0" id="phone_number"
                                            visittype="click" adid="{{$ad_details->adid}}">
                                        <i class="fa fa-phone ico2" aria-hidden="true"></i>
                                        @if(!empty($contacts))
                                            {{'0'.substr($contacts['0'], 2, 2).'-'.substr($contacts['0'], 4, 3).'-'.'XXX'}}
                                        @else
                                            View Number
                                        @endif
                                    </button>
                                </section>
                                <p class="text-center margin-0">
                                    <img src="{{asset('')}}/images/loading-img.gif" class="loading_gif"></p>
                                <section class="show_tel">
                                    @if(!empty($contacts))
                                        @foreach($contacts as $key=>$contact)
                                            <a href="tel:+{{$contact}}" rel="external" data-role="button">
                                                <button class="btn btn-warning btn-contact-num-active ">
                                                    <i class="fa fa-phone ico2"
                                                       aria-hidden="true"></i>
                                                    {{'0'.substr($contact, 2, 2).'-'.substr($contact, 4, 3).'-'.substr($contact,7)}}
                                                </button>
                                            </a>
                                        @endforeach
                                    @endif
                                </section>
                            </div>
                            <div class="col-sm-6 mr-t-10 pd-l-5 view-phone-number col-xs-12">
                                <section class="progress-demo">
                                    <button class="btn btn-warning  btn-contact-email margin-0" id="phone_number1"
                                            visittype="click" data-toggle="modal" data-target=".send-to-friend">
                                        <i class="fa fa-envelope-o ico2" aria-hidden="true"></i>Email to Friend
                                    </button>
                                </section>

                            </div>
                            {{--<div class="col-sm-4 pd-r-0 pd-l-5 view-phone-number view-email col-xs-12">--}}
                            {{--<button type="button" class="btn btn-warning  btn-contact-email" data-toggle="modal"--}}
                            {{--data-target=".inquiry">--}}
                            {{--<i class="fa fa-envelope-o ico2" aria-hidden="true"></i>&nbsp;&nbsp;Email Now--}}
                            {{--</button>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                    <div class="clear-fix"></div>
                    <div class="text-left social-share margin-t-20">
                        <div class="fb-send"
                             data-href="{{Request::url()}}"
                             data-layout="button_count"
                             data-colorscheme="dark">
                        </div>
                        <a href="https://www.facebook.com/sharer.php?u={{Request::url()}}"
                           class="btn btn-social-icon btn-facebook facebook customer share"
                           title="Share this ad on Facebook">
                            <i class="ion-social-facebook"></i>
                        </a>
                        <a href="https://plus.google.com/share?url={{Request::url()}}"
                           class="btn btn-social-icon btn-google-plus twitter customer share"
                           title="Share this ad on Google Plus">
                            <i class="ion-social-googleplus-outline"></i>
                        </a>
                        <a href="https://twitter.com/share?url={{Request::url()}}&amp;text= &amp;hashtags=saleme.lk"
                           class="btn btn-social-icon btn-twitter google_plus customer share"
                           title="Share this ad on Twitter">
                            <i class="ion-social-twitter"></i>
                        </a>
                        <a href="https://www.linkedin.com/shareArticle?mini=true&url={{Request::url()}}"
                           class="btn btn-social-icon btn-linkedin linkedin customer share"
                           title="Share this ad on Linkedin">
                            <i class="ion-social-linkedin"></i>
                        </a>
                    </div>
                    <div class="row">
                        <div class="col-md-12 ">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row cont-bg">
            <div class="col-md-8 col-sm-12 col-xs-12 details-des-div" id="read-more">
                <div class="col-xs-12 cont-bg border-eee detils-top-div">
                    @if(array_key_exists("Condition",$icons))
                        @if($icons['Condition']!="")
                            <div class="col-md-3 vap-details-tail col-xs-6">
                                <div class="vap-tail-icon">
                                    <i class="icon-saleme-condition"></i>
                                </div>
                                <div class="vap-tail-desc">
                                    <span>Condition</span>
                                    <span class="vap-tail-values">{{$icons['Condition']}}</span>
                                </div>
                            </div>
                        @endif
                    @endif
                    @if(array_key_exists("Transmission",$icons))
                        @if($icons['Transmission']!="")
                            <div class="col-md-3 vap-details-tail col-xs-6">
                                <div class="vap-tail-icon">
                                    <i class="icon-saleme-gear"></i>
                                </div>
                                <div class="vap-tail-desc">
                                    <span>Transmission</span>
                                    <span class="vap-tail-values">{{$icons['Transmission']}}</span>
                                </div>
                            </div>
                        @endif
                    @endif
                    @if(array_key_exists("Fuel Type",$icons))
                        @if($icons['Fuel Type']!="")
                            <div class="col-md-3 vap-details-tail col-xs-6">
                                <div class="vap-tail-icon">
                                    <i class="icon-saleme-fuel"></i>
                                </div>
                                <div class="vap-tail-desc">
                                    <span>Fuel Type</span>
                                    <span class="vap-tail-values">{{$icons['Fuel Type']}}</span>
                                </div>
                            </div>
                        @endif
                    @endif
                    @if(array_key_exists("Kms Driven",$icons))
                        @if($icons['Kms Driven']!="")
                            <div class="col-md-3 vap-details-tail col-xs-6">
                                <div class="vap-tail-icon">
                                    <i class="icon-saleme-speedo"></i>
                                </div>
                                <div class="vap-tail-desc">
                                    <span>Kms Driven</span>
                                    <span class="vap-tail-values">{{$icons['Kms Driven'] > 0 ? number_format($icons['Kms Driven']) : '' }}
                                        kms</span>
                                </div>
                            </div>
                        @endif
                    @endif
                    @if(array_key_exists("Authenticity",$icons))
                        @if($icons['Authenticity']!="")
                            <div class="col-md-3 vap-details-tail col-xs-6">
                                <div class="vap-tail-icon">
                                    <i class="ion-loop"></i>
                                </div>
                                <div class="vap-tail-desc">
                                    <span>Authenticity</span>
                                    <span class="vap-tail-values">{{$icons['Authenticity'] }} </span>
                                </div>
                            </div>
                        @endif
                    @endif
                    @if(array_key_exists("Bedrooms",$icons))
                        @if($icons['Bedrooms']!="")
                            <div class="col-md-3 vap-details-tail col-xs-6">
                                <div class="vap-tail-icon">
                                    <i class=" fa fa-bed"></i>
                                </div>
                                <div class="vap-tail-desc">
                                    <span>Bedrooms</span>
                                    <span class="vap-tail-values">{{$icons['Bedrooms'] }} </span>
                                </div>
                            </div>
                        @endif
                    @endif
                    @if(array_key_exists("Bathrooms",$icons))
                        @if($icons['Bathrooms']!="")
                            <div class="col-md-3 vap-details-tail col-xs-6">
                                <div class="vap-tail-icon">
                                    <i class="fa fa-shower"></i>
                                </div>
                                <div class="vap-tail-desc">
                                    <span>Bathrooms</span>
                                    <span class="vap-tail-values">{{$icons['Bathrooms'] }} </span>
                                </div>
                            </div>
                        @endif
                    @endif
                    @if(array_key_exists("Land Size",$icons))
                        @if($icons['Land Size']!="")
                            <div class="col-md-3 vap-details-tail col-xs-6">
                                <div class="vap-tail-icon">
                                    <i class="ion-ios-albums-outline"></i>
                                </div>
                                <div class="vap-tail-desc">
                                    <span>Land Size</span>
                                    <span class="vap-tail-values">{{$icons['Land Size'] }} </span>
                                </div>
                            </div>
                        @endif
                    @endif
                    @if(array_key_exists("Property Size",$icons))
                        @if($icons['Property Size']!="")
                            <div class="col-md-3 vap-details-tail col-xs-6">
                                <div class="vap-tail-icon">
                                    <i class="fa fa-building-o"></i>
                                </div>
                                <div class="vap-tail-desc">
                                    <span>Property Size</span>
                                    <span class="vap-tail-values">{{$icons['Property Size'] }} </span>
                                </div>
                            </div>
                        @endif
                    @endif
                    <div class="clear-fix"></div>
                    @if(!empty($generalinfo))
                        <div class="cont-bg spacification-div ">
                            <h3 class="spec-title">Specifications</h3>
                            {{--@foreach(array_chunk($generalinfo,2, true) as $row)--}}
                            <ul class="spec-ul">
                                @foreach($generalinfo as $key => $info)
                                    @if(!empty($info))
                                        <li class="col-lg-6 col-xs-12 col-md-6">
                                            <span class="col-lg-1 col-xs-1 pd-0-xs">
                                                <i class="icon-saleme-{{str_replace(' ','-',strtolower($key))}} icon-saleme"></i>
                                            </span>
                                            <span class="col-lg-4 spec  col-xs-4">{{$key}}</span>
                                            <span class="col-lg-6 spec-des  col-xs-6">{{$info}}</span>
                                            <div class="clear-fix"></div>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                            {{--@endforeach--}}
                            <div class="clear-fix"></div>
                        </div>
                    @endif

                    @if(count($features) >0)
                        <div class="feature-div">
                            <h3 class="spec-title">Features</h3>
                            <ul class="feature-ul">
                                @if(count($features) >0)
                                    @foreach($features as $key=>$feature)
                                        <li class="col-lg-3 col-xs-6 col-md-3 col-sm-4 pd-0-xs">
                                            <span><i class="ion-ios-checkmark"></i>&nbsp;&nbsp;{{$feature}}</span>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                            <div class="clear-fix"></div>
                        </div>
                    @endif
                    @if(!empty($ad_details->description))
                        <div class="cont-bg description-div">
                            <h3 class="spec-title">Description</h3>
                            <p>{!!$ad_details->description!!}</p>
                            <div class="clear-fix"></div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="squre-div visible-md visible-lg">
                    <div class="image-wrapper">
                        <!-- /21634329919/slm_square_300x250_btf -->
                        <div id='div-gpt-ad-1512731616849-0' style='height:250px; width:300px;'>
                            <script>
                                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1512731616849-0'); });
                            </script>
                        </div>
                        {{--{{ Html::image('images/salenow/banners/squre-desktop.jpg', 'saleme.lk', array('class' => '')) }}--}}
                    </div>
                </div>
            </div>
        </div>
        <div class="row cont-bg">
            <div class="col-lg-12 related-ads-div">
                @if($relatedads)
                    <div class="col-md-12 related-items hidden-xs">
                        <h4 class="section-title-1 related">Some Related Ads
                            @if($relatedByPremium && $memeberdetails->member_type == 'premium')
                                <span>by
                                    <a href="{{asset('')}}member/{{$memeberdetails->slug}}"
                                       target="_blank">{{$memeberdetails->company_name}}</a>
                                </span>
                            @endif
                            <p class="small pull-right seemore">
                                {{--                                <a href="{{asset('')}}{{$seeMOreLink}}"><u><i class="ion-ios-plus-outline"></i> See More</u></a>--}}
                                {{--<a href="{{asset('')}}"><u><i class="ion-ios-plus-outline"></i> See More</u></a>--}}
                            </p></h4>
                        <hr class="related-hr">
                        <div class="row">
                            <section class="regular slider">
                                @foreach($relatedads as $key=>$relatedad)
                                    <div>
                                        <a href="{{asset('')}}ad/{{$relatedad->slug}}">
                                            <div class="item-box">
                                                <div class="img-hover-style js-image-click">
                                                    <div class="img-class-to-wrap"
                                                         style="background-image: url({{asset('')}}salenow/images/uploads/{{$relatedad->adid}}/thumb/{{$relatedad->featuredimage}});">
                                                        <div class="overlay">
                                                            <span id="plus"> <i
                                                                        class="ion-ios-search-strong"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="caption">
                                                    <h2 class="col-xs-12 group inner list-group-item-heading"
                                                        title="{{$relatedad->adtitle}}">
                                                        {{truncateText($relatedad->adtitle,20)}}
                                                    </h2>
                                                    <ul class="prime-features">
                                                        {!! $relatedad->mileage > 0 ? '<li>'.number_format($relatedad->mileage).'KM</li>' : '' !!}
                                                        {!! !empty($relatedad->fuel_name)? '<li>'.ucwords($relatedad->fuel_name).'</li>':'' !!}
                                                        {!! !empty($relatedad->landsize)? '<li>'.ucwords($relatedad->landsize).'</li>':''!!}
                                                        {!! !empty($relatedad->propertysize)? '<li>'.ucwords($relatedad->propertysize).'</li>':''!!}
                                                        {!! !empty($relatedad->beds)? '<li>'.ucwords($relatedad->beds).' Bedrooms</li>':''!!}
                                                        {!! !empty($relatedad->baths)? '<li>'.ucwords($relatedad->baths).' Bathrooms</li>':''!!}
                                                    </ul>
                                                    <h3 class="item-price">
                                                        Rs. {{$relatedad->price > 0 ? number_format($relatedad->price) : '' }}</h3>
                                                    <div class="clear-fix"></div>
                                                </div>
                                                <div class="item-footer">
                                                    <p class="item-loc pull-left"><i
                                                                class="ion-ios-location-outline"></i> {{$relatedad->city}}
                                                    </p>
                                                    <p class="item-loc pull-left"><i
                                                                class="ion-ios-stopwatch-outline"></i>
                                                        {{$ad_details->created_at->formatLocalized('%d %B %Y') }}
                                                    </p>
                                                    <div class="clear-fix"></div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </section>
                            <!--end one related item-->
                        </div>
                    </div>

                    {{--mobile--}}
                    <div class="col-md-12 col-xs-12 related-items pd-0-xs visible-xs">

                        @if($memeberdetails->member_type == 'premium')
                            <h4 class="section-title-1 related">More Ads
                                <span>by
                                    <a href="{{asset('')}}{{$memeberdetails->slug}}">{{$memeberdetails->company_name}}</a>
                                </span>
                            </h4>
                        @else
                            <h4 class="section-title-1 related">Some Related Ads</h4>
                        @endif
                        {{--<p class="small pull-right">--}}
                        {{--<a href="{{ URL::previous() }}"><u><i class="ion-ios-plus-outline"></i> See More</u></a>--}}
                        {{--</p>--}}
                        <div class="container">
                            <div class="row">
                            @foreach($relatedads as $key=>$relatedad)
                                <!--one related item-->
                                    <a href="{{asset('')}}ad/{{$relatedad->slug}}">

                                        <div class="col-md-3 col-xs-12 pd-0-xs">
                                            <div class="item-box">
                                                <div class="img-hover-style js-image-click col-xs-3 pd-0-xs">
                                                    <!--<img class="img-responsive" src="assets/img/uploads/phone.jpg">-->
                                                    <div class="img-class-to-wrap-related pull-left"
                                                         style="background-image: url({{asset('')}}salenow/images/uploads/{{$relatedad->adid}}/thumb/{{$relatedad->featuredimage}});">
                                                        <div class="overlay hidden-xs">
                                                            <span class="plus"> <i class="ion-ios-search"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="caption pull-right col-xs-9">
                                                    <h2 class="col-xs-12 group inner list-group-item-heading"
                                                        title="{{$relatedad->adtitle}}">
                                                        {{$relatedad->adtitle}}
                                                    </h2>
                                                    <ul class="prime-features prime-xs">
                                                        {!! $relatedad->mileage > 0 ? '<li>'.number_format($relatedad->mileage).'KM</li>' : '' !!}
                                                        {!! !empty($relatedad->fuel_name)? '<li>'.ucwords($relatedad->fuel_name).'</li>':'' !!}
                                                        {!! !empty($relatedad->landsize)? '<li>'.ucwords($relatedad->landsize).'</li>':''!!}
                                                        {!! !empty($relatedad->propertysize)? '<li>'.ucwords($relatedad->propertysize).'</li>':''!!}
                                                        {!! !empty($relatedad->beds)? '<li>'.ucwords($relatedad->beds).' Bedrooms</li>':''!!}
                                                        {!! !empty($relatedad->baths)? '<li>'.ucwords($relatedad->baths).' Bathrooms</li>':''!!}
                                                    </ul>
                                                    <h3 class="item-price">
                                                        Rs. {{$relatedad->price > 0 ? number_format($relatedad->price) : '' }}</h3>
                                                    <p class="item-loc hidden-lg hidden-md"><i
                                                                class="ion-ios-location-outline"></i> {{$relatedad->city}}
                                                        <span
                                                                class="pd-l-5"><i
                                                                    class="ion-ios-stopwatch-outline"></i> {{$ad_details->created_at->formatLocalized('%d %B %Y') }}</span>
                                                    </p>
                                                    <div class="clear-fix"></div>
                                                </div>
                                                <div class="clear-fix"></div>
                                            </div>
                                        </div>
                                        <div class="clear-fix"></div>
                                    </a>
                                    <!--end one related item-->
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <!--Email to friend popup-->
    <div class="modal fade inquiry send-to-friend" tabindex="-1" role="dialog" id="modalsendtofriend"
         aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="ion-ios-close-empty"></i>
                    </button>
                    <h4 class="modal-title text-center"><span class="no-1"><i
                                    class="ion-paper-airplane"></i> </span> &nbsp;Email to Friend</h4>
                </div>
                <div class="col-lg-12 col-xs-12 pd-0-xs">
                    <form role="form" id="email-to-friend-form" class="inquiry-form">
                        <input type="hidden" id="adid" name="adid"
                               value="{{(!empty($ad_details->adid))?$ad_details->adid:''}}">
                        <input type="hidden" name="category_code" id="category_code"
                               value="{{$ad_details->category_code}}">
                        <div class="">
                            <div class="col-md-2 col-xs-12">
                                <div class="form-group">
                                    <label for="">To</label>
                                </div>
                            </div>
                            <div class="col-md-10 col-xs-12">
                                <div class="form-group">
                                    <input type="email" class="form-control" name="to_email" autocomplete="off"
                                           id="to_email"
                                           placeholder="Friend's E-mail Address">
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="sender_name" autocomplete="off"
                                           id="sender_name"
                                           value="{{(!empty(getMember())?getMember()->first_name.' '.getMember()->last_name:'')}}"
                                           placeholder="Name">
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="col-md-12 mg-b-10 col-xs-12">
                                <div class="form-group">
                                <textarea class="form-control textarea" rows="1" name="message" id="message"
                                          placeholder="Your Message"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="clear-fix"></div>
                        <div class="email-to-friend-btn">
                            <div class="col-md-12 top-mar-20">
                                <button type="button" id="email-to-friend"
                                        class="btn main-btn pull-right email-to-friend">Send
                                </button>
                            </div>
                        </div>
                        <div class="clear-fix"></div>
                    </form>
                </div>
                <div class="clear-fix"><br></div>
            </div>
        </div>
    </div>
    {{ Html::script('js/salenow/plugin/jquery.validate.js') }}
    <!-- FlexSlider Detail page inner-->
    {{ Html::style('css/salenow/plugin/inner-slider/flexslider.min.css') }}
    {{ Html::script('js/salenow/plugin/inner-slider/flexslider.js') }}
    {{ Html::script('plugins/popup-gallery/popup.js') }}
    {{--iem slider related ads start--}}
    {{ Html::script('plugins/slick/slick.js') }}
    {{--light box--}}
    {{ Html::script('plugins/lightbox_2/simple-lightbox.js') }}
    {{--Alerts--}}
    <script src="{{ asset("backend/assets/js/sweetalert.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("plugins/blockui/jquery.blockUI.js")}}"></script>
    <script src="{{asset('plugins/default/platform.js')}}" async defer></script>
    <script type="text/javascript">
        $(document).on('ready', function () {
            $(".regular").slick({
                dots: true,
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 4,
                infinite: false
            });

            $('#phone_number').click(function () {
                var _token = $('input[name="_token"]').val();
                var type = $(this).attr('visittype');
                var id = $(this).attr('adid');
                $.ajax({
                    type: 'post',
                    url: '/visitor-click',
                    data: {
                        '_token': _token,
                        'type': type,
                        'adid': id
                    }
                })

                $(this).css("display", "none")
                $('.loading_gif').show();
                setTimeout(function () {
                    $('.loading_gif').css("display", "none");
                }, 500);
                setTimeout(function () {
                    $('.show_tel').css("display", "block");
                }, 500);
            });


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $("#email-to-friend-form").validate({
                rules: {
                    to_email: {
                        required: true,
                        email: true
                    },
                    sender_name: {
                        required: true,
                    },
                },
                messages: {
                    to_email: {
                        required: "Please Enter Receiver's E-Mail Address !",
                    },
                    sender_name: {
                        required: "Please Enter Your Name!",
                    }
                },
            });
            $('#email-to-friend').click(function () {
                if ($("#email-to-friend-form").valid()) {
                    var _token = $('input[name="_token"]').val();
                    var adid = $("#adid").val();
                    var category_code = $("#category_code").val();
                    $.ajax({
                        type: 'post',
                        url: '/ad/email-to-friend',
                        data: $("#email-to-friend-form").serializeArray(),
                        beforeSend: function (d) {
                            $('#modalsendtofriend').block({
                                message: '<h5>Sending...</h5>', css: {
                                    border: 'none',
                                    padding: '15px',
                                    backgroundColor: '#000',
                                    '-webkit-border-radius': '5px',
                                    '-moz-border-radius': '5px',
                                    opacity: .5,
                                    color: '#fff'
                                }
                            });
                        },
                        success: function (res) {
                            $.unblockUI();
                            swal("Grate!", "Suggestion Email Sent!", "success").then((value) => {
                                $('#modalsendtofriend').modal('toggle');
                        });
                        }
                    });
                }
            });
            /***************************************************************************************************************************************************************
             *
             ***************************************************************************************************************************************************************/
//            $.ajaxSetup({
//                headers: {
//                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                }
//            });
//            $('#btn_submit_signup').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
//            $("#inquiry-form").validate({
//                rules: {
//                    name: "required",
//                    message: "required",
//                    email: {
//                        required: true,
//                        email: true
//                    },
//                    mobile: {
//                        required: true,
//                        number: true
//                    }
//                }
//            });
//
//            $("#inquiry_post_ad").click(function (e) {
//                e.preventDefault();
//                if($("#inquiry-form").valid()){
//                    var formdata = $("#inquiry-form").serializeArray();
//                    $.ajax({
//                        type: "POST",
//                        url: '/member/sendinquirymail',
//                        data: formdata,
//                        success: function (res) {
//                            if (res.errors) {
//                                var li = '';
//                                $.each(res.errors, function (key, value) {
//                                    li += '<li>' + value + '</li>'
//                                });
//                                $('#error-message').html(li);
//                                $('#error-div').removeClass("hide");
//                                $('html, body').animate({
//                                    scrollTop: $(".category-title").offset().top
//                                }, 1000);
//                                return false;
//                            }
//                            if (res.response.message == 'success') {
//                                $('#modalinquiry').modal('hide')
//                            }
//                        }
//                    });
//                }
//            });
        });

        $(function () {
            var $gallery = $('.gallery a').simpleLightbox();
        });

        //Details page image slider
        $(window).load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                controlNav: "thumbnails"
            });
        });

        (function ($) {
            $.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {
                e.preventDefault();
                intWidth = intWidth || '500';
                intHeight = intHeight || '400';
                strResize = (blnResize ? 'yes' : 'no');
                var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
                    strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize,
                    objWindow = window.open(this.attr('href'), strTitle, strParam).focus()
            }
            $(document).ready(function ($) {
                $('.customer.share').on("click", function (e) {
                    $(this).customerPopup(e)
                })
            })
        }(jQuery))
    </script>
    {{--light box--}}
    <script>
        gtag('event', 'page_view', {
            'send_to': 'AW-834902320',
            'dynx_itemid': '{{(!empty($ad_details->adid))?$ad_details->adid:''}}',
            'dynx_itemid2': '',
            'dynx_pagetype': 'other',
            'dynx_totalvalue': '{{(!empty($ad_details->price))?$ad_details->price:''}}'
        });

    </script>
@endsection