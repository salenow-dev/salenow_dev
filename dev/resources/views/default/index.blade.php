@extends('layouts.saleme_home')
@section('title', 'Cars, Electronics, Property and Jobs in Sri Lanka Used New Sale')
@section('content')

    @if(is_null($image))
        <img id="random-hero-img" class="img-responsive"
             src="{{asset('')}}images/homepage/main-banner/1.jpg">
    @else
        <img id="random-hero-img" class="img-responsive"
             src="{{asset('')}}images/homepage/main-banner/{{$image->filename}}">
    @endif

    <section class="home-search-div cont-bg1 hidden-xs">
        <div class="container">
            <div class="row">
                <div class="">
                    <form id="search_form1" method="GET" action="{{asset('')}}ads">
                        <div class="  search-panel1 search-form-wrapper">
                            <div class="input-group home-search1">
                                <div class="input-group-btn ">
                                    <button type="button" class="btn location-btn " data-toggle="modal"
                                            data-target=".select-city-modal">
                                        <span class="s-cat lnr lnr-pointer-down"></span> <span id="search_concept">&nbsp;Select City</span>
                                    </button>
                                    <button type="button" class="btn cat-btn" data-toggle="modal"
                                            data-target=".select-category-modal">
                                        <span class=" s-cat lnr lnr-tag"></span> <span
                                                id="search_concept">&nbsp;All Category</span>
                                    </button>
                                </div>
                                <input type="text" class="form-control search-txt" name="query"
                                       placeholder="What you looking for...">
                                <span class="input-group-btn">
                                <button class="btn  search-btn" id="more_query1" type="button"><span
                                            class="lnr lnr-magnifier"></span></button>
                            </span>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <!--Search start-->
    {{--desktop--}}


    {{--<div class="callcenterBox ">--}}
    {{--<div class="boxcontent">--}}
    {{--<p>Need help?</p>--}}
    {{--<p class="number">Call us at<br>--}}
    {{--<span class="block">--}}
    {{--<a class="call" href="tel:0115818188" id="callTollFree">011-5 81 81 88</a>--}}
    {{--</span>--}}
    {{--</p>--}}
    {{--<p class="color-1">(24 hours all 7 days)</p>--}}
    {{--</div>--}}
    {{--<a href="javascript:void(0)" class="slider"></a>--}}
    {{--</div>--}}
    <!--search mobile-->
    {{--mobile--}}


    <!--Search mobile end-->
    <!--Search end-->
    <div class="mar-70 visible-xs">
        {{--{{ Html::image('images/salenow/banner.gif', 'saleme.lk', array('class' => 'img-responsive')) }}--}}
    </div>

    <div class="mobile-search-div visible-xs">
        <div class="col-md-12 no-padding">
            <div class="inner-serch-div" data-toggle="modal"
                 data-target=".mobile-search-pop">
                <i class="ion-ios-search-strong"></i>
                <p>I'm looking for...</p>
            </div>
        </div>
        <div class="col-md-12">
            <a href="{{(checkMember())?asset('').'addtype':'/member-login'}}">
                <button class="btn btn-block post-ad-mobi-home" type="button">Post Free Ad</button>
            </a>

        </div>
    </div>
    <!--category icons-->
    <section class="bg-fff hidden-xs">
        <div class="container cont-bg">
            <div class="row icon-row">
                <h1 class="promo-head3 text-center featured-title"
                    style="">Featured
                    Categories</h1>
                <p class="text-center promo-sub-1"
                   style='    font-size: 18px;    margin: 2px 0px 20px;    font-weight: 300;    color: #555;    font-family: "Work Sans", sans-serif;'>
                    All Your Needs – Right At Your Fingertips</p>
            </div>

            <div class="row" style="margin-bottom: 50px">
                @for($x=0;$x<4;$x++)
                    <a href="{{asset('')}}ads/srilanka/{{$allcategory[$x]['slug']}}">
                        <div class="col-md-3 cat-item-fea col-xs-6 col-sm-3">
                            <img src="{{asset('')}}images/homepage/{{$allcategory[$x]['slug']}}.jpg" alt=""
                                 class="img-responsive">
                            {{--<p>Cars & Vehicals</p>--}}
                            <div class="infodiv">
                                <h3>{{$allcategory[$x]['category_name']}}</h3>
                                <p>{{($allcategory[$x]['all_ads_count'])? $allcategory[$x]['all_ads_count']:0 }} Ads</p>
                            </div>
                        </div>
                    </a>
                @endfor
            </div>
        </div>

        <div class="container-fluid"
             style="background: rgba(238, 238, 238, 0.48);;padding-top: 15px; padding-bottom: 15px">
            <div class="row">
                <div class="col-md-12 other-cat">
                    @for($x=4;$x<count($allcategory);$x++)
                        <a href="{{asset('')}}ads/srilanka/{{$allcategory[$x]['slug']}}">
                            <div class="row-item" style=" width: {{100/(count($allcategory)-4)}}%;">
                                <img src="{{asset('')}}images/homepage/icon/{{$allcategory[$x]['slug']}}.png" alt=""
                                     class="img-responsive">
                                <p class="text-center">{{$allcategory[$x]['category_name']}}</p>
                                <span class="text-center">({{($allcategory[$x]['all_ads_count'])? $allcategory[$x]['all_ads_count']:0 }}
                                    )</span>
                            </div>
                        </a>
                    @endfor

                </div>

            </div>

        </div>

    </section>
    {{--mobile icons--}}
    <section class="bg-fff visible-xs">
        <div class="container main-cat-mobi">
            <div class="row icon-row">
                <h4 class="promo-head3 text-center "
                    style="margin: 25px 0px 15px;letter-spacing: 1px;">Featured
                    Categories</h4>

            </div>

            <div class="row featured-row">
                @for($x=0;$x<4;$x++)
                    <a href="{{asset('')}}ads/srilanka/{{$allcategory[$x]['slug']}}">
                        <div class="col-md-3 cat-item-fea col-xs-6 col-sm-3"
                             style="margin-bottom: 16px;padding-right: 8px;padding-left: 8px;">
                            <img src="{{asset('')}}images/homepage/{{$allcategory[$x]['slug']}}.jpg" alt=""
                                 class="img-responsive">
                            {{--<p>Cars & Vehicals</p>--}}
                            <div class="infodiv">
                                <h5>{{$allcategory[$x]['category_name']}}</h5>
                                <h6>{{($allcategory[$x]['all_ads_count'])? $allcategory[$x]['all_ads_count']:0 }}
                                    Ads</h6>
                            </div>
                        </div>
                    </a>
                @endfor
            </div>
        </div>

        <div class="container-fluid other-cat-div">
            <div class="row">
                <div class="col-md-12 other-cat">
                    @for($x=4;$x<count($allcategory);$x++)
                        <a href="{{asset('')}}ads/srilanka/{{$allcategory[$x]['slug']}}">
                            <div class="row-item" style=" width: 50%">
                                <img src="{{asset('')}}images/homepage/icon/{{$allcategory[$x]['slug']}}.png" alt=""
                                     class="img-responsive" style="width: 30px !important;">
                                <p class="text-center" style="font-size: 11px">{{$allcategory[$x]['category_name']}}</p>
                                <span class="text-center"
                                      style="font-size: 10px">({{($allcategory[$x]['all_ads_count'])? $allcategory[$x]['all_ads_count']:0 }}
                                    )</span>
                            </div>
                        </a>
                    @endfor
                </div>
            </div>
        </div>
    </section>
    {{--end mobile--}}
    <style>
        .mobile-search-div {
            background: rgba(238, 238, 238, 0.48);
        }

        .mobile-search-div i {
            font-size: 19px;
            color: #fea502;
        }

        .post-ad-mobi-home {
            padding: 10px;
            font-size: 18px !important;
            background: #fea502;
            color: #fff;
            margin-top: 15px;
        }

        .mobile-search-div p {
            font-size: 17px;
            padding-left: 5px;
            display: inline;
        }

        .mobile-search-div .inner-serch-div {
            background: #fff;
            padding: 15px;
            box-shadow: 0 2px 8px rgba(0, 0, 0, 0.1);
        }

        .row-item {

            float: left;
            /*background: #eee;*/
            padding: 10px 0px;
        }

        .row-item:hover {
            background: #dcdada;
        }

        .row-item p {
            font-size: 14px;
            padding-top: 10px;
            font-weight: 400;
            margin: 0;
        }

        .featured-row {
            margin-bottom: 25px
        }

        .row-item span {
            display: block;
            color: #444;
            font-size: 13px;
        }

        .other-cat img {
            width: 50px;
            margin: 0px auto;
            display: block;
        }

        .cat-item-fea {
            position: relative
        }

        .cat-item-fea img {
            width: 100%;
        }

        .cat-item-fea:hover img {
            filter: saturate(3);
            /*border-bottom: 9px solid #ff0000;*/
        }

        .infodiv {
            position: absolute;
            bottom: 0px;
        }

        .infodiv h3 {
            color: #fff;
            font-weight: 600;
            padding-left: 15px;
            margin: 0
        }

        .infodiv h5 {
            color: #fff;
            font-weight: 600;
            padding-left: 5px;
            margin: 0;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        .infodiv h6 {
            color: #fea502;
            font-weight: 400;
            padding-left: 5px;
            margin: 0;
            padding-bottom: 5px;
        }

        .infodiv p {
            padding-left: 15px;
            font-size: 18px;
            color: #fea502;
            font-weight: 400;
        }

        .featured-title {
            font-size: 30px;
            margin: 25px 0px 5px;
            letter-spacing: 1px;
        }

        .other-cat-div {
            background: rgba(233, 232, 232, 0.9);
            padding-top: 15px;
            padding-bottom: 15px
        }

        /*model*/
        .mobile-search-pop .modal-dialog {
            margin: 0px;
            height: 100%;
        }

        .mobile-search-pop .modal-content {
            height: 100% !important;
            /*border-color: #fff !important;*/
            border: 0px !important;
            background: #f0f0f0;
        }

        .mobile-search-pop .modal-header {
            background: #383838;
            padding: 12px;
        }

        .mobile-search-pop .modal-header .close {
            text-shadow: none;
            font-size: 34px;
            opacity: 1 !important;
        }

        .mobile-search-pop .modal-header .modal-title {
            font-size: 23px !important;
            color: #fff;
            font-weight: 300;
        }

        .location-btn1,
        .cat-btn1 {
            width: 100%;
            padding: 15px;
            background: #414141;
            margin: 10px 0px;
            color: #fff;
            border-radius: 4px !important;
            font-weight: 500;
            font-size: 18px;
        }

        .search-txt-xs {
            height: 50px;
            border: 1px solid #d8d7d7;
            border-radius: 4px;
            font-size: 17px;
        }

        .search-btn-xs {
            margin-top: 10px;
            border-radius: 4px !important;
            padding: 15px !important;
            font-size: 18px !important;
            background: #3baa33;
            color: #fff;
        }

        .main-cat-mobi {
            background: rgba(238, 238, 238, 0.48);
        }
    </style>

    {{--mobile pop serch--}}
    <div class="fade modal in mobile-search-pop" aria-labelledby=myLargeModalLabel role=dialog tabindex=-1
         style="top: 0px !important;">
        <div class="modal-dialog modal-md">
            <div class=modal-content>
                <div class=modal-header>
                    <button class=close type=button data-dismiss=modal><i class="ion-android-close"></i></button>
                    <h4 class="text-center modal-title">Search</h4></div>

                <div class="">
                    <div class="col-xs-6 pd-r-5">
                        <div class="input-group home-search1">
                            <div class="input-group-btn ">
                                <button type="button" class="btn location-btn1 " data-toggle="modal"
                                        data-target=".select-city-modal">
                                    <span id="search_concept">City</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 pd-l-5">
                        <div class="input-group home-search1">
                            <div class="input-group-btn ">
                                <button type="button" class="btn cat-btn1" data-toggle="modal"
                                        data-target=".select-category-modal">
                                    <span id="search_concept">Category</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="clear-fix"></div>
                    <div class="col-xs-12 " style="padding: 0px 10px !important;">
                        <form id="search_form" method="GET" action="{{asset('')}}ads">
                            <div class="">
                                <input type="text" class="form-control search-txt-xs" name="query"
                                       placeholder="I'm looking for...">
                            </div>
                            <button type="submit" id="more_query" class="search-btn-xs btn btn-lg btn-block">
                                <i class="ion-ios-search-strong"></i>&nbsp;Search
                            </button>
                        </form>
                    </div>
                </div>


                <div class=clear-fix></div>
            </div>
        </div>
    </div>
    {{--mobile pop serch--}}

    <!--category icons-->
    @if(!empty(\Illuminate\Support\Facades\Session::get('ad_slug')))
        <script>
            $(function () {
                window.location = '/member-login';
            });

        </script>
    @endif
@endsection

 
