<div class="fade modal in location select-city-modal" aria-labelledby=myLargeModalLabel role=dialog tabindex=-1>
    <div class="modal-dialog modal-md">
        <div class=modal-content>
            <div class=modal-header>
                <button class=close type=button data-dismiss=modal><i class=ion-ios-close-empty></i></button>
                <h4 class="text-center modal-title"><span class=no-1><i class=ion-ios-location-outline></i> </span> 
                    Select Your City</h4></div>
            <div id=custom-search-input-desktop>
                <div class="col-md-12 input-group loc-pop-search">
                    <div class="col-xs-12 col-md-6 col-sm-8 pull-right">
                        <div id=custom-search-input>
                            <div class="col-md-12 input-group"><input id=search name=query class="form-control input-lg"
                                                                      placeholder="Type Your City Here"> <span
                                        class=input-group-btn><button class="btn btn-info btn-lg" type=button><i
                                                class=ion-ios-search-strong></i></button></span></div>
                        </div>
                    </div>
                </div>
                <ul class="list-group search-result"><h3 class=text-center
                                                         id=all-srilanka-txt></h3>@if(!empty($citylist)) @foreach($citylist as $city)
                        <a href="{{asset('')}}ads/{{strtolower(trans($city->slug))}}{{!empty($queryString)?$queryString:''}}">
                            <li class=list-group-item><i class="loc-io ion-ios-location-outline"></i>
                                 {{$city->city_name }}</a>@endforeach @endif</ul>
                <div class=clear-fix></div>
            </div>
            <div class="col-md-5 col-xs-5 district-list popular-city">
                <div>
                    <a href="{{asset('')}}ads/new-zealand/{{!empty($scategory_slug) ? $scategory_slug :!empty($category_slug) ? $category_slug:''}}{{!empty($queryString)?$queryString:''}}">
                        <p class=allsrilankalink> All New Zealand <i class="loc-io ion-ios-arrow-right pull-right"></i>
                        </p></a>
                    <h3 class=pop-title-1>All Regions</h3>@if(!empty($citywithdis))
                        <div class="col-xs-12 pd-r-0">@foreach ($alldidcity as $discity)<p class="accordion-toggle loc"
                                                                                           data-parent=#accordion3
                                                                                           data-toggle=collapse
                                                                                           href="#d_{{$discity->id}}">
                                 {{$discity->district_name}} <i class="loc-io ion-ios-arrow-right pull-right"></i>
                            </p>@endforeach</div>@endif</div>
                <div class=clear-fix></div>
            </div>
            <div class="city-list-right col-md-7 col-xs-7 res-div">
                <div class=panel-group id=accordion3>
                    <div class="panel panel-default">@if(!empty($alldidcity)) @foreach($alldidcity as $key=>$discity)
                            <div class="collapse panel-collapse" id="d_{{$discity->id}}">
                                <div class="panel-body pd-t-5"><h3
                                            class="text-center m-t-0 pop-title">{{$discity->district_name}}</h3>
                                    <ul class=loc-city-list>
                                        <li>
                                            <a href="{{asset('')}}ads/{{$discity->slug}}/{{!empty($category_slug) ? $category_slug :''}}{{!empty($queryString)?$queryString:''}}"><i
                                                        class="loc-io ion-ios-location"></i>  all
                                                of {{$discity->district_name}}</a>
                                        </li>@if(!empty($discity->cities)) @foreach($discity->cities as $dcity)
                                            <li>
                                                <a href="{{asset('')}}ads/{{$dcity->slug}}/{{!empty($category_slug) ? $category_slug :''}}{{!empty($queryString)?$queryString:''}}"><i
                                                            class="loc-io ion-ios-location"></i>  {{$dcity->city_name}}
                                                </a></li>@endforeach @endif</ul>
                                    <div class=clear-fix></div>
                                </div>
                            </div>@endforeach @endif</div>
                </div>
            </div>
            <div class=clear-fix></div>
        </div>
    </div>
</div>
<div class="fade modal select-cat-modal select-category-modal" aria-labelledby=myLargeModalLabel role=dialog
     tabindex=-1>
    <div class="modal-dialog modal-md">
        <div class=modal-content>
            <div class=modal-header>
                <button class=close type=button data-dismiss=modal><i class=ion-ios-close-empty></i></button>
                <h4 class="text-center modal-title"><span class=no-1><i class=ion-ios-settings></i> </span>  Select
                    Category</h4></div>
            <div class=panel-group id=accordion>
                <div class="panel panel-default">
                    <div class=panel-heading><a
                                href="{{asset('')}}ads/{{!empty($location_slug) ? $location_slug :'sri-lanka'}}/{{!empty($queryString)?$queryString:''}}">
                            <h4 class=panel-title><i class="cat-icons-big ion-social-buffer-outline"></i>   All
                                Categories <span class="pull-right ion-ios-arrow-right cat-pop-right-arrow"></span></h4>
                        </a></div>
                </div>@if(!empty($allcategory)) @foreach($allcategory as $key=>$allcate)
                    <div class="panel panel-default">
                        <div class=panel-heading data-parent=#accordion data-toggle=collapse
                             href="#cat_{{$allcate->id}}"><h4 class=panel-title><i
                                        class="cat-icons-big {{$allcate->icon}}"></i>   {{$allcate->category_name}}<span
                                        class="pull-right ion-ios-arrow-right cat-pop-right-arrow"></span></h4></div>
                        <div class="collapse panel-collapse" id="cat_{{$allcate->id}}">
                            <div class=panel-body>
                                <table class=table>
                                    <tr>
                                        <td><i class="loc-io ion-ios-pricetag"></i>   <a
                                                    href="{{asset('')}}ads/{{!empty($location_slug) ? $location_slug :'sri-lanka'}}/{{$allcate->slug}}{{!empty($queryString)?$queryString:''}}"
                                                    class=sub-cat-txt>All {{$allcate->category_name}}</a>
                                    </tr>@if(!empty($allcate->subcategories)) @foreach($allcate->subcategories as $key=>$suballcate)
                                        <tr>
                                            <td><i class="loc-io ion-ios-pricetag"></i>   <a
                                                        href="{{asset('')}}ads/{{!empty($location_slug) ? $location_slug :'sri-lanka'}}/{{$suballcate->slug}}{{!empty($queryString)?$queryString:''}}"
                                                        class=sub-cat-txt>{{$suballcate->sub_category_name}}</a>
                                        </tr>@endforeach @endif</table>
                            </div>
                        </div>
                    </div>@endforeach @endif</div>
        </div>
    </div>
</div>
<div class="fade modal inquiry inquiry-model" id=modalinquiry aria-labelledby=myLargeModalLabel role=dialog tabindex=-1>
    <div class="modal-dialog modal-md">
        <div class=modal-content>
            <div class=modal-header>
                <button class=close type=button data-dismiss=modal><i class=ion-ios-close-empty></i></button>
                <h4 class="text-center modal-title"><span class=no-1><i class=ion-paper-airplane></i> </span> Make an
                    Inquiry</h4></div>
            <div class="col-xs-12 col-lg-12 pd-0-xs">
                <form class=inquiry-form id=inquiry-form role=form><input id=ad_id name=ad_id type=hidden
                                                                          value="{{(!empty($ad_details->adid))?$ad_details->adid:''}}">
                    <div>
                        <div class="col-xs-12 col-md-6">
                            <div class=form-group><input id=name name=name class=form-control placeholder=Name
                                                         autocomplete=off></div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class=form-group><input id=mobile name=mobile class=form-control
                                                         placeholder="Mobile Number" autocomplete=off></div>
                        </div>
                        <div class="col-xs-12 col-md-12">
                            <div class=form-group><input id=email name=email class=form-control
                                                         placeholder="Your E-mail Address" autocomplete=off type=email>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="col-xs-12 col-md-12 mg-b-10">
                            <div class=form-group><textarea class="form-control textarea" id=message name=message
                                                            placeholder=Message rows=3></textarea></div>
                        </div>
                    </div>
                    <div class=clear-fix></div>
                    <div>
                        <div class="col-md-12 top-mar-20">
                            <button class="pull-right btn main-btn" type=button id=inquiry_post_ad>Send a message
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>