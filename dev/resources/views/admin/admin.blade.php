@extends('layouts.app')
@section('title', 'User Autherization')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>User</strong> Authorization</h3>

                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <th scope="row">Name</th>
                                <th>E-Mail</th>
                                @if(!empty($roles))
                                    @foreach($roles as $role)
                                        <th>{{$role->name}}</th>
                                    @endforeach
                                @endif
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @if(!empty($roles))
                                    @foreach($users as $user)
                                        <?php $rand = rand(0, 1000); ?>
                                        <tr>
                                            {!! Form::open(['url' => 'user/assignroles']) !!}
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }} <input type="hidden" name="email"
                                                                          value="{{ $user->email }}">
                                            </td>
                                            @foreach($roles as $role)
                                                <td>
                                                    {{--<input type="checkbox" {{ $user->hasRole($role->slug) ? 'checked' : '' }}--}}
                                                    {{--name="role[]" value="{{$role->slug}}">--}}

                                                    <div class="checkbox checkbox-primary">
                                                        <input id="{{$user->id.'_'.$role->id}}" type="checkbox"
                                                               {{ $user->hasRole($role->slug) ? 'checked' : '' }} name="role[]"
                                                               value="{{$role->slug}}">
                                                        <label for="{{$user->id.'_'.$role->id}}">{{$role->name}}</label>
                                                    </div>
                                                </td>
                                            @endforeach
                                            <td>
                                                <input type="submit" value="Save" class="btn btn-success">
                                            </td>
                                            {!! Form::close() !!}
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection