<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }} @yield('title')</title>
    <!-- START third party CSS -->
    <link href="{{ asset("backend/assets/css/theme-default.css") }}" rel="stylesheet">
    <link href="{{ asset("backend/assets/css/icon-font.min.css") }}" rel="stylesheet">
    <link href="{{ asset("backend/assets/css/saleme-backend-custom.css") }}" rel="stylesheet">
    <!-- END third party CSS -->
    <!-- START third party JS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- END third party JS -->
    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset("backend/assets/js/plugins/jquery/jquery.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("backend/assets/js/plugins/jquery/jquery-ui.min.js") }}"></script>
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
<!-- START PAGE CONTAINER -->
<div class="page-container">
    <!-- START PAGE SIDEBAR -->
    <div class="page-sidebar">
        <!-- START X-NAVIGATION -->
        <ul class="x-navigation">
            <li class="xn-logo">
                <a href="{{asset('')}}/dashboard">Saleme.lk</a>
                <a href="#" class="x-navigation-control"></a>
            </li>
            <li class="xn-profile">
                <a href="#" class="profile-mini">
                    <img src="../backend/assets/img/users/avatar.jpg" alt=""/>
                </a>
                <div class="profile">
                    <div class="profile-image">
                        <img src="../backend/assets/img/users/avatar.jpg" alt=""/>
                    </div>
                    <div class="profile-data">
                        <div class="profile-data-name">
                            @if(Auth::check())
                                {{ Auth::user()->name }}
                            @endif
                        </div>
                        @if(Auth::check())
                            <div class="profile-data-title">{{Auth::user()->roles[0]->name}}</div>
                        @endif
                    </div>

                </div>
            </li>
            @include('layouts.backendsidemenu')
        </ul>
        <!-- END X-NAVIGATION -->
    </div>
    <!-- END PAGE SIDEBAR -->
    <!-- PAGE CONTENT -->
    <div class="page-content">
        <!-- START X-NAVIGATION VERTICAL -->
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
            <!-- TOGGLE NAVIGATION -->
            <li class="xn-icon-button">
                <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
            </li>
            <!-- END TOGGLE NAVIGATION -->
            <!-- SEARCH -->
            <li class="xn-search">
                <form role="form">
                    <input type="text" name="search" placeholder="Search..."/>
                </form>
            </li>
            <!-- END SEARCH -->
            <!-- POWER OFF -->
            <li class="xn-icon-button pull-right last">
                <a href="#"><span class="fa fa-power-off"></span></a>
                <ul class="xn-drop-left animated zoomIn">
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li><a href="/user/{{getUserId()}}/edit"><span class="fa fa-lock"></span> My Account</a></li>
                        <li>
                            <a href="{{ url('/logout') }}" class="mb-control"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <span class="fa fa-sign-out"></span>
                                SignOut</a>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    @endif
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                </ul>
            </li>
            <!-- END POWER OFF -->
            <!-- MESSAGES -->
            {{--<li class="xn-icon-button pull-right">--}}
                {{--<a href="#"><span class="fa fa-comments"></span></a>--}}
                {{--<div class="informer informer-danger">4</div>--}}
                {{--<div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">--}}
                    {{--<div class="panel-heading">--}}
                        {{--<h3 class="panel-title"><span class="fa fa-comments"></span> Messages</h3>--}}
                        {{--<div class="pull-right">--}}
                            {{--<span class="label label-danger">4 new</span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="panel-body list-group list-group-contacts scroll" style="height: 200px;">--}}
                        {{--<a href="#" class="list-group-item">--}}
                            {{--<div class="list-group-status status-online"></div>--}}
                            {{--<img src="assets/images/users/user2.jpg" class="pull-left" alt="John Doe"/>--}}
                            {{--<span class="contacts-title">John Doe</span>--}}
                            {{--<p>Praesent placerat tellus id augue condimentum</p>--}}
                        {{--</a>--}}
                        {{--<a href="#" class="list-group-item">--}}
                            {{--<div class="list-group-status status-away"></div>--}}
                            {{--<img src="assets/images/users/user.jpg" class="pull-left" alt="Dmitry Ivaniuk"/>--}}
                            {{--<span class="contacts-title">Dmitry Ivaniuk</span>--}}
                            {{--<p>Donec risus sapien, sagittis et magna quis</p>--}}
                        {{--</a>--}}
                        {{--<a href="#" class="list-group-item">--}}
                            {{--<div class="list-group-status status-away"></div>--}}
                            {{--<img src="assets/images/users/user3.jpg" class="pull-left" alt="Nadia Ali"/>--}}
                            {{--<span class="contacts-title">Nadia Ali</span>--}}
                            {{--<p>Mauris vel eros ut nunc rhoncus cursus sed</p>--}}
                        {{--</a>--}}
                        {{--<a href="#" class="list-group-item">--}}
                            {{--<div class="list-group-status status-offline"></div>--}}
                            {{--<img src="assets/images/users/user6.jpg" class="pull-left" alt="Darth Vader"/>--}}
                            {{--<span class="contacts-title">Darth Vader</span>--}}
                            {{--<p>I want my money back!</p>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    {{--<div class="panel-footer text-center">--}}
                        {{--<a href="pages-messages.html">Show all messages</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</li>--}}
            <!-- END MESSAGES -->
            <!-- TASKS -->
            {{--<li class="xn-icon-button pull-right">--}}
                {{--<a href="#"><span class="fa fa-tasks"></span></a>--}}
                {{--<div class="informer informer-warning">3</div>--}}
                {{--<div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">--}}
                    {{--<div class="panel-heading">--}}
                        {{--<h3 class="panel-title"><span class="fa fa-tasks"></span> Tasks</h3>--}}
                        {{--<div class="pull-right">--}}
                            {{--<span class="label label-warning">3 active</span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="panel-body list-group scroll" style="height: 200px;">--}}
                        {{--<a class="list-group-item" href="#">--}}
                            {{--<strong>Phasellus augue arcu, elementum</strong>--}}
                            {{--<div class="progress progress-small progress-striped active">--}}
                                {{--<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50"--}}
                                     {{--aria-valuemin="0" aria-valuemax="100" style="width: 50%;">50%--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<small class="text-muted">John Doe, 25 Sep 2015 / 50%</small>--}}
                        {{--</a>--}}
                        {{--<a class="list-group-item" href="#">--}}
                            {{--<strong>Aenean ac cursus</strong>--}}
                            {{--<div class="progress progress-small progress-striped active">--}}
                                {{--<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80"--}}
                                     {{--aria-valuemin="0" aria-valuemax="100" style="width: 80%;">80%--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<small class="text-muted">Dmitry Ivaniuk, 24 Sep 2015 / 80%</small>--}}
                        {{--</a>--}}
                        {{--<a class="list-group-item" href="#">--}}
                            {{--<strong>Lorem ipsum dolor</strong>--}}
                            {{--<div class="progress progress-small progress-striped active">--}}
                                {{--<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="95"--}}
                                     {{--aria-valuemin="0" aria-valuemax="100" style="width: 95%;">95%--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<small class="text-muted">John Doe, 23 Sep 2015 / 95%</small>--}}
                        {{--</a>--}}
                        {{--<a class="list-group-item" href="#">--}}
                            {{--<strong>Cras suscipit ac quam at tincidunt.</strong>--}}
                            {{--<div class="progress progress-small">--}}
                                {{--<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0"--}}
                                     {{--aria-valuemax="100" style="width: 100%;">100%--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<small class="text-muted">John Doe, 21 Sep 2015 /</small>--}}
                            {{--<small class="text-success"> Done</small>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    {{--<div class="panel-footer text-center">--}}
                        {{--<a href="pages-tasks.html">Show all tasks</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</li>--}}
            <!-- END TASKS -->
            <!-- LANG BAR -->
            {{--<li class="xn-icon-button pull-right">--}}
                {{--<a href="#"><span class="flag flag-gb"></span></a>--}}
                {{--<ul class="xn-drop-left xn-drop-white animated zoomIn">--}}
                    {{--<li><a href="#"><span class="flag flag-gb"></span> English</a></li>--}}
                    {{--<li><a href="#"><span class="flag flag-de"></span> Deutsch</a></li>--}}
                    {{--<li><a href="#"><span class="flag flag-cn"></span> Chinese</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            <!-- END LANG BAR -->
        </ul>
        <!-- END X-NAVIGATION VERTICAL -->
        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">{{ucwords(Request::segment(1))}}</li>
            <li class="active">{{ucwords(Request::segment(2))}}</li>
        </ul>
        <!-- END BREADCRUMB -->
        <!-- PAGE CONTENT WRAPPER -->
        <div class="page-content-wrap">
            <div class="row">
                <div class="col-md-12">
                    @include('errors.flash')
                    @include('errors.list')
                    <div class="alert alert-danger hide" id="error-div" role="alert">
                        <i class="fa fa-exclamation-triangle"></i>&nbsp;
                        <span class="error-title">Alert! The following errors were found.</span>
                        <hr>
                        <div id="error-message"></div>
                    </div>
                </div>
            </div>
            @yield('content')
        </div>
        <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- Scripts -->
{{--<script src="/js/app.js"></script>--}}
<!-- START SCRIPTS -->
<!-- START PLUGINS -->
{{--<script type="text/javascript" src="{{ asset("backend/assets/js/plugins/jquery/jquery.min.js") }}"></script>--}}
{{--<script type="text/javascript" src="{{ asset("backend/assets/js/plugins/jquery/jquery-ui.min.js") }}"></script>--}}
<script type="text/javascript" src="{{ asset("backend/assets/js/plugins/bootstrap/bootstrap.min.js") }}"></script>
<script type="text/javascript"
        src="{{ asset("backend/assets/js/plugins/jquery-validation/jquery.validate.js") }}"></script>
<!-- END PLUGINS -->

<!-- START THIS PAGE PLUGINS-->
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/icheck/icheck.min.js") }}'></script>
<script type="text/javascript"
        src="{{ asset("backend/assets/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("backend/assets/js/plugins/scrolltotop/scrolltopcontrol.js") }}"></script>
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js") }}'></script>
<script type='text/javascript'
        src='{{ asset("backend/assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js") }}'></script>
<script type='text/javascript'
        src='{{ asset("backend/assets/js/plugins/bootstrap/bootstrap-datepicker.js") }}'></script>
<script type="text/javascript" src="{{ asset("backend/assets/js/plugins/owl/owl.carousel.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("backend/assets/js/plugins/datatables/jquery.dataTables.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("backend/assets/js/plugins/moment.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("backend/assets/js/plugins/daterangepicker/daterangepicker.js") }}"></script>
<!-- END THIS PAGE PLUGINS-->
<script type="text/javascript" src="{{ asset("backend/assets/js/plugins/bootstrap/bootstrap-select.js") }}"></script>
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/noty/jquery.noty.js")}}'></script>
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/noty/layouts/topCenter.js")}}'></script>
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/noty/layouts/topLeft.js")}}'></script>
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/noty/layouts/topRight.js")}}'></script>
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/noty/themes/default.js")}}'></script>
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/blueimp/jquery.blueimp-gallery.min.js")}}'></script>
<script type="text/javascript" src='{{ asset("backend/assets/js/plugins/tagsinput/jquery.tagsinput.min.js")}}'></script>
<script type="text/javascript" src="{{ asset("plugins/blockui/jquery.blockUI.js")}}"></script>
<!-- Init Common functions - public/js/default -->
<script type="text/javascript" src="{{ asset('backend/assets/js/default/init_functions.js') }}"></script>
<!-- START TEMPLATE -->
<script type="text/javascript" src="{{ asset("backend/assets/js/settings.js") }}"></script>
<script type="text/javascript" src="{{ asset("backend/assets/js/plugins.js") }}"></script>
<script type="text/javascript" src="{{ asset("backend/assets/js/actions.js") }}"></script>
{{--<script type="text/javascript" src="{{ asset("backend/assets/js/demo_dashboard.js") }}"></script>--}}
<!-- END TEMPLATE -->
<!-- END SCRIPTS -->

<script type="text/javascript" src="{{ asset("backend/assets/js/plugins/morris/morris.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("backend/assets/js/demo_charts_morris.js") }}"></script>
{{--Alerts--}}
<script src="{{ asset("backend/assets/js/sweetalert.min.js") }}"></script>

</body>
</html>
