@include('main.searchheader')
<?php
$sub_layout = '';
$tableswitch = '';
$js = 'default';
if (!empty($category_code)) {
    switch ($category_code) {
        case 'veh':
            $tableswitch = 'veh';
            break;
        case 'ele':
            $tableswitch = 'ele';
            break;
        case 'pro':
            $tableswitch = 'pro';
            break;
        case 'job':
            $tableswitch = 'job';
            break;
        default:
            $tableswitch = 'default';
    }
}
//dd($subcategory_code);
if (!empty($subcategory_code)) {
    switch ($subcategory_code) {
        case 'car':
            $sub_layout = $subcategory_code;
            break;
        case 'bike':
            $sub_layout = $subcategory_code;
            break;
        case 'three':
            $sub_layout = $subcategory_code;
            break;
        case 'van':
            $sub_layout = $subcategory_code;
            break;
        case 'hea':
            $sub_layout = $subcategory_code;
            break;
        case 'auto-ser':
            $sub_layout = $subcategory_code;
            break;
        case 'auto-acc':
            $sub_layout = $subcategory_code;
            break;
        case 'push':
            $sub_layout = $subcategory_code;
            break;
        case 'boat':
            $sub_layout = $subcategory_code;
            break;
        case 'mobi':
            $sub_layout = $subcategory_code;
            break;
        case 'mobi-acc':
            $sub_layout = $subcategory_code;
            break;
        case 'com-tab':
            $sub_layout = $subcategory_code;
            break;
        default:
        case 'tv':
            $sub_layout = $subcategory_code;
            break;
        case 'cam':
            $sub_layout = $subcategory_code;
            break;
        case 'land':
            $sub_layout = $subcategory_code;
            break;
        case 'aprt':
            $sub_layout = $subcategory_code;
            break;
        case 'com-prop':
            $sub_layout = $subcategory_code;
            break;
        case 'hous':
            $sub_layout = $subcategory_code;
            break;
        case 'holi-rent':
            $sub_layout = $subcategory_code;
            break;
        case 'holi-rent':
            $sub_layout = $subcategory_code;
            break;
        case 'room':
            $sub_layout = $subcategory_code;
            break;
        case 'pets':
            $sub_layout = $subcategory_code;
            break;
        case 'jobs-in-sl':
            $sub_layout = $subcategory_code;
            break;
//        default:
//            $sub_layout = 'default';
    }
}
?>
<!--search section-->

<section>
    <div class="container">
        <div class="row search-results">
            <div class="col-xs-12 visible-xs hidden-md text-center show-filter-div-xs">
                <button class="show-filter-btn-xs" onclick="openNav()">
                    <i class="ion-ios-settings-strong"></i> &nbsp;show filters
                </button>
            </div>
            <div class="clear-fix"></div>
            <form id="filter_form" method="get" action="{{ app('url')->full() }}">
                <input type="hidden" name="query" value="{{!empty($querydata['query'])?$querydata['query']:''}}">
                <div class="col-md-3 col-sm-4 filter-section sidenav" id="mySidenav">
                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">
                        <i class="ion-ios-close-outline"></i>
                    </a>
                    <div class="sort-div visible-xs">
                        <hr class="fade-line">
                        <h3 class="filter-title">Search Filters</h3>
                        <hr class="fade-line">
                    </div>
                    <div class="sort-div">
                        <p class="section-title">Sort result by </p>

                        {{--<hr class="hidden-xs">--}}
                        <select id="sort" name="sort" class="form-control" data-style="btn-new">
                            <option @if($querydata['sort'] == 'desc_date'){{'selected'}} @endif value="desc_date">Time :
                                More recent
                            </option>
                            <option @if($querydata['sort'] == 'asc_price'){{'selected'}} @endif value="asc_price">Price
                                : Lowest first
                            </option>
                            <option @if($querydata['sort'] == 'desc_price'){{'selected'}} @endif  value="desc_price">
                                Price : Highest first
                            </option>
                        </select>
                        <hr class="fade-line">
                    </div>
                    @if(!empty($category_code) && !empty($subcategory_code))
                        @if($category_code == 'veh' OR $category_code == 'ele' OR $category_code == 'pro' OR $category_code == 'job')

                            @include('layouts.filters.filter_'.$sub_layout)

                        @else
                            @include('layouts.filters.filter_default')
                        @endif
                    @endif
                    <style>
                        .no_display{display: none;}
                    </style>
                    <div class="category-div"> <div class="panel-group" id="accordion1"> <div class="panel panel-default"> <div class="panel-heading"> <h4 class="panel-title"> <a class="section-title accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#categories1"> Category </a> </h4> </div><div id="categories1" class="panel-collapse collapse in cat-list"> <div class="panel-body"> <ul class="accordion"> <li> <a class="" href="{{asset('')}}ads/{{!empty($location_slug) ? $location_slug :'sri-lanka'}}{{$queryString}}"> <i class="ion-ios-arrow-left cat-ico"></i> <p>All Categories</p></a> </li>@if(!empty($allcategory)) @foreach($allcategory as $key=>$allcate) <li> <a class="toggle" href="{{asset('')}}ads/{{!empty($location_slug) ? $location_slug :'sri-lanka'}}/{{$allcate->slug}}{{$queryString}}"> <i class="{{$allcate->icon}}"></i> <p>{{$allcate->category_name}}<span class="{{(!empty($category_code))?($allcate->category_code==$category_code)?'no_display':'':''}}"> ({{$allcate->all_ads_count}}) </span> </p></a> <ul class="inner{{(!empty($category_code))?($allcate->category_code==$category_code)?'show':'':''}}"> @if(!empty($allcate->subcategories)) @foreach($allcate->subcategories as $key=>$suballcate) <li> <a href="{{asset('')}}ads/{{!empty($location_slug) ? $location_slug :'sri-lanka'}}/{{$suballcate->slug}}{{$queryString}}" class="inner-link{{(!empty($subcategory_code))?($suballcate->sub_category_code==$subcategory_code) ? ' location-active':'':''}}"> <span class="ion-ios-minus-empty"></span>&nbsp;{{$suballcate->sub_category_name}}<span>({{$suballcate->allads_count}}) </span> </a> </li>@endforeach @endif </ul> </li>@endforeach @endif </ul> </div></div></div></div><hr class="fade-line"> </div><div class="location-div"> <div class="panel-group" id="accordion12"> <div class="panel panel-default"> <div class="panel-heading"> <h4 class="panel-title"> <a class="section-title accordion-toggle" data-toggle="collapse" data-parent="#accordion12" href="#locations"> Location </a> </h4> </div><div id="locations" class="panel-collapse loc-list"> <div class="panel-body"> <ul class="accordion"> <li> <a class="" href="{{asset('')}}ads/sri-lanka/{{!empty($scategory_slug) ? $scategory_slug :!empty($category_slug) ? $category_slug:''}}{{$queryString}}"> <i class="ion-ios-arrow-left cat-ico"></i> <p>All Of Sri Lanka<span></span></p></a> </li>@if(!empty($citywithdis)) @foreach($citywithdis as $discity) @if(count($discity->cities)) <li> <a class="toggle" href="{{asset('')}}ads/{{$discity->slug}}/{{!empty($category_slug) ? $category_slug :''}}{{$queryString}}"> <i class="ion-ios-location-outline cat-ico "></i> <p>{{$discity->district_name}}<span @if(!$showCity) style="display:none" @endif> ({{$discity->allads_count}}) </span> </p></a> <ul class="inner @if(!empty($discity->slug)) @if(!empty($location_slug)) @if($discity->slug==$location_slug) show @endif @endif @endif @if(!$showCity)show @endif"> @if(!empty($discity->cities)) @foreach($discity->cities as $dcity) <li> <a href="{{asset('')}}ads/{{$dcity->slug}}/{{!empty($category_slug) ? $category_slug :''}}{{$queryString}}" class="inner-link{{(!empty($location_slug))? ($dcity->slug==$location_slug)? ' location-active':'':''}}"> <span class="ion-ios-arrow-forward"></span>&nbsp;{{$dcity->city_name}}<span> ({{$dcity->alladscity_count}}) </span> </a> </li>@endforeach @endif </ul> </li>@endif @endforeach @endif </ul> </div></div></div></div></div>
                </div>
            </form>
            <!--  inner content -->
            @yield('content')
            <div class="col-md-2 search-right-bar hidden-xs hidden-sm">
            {{--                {{ Html::image('images/salenow/advertisments/skyscraper-desktop.jpg', 'saleme.lk', array('class' => 'img-responsive')) }}--}}
            <!-- /21634329919/slm_skyscraper_160x600_atf -->
                <div id='div-gpt-ad-1512040136576-0' style='height:600px; width:160px;'>
                    <script>
                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1512040136576-0'); });
                    </script>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="container large-mobile-buttom">
    <section class=" visible-xs">
        <div class="image-div-xs" align="center">


        {{--<img src="{{asset('/images/salenow/banners/leader-board-mobile.jpg')}}" class="">--}}
        <!-- /21634329919/slm_bottom_970x90 -->
            <div id='div-gpt-ad-1528093226192-0'>
                <script>
                    googletag.cmd.push(function () {
                        googletag.display('div-gpt-ad-1528093226192-0');
                    });
                </script>
            </div>-->

	<a href="https://saleme.lk/enhance-technologies(pvt)-ltd" target="_blank"><img src="https://res.cloudinary.com/saleme-lk/image/upload/v1528085224/ENGANCEGIF2_kh2ula.gif" class="img-responsive">
        </div>
    </section>
    <div class="container visible-md visible-lg">
        <div class="image-div">

        {{-- <img src="{{asset('/images/salenow/banners/leader-board-desktop.jpg')}}" class="img-responsive">--}}
        <!-- /21634329919/slm_bottom_970x90 -->
            <div id='div-gpt-ad-1528093226192-1'>
                <script>
                    googletag.cmd.push(function () {
                        googletag.display('div-gpt-ad-1528093226192-1');
                    });
                </script>
            </div>
        </div>
    </div>
</section>
<script>
    $('#sort').change(function () {
        $('#filter_form').submit();
    });

    $('input[type=radio][name=bedStatus]').on('change', function () {
        switch ($(this).val()) {
            case 'allot' :
                alert("Allot Thai Gayo Bhai");
                break;
            case 'transfer' :
                alert("Transfer Thai Gayo");
                break;
        }
    });


    $('input[type=radio][name=adtype]').on('change', function () {
        $('#filter_form').submit();
    });

    $('#vehi_condition').on('change', function () {
        $('#filter_form').submit();
    });

    $('#vehicle_milage').on('change', function () {
        $('#filter_form').submit();
    });


    //Get the text using the value of select
    var text = $("select[name=sort] option[value='1']").text();
    //We need to show the text inside the span that the plugin show
    $('.bootstrap-select .filter-option').text(text);
    //Check the selected attribute for the real select
    $('select[name=sort]').val();


    $('#vehi_filter1').click(function () {
        $('#filter_form').submit();
    });

    $('#btn_price_range').click(function () {
        $('#filter_form').submit();
    });


    $('#vehicle_type').on('click', 'input[type=radio]', function () {
        $('#filter_form').submit();
    });

    $('#vehicle_brand').on('click', 'input[type=checkbox]', function () {
        $('#filter_form').submit();
    });
    $('#vehi_trans').on('change', function () {
        $('#filter_form').submit();
    });
    $('#vehi_fuel').on('change', function () {
        $('#filter_form').submit();
    });

    $('#vehi_filter2').click(function () {
        $('#filter_form').submit();
    });
    $('#elec_filter1').click(function () {
        $('#filter_form').submit();
    });

    $('#elec_condition').on('change', 'input[type=radio]', function () {
        $('#filter_form').submit();
    });
    $('#vehi_condition').on('change', 'input[type=radio]', function () {
        $('#filter_form').submit();
    });
    $('#mobi_authenticity').on('change', 'input[type=radio]', function () {
        $('#filter_form').submit();
    });


</script>
<script>
    $(document).on('click', '.clickable', function (e) {
        var $this = $(this);
        if (!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp(100);
            $this.addClass('panel-collapsed');
            $this.find('i').removeClass('lnr-chevron-up').addClass('lnr-chevron-down');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown(100);
            $this.removeClass('panel-collapsed');
            $this.find('i').removeClass('lnr-chevron-down').addClass('lnr-chevron-up');
        }
    })

</script>
<script>
    var rangeSlider = function () {
        var slider = $('.range-slider'),
            range = $('.range-slider__range'),
            value = $('.range-slider__value');

        slider.each(function () {

            value.each(function () {
                var value = $(this).prev().attr('value');
                $(this).html(value);
            });

            range.on('input', function () {
                $(this).next(value).html(this.value);
            });
        });
    };

    rangeSlider();

    function openNav() {
        $('#mySidenav').addClass('sidenav-width');
//        document.getElementById("mySidenav").style.width = "280px";
//        document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
    }

    /* Set the width of the side navigation to 0 */
    function closeNav() {
        $('#mySidenav').removeClass('sidenav-width');
        document.getElementById("mySidenav").style.width = "0";
//        document.body.style.backgroundColor = "white";
    }
</script>
@include('main.footer')