@extends('layouts.saleme-shortfooter')
@section('content')
    <!--bootstrap select with search-->
    {{ Html::style('css/salenow/layout/select-search/bootstrap-select.min.css') }}
    <!--END css-->
<div class="container cont-bg">
    <section>
        <div class="row ">
            <div class="post-form">
                <div class="col-md-2 hidden-sm hidden-xs"> <!-- required for floating -->
                    <div class="menulist2 ">
                        @if( !empty($categories))
                            @foreach($categories as $data)
                                @if(!empty($data))
                                    <a href="/selectcat?type={{$ad_type}}#select_{{$data->category_code}}"
                                       data-tooltip="{{$data->category_name}}">
                                        <div class="menu-item2 menu-item-selected2 {{($data->id == $category_id)? 'selected':''}}">
                                            <div class="menu-item-green-bar"></div>
                                            <div class="menu-item-icon">
                                                <span class="{{$data->icon}} cat-ico adicon"></span>
                                            </div>
                                            <div class="menu-item-select-arrow">
                                            </div>
                                        </div>
                                    </a>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
                @yield('content_post_ad')
            </div>
        </div>
    </section>
</div>
<script src="{{ asset("js/location/district_city.js") }}"></script>
@endsection