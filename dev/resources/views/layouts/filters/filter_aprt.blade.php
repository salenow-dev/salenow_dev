@include('layouts.filters.add_type')
<div id="elec_condition">
    @foreach($filters as $filterName => $filterValues)
        @if($filterName == 'Item condition')
            <div class="condition-div">
                <p class="section-title">Condition</p>
                {{--<hr class="hidden-xs">--}}
                @foreach ($filterValues as $id => $filtervalue)
                    <label><input value="{{$id}}" name="comn[filter_condition]"
                                  @if(!empty($querydata['comn_data']['filter_condition'])) @if($querydata['comn_data']['filter_condition'] == $id) {{'checked'}} @endif @endif type="radio"> {{$filtervalue}}
                    </label>
                @endforeach
                <hr class="fade-line">
            </div>
        @endif
    @endforeach
</div>
<div class="sort-div">
    <p class="section-title">Filter By</p>
    {{--<hr class="hidden-xs">--}}
    @foreach($filters as $filterName => $filterValues)
        @if($filterName == 'Baths')
            <div class="col-sm-12 col-xs-12 pd-0-xs  filter-brand-div">
                <div class="panel filter-pannel">
                    <div class="panel-heading filter-heading text-left clickable panel-collapsed">
                        <h3 class="panel-title clickable panel-collapsed pull-left">{{$filterName}}</h3>
                        <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                        <div class="clear-fix"></div>
                    </div>

                    <div class="panel-body filter-body" id="elec_service_type"
                         style="{{!empty($querydata['prop_data']['Baths'])?'display:block;':'display: none;'}}">
                        @foreach ($filterValues as $id => $filtervalue)
                            <div class="checkbox  checkbox-primary">
                                <input id="{{$id}}"
                                       @if(!empty($querydata['prop_data']['Baths'])  && is_array($querydata['prop_data']['Baths']))  @if(in_array($id,$querydata['prop_data']['Baths'])) {{'checked'}}@endif @endif name="prop[Baths][]"
                                       value="{{$id}}" type="checkbox">
                                <label for="{{$id}}">&nbsp;{{$filtervalue}}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @elseif($filterName == 'Beds')
            <div class="col-sm-12 col-xs-12 pd-0-xs filter-brand-div">
                <div class="panel filter-pannel">
                    <div class="panel-heading filter-heading text-left clickable panel-collapsed">
                        <h3 class="panel-title clickable panel-collapsed pull-left">{{$filterName}}</h3>
                        <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                        <div class="clear-fix"></div>
                    </div>
                    <div class="panel-body filter-body" id="elec_service_type"
                         style="{{!empty($querydata['prop_data']['Baths'])?'display:block;':'display: none;'}}">
                        @foreach ($filterValues as $id => $filtervalue)
                            <div class="checkbox  checkbox-primary">
                                <input id="{{$id}}"
                                       @if(!empty($querydata['prop_data']['Baths'])  && is_array($querydata['prop_data']['Baths']))  @if(in_array($id,$querydata['prop_data']['Baths'])) {{'checked'}}@endif @endif name="prop[Baths][]"
                                       value="{{$id}}" type="checkbox">
                                <label for="{{$id}}">&nbsp;{{$filtervalue}}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
    @endforeach
    <div class="col-sm-12 col-xs-12 pd-0-xs filter-model-div">
        <div>
            <input name="prop[property_size]" type="text" class="model-input-search2 form-control" placeholder="Size">
        </div>
    </div>
    <div class="clear-fix"></div>
    <button type="button" id="elec_filter1" class="btn btn-warning pull-right filter-btn-2">Filter</button>
    <div class="clear-fix"></div>
    <hr class="fade-line">
</div>


@include('layouts.filters.price_range')


