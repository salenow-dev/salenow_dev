{{--{{dd($querydata)}}--}}
<div class="budget-div">
    <p class="section-title">Budget</p>
    {{--<hr class="hidden-xs">--}}
    {{--<p class="pr-title">From</p>--}}
    {{--<div class="range-slider">--}}
    {{--<input class="range-slider__range" name="f_price"--}}
    {{--value="@if(!empty($querydata['from_price'])){{$querydata['from_price']}}@else{{0}}@endif" min="0"--}}
    {{--max="100000" type="range" step="100">--}}
    {{--<span class="range-slider__value">@if(!empty($querydata['from_price'])){{$querydata['from_price']}}@else {{0}} @endif</span>--}}
    {{--</div>--}}

    {{--<p class="pr-title">To</p>--}}
    {{--<div class="range-slider">--}}
    {{--<input name="t_price" class="range-slider__range"--}}
    {{--value="@if(!empty($querydata['to_price'])){{$querydata['to_price']}}@else {{150000}} @endif" min="0"--}}
    {{--max="5000000" type="range" step="100">--}}
    {{--<span class="range-slider__value">@if(!empty($querydata['to_price'])){{$querydata['to_price']}}@else {{150000}} @endif</span>--}}
    {{--</div>--}}
    <div class="col-md-6 col-lg-6 pd-0-xs pd-r-5">
        <label class="text-left price-input-lables pull-left">From</label>
        <input type="number" name="f_price" id="f_price"
               value="{{(!empty($querydata['from_price']))?$querydata['from_price']:'1'}}" placeholder="" min="1"
               class="form-control price-input-field">
        <div class="clear-fix"></div>
    </div>
    {{--<div class="clear-fix"></div>--}}
    <div class="col-md-6 pd-0-xs col-lg-6 pd-l-5">
        <label class="text-left price-input-lables pull-left">To</label>
        <input type="number" name="t_price" id="t_price"
               value="{{(!empty($querydata['to_price']))?$querydata['to_price']:''}}" placeholder="" min="1"
               class="form-control price-input-field">
        <div class="clear-fix"></div>
    </div>
    <div class="clear-fix"></div>
    <button type="button" id="btn_price_range" class="btn btn-warning pull-right">Filter</button>
    <div class="clear-fix"></div>
    <hr class="fade-line">
</div>

