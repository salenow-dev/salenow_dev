@include('layouts.filters.add_type')

<div id="elec_condition">
    @foreach($filters as $filterName => $filterValues)
        @if($filterName == 'Item condition')
            <div class="condition-div">
                <p class="section-title">Condition</p>
                {{--<hr class="hidden-xs">--}}
                @foreach ($filterValues as $id => $filtervalue)
                    <label><input value="{{$id}}" name="comn[filter_condition]"
                                  @if(!empty($querydata['comn_data']['filter_condition'])) @if($querydata['comn_data']['filter_condition'] == $id) {{'checked'}} @endif @endif type="radio"> {{$filtervalue}}
                    </label>
                @endforeach
                <hr class="fade-line">
            </div>
        @endif
    @endforeach
</div>
<div class="sort-div">
    <p class="section-title">Filter By</p>
    {{--<hr class="hidden-xs">--}}
    @if(count($elecclebrandlist))
        <div class="col-sm-12 col-xs-12 pd-0-xs filter-brand-div">
            <div class="panel filter-pannel">
                <div class="panel-heading filter-heading text-left clickable panel-collapsed ">
                    <h3 class="panel-title clickable panel-collapsed pull-left">All Brands</h3>
                    <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                    <div class="clear-fix"></div>
                </div>
                <div class="panel-body filter-body" id="elec_brand"
                     style="{{!empty($querydata['elec_data']['brand'])?'display:block;':'display: none;'}}">
                    @foreach($elecclebrandlist as $ebrand)
                        <div class="checkbox  checkbox-primary">
                            <input id="{{$ebrand->id}}"
                                   @if(!empty($querydata['elec_data']['brand']) && is_array($querydata['elec_data']['brand']))  @if(in_array($ebrand->id,$querydata['elec_data']['brand'])) {{'checked'}}@endif @endif name="elec[brand][]"
                                   value="{{$ebrand->id}}" type="checkbox">
                            <label for="{{$ebrand->id}}">&nbsp;{{$ebrand->brand_name}}</label>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
        <div class="clear-fix"></div>
    @endif

    @foreach($filters as $filterName => $filterValues)
        @if($filterName == 'Item type')
            <div class="col-sm-12 col-xs-12 pd-0-xs filter-brand-div">
                <div class="panel filter-pannel">
                    <div class="panel-heading filter-heading text-left clickable panel-collapsed ">
                        <h3 class="panel-title clickable panel-collapsed pull-left">{{$filterName}}</h3>
                        <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                        <div class="clear-fix"></div>
                    </div>

                    <div class="panel-body filter-body" id="elec_service_type"
                         style="{{!empty($querydata['comn_data']['item_type'])?'display:block;':'display: none;'}}">
                        @foreach ($filterValues as $id => $filtervalue)
                            <div class="checkbox  checkbox-primary">
                                <input id="{{$id}}"
                                       @if(!empty($querydata['comn_data']['item_type']) && is_array($querydata['comn_data']['item_type'])) @if(in_array($id,$querydata['comn_data']['item_type'])) {{'checked'}}@endif @endif name="comn[item_type][]"
                                       value="{{$id}}" type="checkbox">
                                <label for="{{$id}}">&nbsp;{{$filtervalue}}</label>
                            </div>
                        @endforeach

                    </div>


                </div>
            </div>
        @endif
    @endforeach

    <div class="clear-fix"></div>
    <button type="button" id="elec_filter1" class="btn btn-warning pull-right filter-btn">Filter</button>
    <div class="clear-fix"></div>
    <hr class="fade-line">
</div>


@include('layouts.filters.price_range')
