@include('layouts.filters.add_type')

@if(!empty($vehicleconditionlist))
    <div class="condition-div">
        <p class="section-title">Condition</p>
        {{--<hr class="hidden-xs">--}}
        @foreach($vehicleconditionlist as $vconditionlist)
            <label><input id="vehi_condition" value="{{$vconditionlist->id}}" name="vehi[condition]"
                          @if(!empty($querydata['vehi_data']['condition'])) @if($querydata['vehi_data']['condition'] == $vconditionlist->id) {{'checked'}} @endif @endif type="radio"> {{$vconditionlist->condition_name}}
            </label>
        @endforeach
        <hr class="fade-line">
    </div>
@endif

@if($filters)
    <div class="sort-div">
        <p class="section-title">Filter By</p>
        {{--<hr class="hidden-xs">--}}
        @foreach($filters as $filterName => $filterValues)
            @if($filterName == 'Baths')
                <div class="col-sm-12 col-xs-12 pd-0-xs">
                    <div class="panel filter-pannel">
                        <div class="panel-heading filter-heading text-left ">
                            <h3 class="panel-title clickable panel-collapsed">{{$filterName}}</h3>
                            <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                        </div>

                        <div class="panel-body filter-body" id="elec_service_type"
                             style="{{!empty($querydata['prop_data']['Baths'])?'display:block;':'display: none;'}}">
                            @foreach ($filterValues as $id => $filtervalue)
                                <div class="checkbox  checkbox-primary">
                                    <input id="{{$id}}"
                                           @if(!empty($querydata['prop_data']['Baths'])  && is_array($querydata['prop_data']['Baths']))  @if(in_array($id,$querydata['prop_data']['Baths'])) {{'checked'}}@endif @endif name="prop[Baths][]"
                                           value="{{$id}}" type="checkbox">
                                    <label for="{{$id}}">&nbsp;{{$filtervalue}}</label>
                                </div>
                            @endforeach

                        </div>


                    </div>
                </div>
            @endif
        @endforeach
        @foreach($filters as $filterName => $filterValues)
            @if($filterName == 'Beds')
                <div class="col-sm-12">
                    <div class="panel filter-pannel">
                        <div class="panel-heading filter-heading text-left ">
                            <h3 class="panel-title clickable panel-collapsed">{{$filterName}}</h3>
                            <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                        </div>

                        <div class="panel-body filter-body" id="elec_service_type"
                             style="{{!empty($querydata['prop_data']['Beds'])?'display:block;':'display: none;'}}">
                            @foreach ($filterValues as $id => $filtervalue)
                                <div class="checkbox  checkbox-primary">
                                    <input id="{{$id}}"
                                           @if(!empty($querydata['prop_data']['Beds'])  && is_array($querydata['prop_data']['Beds']))   @if(in_array($id,$querydata['prop_data']['Beds'])) {{'checked'}}@endif @endif name="prop[Beds][]"
                                           value="{{$id}}" type="checkbox">
                                    <label for="{{$id}}">&nbsp;{{$filtervalue}}</label>
                                </div>
                            @endforeach

                        </div>


                    </div>
                </div>
            @endif
        @endforeach

        <div class="clear-fix"></div>
        <button type="button" id="elec_filter1" class="btn btn-warning pull-right">Filter</button>
        <div class="clear-fix"></div>
        <hr class="fade-line">
    </div>
@endif

@include('layouts.filters.price_range')
