@include('layouts.filters.add_type')

<div id="vehi_condition">
    @if(!empty($vehicleconditionlist))
        <div class="condition-div">
            <p class="section-title">Condition</p>
            {{--<hr class="hidden-xs">--}}
            @foreach($vehicleconditionlist as $vconditionlist)
                <label><input id="vehi_condition" value="{{$vconditionlist->id}}" name="vehi[condition]"
                              @if(!empty($querydata['vehi_data']['condition'])) @if($querydata['vehi_data']['condition'] == $vconditionlist->id) {{'checked'}} @endif @endif type="radio"> {{$vconditionlist->condition_name}}
                </label>
            @endforeach
            <hr class="fade-line">
        </div>
    @endif
</div>
<div class="sort-div filter-by">
    <p class="section-title">Filter By</p>
    {{--<hr class="hidden-xs">--}}
    @if(!empty($vehiclebrandlist))
        <div class="col-sm-12 col-xs-12 pd-0-xs filter-brand-div">
            <div class="panel filter-pannel">
                <div class="panel-heading filter-heading text-left clickable panel-collapsed">
                    <h3 class="panel-title clickable panel-collapsed pull-left">All Brands</h3>
                    <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                    <div class="clear-fix"></div>
                </div>
                <div class="panel-body filter-body" id="vehicle_brand"
                     style="{{!empty($querydata['vehi_data']['brand'])?'display:block;':'display: none;'}}">
                    @foreach($vehiclebrandlist as $vbrand)
                        <div class="checkbox  checkbox-primary">
                            <input id="{{$vbrand->id}}"
                                   @if(!empty($querydata['vehi_data']['brand'])  && is_array($querydata['vehi_data']['brand']))  @if(in_array($vbrand->id,$querydata['vehi_data']['brand'])) {{'checked'}}@endif @endif name="vehi[brand][]"
                                   value="{{$vbrand->id}}" type="checkbox">
                            <label for="{{$vbrand->id}}">&nbsp;{{$vbrand->brand_name}}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    @foreach($filters as $filterName => $filterValues)
        @if($filterName == 'Item type')
            <div class="col-sm-12 col-xs-12 pd-0-xs filter-brand-div">
                <div class="panel filter-pannel">
                    <div class="panel-heading filter-heading text-left clickable panel-collapsed">
                        <h3 class="panel-title clickable panel-collapsed pull-left">Vehicle Type</h3>
                        <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                        <div class="clear-fix"></div>
                    </div>
                    <div class="panel-body filter-body" id="elec_service_type"
                         style="{{!empty($querydata['vehi_data']['item_type'])?'display:block;':'display: none;'}}">
                        @foreach ($filterValues as $id => $filtervalue)
                            <div class="checkbox  checkbox-primary">
                                <input id="{{$id}}"
                                       @if(!empty($querydata['vehi_data']['item_type']) && is_array($querydata['vehi_data']['item_type']))  @if(in_array($id,$querydata['vehi_data']['item_type'])) {{'checked'}}@endif @endif name="vehi[item_type][]"
                                       value="{{$id}}" type="checkbox">
                                <label for="{{$id}}">&nbsp;{{$filtervalue}}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
    @endforeach
    <div class="clear-fix"></div>
    <hr class="fade-line">
</div>

@include('layouts.filters.price_range')