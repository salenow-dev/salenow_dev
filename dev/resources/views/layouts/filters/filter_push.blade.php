@include('layouts.filters.add_type')

<div id="vehi_condition">
    @if(!empty($vehicleconditionlist))
        <div class="condition-div">
            <p class="section-title">Condition</p>
            {{--<hr class="hidden-xs">--}}
            @foreach($vehicleconditionlist as $vconditionlist)
                <label><input id="vehi_condition" value="{{$vconditionlist->id}}" name="vehi[condition]"
                              @if(!empty($querydata['vehi_data']['condition'])) @if($querydata['vehi_data']['condition'] == $vconditionlist->id) {{'checked'}} @endif @endif type="radio"> {{$vconditionlist->condition_name}}
                </label>
            @endforeach
            <hr class="fade-line">
        </div>
    @endif
</div>

@include('layouts.filters.price_range')

