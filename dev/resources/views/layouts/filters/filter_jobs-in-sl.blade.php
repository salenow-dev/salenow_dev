{{--@include('layouts.filters.add_type')--}}

<div class="sort-div filter-by">
    <p class="section-title">Filter By</p>
    {{--<hr class="hidden-xs">--}}{{--{{print_r($filters)}}--}}
    @foreach($filters as $filterName => $filterValues)
        @if($filterName == 'Item type')
            <div class="col-sm-12 col-xs-12 pd-0-xs filter-brand-div">
                <div class="panel filter-pannel">
                    <div class="panel-heading filter-heading text-left clickable panel-collapsed ">
                        <h3 class="panel-title clickable panel-collapsed pull-left">Job Type</h3>
                        <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                        <div class="clear-fix"></div>
                    </div>

                    <div class="panel-body filter-body" id="elec_service_type"
                         style="{{!empty($querydata['comn_data']['item_type'])?'display:block;':'display: none;'}}">
                        @foreach ($filterValues as $id => $filtervalue)
                            <div class="checkbox  checkbox-primary">
                                <input id="{{$id}}"
                                       @if(!empty($querydata['comn_data']['item_type']) && is_array($querydata['comn_data']['item_type']))  @if(in_array($id,$querydata['comn_data']['item_type'])) {{'checked'}}@endif @endif name="comn[item_type][]"
                                       value="{{$id}}" type="checkbox">
                                <label for="{{$id}}">&nbsp;{{$filtervalue}}</label>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        @endif

            @if($filterName == 'Industry')
                <div class="col-sm-12 col-xs-12 pd-0-xs filter-brand-div">
                    <div class="panel filter-pannel">
                        <div class="panel-heading filter-heading text-left clickable panel-collapsed ">
                            <h3 class="panel-title clickable panel-collapsed pull-left">{{$filterName}}</h3>
                            <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                            <div class="clear-fix"></div>
                        </div>

                        <div class="panel-body filter-body" id="elec_service_type"
                             style="{{!empty($querydata['comn_data']['industry'])?'display:block;':'display: none;'}}">
                            @foreach ($filterValues as $id => $filtervalue)
                                <div class="checkbox  checkbox-primary">
                                    <input id="{{$id}}"
                                           @if(!empty($querydata['comn_data']['industry']) && is_array($querydata['comn_data']['industry']))  @if(in_array($id,$querydata['comn_data']['industry'])) {{'checked'}}@endif @endif name="comn[industry][]"
                                           value="{{$id}}" type="checkbox">
                                    <label for="{{$id}}">&nbsp;{{$filtervalue}}</label>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            @endif
            @if($filterName == 'Salary')
                <div class="col-sm-12 col-xs-12 pd-0-xs filter-brand-div">
                    <div class="panel filter-pannel">
                        <div class="panel-heading filter-heading text-left clickable panel-collapsed ">
                            <h3 class="panel-title clickable panel-collapsed pull-left">{{$filterName}}</h3>
                            <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                            <div class="clear-fix"></div>
                        </div>

                        <div class="panel-body filter-body" id="elec_service_type"
                             style="{{!empty($querydata['comn_data']['salary'])?'display:block;':'display: none;'}}">
                            @foreach ($filterValues as $id => $filtervalue)
                                <div class="checkbox  checkbox-primary">
                                    <input id="{{$id}}"
                                           @if(!empty($querydata['comn_data']['salary']) && is_array($querydata['comn_data']['salary']))  @if(in_array($id,$querydata['comn_data']['salary'])) {{'checked'}}@endif @endif name="comn[salary][]"
                                           value="{{$id}}" type="checkbox">
                                    <label for="{{$id}}">&nbsp;{{$filtervalue}}</label>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            @endif
    @endforeach
    <div class="clear-fix"></div>
    <button type="button" id="elec_filter1" class="btn btn-warning pull-right filter-btn">Filter</button>
    <div class="clear-fix"></div>
    <hr class="fade-line">
</div>

