<!DOCTYPE HTML>
<html>
<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    @if($memberCompany->profile_image ==='saleme-default-profile.jpg' OR is_null($memberCompany->profile_image))
        <link rel="icon" type="image/png" href="{{asset('')}}salenow/images/prime-members/default_fav.jpg">
    @else
        <link rel="icon" type="image/png"
              href="{{asset('')}}salenow/images/prime-members/{{$memberCompany->member_id.'/fav/'.$memberCompany->profile_image}}">
    @endif
    <title>{{$memberCompany->company_name}} on SaleMe.lk</title>

    <meta content="{{$memberCompany->description}}" name=description>
    <meta content="saleme.lk, saleme, saleme lk, srilanka, sri lanka, premium member, premium customer, premium user, {{$memberCompany->company_name}}" name=keywords>
    <meta content=#fea502 name=theme-color>
    <meta content={{url()->full()}} property=og:url>
    <meta content=www.saleme.lk property=og:site_name>
    <meta content=product property=og:type>
    <meta content="{{$memberCompany->company_name}} on SaleMe.lk" property=og:title>
    <meta content="{{$memberCompany->description}}" property=og:description>
    <meta content=600 property=og:image:width>
    <meta content=315 property=og:image:height>
    @if($memberCompany->profile_image ==='saleme-default-profile.jpg' OR is_null($memberCompany->profile_image))
        <meta content="{{asset('')}}salenow/images/prime-members/premium_default.jpg" property=og:image>
    @else
        <meta content="{{asset('')}}salenow/images/prime-members/{{$memberCompany->member_id.'/'.$memberCompany->profile_image}}"
              property=og:image>
    @endif
    <meta content=1867918153484824 property=fb:app_id>


    <!--[if lte IE 8]><!--<script src="assets/js/ie/html5shiv.js"></script>--><![endif]-->
    {{ Html::style('premium_assets/theme_01/css/main.css?V=0.2') }}
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"/>
    <script src="{{asset('')}}premium_assets/theme_01/js/jquery.min.js"></script>
    <!--[if lte IE 8]><!--<link rel="stylesheet" href="assets/css/ie8.css" />--><![endif]-->
</head>
<body class="homepage">
<div id="page-wrapper">

    <!-- Header -->
    <div id="header-wrapper">
        <div id="header">
        @include('layouts.member.premium_theme_01_nav')

        @yield('premium_content')

        <!-- Footer -->
            <div id="footer-wrapper">
                <section id="footer" class="container">

                    <div class="row">

                        <div class="6u 12u(mobile)"
                             style="{{(ucwords(Request::segment(2)=='contact') ? 'display:none' :'')}}">
                            <section>
                                <!-- <header>
                                    <h2>Contact Information</h2>
                                </header> -->
                                <!-- <ul class="social">
                                    <li><a class="icon fa-facebook" href="#"><span class="label">Facebook</span></a></li>
                                    <li><a class="icon fa-twitter" href="#"><span class="label">Twitter</span></a></li>
                                    <li><a class="icon fa-dribbble" href="#"><span class="label">Dribbble</span></a></li>
                                    <li><a class="icon fa-linkedin" href="#"><span class="label">LinkedIn</span></a></li>
                                    <li><a class="icon fa-google-plus" href="#"><span class="label">Google+</span></a></li>
                                </ul> -->
                                <ul class="contact">
                                    <li>
                                        <h3>Address</h3>
                                        <p>
                                            {{$memberCompany->company_name}}<br/>
                                            {{str_replace('\n',' ',$memberCompany->address )}}
                                            {{--{!! nl2br($memberCompany->address) !!}--}}
                                        </p>
                                    </li>
                                    <li>
                                        @if($memberCompany->email)
                                            <h3>E-Mail</h3>
                                            <p><a href="mailto:{{$memberCompany->email}}">{{$memberCompany->email}}</a>
                                            </p>
                                        @endif
                                    </li>

                                    @if($memberCompany->telephone)
                                        <li>
                                            <h3>Telephone</h3>
                                            <p>
                                                @foreach($memberCompany->telephone as $com_con)
                                                    {{$com_con.'&nbsp;/&nbsp;'}}
                                                @endforeach
                                            </p>
                                        </li>
                                    @endif
                                    @if($memberCompany->mobile)
                                        <li>
                                            <h3>Mobile</h3>
                                            <p>
                                                @foreach($memberCompany->mobile as $mobi)
                                                    {{$mobi.'&nbsp;/&nbsp;'}}
                                                @endforeach
                                            </p>
                                        </li>
                                    @endif
                                    @if($memberCompany->website)
                                        <li>
                                            <h3>Website</h3>
                                            <p>
                                                <a href="{{$memberCompany->website}}" target="_blank">Visit Website</a>
                                            </p>
                                        </li>
                                    @endif
                                    @if($memberCompany->facebook_page)
                                        <li>
                                            <h3>Facebook</h3>
                                            <p>
                                                <a href="{{$memberCompany->facebook_page}}" target="_blank">Visit Facebook page</a>
                                            </p>
                                        </li>
                                    @endif
                                </ul>
                            </section>
                        </div>
                        @if(ucwords(Request::segment(2)!='contact'))
                            <div class="6u 12u(mobile) "
                                 style="{{(ucwords(Request::segment(2)=='contact') ? 'display:none' :'')}}">
                                <section>
                                    <div style="width: 100%; height: 300px; background-color: #fff" id="map">

                                    </div>
                                </section>
                            </div>
                        @endif

                        <div class="clear"></div>
                    </div>
                    <div class="row">
                        <div class="12u footer-div">
                            <!-- Copyright -->
                            <div id="copyright">
                                <ul class="links">
                                    <li>&copy; {{$memberCompany->company_name}}. All rights reserved.</li>
                                    <li>Copyright © 2018 <b>SaleMe.lk PVT LTD</b></li>
                                    <li>
                                        <a href="{{asset('')}}" target="_blank" title="SaleMe.lk PVT LTD">
                                            {{ Html::image('premium_assets/frontend/images/saleme-logo.png', 'saleme.lk logo', array('class' => 'footer-saleme')) }}
                                        </a>

                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </section>
            </div>

        </div>

</body><!-- Scripts -->

<script src="{{asset('')}}premium_assets/theme_01/js/jquery.dropotron.min.js"></script>
<script src="{{asset('')}}premium_assets/theme_01/js/skel.min.js"></script>
<script src="{{asset('')}}premium_assets/theme_01/js/skel-viewport.min.js"></script>
<script src="{{asset('')}}premium_assets/theme_01/js/util.js"></script>
{{--<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->--}}
<script src="{{asset('')}}premium_assets/theme_01/js/main.js"></script>

<script src="{{asset('')}}premium_assets/frontend/dist/js/lightgallery-all.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#lightgallery').lightGallery();
    });
</script>
<!-- google maps -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMZ1lBPKgmEWvAP6uvBoEv7zouf_73FLw"
        type="text/javascript"></script>
<script type="text/javascript">
    var locations = [
        ['{{$memberCompany->company_name}}', {{$memberCompany->lat}}, {{$memberCompany->lng}}, 2]
    ];
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 17,
        center: new google.maps.LatLng( {{$memberCompany->lat}}, {{$memberCompany->lng}}),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        styles: [
            {
                "featureType": "administrative.country",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            }
        ]
    });

    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),

            icon: '{{asset('')}}premium_assets/frontend/images/place-pin.png',
            map: map
        });
        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }
</script>


</html>