{{--@section('title', 'SaleMe | Member profile')--}}
{{--@include('main.header')--}}
<!--  Fonts and icons     -->

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <link rel="icon" type="image/png"
          href="{{asset('')}}salenow/images/prime-members/{{$member->premiummember->member_id.'/fav/'.$member->premiummember->profile_image}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>SaleMe.lk | {{$member->premiummember->company_name}} Dashboard</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>

    {{ Html::style('css/salenow/layout/bootstrap-3.3.7/css/bootstrap.min.css') }}
    {{ Html::style('assets/css/premium-dashboard.css') }}
    {{ Html::style('assets/css/animate.min.css') }}
    {{ Html::style('css/salenow/layout/font-awesome-4.7.0/css/font-awesome.min.css') }}
    {{ Html::style('css/salenow/layout/ionicons/css/ionicons.min.css') }}
    {{ Html::style('https://fonts.googleapis.com/css?family=Muli:400,300') }}
    {{ Html::script('js/salenow/jquery.min.js') }}
    {{ Html::script('js/salenow/bootstrap.min.js') }}
</head>
<body>
<div class="wrapper">
    <div class="sidebar" data-background-color="white" data-active-color="danger">
        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="{{asset('')}}" class="">
                    <img src="{{asset('')}}images/salenow/saleme-logo.png" class="img-responsive saleme-logo-premium"
                         alt="saleme.lk logo">
                </a>
            </div>
            <div class="logo-company">
                <a href="javascropt:;" class="simple-text">
                    <img align="left" class="thumbnail img-responsive"
                         src="{{asset('')}}salenow/images/prime-members/{{$member->premiummember->member_id.'/'.$member->premiummember->profile_image}}"
                         alt="{{$member->premiummember->company_name}} Cover image"/>
                </a>
            </div>
            <div class="clear-fix"></div>
            <ul class="nav">
                <li class="{{(Request::segment(1))==='myprofile' ? 'active' : ''}}">
                    <a href="{{asset('')}}myprofile">
                        <i class="ion-ios-analytics-outline"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                @if(Request::segment(1)!='myprofile')
                    <li>
                        <a href="{{asset('')}}myprofile#ad_summery">
                            <i class="ion-stats-bars"></i>
                            <p>Ads Summary</p>
                        </a>
                    </li>
                @else
                    <li>
                        <a href="#ad_summery" data-toggle="tab">
                            <i class="ion-stats-bars"></i>
                            <p>Ads Summary</p>
                        </a>
                    </li>
                @endif
                {{--<li class="{{(Request::segment(2))==='wishlist' ? 'active' : ''}}">--}}
                    {{--<a href="/wishlist">--}}
                        {{--<i class="ion-ios-heart"></i>--}}
                        {{--<p>Wish List</p>--}}
                    {{--</a>--}}
                {{--</li>--}}

                <li class="{{(Request::segment(2))==='about' ? 'active' : ''}}">
                    <a href="/{{$member->premiummember->slug}}/about">
                        <i class="ion-gear-b"></i>
                        <p>Settings</p>
                    </a>
                </li>

                <li class="{{(Request::segment(1))==='wishlist' ? 'active' : ''}}">
                    <a href="/wishlist">
                        <i class="ion-ios-heart"></i>
                        <p>Wish List</p>
                    </a>
                </li>
                {{--{{dd('')}}--}}
                @if(isEmployeeAdmin())
                    <li class="{{(Request::segment(2))==='location' ? 'active' : ''}}">
                        <a href="/{{$member->premiummember->slug}}/location">
                            <i class="ion-ios-location"></i>
                            <p>Business Location</p>
                        </a>
                    </li>
                @endif
                <li class="{{(Request::segment(2))==='services' ? 'active' : ''}}">
                    <a href="/{{$member->premiummember->slug}}/services-add">
                        <i class="ion-coffee"></i>
                        <p>Services</p>
                    </a>
                </li>
                @if(isEmployeeAdmin())
                    <li class="{{(Request::segment(2))==='gallery-add' ? 'active' : ''}}">
                        <a href="/{{$member->premiummember->slug}}/gallery-add">
                            <i class="ion-images"></i>
                            <p>Image Gallery</p>
                        </a>
                    </li>
                @endif
                <li class="{{(Request::segment(2))==='team' ? 'active' : ''}}">
                    <a href="/{{$member->premiummember->slug}}/team">
                        <i class="ion-ios-people"></i>
                        <p>Team</p>
                    </a>
                </li>

                @if(isEmployeeAdmin())
                    <li class="{{(Request::segment(2))==='manage' ? 'active' : ''}}">
                        <a href="/{{$member->premiummember->slug}}/manage">
                            <i class="ion-person-add"></i>
                            <p>Manage Team</p>
                        </a>
                    </li>
                    <li>
                        <a href="/premium/{{str_random(3).$member->premiummember->id}}/edit">
                            <i class="ion-compose"></i>
                            <p>Edit Company Account</p>
                        </a>
                    </li>
                @endif
                @if(getMemberType() != 'premium')
                    <li>
                        <a href="/my_ads?type=private">
                            <i class="ion-ios-arrow-right"></i>
                            <p>Visit Private Profile</p>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid premium-header">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">{{ucwords($member->premiummember->company_name)}}</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="{{asset('')}}ads">
                                <i class="ion-pricetags"></i>&nbsp;
                                <p>All Ads</p>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="ti-user"></i>
                                <p>Premium Account
                                </p>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{asset('')}}myprofile"><span class="ion-person"></span>&nbsp;&nbsp;&nbsp;
                                        {{(checkCompany())?(temporyPrivateLogin())?'My Account':'Company Account':'My Account'}}
                                    </a>
                                </li>
                                @if(getMember()->employee && !temporyPrivateLogin())
                                    <li><a href="/employee/{{getMember()->employee->id}}/edit">
                                            <span class="ion-ios-pricetags"></span> &nbsp;&nbsp;&nbsp;Company
                                            Profile</a>
                                    </li>
                                @endif
                                @if(!checkCompany() || temporyPrivateLogin())
                                    <li><a href="{{asset('')}}my_ads"><span class="ion-ios-pricetags"></span> My Ads</a>
                                    </li>
                                @endif
                                <li class="divider"></li>
                                <li><a href="{{asset('')}}member_logout"><span class="ion-power"></span>&nbsp;&nbsp;&nbsp;Logout</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{(getMemberId())?asset('').'addtype':'/member-login'}}" class="post-free-ad-btn">
                                <button type="button" class="btn post-lg  btn-lg ">Post Free Ad</button>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="content bg-ccc">
            <div class="container-fluid">
                @include('errors.flash')
                @yield('content')
                <dvi class="clear-fix"></dvi>
            </div>
        </div>

        <footer class="footer bg-fff">
            <div class="container-fluid">
                <nav class="pull-left hidden-xs">
                    <ul>

                        <li>
                            <a href="{{asset('')}}ads">
                                All Ads
                            </a>
                        </li>
                        <li>
                            <a href="{{asset('')}}privacy-policy">
                                Privacy Policy
                            </a>
                        </li>
                        <li>
                            <a href="{{asset('')}}terms-conditions">
                                Terms & Conditions
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright pull-right">
                    ©
                    <script>document.write(new Date().getFullYear())</script>
                    SaleMe.lk (Pvt) Ltd. All right reserved.
                </div>
            </div>
        </footer>

    </div>
</div>


</body>


<!--  Notifications Plugin    -->
{{ Html::script('assets/js/bootstrap-notify.js') }}

<!--  Google Maps Plugin    -->
{{--{{ Html::script('https://maps.googleapis.com/maps/api/js') }}--}}

<!-- Dashboard Core javascript and methods for Demo purpose -->
{{ Html::script('assets/js/premium-dashboard.js') }}

{{--<script type="text/javascript">--}}
{{--$(document).ready(function () {--}}
{{--$.notify({--}}
{{--icon: 'ti-gift',--}}
{{--message: "Welcome to <b>Paper Dashboard</b> - a beautiful Bootstrap freebie for your next project."--}}
{{--}, {--}}
{{--type: 'success',--}}
{{--timer: 4000--}}
{{--});--}}
{{--});--}}
{{--</script>--}}
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/noty/jquery.noty.js")}}'></script>
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/noty/layouts/topCenter.js")}}'></script>
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/noty/layouts/topLeft.js")}}'></script>
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/noty/layouts/topRight.js")}}'></script>
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/noty/themes/default.js")}}'></script>

<script type="text/javascript" src="{{ asset("js/member/ad-delete.js") }}"></script>
<script type="text/javascript" src="{{ asset("js/memeber/number-verify.js") }}"></script>
<script>
    function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".more-less")
            .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
<script>
    jQuery(document).ready(function () {
//        edit section togle
        $('.edit-btn').click(function (event) {
            $('.edit-section').slideToggle('show');
            $('.user-details-section').slideToggle('hide');
        });
        $('.edit-close-btn').click(function (event) {
            $('.edit-section').slideToggle('hide');
            $('.user-details-section').slideToggle('show');
        });
//        verify no togle
        $('.verify-toggle').click(function (event) {
            $('.verify-phone-section').slideToggle('show');
        });
        $('.verify-no-close').click(function (event) {
            $('.verify-phone-section').slideToggle('hide');
        });
    });
</script>

<script>
    $('.expan-click').click(function () {
        $('.tele-nos').slideToggle('slow');
        $(this).hide();
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#cover_preview')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#pro_preview')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
{{ Html::script('js/salenow/plugin/jquery.validate.js') }}
<script src="{{ asset("js/location/district_city.js") }}"></script>
<script>
    //change theme
    function notyConfirmtheme(theme, memberid) {
        noty({
            text: 'Do you want to change theme?',
            layout: 'topRight',
            buttons: [{
                addClass: 'btn btn-success btn-clean', text: 'Change', onClick: function ($noty) {
                    theme = theme;
                    changeTheme(theme, memberid);
                    $noty.close();
                }
            },
                {
                    addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function ($noty) {
                    $noty.close();
                }
                }
            ]
        })
    }
    function changeTheme(theme, memberid) {
//        alert(memberid);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var data = {'memberid': memberid, 'theme': theme};
        $.ajax({
            type: "POST",
            url: '/change_theme/' + memberid,
            data: data,
            success: function (res) {
                if (res.status == 'success') {
                    $('.responce').html('<div class="col-md-12 col-sm-12 ">' +
                        '<div class="alert alert-success">' + res.message +
                        '</div></div>');
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                } else if (res.status == 'error') {
                    $('.responce').html('<div class="col-md-12 col-sm-12 ">' +
                        '<div class="alert alert-success">' + res.message +
                        '</div></div>')
                }
            }
        });
    }

    function removePendingOPT(id) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var data = {'id': id};
        $.ajax({
            type: "POST",
            url: '/delete_otp/' + id,
            data: data,
            success: function (res) {
                if (res.message == 'success') {
                    $('#contact_' + id).toggle(400);
                    return false;
                }
            }
        });
    }

    $(document).ready(function () {
        $("#otpVerify").validate({
            rules: {
                number_verify: {
                    required: true,
                    number: true
                },
            }, submitHandler: function (form) {
                $.ajax({
                    type: "POST",
                    url: '/verifynumberMember',
                    data: $("#otpVerify").serialize(),
                    success: function (res) {
                        if (res.response.message == 'verified') {
                            $("#message").html('Number Verified! Thank You.');
//                            setTimeout("window.location.href='myprofile';", 1000);
                            $('.add-phone-number').hide();
                            window.location.replace('myprofile');
                        }
                        if (res.response.error != '') {
                            $("#message").html(res.response.error)
                            $("#message").toggle();
                        }
                    }
                });
            }
        });

    });
</script>
@if(Request::segment(1)=='myprofile')
    <script type="text/javascript">
        var activeTab = "";
        var url = window.location.href;
        activeTab = url.substring(url.indexOf("#") + 1);
        if (activeTab != "") {
            $("#" + activeTab).addClass("active in");
            $('a[href="#' + activeTab + '"]').tab('show');
        }
    </script>
@endif
<!-- select sub-category tabs-->

<!--tabs-->
</html>
