<h1><a href="{{asset('')}}{{$memberCompany->slug}}">{{title_case($memberCompany->company_name)}}</a></h1>
<nav id="nav">
    <ul>
        <li class="{{(ucwords(Request::segment(2)=='') ? 'current' :'')}}">
            <a href="{{asset('')}}{{$memberCompany->slug}}">Home</a>
        </li>
        <li class="{{(ucwords(Request::segment(2)=='ads') ? 'current' :'')}}">
            <a href="{{asset('')}}{{$memberCompany->slug}}/ads">All Ads</a>
        </li>
        @if($service_count)
        <li class="{{(ucwords(Request::segment(2)=='services') ? 'current' :'')}}">
            <a href="{{asset('')}}{{$memberCompany->slug}}/services">services</a>
        </li>
        @endif
        @if($image_count)
        <li class="{{(ucwords(Request::segment(2)=='gallery') ? 'current' :'')}}">
            <a href="{{asset('')}}{{$memberCompany->slug}}/gallery">Gallery</a>
        </li>
        @endif
         <li class="{{(ucwords(Request::segment(2)=='contact') ? 'current' :'')}}">
            <a href="{{asset('')}}{{$memberCompany->slug}}/contact">Contact</a>
        </li>
        <li class="">
            <a href="{{asset('')}}ads" class="back-to-saleme-link btn">
                {{ Html::image('premium_assets/frontend/images/saleme-logo.png', 'saleme.lk logo', array('class' => 'footer-saleme')) }}

            </a>
        </li>
    </ul>

</nav>