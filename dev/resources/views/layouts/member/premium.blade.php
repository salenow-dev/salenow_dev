<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    @if($memberCompany->profile_image ==='saleme-default-profile.jpg' OR is_null($memberCompany->profile_image))
        <link rel="icon" type="image/png" href="{{asset('')}}salenow/images/prime-members/default_fav.jpg">
    @else
        <link rel="icon" type="image/png"
              href="{{asset('')}}salenow/images/prime-members/{{$memberCompany->member_id.'/fav/'.$memberCompany->profile_image}}">
    @endif

    <title>{{$memberCompany->company_name}} on SaleMe.lk</title>

    <meta content="{{$memberCompany->description}}" name=description>
    <meta content="saleme.lk, saleme, saleme lk, srilanka, sri lanka, premium member, premium customer, premium user, {{$memberCompany->company_name}}" name=keywords>
    <meta content=#fea502 name=theme-color>
    <meta content={{url()->full()}} property=og:url>
    <meta content=www.saleme.lk property=og:site_name>
    <meta content=product property=og:type>
    <meta content="{{$memberCompany->company_name}} on SaleMe.lk" property=og:title>
    <meta content="{{$memberCompany->description}}" property=og:description>
    <meta content=600 property=og:image:width>
    <meta content=315 property=og:image:height>
    @if($memberCompany->profile_image ==='saleme-default-profile.jpg' OR is_null($memberCompany->profile_image))
        <meta content="{{asset('')}}salenow/images/prime-members/premium_default.jpg" property=og:image>
    @else
        <meta content="{{asset('')}}salenow/images/prime-members/{{$memberCompany->member_id.'/'.$memberCompany->profile_image}}"
              property=og:image>
    @endif
    <meta content=1867918153484824 property=fb:app_id>
    {{ Html::style('premium_assets/frontend/css/bootstrap.css') }}
    {{ Html::style('premium_assets/frontend/css/font-icons/entypo/css/entypo.css') }}
    {{ Html::style('premium_assets/frontend/css/premium_styles.css?v=0.1') }}
    {{ Html::script('js/salenow/jquery.min.js') }}
    {{--fonts--}}
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel="stylesheet">
    {{--fonts--}}
</head>
<body>
{{--{{dd($memberCompany)}}--}}
<div class="wrap">
    <div class="container">
        <div class="row" style="border-bottom: 1px solid #eee;">
            <div class="col-md-12">
                <!-- <header class="site-header padiing-left-50 padiing-right-50"> -->
                <header class="site-header">
                    <!-- logo section -->
                    <div class="col-md-6 col-sm-6 col-xs-10 pd-0-xs">
                        <section class="site-logo">
                            <a href="{{asset('')}}{{$memberCompany->slug}}">
                                @if($memberCompany->profile_image ==='saleme-default-profile.jpg')
                                    <img src="{{asset('')}}salenow/images/prime-members/premium_default.jpg" width="" class="hidden-xs">
                                @else
                                    <img src="{{asset('')}}salenow/images/prime-members/{{$memberCompany->member_id.'/'.$memberCompany->profile_image}}" width="" class="hidden-xs">
                                @endif
                                <h1 class="company-name">{{title_case($memberCompany->webtitle)}}</h1>
                            </a>

                        </section>
                    </div>
                    <!-- navigation section -->
                    <div class="col-md-6 col-sm-6 col-xs-2">
                        <nav class="site-nav">
                            <ul class="main-menu hidden-xs" id="main-menu">
                                <li class="{{(ucwords(Request::segment(2)=='') ? 'active' :'')}}">
                                    <a href="{{asset('')}}{{$memberCompany->slug}}"> <span>Home</span> </a>
                                </li>
                                <li class="{{(ucwords(Request::segment(2)=='ads') ? 'active' :'')}}">
                                    <a href="{{asset('')}}{{$memberCompany->slug}}/ads"> <span>All Ads</span> </a>
                                </li>
                                @if($service_count)
                                <li class="{{(ucwords(Request::segment(2)=='services') ? 'active' :'')}}">
                                    <a href="{{asset('')}}{{$memberCompany->slug}}/services"> <span>services</span> </a>
                                </li>
                                @endif
                                @if($image_count)
                                    <li class="{{(ucwords(Request::segment(2)=='gallery') ? 'active' :'')}}">
                                        <a href="{{asset('')}}{{$memberCompany->slug}}/gallery"> <span>Gallery</span> </a>
                                    </li>
                                @endif
                                <li class="{{(ucwords(Request::segment(2)=='contact') ? 'active' :'')}}">
                                    <a href="{{asset('')}}{{$memberCompany->slug}}/contact"> <span>Contact</span> </a>
                                </li>
                                <li class="visible-xs visible-sm">
                                    <a href="{{asset('')}}ads"> <span>Back to SaleMe.lk</span> </a>
                                </li>
                                <li class="hidden-xs hidden-sm">
                                    <a href="{{asset('')}}ads" class="back-to-saleme-link btn">
                                        {{ Html::image('premium_assets/frontend/images/saleme-logo.png', 'saleme.lk logo', array('class' => 'header-saleme-logo')) }}

                                        {{--<button class="">Back to SaleMe.lk</button>--}}
                                    </a>
                                </li>
                            </ul>
                            <div class="visible-xs">
                                <a href="#" class="menu-trigger"> <i class="entypo-menu"></i> </a>
                            </div>
                        </nav>
                    </div>
                </header>
            </div>
        </div>
    </div>

    <!-- <h1 class="text-center margin-top-0">Jayasena Motors Pvt Ltd</h1> -->
    <!-- <p class="text-center color-eee"> <i class="entypo-location"></i> Kandy > Katugasthota</p> -->
    <!-- <p class="text-center color-eee"> <i class="entypo-tag"></i> Cars & Vehicales</p> -->
    <!-- <p class="text-center color-eee"> <i class="entypo-back-in-time"></i> Open Now</p> -->
    @yield('premium_content')
    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 footer-company-name">
                    <b>{{title_case($memberCompany->company_name)}}</b> - All Rights Reserved
                </div>
                <div class="col-sm-6 text-right">
                    <b>Copyright &copy; {{date('Y')}} <a href="{{asset('')}}" target="_blank" title="SaleMe.lk PVT LTD">SaleMe.lk
                            PVT LTD</a> </b>
                    <a href="{{asset('')}}" target="_blank" title="SaleMe.lk PVT LTD">
                        {{ Html::image('premium_assets/frontend/images/saleme-logo.png', 'saleme.lk logo', array('class' => 'footer-saleme-logo')) }}
                    </a>
                </div>
            </div>
        </div>
    </footer>
</div>
{{ Html::script('premium_assets/frontend/js/bootstrap.js') }}
{{ Html::script('premium_assets/frontend/js/neon-custom.js') }}
<!-- google maps -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMZ1lBPKgmEWvAP6uvBoEv7zouf_73FLw"
        type="text/javascript"></script>
<script type="text/javascript">
    var locations = [
        ['{{$memberCompany->company_name}}', {{$memberCompany->lat}}, {{$memberCompany->lng}}, 2]
    ];
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 17,
        center: new google.maps.LatLng( {{$memberCompany->lat}}, {{$memberCompany->lng}}),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        styles: [
            {
                "featureType": "administrative.country",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "simplified"
                    },
                    {
                        "hue": "#ff0000"
                    }
                ]
            }
        ]
    });

    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),

            icon: '{{asset('')}}premium_assets/frontend/images/place-pin.png',
            map: map
        });
        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#lightgallery').lightGallery();
    });
</script>
<script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
<script src="{{asset('')}}premium_assets/frontend/dist/js/lightgallery-all.min.js"></script>
<script src="{{asset('')}}premium_assets/frontend/dist/js/jquery.mousewheel.min.js"></script>
</body>
</html>