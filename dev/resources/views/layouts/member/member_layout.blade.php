@include('main.header')
{{ Html::style('css/salenow/layout/my-account-custom-css.css') }}
<section class="hidden-xs">
    <div class="container ">
        <div class="row">
            <div class="col-md-12">
                <p class="breadcrumb-text">
                    <i class="ion-ios-home-outline"></i> / My Account
                </p>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container ">
        <div class="row search-results no-margin-top-mobile">
            <section class="sub-menu">
                <div class="col-md-3 col-sm-3 hidden-xs">
                    <div class="list-group left-nav">
                        @if(checkCompany())
                            <a href="/myprofile?type=company" class="list-group-item">Visit Company Profile</a>
                        @endif
                        <a href="/myprofile"
                           class="list-group-item {{(Request::segment(1))==='myprofile' ? 'active-item' : ''}}">
                            My Account</a>
                        @if(count($member->otpnumbers))
                            <a href="javascript:void(0);" class="list-group-item verify-toggle">Verify Number</a>
                        @endif
                        <a href="/my_ads"
                           class="list-group-item {{(Request::segment(1))==='my_ads' ? 'active-item' : ''}}">
                            My Ads<span></span></a>
                        <div class="list-group-item my-ads-nav-div
                        {{(Request::segment(1)==='my_ads')? ($show_nav)?'show-item':'hide-item' :'hide-item'}}">
                            <ul class="list-group my-ad-sub">
                                <li class="list-group-item"><a href="/my_ads?state=confirm">Published Ads</a><span
                                            class="badge">{{!empty($adscount['0']->addcount)?$adscount['0']->addcount : '0'}}</span>
                                </li>
                                <li class="list-group-item"><a href="/my_ads?state=pending">Pending Ads</a><span
                                            class="badge">{{!empty($pendingads['0']->addcount)?$pendingads['0']->addcount : '0'}}</span>
                                </li>
                                <li class="list-group-item"><a href="/my_ads?state=cancel">Canceled Ads</a><span
                                            class="badge">{{!empty($spamedadscount['0']->addcount)?$spamedadscount['0']->addcount : '0'}}</span>
                                </li>
                                <li class="list-group-item"><a href="/my_ads?state=draft"><strong>Draft Ads</strong></a><span
                                            class="badge badge-danger">{{!empty($draftAds['0']->addcount)?$draftAds['0']->addcount : '0'}}</span>
                                </li>
                            </ul>
                        </div>
                            <a href="/wishlist" class="list-group-item {{(Request::segment(1))==='wishlist' ? 'active-item' : ''}}">My Wish List</a>
                        {{--<a href="" class="list-group-item">Payment History</a>--}}
                        <a href="{{asset('')}}member_logout" class="list-group-item logout-link"><i
                                    class="ion-log-out"></i> Logout</a>
                    </div>
                </div>
                <div class="col-lg-9 col-sm-9 pd-0-xs pd-0-sm">
                    @if(getMemberStatus() == 'inactive')
                        <div class="alerts alert-danger-saleme">
                            <span class="ion-android-warning"></span> <strong>Please Verify your
                                Email {{getMember()->email}} by
                                clicking the activation email we sent.</strong> <br>
                            <small> &nbsp; If not, you will not recieve any Email Ad Notifications</small>
                        </div>
                    @endif
                    @if(!empty($member->premiummember))
                        @if(!$member->premiummember->verified)
                            <div class="alerts alert-info">
                                <span class="ion-information-circled"></span> <strong>Your requested company profile is
                                    not yet verified!</strong>
                                <small> &nbsp; Please contact SaleMe.lk Support Team.</small>
                            </div>
                        @endif
                    @endif
                    @include('errors.flash')
                    @yield('content')
                </div>
                {{--tab--}}
               
            </section>
        </div>
    </div>
</section>
<!--verify phone popup-->
<div class="modal fade add-phone-number" id="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="ion-ios-close-empty"></i>
                </button>
                <h4 class="modal-title text-center"><span class="no-1"><i
                                class="ion-ios-telephone-outline"></i> </span> &nbsp; Verify Phone Number</h4>
            </div>

            <div class="col-lg-12">
                <h3>Please enter your mobile phone number</h3>
                <p>For first time use, Please Obtain our Verification PIN Code via SMS alert to getting touch with
                    you.</p>
                <form id="formNumberVerify" class="verify-number-form">
                    <div id="message" class="alert alert-info" style="display: none"></div>
                    <div class="inline-group" id="newnumber">
                        <input type="text" name="number" id="number" class="form-control inline-item enter-phone"
                               required="required" placeholder="eg:07xxxxxxxx"/>
                        <input type="button" name="numberSubmit" id="numberSubmit"
                               class="btn btn-primary inline-item verify-btn"
                               value="Add Number"/>
                    </div>
                    <div class="inline-group" id="verifynumber">
                        <input type="text" name="number_verify" class="form-control inline-item enter-phone"
                               required="required" placeholder="Enter Pin Code"/>
                        <input type="button" name="verify" id="verify"
                               class="btn btn-primary inline-item verify-btn"
                               value="Verify"/>

                        {{--Resend PIN--}}
                        <input type="hidden" name="resend" id="resend" value="">
                        <div>
                            Did not receive PIN code?&nbsp;
                            <input type="button" name="resendpin" id="resendpin" onclick="resendPIN()"
                                   class="btn btn-primary inline-item verify-btn"
                                   value="Resend PIN "/>
                        </div>


                    </div>
                    <div class="inline-group">
                        <p class="nopadding text-center"><label id="number-error" class="error" for="number"
                                                                style="display: none"></label></p>
                    </div>
                    <div class="clear-fix"></div>
                </form>
            </div>
            <div class="clear-fix"></div>
        </div>
    </div>
</div>

<div class="modal fade change-password" id="password_change" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="ion-ios-close-empty"></i></button>
                <h4 class="modal-title text-center"><span class="no-1"><i class="ion-android-unlock"></i> </span> &nbsp;
                    Change Password</h4>
            </div>
            <div class="col-lg-12">
                <form id="updatepass_form" action="" method="post" class="change-password-form" role="form">
                    <div class="messager"></div>
                    <div class="form-group">
                        <label class="lable-form" for="old-pwd">Old Password</label>
                        <input type="password" name="old_password" class="form-control" placeholder="Old Password"
                               id="old_password"/>
                    </div>
                    <div class="form-group">
                        <label class="lable-form" for="new-pwd">New Password</label>
                        <input type="password" name="new_password" class="form-control" placeholder="New Password"
                               id="new_password"/>
                    </div>
                    <div class="form-group">
                        <label class="lable-form" for="confirm-pwd">Confirm Password</label>
                        <input type="password" name="confirm_password" class="form-control"
                               placeholder="Confirm Password" id="" confirm_password"/>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-managead blue-btn pull-right">Change Password</button>
                        <div class="clear-fix"></div>
                    </div>
                </form>
            </div>
            <div class="clear-fix"></div>
        </div>
    </div>
</div>
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/noty/jquery.noty.js")}}'></script>
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/noty/layouts/topCenter.js")}}'></script>
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/noty/layouts/topLeft.js")}}'></script>
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/noty/layouts/topRight.js")}}'></script>
<script type='text/javascript' src='{{ asset("backend/assets/js/plugins/noty/themes/default.js")}}'></script>

<script type="text/javascript" src="{{ asset("js/member/ad-delete.js") }}"></script>
<script type="text/javascript" src="{{ asset("js/memeber/number-verify.js") }}"></script>
<script>
    function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".more-less")
            .toggleClass('glyphicon-plus glyphicon-minus');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
<script>
    jQuery(document).ready(function () {
//        edit section togle
        $('.edit-btn').click(function (event) {
            $('.edit-section').slideToggle('show');
            $('.user-details-section').slideToggle('hide');
        });
        $('.edit-close-btn').click(function (event) {
            $('.edit-section').slideToggle('hide');
            $('.user-details-section').slideToggle('show');
        });
//        verify no togle
        $('.verify-toggle').click(function (event) {
            $('.verify-phone-section').slideToggle('show');
        });
        $('.verify-no-close').click(function (event) {
            $('.verify-phone-section').slideToggle('hide');
        });
    });
</script>
@include('main.footer')