<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') | {{ config('app.name') }}</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <!-- CSS Files -->
    <link href="{{ asset("gamezone/css/bootstrap.min.css") }}" rel="stylesheet"/>
    <link href="{{ asset("gamezone/css/now-ui-kit.css") }}" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{ asset("gamezone/css/demo.css") }}" rel="stylesheet"/>
    {{ Html::style('css/salenow/layout/ionicons/css/ionicons.min.css') }}
    {{ Html::style('css/salenow/layout/linericons/style.min.css') }}
    {{ Html::style('css/salenow/layout/font-awesome-4.7.0/css/font-awesome.min.css') }}
    {{ Html::script('gamezone/js/core/jquery.3.2.1.min.js') }}
    {{ Html::script('gamezone/js/core/popper.min.js') }}
    {{ Html::script('gamezone/js/core/bootstrap.min.js') }}
</head>

<body class="profile-page sidebar-collapse">

<?php
$datetime = new DateTime('tomorrow');
?>
@include('gamezone.navigation')
@yield('content')
<footer class="footer footer-default">
    <div class="container">
        <nav>
            <ul>
                <li>
                    <a href="{{asset('')}}" target="_blank">
                        Back to SaleMe.lk
                    </a>
                </li>
                <li>
                    <a href="{{asset('')}}ads/" target="_blank">
                        All Ads
                    </a>
                </li>
                <li>
                    <a href="{{asset('')}}terms-conditions" target="_blank">
                        Terms & Conditions
                    </a>
                </li>
                <li>
                    <a href="{{asset('')}}privacy-policy" target="_blank">
                        Privacy & Policies
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright">
            © 2018 <a href="{{asset('')}}" target="_blank">SaleMe.lk (Pvt) Ltd</a>. All right reserved.
        </div>
    </div>
</footer>
</div>
</body>
<!--   Core JS Files   -->

{{ Html::script('gamezone/js/plugins/bootstrap-switch.js') }}
{{ Html::script('gamezone/js/plugins/nouislider.min.js') }}
{{ Html::script('gamezone/js/plugins/bootstrap-datepicker.js') }}
{{ Html::script('gamezone/js/now-ui-kit.js') }}
{{ Html::script('gamezone/js/gamezone-custom.js') }}
<script>
    $(function () {
        $(".view").on("click", function () {
            $(this).next().slideToggle(250);
            $fexpand = $(this).find(">:first-child");
            if ($(this).hasClass('opened')) {
                $(this).removeClass('opened');
            } else {
                $(this).addClass('opened');
            }
            ;
        });
    });
</script>
@if(Request::segment(2)=='home' OR Request::segment(2)=='play')
    @if(!is_null($missiondata))
        <script>
            // Set the date we're counting down to
            var countDownDate = new Date("{{$datetime->format('Y-m-d H:i:s')}}").getTime();

            // Update the count down every 1 second
            var x = setInterval(function () {

                // Get todays date and time
                var now = new Date().getTime();

                // Find the distance between now an the count down date
                var distance = countDownDate - now;

                // Time calculations for days, hours, minutes and seconds
//        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                // Output the result in an element with id="demo"
                var time = hours + "h " + minutes + "m " + seconds + "s ";
                @if(!$daily_coin)
                    document.getElementById("coin").innerHTML = "Next Misson " + time;
                @endif

                // If the count down is over, write some text
                if (distance < 0) {
                    clearInterval(x);
//                    document.getElementById("coin").innerHTML = "EXPIRED";
                }
            }, 1000);

            var x = setInterval(function () {

                // Get todays date and time
                var now = new Date().getTime();

                // Find the distance between now an the count down date
                var distance = countDownDate - now;

                // Time calculations for days, hours, minutes and seconds
//        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                // Output the result in an element with id="demo"
                var time = hours + "h " + minutes + "m " + seconds + "s ";
                @if(!$daily_que)
                    document.getElementById("que").innerHTML = "Next Misson " + time;
                @endif

                // If the count down is over, write some text
                if (distance < 0) {
                    clearInterval(x);
//                    document.getElementById("que").innerHTML = "EXPIRED";
                }
            }, 1000);
            var countDownDate1 = new Date("{{date('Y-m-d', strtotime($missiondata->extra_question_last_update_at. ' + 2 days'))}}").getTime();
            var x = setInterval(function () {

                // Get todays date and time
                var now = new Date().getTime();

                // Find the distance between now an the count down date
                var distance = countDownDate1 - now;

                // Time calculations for days, hours, minutes and seconds
//        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                // Output the result in an element with id="demo"
                var time = hours + "h " + minutes + "m " + seconds + "s ";
                @if(!$extra_que)
                    document.getElementById("extra").innerHTML = "Next Misson " + time;
                @endif

                // If the count down is over, write some text
                if (distance < 0) {
                    clearInterval(x);
//                    document.getElementById("extra").innerHTML = "EXPIRED";
                }
            }, 1000);
        </script>
    @endif
@endif

</html>