<link href="{{ asset("plugins/fileinput/fileinput.css") }}" rel="stylesheet">
<script type="text/javascript" src="{{ asset("plugins/fileinput/fileinput.js") }}"></script>
<div class="row border-eee pd-10 mg-b-10 pd-10">
    <div class="col-md-12">
        <h4 class="category-title">Primary Details</h4>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="lable-form" for="exampleInputEmail1">Select Manufacturer(Brand)</label>
            <select id="vehibrand_id" name="vehibrand_id" class="show-tick form-control" data-live-search="true">
                <option value="">Please Select Vehicle Brand</option>
                @foreach ($vehiclebrandlist as $brand)
                    <option value="{{$brand->id}}">{{$brand->brand_name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="lable-form" for="exampleInputEmail1">Select Model</label>
            <select id="vehimodel" name="vehimodel" class="show-tick form-control"
                    data-live-search="true">
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="lable-form" for="exampleInputEmail1">Select Model Year</label>
            <select id="basic" name="registry_year" class="show-tick form-control">
                <option value="">Please Select Year</option>
                <?php
                $thisyear = date('Y');
                $yeraslist = range($thisyear, 1970); ?>
                @foreach ($yeraslist as $year)
                    <option value="{{$year}}">{{$year }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="lable-form" for="mileage">Mileage (Km)</label>
            <input type="number" class="form-control" id="mileage" name="milage" placeholder="E.g 1000">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="lable-form" for="exampleInputEmail1">Select Fuel Type</label>
            <select id="fuel_id" class="show-tick form-control" name="fuel_id" data-live-search="false">
                <option value="">Please Select Fuel</option>
                @foreach ($vehiclefuellist as $fuel)
                    <option value="{{$fuel->id}}">{{$fuel->fuel_name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="lable-form" for="exampleInputEmail1">Transmission Type</label>
            <select id="tranmission_id" class="show-tick form-control" name="tranmission_id" data-live-search="false">
                <option value="">Please Select Transmission Type</option>
                @foreach ($vehicletransmissionslist as $trans)
                    <option value="{{$trans->id}}">{{$trans->transmission_name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="lable-form" for="enginesize">Engine Capacity (CC)</label>
            <input type="number" class="form-control" id="enginesize" name="vehieng" placeholder="E.g 1500">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="lable-form" for="color">Color</label>
            <input type="text" class="form-control" id="color" name="vehicolor" placeholder="E.g Red">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <select id="vehicondition" class="show-tick form-control" name="vehicondition"
                    data-live-search="false">
                <option value="">Condition</option>
                @foreach ($vehicleconditionlist as $condition)
                    <option value="{{$condition->id}}">{{$condition->condition_name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="clear-fix"></div>
</div>
<div class="row  hover-eee">
    <div class="panel panel-default">
        <div class="panel-heading col-panel-heading" data-toggle="collapse" data-parent="#accordion"
             href="#cars">
            <h4 class="panel-title category-title2">
                Value Added Features
                <span class="ion-ios-plus-outline pull-right"></span>
            </h4>
        </div>
        <div id="cars" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="row">
                    @if($vehicleoptions)
                        <?php $i = 1; ?>
                        @foreach ($vehicleoptions as $voptions)
                            <div class="col-md-3">
                                <div class="checkbox checkbox-primary">
                                    <input id="vl-ad{{ $i }}" name="option[]"
                                           value="{{ $voptions->id}}" type="checkbox">
                                    <label for="vl-ad{{ $i }}"> {{ $voptions->display_name}} </label>
                                </div>
                            </div>
                            <?php $i++ ?>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
{{--custom validation js--}}
<script src="{{ asset('js/layout-validation/car.js') }}"></script>