<div class="row border-eee mg-b-10">
    <div class="col-md-3">
        <h4 class="category-titles primary-d">Primary Details</h4>
    </div>
    <div class="col-md-9">
        <div class="row">

            @include('layouts.frontend.filter_primary')
            <div class="col-md-4">
                <div class="form-group">
                    <select id="brand_id" name="brand_id" class="show-tick selectpicker form-control"
                            data-live-search="true">
                        <option value=""> Select Brand</option>
                        @foreach ($vehiclebrandlist as $brand)
                            <option value="{{$brand->id}}" {{(!empty($editAd->brand_id) && $brand->id==$editAd->brand_id)?'selected':''}}>
                                {{$brand->brand_name }}</option>
                        @endforeach
                    </select>
                </div>
                <label id="brand_id-error" class="error" for="brand_id" style="display: none;"></label>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" class="form-control" id="model" name="model" placeholder="Model">
                </div>
                <label id="model-error" class="error" for="model" style="display: none;"></label>
            </div>
        </div>
        <div class="clear-fix"></div>
    </div>
    <div class="clear-fix"></div>
</div>
