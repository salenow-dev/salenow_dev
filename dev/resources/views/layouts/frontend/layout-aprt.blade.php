<div class="row  mg-b-10 ">
    <div class="col-md-3">
        <h4 class="category-titles primary-d">Primary Details</h4>
    </div>
    <div class="col-md-9 mg-b-10">
        <div class="row">
                @include('layouts.frontend.filter_primary')
            <div class="col-md-4 col-sm-4">
                {{--<div class="form-group">--}}
                    {{--<input type="text" class="form-control" id="propertysize" name="propertysize"--}}
                           {{--placeholder="House Size (sqft)" value="{{(!empty($editAd->propertysize))?$editAd->propertysize:''}}">--}}
                {{--</div>--}}
                <div class="input-group input-grp-with-labl">
                    <input type="text" class="form-control" id="propertysize" name="propertysize"
                           placeholder="House Size *" value="{{ !empty($editAd->propertysize) ?preg_replace("/[^0-9,.]/", "", "$editAd->propertysize" ) :''}}">
                    <span class="input-group-addon">sqft</span>
                </div>
                <label id="propertysize-error" class="error" for="propertysize" style="display: none;"></label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group mar-top-0">
                    <label class="lable-form" for="address"></label>
                    <input type="text" class="form-control" id="address" name="address" placeholder="Address (optional)"
                           value="{{(!empty($editAd->address))?$editAd->address:''}}">
                </div>
            </div>
        </div>
    </div>
    {{--<div class="col-md-3">--}}
    {{--<div class="form-group">--}}
    {{--<label class="lable-form" for="mileage">Property Unit</label>--}}
    {{--<select id="propertyunit" class="show-tick form-control valid" name="propertyunit"--}}
    {{--data-live-search="false"--}}
    {{--aria-required="true" aria-invalid="false">--}}
    {{--<option value="">Please Select</option>--}}
    {{--<option value="Perches">Perches</option>--}}
    {{--<option value="Acres">Acres</option>--}}
    {{--</select>--}}
    {{--</div>--}}
    {{--</div>--}}
    <div class="clear-fix"></div>
</div>





