<div class="row border-eee  mg-b-10 ">
    <div class="col-md-3">
        <h4 class="category-titles primary-d">Primary Details</h4>
    </div>
    <div class="col-md-9">
        <div class="row">
            @include('layouts.frontend.filter_primary')

        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    {{--<lable>Closing Date</lable>--}}
                    <input type="text" class="form-control" id="company" name="company"
                           value="{{(!empty($editAd->company))?$editAd->company:''}}" placeholder="company Name">
                </div>
                <label id="model-error" class="error" for="company" style="display: none;"></label>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    {{--<lable>Closing Date</lable>--}}
                    <input type="text" class="form-control" id="closingdate" name="closingdate"
                           value="{{(!empty($editAd->closingdate))?$editAd->closingdate:''}}" placeholder="Closing Date" onfocus="this.type='date'" onblur="this.type='text'">
                </div>
                <label id="model-error" class="error" for="closingdate" style="display: none;"></label>
            </div>
        </div>
    </div>
    <div class="clear-fix"></div>
</div>
<script>
    var today = new Date().toISOString().split('T')[0];
    document.getElementById("closingdate").setAttribute('min', today);
</script>