<div class="row  ">
    <div class="col-md-3">
        <h4 class="category-titles primary-d">Primary Details</h4>
    </div>
    <div class="col-md-9">
        <div class="row">
            @include('layouts.frontend.filter_primary')
            <div class="col-md-4">
                <div class="form-group">
                    <select id="itemcondition" class="show-tick form-control selectpicker" name="itemcondition"
                            data-live-search="false">
                        <option value="">Select Condition</option>
                        @foreach ($vehicleconditionlist as $condition)
                            <option value="{{$condition->id}}" {{(!empty($editAd->itemcondition) && $condition->id==$editAd->itemcondition)?'selected':''}}>
                                {{$condition->condition_name}}
                            </option>
                        @endforeach
                    </select>
                </div>
                <label id="itemcondition-error" class="error" for="itemcondition" style="display: none;"></label>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <select id="brand_id" name="brand_id" class="selectpicker show-tick form-control"
                            data-live-search="true">
                        <option value="">Vehicle Brand</option>
                        @foreach ($vehiclebrandlist as $brand)
                            <option value="{{$brand->id}}" {{(!empty($editAd->brand_id) && $brand->id==$editAd->brand_id)?'selected':''}}>
                                {{$brand->brand_name }}</option>
                        @endforeach
                    </select>
                </div>
                <label id="brand_id-error" class="error" for="brand_id" style="display: none;"></label>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" class="form-control" id="model" name="model"
                           value="{{(!empty($editAd->model))?$editAd->model:''}}" placeholder="Model">
                </div>
                <label id="model-error" class="error" for="model" style="display: none;"></label>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    @if(!empty($editAd->registry_year))
                        <input type="number" class="form-control" id="registry_year" name="registry_year"
                               value="{{(!empty($editAd->registry_year))?$editAd->registry_year:''}}"
                               placeholder="E.g 35,000">
                    @else
                        <select id="registry_year" name="registry_year" class="selectpicker show-tick form-control"
                                data-live-search="true">
                            <option value="">Model Year</option>
                            <?php
                            $thisyear = date('Y');
                            $yeraslist = range($thisyear, 1970); ?>
                            @foreach ($yeraslist as $year)
                                <option value="{{$year}}">
                                    {{$year }}</option>
                            @endforeach
                        </select>
                    @endif
                </div>
                <label id="registry_year-error" class="error" for="registry_year" style="display: none;"></label>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <select id="fuel_id" class="selectpicker show-tick form-control" name="fuel_id"
                            data-live-search="false">
                        <option value="">Fuel Type</option>
                        @foreach ($vehiclefuellist as $fuel)
                            <option value="{{$fuel->id}}" {{(!empty($editAd->fuel_id) && $fuel->id==$editAd->fuel_id)?'selected':''}}>
                                {{$fuel->fuel_name }}</option>
                        @endforeach
                    </select>
                </div>
                <label id="fuel_id-error" class="error" for="fuel_id" style="display: none;"></label>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <div class="input-group">
                        <input type="number" class="form-control" id="mileage" name="mileage"
                               value="{{(!empty($editAd->mileage))?$editAd->mileage:''}}" placeholder="Mileage" min="0"/>
                        <span class="input-group-addon">Km</span>
                    </div>
                </div>
                <label id="mileage-error" class="error" for="mileage" style="display: none;"></label>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <div class="input-group">
                        <input type="number" class="form-control" id="enginesize" name="enginesize" min="0"
                               value="{{(!empty($editAd->enginesize))?$editAd->enginesize:''}}" placeholder="Engine Capacity">
                        <span class="input-group-addon">CC</span>
                    </div>
                </div>
                <label id="mileage-error" class="error" for="enginesize" style="display: none;"></label>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" class="form-control" id="color" name="color"
                           value="{{(!empty($editAd->color))?$editAd->color:''}}" placeholder="Colour">
                </div>
            </div>
        </div>
    </div>
    <div class="clear-fix "></div>
</div>




