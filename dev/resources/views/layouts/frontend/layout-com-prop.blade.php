<div class="row border-eee  mg-b-10 ">
    <div class="col-md-3">
        <h4 class="category-titles primary-d">Primary Details</h4>
    </div>
    <div class="col-md-9">
        <div class="row">
            @include('layouts.frontend.filter_primary')
            <div class="col-md-4 col-sm-4">
                {{--<div class="form-group">--}}
                    {{--<input type="number" class="form-control" name="propertysize" id="propertysize"--}}
                           {{--placeholder="Size (sqft)" min="0">--}}
                {{--</div>--}}
                <div class="input-group input-grp-with-labl">
                    <input type="text" class="form-control" id="propertysize" name="propertysize"
                           placeholder="Size *" value="{{(!empty($editAd->propertysize))?preg_replace("/[^0-9,.]/", "", "$editAd->propertysize" ):''}}">
                    <span class="input-group-addon">sqft</span>
                </div>
                <label id="propertysize-error" class="error" for="propertysize" style="display: none;"></label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">

                    <input type="text" class="form-control" id="address" name="address"
                           placeholder="Address (optional)">
                </div>
            </div>
        </div>
    </div>
    <div class="clear-fix"></div>
</div>
