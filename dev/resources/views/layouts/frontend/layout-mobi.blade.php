<div class="row mg-b-10 ">
    <div class="col-md-3">
        <h4 class="category-titles primary-d">Primary Details</h4>
    </div>
    <div class="col-md-9">
        <div class="row mg-b-10">

            @foreach($filters as $filterName => $filterValues)
                <div class="col-md-3 pd-r-0">
                    <div class="form-group">
                        <select class="show-tick selectpicker form-control"
                                id="{{strtolower(str_replace(' ','',$filterName))}}" class="show-tick form-control"
                                name="{{strtolower(str_replace(' ','',$filterName))}}" data-live-search="false">
                            <option value=""> {{$filterName}}</option>
                            @foreach ($filterValues as $id => $filtervalue)
                                <option value="{{$id}}" {{(isset($editAd->dbFilters[$id]))?'selected':''}}>
                                    {{$filtervalue}}</option>
                            @endforeach
                        </select>
                    </div>
                    <label id="{{strtolower(str_replace(' ','',$filterName))}}-error" class="error" for="{{strtolower(str_replace(' ','',$filterName))}}" style="display:none "></label>
                </div>
            @endforeach

            <div class="col-md-3 pd-r-0">
                <div class="form-group">
                    <select id="brand_id" name="brand_id" class="show-tick selectpicker form-control"
                            data-live-search="true">
                        <option value=""> Select Brand</option>
                        @foreach ($vehiclebrandlist as $brand)
                            <option value="{{$brand->id}}" {{(!empty($editAd->brand_id) && $brand->id==$editAd->brand_id)?'selected':''}}>
                                {{$brand->brand_name }}</option>
                        @endforeach
                    </select>
                </div>
                <label id="brand_id-error" class="error" for="brand_id" style="display: none;"></label>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <input type="text" class="form-control" value="{{(!empty($editAd->model))?$editAd->model:''}}"
                           id="model"
                           placeholder="Model" name="model">
                </div>
                <label id="model-error" class="error" for="model" style="display: none;"></label>
            </div>
        </div>
    </div>
</div>
<div class="row hover-eee">
    <div class="col-md-12 hover-eee">
        <div class="panel panel-default">
            <div class="panel-heading col-panel-heading" data-toggle="collapse" data-parent="#accordion"
                 href="#cars">
                <h4 class="panel-title category-title2">
                    Features
                    <span class="ion-ios-plus-outline pull-right"></span>
                </h4>
            </div>
            <div id="cars" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="row">
                        @if($features)
                            <?php
                            $i = 1;
                            $check = array();
                            if (!empty($editAd->features)) {
                                foreach ($editAd->features as $feature) {
                                    $check[$feature] = true;
                                }
                            }
                            ?>
                            @foreach ($features as $feature)
                                <div class="col-md-3 col-xs-6">
                                    <div class="checkbox checkbox-primary">
                                        <input id="vl-ad{{ $i }}" name="option[]"
                                               value="{{ $feature->id}}" type="checkbox"
                                                {{(isset($check[$feature->id]))?'checked':''}}>
                                        <label for="vl-ad{{ $i }}"> {{ $feature->display_name}} </label>
                                    </div>
                                </div>
                                <?php $i++ ?>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

