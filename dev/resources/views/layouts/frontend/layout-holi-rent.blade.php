<div class="row border-eee  mg-b-20">
<br>
</div>
<div class="row  hover-eee  mar-top-10">
    <div class="panel panel-default">
        <div class="panel-heading col-panel-heading" data-toggle="collapse" data-parent="#accordion" href="#cars">
            <h4 class="panel-title category-title2">
                Value Added Features
                <span class="ion-ios-plus-outline pull-right"></span>

            </h4>
        </div>
        <div id="cars" class="panel-collapse collapse in">
            <div class="panel-body">
                <div class="row">
                    @if($features)
                        <?php
                        $i = 1;
                        $check = array();
                        if (!empty($editAd->features)) {
                            foreach ($editAd->features as $feature) {
                                $check[$feature] = true;
                            }
                        }
                        ?>
                        @foreach ($features as $feature)
                            <div class="col-md-3">
                                <div class="checkbox checkbox-primary">
                                    <input id="vl-ad{{ $i }}" name="option[]"
                                           value="{{ $feature->id}}" type="checkbox"
                                            {{(isset($check[$feature->id]))?'checked':''}}>
                                    <label for="vl-ad{{ $i }}"> {{ $feature->display_name}} </label>
                                </div>
                            </div>
                            <?php $i++ ?>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>