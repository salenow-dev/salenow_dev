@foreach($filters as $filterName => $filterValues)
    <div class="col-md-4">
        <div class="form-group">
            <select id="{{strtolower(str_replace(' ','',$filterName))}}" class="selectpicker show-tick form-control"
                    name="{{strtolower(str_replace(' ','',$filterName))}}" data-live-search="false">
                <option value="">{{$filterName}} *</option>
                @foreach ($filterValues as $id => $filtervalue)
                    <option value="{{$id}}" {{(isset($editAd->dbFilters[$id]))?'selected':''}}>
                        {{$filtervalue}}</option>
                @endforeach
            </select>
        </div>
        <label id="{{strtolower(str_replace(' ','',$filterName))}}-error" class="error" for="{{strtolower(str_replace(' ','',$filterName))}}" style="display: none"></label>
    </div>
@endforeach