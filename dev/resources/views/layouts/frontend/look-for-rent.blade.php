@extends('layouts.postadd')
@section('title', 'Post Ad')
@section('content_post_ad')
    <?php
    $js = 'default';
    if (!empty($basedata)) {
        $category_id = (!empty($basedata['category_id'])) ? $basedata['category_id'] : '';
        $category_name = (!empty($basedata['category_name'])) ? $basedata['category_name'] : '';
        $category_code = (!empty($basedata['category_code'])) ? $basedata['category_code'] : '';
        $sub_cat_id = (!empty($basedata['sub_cat_id'])) ? $basedata['sub_cat_id'] : '';
        $sub_cat_ref = (!empty($basedata['sub_cat_ref'])) ? $basedata['sub_cat_ref'] : '';
        $sub_cat_name = (!empty($basedata['sub_cat_name'])) ? $basedata['sub_cat_name'] : '';
        $ad_type = (!empty($basedata['ad_type'])) ? $basedata['ad_type'] : '';
    }

    ?>
    <style>
        .phone-section {
            background: #eee;
            padding-top: 15px;
            padding-bottom: 15px;
            border: 1px dashed #ccc;
            border-radius: 5px;
            margin-top: 15px;
        }

        .add-phone-number-btn {
            background: #c31414;
            margin-top: 15px;
            color: #fff;
            border-radius: 30px;
        }
    </style>
    <div class="col-md-10">
        <div class="row bg-eee mg-b-10">
            <div class="col-md-12 category-details">
                <h4 class="category-title">Category</h4>
                <ul class="prime-features">
                    <li>{{$category_name}}</li>
                    <li>{{$sub_cat_name}}</li>
                </ul>
                <p><a href="/selectcat?type={{$ad_type}}" class="change-cat pull-right"><i
                                class="ion-ios-arrow-thin-left ico2"></i>&nbsp; Change Category</a></p>
            </div>
        </div>
        <div class="col-md-12">
            @include('errors.flash')
            @include('errors.list')
            <div class="alert alert-danger hide" id="error-div" role="alert">
                <i class="fa fa-exclamation-triangle"></i>&nbsp;
                <span class="error-title">Alert! The following errors were found.</span>
                <hr>
                <div id="error-message"></div>
            </div>
        </div>
        <form id="from-submit" action="" method="post" role="form" enctype="multipart/form-data">
            {{--hidden fields--}}
            <input type="hidden" name="formtype" value="{{(!empty($editAd->id))?'edit':'create'}}">
            <input type="hidden" name="adId" id="adId" value="{{(!empty($editAd->id))?$editAd->id:''}}">
            <input type="hidden" name="adRef" id="adRef" value="{{(!empty($editAd->ad_referance))?$editAd->ad_referance:''}}">
            <input type="hidden" name="category_id" value="{{$category_id}}">
            <input type="hidden" name="sub_category_id" value="{{$sub_cat_id}}">
            <input type="hidden" name="sub_category_ref" value="{{$sub_cat_ref}}">
            <input type="hidden" name="tableswitch" value="">
            <input type="hidden" name="ad_type" value="{{$ad_type}}" id="ad_type">

            <input type="hidden" name="company" value="{{(temporyPrivateLogin())?'1':'0'}}" id="company">
            {{--hidden fields--}}
            {{--START COMMON FIELDS--}}
            <section id="common">
                <div class="row  hover-eee mar-b-5-xs">
                    <div class="col-md-3 col-xs-12">
                        <h4 class="category-titles">Your City *</h4>
                    </div>
                    <div class="col-md-5 col-xs-12">
                        <div class="form-group">
                            <select id="district_id" name="district_id"
                                    class="selectpicker show-tick form-control" data-live-search="true">
                                <option default selected value=""> Select location</option>
                                @foreach ($locationdistrictslist as $district)
                                    <option value="{{$district->id}}" {{(!empty($member->district_id) && $district->id == $member->district_id)? 'selected':''}}
                                            {{((!empty($editAd->district_id)) && $district->id == $editAd->district_id)? 'selected':''}}>
                                        {{$district->district_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <label id="district_id-error" class="error" for="district_id" style="display: none;"></label>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group">
                            <select id="city_id" name="city_id" class="selectpicker show-tick form-control"
                                    data-live-search="true">
                                {{$locationcities}}

                                @if(!empty($locationcities))
                                    @foreach ($locationcities as $city)
                                        <option value="{{$city->id}}" {{(!empty($member->city_id) && $city->id == $member->city_id)? 'selected':''}}
                                                {{((!empty($editAd->city_id)) && $city->id == $editAd->city_id)? 'selected':''}}>
                                            {{$city->city_name }}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <label id="city_id-error" class="error" for="city_id" style="display: none;"></label>
                    </div>
                    <div class="clear-fix"></div>
                </div>
                <div class="row  hover-eee mar-b-5-xs">
                    <div class="col-md-3 col-xs-12">

                    </div>
                    <div class="col-md-9 col-xs-12 no-padding">
                        @include('layouts.frontend.filter_primary')
                    </div>
                    <div class="clear-fix"></div>
                </div>
            </section>
            {{--END COMMON FIELDS--}}

            {{--START COMMON FIELDS--}}
            <div class="row  mg-b-10 hover-eee " data-toggle="tooltip" data-html="true" title="
            <span class='ion-information-circled tooltip-icon'></span>
                <p class='bold-font'>Give your ad an attractive title.</p>
                <p>Think of words that people would use to search for your Ad.</p>" data-placement="right">
                <div class="col-md-3">
                    <h4 class="category-titles">Title for your Ad </h4>
                    {{--<p class="photo-txt">Good Title More Interest !</p>--}}
                </div>
                <div class="col-md-9">
                    <div class="form-group">
                        <input type="text" name="adtitle" class="form-control" id="adtitle"
                               value="{{(!empty($editAd->adtitle))?$editAd->adtitle:''}}"
                               placeholder="What's unique about your ad?">
                    </div>
                </div>
                <div class="clear-fix"></div>
            </div>
            <div class="row border-eee mg-b-10 hover-eee" data-toggle="tooltip" data-html="true" title="
            <span class='ion-information-circled tooltip-icon'></span>
                <p class='bold-font'>Items with good description sell faster!</p>
                <p>Mention other features, reason for selling and if the item is still under warranty.</p>
                <p>Remember, a good description needs at least 2-3 sentences.</p>" data-placement="right">
                <div class="col-md-3">
                    <h4 class="category-titles">Description *
                        <span class="visible-xs mobile-cution">Discription with Contact Numbers will be
                            Rejected</span></h4>
                    <p class="photo-txt hidden-xs" style="color: red;"><strong>
                            Do not add <b>Phone numbers</b> in description
                        </strong></p>
                </div>
                <div class="col-md-9 ">
                    <div class="form-group main-des-div">
                                <textarea class="form-control" name="description" rows="5" maxlength="5000"
                                          id="description">{!! (!empty($editAd->description))?$editAd->description:'' !!}
                                </textarea>
                        <p id="chars" class="text-right margin-0"></p>
                    </div>
                    <p class="photo-txt hidden-xs" style="color: red; font-size: 12px"><strong>Description with
                            Contact Numbers
                            will be Rejected</strong></p>
                </div>
                <div class="clear-fix"></div>
            </div>
            <div class="row border-eee mg-b-10 hover-eee phone-section" data-toggle="tooltip" data-html="true"
                 title="
                <span class='ion-information-circled tooltip-icon'></span>
                <p class='bold-font'>Add Phone Numbers</p>
                <p>Add your mobile number here</p>
                <p>Add at least 2 phone numbers for more inquiries </p>" data-placement="right">
                <div class="col-md-3">
                    <h4 class="category-titles">Mobile Numbers *</h4>
                </div>
                <div class="col-md-9 ">
                    {{--<div class="col-sm-4 col-md-4 col-lg-4 ">--}}
                    <img src="{{asset('')}}/images/loadingnew.gif" class="loading-post hide" id="loading"
                         width="20">
                    {{--<div class="pull-right">--}}
                    <div class="blockui text-center">
                        <button type="submit" name="formsubmit"
                                class="vehicel_btn btn  btn-lg blockui add-button add-phone-number-btn">
                            <span class="lnr lnr-phone-handset"></span> Add phone Numbers
                        </button>
                    </div>
                    {{--</div>--}}
                    <br>
                    {{--</div>--}}

                </div>
                <div class="clear-fix"></div>
            </div>
            <section id="contact">
                <p class="required-txt pull-left">* are required</p>
                <p class="pull-right"><i>By posting this ad, you agree to the <a href="#">Terms & Conditions</a>
                        of
                        this site.</i>
                </p>
                {{--<div class="row gm-b-10 ">--}}
                {{--<br class="hidden-xs">--}}
                {{--<br class="hidden-xs">--}}
                {{--<div class="col-sm-8 col-md-8 col-lg-8 ">--}}
                {{--<div class="promo_content">--}}
                {{----}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-sm-4 col-md-4 col-lg-4 ">--}}
                {{--<img src="{{asset('')}}/images/loadingnew.gif" class="loading-post hide" id="loading"--}}
                {{--width="20">--}}
                {{--<div class="pull-right">--}}
                {{--<div class="blockui">--}}
                {{--<input type="submit" name="formsubmit" value="Continue"--}}
                {{--class="vehicel_btn btn post-btn-bottom  btn-lg blockui"/>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<br>--}}
                {{--</div>--}}
                {{--</div>--}}
            </section>
            {{--END COMMON FIELDS--}}
        </form>
    </div>
    <div class="clear-fix"></div>
    <!--verify phone popup-->
    <div class="modal fade add-phone-number" id="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="ion-ios-close-empty"></i>
                    </button>
                    <h4 class="modal-title text-center"><span class="no-1"><i
                                    class="ion-ios-telephone-outline"></i> </span> &nbsp; Verify Phone Number</h4>
                </div>
                <div class="col-lg-12">
                    <p>
                    <h3>Please enter your mobile phone number</h3>
                    If you haven't verified it before, we will send you an SMS with a PIN code to make sure that we can
                    get in touch with you.</p>
                    <form id="formNumberVerify" class="verify-number-form">
                        <div id="message"></div>
                        <div class="inline-group" id="newnumber">
                            <input type="text" name="number" id="number" class="form-control inline-item enter-phone"
                                   required="required" placeholder="eg:07xxxxxxxx"/>
                            <input type="button" name="numberSubmit" id="numberSubmit"
                                   class="btn btn-primary inline-item verify-btn"
                                   value="Add Number"/>
                        </div>
                        <div class="inline-group" id="verifynumber">
                            <input type="text" name="number_verify" class="form-control inline-item enter-phone"
                                   required="required" placeholder="Enter Pin Code"/>
                            <input type="button" name="verify" id="verify"
                                   class="btn btn-primary inline-item verify-btn"
                                   value="Verify"/>
                        </div>
                    </form>
                </div>
                <div class="clear-fix"></div>
            </div>
        </div>
    </div>
    <div class="callcenterBox  ">
        <div class="boxcontent">
            <p>Need help?</p>
            <p class="number">Call us at<br>
                <span class="block">
            <a class="call" href="tel:0815662566" id="callTollFree">081 - 566 2 566</a>
             </span>
            </p>
            <p class="color-1">(10am - 7pm all 7 days)</p>
        </div>
        <a href="#" class="slider"></a>
    </div>

    <!--bootstap select with search-->
    {{ Html::script('js/salenow/plugin/select-search/bootstrap-select.min.js') }}
    {{ Html::script('js/salenow/plugin/jquery.validate.js') }}
    <script type="text/javascript" src="{{ asset("plugins/blockui/jquery.blockUI.js")}}"></script>
    <script type="text/javascript" src="{{ asset("plugins/mask/jquery.mask.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("js/layout-validation/submithandler.js") }}"></script>
    <script type="text/javascript" src="{{ asset("js/post-ad/number-verify.js") }}"></script>

    <script type="text/javascript" src="{{ asset("js/layout-validation/". $js.".js") }}"></script>
    
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".gallery-item-remove").on("click", function () {
            var id = $(this).attr('uid');
            var data = {'id': id};
            $.ajax({
                type: "POST",
                url: '/ad/deleteimage',
                data: data,
                success: function (res) {
                    if (res.message == 'success') {
                        $('#gallery_image_' + id).fadeOut(400);
                        return false;
                    }
                }
            });
        });
        //featured image delete
        $(".gallery-item-remove-featured").on("click", function () {
            var id = $(this).attr('uid');
            var data = {'id': id, 'type': 'featured'};
            $.ajax({
                type: "POST",
                url: '/ad/deleteimage',
                data: data,
                success: function (res) {
                    if (res.message == 'success') {
                        $('#featuredimage_db').val('');
                        $('#fetured_' + id).fadeOut(400);
                        return false;
                    }
                }
            });
        });
    </script>

    <script>
        $(".callcenterBox").click(function (e) {
            $(this).toggleClass('active');
        });
        $(function () {
            $('[data-toggle="tooltip"]').tooltip({
                template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner large"></div></div>'
            })
        });
    </script>
    <script type="text/javascript">
        // price mask
        $('.price').mask("#,##0", {reverse: true});
        //        character count
        $(document).ready(function () {
            //description
            $('#description').each(function () {
                var $this = $(this);
                var counter = $('#chars');
                var maxlength = $this.attr('maxlength');
                counter.text(maxlength + ' characters allowed');
                $this.bind('input keyup keydown', function (e) {
                    var value = $this.val();
                    if (value.length > 0) {
                        counter.text((maxlength - $this.val().length) + ' characters left');
                        if ((maxlength - $this.val().length) == 0) {
                            counter.css('color', 'red');
                        } else {
                            counter.css('color', '#333');
                        }
                    } else {
                        counter.text(maxlength + ' characters allowed');
                    }
                });
            });

            // Ad title
            $('#adtitle').each(function () {
                var $this = $(this);
                var counter = $('#chars_title');
                var maxlength = $this.attr('maxlength');
                counter.text(maxlength + ' characters allowed');
                $this.bind('input keyup keydown', function (e) {
                    var value = $this.val();
                    if (value.length > 0) {
                        counter.text((maxlength - $this.val().length) + ' characters left');
                        if ((maxlength - $this.val().length) == 0) {
                            counter.css('color', 'red');
                        } else {
                            counter.css('color', '#333');
                        }
                    } else {
                        counter.text(maxlength + ' characters allowed');
                    }
                });
            });
        });
    </script>

@endsection
