<?php
if (!empty($editAd->landsize)) {
    $unit = strtolower(preg_replace("/[^A-Za-z]+/", "", $editAd->landsize));
} else {
    $unit = "";
}
?>
<div class="row mg-b-10 ">
    <div class="col-md-3">
        <h4 class="category-titles primary-d">Primary Details</h4>
    </div>
    <div class="col-md-9">
        <div class="row">
            @include('layouts.frontend.filter_primary')
            <div class="col-md-4">
                <div class="form-group">
                    <input type="number" class="form-control" id="landsize" placeholder="Land Size *" name="landsize"
                           value="{{ !empty($editAd->landsize) ?preg_replace("/[^0-9,.]/", "", "$editAd->landsize" ) :''}}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <select id="landunit" class="show-tick selectpicker form-control valid" name="landunit"
                            data-live-search="false" aria-required="true" aria-invalid="false">
                        <option value="">Unit *</option>
                        <option value="Perches" {{($unit==='perches')?'selected' : ''}}>Perches</option>
                        <option value="Acres" {{($unit==='acres')?'selected' : ''}}>Acres</option>
                    </select>
                </div>
                <label id="landunit-error" class="error" for="landunit" style="display: none"></label>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <input type="text" class="form-control" id="address" name="address" placeholder="Address (Optional)"
                           value="{{(!empty($editAd->address))?$editAd->address:''}}">
                </div>
            </div>
        </div>
    </div>
    <div class="clear-fix"></div>
</div>
