<div class="row border-eee mg-b-10">
    <div class="col-md-3">
        <h4 class="category-titles primary-d">Primary Details</h4>
    </div>
    <div class="col-md-9">
        <div class="row">
            @include('layouts.frontend.filter_primary')
            <div class="col-md-4">
                <div class="form-group">
                    <select id="itemcondition" class="show-tick form-control selectpicker" name="itemcondition"
                            data-live-search="false">
                        <option value="">Select Condition</option>
                        @foreach ($vehicleconditionlist as $condition)
                            <option value="{{$condition->id}}" {{(!empty($editAd->itemcondition) && $condition->id==$editAd->itemcondition)?'selected':''}}>
                                {{$condition->condition_name}}
                            </option>
                        @endforeach
                    </select>
                </div>
                <label id="itemcondition-error" class="error" for="itemcondition" style="display: none;"></label>
            </div>
        </div>
    </div>
    <div class="clear-fix"></div>
</div>
