<div class="row border-eee mg-b-10">
    <div class="col-md-3">
        <h4 class="category-titles primary-d">Primary Details</h4>
    </div>
    <div class="col-md-9">
        <div class="row">
            @include('layouts.frontend.filter_primary')
        </div>
    </div>
    <div class="clear-fix"></div>
</div>
