<div class="row border-eee  mg-b-10 ">
    <div class="col-md-3">
        <h4 class="category-titles primary-d">Primary Details</h4>
    </div>
    <div class="col-md-9">
        <div class="row">
            @include('layouts.frontend.filter_primary')

        </div>
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <div class="form-group">
                    <input type="number" class="form-control" id="landsize" name="landsize" placeholder="Land Size"
                           value="{{(!empty($editAd->landsize))?str_replace(" Perches", "", $editAd->landsize):''}}"
                           min="0">
                </div>
                <label id="landsize-error" class="error" for="landsize" style="display: none;"></label>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="form-group">
                    <?php
                    if (!empty($editAd)) {
                        if (preg_match('(Perches|Acres)', $editAd->landsize) === 1) {
                            if (strpos($editAd->landsize, 'Perches') !== false) {
                                $size = 'Perches';
                            } else {
                                $size = 'Acres';
                            }
                        }
                    }
                    ?>
                    <select id="landunit" name="landunit" class="selectpicker  show-tick form-control"
                            data-live-search="false">
                        <option {{(!empty($size))?'selected':''}} value="">Unit *</option>
                        <option {{(!empty($size) && $size == 'Perches')?'selected':''}} value="Perches">Perches</option>
                        <option {{(!empty($size) && $size == 'Acres')?'selected':''}} value="Acres">Acres</option>
                    </select>
                </div>
                <label id="landunit-error" class="error" for="landunit" style="display: none;"></label>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="form-group">
                    <div class="input-group">
                        {{--<input type="number" class="form-control" id="propertysize" name="propertysize"--}}
                        {{--placeholder="House Size*" min="0">--}}
                        <input type="text" class="form-control" id="propertysize" name="propertysize"
                               placeholder="House Size *"
                               value="{{(!empty($editAd->propertysize))?str_replace(" sqft", "", $editAd->propertysize):''}}">
                        <span class="input-group-addon">sqft</span>
                    </div>
                </div>
                <label id="propertysize-error" class="error" for="propertysize" style="display: none;"></label>
            </div>
            <div class="clear-fix"></div>
            <div class="col-md-12">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Address(optional)" id="address" name="address"
                           value="{{(!empty($editAd->address))?$editAd->address:''}}">
                </div>
            </div>
        </div>
    </div>
    <div class="clear-fix"></div>
</div>
