@if(!empty($filters))
    {{--{{dd($editAd)}}--}}
    <div class="row border-eee mg-b-10">
        <div class="col-md-3">
            <h4 class="category-titles primary-d">Primary Details</h4>
        </div>
        <div class="col-md-9">
            <div class="row">
                @foreach($filters as $filterName => $filterValues)
                    @if($filterName === 'Type')
                        <div class="col-md-6">
                            <div class="form-group">
                                <select id="{{strtolower(str_replace(' ','',$filterName))}}"
                                        class="selectpicker show-tick form-control"
                                        name="{{strtolower(str_replace(' ','',$filterName))}}" data-live-search="false">
                                    <option value="">Room Type *</option>
                                    @foreach ($filterValues as $id => $filtervalue)
                                        <option value="{{$id}}" {{(isset($editAd->dbFilters[$id]))?'selected':''}}>
                                            {{$filtervalue}}</option>
                                        {{--<option value="{{$id}}" {{(isset($editAd->dbFilters[$id]))?'selected':''}}>--}}
                                        {{--{{$filtervalue}}</option>--}}
                                    @endforeach
                                </select>
                            </div>
                            <label id="{{strtolower(str_replace(' ','',$filterName))}}-error" class="error"
                                   for="{{strtolower(str_replace(' ','',$filterName))}}" style="display: none"></label>
                        </div>
                    @else
                        <div class="col-md-6">
                            <div class="form-group">
                                <select id="{{strtolower(str_replace(' ','',$filterName))}}"
                                        class="selectpicker show-tick form-control"
                                        name="{{strtolower(str_replace(' ','',$filterName))}}" data-live-search="false">
                                    <option value="">{{$filterName}} *</option>
                                    @foreach ($filterValues as $id => $filtervalue)
                                        <option value="{{$id}}" {{(isset($editAd->dbFilters[$id]))?'selected':''}}>
                                            {{$filtervalue}}</option>
                                        {{--<option value="{{$id}}" {{(isset($editAd->dbFilters[$id]))?'selected':''}}>--}}
                                        {{--{{$filtervalue}}</option>--}}
                                    @endforeach
                                </select>
                            </div>
                            <label id="{{strtolower(str_replace(' ','',$filterName))}}-error" class="error"
                                   for="{{strtolower(str_replace(' ','',$filterName))}}" style="display: none"></label>
                        </div>
                    @endif
                @endforeach
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <lable>Min Check In time (Optional)</lable>
                        <input type="time" class="form-control" name="min_check_in_time"
                               value="{{(!empty($editAd->min_check_in_time))?$editAd->min_check_in_time:''}}">
                    </div>
                    <label id="-error" class="error" for="" style="display: none"></label>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <lable>Max Check Out time (Optional)</lable>
                        <input type="time" class="form-control" name="max_check_out_time"
                               value="{{(!empty($editAd->max_check_out_time))?$editAd->max_check_out_time:''}}">
                    </div>
                    <label id="-error" class="error" for="" style="display: none"></label>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Address (Optional)"
                               name="address" id="address" value="{{(!empty($editAd->address))?$editAd->address:''}}">
                    </div>
                </div>
            </div>
            <div class="clear-fix"></div>
        </div>
    </div>

    <div class="row border-eee mg-b-10">
        <div class="col-md-3">
            <h4 class="category-titles primary-d">Price as Meal type</h4>
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group" style="width: 100%;">
                            <span class="input-group-addon" style="width: 50%;text-align: left;">Only Bed (OB)</span>
                            <input type="text" class="form-control price"
                                   value=""
                                   placeholder="Price" name="price_meal[]"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group" style="width: 100%;">
                            <span class="input-group-addon" style="width: 50%;text-align: left;">Bed and Breakfast (BB)</span>
                            <input type="text" class="form-control price"
                                   value=""
                                   placeholder="Price" name="price_meal[]"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group" style="width: 100%;">
                            <span class="input-group-addon" style="width: 50%;text-align: left;">Half Board (HB)</span>
                            <input type="text" class="form-control price"
                                   value=""
                                   placeholder="Price" name="price_meal[]"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group" style="width: 100%;">
                            <span class="input-group-addon" style="width: 50%;text-align: left;">Full Board (FB)</span>
                            <input type="text" class="form-control price"
                                   value=""
                                   placeholder="Price" name="price_meal[]"/>
                        </div>
                    </div>
                </div>

            </div>

            <div class="clear-fix"></div>
        </div>
    </div>
    <br>
    <div class="row mar-top-20 hover-eee">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading col-panel-heading" data-toggle="collapse" data-parent="#accordion"
                     href="#cars">
                    <h4 class="panel-title category-title2">
                        Amenities
                        <span class="ion-ios-plus-outline pull-right"></span>
                    </h4>
                </div>
                <div id="cars" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="row">
                            @if($features)
                                <?php $i = 1;
                                $check = array();
                                if (!empty($editAd->features)) {
                                    foreach ($editAd->features as $feature) {
                                        $check[$feature] = true;
                                    }
                                }
                                ?>
                                @foreach ($features as $voptions)
                                    <div class="col-md-3 col-xs-6 col-sm-3">
                                        <div class="checkbox checkbox-primary">
                                            <input id="vl-ad{{ $i }}" name="option[]"
                                                   value="{{ $voptions->id}}" type="checkbox"
                                                    {{(isset($check[$voptions->id]))?'checked':''}}>
                                            <label for="vl-ad{{ $i }}"> {{ $voptions->display_name}} </label>
                                        </div>
                                    </div>
                                    <?php $i++ ?>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
