  @include('layouts.filters.add_type')
  
@if(!empty($vehicleconditionlist))
    <div class="condition-div">
        <p class="section-title">Condition</p>
        <hr class="hidden-xs">
        @foreach($vehicleconditionlist as $vconditionlist)
            <label><input id="vehi_condition" value="{{$vconditionlist->id}}" name="vehi[condition]"
                          @if(!empty($querydata['vehi_data']['condition'])) @if($querydata['vehi_data']['condition'] == $vconditionlist->id) {{'checked'}} @endif @endif type="radio"> {{$vconditionlist->condition_name}}
            </label>
        @endforeach
        <hr class="fade-line">
    </div>
@endif
<div class="sort-div filter-by">
    <p class="section-title">Filter By</p>
    <hr class="hidden-xs">
    @if(!empty($vehiclebrandlist))
        <div class="col-sm-12 col-xs-12 pd-0-xs">
            <div class="panel filter-pannel">
                <div class="panel-heading filter-heading text-left ">
                    <h3 class="panel-title clickable panel-collapsed">All Brands</h3>
                    <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                </div>
                <div class="panel-body filter-body" id="vehicle_brand" style="{{!empty($querydata['vehi_data']['brand'])?'display:block;':'display: none;'}}">
                    @foreach($vehiclebrandlist as $vbrand)
                        <div class="checkbox  checkbox-primary">
                            <input id="{{$vbrand->id}}"
                                   @if(!empty($querydata['vehi_data']['brand']) && is_array($querydata['vehi_data']['brand']))  @if(in_array($vbrand->id,$querydata['vehi_data']['brand'])) {{'checked'}}@endif @endif name="vehi[brand][]"
                                   value="{{$vbrand->id}}" type="checkbox">
                            <label for="{{$vbrand->id}}">&nbsp;{{$vbrand->brand_name}}</label>
                        </div>
                    @endforeach

                </div>
            </div>
            <div class="clear-fix"></div>
        </div>
    @endif
    <div class="col-sm-12 col-xs-12 pd-0-xs">
        <div class="panel filter-pannel model-year-panel-head">
            <div class="panel-heading filter-heading text-left ">
                <h3 class="panel-title clickable panel-collapsed">Year</h3>
                <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
            </div>
           <div class="panel-body filter-body model-year-panel" style="{{!empty($querydata['from_year']) || !empty($querydata['to_year']) ?'display:block;':'display: none;'}}">

                <div class="col-md-6">
                    <input type="number" value="@if(!empty($querydata['from_year'])){{$querydata['from_year']}}@else {{''}} @endif" name="f_year" placeholder="Min" class="form-control">
                </div>
                <div class="col-md-6">
                    <input type="number" value="@if(!empty($querydata['to_year'])){{$querydata['to_year']}}@else {{''}} @endif" name="t_year" placeholder="Max" class="form-control">
                </div>
                <div class="clear-fix"></div>
                <button type="button" id="vehi_filter2" class="btn btn-warning pull-right">Filter</button>
            </div>
        </div>
    </div>


   <select id="vehicle_milage" name="vehi[milage]" class="selectpicker">
        <option value="">Mileage</option>
        <option @if(!empty($querydata['vehi_data']['milage'])) @if($querydata['vehi_data']['milage'] == '0-5000') {{'selected'}}@endif @endif  value="0-5000">
            <5,000 km
        </option>
        <option @if(!empty($querydata['vehi_data']['milage'])) @if($querydata['vehi_data']['milage'] == '5000-10000') {{'selected'}}@endif @endif  value="5000-10000">
            5,000km - 10,000 km
        </option>
        <option @if(!empty($querydata['vehi_data']['milage'])) @if($querydata['vehi_data']['milage'] == '10000-30000') {{'selected'}}@endif @endif  value="10000-30000">
            10,000km - 30,000 km
        </option>
        <option @if(!empty($querydata['vehi_data']['milage'])) @if($querydata['vehi_data']['milage'] == '30000-50000') {{'selected'}}@endif @endif  value="30000-50000">
            30,000km - 50,000 km
        </option>
        <option @if(!empty($querydata['vehi_data']['milage'])) @if($querydata['vehi_data']['milage'] == '50000-100000') {{'selected'}}@endif @endif  value="50000-100000">
            50,000km - 100,000 km
        </option>
        <option @if(!empty($querydata['vehi_data']['milage'])) @if($querydata['vehi_data']['milage'] == '100000-1500000') {{'selected'}}@endif @endif  value="100000-1500000">
            100,000km - 1500,000 km
        </option>
        <option @if(!empty($querydata['vehi_data']['milage'])) @if($querydata['vehi_data']['milage'] == '150000-2500000') {{'selected'}}@endif @endif  value="150000-2500000">
            1500,000km - 2500,000 km
        </option>
    </select>
    @if(!empty($vehiclefuellist))
        <select name="vehi[fuel]" class="selectpicker">

            <option value="">Fuel Type</option>
            @foreach($vehiclefuellist as $vfuel)
                <option @if(!empty($querydata['vehi_data']['fuel']))  @if($querydata['vehi_data']['fuel'] ==$vfuel->id) {{'selected'}}@endif @endif value="{{$vfuel->id}}">{{$vfuel->fuel_name}}</option>
            @endforeach

        </select>
    @endif
    <div class="clear-fix"></div>
    <hr class="fade-line">
 </div>

  @include('layouts.filters.price_range')

