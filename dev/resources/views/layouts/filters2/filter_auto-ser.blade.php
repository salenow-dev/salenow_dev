@include('layouts.filters.add_type')

<div class="sort-div">
    <p class="section-title">Filter By</p>
    <hr class="hidden-xs">
    @foreach($filters as $filterName => $filterValues)
        <div class="col-sm-12 col-xs-12 pd-0-xs">
            <div class="panel filter-pannel">
                <div class="panel-heading filter-heading text-left ">
                    <h3 class="panel-title clickable panel-collapsed">Service Type</h3>
                    <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                </div>

                <div class="panel-body filter-body" id="vehi_service_type"
                     style="{{!empty($querydata['vehi_data']['service'])?'display:block;':'display: none;'}}">
                    @foreach ($filterValues as $id => $filtervalue)
                        <div class="checkbox  checkbox-primary">
                            <input id="{{$id}}"
                                   @if(!empty($querydata['vehi_data']['service']) && is_array($querydata['vehi_data']['service']))  @if(in_array($id,$querydata['vehi_data']['service'])) {{'checked'}}@endif @endif name="vehi[service][]"
                                   value="{{$id}}" type="checkbox">
                            <label for="{{$id}}">&nbsp;{{$filtervalue}}</label>
                        </div>
                    @endforeach

                </div>

            </div>
        </div>
    @endforeach
    <div class="clear-fix"></div>
    <hr class="fade-line">
</div>

@include('layouts.filters.price_range')

