@include('layouts.filters.add_type')

<div id="elec_condition">
    @foreach($filters as $filterName => $filterValues)
        @if($filterName == 'Item condition')
            <div class="condition-div">
                <p class="section-title">Condition</p>
                <hr class="hidden-xs">
                @foreach ($filterValues as $id => $filtervalue)
                    <label><input value="{{$id}}" name="comn[filter_condition]"
                                  @if(!empty($querydata['comn_data']['filter_condition'])) @if($querydata['comn_data']['filter_condition'] == $id) {{'checked'}} @endif @endif type="radio"> {{$filtervalue}}
                    </label>
                @endforeach
                <hr class="fade-line">
            </div>
        @endif
    @endforeach
</div>
<div class="sort-div">
    <p class="section-title">Filter By</p>
    <hr class="hidden-xs">
    @foreach($filters as $filterName => $filterValues)
        @if($filterName == 'Baths')
            <div class="col-sm-12 col-xs-12 pd-0-xs">
                <div class="panel filter-pannel">
                    <div class="panel-heading filter-heading text-left ">
                        <h3 class="panel-title clickable panel-collapsed">{{$filterName}}</h3>
                        <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                    </div>

                    <div class="panel-body filter-body" id="elec_service_type"
                         style="{{!empty($querydata['prop_data']['Baths'])?'display:block;':'display: none;'}}">
                        @foreach ($filterValues as $id => $filtervalue)
                            <div class="checkbox  checkbox-primary">
                                <input id="{{$id}}"
                                       @if(!empty($querydata['prop_data']['Baths'])  && is_array($querydata['prop_data']['Baths']))  @if(in_array($id,$querydata['prop_data']['Baths'])) {{'checked'}}@endif @endif name="prop[Baths][]"
                                       value="{{$id}}" type="checkbox">
                                <label for="{{$id}}">&nbsp;{{$filtervalue}}</label>
                            </div>
                        @endforeach

                    </div>


                </div>
            </div>
        @endif
    @endforeach
    @foreach($filters as $filterName => $filterValues)
        @if($filterName == 'Beds')
            <div class="col-sm-12 col-xs-12 pd-0-xs">
                <div class="panel filter-pannel">
                    <div class="panel-heading filter-heading text-left ">
                        <h3 class="panel-title clickable panel-collapsed">{{$filterName}}</h3>
                        <span class="pull-right  "><span class="bs-caret"><span class="caret"></span></span></span>
                    </div>

                    <div class="panel-body filter-body" id="elec_service_type"
                         style="{{!empty($querydata['prop_data']['Beds'])?'display:block;':'display: none;'}}">
                        @foreach ($filterValues as $id => $filtervalue)
                            <div class="checkbox  checkbox-primary">
                                <input id="{{$id}}"
                                       @if(!empty($querydata['prop_data']['Beds']) && is_array($querydata['prop_data']['Beds']))  @if(in_array($id,$querydata['prop_data']['Beds'])) {{'checked'}}@endif @endif name="prop[Beds][]"
                                       value="{{$id}}" type="checkbox">
                                <label for="{{$id}}">&nbsp;{{$filtervalue}}</label>
                            </div>
                        @endforeach

                    </div>


                </div>
            </div>
        @endif
    @endforeach
    <div class="col-sm-12 col-xs-12 pd-0-xs">
        <div>
            <input name="prop[land_size]" type="text" class="model-input-search2 form-control" placeholder="Land Size">

        </div>
    </div>
    <div class="col-sm-12 col-xs-12 pd-0-xs">
        <div>
            <input name="prop[property_size]" type="text" class="model-input-search3 form-control"
                   placeholder="House Size">

        </div>
    </div>

    <div class="clear-fix"></div>
    <button type="button" id="elec_filter1" class="btn btn-warning pull-right">Filter</button>
    <div class="clear-fix"></div>
    <hr class="fade-line">
</div>


@include('layouts.filters.price_range')


