{{--{{dd($querydata)}}--}}
<div class="budget-div">
    <p class="section-title">Budget</p>
    <hr class="hidden-xs">
    {{--<p class="pr-title">From</p>--}}
    {{--<div class="range-slider">--}}
    {{--<input class="range-slider__range" name="f_price"--}}
    {{--value="@if(!empty($querydata['from_price'])){{$querydata['from_price']}}@else{{0}}@endif" min="0"--}}
    {{--max="100000" type="range" step="100">--}}
    {{--<span class="range-slider__value">@if(!empty($querydata['from_price'])){{$querydata['from_price']}}@else {{0}} @endif</span>--}}
    {{--</div>--}}

    {{--<p class="pr-title">To</p>--}}
    {{--<div class="range-slider">--}}
    {{--<input name="t_price" class="range-slider__range"--}}
    {{--value="@if(!empty($querydata['to_price'])){{$querydata['to_price']}}@else {{150000}} @endif" min="0"--}}
    {{--max="5000000" type="range" step="100">--}}
    {{--<span class="range-slider__value">@if(!empty($querydata['to_price'])){{$querydata['to_price']}}@else {{150000}} @endif</span>--}}
    {{--</div>--}}
    <div class="col-md-12 pd-0-xs">
        <label class="text-left price-input-lables">From</label>
        <input type="text" name="f_price" id="f_price"
               value="{{(!empty($querydata['from_price']))?$querydata['from_price']:'100'}}" placeholder=""
               class="form-control price-input-field">
    </div>
    <div class="clear-fix"></div>
    <div class="col-md-12 pd-0-xs">
        <label class="text-left price-input-lables">To</label>
        <input type="text" name="t_price" id="t_price"
               value="{{(!empty($querydata['to_price']))?$querydata['to_price']:''}}" placeholder=""
               class="form-control price-input-field">
    </div>
    <button type="button" id="btn_price_range" class="btn btn-warning pull-right">Filter</button>
    <div class="clear-fix"></div>
    <hr class="fade-line">
</div>

