@if(!empty($addtypelist))
<div class="condition-div">
    <p class="section-title">Ad Type</p>
    <hr class="hidden-xs">
   @foreach($addtypelist as $addtype) 
    <label><input type="radio" value="{{$addtype['typeref']}}" @if($querydata['addtype'] == $addtype['typeref']) {{'checked'}} @endif name="adtype"/>
        {{$addtype['typename']}}</label>
@endforeach
    <hr class="fade-line">
</div>
@endif