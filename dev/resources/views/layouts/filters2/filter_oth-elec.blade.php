@include('layouts.filters.add_type')
  
<div id="elec_condition">
  @foreach($filters as $filterName => $filterValues)
      @if($filterName == 'Item condition')
    <div class="condition-div">
        <p class="section-title">Condition</p>
        <hr class="hidden-xs">
         @foreach ($filterValues as $id => $filtervalue)
            <label><input value="{{$id}}" name="comn[filter_condition]"
                          @if(!empty($querydata['comn_data']['filter_condition'])) @if($querydata['comn_data']['filter_condition'] == $id) {{'checked'}} @endif @endif type="radio"> {{$filtervalue}}
            </label>
         @endforeach
        <hr class="fade-line">
    </div>
    @endif
    @endforeach
</div>
  @include('layouts.filters.price_range')
