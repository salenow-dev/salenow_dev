@include('main.header')
@yield('content')
<div class="hidden-xs">
    @include('main.footer')
</div>
