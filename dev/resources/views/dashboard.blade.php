@extends('backend.backend_dashboard_layout')
@section('title', 'Dashboard')
@section('content')
    <!-- START WIDGETS -->
    <div class="row">
        <div class="col-md-3">
            <!-- START WIDGET SLIDER -->
            <div class="widget widget-default widget-carousel">
                <div class="owl-carousel" id="owl-example">
                    <div>
                        <div class="widget-title">Today Members</div>
                        <div class="widget-subtitle">{{\Carbon\Carbon::now()->toFormattedDateString()}}</div>
                        <div class="widget-int">{{$todayMemberCount}}</div>
                    </div>

                </div>
            </div>
            <!-- END WIDGET SLIDER -->
        </div>
        <div class="col-md-3">
            <!-- START WIDGET MESSAGES -->
            <div class="widget widget-default widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-users"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count">{{$memberCount}}</div>
                    <div class="widget-title">Registred Members</div>
                    <div class="widget-subtitle">in SaleMe.lk</div>
                </div>

            </div>
            <!-- END WIDGET MESSAGES -->
        </div>
        <div class="col-md-3">
            <!-- START WIDGET REGISTRED -->
            <div class="widget widget-default widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-tags"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count">{{$todatAds}}</div>
                    <div class="widget-title">Today Ads</div>
                    <div class="widget-subtitle">On SaleMe.lk</div>
                </div>

            </div>
            <!-- END WIDGET REGISTRED -->
        </div>
        <div class="col-md-3">
            <!-- START WIDGET CLOCK -->
            <div class="widget widget-danger widget-padding-sm">
                <div class="widget-big-int plugin-clock">00:00</div>
                <div class="widget-subtitle plugin-date">Loading...</div>
                {{--<div class="widget-buttons widget-c3">--}}
                {{--<div class="col">--}}
                {{--<a href="#"><span class="fa fa-clock-o"></span></a>--}}
                {{--</div>--}}
                {{--<div class="col">--}}
                {{--<a href="#"><span class="fa fa-bell"></span></a>--}}
                {{--</div>--}}
                {{--<div class="col">--}}
                {{--<a href="#"><span class="fa fa-calendar"></span></a>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
            <!-- END WIDGET CLOCK -->
        </div>
    </div>
    <!-- END WIDGETS -->
    <div class="row">
        <div class="col-md-3">
            <!-- START PROJECTS BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Ads Count Analysis</h3>
                        <span>Last Two Weeks</span>
                    </div>

                </div>
                <div class="panel-body panel-body-table">
                    <div class="table-responsive">
                        <table class="table table-condensed table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="50%">Date</th>
                                <th width="20%">Ads</th>
                                {{--<th width="20%">Member Count</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($pastWeekAds))
                                {{--{{dd($pastWeekAds)}}--}}
                                @foreach($pastWeekAds as $item)
                                    <?php $myDate = new \Carbon\Carbon($item->date); ?>
                                    {{--$item->created_at->toFormattedDateString()--}}
                                    <tr>
                                        <td><strong>{{$myDate->format('M d, l')}}</strong></td>
                                        <td><span class="label label-danger">{{$item->count}}</span></td>
                                        {{--<td><span class="label label-primary">{{$item->member_count}}</span></td>--}}
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END PROJECTS BLOCK -->
        </div>
        <div class="col-md-6">

            <!-- START LINE CHART -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Ads Count Graph</h3>
                        <span>Last Two Weeks</span>
                    </div>
                </div>
                <div class="panel-body">
                    <div id="morris-line-example" style="height: 400px;"></div>
                </div>
            </div>
            <!-- END LINE CHART -->

        </div>
        <div class="col-md-3">
            <!-- START PROJECTS BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Premium Members</h3>
                        <span>Expired as at <b>{{\Carbon\Carbon::now()->toFormattedDateString()}}</b></span>
                    </div>

                </div>
                <div class="panel-body panel-body-table">
                    <div class="table-responsive">
                        <table class="table table-condensed table-bordered table-striped">
                            {{--<thead>--}}
                            {{--<tr>--}}
                                {{--<th width="">#</th>--}}
                                {{--<th width="100%">Member</th>--}}
                            {{--</tr>--}}
                            {{--</thead>--}}
                            <tbody>
                            @if(count($expiredMembers))
                                <?php $x = 1; ?>
                                @foreach($expiredMembers as $mem)
                                    <tr>
                                        <td>{{$x}}</td>
                                        <td>
                                            Name : <b>{{$mem->company_name}}</b><br>
                                            Contact : <b>
                                                @if(count($mem->mobile))
                                                    @foreach($mem->mobile as $mob)
                                                        {{$mob}} /
                                                    @endforeach
                                                @endif
                                            </b><br>
                                            Expired On : <b>
                                                <?php
                                                $expiredate = \Carbon\Carbon::parse($mem->expire_on);
                                                ?>
                                                {{$expiredate->toFormattedDateString()}}</b><br>
                                            <span class="text-danger pull-right">{{$expiredate->diffInDays(\Carbon\Carbon::now())}}
                                                days Ago</span>
                                        </td>
                                    </tr>
                                    <?php $x++; ?>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {{--expire this week --}}

            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <span>Expired as at <b>{{\Carbon\Carbon::now()->toFormattedDateString()}}</b></span>
                    </div>

                </div>
                <div class="panel-body panel-body-table">
                    <div class="table-responsive">
                        <table class="table table-condensed table-bordered table-striped">
                            <tbody>
                            @if(count($expirThisWeek))
                                <?php $x = 1; ?>
                                @foreach($expirThisWeek as $mem)
                                    <tr>
                                        <td>{{$x}}</td>
                                        <td>
                                            Name : <b>{{$mem->company_name}}</b><br>
                                            Contact : <b>
                                                @if(count($mem->mobile))
                                                    @foreach($mem->mobile as $mob)
                                                        {{$mob}} /
                                                    @endforeach
                                                @endif
                                            </b><br>
                                            Expired On : <b>
                                                <?php
                                                $expiredate = \Carbon\Carbon::parse($mem->expire_on);
                                                ?>
                                                {{$expiredate->toFormattedDateString()}}</b><br>
                                            <span class="text-success pull-right">Will expire {{$expiredate->diffInDays(\Carbon\Carbon::now())}}
                                                Within days</span>
                                        </td>
                                    </tr>
                                    <?php $x++; ?>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END PROJECTS BLOCK -->
        </div>

    </div>


    {{--<div class="row">--}}
    {{--<div class="col-md-4">--}}
    {{--<!-- START SALES & EVENTS BLOCK -->--}}
    {{--<div class="panel panel-default">--}}
    {{--<div class="panel-heading">--}}
    {{--<div class="panel-title-box">--}}
    {{--<h3>Sales & Event</h3>--}}
    {{--<span>Event "Purchase Button"</span>--}}
    {{--</div>--}}
    {{--<ul class="panel-controls" style="margin-top: 2px;">--}}
    {{--<li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>--}}
    {{--<li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>--}}
    {{--<li class="dropdown">--}}
    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span--}}
    {{--class="fa fa-cog"></span></a>--}}
    {{--<ul class="dropdown-menu">--}}
    {{--<li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span>--}}
    {{--Collapse</a></li>--}}
    {{--<li><a href="#" class="panel-remove"><span class="fa fa-times"></span>--}}
    {{--Remove</a></li>--}}
    {{--</ul>--}}
    {{--</li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--<div class="panel-body padding-0">--}}
    {{--<div class="chart-holder" id="dashboard-line-1" style="height: 200px;"></div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<!-- END SALES & EVENTS BLOCK -->--}}
    {{--</div>--}}
    {{--<div class="col-md-4">--}}
    {{--<!-- START USERS ACTIVITY BLOCK -->--}}
    {{--<div class="panel panel-default">--}}
    {{--<div class="panel-heading">--}}
    {{--<div class="panel-title-box">--}}
    {{--<h3>Users Activity</h3>--}}
    {{--<span>Users vs returning</span>--}}
    {{--</div>--}}
    {{--<ul class="panel-controls" style="margin-top: 2px;">--}}
    {{--<li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>--}}
    {{--<li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>--}}
    {{--<li class="dropdown">--}}
    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span--}}
    {{--class="fa fa-cog"></span></a>--}}
    {{--<ul class="dropdown-menu">--}}
    {{--<li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span>--}}
    {{--Collapse</a></li>--}}
    {{--<li><a href="#" class="panel-remove"><span class="fa fa-times"></span>--}}
    {{--Remove</a></li>--}}
    {{--</ul>--}}
    {{--</li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--<div class="panel-body padding-0">--}}
    {{--<div class="chart-holder" id="dashboard-bar-1" style="height: 200px;"></div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<!-- END USERS ACTIVITY BLOCK -->--}}
    {{--</div>--}}
    {{--<div class="col-md-4">--}}
    {{--<!-- START VISITORS BLOCK -->--}}
    {{--<div class="panel panel-default">--}}
    {{--<div class="panel-heading">--}}
    {{--<div class="panel-title-box">--}}
    {{--<h3>Visitors</h3>--}}
    {{--<span>Visitors (last month)</span>--}}
    {{--</div>--}}
    {{--<ul class="panel-controls" style="margin-top: 2px;">--}}
    {{--<li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>--}}
    {{--<li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>--}}
    {{--<li class="dropdown">--}}
    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span--}}
    {{--class="fa fa-cog"></span></a>--}}
    {{--<ul class="dropdown-menu">--}}
    {{--<li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span>--}}
    {{--Collapse</a></li>--}}
    {{--<li><a href="#" class="panel-remove"><span class="fa fa-times"></span>--}}
    {{--Remove</a></li>--}}
    {{--</ul>--}}
    {{--</li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--<div class="panel-body padding-0">--}}
    {{--<div class="chart-holder" id="dashboard-donut-1" style="height: 200px;"></div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<!-- END VISITORS BLOCK -->--}}
    {{--</div>--}}
    {{--</div>--}}
    <!-- START DASHBOARD CHART -->
    {{--<div class="block-full-width">--}}
    {{--<div id="dashboard-chart" style="height: 250px; width: 100%; float: left;"></div>--}}
    {{--<div class="chart-legend">--}}
    {{--<div id="dashboard-legend"></div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <!-- END DASHBOARD CHART -->
    <script type="text/javascript" src="{{ asset("backend/assets/js/plugins/morris/raphael-min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("backend/assets/js/plugins/morris/morris.min.js") }}"></script>
    <script type="text/javascript">
        var morrisCharts = function () {

            Morris.Line({
                element: 'morris-line-example',
                data: [
                        @foreach($pastWeekAds as $item)
                    {
                        y: '{{$item->date}}', a: '{{$item->count}}'
                    },
                    @endforeach
                ],
                xkey: 'y',
                ykeys: ['a'],
                labels: ['Ads'],
                resize: true,
                lineColors: ['#33414E']
            });
        }();
    </script>
@endsection