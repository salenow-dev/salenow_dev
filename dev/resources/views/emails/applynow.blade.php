<?php
/**
 * Created by PhpStorm.
 * User: 006342
 * Date: 5/19/2018
 * Time: 4:42 PM
 */
?>
        <!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title></title>
    <style>
        @media only screen and (max-width: 580px) {

        }
    </style>
</head>
<body style="margin: 0; padding: 0; width: 100%; background-color: #F2F4F6;">
<table style="    margin: auto; max-width: 600px; font-size: 12px;line-height: 19px;
                    padding: 20px 0 5px 0;background: url() repeat #f2f4f6;color: #444444; text-align: center;"
       width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td style="width: 100%; margin: 0; padding: 0; background-color: #F2F4F6;" align="center">
            <table width="100%" cellpadding="0" cellspacing="0">

                <!-- Logo -->
                <tr>
                </tr>
                <tr>
                    <td>
                        <div style="float:left"><a href="https://saleme.lk">
                                <img src="https://res.cloudinary.com/wfx/image/upload/v1499862015/saleme-logo_wxhphw.png"
                                     width="115" alt="" class="CToWUd">
                            </a></div>

                        <div style="float:right;text-align:center;margin-top:3px">
                            <a href=""
                               style="width:125px;height:30px;color:#000;border:1px solid #000;display:block;text-decoration:none;font-size:13px;border-radius:3px;line-height:30px"
                               target="_blank" data-saferedirecturl="">Post Free Ad</a>
                        </div>
                    </td>
                </tr>
                <!-- Email Body -->
                <tr>
                    <td style="width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; border-bottom: 1px solid #EDEFF2; background-color: #FFF;"
                        width="100%">
                        <table style="width: auto; max-width: 570px; margin: 0 auto; padding: 0;" align="center"
                               width="570" cellpadding="0"
                               cellspacing="0">
                            <tr>
                                <td style="font-family: Arial, &#039;Helvetica Neue&#039;, Helvetica, sans-serif; padding: 35px;">
                                    <p style="text-align:center;margin:0 0 2px 0;padding:0">
                                        <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1526963875/job_ymyfwg.png"
                                             width="75" height="75" alt="" class="CToWUd">
                                    </p>
                                    <div style="font-size:25px;padding:5px 0;text-align:center;color:#000000;margin-bottom:30px">
                                        <p style="margin:0;padding:0;line-height:25px">Your Have New CV!</p>
                                        <div style="    width: 140px;height: 3px;background: #ffa70e;margin: auto;margin-top: 15px;"></div>
                                    </div>
                                    <br>
                                    <h1 style="margin-top: 0; color: #2F3133; font-size: 19px; font-weight: bold; text-align: left;">
                                        Hello {{ $reciver }},</h1>
                                    <!-- Intro -->
                                    <p style="margin-top: 0; color: #74787E; font-size: 14px; line-height: 1.5em;">
                                        Your Have New Cv for <b>{{ !empty($position)? $position :'' }}</b>
                                    </p>
                                    <p style="margin-top: 0; color: #74787E; font-size: 14px; line-height: 1.5em;">
                                        Name : {{ $applicentName }}
                                    </p>
                                    <p style="margin-top: 0; color: #74787E; font-size: 14px; line-height: 1.5em;">
                                        Email : {{ $applicentEmail }}
                                    </p>
                                    <p style="margin-top: 0; color: #74787E; font-size: 14px; line-height: 1.5em;">
                                        Cover Note: {{ !empty($applicentCoverNote)?$applicentCoverNote:'-' }}
                                    </p>
                                    <br>
                                    <p style="margin-top: 0;  font-size: 14px; line-height: 1.5em;    background: #fea502;    padding: 10px;    color: #fff;    font-weight: 700;    font-size: 16px;">
                                        Curriculum Vitae (CV) attached. Please find the attachment.
                                    </p>
                                    <br>
                                {{--<center><a>--}}
                                {{--<button type="button"--}}
                                {{--style=" background-color: #fea502;border: none; color: black;padding: 11px 55px; text-align: center;text-decoration: none; display: inline-block; font-size: 25px;margin: 9px 6px;">--}}
                                {{--<img src="https://res.cloudinary.com/saleme-lk/image/upload/v1526963877/down-arrow_bbavru.png"--}}
                                {{--style="margin-right: 14px; margin-top: 4px;" width="23" height="23"--}}
                                {{--alt="" class="CToWUd"> Download Cv--}}
                                {{--</button>--}}
                                {{--</center>--}}


                                <!-- Action Button -->
                                    <!-- Outro -->
                                    <!-- Salutation -->
                                    <div style="width:93%;font-size: 14px;float:left;padding:20px 3.5% 20px 0">
                                        Thank you,<br>
                                        <strong>Team <span class="il">Saleme.lk</span></strong>
                                    </div>
                                    <!-- Sub Copy -->
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- Footer -->
                <tr>
                    <td>
                        <table style="margin-top: 20px; margin-bottom: -20px;" cellpadding="0" cellspacing="0"
                               align="center" border="0">
                            <tbody>
                            <tr>
                                <td width="27">
                                    <a href="" target="_blank" data-saferedirecturl="" width="27" height="27"
                                       alt="Facebook" class="CToWUd">
                                        <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1500274188/unnamed_4_zq4trt.gif"
                                             width="27" height="27" alt="Google Plus" class="CToWUd">
                                    </a>
                                </td>
                                <td width="6">
                                <td width="27">
                                    <a href="" target="_blank" data-saferedirecturl="" width="27" height="27"
                                       alt="Twitter" class="CToWUd">
                                        <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1500274609/unnamed_5_pjqgvk.gif"
                                             width="27" height="27" alt="Google Plus" class="CToWUd">
                                    </a>
                                </td>
                                <td width="6">

                                </td>
                                <td width="27">
                                    <a href="" target="_blank"
                                       data-saferedirecturl="">
                                        <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1500274188/unnamed_dntuvv.gif"
                                             width="27" height="27" alt="Google Plus" class="CToWUd">
                                    </a>
                                </td>
                                <td width="6"></td>
                                <td width="27">
                                    <a href="" target="_blank"
                                       data-saferedirecturl="">
                                        <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1500274189/unnamed_3_iwunsm.gif"
                                             width="27" height="27" alt="Linkedin" class="CToWUd">
                                    </a>
                                </td>
                                <td width="6">

                                </td>
                                <td width="27">
                                    <a href="" target="_blank"
                                       data-saferedirecturl="">
                                        <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1500274189/unnamed_2_c00ycp.gif"
                                             width="27" height="27" alt="Youtube" class="CToWUd">
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <table style="width: auto; max-width: 570px; margin: 0 auto; padding: 0; text-align: center;"
                               align="center" width="570" cellpadding="0"
                               cellspacing="0">
                            <tr>
                                <td style="font-family: Arial, &#039;Helvetica Neue&#039;, Helvetica, sans-serif; color: #AEAEAE; padding: 35px; text-align: center;">
                                    <p style="margin-top: 0; color: #74787E; font-size: 12px; line-height: 1.5em;">
                                        &copy; 2018
                                        <a style="color: #3869D4;" href="http://saleme.me"
                                           target="_blank">SaleMe.lk Free Classified Sri Lanka</a>.
                                        All rights reserved.
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>

