<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title></title>
    <style>
        @media only screen and (max-width: 580px) {

        }

        ul li {
            list-style: none;
            font-size: 14px;
            color: #888;
            padding: 5px;
        }
    </style>
</head>
<body style="margin: 0; padding: 0; width: 100%; background-color: #F2F4F6;">
<table style="    margin: auto; max-width: 600px; font-size: 12px;line-height: 19px;
        padding: 20px 0 5px 0;background: url() repeat #f2f4f6;color: #444444; text-align: center;"
       width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td style="width: 100%; margin: 0; padding: 0; background-color: #F2F4F6;" align="center">
            <table width="100%" cellpadding="0" cellspacing="0">

                <!-- Logo -->
                <tr>
                </tr>
                <tr>
                    <td>
                        <div style="float:left"><a href="https://saleme.lk">
                                <img src="https://res.cloudinary.com/wfx/image/upload/v1499862015/saleme-logo_wxhphw.png"
                                     width="115" alt="" class="CToWUd">
                            </a></div>

                        <div style="float:right;text-align:center;margin-top:3px">
                            <a href=""
                               style="width:125px;height:30px;color:#000;border:1px solid #000;display:block;text-decoration:none;font-size:13px;border-radius:3px;line-height:30px"
                               target="_blank" data-saferedirecturl="">Post Free Ad</a>
                        </div>
                    </td>
                </tr>
                <!-- Email Body -->
                <tr>
                    <td style="width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; border-bottom: 1px solid #EDEFF2; background-color: #FFF;"
                        width="100%">
                        <table style="width: auto; max-width: 570px; margin: 0 auto; padding: 0;" align="center"
                               width="570" cellpadding="0"
                               cellspacing="0">
                            <tr>
                                <td style="font-family: Arial, &#039;Helvetica Neue&#039;, Helvetica, sans-serif; padding: 35px;">
                                    <p style="text-align:center;margin:0 0 2px 0;padding:0">
                                        <img src="http://res.cloudinary.com/saleme-lk/image/upload/v1530509465/gift_r0av8w.png"
                                             width="75" height="75" alt="" class="CToWUd">
                                    </p>
                                    <div style="font-size:25px;padding:5px 0;text-align:center;color:#000000;margin-bottom:30px">
                                        <p style="margin:0;padding:0;line-height:25px">{{ $itemTitle }} Ordered
                                            successfully</p>
                                        <div style="    width: 140px;height: 3px;background: #ffa70e;margin: auto;margin-top: 15px;"></div>
                                    </div>
                                    <br>
                                    <h1 style="margin-top: 0; color: #2F3133; font-size: 17px; font-weight: bold; text-align: left;">
                                        Dear {{ $name }},</h1>
                                    <!-- Intro -->
                                    <p style="margin-top: 0; color: #74787E; font-size: 14px; line-height: 1.5em;">
                                        Your order placed successfully. you will recived the item soon.
                                    </p>
                                    <p style="margin-top: 0; color: #74787E; font-size: 14px; line-height: 1.5em;">
                                        <b>Shipping details as following:</b>
                                    </p>
                                    <ul>
                                        <li>
                                            Name : <b>{{$name}}</b>
                                        </li>
                                        <li>
                                            Address : <b>{{$address}}</b>
                                        </li>
                                        <li>
                                            Phone : <b>{{$mobile}}</b>
                                        </li>
                                    </ul>

                                    <p style="margin-top: 0; color: #74787E; font-size: 14px; line-height: 1.5em;">
                                        <b>Item details as following:</b>
                                    </p>
                                    <ul>
                                        <li>
                                            Title : <b>{{$itemTitle}}</b>
                                        </li>
                                        <li>
                                            Description : <b>{{$itemDescription}}</b>
                                        </li>
                                        <li>
                                            Points : <b>{{$itemMinPints}} SaleMe Coins</b>
                                        </li>
                                        <li>
                                            <img src="{{asset('')}}/gamezone/img/store/{{$itemImage}}"
                                                 width="200px">
                                        </li>
                                    </ul>
                                    {{--<p style="margin-top: 0; color: #74787E; font-size: 14px; line-height: 1.5em;">--}}
                                        {{--You will recieve {{$itemTitle}} within 14 days--}}
                                    {{--</p>--}}
                                    <p style="margin-top: 0; color: #74787E; font-size: 14px; line-height: 1.5em;">
                                        <a href="{{asset('')}}playzone/profile">View purchase history</a>
                                    </p>
                                    <div style="width:93%;font-size: 14px;float:left;padding:20px 3.5% 20px 0">
                                        Thank you,<br>
                                        <strong>Team <span class="il">Saleme.lk</span></strong>
                                    </div>
                                    <div style="width:93%;font-size: 14px;float:left;padding:10px 3.5% 20px 0">
                                        <b>For Inquiries:</b> <br>
                                        Please call us on <strong>011 5 81 81 88</strong> (9.00 am - 5.00pm)<br>
                                        Please call us on <strong>076 3 81 81 88</strong> (24 x 7)<br>
                                        Please send us a Email <strong>support@saleme.lk</strong><br>

                                    </div>
                                    <!-- Sub Copy -->
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- Footer -->
                <tr>
                    <td>
                        <table style="margin-top: 20px; margin-bottom: -20px;" cellpadding="0" cellspacing="0"
                               align="center" border="0">
                            <tbody>
                            <tr>
                                <td width="27">
                                    <a href="https://www.facebook.com/salemeclassified/" target="_blank"
                                       data-saferedirecturl="" width="27" height="27"
                                       alt="Facebook" class="CToWUd">
                                        <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1500274188/unnamed_4_zq4trt.gif"
                                             width="27" height="27" alt="Google Plus" class="CToWUd">
                                    </a>
                                </td>
                                <td width="6">
                                <td width="27">
                                    <a href="" target="_blank" data-saferedirecturl="" width="27" height="27"
                                       alt="Twitter" class="CToWUd">
                                        <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1500274609/unnamed_5_pjqgvk.gif"
                                             width="27" height="27" alt="Google Plus" class="CToWUd">
                                    </a>
                                </td>
                                <td width="6">

                                </td>
                                <td width="27">
                                    <a href="" target="_blank"
                                       data-saferedirecturl="">
                                        <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1500274188/unnamed_dntuvv.gif"
                                             width="27" height="27" alt="Google Plus" class="CToWUd">
                                    </a>
                                </td>
                                <td width="6"></td>
                                <td width="27">
                                    <a href="" target="_blank"
                                       data-saferedirecturl="">
                                        <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1500274189/unnamed_3_iwunsm.gif"
                                             width="27" height="27" alt="Linkedin" class="CToWUd">
                                    </a>
                                </td>
                                <td width="6">

                                </td>
                                <td width="27">
                                    <a href="" target="_blank"
                                       data-saferedirecturl="">
                                        <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1500274189/unnamed_2_c00ycp.gif"
                                             width="27" height="27" alt="Youtube" class="CToWUd">
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <table style="width: auto; max-width: 570px; margin: 0 auto; padding: 0; text-align: center;"
                               align="center" width="570" cellpadding="0"
                               cellspacing="0">
                            <tr>
                                <td style="font-family: Arial, &#039;Helvetica Neue&#039;, Helvetica, sans-serif; color: #AEAEAE; padding: 35px; text-align: center;">
                                    <p style="margin-top: 0; color: #74787E; font-size: 12px; line-height: 1.5em;">
                                        &copy; 2018
                                        <a style="color: #3869D4;" href="http://saleme.me"
                                           target="_blank">SaleMe.lk Free Classified Sri Lanka</a>.
                                        All rights reserved.
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>

