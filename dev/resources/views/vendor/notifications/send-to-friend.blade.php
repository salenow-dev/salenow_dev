<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml><![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->

    <link media="all" type="text/css" rel="stylesheet"
          href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link media="all" type="text/css" rel="stylesheet"
          href="https://saleme.lk/css/salenow/layout/bootstrap-3.3.7/css/bootstrap-theme.min.css">
    <link media="all" type="text/css" rel="stylesheet"
          href="https://saleme.lk/css/salenow/layout/bootstrap-3.3.7/css/bootstrap.min.css">
    <style>
        .coupon {
            border: 3px dashed #fea502;
            border-radius: 10px;
            font-family: "HelveticaNeue-Light", "Helvetica Neue Light",
            "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
            font-weight: 300;
        }

        .coupon #head {
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            min-height: 56px;
        }

        .coupon #footer {
            border-bottom-left-radius: 10px;
            border-bottom-right-radius: 10px;
        }

        #title .visible-xs {
            font-size: 12px;
        }

        .coupon #title img {
            font-size: 60px;
            height: 60px;
            margin-top: 5px;
        }

        .panel .panel-heading {
            background: #ffa620;
        }

        @media screen and (max-width: 500px) {
            .coupon #title img {
                height: 15px;
            }
        }

        .coupon #title span {
            float: right;
            margin-top: 18px;
            font-size: 18px;
            font-weight: 700;
        }

        .coupon-img {
            width: 100%;
            margin-bottom: 15px;
            padding: 0;
        }

        .items {
            margin: 15px 0;
        }

        .usd, .cents {
            font-size: 20px;
            margin: 0;
        }

        .number {
            font-size: 33px;
            font-weight: 700;
        }

        p {
            font-size: 13px;
            font-weight: 300;
            text-align: justify;
        }

        sup {
            top: 9px;
            font-size: 18px;
        }

        #business-info ul {
            margin: 0;
            padding: 0;
            list-style-type: none;
            text-align: center;
        }

        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
            padding: 8px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 0px solid #ddd;
            font-size: 12px;
        }

        #business-info ul li {
            display: inline;
            text-align: center;
        }

        #business-info ul li span {
            text-decoration: none;
            padding: .2em 1em;
        }

        #business-info ul li span i {
            padding-right: 5px;
        }

        .disclosure {
            padding-top: 15px;
            font-size: 11px;
            color: #bcbcbc;
            text-align: center;
        }

        .coupon-code {
            color: #333333;
            font-size: 11px;
        }

        .exp {
            color: #f34235;
        }

        .print {
            font-size: 14px;
            float: right;
        }

        /*------------------dont copy these lines----------------------*/
        body {
            font-family: "HelveticaNeue-Light", "Helvetica Neue Light",
            "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
            font-weight: 300;
        }

        #quicknav ul {
            margin: 0;
            padding: 0;
            list-style-type: none;
            text-align: center;
        }

        #quicknav ul li {
            display: inline;
        }

        #quicknav ul li a {
            text-decoration: none;
            padding: .2em 1em;
        }

        .btn-danger,
        .btn-success,
        .btn-info,
        .btn-warning,
        .btn-primary {
            width: 105px;
        }

        .btn-default {
            margin-bottom: 40px;
        }

        .loc {
            margin-top: -10px;
            margin-bottom: 17px;
            color: #fea502;
        }

        .hr-email {
            border-top: 2px solid #eee;
            margin-top: 5px;
            margin-bottom: 0px;
        }

        /*-------------------------------------------------------------*/
    </style>
</head>
<body class="clean-body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #E6E7E8">

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default coupon">
            <div class="panel-heading" id="head">
                <div class="panel-title" id="title">
                    <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1509515926/saleme-logo_uduoz9.png">

                    <span class="">Suggest Ad By : {{ucfirst($from)}}</span>
                </div>
            </div>
            <div class="panel-body">
                <img src="{{asset('')}}salenow/images/uploads/{{$adinfo->adid}}/{{$adinfo->featuredimage}}"
                     class="coupon-img img-rounded">
                <div class="row">
                    <div class="col-md-8">
                        <h3><b>{{$adinfo->adtitle}}</b></h3>
                        <div class="loc">Location : {{$adinfo->district}}, {{$adinfo->city_name}}</div>
                    </div>
                    <div class="col-md-4">
                        <div class="offer">
                            <p class="usd"><sup>Rs</sup></p>
                            <!--<span class="number">5100000</span>-->
                            <span class="number">{{number_format($adinfo->price)}}</span>

                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="table-container">
                        @if(!empty($adinfo->specification))
                            <b>Specifications</b>
                            <hr class="hr-email">
                            <table class="table-users table" border="0">
                                <tbody>
                                <![endif]-->
                                @foreach (array_chunk($adinfo->specification, 3,true) as $specs)
                                    <tr>
                                        @foreach($specs as $key => $item)
                                            <td> {{$key}} - {{$item}}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        @endif
                        @if(!empty($adinfo->features))
                            <b>Features</b>
                            <hr class="hr-email">
                            <table class="table-users table" border="0">
                                <tbody>
                                <![endif]-->
                                @foreach (array_chunk($adinfo->features, 3) as $features)
                                    <tr>
                                        @foreach($features as $item)
                                            <td>  {{$item}}&nbsp; <i class="ion-checkmark-circled"></i></td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                        <b>Description</b>
                        <hr class="hr-email">
                        <p class="justify" style="padding: 5px 0px 5px 0px;">
                            {!! $adinfo->description !!}
                        </p>
                    </div>
                </div>
                <center>
                    @if (isset($actionText))
                        <table style="width: 100%; margin: 30px auto; padding: 0; text-align: center;" align="center"
                               width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center">
                                    <a href="{{ $actionUrl }}"
                                       style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; display: block; display: inline-block; width: 200px; min-height: 20px; padding: 10px;
                 background-color: #3869D4; border-radius: 3px; color: #ffffff; font-size: 15px; line-height: 25px;
                 text-align: center; text-decoration: none; -webkit-text-size-adjust: none;background-color: #fea502;"
                                       class="button"
                                       target="_blank">
                                        {{ $actionText }}
                                    </a>
                                </td>
                            </tr>
                        </table>
                    @endif
                </center>

            </div>

        </div>
    </div>
</div>


</body>
</html>