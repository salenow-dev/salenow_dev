<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <style type="text/css" rel="stylesheet" media="all">
        /* Media Queries */
        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>
</head>

<body style="">
<table style="" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td style="" align="center">
            <table width="100%" cellpadding="0" cellspacing="0">
                <!-- Logo -->
                <tr>
                </tr>
                <tr>
                    <td>
                        <div style="float:left"><a target="_blank"  href="https://saleme.lk">
                                <img src="https://res.cloudinary.com/wfx/image/upload/v1499862015/saleme-logo_wxhphw.png"
                                     width="115" alt="" class="CToWUd">
                            </a></div>

                        <div style="float:right;text-align:center;margin-top:3px">
                            <a href=""
                               style="width:125px;height:30px;color:#000;border:1px solid #000;display:block;text-decoration:none;font-size:13px;border-radius:3px;line-height:30px"
                               target="_blank" data-saferedirecturl="">Post Free Ad</a>
                        </div>
                    </td>
                </tr>
                <!-- Email Body -->
                <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
                    <tbody>
                    <tr>
                        <td width="636">
                            @if($voucherPrice!='success')
                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                                       bgcolor="#ffffff"
                                       style="font-family:Arial,Helvetica,Sans-Serif;font-size:16px;line-height:24px;color:#2f3432">
                                    <tbody>
                                    <tr></tr>
                                    <tr>
                                        <td style="padding:35px 8% 12px 8%;font-family:Arial,Helvetica,Sans-Serif;font-size:18px;color:#2f3432"
                                            align="left" valign="middle"><strong>Dear Valued Customer,</strong></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0px 8% 0px 8%;font-family:Arial,Helvetica,Sans-Serif;font-size:14px;color:#5d6260;line-height:22px"
                                            align="left" valign="middle">
                                            Thank you for using Saleme.lk. This is to kindly inform you that We have received your request to "pin to top" you ad.and the under mentioned fee charged for the service.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:20px 8% 0px 8%;font-family:Arial,Helvetica,Sans-Serif;font-size:14px;color:#5d6260;line-height:22px"
                                            align="left" valign="top">
                                            Please be kind enough to follow the steps below to activate the service.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:50px 8% 20px 8%;font-family:Arial,Helvetica,Sans-Serif;font-size:20px;color:#2f3432;line-height:22px"
                                            align="left" valign="top">
                                            <strong>1. Please Note your payment details:</strong></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0px 8% 0px 8%;font-family:Arial,Helvetica,Sans-Serif;font-size:14px;color:#000000;line-height:20px">
                                            <table width="100%" border="0" align="left" valign="middle">
                                                <tbody>
                                                <tr>
                                                    <td style="background-color: #ffffff;padding-top: 20px;padding-bottom: 20px;border: 1px solid #ffc800;"
                                                        width="100%" height="60" align="center" valign="middle">
                                                        <table width="90%" border="0">
                                                            <tbody>
                                                            <tr>
                                                                <td width="379" height="30" align="left" valign="middle"
                                                                    style="font-family:Arial,Helvetica,Sans-Serif;font-size:14px;color:#2f3432;line-height:18px">
                                                                    {{($pinAd->ad_referance)?'Your ad Refference ID':'Boost Pack info'}}
                                                                </td>
                                                                <td width="180" align="right" valign="middle"
                                                                    style="font-family:Arial,Helvetica,Sans-Serif;font-size:16px;color:#2f3432;line-height:18px">
                                                                    <strong>
                                                                        <font color="#910000">
                                                                            @if($pinAd->ad_referance)
                                                                                {{$pinAd->ad_referance}}
                                                                            @else
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>Pack Name</td>
                                                                                        <td>{{$pinAd->boostpackname}}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Boost Ad Count</td>
                                                                                        <td>{{$pinAd->pin_to_top}}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Free</td>
                                                                                        <td>{{$pinAd->price}}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Validity</td>
                                                                                        <td>{{$pinAd->validitymonths}}
                                                                                            Months
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Description</td>
                                                                                        <td>{{$pinAd->description}}</td>
                                                                                    </tr>
                                                                                </table>
                                                                            @endif
                                                                        </font></strong>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="30" align="left" valign="middle"
                                                                    style="font-family:Arial,Helvetica,Sans-Serif;font-size:13px;color:#000000;line-height:18px">
                                                                    <span style="font-family:Arial,Helvetica,Sans-Serif;font-size:14px;color:#2f3432;line-height:18px">Listing Fee for Commercial Property:</span>
                                                                </td>
                                                                <td align="right" valign="middle"
                                                                    style="font-family:Arial,Helvetica,Sans-Serif;font-size:16px;color:#2f3432;line-height:18px">
                                                                    <strong>Rs. {{($voucherPrice=='success')? $voucherPrice: number_format($voucherPrice)}}</strong>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:50px 8% 20px 8%;font-family:Arial,Helvetica,Sans-Serif;font-size:20px;color:#2f3432;line-height:22px"
                                            align="left" valign="top">
                                            <strong>2. Choose payment option:</strong></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0px 8% 0px 8%;font-family:Arial,Helvetica,Sans-Serif;font-size:14px;color:#000000;line-height:20px">
                                            <table width="100%" border="0" align="left" valign="middle">
                                                <tbody>
                                                <tr>
                                                    <td style="padding:0px 0px 0px 0px;background-color:#ffffff"
                                                        width="90%"
                                                        height="60" align="center" valign="middle">
                                                        <table width="90%" border="0">
                                                            <tbody>
                                                            <tr>

                                                                <td height="30" colspan="4" align="left" valign="middle"
                                                                    style="font-family:Arial,Helvetica,Sans-Serif;font-size:13px;color:#000000;line-height:18px">
                                                                    <span style="font-family:Arial,Helvetica,Sans-Serif;font-size:18px;color:#2f3432;line-height:18px"><strong>Bank Deposit/Transfer</strong></span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="middle">&nbsp;</td>
                                                                <td colspan="5" align="left" valign="middle"
                                                                    style="font-family:Arial,Helvetica,Sans-Serif;font-size:14px;color:#000000;line-height:18px"><span
                                                                            style="font-family:Arial,Helvetica,Sans-Serif;font-size:14px;color:#5d6260"><font
                                                                                color="#2f3432"><strong>A.</strong></font> Choose any of the following:<br>
                    Act. name for all banks: </span><span style="font-family:Arial,Helvetica,Sans-Serif;font-size:14px"><font
                                                                                color="#2f3432"><strong>saleme.lk (Pvt) Ltd</strong></font></span><span
                                                                            style="font-family:Arial,Helvetica,Sans-Serif;font-size:14px;color:#5d6260"><font
                                                                                color="#2f3432"><strong>.</strong></font></span>
                                                                    <br> <font color="#910000"><b>Please mention your Ad
                                                                            Refference Number in the Bank Slip *</b>
                                                                    </font>
                                                                </td>

                                                            </tr>
                                                            <tr>
                                                                <td align="left" valign="middle"
                                                                    style="font-family:Arial,Helvetica,Sans-Serif;font-size:13px;color:#000000;line-height:18px">
                                                                    &nbsp;
                                                                </td>
                                                                <td width="15%" align="left" valign="middle"
                                                                    style="font-family:Arial,Helvetica,Sans-Serif;font-size:13px;color:#000000;line-height:18px">
                                                                    &nbsp;
                                                                </td>
                                                                <td width="38%" align="left" valign="middle"
                                                                    style="font-family:Arial,Helvetica,Sans-Serif;font-size:13px;color:#000000;line-height:18px">
                                                                    &nbsp;
                                                                </td>
                                                                <td width="40%" align="left" valign="middle"
                                                                    style="font-family:Arial,Helvetica,Sans-Serif;font-size:13px;color:#000000;line-height:18px">
                                                                    &nbsp;
                                                                </td>
                                                                <td width="2%" rowspan="8" align="left" valign="middle"
                                                                    style="font-family:Arial,Helvetica,Sans-Serif;font-size:13px;color:#000000;line-height:18px"></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="40" align="left" valign="middle"
                                                                    style="font-family:Arial,Helvetica,Sans-Serif;font-size:13px;color:#000000;line-height:18px">
                                                                    &nbsp;
                                                                </td>
                                                                <td align="center" valign="middle" bgcolor="#e7edee"
                                                                    style="font-family:Arial,Helvetica,Sans-Serif;border-bottom:#d7dfe2 solid 1px;font-size:14px;color:#fff;background: #ffa50d;line-height:18px">
                                                                    Bank
                                                                </td>
                                                                <td align="center" valign="middle" bgcolor="#e7edee"
                                                                    style="font-family:Arial,Helvetica,Sans-Serif;border-bottom:#d7dfe2 solid 1px;font-size:14px;color:#fff;background: #ffa50d;line-height:18px">
                                                                    Branch
                                                                </td>
                                                                <td align="center" valign="middle" bgcolor="#e7edee"
                                                                    style="font-family:Arial,Helvetica,Sans-Serif;border-bottom:#d7dfe2 solid 1px;font-size:14px;color:#fff;background: #ffa50d;line-height:18px">
                                                                    Acc. no.
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="50" align="left" valign="middle"
                                                                    style="font-family:Arial,Helvetica,Sans-Serif;font-size:13px;color:#000000;line-height:18px">
                                                                    &nbsp;
                                                                </td>
                                                                <td align="left" valign="middle"
                                                                    style="font-family:Arial,Helvetica,Sans-Serif;border-bottom:#d7dfe2 solid 1px;font-size:13px;color:#000000;line-height:18px">
                                                                <span style="line-height:0px"><img
                                                                            src="https://ci3.googleusercontent.com/proxy/TCYOR6EgdyCXBKeuKruh6b6APbmQD8YMyGvJ9Md1yfgoeOnIKHG9UhadjvCzYabGQvLXfLycOZaYxKGlkDu5HRHaL_mhsmwxUnQa3AbyLu1covmdKX1ulE6o1R82kIPB9AP8S8OuPA56Sq-qZlx9pA=s0-d-e1-ft#http://ikmanservices.com/ikman/emails/20170526-ikman-payments/img/logo-sampath_bank@2x.png"
                                                                            width="80" height="26" alt=""
                                                                            class="CToWUd"></span></td>
                                                                <td align="center" valign="middle"
                                                                    style="font-family:Arial,Helvetica,Sans-Serif;border-bottom:#d7dfe2 solid 1px;font-size:13px;color:#000000;line-height:16px"><span
                                                                            style="font-family:Arial,Helvetica,Sans-Serif;font-size:12px;color:#5d6260">Kandy Metro
Super</span></td>
                                                                <td align="center" valign="middle"
                                                                    style="font-family:Arial,Helvetica,Sans-Serif;border-bottom:#d7dfe2 solid 1px;font-size:13px;color:#000000;line-height:16px">
                                                                    <span style="font-family:Arial,Helvetica,Sans-Serif;font-size:12px;color:#5d6260">010010005103</span>
                                                                </td>
                                                            </tr>


                                                            <tr>
                                                                <td align="left" valign="middle"
                                                                    style="font-family:Arial,Helvetica,Sans-Serif;font-size:13px;color:#000000;line-height:18px">
                                                                    &nbsp;
                                                                </td>
                                                                <td colspan="4" align="left" valign="middle"
                                                                    style="font-family:Arial,Helvetica,Sans-Serif;font-size:13px;color:#000000;line-height:18px">
                                                                <span style="font-family:Arial,Helvetica,Sans-Serif;font-size:14px"><font
                                                                            color="#2f3432"><strong>B</strong></font></span><span
                                                                            style="font-family:Arial,Helvetica,Sans-Serif;font-size:14px;color:#5d6260"><font
                                                                                color="#2f3432"><strong>.</strong></font></span>
                                                                    <span style="font-family:Arial,Helvetica,Sans-Serif;font-size:14px"><font
                                                                                color="#2f3432"><strong>Aft</strong></font></span><span
                                                                            style="font-family:Arial,Helvetica,Sans-Serif;font-size:14px;color:#5d6260"><font
                                                                                color="#2f3432"><strong>er sucessfull payment: </strong></font>Please Send  Us the copy of payment slip or a screenshot / the screanshot or the downloaded invoice of the online bank transaction with the required details to one of the following:<br>
Email: <strong><a href="mailto:postad@saleme.lk" target="_blank">postad@saleme.lk</a></strong><br>
                                                                        <!--WhatsApp or Viber: </span><span style="font-family:Arial,Helvetica,Sans-Serif;font-size:14px"><font
                                                                                                                                                        color="#2f3432"><strong>0777 272 717</strong></font></span>-->
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:10px 8% 0px 8%;font-family:Arial,Helvetica,Sans-Serif;font-size:14px;color:#000000;line-height:20px"
                                            align="center" valign="middle">
                                            <table align="center" border="0" cellpadding="0" cellspacing="0"
                                                   width="100%"
                                                   style="margin:0 auto">
                                                <tbody>
                                                <tr>
                                                    <td align="center">


                                                        <table width="1" height="40" border="0" cellpadding="0"
                                                               cellspacing="0" align="left">
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:10px 8% 5px 8%;font-family:Arial,Helvetica,Sans-Serif;font-size:20px;color:#2f3432;line-height:22px"
                                            align="left" valign="top">
                                            <strong>3. Your ad will be activated:</strong></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0px 8% 0px 8%;font-family:Arial,Helvetica,Sans-Serif;font-size:14px;color:#5d6260;line-height:22px"
                                            align="left" valign="middle">
                                            <p>Once the payment is confirmed the service will be automatically activated for your ad within
                                                a short period of time.

                                            </p>

                                            <p><strong>For Inquiries:</strong><br>
                                                Please call us on <font color="#2f3432">0115 818188</font> (<span
                                                        class="aBn" data-term="goog_760284591" tabindex="0"><span
                                                            class="aQJ">9.00 am - 5.00pm</span></span>).</p></td>
                                    </tr>
                                    </tbody>
                                </table>
                            @else
                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                                       bgcolor="#ffffff"
                                       style="font-family:Arial,Helvetica,Sans-Serif;font-size:16px;line-height:24px;color:#2f3432">
                                    <tr></tr>
                                    <tr>
                                        <td style="padding:35px 8% 12px 8%;font-family:Arial,Helvetica,Sans-Serif;font-size:18px;color:#2f3432"
                                            align="left" valign="middle"><strong>Dear Valued Customer,</strong></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:35px 8% 12px 8%;font-family:Arial,Helvetica,Sans-Serif;font-size:18px;color:#2f3432"
                                            align="left" valign="middle">
                                            Your Ad {{$pinAd->adtitle}} is Pined to top on Saleme.lk
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Ad Referance - {{$pinAd->ad_referance}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0px 8% 0px 8%;font-family:Arial,Helvetica,Sans-Serif;font-size:14px;color:#5d6260;line-height:22px"
                                            align="left" valign="middle">
                                            Thank you for using Saleme.lk!
                                        </td>
                                    </tr>
                                </table>
                        @endif
                        <!-- Footer -->
                    <tr>
                        <td>
                            <table style="margin-top: 20px; margin-bottom: -20px;" cellpadding="0" cellspacing="0"
                                   align="center" border="0">
                                <tbody>
                                <tr>
                                    <td width="27">
                                        <a href="" target="_blank" data-saferedirecturl="" width="27" height="27"
                                           alt="Facebook" class="CToWUd">
                                            <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1500274188/unnamed_4_zq4trt.gif"
                                                 width="27" height="27" alt="Google Plus" class="CToWUd">
                                        </a>
                                    </td>
                                    <td width="6">
                                    <td width="27">
                                        <a href="" target="_blank" data-saferedirecturl="" width="27" height="27"
                                           alt="Twitter" class="CToWUd">
                                            <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1500274609/unnamed_5_pjqgvk.gif"
                                                 width="27" height="27" alt="Google Plus" class="CToWUd">
                                        </a>
                                    </td>
                                    <td width="6">

                                    </td>
                                    <td width="27">
                                        <a href="" target="_blank"
                                           data-saferedirecturl="">
                                            <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1500274188/unnamed_dntuvv.gif"
                                                 width="27" height="27" alt="Google Plus" class="CToWUd">
                                        </a>
                                    </td>
                                    <td width="6"></td>
                                    <td width="27">
                                        <a href="" target="_blank"
                                           data-saferedirecturl="">
                                            <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1500274189/unnamed_3_iwunsm.gif"
                                                 width="27" height="27" alt="Linkedin" class="CToWUd">
                                        </a>
                                    </td>
                                    <td width="6">

                                    </td>
                                    <td width="27">
                                        <a href="" target="_blank"
                                           data-saferedirecturl="">
                                            <img src="https://res.cloudinary.com/saleme-lk/image/upload/v1500274189/unnamed_2_c00ycp.gif"
                                                 width="27" height="27" alt="Youtube" class="CToWUd">
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <table style="" align="center" width="570" cellpadding="0"
                                   cellspacing="0">
                                <tr>
                                    <td style="">
                                        <p style="">
                                            &copy; {{ date('Y') }}
                                            <a style="" href="{{ url('/') }}"
                                               target="_blank">{{ config('app.name') }}</a>.
                                            All rights reserved.
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </td>
                </tr>
            </table>
</body>
</html>
