@if(session()->has('flash_message'))
    <div class="col-md-12 col-sm-12">
        <div class="alert alert-{{session('flash_message_level')}}">
            {{session('flash_message')}}
        </div>
    </div>
@endif
