@if($errors->any())
<div class=" col-md-12 alert alert-danger" id="error-div" role="alert">
    <i class="fa fa-exclamation-triangle"></i>&nbsp;
    <span class="error-title">Alert! The following errors were found.</span>
    <hr>
    <div id="error-message">
        @foreach($errors->all() as $error)
            <li>{{$error}}</li>
        @endforeach
    </div>
</div>
@endif