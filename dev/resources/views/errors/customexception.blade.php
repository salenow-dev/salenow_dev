<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name') }} @yield('title')</title>
    <!-- START third party CSS -->
    <link href="{{ asset("backend/assets/css/theme-default.css") }}" rel="stylesheet">
    <link href="{{ asset("css/salenow/layout/saleme-custom.css") }}" rel="stylesheet">
</head>
<body class="pin-bg">

<div class="mar-70 text-center">
    <h1 class="promo-head">
        Buy &amp; Sell on Saleme.lk
    </h1>
    <h1 class="promo-head2">
        From the <b>Sri Lanka’s Best<span class="no-1"></span></b> Marketplace
    </h1>

</div>


<div class="row">
    <div class="col-md-12">
        <div class="error-container">
            <div class="col-md-12 col-xs-12">
                @if(!empty($error))
                    <div class="promo-head2">
                        {{$error}}
                    </div>
                @endif
            </div>
            <div class="error-subtext">
                Oh Snap! We can't find what you are looking for...
            </div>
            <div class="error-actions">
                <div class="row">
                    <div class="col-md-6">

                        <button class="btn btn-info btn-block btn-lg" onClick="document.location.href = '/';">Back to
                            Home Page
                        </button>

                    </div>
                    <div class="col-md-6">
                        <button class="btn post-btn-bottom btn-block btn-lg" onClick="history.back();">Previous page
                        </button>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<div class="top-mar-100"></div>
<section>
    <div class="container">
        <div class=" hidden-xs"></div>
        <div class="row postadbannerbottom">
            <div class="col-sm-9 col-md-9 col-lg-9">
                <div class="promo_content">
                    <h3><span class="quick blink_me">Quick</span> Buy, Sell, Rent or Anything - Post your Ad on
                        saleme.lk</h3>
                </div>
            </div>
            <div class="col-sm-3 col-md-3 col-lg-3">
                <div class="pb_action">
                    @if(getMemberId())
                        <a class="btn post-btn-bottom  btn-lg " href="{{asset('')}}addtype"> Post Free Ad
                        </a>   @else
                        <a class="btn post-btn-bottom  btn-lg" data-toggle="modal" data-target=".login-register"
                           href="#">
                            Post Free Ad
                        </a>
                    @endif
                </div>
            </div>
        </div>
        <div class=" hidden-xs"></div>
    </div>
</section>
</body>
</html>