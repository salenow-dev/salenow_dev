<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '1867918153484824',
        'client_secret' => '59bee5c3ddead1c1478d05696950f138',
        'redirect' => 'http://saleme.me/sociallogin/facebook',
    ],
    'google' => [
        'client_id' => '751409729418-v313glk3nkqt8kjp4lm6j5c9733gr0f0.apps.googleusercontent.com',
        'client_secret' => 'iWJeXmHLwOQF4bkiCvGSj3xO',
        'redirect' => 'http://saleme.me/sociallogin/google',
    ],

];
