<?php
/**
 * Created by PhpStorm.
 * User: Niranjana
 * Date: 8/21/2017
 * Time: 12:04 PM
 */

namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class VisitcounterFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        //define key for ioc container in relevet Service Provider
        return 'visicount';
    }
}