<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AgentInvoiceMail extends Mailable
{
    use Queueable, SerializesModels;
    public $vehiclemonth;
    public $propertymonth;
    public $invoice_ref;
    public $earn;
    public $today;
    public $premeium;
    public $total;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($vehiclemonth,$propertymonth,$invoice_ref,$earn,$today,$premeium,$total)
    {
        //
        $this->total =$total;
        $this->vehiclemonth =$vehiclemonth;
        $this->propertymonth =$propertymonth;
        $this->invoice_ref =$invoice_ref;
        $this->earn =$earn;
        $this->today =$today;
        $this->premeium =$premeium;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('emails.agentinvoiceemail')
        ->subject('Agent Monthly Payment Invoice - SaleMe.lk')
        ->from('noreply@saleme.lk');
    }
}
