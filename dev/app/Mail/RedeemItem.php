<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RedeemItem extends Mailable
{
    use Queueable, SerializesModels;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.redeemItem')
            ->subject('Order success for '.$this->data['itemTitle'])
            ->from($this->data['email'])
            ->with([
                'email'=> $this->data['email'],
                'name'=> $this->data['name'],
                'address'=> $this->data['address'],
                'mobile'=>$this->data['mobile'],
                'itemTitle'=>$this->data['itemTitle'],
                'itemDescription'=>$this->data['itemDescription'],
                'itemImage'=>$this->data['itemImage'],
                'itemMinPints'=>$this->data['itemMinPints'],
                'prize'=>$this->data['prize']
            ]);
    }
}
