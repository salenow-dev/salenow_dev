<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApplyNow extends Mailable
{
    use Queueable, SerializesModels;
    public $file;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $file)
    {
        //
        $this->file = $file;
        $this->data = $data;
    }
    /**
     * Build the message.de
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.applynow')
            ->subject($this->data['apply_for'])
            ->from($this->data['email'])
            ->with([
                'applicentName' => $this->data['name'],
                'applicentEmail' => $this->data['email'],
                'applicentCoverNote' => $this->data['message'],
                'position' => $this->data['apply_for'],
                'reciver' => $this->data['reciveemail'],
            ])->attach($this->file->getRealPath(), [
                'as' => 'CV-'.str_replace(array(' ','/','  ','.',',','@','#'),'-',$this->data['name']).'.'.$this->file->getClientOriginalExtension(),
                'mime' => $this->file->getMimeType(),
            ]);
    }
}
