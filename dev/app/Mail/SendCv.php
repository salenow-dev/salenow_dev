<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendCv extends Mailable
{
    use Queueable, SerializesModels;

    public $file;
    public $data;


    public function __construct($data ,$file)
    {
        $this->file = $file;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.cv')
        ->subject($this->data['position'])
        ->from($this->data['first_name']);

    }

}
