<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand_subcategory extends Model
{
    protected $fillable = ['brand_id','subcategory_id'];
}
