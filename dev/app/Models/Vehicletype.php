<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Vehicletype extends Model
{
    protected $table = 'vehicle_types';
}
