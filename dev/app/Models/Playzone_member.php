<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Playzone_member extends Model
{
    protected $fillable=['member_id','email','address','mobile','gender','tshirt_size','post_code'];

    public function member()
    {
        return $this->belongsTo(Member::class);
    }
}
