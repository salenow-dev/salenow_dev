<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class alladsmaster_spamreason extends Model
{
    protected $fillable = ['alladsmaster_id','spamreasons_id'];
}
