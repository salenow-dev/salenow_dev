<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Propertyoption extends Model
{
    protected $table = 'propertyoptions';


    //  laod model for chain select
    public static function OptionsGetById($optionid)
    {
        $activelist = \DB::table('propertyoptions as prop')
            ->where('prop.status', '=', "ACTIVE")
            ->where('prop.id', '=', "$optionid")
            ->select('prop.display_name', 'prop.option_name', 'prop.id')
            ->first();
        return !empty($activelist) ? $activelist : '';
    }
}
