<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Adimage extends Model
{
    protected $fillable = ['alladsmaster_id', 'imagename'];

    public function allads()
    {
        return $this->belongsTo(District::class);
    }


    public function createRecord($request)
    {
        $obj = new Adimage($request);
        return $obj->save();
    }

    public function updateRecord($request, $adimage)
    {
        $adimage->updated_by = getUserId();
        return $adimage->update($request);
    }

    public function insertFeaturedImg($id, $img)
    {
        $obj = new Adimage();
        $obj->alladsmaster_id = $id;
        $obj->imagename = $img;
        return $obj->save();
    }

    //   get images by alladsmasters id
    public static function GetImage($alladsmaster_id)
    {
        $alldataegt = DB::table('adimages as img')
            ->select('id','alladsmaster_id','imagename')
            ->where('img.alladsmaster_id', '=', $alladsmaster_id)
            ->get()->toArray();

        return $alldataegt;
    }
}
