<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Alladsmaster_member extends Model
{
    protected $table='alladsmaster_member';

    public static function getfavoriteAdsForMember($member_id){
        $alldataegt = DB::table('alladsmaster_member as amm')
//            ->leftJoin('alladsmasters as all', 'amm.alladsmaster_id', '=', 'all.id')
            ->leftJoin('alladsmasters as all',function ($join){
                $join->on('all.id','=','amm.alladsmaster_id');
                $join->where('all.status','=','confirm');
            })
            ->leftJoin('allvehicleads as vm', 'all.id', '=', 'vm.alladsmaster_id')
            ->leftJoin('categories as cat', 'all.category_id', '=', 'cat.id')
            ->leftJoin('sub_categories as scat', 'all.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_districts as dist', 'all.district_id', '=', 'dist.id')
            ->leftJoin('location_cities as cit', 'all.city_id', '=', 'cit.id')
            ->leftJoin('brands as vb', 'vm.brand_id', '=', 'vb.id')
            ->where('amm.member_id', '=', $member_id)
            ->select('amm.id', 'all.adtitle','all.id as adid','all.featuredimage', 'all.ad_referance', 'all.created_at', 'all.updated_at', 'all.published_at', 'all.ad_boost_status', 'vb.brand_name as brand',
            'vm.model as model', 'all.price', 'vm.mileage', 'cat.category_name as category', 'cat.category_code', 'cit.city_name as city', 'dist.district_name',
            'all.category_id', 'scat.sub_category_code', 'scat.sub_category_name as subcategory', 'all.ad_type', 'all.slug', 'all.status' )
            ->paginate(8);


//            ->leftJoin('categories as cat', 'all.category_id', '=', 'cat.id')
//            ->leftJoin('sub_categories as scat', 'all.sub_category_id', '=', 'scat.id')
//            ->leftJoin('location_districts as dist', 'mm.district_id', '=', 'dist.id')
//            ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
//            ->leftJoin('brands as vb', 'vm.brand_id', '=', 'vb.id')

//
//        $alldataegt->select('mm.id as adid', 'mm.adtitle', 'mm.description', 'mm.featuredimage', 'mm.ad_referance', 'mm.created_at', 'mm.updated_at', 'mm.published_at', 'mm.ad_boost_status', 'vb.brand_name as brand',
//            'vm.model as model', 'mm.price', 'vm.mileage', 'cat.category_name as category', 'cat.category_code', 'cit.city_name as city', 'dist.district_name',
//            'mm.category_id', 'scat.sub_category_code', 'scat.sub_category_name as subcategory', 'mm.ad_type', 'mm.slug', 'mm.status', 'mvc.status as vouchercode_status','mm.views','mm.clicks'
//        );
//        $result = $alldataegt->orderBy('mm.id', 'desc')
//            ->paginate(8);
        return $alldataegt;
    }

}
