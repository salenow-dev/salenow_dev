<?php

namespace App\Models;

use App\Notifications\MemberResetPasswordNotification;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class Member extends User
{
    use Notifiable;

    protected $fillable = [
        'first_name', 'last_name', 'telephone', 'mobile', 'sex', 'district_id', 'city_id', 'address', 'member_type','parent_id', 'status', 'email', 'password', 'avatar'
    ];

    protected $hidden = ['password', 'remember_token'];

    protected $table = 'members';

    public function membercontacts()
    {
        return $this->hasMany(Membercontact::class);
    }

    public function premiummember()
    {
        return $this->hasOne(Memberpremium::class,'member_id','parent_id');
    }

    public function playzone_member()
    {
        return $this->hasOne(Playzone_member::class,'member_id');
    }

    public function allads()
    {
        return $this->belongsToMany(Alladsmaster::class);
//        return $this->hasMany(Alladsmaster::class);
    }

    public function membersocial()
    {
        return $this->hasMany(Membersocial::class);
    }

    public function otpnumbers()
    {
        return $this->hasMany(Otp::class);
    }

    public function vouchers()
    {
        return $this->hasMany(Membervoucher::class);
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function employee()
    {
        return $this->hasOne(Memberemployee::class);
    }

    //Send password reset notification
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MemberResetPasswordNotification($token));
    }

    public function addMember($request)
    {
        $obj = new Member();
        $obj->first_name = $request['signup_username'];
        $obj->email = $request['signup_email'];
        $obj->status = 'inactive';
        $obj->password = bcrypt($request['signup_pass']);
        if ($obj->save()) {
            return $obj->id;
        }
    }

    public function addMemberFromfb($email, $fname, $lname, $gender)
    {
        $id = DB::table('members')->insertGetId(
            array(
                'email' => $email,
                'first_name' => $fname,
                'last_name' => $lname,
                'sex' => $gender,
            ));
        return $id;
    }

    // check member username and pwd
    public static function Memberlogin($email, $password)
    {
        $memberdetails = DB::table('members as mem')
//            ->where('mem.status', '=', "active")
            ->where('mem.password', '=', bcrypt($password))
            ->where('mem.email', '=', "$email")
            ->first();
        return !empty($memberdetails) ? $memberdetails : '';
    }


    // check member  by  email for  social auth
    public static function Checkemailaddress($email)
    {
        $memberdetails = DB::table('members as mem')
//            ->where('mem.status', '=', "active")
            ->where('mem.email', '=', "$email")
            ->first();
        return !empty($memberdetails) ? $memberdetails : '';
    }

    // memeber  details get by id
    public static function MemberDetailsByIdsocial($id, $provider)
    {
        $memberdetails = DB::table('members as mem')
            ->select('mem.first_name', 'mem.last_name', 'mem.email', 'mem.sex', 'mem.address', 'mem.id as memberid',
                'mem.district_id as district', 'mem.city_id as city', 'dis.district_name', 'city.city_name as cityname', 'mem.avatar','mem.status');
//        $memberdetails->where('mem.status', '=', "active");
        if (!empty($provider)) {
            $memberdetails->leftJoin('member_socials as memso', 'mem.id', '=', 'memso.member_id');
            $memberdetails->where('memso.provider', '=', $provider);
            $memberdetails->select('memso.image_link', 'mem.first_name', 'mem.last_name', 'mem.email', 'mem.sex',
                'mem.address', 'mem.id as memberid', 'mem.district_id as district', 'mem.city_id as city', 'dis.district_name', 'city.city_name');

        }
        $memberdetails->leftJoin('location_districts as dis', 'mem.district_id', '=', 'dis.id');
        $memberdetails->leftJoin('location_cities as city', 'mem.city_id', '=', 'city.id');
//        $memberdetails->leftJoin('location_cities as city', 'mem.city_id', '=', 'city.id');
        $result = $memberdetails->where('mem.id', '=', "$id")
            ->first();

        return !empty($result) ? $result : '';
    }

    // memeber  details get by id
    public static function MemberDetailsById($id)
    {
        $memberdetails = DB::table('members as mem')
            ->leftJoin('memberpremiums as mp', 'mem.id', '=', 'mp.member_id')
            ->select('mem.id', 'mem.first_name', 'mem.last_name', 'mem.telephone', 'mem.mobile', 'mem.email', 'mem.avatar', 'mem.sex', 'mem.district_id', 'mem.city_id',
                'mem.address', 'mem.member_type', 'mem.status', 'mp.slug', 'mp.company_name')
//            ->where('mem.status', '=', "active")
            ->where('mem.id', '=', "$id")
            ->first();
        return !empty($memberdetails) ? $memberdetails : '';
    }

    public static function MemberDetailsByIdforAdDetails($id)
    {
        $memberdetails = DB::table('members as mem')
            ->leftJoin('memberpremiums as mp', 'mem.id', '=', 'mp.member_id')
            ->select('mem.id', 'mem.first_name', 'mem.last_name', 'mem.member_type', 'mp.slug', 'mp.company_name','mp.profile_image')
            ->where('mem.id', '=', "$id")->first();
        return !empty($memberdetails) ? $memberdetails : '';
    }
    public static function GetMemberDetailsByIdforJobApply($id)
    {
        $memberdetails = DB::table('members as mem')
            ->leftJoin('memberpremiums as mp', 'mem.id', '=', 'mp.member_id')
            ->select('mem.id', 'mem.first_name', 'mem.last_name', 'mem.member_type', 'mp.slug', 'mp.company_name',
                'mp.profile_image','mem.email as privetemail','mp.email as premiumemail')
            ->where('mem.id', '=', "$id")->first();
        return !empty($memberdetails) ? $memberdetails : '';
    }


    // memeber  details get by id
    public static function MemberNameById($id)
    {
        $memberdetails = DB::table('members as mem')
            ->select('mem.first_name', 'mem.last_name')
//            ->where('mem.status', '=', "active")
            ->where('mem.id', '=', "$id")
            ->first();
        return !empty($memberdetails) ? $memberdetails : '';
    }
}
