<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tempimage extends Model
{
    protected $fillable = ['member_id','imagename','featuredimage'];

    public function saveNewTempImages($request,$checkForPastImages,$setFeatured)
    {
        $checked = '';
        $obj = new Tempimage($request);
        if($setFeatured){
            if (count($checkForPastImages) == 0){
                $obj->featuredimage = true;
                $checked = 'checked';
            }
        }
        $obj->save();
        return ['id'=>$obj->id,'checked'=>$checked];
    }
}
