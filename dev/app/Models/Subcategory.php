<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $table = 'sub_categories';
    protected $fillable = ['category_id', 'sub_category_name', 'sub_category_code', 'icon', 'widget_id', 'adtype', 'status'];
    protected $with = ['subcategoryfilters'];

    public function allads()
    {
        return $this->hasMany(Alladsmaster::class,'sub_category_id');
    }
    public function subcategoryfilters()
    {
        return $this->belongsToMany(Categoryfilter::class);
    }

    public function brands()
    {
        return $this->belongsToMany(Brand::class)->orderBy('brand_name');;
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function createRecord($request)
    {
//        dd($request);
        $obj = new Subcategory($request);
        $obj->updated_by = getUserId();
        $obj->created_by = getUserId();
        if($obj->save()){
            $obj->subcategoryfilters()->attach($request['categoryfilter_id']);
            return true;
        }
    }

    public function updateRecord($request, $subcategory)
    {
        //delete the many to many relation - detach()
        $pivot = $subcategory->subcategoryfilters()->get();
        if(!empty($pivot)){
            $subcategory->subcategoryfilters()->detach();
        }
        
        $subcategory->updated_by = getUserId();
        if($subcategory->update($request)){
            if(!empty($request['categoryfilter_id'])){
                $subcategory->subcategoryfilters()->attach($request['categoryfilter_id']);
            }
            return true;
        }
    }
}
