<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Premiumgaleryimage extends Model
{
    protected $fillable =['memberpremium_id','image'];
}
