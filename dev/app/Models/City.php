<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class City extends Model
{
    protected $table = 'location_cities';

    /*************************************************************************************************
     * Relations
     *************************************************************************************************/
    public function alladscity()
    {
        return $this->hasMany(Alladsmaster::class);
    }
    
    public function District()
    {
        return $this->belongsTo(District::class);
    }
    /************************************************************************************************
     * END Relations
     * Query Scopes
     ************************************************************************************************/

    public function scopeSearchcity($query,$search)
    {
        return $query->where('slug', 'like', "%" . $search . "%");
    }
    /************************************************************************************************
     * END Query Scopes
     ************************************************************************************************/


    public static function CityByDistrictId($DistrictId)
    {
        $activelist = \DB::table('location_cities as city')
            ->where('city.status', '=', "active")
            ->where('city.district_id', '=', "$DistrictId")
            ->select('city.city_name', 'city.id')
            ->get();
        return !empty($activelist) ? $activelist : '';
    }
    
    
     //  laod  citiyes group by District
    public static function CityGetBYDistrictID($DistrictId)
    {
        $citylist = DB::table('location_cities as city')    
                ->where('city.status', '=', "active")
            ->where('city.district_id', '=', "$DistrictId")
            ->select('city.city_name','city.distric_id','city.slug')
            ->get()->toArray();
        return !empty($citylist) ? $citylist : '';
    }
}
