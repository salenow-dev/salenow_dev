<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Premiumservice extends Model
{
    protected $fillable = ['memberpremium_id','servicetitle','service','featured','image'];

    public function premiummember()
    {
        $this->hasOne(Memberpremium::class);
    }
}
