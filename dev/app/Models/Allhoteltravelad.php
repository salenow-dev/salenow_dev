<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Allhoteltravelad extends Model
{
    protected $fillable=['alladsmaster_id','numberofguest','bedsize','beds','bathrooms','bathroomtypes','numberofrooms','features','address','min_check_in_time','max_check_out_time','type','occupancy','price_meal','size'];
    public $timestamps = false;

    public function saveData($request, $member_master_id)
    {
        if (isset($request['numberofguest']) OR isset($request['bedsize']) OR isset($request['beds'])
            OR isset($request['bathrooms']) OR isset($request['features']) OR isset($request['type'])
            OR isset($request['occupancy'])OR isset($request['size']) ) {
            $obj = new Allhoteltravelad($request);
            $obj->alladsmaster_id = $member_master_id;

            if (!empty($request['price_meal'])) {
                $obj->price_meal = serialize($request['price_meal']);
            }
            if (!empty($request['option'])) {
                $obj->features = serialize($request['option']);
            }
            if ($obj->save()) {
                return $obj->id;
            }
        }
    }

    public function updateData($request, $member_master_id)
    {
        if (isset($request['numberofguest']) OR isset($request['bedsize']) OR isset($request['beds'])
            OR isset($request['bathrooms']) OR isset($request['features']) OR isset($request['type'])
            OR isset($request['occupancy'])OR isset($request['size']) ) {
            $obj = Allhoteltravelad::where('alladsmaster_id',$member_master_id)->first();
            $obj->alladsmaster_id = $member_master_id;

            if (!empty($request['price_meal'])) {
                $obj->price_meal = serialize($request['price_meal']);
            }
            if (!empty($request['option'])) {
                $obj->features = serialize($request['option']);
            }
            if ($obj->update($request)) {
                return $obj->id;
            }
        }
    }
}
