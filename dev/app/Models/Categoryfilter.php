<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categoryfilter extends Model
{
    protected $fillable = ['filtername','filterref','sortby','status'];

    public function categories()
    {
        return $this->belongsToMany(Subcategory::class);
    }

    public function categoryfiltervalues()
    {
        return $this->hasMany(Categoryfiltervalue::class)->orderBy('id');
    }

    public function createRecord($request)
    {
//        dd($request);
        $obj = new Categoryfilter($request);
        $obj->updated_by = getUserId();
        $obj->created_by = getUserId();
        if($obj->save()){
            foreach ($request['filtervalue'] as $item) {
                $obj->categoryfiltervalues()->save(new Categoryfiltervalue(['filtervalue'=>$item]));
            }
            return true;
        }
    }

    public function updateRecord($request,$filter)
    {
        //delete filter values associating with main filter
        $deletedRows = Categoryfiltervalue::where('categoryfilter_id',$filter->id)->delete();
        //update record
        $filter->updated_by = getUserId();
        if($filter->update($request)){
            foreach ($request['filtervalue'] as $item) {
                $filter->categoryfiltervalues()->save(new Categoryfiltervalue(['filtervalue'=>$item]));
            }
        }
        return true;
    }
}
