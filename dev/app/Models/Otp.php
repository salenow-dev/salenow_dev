<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
    protected $fillable = ['member_id', 'number', 'message', 'code', 'status', 'count'];

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function savedata($request)
    {
        $obj = new Otp($request);
        $obj->member_id = getMemberId();
        $obj->save();
        return true;
    }

    public function updatedata($request, $id)
    {
        $obj = Otp::find($id);
        $obj->member_id = getMemberId();
        $obj->count = $obj->count + 1;
        $obj->update($request);
        return true;
    }


}
