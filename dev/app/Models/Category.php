<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    protected $fillable = ['sequence', 'category_name', 'category_code', 'slug', 'icon', 'status'];

    public function allAds()
    {
        return $this->hasMany(Alladsmaster::class);
    }

    public function subcategories()
    {
        return $this->hasMany(Subcategory::class);
    }

    /************************************************************************************************
     *  START All District Ad Count
     * */
    public function scopeByLocation($query, $locationId)
    {
        return $query->whereCurrent('district_id', $locationId);
    }


//    public function alladsCount()
//    {
//        return $this->hasOne(Alladsmaster::class)
//            ->where('status','confirm')
//            ->selectRaw('category_id, count(*) as adsCount')
//            ->groupBy('category_id');
//    }
//
//    public function getAlladsCountAttribute()
//    {
//        // if relation is not loaded already, let's do it first
//        if (!array_key_exists('alladsCount', $this->relations))
//            $this->load('alladsCount');
//        $related = $this->getRelation('alladsCount');
//        // then return the count directly
//        return ($related) ? (int)$related->adsCount : 0;
//    }
    //************************************************************************************************

    public function createRecord($request)
    {
        $obj = new Category($request);
        $obj->updated_by = getUserId();
        $obj->created_by = getUserId();
        return $obj->save();
    }

    public function updateRecord($request, $category)
    {
        $category->updated_by = getUserId();
        $category->slug = str_slug($category['category_name']);
        return $category->update($request);
    }

    public function updateOrderSequence()
    {

    }

    public static function Catrgorylsit()
    {
        $Catrgorylsit = DB::table('categories')
            ->select('id', 'category_name', 'category_code', 'icon', 'sequence', 'slug')
            ->orderBy('sequence', 'asc')
            ->get()->toArray();
        return !empty($Catrgorylsit) ? $Catrgorylsit : '';
    }

    public static function CatrgorylsitNames($adId)
    {
        $Catrgorylsit = DB::table('categories')
            ->select('id', 'category_name', 'category_code', 'icon', 'sequence')
            ->orderBy('sequence', 'asc')
            ->where('id', '=', $adId)
            ->get()->first();
        return !empty($Catrgorylsit) ? $Catrgorylsit : '';
    }

    public static function subcatlist()
    {
        $subcatlist = DB::table('categories as cat')
            ->leftJoin('sub_categories as scat', 'cat.id', '=', 'scat.category_id')
            ->select('cat.id as catid', 'cat.category_name as catname', 'cat.category_code', 'scat.adtype',
                'scat.sub_category_name', 'scat.sub_category_code', 'scat.id as subcatid')
            ->get()->toArray();
        return $subcatlist;
    }


}
