<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alladsfilter extends Model
{
    protected $fillable = ['alladsmaster_id','filtername','categoryfiltervalue_id'];
    public $timestamps = false;

    public function allad()
    {
        return $this->belongsTo(Alladsmaster::class);
    }
}
