<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Vehicleoption extends Model
{
    protected $table = 'vehicle_options';


    //  laod model for chain select
    public static function OptionsGetById($optionid)
    {
        $activelist = \DB::table('vehicle_options as vop')
            ->where('vop.status', '=', "active")
            ->where('vop.id', '=', "$optionid")
            ->select('vop.display_name', 'vop.option_name', 'vop.id')
            ->first();
        return !empty($activelist) ? $activelist : '';
    }
}
