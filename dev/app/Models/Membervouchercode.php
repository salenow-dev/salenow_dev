<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Membervouchercode extends Model
{
    public $timestamps = false;
    protected $fillable = ['membervoucher_id','ad_referance','boosttype','vouchercode'];

    public function voucher()
    {
        return $this->belongsTo(Membervoucher::class,'membervoucher_id');
    }
}
