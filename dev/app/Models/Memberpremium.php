<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Memberpremium extends Model
{
    use Notifiable;
    protected $table = 'memberpremiums';
    protected $fillable = ['id','member_id', 'webtitle', 'company_name', 'slug', 'address', 'category_id',
        'website', 'email', 'telephone', 'mobile', 'description', 'verified','teampublic','lat','lng','facebook_page',
        'period','pintotopactivated'];

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getTelephoneAttribute($value)
    {
        return unserialize($value);
    }

    public function getMobileAttribute($value)
    {
        return unserialize($value);
    }
    public static function getAllActivePremiumMembers(){
        $premiummembers = DB::table('memberpremiums as mp')
            ->leftJoin('categories as cat', 'mp.category_id', '=', 'cat.id')
            ->select('mp.member_id','mp.company_name','mp.slug','mp.email','mp.telephone','mp.mobile','mp.address',
                'mp.website','mp.profile_image','cat.category_name as category')
            ->where('mp.verified', '=', 1)
            ->where('mp.list_show', '=', 1)
            ->orderBy('mp.created_at','asc')
//            ->first();
            ->paginate(10);
        return $premiummembers;
    }
}
