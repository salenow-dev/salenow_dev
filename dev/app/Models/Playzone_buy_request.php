<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Playzone_buy_request extends Model
{
    protected $fillable = ['playzone_member_id', 'playzone_store_id', 'is_prize'];

    public static function allPrizesList($status)
    {
//        dd($status);
        $prizes = DB::table('playzone_buy_requests as breq')
        ->leftJoin('playzone_members as plmem', 'breq.playzone_member_id','=','plmem.id')
        ->leftJoin('members as mem', 'plmem.member_id','=','mem.id')
        ->leftJoin('playzone_stores as plsto',function ($join){
            $join->on('breq.playzone_store_id','=','plsto.id');
            $join->where('plsto.status','=','active');
        });
        if ($status !='all'){
            $prizes->where('breq.status',$status);
        }
        $prizes->orderBy('breq.created_at','DESC');
        $prizes->select('breq.id','breq.is_prize as is_prize','breq.status as buy_status','breq.created_at as created_at','breq.shiped_date as shiped_date',
            'mem.first_name as first_name', 'mem.last_name as last_name', 'plmem.email as email','plmem.address','plmem.post_code','plmem.mobile',
            'plsto.title','plsto.description','plsto.image','plsto.qty','plsto.is_prize');
        return $prizes->get();
    }

    public static function purchesHistory($memberid)
    {
//        dd($status);
        $prizes = DB::table('playzone_buy_requests as breq')
            ->leftJoin('playzone_members as plmem', 'breq.playzone_member_id','=','plmem.id')
            ->leftJoin('playzone_stores as plsto',function ($join){
                $join->on('breq.playzone_store_id','=','plsto.id');
            })
            ->where('breq.member_id',$memberid)
            ->orderBy('breq.created_at','DESC');
        $prizes->select('breq.id','breq.is_prize as is_prize','breq.status as buy_status','breq.created_at as created_at','breq.shiped_date as shiped_date','breq.status as status',
            'plmem.email as email','plmem.address','plmem.post_code','plmem.mobile',
            'plsto.title','plsto.description','plsto.image','plsto.qty','plsto.is_prize','plsto.min_point','plsto.price');
        return $prizes->get();
    }
    public static function prizesList($status = null)
    {
     dd($status);
    }
}
