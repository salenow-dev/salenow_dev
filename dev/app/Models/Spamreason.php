<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Spamreason extends Model
{
    public function allads()
    {
        return $this->belongsToMany(Alladsmaster::class);
    }
}
