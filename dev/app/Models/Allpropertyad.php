<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Allpropertyad extends Model
{
    protected $fillable = ['landsize', 'propertysize', 'beds', 'baths', 'address', 'features'];
    public $timestamps = false;


    public function saveData($request, $member_master_id)
    {
        if (isset($request['landsize']) OR isset($request['propertysize']) OR isset($request['beds']) OR isset($request['baths']) OR isset($request['address']) OR isset($request['features'])) {
            $obj = new Allpropertyad($request);
            $obj->alladsmaster_id = $member_master_id;
            if (!empty($request['landsize'])) {
                $obj->landsize = $request['landsize'].' '.$request['landunit'];
            }
            if (!empty($request['propertysize'])) {
//                $obj->propertysize = $request['propertysize'].' '.$request['propertyunit'];
                $obj->propertysize = $request['propertysize'].' sqft';
            }
            if (!empty($request['option'])) {
                $obj->features = serialize($request['option']);
            }
            if ($obj->save()) {
                return $obj->id;
            }
        }
    }

    public function updateData($request, $member_master_id)
    {
        if (isset($request['landsize']) OR isset($request['propertysize']) OR isset($request['beds']) OR isset($request['baths']) OR isset($request['address']) OR isset($request['features'])) {
            $obj = Allpropertyad::where('alladsmaster_id',$member_master_id)->first();
            $obj->alladsmaster_id = $member_master_id;
            if (!empty($request['landsize'])) {
                $obj->landsize = $request['landsize'].' '.$request['landunit'];
            }
            if (!empty($request['propertysize'])) {
//                $obj->propertysize = $request['propertysize'].' '.$request['propertyunit'];
                $obj->propertysize = $request['propertysize'].' sqft';
            }
            if (!empty($request['option'])) {
                $obj->features = serialize($request['option']);
            }
            if ($obj->update($request)) {
                return $obj->id;
            }
        }
    }
}
