<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Playzone_mission extends Model
{
   protected $fillable=['final_score'];

   public static function getPlayzoneMember()
   {
       $member = DB::table('playzone_missions as plmis')
           ->leftJoin('members as mbr' , 'plmis.member_id' , '=' , 'mbr.id')
           ->select('mbr.first_name', 'mbr.email',
               'plmis.daily_coin_last_update_at','plmis.daily_question_last_update_at','plmis.extra_question_last_update_at',
               'plmis.facebook_page_like','plmis.facebook_page_share','plmis.facebook_page_first_post_share','plmis.id',
               'plmis.facebook_page_second_post_share','plmis.google_plus_follow','plmis.google_plus_share',
               'plmis.youtube_subscribe','plmis.twitter_follow','plmis.instagram_follow','plmis.linkedin_follow',
               'plmis.game_01_last_played_at','plmis.game_01_palyed_times','plmis.game_02_last_played_at','plmis.game_02_palyed_times',
               'plmis.current_score','plmis.final_score','plmis.created_at')
           ->get();
       return $member;
   }

}
