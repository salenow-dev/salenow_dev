<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Paymentdetail extends Model
{
    protected $fillable =['order_id','items','currency','first_name','last_name','email','address','city','country','created_at','updated_at','md5sig','status'];
}
