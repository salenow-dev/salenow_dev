<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Adrepotrdetail extends Model
{
    protected $fillable = ['alladsmaster_id', 'pintotopview'];

    public function saveAdInfo($request)
    {
        $exist = Adrepotrdetail::where('alladsmaster_id',$request->id)->first();
        if(!$exist){
            $adDetails = new Adrepotrdetail();
            $adDetails->alladsmaster_id = $request->id;
            $adDetails->pintotopview = $request->boostads->views;
            $adDetails->save();
        } else{
            //update other info like boost bump up data
            $exist->pintotopview = $request->boostads->views;
            $exist->save();
        }
    }
}
