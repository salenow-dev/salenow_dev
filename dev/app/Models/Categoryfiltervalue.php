<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Categoryfiltervalue extends Model
{
    protected $fillable = ['filtervalue'];

    public function categoryfilter()
    {
        return $this->belongsTo(Categoryfilter::class);
    }

    public function propertyad()
    {
        return $this->belongsTo(Allpropertyad::class);
    }

    public static function getFilterValue($id)
    {
        $info = DB::table('categoryfiltervalues')->where('id', '=', $id)->select('filtervalue')->first();
        return (!empty($info) ? $info : '');
    }
}
