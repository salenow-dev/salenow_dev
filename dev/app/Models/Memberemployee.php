<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Memberemployee extends Model
{
    protected $fillable = ['member_id','memberpremium_id','membergroup_id','email','designation','nic','dob','joindate','status','teampublic'];

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function employeegroup()
    {
        return $this->belongsTo(Membergroup::class,'membergroup_id');
    }

    public function saveEmployeeInfo($request)
    {
        $employee = new Memberemployee($request);
        $employee->save();
        return $employee->id;
    }
}
