<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Common extends Model
{
    public static function DeleteByid($id,$field,$table)
    {
      DB::table($table)->where($field, '=', $id)->delete();  
    }
    
     public static function object_to_array($data) {
        if (is_array($data) || is_object($data)) {
            $result = array();
            foreach ($data as $key => $value) {
                $result[$key] = self::object_to_array($value);
            }
            return $result;
        }
        return $data;
    }
    
    
 
}
