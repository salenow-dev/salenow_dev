<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categoryfiltersubcategory extends Model
{
    protected $table = ['categoryfilter_subcategory'];
    protected $fillable = ['categoryfilter_id','subcategory_id'];
}
