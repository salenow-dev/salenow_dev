<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Membersocial extends Model
{
    protected $table = 'member_socials';
    protected $fillable = ['member_id','provider','provider_user_id','image_link'];
    public $timestamps = false;

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function addSocialMember($member_id, $provider, $provider_uid, $profileimage)
    {

        $obj = new Membersocial();
        $obj->member_id = $member_id;
        $obj->provider = $provider;
        $obj->provider_user_id = $provider_uid;
        $obj->image_link = $profileimage;

        if ($obj->save()) {

            return true;
        }
    }

    // check member  by memberid from membersocial

    public static function CheckMemberById($id)
    {
        $memberdetails = DB::table('member_socials as mem')
            ->where('mem.member_id', '=', "$id")
            ->first();
        return !empty($memberdetails) ? $memberdetails : '';
    }


    // check member  by provider uid from membersocial

    public static function CheckMemberByauthid($provider_uid, $provider)
    {
        $memberdetails = DB::table('member_socials as mem')
            ->where('mem.provider_uid', '=', "$provider_uid")
            ->where('mem.provider', '=', $provider)
            ->first();

        return !empty($memberdetails) ? $memberdetails : '';
    }
}
