<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Advertisementtype extends Model
{
    protected $table = 'advertisementtypes';
    protected $fillable = ['typename','typeref','status'];

    public function createRecord($request)
    {
        $obj = new Advertisementtype($request);
        $obj->updated_by = getUserId();
        $obj->created_by = getUserId();
        return $obj->save();
    }

    public function updateRecord($request,$category)
    {
        $category->updated_by = getUserId();
        return $category->update($request);
    }
}
