<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    public function subcategories()
    {
        return $this->belongsToMany(Subcategory::class);
    }
}
