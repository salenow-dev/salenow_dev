<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Adspam extends Model
{
    protected $fillable = ['alladsmaster_id', 'spamreason_id'];

    public function allads()
    {
        return $this->belongsTo(Alladsmaster::class);
    }

    public function saveNewSpam($request)
    {
        $obj = new Adspam();
        $obj->alladsmaster_id = $request['ad_id'];
        $obj->spamreason_id = serialize($request['spamreason_id']);
        $obj->created_by = getUserId();
        $obj->updated_by = getUserId();
        if ($obj->save()) {
            return $obj->id;
        }
    }
}
