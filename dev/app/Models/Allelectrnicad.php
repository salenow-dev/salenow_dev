<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Allelectrnicad extends Model
{
    protected $fillable = ['brand_id', 'model', 'authenticity', 'features'];
    public $timestamps = false;

    public function saveData($request, $member_master_id)
    {

        if (isset($request['brand_id']) OR isset($request['model']) OR isset($request['authenticity']) OR isset($request['features'])) {
            $obj = new Allelectrnicad($request);
            $obj->alladsmaster_id = $member_master_id;
            if (!empty($request['option'])) {
                $obj->features = serialize($request['option']);
            }
            if ($obj->save()) {
                return $obj->id;
            }
        }
    }

    public function updateData($request, $member_master_id)
    {

        if (isset($request['brand_id']) OR isset($request['model']) OR isset($request['authenticity']) OR isset($request['features'])) {
            $obj = Allelectrnicad::where('alladsmaster_id',$member_master_id)->first();
            if (!empty($request['option'])) {
                $obj->features = serialize($request['option']);
            }
            if ($obj->update($request)) {
                return $obj->id;
            }
        }
    }
}
