<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Membervoucher extends Model
{
    protected $fillable = ['ad_referance', 'member_id', 'price', 'boostpack_id', 'boostpackinvoiceno', 'status', 'validitymonths'];

    public function vouchercodes()
    {
        return $this->hasMany(Membervouchercode::class);
    }

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function allad()
    {
        return $this->belongsTo(Alladsmaster::class,'ad_referance','ad_referance');
    }

    public function newVoucherForAd($request, $code)
    {
        $obj = new Membervoucher($request);
        $obj->save();
        foreach ($code as $key => $voucherCode) {
            $obj->vouchercodes()->create([
                'ad_referance' => $request['ad_referance'],
                'boosttype' => $key,
                'vouchercode' => $voucherCode,
            ]);
        }
    }

    public function newVoucherForPacks($request, $code)
    {
        $obj = new Membervoucher($request);
        $obj->save();
        foreach ($code as $key => $voucherCode) {
            foreach ($voucherCode as $item) {
                $obj->vouchercodes()->create([
                    'boosttype' => $key,
                    'vouchercode' => $item,
                ]);
            }
        }
    }


    public static function getAllAdPackRequests()
    {
        $alldata = DB::table('membervouchers as mv')
            ->leftJoin('membervouchercodes as mvc', 'mv.id', '=', 'mvc.membervoucher_id')
            ->leftJoin('members as m', 'mv.member_id', '=', 'm.id')
            ->leftJoin('boostpacks as bp', 'mv.boostpack_id', '=', 'bp.id')
            ->where('mv.status', '=', "pending")
            ->whereNotNull('mv.boostpack_id')
            ->select('mv.id', 'bp.boostpackname', 'mv.boostpackinvoiceno', 'bp.price',
                'mvc.boosttype', 'm.id as member_id', 'm.first_name', 'm.last_name', 'm.email', 'm.member_type',
                'mv.created_at', 'mv.status'
            )
            ->groupBy('mv.member_id')->get();
        return $alldata;
    }

    public static function getAllAdPackRequestsSingle()
    {
        $alldata = DB::table('membervouchers as mv')
            ->leftJoin('membervouchercodes as mvc', 'mv.id', '=', 'mvc.membervoucher_id')
            ->leftJoin('members as m', 'mv.member_id', '=', 'm.id')
            ->leftJoin('alladsmasters as adm', 'mv.ad_referance', '=', 'adm.ad_referance')
            ->where('mv.status', '=', "pending")
            ->whereNotNull('mv.ad_referance')
            ->whereNull('mv.boostpack_id')
            ->select('mv.id', 'adm.id as adid','adm.adtitle', 'adm.slug', 'mv.boostpackinvoiceno', 'mvc.vouchercode', 'mv.price', 'mv.ad_referance',
                'm.id as member_id', 'm.first_name', 'm.last_name', 'm.email', 'm.member_type',
                'mvc.id as vouchercode_id', 'mvc.boosttype', 'mv.created_at', 'mv.status'
            )
            ->orderBy('mv.created_at', 'desc')
            ->get();
        return $alldata;
    }

    public function getAllActiveVouchersByMember($memberId, $boosttype)
    {
        $alldata = DB::table('membervouchers as mv')
            ->leftJoin('membervouchercodes as mvc', 'mv.id', '=', 'mvc.membervoucher_id')
            ->where('mv.member_id', '=', $memberId)
            ->whereNull('mv.ad_referance')
            ->where('mvc.boosttype', '=', $boosttype)
            ->where('mvc.status', '=', false)
            ->where('mv.status', '=', 'active')
            ->select('mvc.id', 'mvc.vouchercode', 'mvc.boosttype')
//            ->groupBy('mvc.boosttype')
            ->get();
        return ($alldata->isEmpty()) ? '' : $alldata;
    }

    public function checkInactiveVouchersByMember($memberId, $boosttype)
    {
        $alldata = DB::table('membervouchers as mv')
            ->leftJoin('membervouchercodes as mvc', 'mv.id', '=', 'mvc.membervoucher_id')
            ->where('mv.member_id', '=', $memberId)
            ->where('mvc.status', '=', false)
            ->select('mvc.id', 'mvc.vouchercode', 'mvc.ad_referance', 'mvc.boosttype')
//            ->groupBy('mvc.boosttype')
            ->get();
        return $alldata;
    }

}
