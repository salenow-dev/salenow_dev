<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'location_districts';

    /*
     * Relations --------------------------------------------------------------------------------------
     * */
    public function adsdistricts()
    {
        return $this->hasMany(Alladsmaster::class);
    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }

    public function alladscity()
    {
        return $this->hasMany(Alladsmaster::class, 'city_id');
    }

    public function allads()
    {
        return $this->hasMany(Alladsmaster::class, 'district_id');
    }

    /************************************************************************************************
     *  START All District Ad Count
     * */
//    public function allad()
//    {
//        return $this->hasMany(Alladsmaster::class);
//    }
//    public function alladsCount()
//    {
//        return $this->hasOne(Alladsmaster::class)
//            ->where('status', 'confirm')
//            ->selectRaw('district_id, count(*) as adsCount')->orderBy('adsCount','desc')
//            ->groupBy('district_id');
//    }
//
//    public function getAlladsCountAttribute()
//    {
//        // if relation is not loaded already, let's do it first
//        if (!array_key_exists('alladsCount', $this->relations))
//            $this->load('alladsCount');
//        $related = $this->getRelation('alladsCount');
//        // then return the count directly
//        return ($related) ? (int)$related->adsCount : 0;
//    }

    /************************************************************************************************
     *  START All City Ad Count
     * */
    public function allcityadsCount()
    {
        return $this->hasOne(Alladsmaster::class)
            ->selectRaw('city_id, count(*) as cityAdCount')
            ->groupBy('city_id')
            ->orderBy('cityAdCount', 'DESC');
    }

    public function getAllcityadsCountAttribute()
    {
        // if relation is not loaded already, let's do it first
        if (!array_key_exists('allcityadsCount', $this->relations))
            $this->load('allcityadsCount');
        $related = $this->getRelation('allcityadsCount');
        // then return the count directly
        return ($related) ? (int)$related->cityAdCount : 0;
    }
    /*
     *  END All City Ad Count
     **************************************************************************************************/

    /*-------------------------------------------------------------------------------------------------*/
}
