<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $fillable=['ref_code','member_id','company_name','email','contact_no','status'];
}
