<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Boostviewcount extends Model
{
    protected $fillable = ['alladsmaster_id','views','status'];

    public function allads()
    {
        return $this->belongsTo(Alladsmaster::class,'alladsmaster_id');
    }
}
