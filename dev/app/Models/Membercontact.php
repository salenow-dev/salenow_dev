<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Membercontact extends Model
{
    protected $fillable = ['member_id','contactnumber'];
    public $timestamps = false;

    public function member()
    {
        return $this->belongsTo(Member::class);
    }
}
