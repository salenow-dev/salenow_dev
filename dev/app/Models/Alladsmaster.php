<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Alladsmaster extends Model
{

    use Notifiable;

    protected $fillable = ['ad_referance', 'category_id', 'sub_category_id', 'member_id', 'district_id', 'city_id', 'adtitle', 'description', 'price',
        'nego', 'featuredimage', 'itemcondition', 'itemtype', 'contact', 'status', 'slug', 'ad_boost_status', 'ad_type', 'updated_by', 'published_at', 'views', 'clicks'];
    public $last_name = false;
    protected $dates = ['created_at', 'updated_at'];

    /*****************************************************************************************************************************************************************************
     * START Relations
     *****************************************************************************************************************************************************************************/

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function subcategory()
    {
        return $this->belongsTo(Subcategory::class, 'sub_category_id');
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function propertyad()
    {
        return $this->hasOne(Allpropertyad::class);
    }

    public function vehiclead()
    {
        return $this->hasOne(Allvehiclead::class);
    }

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function spamreasons()
    {
        return $this->belongsToMany(Spamreason::class);
    }

    public function boostads()
    {
        return $this->hasOne(Boostviewcount::class);
    }

    public function vouchers()
    {
        return $this->hasOne(Membervoucher::class, 'ad_referance', 'ad_referance');
    }

    public function filters()
    {
        return $this->hasMany(Alladsfilter::class);
    }

    /****************************************************************************************************************************************************************************
     * END RELATIONS
     * STSRT CURD
     ****************************************************************************************************************************************************************************/

    // Post new Ad
    public function SaveMemberAdds($request, $filterVals = null)
    {
        $memberId = getMemberId();
        $obj = new Alladsmaster($request);
        $obj->status = 'draft';
        //$obj->status = 'pending';
        //$obj->contact = serialize($request['contactnumber']);
        //$obj->member_id = getMemberId();
        //check weather member belongs to company or not by checking the memberpremiums relation
        $obj->member_id = $memberId;
        if ($obj->save()) {
            foreach ($filterVals as $key => $item) {
                if ($item != '') {
                    $obj->filters()->save(new Alladsfilter(['filtername' => $key, 'categoryfiltervalue_id' => $item]));
                }
            }
            return $obj->id;
        }
    }

    public function UpdateMemberAdds($request, $filterVals = null)
    {
        $allAdFilters = Alladsfilter::where('alladsmaster_id', $request['adId'])->get();
        if ($allAdFilters->isNotEmpty()) {
            Alladsfilter::where('alladsmaster_id', $request['adId'])->delete();
        }
        $obj = Alladsmaster::lockForUpdate()->find($request['adId']);
        $obj->status = 'draft';
        //$obj->status = 'pending';
        //$obj->contact = serialize($request['contactnumber']);
        if ($obj->update($request)) {
            foreach ($filterVals as $key => $item) {
                if ($item != '') {
                    $obj->filters()->save(new Alladsfilter(['filtername' => $key, 'categoryfiltervalue_id' => $item]));
                }
            }
            return $request['adId'];
        }
    }

    // Autometically update the ad Referance no
    public function updateReferance($id, $ref)
    {
        $obj = Alladsmaster::findOrFail($id);
        $obj->ad_referance = $ref;
        if ($obj->save()) {
            return $ref;
        }
    }

    public function updateFeatuedImageName($id, $name)
    {
        $obj = Alladsmaster::findOrFail($id);
        $obj->featuredimage = $name;
        if ($obj->save()) {
            return true;
        }
    }

    public function updateThumb($id, $name)
    {
        $obj = Alladsmaster::findOrFail($id);
        $obj->feturedthumb = $name;
        if ($obj->save()) {
            return true;
        }
    }

    //add featured image
    public function updateFeturedImage($id, $fetured)
    {
        $obj = Alladsmaster::findOrFail($id);
        $obj->featuredimage = $fetured;
        if ($obj->save()) {
            return $obj->id;
        }
    }

    //update the ad status - publish, pending, spam
    public static function updateStatus($obj, $status)
    {
        $obj->status = $status;
        $obj->updated_by = getUserId();
        $obj->published_at = Carbon::now();
        if ($obj->save()) {
            return $obj->id;
        }
    }

    public function plusViews()
    {
        return $this->update(['views' => $this->views + 1]);
    }

    public function plusClicks()
    {
        return $this->update(['clicks' => $this->clicks + 1]);
    }

    /***************************************************************************************************************************************************************************
     * END CURD
     * Start Scope
     ***************************************************************************************************************************************************************************/
    public function scopeStatus($query, $status = 'confirm')
    {
        return $query->where('status', '=', $status);
    }

    public function scopeSearch($query, $search)
    {
        return $query
            ->where('adtitle', 'like', "%" . $search . "%")
            ->orWhere('ad_referance', 'like', "%" . $search . "%")
            ->orWhere('created_at', 'like', "%" . $search . "%")
            ->orWhereHas('city', function ($q) use ($search) {
                $q->searchcity($search);
            });
    }

    /***************************************************************************************************************************************************************************
     * END Scope
     ***************************************************************************************************************************************************************************/

    public static function allPublishedAds()
    {

        $alldataegt = DB::table('alladsmasters as mm')
            ->leftJoin('allvehicleads as vm', 'mm.id', '=', 'vm.alladsmaster_id')
            ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
            ->leftJoin('sub_categories as scat', 'mm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_districts as dist', 'mm.district_id', '=', 'dist.id')
            ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
            ->leftJoin('brands as vb', 'vm.brand_id', '=', 'vb.id')
            ->where('mm.status', '=', "confirm")
            ->select('mm.id as adid', 'mm.adtitle', 'mm.featuredimage', 'mm.created_at', 'mm.updated_at', 'vb.brand_name as brand', 'vm.model as model', 'mm.price', 'vm.mileage', 'mm.slug', 'cat.category_name as category', 'scat.sub_category_name as subcategory', 'cit.city_name as city', 'dist.district_name'
            )
            ->orderBy('mm.updated_at', 'DESC')
            ->paginate(10);
        return $alldataegt;
    }

    public static function getAllDeletedAds()
    {
        $alldataegt = DB::table('alladsmasters as mm')
            ->leftJoin('allvehicleads as vm', 'mm.id', '=', 'vm.alladsmaster_id')
            ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
            ->leftJoin('sub_categories as scat', 'mm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_districts as dist', 'mm.district_id', '=', 'dist.id')
            ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
            ->leftJoin('brands as vb', 'vm.brand_id', '=', 'vb.id')
            ->leftJoin('deletereasons as dr', 'mm.deletereason_id', '=', 'dr.id')
            ->where('mm.status', '=', "deleted")
            ->select('mm.id as id', 'mm.ad_referance as ad_referance', 'mm.adtitle', 'mm.featuredimage', 'mm.created_at', 'mm.updated_at',
                'vb.brand_name as brand', 'vm.model as model', 'mm.price', 'vm.mileage', 'mm.slug',
                'cat.category_name as category', 'scat.sub_category_name as subcategory',
                'cit.city_name as city', 'dist.district_name', 'dr.reason as del_reason', 'cat.category_code as cat_code '
            )
            ->orderBy('mm.updated_at', 'DESC')
            ->paginate(10);
        return $alldataegt;
    }

    public static function GetVehidatabyid($id)
    {
        $alldataegt = DB::table('alladsmasters as mm')
            ->leftJoin('allvehicleads as vm', 'mm.id', '=', 'vm.alladsmaster_id')
            ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
            ->leftJoin('sub_categories as scat', 'mm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_districts as dist', 'mm.district_id', '=', 'dist.id')
            ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
            ->leftJoin('brands as vb', 'vm.brand_id', '=', 'vb.id')
            ->leftJoin('vehicle_fuels as vfu', 'vm.fuel_id', '=', 'vfu.id')
            ->leftJoin('vehicle_transmissions as vtra', 'vm.tranmission_id', '=', 'vtra.id')
            ->leftJoin('vehicle_condition as vcon', 'mm.itemcondition', '=', 'vcon.id')
            ->leftJoin('vehicle_types as vhtype', 'vm.bodytype_id', '=', 'vhtype.id')
            ->where('mm.id', '=', "$id")
            ->where('mm.status', '=', 'confirm')
            ->select('mm.id as adid', 'mm.ad_referance', 'vb.id as brand_id', 'vb.brand_name', 'vm.model as model', 'mm.price', 'vm.mileage', 'cat.category_name', 'cat.category_code',
                'vm.bodytype_id',
                'scat.sub_category_name', 'scat.slug', 'dist.district_name as district', 'cit.city_name', 'vfu.fuel_name', 'vm.enginesize', 'vm.registry_year', 'mm.adtitle',
                'mm.description', 'mm.ad_type', 'mm.created_at', 'mm.updated_at', 'mm.published_at', 'mm.featuredimage', 'vtra.transmission_name', 'vcon.condition_name',
                'mm.slug', 'mm.itemcondition', 'mm.nego', 'mm.contact', 'mm.views', 'mm.member_id', 'vhtype.type_name as bodytype')
            ->get();

        return (!empty($alldataegt[0]) ? $alldataegt[0] : '');
    }

    public static function GetElecdatabyid($id)
    {
        $alldataegt = DB::table('alladsmasters as mm')
            ->leftJoin('allelectrnicads as ead', 'mm.id', '=', 'ead.alladsmaster_id')
            ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
            ->leftJoin('sub_categories as scat', 'mm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_districts as dist', 'mm.district_id', '=', 'dist.id')
            ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
            ->leftJoin('brands as vb', 'ead.brand_id', '=', 'vb.id')
            ->where('mm.id', '=', $id)
            ->where('mm.status', '=', 'confirm')
            ->select('mm.id as adid', 'mm.ad_referance', 'vb.id as brand_id', 'vb.brand_name', 'ead.model as model', 'mm.price', 'mm.nego', 'cat.category_code',
                'cat.category_name', 'scat.sub_category_name', 'scat.slug', 'dist.district_name as district', 'cit.city_name', 'mm.adtitle', 'mm.description', 'mm.ad_type',
                'mm.created_at', 'mm.updated_at', 'mm.featuredimage', 'ead.features', 'mm.contact', 'ead.authenticity', 'mm.itemtype',
                'mm.slug', 'mm.itemcondition', 'mm.published_at', 'mm.views', 'mm.member_id')
            ->get();
        return (!empty($alldataegt[0]) ? $alldataegt[0] : '');
    }

    public static function GetPropertybyid($id)
    {
        $alldataegt = DB::table('alladsmasters as mm')
            ->leftJoin('allpropertyads as pad', 'mm.id', '=', 'pad.alladsmaster_id')
            ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
            ->leftJoin('sub_categories as scat', 'mm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_districts as dist', 'mm.district_id', '=', 'dist.id')
            ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
            ->where('mm.status', '=', "confirm")
            ->where('mm.id', '=', "$id")
            ->where('mm.status', '=', 'confirm')
            ->select('mm.id as adid', 'mm.ad_referance', 'mm.category_id', 'mm.price', 'mm.description', 'mm.id as adid', 'mm.published_at', 'mm.adtitle', 'cat.category_code',
                'mm.featuredimage', 'mm.created_at', 'mm.updated_at', 'mm.ad_type', 'mm.ad_boost_status', 'mm.itemtype', 'mm.itemcondition', 'mm.nego',
                'cat.category_name', 'scat.sub_category_name', 'scat.slug', 'dist.district_name as district', 'cit.city_name', 'mm.contact', 'pad.landsize', 'pad.propertysize',
                'mm.slug', 'pad.beds', 'pad.baths', 'pad.address', 'pad.features', 'mm.views', 'mm.member_id'
            )
            ->get();
        return (!empty($alldataegt[0]) ? $alldataegt[0] : '');
    }

    public static function GetHotelAdsbyid($id)
    {
        $alldataegt = DB::table('alladsmasters as mm')
            ->leftJoin('allhoteltravelads as htl', 'mm.id', '=', 'htl.alladsmaster_id')
            ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
            ->leftJoin('sub_categories as scat', 'mm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_districts as dist', 'mm.district_id', '=', 'dist.id')
            ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
//
//            ->leftJoin('categoryfiltervalues as catfill', 'htl.numberofguest', '=', 'catfill.id')
//            ->leftJoin('categoryfiltervalues as catfill2', 'htl.bedsize', '=', 'catfill2.id')
//            ->leftJoin('categoryfiltervalues as catfill3', 'htl.beds', '=', 'catfill3.id')
//            ->leftJoin('categoryfiltervalues as catfill4', 'htl.bathrooms', '=', 'catfill4.id')
//            ->leftJoin('categoryfiltervalues as catfill5', 'htl.bathroomtypes', '=', 'catfill5.id')
//            ->leftJoin('categoryfiltervalues as catfill6', 'htl.numberofrooms', '=', 'catfill6.id')
//            ->leftJoin('categoryfiltervalues as catfill7', 'htl.type', '=', 'catfill7.id')
//            ->leftJoin('categoryfiltervalues as catfill8', 'htl.occupancy', '=', 'catfill8.id')
//            ->leftJoin('categoryfiltervalues as catfill9', 'htl.size', '=', 'catfill9.id')
            ->where('mm.status', '=', "confirm")
            ->where('mm.id', '=', "$id")
            ->where('mm.status', '=', 'confirm')
            ->select('mm.id as adid', 'mm.ad_referance', 'mm.category_id', 'mm.price', 'mm.description', 'mm.id as adid',
                'mm.published_at', 'mm.adtitle', 'cat.category_code', 'mm.featuredimage', 'mm.created_at', 'mm.updated_at',
                'mm.ad_type', 'mm.ad_boost_status', 'mm.itemtype', 'mm.itemcondition', 'mm.nego',
                'cat.category_name', 'scat.sub_category_name', 'scat.slug', 'dist.district_name as district', 'cit.city_name',
                'mm.contact',  'mm.views', 'mm.member_id', 'htl.numberofguest as numberofguest', 'htl.bedsize as bedsize',
                'htl.beds as beds', 'htl.bathrooms as bathrooms', 'htl.bathroomtypes as bathroomtypes',
                'htl.numberofrooms as numberofrooms', 'htl.min_check_in_time as min_check_in_time', 'htl.max_check_out_time as max_check_out_time',
                'htl.type as type', 'htl.address as address', 'htl.occupancy as occupancy', 'htl.size as size', 'htl.price_meal as price_meal',
                'htl.features as features'

            )
            ->get();
        return (!empty($alldataegt[0]) ? $alldataegt[0] : '');
    }

    public static function GetJobsbyid($id)
    {
        $alldataegt = DB::table('alladsmasters as mm')
            ->leftJoin('alljobads as job', 'mm.id', '=', 'job.alladsmaster_id')
            ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
            ->leftJoin('sub_categories as scat', 'mm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_districts as dist', 'mm.district_id', '=', 'dist.id')
            ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
            ->where('mm.status', '=', "confirm")
            ->where('mm.id', '=', "$id")
            ->where('mm.status', '=', 'confirm')
            ->select('mm.id as adid', 'mm.ad_referance', 'mm.category_id', 'mm.price', 'mm.description', 'mm.id as adid', 'mm.published_at', 'mm.adtitle', 'cat.category_code',
                'mm.featuredimage', 'mm.created_at', 'mm.updated_at', 'mm.ad_type', 'mm.ad_boost_status', 'mm.itemtype', 'mm.itemcondition', 'mm.nego', 'mm.featuredimage',
                'cat.category_name', 'scat.sub_category_name', 'scat.slug', 'dist.district_name as district', 'cit.city_name', 'mm.contact', 'job.closingdate', 'mm.itemtype',
                'mm.slug', 'job.salary', 'mm.views', 'mm.member_id'
            )
            ->get();
        return (!empty($alldataegt[0]) ? $alldataegt[0] : '');
    }

    public static function GetAllAdMasterSingleFrontend($id)
    {
        $alldataget = DB::table('alladsmasters as mm')
            ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
            ->leftJoin('sub_categories as scat', 'mm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_districts as dist', 'mm.district_id', '=', 'dist.id')
            ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
            ->leftJoin('members as mbr', 'mm.member_id', '=', 'mbr.id')
            ->where('mm.id', '=', $id)
            ->where('mm.status', '=', 'confirm')
            ->select('mm.id as adid', 'mm.category_id', 'mm.ad_referance', 'mm.adtitle', 'mm.price', 'mm.nego', 'mm.featuredimage', 'mm.feturedthumb', 'cat.category_code',
                'cat.slug as catslug', 'cat.category_name', 'cat.category_code as cat_code', 'scat.sub_category_name', 'scat.slug', 'dist.district_name as district', 'cit.city_name', 'mm.description',
                'mm.slug', 'mm.itemcondition', 'mm.itemtype', 'mm.contact', 'mm.published_at', 'mm.created_at', 'mm.updated_at', 'mm.status', 'mm.slug', 'mbr.id as mbr_id',
                'mbr.first_name', 'mbr.last_name', 'mbr.email', 'mbr.member_type', 'mm.views', 'mm.member_id'
            )->first();
        return (!empty($alldataget) ? $alldataget : '');
    }

    /* ------------------------------------------------------------------------------------------------------------------------
     * Backend
     * ----------------------------------------------------------------------------------------------------------------------- */

    public static function getAllPendingVehiclesAds()
    {
        $alldataegt = DB::table('alladsmasters as adm')
            ->leftJoin('categories as cat', 'adm.category_id', '=', 'cat.id')
            ->leftJoin('sub_categories as scat', 'adm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_districts as dist', 'adm.district_id', '=', 'dist.id')
            ->leftJoin('location_cities as cit', 'adm.city_id', '=', 'cit.id')
            ->where('adm.status', '=', "pending")
            ->select('adm.id', 'adm.created_at', 'adm.price', 'adm.adtitle', 'adm.ad_referance', 'adm.featuredimage', 'adm.status', 'cat.category_name as category',
                'cit.city_name as city', 'dist.district_name', 'adm.ad_type'
            )
            ->orderBy('adm.created_at', 'ASC')
            ->get()->toArray();
        return $alldataegt;
    }

    public static function getAllConfirmedAds()
    {
        $alldataegt = DB::table('alladsmasters as adm')
            ->leftJoin('categories as cat', 'adm.category_id', '=', 'cat.id')
            ->leftJoin('sub_categories as scat', 'adm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_districts as dist', 'adm.district_id', '=', 'dist.id')
            ->leftJoin('location_cities as cit', 'adm.city_id', '=', 'cit.id')
            ->where('adm.status', '=', "confirm")
            ->select('adm.id', 'adm.created_at', 'adm.price', 'adm.ad_referance', 'adm.adtitle', 'adm.status', 'adm.featuredimage', 'cat.category_name as category', 'cit.city_name as city', 'dist.district_name', 'adm.views', 'adm.clicks', 'adm.ad_boost_status'
            )
            ->orderBy('adm.id', 'DESC')
            ->paginate(10);

        return $alldataegt;
    }

    public static function getAllDraftAds()
    {
        $alldataegt = DB::table('alladsmasters as adm')
            ->leftJoin('categories as cat', 'adm.category_id', '=', 'cat.id')
            ->leftJoin('sub_categories as scat', 'adm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_districts as dist', 'adm.district_id', '=', 'dist.id')
            ->leftJoin('location_cities as cit', 'adm.city_id', '=', 'cit.id')
            ->where('adm.status', '=', "draft")
            ->select('adm.id', 'adm.created_at', 'adm.price', 'adm.ad_referance', 'adm.adtitle', 'adm.status', 'adm.featuredimage', 'cat.category_name as category', 'cit.city_name as city', 'dist.district_name', 'adm.views', 'adm.clicks', 'adm.ad_boost_status'
            )
            ->orderBy('adm.id', 'DESC')
            ->paginate(10);

        return $alldataegt;
    }

    public static function getAllSpamdAds($status = null)
    {
        $alldataegt = DB::table('alladsmasters as adm')
            ->leftJoin('categories as cat', 'adm.category_id', '=', 'cat.id')
            ->leftJoin('sub_categories as scat', 'adm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_districts as dist', 'adm.district_id', '=', 'dist.id')
            ->leftJoin('location_cities as cit', 'adm.city_id', '=', 'cit.id');
        if (!is_null($status)) {
            $alldataegt->where('adm.status', '=', $status);
        }
        $result = $alldataegt->select('adm.id', 'adm.created_at', 'adm.ad_referance', 'adm.updated_at', 'adm.price', 'adm.adtitle', 'adm.status', 'adm.featuredimage',
            'cat.category_name as category', 'cit.city_name as city', 'dist.district_name', 'cat.category_code as cat_code')
            ->get()->toArray();
        return $result;
    }

    /* -------------------------------------------------------------------------------------------------------------------------------
     * Start Queries of backed ads Review
     * ------------------------------------------------------------------------------------------------------------------------------- */

    public static function GetAllAdMasterSingle($id)
    {
        $alldataget = DB::table('alladsmasters as mm')
            ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
            ->leftJoin('sub_categories as scat', 'mm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_districts as dist', 'mm.district_id', '=', 'dist.id')
            ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
            ->leftJoin('members as mbr', 'mm.member_id', '=', 'mbr.id')
            ->leftJoin('adspams as spm', 'mm.id', '=', 'spm.alladsmaster_id')
            ->where('mm.id', '=', "$id")
            ->select('mm.id as adid', 'mm.ad_referance', 'mm.adtitle', 'mm.price', 'mm.nego', 'mm.featuredimage', 'mm.slug',
                'mm.feturedthumb', 'cat.category_name as category', 'cat.category_code as cat_code',
                'scat.sub_category_name as subcategory', 'dist.district_name as district', 'cit.city_name as city',
                'mm.description', 'mm.itemcondition', 'mm.itemtype', 'mm.contact', 'mm.created_at', 'mm.updated_at', 'mm.published_at',
                'mm.status', 'mm.ad_type', 'mbr.id as mbr_id', 'mbr.first_name', 'mbr.last_name', 'mbr.email', 'mbr.member_type',
                'spm.spamreason_id as spams'
            )->first();
        return (!empty($alldataget) ? $alldataget : '');
    }

    public static function getVehicleAdSingle($id)
    {
        $info = DB::table('allvehicleads as vm')
            ->leftJoin('brands as vb', 'vm.brand_id', '=', 'vb.id')
//            ->leftJoin('vehicle_models as vmod', 'vm.model_id', '=', 'vmod.id')
            ->leftJoin('vehicle_types as vt', 'vm.bodytype_id', '=', 'vt.id')
            ->leftJoin('vehicle_fuels as vfu', 'vm.fuel_id', '=', 'vfu.id')
            ->leftJoin('vehicle_transmissions as vtra', 'vm.tranmission_id', '=', 'vtra.id')
            ->where('vm.alladsmaster_id', '=', "$id")
            ->select(
                'vb.brand_name as brand', 'vm.model', 'vt.type_name as bodytype', 'vfu.fuel_name as fuel', 'vtra.transmission_name as transmission', 'vm.enginesize', 'vm.mileage', 'vm.color', 'vm.registry_year', 'vm.features'
            )->first();
        return (!empty($info) ? $info : '');
    }

    public static function getElectronicAdSingle($id)
    {
        $info = DB::table('allelectrnicads as ead')
            ->leftJoin('brands as vb', 'ead.brand_id', '=', 'vb.id')
            ->leftJoin('categoryfiltervalues as cfv', 'ead.authenticity', '=', 'cfv.id')
            ->where('ead.alladsmaster_id', '=', $id)
            ->select(
                'vb.brand_name as brand', 'ead.model', 'ead.features', 'cfv.filtervalue as authenticity'
            )->first();
        return (!empty($info) ? $info : '');
    }

    public static function getPropertyAdSingle($id)
    {
        $info = DB::table('allpropertyads as apa')
//            ->leftJoin('categoryfiltervalues as cfv', 'ead.authenticity', '=', 'cfv.id')
            ->where('apa.alladsmaster_id', '=', $id)
            ->select(
                'apa.landsize', 'apa.propertysize', 'apa.beds', 'apa.baths', 'apa.address', 'apa.features'
            )->first();
        return (!empty($info) ? $info : '');
    }

    /* -------------------------------------------------------------------------------------------------------------------------------
     * END Queries of backed ads Review
     * ------------------------------------------------------------------------------------------------------------------------------- */

    // get add count by member id
    public static function MemberAdsCountByStatus($id, $status)
    {
        $query = DB::table('alladsmasters')
            ->select(DB::raw('count(*) as addcount, member_id'));
        if ($status != 'all') {
            $query->where('status', '=', $status);
        } else {
            $query->whereIn('status', ['confirm', 'pending', 'cancel', 'draft']);
        }
        $addcount = $query->where('member_id', '=', $id)
            ->groupBy('member_id')
            ->get()->toArray();

        return !empty($addcount) ? $addcount : '';
    }

    public static function MemberBoostAdsCountById($id)
    {
        $addcount = DB::table('alladsmasters')
            ->select(DB::raw('count(*) as addcount, member_id'))
            ->where('ad_boost_status', '=', 'pin-to-top')
            ->where('status', '=', 'confirm')
            ->where('member_id', '=', $id)
            ->groupBy('member_id')
            ->get()->toArray();

        return !empty($addcount) ? $addcount : '0';
    }
//    Sells Agents Ad Count start
    public static function AgentVehicleAdsToday($id)
    {
        $vehicleadcount = DB::table('alladsmasters')
            ->select(DB::raw('count(*) as vehicleadcount, member_id'))
            ->where('status', '=', 'confirm')
            ->where('member_id', '=', $id)
            ->whereIn('sub_category_id', ['1', '2', '3', '4','5','6'])
            ->whereDate('created_at', date('Y-m-d'))
            ->groupBy('member_id')
            ->get()->toArray();

        return($vehicleadcount);
    }

    public static function AgentVehicleAdsMonth($id)
    {
        $from = new Carbon('first day of this month');
        $to = new Carbon('last day of this month');
        $to = new Carbon('now');
        $vehicleadcount = DB::table('alladsmasters')
            ->select(DB::raw('count(*) as vehicleadcount'))
            ->where('status', '=', 'confirm')
            ->where('member_id', '=', $id)
            ->whereIn('sub_category_id', ['1', '2', '3', '4','5','6'])
            ->whereBetween('created_at', array($from->toDateTimeString(), $to->toDateTimeString()))
            ->get()->toArray();

        return($vehicleadcount);
    }
    public static function AgentPropertyAdsToday($id)
    {

        $proadcount = DB::table('alladsmasters')
            ->select(DB::raw('count(*) as agentadcount'))
            ->where('status', '=', 'confirm')
            ->where('member_id', '=', $id)
            ->whereIn('sub_category_id', ['23', '24', '25', '26','27','28'])
            ->get()->toArray();
        return($proadcount);
    }
    public static function AgentPropertyAdsMonth($id)
    {
        $from = new Carbon('first day of this month');
        $to = new Carbon('last day of this month');

        $proadcount = DB::table('alladsmasters')
            ->select(DB::raw('count(*) as agentadcount'))
            ->where('status', '=', 'confirm')
            ->where('member_id', '=', $id)
            ->whereIn('sub_category_id', ['23', '24', '25', '26','27','28'])
            ->whereBetween('created_at', array($from->toDateTimeString(), $to->toDateTimeString()))
            ->get()->toArray();


        return($proadcount);
    }
//    Sells Agents Ad Count End
    public static function MemberDraftAdsCountById($id)
    {
        $addcount = DB::table('alladsmasters')
            ->select(DB::raw('count(id) as addcount, member_id'))
            ->where('status', '=', 'draft')
            ->where('member_id', '=', $id)
            ->groupBy('member_id')
            ->get()->toArray();

        return !empty($addcount) ? $addcount : '';
    }

    //  get  dta by  member id  - **** removed from details page
    public static function GetCatCodedatabyid($id)
    {
        $alldataegt = DB::table('alladsmasters as mm')
            ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
            ->where('mm.id', '=', "$id")
            ->select('mm.id', 'mm.ad_referance', 'cat.category_code', 'mm.member_id', 'mm.sub_category_id', 'mm.status')
            ->get();

        return (!empty($alldataegt[0]) ? $alldataegt[0] : '');
    }

    public static function GetVehifeaturesbyid($id)
    {
        $alldataegt = DB::table('alladsmasters as mm')
            ->leftJoin('allvehicleads as vm', 'mm.id', '=', 'vm.alladsmaster_id')
            ->where('mm.status', '=', "confirm")
            ->where('mm.id', '=', "$id")
            ->select('mm.id as addid', 'vm.features')
            ->get();

        return (!empty($alldataegt[0]) ? $alldataegt[0] : '');
    }

    //no use
    public static function GetElecfeaturesbyid($id)
    {
        $alldataegt = DB::table('alladsmasters as mm')
            ->leftJoin('allelectrnicads as em', 'mm.id', '=', 'em.alladsmaster_id')
            ->where('mm.status', '=', "confirm")
            ->where('mm.id', '=', "$id")
            ->select('mm.id as addid', 'em.features')
            ->get();

        return (!empty($alldataegt[0]) ? $alldataegt[0] : '');
    }

    /*
     * @parm sub_cat_id
     */

//    public static function relatedAdsBysubCatid($sub_cat_id, $brand_id, $id)
//    {
//        $alldataegt = DB::table('alladsmasters as mm')
//            ->leftJoin('allvehicleads as vm', 'mm.id', '=', 'vm.alladsmaster_id')
//            ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
//            ->leftJoin('sub_categories as scat', 'mm.sub_category_id', '=', 'scat.id')
//            ->leftJoin('location_districts as dist', 'mm.district_id', '=', 'dist.id')
//            ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
//            ->leftJoin('brands as vb', 'vm.brand_id', '=', 'vb.id')
//            ->where('mm.sub_category_id', '=', "$sub_cat_id")
//            ->where('vm.brand_id', '=', "$brand_id")
//            ->where('mm.id', '!=', "$id")
//            ->where('mm.status', '=', "confirm")
//            ->select('mm.id as adid', 'mm.adtitle', 'mm.feturedthumb', 'mm.created_at', 'vb.brand_name as brand', 'vm.model as model', 'mm.price', 'vm.mileage',
//                'cat.category_name as category', 'cit.city_name as city', 'dist.district_name', 'vm.registry_year as year')->orderBy('mm.id', 'desc')
//            ->limit(4)
//            ->get()->toArray();
//
//        return $alldataegt;
//    }

    public static function relatedAdsBysubCatid($sub_cat_id, $id, $categorycode = null, $brand_id = null, $memberId = null)
    {
        $alldataegt = DB::table('alladsmasters as mm')
            ->leftJoin('allvehicleads as vm', 'mm.id', '=', 'vm.alladsmaster_id')
            ->leftJoin('allelectrnicads as em', 'mm.id', '=', 'em.alladsmaster_id')
            ->leftJoin('allpropertyads as pm', 'mm.id', '=', 'pm.alladsmaster_id')
            ->leftJoin('alljobads as job', 'mm.id', '=', 'job.alladsmaster_id')
            ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
            ->leftJoin('sub_categories as scat', 'mm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_districts as dist', 'mm.district_id', '=', 'dist.id')
            ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
            ->where('mm.status', '=', "confirm");

        if ($sub_cat_id) {
            $alldataegt->where('mm.sub_category_id', '=', "$sub_cat_id");
        }
        if ($brand_id) {
            if ($categorycode == 'veh') {
                $alldataegt->where('vm.brand_id', '=', "$brand_id");
            }
            if ($categorycode == 'ele') {
                $alldataegt->where('em.brand_id', '=', "$brand_id");
            }
        }
        if ($memberId) {
            $alldataegt->where('mm.member_id', '=', "$memberId");
        }

        $alldataegt->where('mm.id', '!=', "$id")
            ->select('mm.id as adid', 'mm.adtitle', 'mm.featuredimage', 'mm.created_at', 'mm.updated_at', 'mm.itemtype',
                'vm.model as model', 'mm.price', 'vm.mileage', 'mm.slug', 'cat.category_name as category',
                'scat.sub_category_name as subcategory', 'cit.city_name as city', 'dist.district_name');
        if ($categorycode == 'job') {
            $alldataegt->select('mm.id as adid', 'mm.adtitle', 'mm.featuredimage', 'mm.created_at', 'mm.updated_at', 'mm.itemtype',
                'vm.model as model', 'mm.price', 'vm.mileage', 'mm.slug', 'cat.category_name as category',
                'scat.sub_category_name as subcategory', 'cit.city_name as city', 'dist.district_name', 'job.industry as industry', 'job.closingdate as closingdate');
        }
        $result = $alldataegt->limit(20)->orderBy('mm.updated_at', 'DESC')->get()->toArray();
        return $result;
    }

    public static function relatedAdsJobs($sub_cat_id, $id, $categorycode = null, $memberId = null)
    {
        $alldataegt = DB::table('alladsmasters as mm')
            ->leftJoin('allvehicleads as vm', 'mm.id', '=', 'vm.alladsmaster_id')
            ->leftJoin('allelectrnicads as em', 'mm.id', '=', 'em.alladsmaster_id')
            ->leftJoin('allpropertyads as pm', 'mm.id', '=', 'pm.alladsmaster_id')
            ->leftJoin('alljobads as job', 'mm.id', '=', 'job.alladsmaster_id')
            ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
            ->leftJoin('sub_categories as scat', 'mm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_districts as dist', 'mm.district_id', '=', 'dist.id')
            ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
            ->leftJoin('categoryfiltervalues as catfil', 'mm.itemtype', '=', 'catfil.id')
            ->leftJoin('categoryfiltervalues as catfil2', 'job.industry', '=', 'catfil2.id')
            ->leftJoin('memberpremiums as prmbr', 'mm.member_id', '=', 'prmbr.member_id')
            ->where('mm.status', '=', "confirm");

        if ($sub_cat_id) {
            $alldataegt->where('mm.sub_category_id', '=', "$sub_cat_id");
        }
        if ($memberId) {
            $alldataegt->where('mm.member_id', '=', "$memberId");
        }

        $alldataegt->where('mm.id', '!=', "$id");
        $alldataegt->select('mm.id as adid', 'mm.adtitle', 'mm.featuredimage', 'mm.created_at', 'mm.updated_at',
            'vm.model as model', 'mm.price', 'vm.mileage', 'mm.slug', 'cat.category_name as category',
            'scat.sub_category_name as subcategory', 'cit.city_name as city', 'dist.district_name', 'job.industry as industry',
            'job.closingdate as closingdate', 'catfil.filtervalue as itemtype', 'catfil2.filtervalue as industry',
            'prmbr.profile_image as profile_image', 'prmbr.company_name as company_name', 'mm.created_at');
        $result = $alldataegt->limit(20)->orderBy('mm.updated_at', 'DESC')->get()->toArray();
        return $result;
    }


    /*
     * get  data  list by member id
     */

    public static function GetAdsListByMemberId($memberid)
    {
        $alldataegt = DB::table('alladsmasters as mm')
            ->leftJoin('allvehicleads as vm', 'mm.id', '=', 'vm.alladsmaster_id')
            ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
            ->leftJoin('sub_categories as scat', 'mm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_districts as dist', 'mm.district_id', '=', 'dist.id')
            ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
            ->leftJoin('brands as vb', 'vm.brand_id', '=', 'vb.id')
//            ->where('mm.status', '=', "confirm")
//            ->where('mm.status', '=', "cancel")
            ->where('mm.member_id', '=', $memberid)
            ->select('mm.id as adid', 'mm.ad_referance', 'mm.adtitle', 'mm.featuredimage', 'mm.created_at', 'mm.updated_at', 'vb.brand_name as brand', 'vm.model as model', 'mm.price', 'vm.mileage', 'cat.category_name as category', 'cit.city_name as city', 'dist.district_name', 'mm.category_id', 'scat.sub_category_code', 'mm.ad_type', 'mm.slug', 'mm.status'
            )
            ->orderBy('mm.id', 'desc')
            ->paginate(8);

        return $alldataegt;
    }

    //get member ads by member id
    public static function GetAdsListByMemberIdByStatus($memberid, $status = null, $boost = null)
    {
        $alldataegt = DB::table('alladsmasters as mm')
            ->leftJoin('allvehicleads as vm', 'mm.id', '=', 'vm.alladsmaster_id')
            ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
            ->leftJoin('sub_categories as scat', 'mm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_districts as dist', 'mm.district_id', '=', 'dist.id')
            ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
            ->leftJoin('brands as vb', 'vm.brand_id', '=', 'vb.id')
            ->leftJoin('membervouchercodes as mvc', 'mm.ad_referance', '=', 'mvc.ad_referance')
            ->where('mm.member_id', '=', $memberid);

        if ($status) {
            $alldataegt->where('mm.status', '=', $status);
        } else {
            $alldataegt->whereIn('mm.status', ['confirm', 'pending', 'cancel', 'draft']);
        }
        if ($boost) {
            $alldataegt->where('mm.ad_boost_status', '=', $boost);
        }
        $alldataegt->select('mm.id as adid', 'mm.adtitle', 'mm.description', 'mm.featuredimage', 'mm.ad_referance', 'mm.created_at', 'mm.updated_at', 'mm.published_at', 'mm.ad_boost_status', 'vb.brand_name as brand',
            'vm.model as model', 'mm.price', 'vm.mileage', 'cat.category_name as category', 'cat.category_code', 'cit.city_name as city', 'dist.district_name',
            'mm.category_id', 'scat.sub_category_code', 'scat.sub_category_name as subcategory', 'mm.ad_type', 'mm.slug', 'mm.status', 'mvc.status as vouchercode_status', 'mm.views', 'mm.clicks'
        );
        $result = $alldataegt->orderBy('mm.id', 'desc')
            ->paginate(8);
        return $result;
    }

    public static function categoryCountByid()
    {
        $subcatlist = DB::table('alladsmasters as mm')
            ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
            ->select('cat.id', DB::raw('count(*) as total'))
            ->groupBy('cat.id')
            ->where('mm.status', '=', 'confirm')
            ->get()->toArray();
        return $subcatlist;
    }

    /*
     * Get Boost ads
     * */

    //unused function - remove
    public static function allBoostAds()
    {
        $alldataegt = DB::table('alladsmasters as mm')
            ->leftJoin('allvehicleads as vm', 'mm.id', '=', 'vm.alladsmaster_id')
            ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
            ->leftJoin('sub_categories as scat', 'mm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_districts as dist', 'mm.district_id', '=', 'dist.id')
            ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
            ->leftJoin('brands as vb', 'vm.brand_id', '=', 'vb.id')
            ->where('mm.status', '=', "confirm")
            ->where('mm.ad_boost_status', '!=', "none")
            ->select('mm.id as adid', 'mm.adtitle', 'mm.featuredimage', 'mm.created_at', 'mm.updated_at', 'mm.published_at', 'mm.ad_boost_status', 'mm.boost_expire_on', 'vb.brand_name as brand', 'vm.model as model', 'mm.price', 'vm.mileage', 'mm.slug', 'cat.category_name as category', 'scat.sub_category_name as subcategory', 'cit.city_name as city', 'dist.district_name'
            )
            ->orderBy('mm.published_at', 'DESC')
            ->get();
        return $alldataegt;
    }

    public static function allPinToTopAds($data, $order = null)
    {
        $categorycode = !empty($data['categorycode']) ? $data['categorycode'] : '';
        $districtid = !empty($data['districtid']) ? $data['districtid'] : '';
        $cityid = !empty($data['cityid']) ? $data['cityid'] : '';
        $categoryid = !empty($data['categoryid']) ? $data['categoryid'] : '';
        $subcategoryid = !empty($data['subcategoryid']) ? $data['subcategoryid'] : '';
//        $desc_date = !empty($data['sort'] == 'desc_date') ? $data['sort'] : '';
//        $asc_price = !empty($data['sort'] == 'asc_price') ? $data['sort'] : '';
//        $desc_price = !empty($data['sort'] == 'desc_price') ? $data['sort'] : '';
        $add_type = !empty($data['addtype']) ? $data['addtype'] : '';

        $vbrand = !empty($data['vehi_data']['brand']) ? $data['vehi_data']['brand'] : '';
        $vtype = !empty($data['vehi_data']['type']) ? $data['vehi_data']['type'] : '';
        $fuel = !empty($data['vehi_data']['fuel']) ? $data['vehi_data']['fuel'] : '';
        $trans = !empty($data['vehi_data']['trans']) ? $data['vehi_data']['trans'] : '';
        $vconditions = !empty($data['vehi_data']['condition']) ? $data['vehi_data']['condition'] : '';
        $milage = !empty($data['vehi_data']['milage']) ? $data['vehi_data']['milage'] : '';
        $from_price = !empty($data['from_price']) ? $data['from_price'] : ''; //  price  from
        $to_price = !empty($data['to_price']) ? $data['to_price'] : ''; //  price  to
        $querydata = !empty($data['query']) ? $data['query'] : ''; //  query  value


        $alldataegt = DB::table('boostviewcounts as bv')
            ->leftJoin('alladsmasters as mm', 'bv.alladsmaster_id', '=', 'mm.id')
            ->leftJoin('allvehicleads as vm', 'mm.id', '=', 'vm.alladsmaster_id')
            ->leftJoin('sub_categories as scat', 'mm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
            ->leftJoin('members as mbr', 'mm.member_id', '=', 'mbr.id')
            ->leftJoin('vehicle_fuels as vfuel', 'vm.fuel_id', '=', 'vfuel.id');
        if (!empty($querydata)) {
            $alldataegt->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
                ->leftJoin('allelectrnicads as em', 'mm.id', '=', 'em.alladsmaster_id')
                ->leftJoin('allpropertyads as pm', 'mm.id', '=', 'pm.alladsmaster_id')
                ->leftJoin('location_districts as dist', 'mm.district_id', '=', 'dist.id')
                ->leftJoin('brands as vb', 'vm.brand_id', '=', 'vb.id')
                ->leftJoin('brands as eb', 'em.brand_id', '=', 'eb.id')
                ->leftJoin('vehicle_condition as vcond', 'mm.itemcondition', '=', 'vcond.id')
                ->leftJoin('vehicle_types as vtype', 'vm.bodytype_id', '=', 'vtype.id')
                ->leftJoin('vehicle_transmissions as vtrans', 'vm.tranmission_id', '=', 'vtrans.id')
                ->leftJoin('alladsfilters as allfil', 'mm.id', '=', 'allfil.alladsmaster_id');
            $alldataegt->Where(function ($query) use ($querydata) {
                $query->ORwhere("cat.category_name", "LIKE", "%$querydata%");
                $query->ORwhere("scat.sub_category_name", "LIKE", "%$querydata%");
                $query->ORwhere("mm.adtitle", "LIKE", "%$querydata%");
                $query->ORwhere("mm.description", "LIKE", "%$querydata%");
                $query->ORwhere("mm.price", "LIKE", "%$querydata%");
                $query->ORwhere("dist.district_name", "LIKE", "%$querydata%");
                $query->ORwhere("cit.city_name", "LIKE", "%$querydata%");
                $query->ORwhere("vb.brand_name", "LIKE", "%$querydata%");
                $query->ORwhere("vm.model", "LIKE", "%$querydata%");
                $query->ORwhere("vm.mileage", "LIKE", "%$querydata%");
                $query->ORwhere("vm.color", "LIKE", "%$querydata%");
                $query->ORwhere("vm.registry_year", "LIKE", "%$querydata%");
                $query->ORwhere("vfuel.fuel_name", "LIKE", "%$querydata%");
                $query->ORwhere("vcond.condition_name", "LIKE", "%$querydata%");
                $query->ORwhere("vtype.type_name", "LIKE", "%$querydata%");
                $query->ORwhere("vtrans.transmission_name", "LIKE", "%$querydata%");
                $query->ORwhere("eb.brand_name", "LIKE", "%$querydata%");
            });
        }
        if (!empty($vtype) && is_array($vtype)) {
            $alldataegt->Where(function ($query) use ($vtype) {
                foreach ($vtype as $vtypeid) {
                    $query->Where('vm.bodytype_id', $vtypeid);
                }
            });
        }
        if (!empty($vbrand) && is_array($vbrand)) {
            $alldataegt->Where(function ($query) use ($vbrand, $categorycode) {
                if ($categorycode == 'veh') {
                    foreach ($vbrand as $brandid) {
                        $query->orWhere('vm.brand_id', $brandid);
                    }
                }
            });
        }
        if (!empty($ebrand) && is_array($ebrand)) {
            $alldataegt->Where(function ($query) use ($ebrand, $categorycode) {
                if ($categorycode == 'ele') {
                    foreach ($ebrand as $brandid) {
                        $query->orWhere('em.brand_id', $brandid);
                    }
                }
            });
        }
        if (!empty($emodel)) {
            $alldataegt->where('em.model', '=', $emodel);
        }
        if (!empty($bath) && is_array($bath)) {
            $alldataegt->Where(function ($query) use ($bath) {
                foreach ($bath as $bathid) {
                    $query->orWhere('pm.baths', $bathid);
                }
            });
        }
        if (!empty($beds) && is_array($beds)) {
            $alldataegt->Where(function ($query) use ($beds) {
                foreach ($beds as $bedsid) {
                    $query->orWhere('pm.beds', $bedsid);
                }
            });
        }
        if (!empty($land_size)) {
            $alldataegt->where('pm.landsize', '=', $land_size);
        }
        if (!empty($property_size)) {
            $alldataegt->where('pm.propertysize', '=', $property_size);
        }

//    if (!empty($vservice)) {
//            $alldataegt->leftJoin('vehicle_types as vtype', 'vm.bodytype_id', '=', 'vtype.id');
//            $alldataegt->Where(function ($query) use ($vservice) {
//                foreach ($vservice as $vserviceid) {
//                    $query->Where('vm.bodytype_id', $vtypeid);
//                }
//            });
//        }

        if (!empty($gender) && is_array($gender)) {

            $alldataegt->Where(function ($query) use ($gender) {
                foreach ($gender as $genderid) {
                    $query->orWhere('allfil.categoryfiltervalue_id', $genderid);
                }
            });
        }
        if (!empty($districtid)) {
            $alldataegt->where('mm.district_id', '=', $districtid);
        }
        if (!empty($cityid)) {
            $alldataegt->where('mm.city_id', '=', $cityid);
        }
        if (!empty($categoryid)) {
            $alldataegt->where('mm.category_id', '=', $categoryid);
        }
        if (!empty($subcategoryid)) {
            $alldataegt->where('mm.sub_category_id', '=', $subcategoryid);
        }
        if (!empty($add_type)) {
            $alldataegt->where('mm.ad_type', '=', $add_type);
        }
        if (!empty($filter_condition)) {
            $alldataegt->where('mm.itemcondition', '=', $filter_condition);
        }
        if (!empty($item_type) && is_array($item_type)) {
            $alldataegt->Where(function ($query) use ($item_type) {
                foreach ($item_type as $item_typeid) {
                    $query->orWhere('mm.itemtype', $item_typeid);
                }
            });
        }
        if (!empty($filter_authenticity)) {
            $alldataegt->where('em.authenticity', '=', $filter_authenticity);
        }
        if (!empty($fuel)) {
            $alldataegt->where('vfuel.id', '=', $fuel);
        }
        if (!empty($trans)) {
            $alldataegt->where('vm.tranmission_id', '=', $trans);
        }
        if (!empty($milage)) {

            $milage_diffrence = explode('-', $milage);
            $frommilage = $milage_diffrence[0];
            $tomilage = $milage_diffrence[1];
            $alldataegt->whereBetween('vm.mileage', array($frommilage, $tomilage));
        }
        if (!empty($from_price) && !empty($to_price)) {

            $alldataegt->whereBetween('mm.price', array($from_price, $to_price));
        }


        if (!empty($from_year) && !empty($to_year)) {

            $alldataegt->whereBetween('vm.registry_year', array($from_year, $to_year));
        }

        if (!empty($vconditions)) {
            $alldataegt->where('mm.itemcondition', '=', $vconditions);
        }

        $alldataegt->where('mm.status', '=', "confirm")->where('bv.status', 'active')
            ->select('mm.id as adid', 'mm.adtitle', 'mm.featuredimage', 'mm.created_at', 'mm.updated_at', 'mm.published_at', 'mm.ad_boost_status',
                'mm.boost_expire_on',
                // 'vb.brand_name as brand',
                'vm.model as model', 'mm.price', 'vm.mileage', 'mm.slug',
                //'cat.category_name as category',
                'scat.sub_category_name as subcategory', 'cit.city_name as city',
                //'dist.district_name',
                'bv.views', 'bv.id as views_id', 'mbr.member_type as member_type'
            );
//        if (!empty($asc_price)) {
//            $alldataegt->orderBy('mm.price', 'ASC');
//        } elseif (!empty($desc_price)) {
//            $alldataegt->orderBy('mm.price', 'DESC');
//        } elseif (!empty($desc_date)) {
//            $alldataegt->orderBy('mm.published_at', 'DESC');
//        } else {
//            $alldataegt->orderBy('mm.published_at', 'DESC');
//        }
        //pin-to-top ads
        if ($order == 'views') {
            $alldataegt->orderBy('bv.views', 'ASC');
        } else {
            $alldataegt->inRandomOrder();
        }

        $result = $alldataegt->limit(2)
            ->get()->toArray();
        return $result;
    }

    /*
     * serach published ads 
     */

    public static function SearchPublishedAds($data)
    {
        try {
            $categorycode = !empty($data['categorycode']) ? $data['categorycode'] : '';
            $districtid = !empty($data['districtid']) ? $data['districtid'] : '';
            $cityid = !empty($data['cityid']) ? $data['cityid'] : '';
            $categoryid = !empty($data['categoryid']) ? $data['categoryid'] : '';
            $subcategoryid = !empty($data['subcategoryid']) ? $data['subcategoryid'] : '';
            $desc_date = !empty($data['sort'] == 'desc_date') ? $data['sort'] : '';
            $asc_price = !empty($data['sort'] == 'asc_price') ? $data['sort'] : '';
            $desc_price = !empty($data['sort'] == 'desc_price') ? $data['sort'] : '';
            $add_type = !empty($data['addtype']) ? $data['addtype'] : '';

            $filter_condition = !empty($data['comn_data']['filter_condition']) ? $data['comn_data']['filter_condition'] : '';
            $item_type = !empty($data['comn_data']['item_type']) ? $data['comn_data']['item_type'] : '';
            $gender = !empty($data['comn_data']['gender']) ? $data['comn_data']['gender'] : '';
            /*         * * ********************************************************************************************************************************** */
            $vbrand = !empty($data['vehi_data']['brand']) ? $data['vehi_data']['brand'] : '';
            $vtype = !empty($data['vehi_data']['type']) ? $data['vehi_data']['type'] : '';
            $fuel = !empty($data['vehi_data']['fuel']) ? $data['vehi_data']['fuel'] : '';
            $trans = !empty($data['vehi_data']['trans']) ? $data['vehi_data']['trans'] : '';
            $vconditions = !empty($data['vehi_data']['condition']) ? $data['vehi_data']['condition'] : '';
            $milage = !empty($data['vehi_data']['milage']) ? $data['vehi_data']['milage'] : '';
            $vservice = !empty($data['vehi_data']['service']) ? $data['vehi_data']['service'] : '';
            /*         * * ********************************************************************************************************************************** */
            $from_price = !empty($data['from_price']) ? $data['from_price'] : ''; //  price  from
            $to_price = !empty($data['to_price']) ? $data['to_price'] : ''; //  price  to
            $from_year = !empty($data['from_year']) ? $data['from_year'] : ''; // from year
            $to_year = !empty($data['to_year']) ? $data['to_year'] : ''; // to year

            $querydata = !empty($data['query']) && is_string($data['query']) ? trim($data['query']) : ''; //  query  value
            /*         * *********************************************************************************************************************************** */
            $filter_authenticity = !empty($data['elec_data']['filter_authenticity']) ? $data['elec_data']['filter_authenticity'] : ''; //  mobile phone   value
            $ebrand = !empty($data['elec_data']['brand']) ? $data['elec_data']['brand'] : '';
            $emodel = !empty($data['elec_data']['model']) ? $data['elec_data']['model'] : '';
            /*
             * *********************************************************************************************************************************** */

            $bath = !empty($data['prop_data']['Baths']) ? $data['prop_data']['Baths'] : '';
            $beds = !empty($data['prop_data']['Beds']) ? $data['prop_data']['Beds'] : '';
            $land_size = !empty($data['prop_data']['land_size']) ? $data['prop_data']['land_size'] : '';
            $property_size = !empty($data['prop_data']['property_size']) ? $data['prop_data']['property_size'] : '';

            $alldataegt = DB::table('alladsmasters as mm')
                ->leftJoin('allvehicleads as vm', 'mm.id', '=', 'vm.alladsmaster_id')
                ->leftJoin('allelectrnicads as em', 'mm.id', '=', 'em.alladsmaster_id')
                ->leftJoin('allpropertyads as pm', 'mm.id', '=', 'pm.alladsmaster_id')
                ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
                ->leftJoin('sub_categories as scat', 'mm.sub_category_id', '=', 'scat.id')
                ->leftJoin('location_districts as dist', 'mm.district_id', '=', 'dist.id')
                ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
                ->leftJoin('brands as vb', 'vm.brand_id', '=', 'vb.id')
                ->leftJoin('brands as eb', 'em.brand_id', '=', 'eb.id')
                ->leftJoin('vehicle_fuels as vfuel', 'vm.fuel_id', '=', 'vfuel.id')
                ->leftJoin('vehicle_condition as vcond', 'mm.itemcondition', '=', 'vcond.id')
                ->leftJoin('vehicle_types as vtype', 'vm.bodytype_id', '=', 'vtype.id')
                ->leftJoin('vehicle_transmissions as vtrans', 'vm.tranmission_id', '=', 'vtrans.id')
                ->leftJoin('alladsfilters as allfil', 'mm.id', '=', 'allfil.alladsmaster_id')
                ->leftJoin('members as mbr', 'mm.member_id', '=', 'mbr.id');
            if (!empty($querydata)) {
                $alldataegt->Where(function ($query) use ($querydata) {
                    $query->ORwhere("cat.category_name", "LIKE", "%$querydata%");
                    $query->ORwhere("scat.sub_category_name", "LIKE", "%$querydata%");
                    $query->ORwhere("mm.adtitle", "LIKE", "%$querydata%");
                    $query->ORwhere("mm.description", "LIKE", "%$querydata%");
                    $query->ORwhere("mm.price", "LIKE", "%$querydata%");
                    $query->ORwhere("dist.district_name", "LIKE", "%$querydata%");
                    $query->ORwhere("cit.city_name", "LIKE", "%$querydata%");
                    $query->ORwhere("vb.brand_name", "LIKE", "%$querydata%");
                    $query->ORwhere("vm.model", "LIKE", "%$querydata%");
                    $query->ORwhere("vm.mileage", "LIKE", "%$querydata%");
                    $query->ORwhere("vm.color", "LIKE", "%$querydata%");
                    $query->ORwhere("vm.registry_year", "LIKE", "%$querydata%");
                    $query->ORwhere("vfuel.fuel_name", "LIKE", "%$querydata%");
                    $query->ORwhere("vcond.condition_name", "LIKE", "%$querydata%");
                    $query->ORwhere("vtype.type_name", "LIKE", "%$querydata%");
                    $query->ORwhere("vtrans.transmission_name", "LIKE", "%$querydata%");
                    $query->ORwhere("eb.brand_name", "LIKE", "%$querydata%");
                });
            }

            if (!empty($vtype) && is_array($vtype)) {
                $alldataegt->Where(function ($query) use ($vtype) {
                    foreach ($vtype as $vtypeid) {
                        $query->Where('vm.bodytype_id', $vtypeid);
                    }
                });
            }
            if (!empty($vbrand) && is_array($vbrand)) {
                $alldataegt->Where(function ($query) use ($vbrand, $categorycode) {
                    if ($categorycode == 'veh') {
                        foreach ($vbrand as $brandid) {
                            $query->orWhere('vm.brand_id', $brandid);
                        }
                    }
                });
            }
            if (!empty($ebrand) && is_array($ebrand)) {
                $alldataegt->Where(function ($query) use ($ebrand, $categorycode) {
                    if ($categorycode == 'ele') {
                        foreach ($ebrand as $brandid) {
                            $query->orWhere('em.brand_id', $brandid);
                        }
                    }
                });
            }
            if (!empty($emodel)) {
                $alldataegt->where('em.model', '=', $emodel);
            }

            if (!empty($bath) && is_array($bath)) {
                $alldataegt->Where(function ($query) use ($bath) {
                    foreach ($bath as $bathid) {
                        $query->orWhere('pm.baths', $bathid);
                    }
                });
            }
            if (!empty($beds) && is_array($beds)) {
                $alldataegt->Where(function ($query) use ($beds) {
                    foreach ($beds as $bedsid) {
                        $query->orWhere('pm.beds', $bedsid);
                    }
                });
            }

            if (!empty($land_size)) {
                $alldataegt->where('pm.landsize', '=', $land_size);
            }
            if (!empty($property_size)) {
                $alldataegt->where('pm.propertysize', '=', $property_size);
            }


//    if (!empty($vservice)) {
//            $alldataegt->leftJoin('vehicle_types as vtype', 'vm.bodytype_id', '=', 'vtype.id');
//            $alldataegt->Where(function ($query) use ($vservice) {
//                foreach ($vservice as $vserviceid) {
//                    $query->Where('vm.bodytype_id', $vtypeid);
//                }
//            });
//        }

            if (!empty($gender) && is_array($gender)) {

                $alldataegt->Where(function ($query) use ($gender) {
                    foreach ($gender as $genderid) {
                        $query->orWhere('allfil.categoryfiltervalue_id', $genderid);
                    }
                });
            }
            if (!empty($districtid)) {
                $alldataegt->where('mm.district_id', '=', $districtid);
            }
            if (!empty($cityid)) {
                $alldataegt->where('mm.city_id', '=', $cityid);
            }
            if (!empty($categoryid)) {
                $alldataegt->where('mm.category_id', '=', $categoryid);
            }
            if (!empty($subcategoryid)) {
                $alldataegt->where('mm.sub_category_id', '=', $subcategoryid);
            }
            if (!empty($add_type)) {
                $alldataegt->where('mm.ad_type', '=', $add_type);
            }
            if (!empty($filter_condition)) {
                $alldataegt->where('mm.itemcondition', '=', $filter_condition);
            }

            if (!empty($item_type) && is_array($item_type)) {
                $alldataegt->Where(function ($query) use ($item_type) {
                    foreach ($item_type as $item_typeid) {
                        $query->orWhere('mm.itemtype', $item_typeid);
                    }
                });
            }
            if (!empty($filter_authenticity)) {
                $alldataegt->where('em.authenticity', '=', $filter_authenticity);
            }
            if (!empty($fuel)) {
                $alldataegt->where('vfuel.id', '=', $fuel);
            }
            if (!empty($trans)) {
                $alldataegt->where('vm.tranmission_id', '=', $trans);
            }
            if (!empty($milage)) {
                $milage_diffrence = explode('-', $milage);
                $frommilage = $milage_diffrence[0];
                $tomilage = $milage_diffrence[1];
                $alldataegt->whereBetween('vm.mileage', array($frommilage, $tomilage));
            }
            if (!empty($from_price) && !empty($to_price)) {
                $alldataegt->whereBetween('mm.price', array($from_price, $to_price));
            }


            if (!empty($from_year) && !empty($to_year)) {
                $alldataegt->whereBetween('vm.registry_year', array($from_year, $to_year));
            }

            if (!empty($vconditions)) {
                $alldataegt->where('mm.itemcondition', '=', $vconditions);
            }

            $alldataegt->where('mm.status', '=', "confirm")
                ->select('mm.id as adid', 'mm.adtitle', 'mm.featuredimage', 'mm.created_at', 'mm.updated_at', 'mm.published_at', 'mm.ad_boost_status',
                    'mm.boost_expire_on', 'vb.brand_name as brand', 'vm.model as model', 'mm.price', 'vm.mileage', 'mm.slug', 'cat.category_name as category',
                    'scat.sub_category_name as subcategory', 'cit.city_name as city', 'dist.district_name', 'mbr.member_type as member_type'
                );
            if (!empty($asc_price)) {
                $alldataegt->orderBy('mm.price', 'ASC');
            } elseif (!empty($desc_price)) {
                $alldataegt->orderBy('mm.price', 'DESC');
            } elseif (!empty($desc_date)) {
                $alldataegt->orderBy('mm.published_at', 'DESC');
            } else {
                $alldataegt->orderBy('mm.published_at', 'DESC');
            }

            $result = $alldataegt->paginate(20);

            return $result;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public static function getFristPageAllAds($data)
    {
        $querydata = !empty($data['query']) && is_string($data['query']) ? trim($data['query']) : ''; //  query  value
        $alldataegt = DB::table('alladsmasters as mm')
            ->leftJoin('allvehicleads as vm', 'mm.id', '=', 'vm.alladsmaster_id')
            ->leftJoin('sub_categories as scat', 'mm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
            ->leftJoin('members as mbr', 'mm.member_id', '=', 'mbr.id')
            ->where('mm.status', '=', "confirm");
        if (!empty($querydata)) {
            $alldataegt->leftJoin('allelectrnicads as em', 'mm.id', '=', 'em.alladsmaster_id')
                ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
                ->leftJoin('location_districts as dist', 'mm.district_id', '=', 'dist.id')
                ->leftJoin('allpropertyads as pm', 'mm.id', '=', 'pm.alladsmaster_id')
                ->leftJoin('brands as vb', 'vm.brand_id', '=', 'vb.id')
                ->leftJoin('brands as eb', 'em.brand_id', '=', 'eb.id')
                ->leftJoin('vehicle_fuels as vfuel', 'vm.fuel_id', '=', 'vfuel.id')
                ->leftJoin('vehicle_condition as vcond', 'mm.itemcondition', '=', 'vcond.id')
                ->leftJoin('vehicle_types as vtype', 'vm.bodytype_id', '=', 'vtype.id')
                ->leftJoin('vehicle_transmissions as vtrans', 'vm.tranmission_id', '=', 'vtrans.id')
                ->leftJoin('alladsfilters as allfil', 'mm.id', '=', 'allfil.alladsmaster_id')
                ->leftJoin('alljobads as jb', 'mm.id', '=', 'jb.alladsmaster_id')
                ->leftJoin('memberpremiums as mbpr', 'mm.member_id', '=', 'mbpr.member_id');
            $alldataegt->Where(function ($query) use ($querydata) {
                $query->ORwhere("cat.category_name", "LIKE", "%$querydata%");
                $query->ORwhere("scat.sub_category_name", "LIKE", "%$querydata%");
                $query->ORwhere("mm.adtitle", "LIKE", "%$querydata%");
                $query->ORwhere("mm.description", "LIKE", "%$querydata%");
                $query->ORwhere("mm.price", "LIKE", "%$querydata%");
                $query->ORwhere("dist.district_name", "LIKE", "%$querydata%");
                $query->ORwhere("cit.city_name", "LIKE", "%$querydata%");
                $query->ORwhere("vb.brand_name", "LIKE", "%$querydata%");
                $query->ORwhere("vm.model", "LIKE", "%$querydata%");
                $query->ORwhere("vm.mileage", "LIKE", "%$querydata%");
                $query->ORwhere("vm.color", "LIKE", "%$querydata%");
                $query->ORwhere("vm.registry_year", "LIKE", "%$querydata%");
                $query->ORwhere("vfuel.fuel_name", "LIKE", "%$querydata%");
                $query->ORwhere("vcond.condition_name", "LIKE", "%$querydata%");
                $query->ORwhere("vtype.type_name", "LIKE", "%$querydata%");
                $query->ORwhere("vtrans.transmission_name", "LIKE", "%$querydata%");
                $query->ORwhere("eb.brand_name", "LIKE", "%$querydata%");
                $query->ORwhere("mbpr.company_name", "LIKE", "%$querydata%");
            });
        }
        $alldataegt
            ->select('mm.id as adid', 'mm.adtitle', 'mm.featuredimage', 'mm.created_at', 'mm.updated_at', 'mm.published_at', 'mm.ad_boost_status',
                'mm.boost_expire_on', 'mm.price', 'mm.slug',
                'vm.model as model',
                'vm.mileage',
//                'cat.category_name as category',
                'scat.sub_category_name as subcategory',
                'cit.city_name as city',
                'mbr.member_type as member_type'
//                ,'dist.district_name'
            )->orderBy('mm.published_at', 'DESC');
        $result = $alldataegt->paginate(20);
        return $result;
    }

    public static function getFristPageAllAdsNoSearch()
    {
        $alldataegt = DB::table('alladsmasters as mm')
            ->leftJoin('allvehicleads as vm', 'mm.id', '=', 'vm.alladsmaster_id')
            ->leftJoin('sub_categories as scat', 'mm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
            ->leftJoin('members as mbr', 'mm.member_id', '=', 'mbr.id')
            ->where('mm.status', '=', "confirm");
        $alldataegt
            ->select('mm.id as adid', 'mm.adtitle', 'mm.featuredimage', 'mm.created_at', 'mm.updated_at', 'mm.published_at', 'mm.ad_boost_status',
                'mm.boost_expire_on', 'mm.price', 'mm.slug',
                'vm.model as model', 'vm.mileage',
                'scat.sub_category_name as subcategory',
                'cit.city_name as city',
                'mbr.member_type as member_type'
            )->orderBy('mm.published_at', 'DESC');
        $result = $alldataegt->paginate(20);
        return $result;
    }


//    --------------------get ads feed to blog--------------------
    public static function getAdsFeedToBlog()
    {
        $adFeed = DB::table('alladsmasters as mm')
            ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
            ->leftJoin('location_cities as loc', 'mm.city_id', '=', 'loc.id')
            ->leftJoin('allvehicleads as vehi', 'mm.id', '=', 'vehi.id')
            ->leftJoin('allpropertyads as prop', 'mm.id', '=', 'prop.id')
            ->whereIn('mm.category_id', [1, 2, 3])
            ->select('mm.id', 'mm.category_id', 'mm.city_id', 'vehi.mileage as mileage', 'loc.city_name as city_name',
                'cat.category_name as category_name', 'mm.adtitle', 'mm.price', 'mm.featuredimage', 'mm.slug',
                'prop.landsize as land_size', 'prop.propertysize as prop_size','mm.published_at')
            ->where('mm.status', '=', 'confirm')
            ->limit(25)
            ->orderby('mm.published_at', 'DESC')
            ->get()
            ->toArray();
        return $adFeed;
    }
//    --------------------END get ads feed to blog--------------------


//--------------------------------------api------------------------------
    public static function getAdDetailsApi($slug)
    {
        $alldataegt = DB::table('alladsmasters as mm')
            ->leftJoin('allvehicleads as vm', 'mm.id', '=', 'vm.alladsmaster_id')
            ->leftJoin('allpropertyads as pro', 'mm.id', '=', 'pro.alladsmaster_id')
            ->leftJoin('allelectrnicads as ele', 'mm.id', '=', 'ele.alladsmaster_id')
            ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
            ->leftJoin('vehicle_types as vt', 'vm.bodytype_id', '=', 'vt.id')
            ->leftJoin('vehicle_fuels as vf', 'vm.fuel_id', '=', 'vf.id')
            ->leftJoin('vehicle_transmissions as vtra', 'vm.tranmission_id', '=', 'vtra.id')
            ->leftJoin('sub_categories as scat', 'mm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_districts as dist', 'mm.district_id', '=', 'dist.id')
            ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
            ->leftJoin('brands as vb', 'vm.brand_id', '=', 'vb.id')
            ->leftJoin('brands as eb', 'ele.brand_id', '=', 'eb.id')
            ->leftJoin('categoryfiltervalues as cfv', 'ele.authenticity', '=', 'cfv.id')
            ->leftJoin('categoryfiltervalues as cfv2', 'pro.beds', '=', 'cfv2.id')
            ->leftJoin('categoryfiltervalues as cfv3', 'pro.baths', '=', 'cfv3.id')
            ->leftJoin('categoryfiltervalues as cfv4', 'mm.itemtype', '=', 'cfv4.id')
            ->leftJoin('vehicle_condition as vco', 'mm.itemcondition', '=', 'vco.id')
            ->leftJoin('members as mbr', 'mm.member_id', '=', 'mbr.id')
            ->leftJoin('memberpremiums as mpre', 'mm.member_id', '=', 'mpre.member_id')
            ->where('mm.slug', '=', $slug)
            ->select('mm.id as adid', 'mm.adtitle', 'mm.description', 'mm.featuredimage', 'mm.ad_referance', 'mm.created_at',
                'mm.updated_at', 'mm.published_at', 'mm.ad_boost_status', 'mm.contact', 'mm.member_id',
                'mm.price', 'cat.category_name as category', 'cat.category_code', 'cit.city_name as city', 'mm.sub_category_id',
                'dist.district_name', 'mm.category_id', 'scat.sub_category_code', 'scat.sub_category_name as subcategory',
                'mm.ad_type', 'mm.slug', 'mm.status', 'mm.views', 'mm.clicks',
                'vm.model as model', 'vb.brand_name as brand', 'vm.model as model', 'vb.brand_name as brand',
                'vt.type_name as bodytype_name', 'vt.image_name as bodytype_image', 'vt.image_name as bodytype_image',
                'vf.fuel_name as fuel', 'vtra.transmission_name as transmission', 'vm.enginesize as enginesize', 'vm.mileage as mileage',
                'vm.color as color', 'vm.registry_year as registry_year', 'vm.features as veh_features', 'vm.brand_id as brand_id',
                'pro.landsize as landsize', 'pro.propertysize as propertysize', 'cfv3.filtervalue as beds', 'cfv3.filtervalue as baths',
                'pro.address as address', 'pro.features as pro_features', 'vco.condition_name as condition', 'cfv4.filtervalue as itemtype',
                'eb.brand_name as ele_brand', 'ele.brand_id as ele_brand_id', 'ele.model as ele_model', 'cfv.filtervalue as authenticity', 'ele.features as ele_features',
                'mbr.first_name as first_name', 'mbr.last_name as last_name', 'mbr.member_type as member_type', 'mbr.status as member_status',
                'mpre.webtitle as webtitle', 'mpre.company_name as company_name'
            );
        $result = $alldataegt->first();
        return $result;
    }


//    Related ads by premium
    public static function relatedAdsByPremium($adid, $memberId = null)
    {
        $alldataegt = DB::table('alladsmasters as mm')
            ->leftJoin('allvehicleads as vm', 'mm.id', '=', 'vm.alladsmaster_id')
            ->leftJoin('allelectrnicads as em', 'mm.id', '=', 'em.alladsmaster_id')
            ->leftJoin('allpropertyads as pm', 'mm.id', '=', 'pm.alladsmaster_id')
            ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
            ->leftJoin('sub_categories as scat', 'mm.sub_category_id', '=', 'scat.id')
            ->leftJoin('location_districts as dist', 'mm.district_id', '=', 'dist.id')
            ->leftJoin('location_cities as cit', 'mm.city_id', '=', 'cit.id')
            ->where('mm.status', '=', "confirm");

        if ($memberId) {
            $alldataegt->where('mm.member_id', '=', "$memberId");
        }

        $result = $alldataegt->where('mm.id', '!=', "$adid")
            ->select('mm.id as adid', 'mm.adtitle', 'mm.featuredimage', 'mm.created_at', 'mm.updated_at',
                'vm.model as model', 'mm.price', 'vm.mileage', 'mm.slug', 'cat.category_name as category',
                'scat.sub_category_name as subcategory', 'cit.city_name as city', 'dist.district_name')
            ->limit(20)->orderBy('mm.updated_at', 'DESC')->get()->toArray();
        return $result;
    }
//--------------------------------------api------------------------------
}
