<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Playzone_dailyquestion extends Model
{
    protected $filable=['title','image','answer1','answer2','answer3'];
    protected $table = 'playzone_dailyquestions';
}
