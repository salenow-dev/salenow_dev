<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Allvehiclead extends Model
{
    protected $fillable = ['brand_id','model','bodytype_id','fuel_id','steering_id','tranmission_id','enginesize','mileage',
        'color','registry_year','features'];
    public $timestamps = false;
    public $last_name = false;



    public function saveData($request, $member_master_id)
    {
        $obj = new Allvehiclead($request);
        $obj->alladsmaster_id = $member_master_id;
        if(!empty($request['option'])){
            $obj->features =serialize($request['option']);
        }
        if ($obj->save()) {
            return $obj->id;
        }
    }

    public function updateData($request, $member_master_id)
    {
        $obj = Allvehiclead::where('alladsmaster_id',$member_master_id)->first();
        if(!empty($request['option'])){
            $obj->features =serialize($request['option']);
        }
        if ($obj->update($request)) {
            return $obj->id;
        }
    }
}
