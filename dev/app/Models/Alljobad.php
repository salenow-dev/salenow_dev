<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alljobad extends Model
{
    protected $fillable = ['alladsmaster_id', 'industry', 'closingdate', 'salary', 'company'];

    public function saveData($request, $member_master_id)
    {
        $obj = new Alljobad($request);
        $obj->alladsmaster_id = $member_master_id;
        if ($obj->save()) {
            return $obj->id;
        }
    }

    public function updateData($request, $member_master_id)
    {
        $obj = Alljobad::where('alladsmaster_id', $member_master_id)->first();
        if ($obj->update($request)) {
            return $obj->id;
        }

    }
}
