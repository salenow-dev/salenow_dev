<?php

namespace App\Dialogsms;
/* 
 *dialog sms getway 
 */


class Sms
{

    Public static function Sendsms($phoneno, $message, $chanel)
    {

        $accesstoken = self::GetaccessToken();
        $url = 'https://digitalreachapi.dialog.lk/camp_req.php';
        $date = date("Y-m-d");// current date 
        $date1 = date('Y-m-d', strtotime('-1 day', strtotime($date)));
        $date2 = date('Y-m-d', strtotime('+1 day', strtotime($date)));
        // DATA JASON ENCODED
        $data = array(
            "msisdn" => $phoneno,
            "channel" => $chanel,
            "mt_port" => "Saleme.lk",
            "s_time" => $date1 . " 08:00:00",
            "e_time" => $date2 . " 08:00:00",
            "msg" => $message,
            "callback_url" => "https://digitalreachapi.dialog.lk//call_back.php"
        );
        $data_json = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);


        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization:' . $accesstoken));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // DATA ARRAY
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);

        if ($response === false)
            $response = curl_error($ch);
        return ($response);

        curl_close($ch);

    }


    private static function GetaccessToken()
    {

        $url = 'https://digitalreachapi.dialog.lk/refresh_token.php';

        // DATA JASON ENCODED
        $data = array("u_name" => "salemelk", "passwd" => "Saleme@343");
        $data_json = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // DATA ARRAY
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responses = curl_exec($ch);
        $access = json_decode($responses);

        if ($responses === false)
            $responses = curl_error($ch);
        curl_close($ch);
        return !empty($access->access_token) ? $access->access_token : '';
    }

}