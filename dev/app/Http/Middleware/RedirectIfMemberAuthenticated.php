<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class RedirectIfMemberAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd(Auth::guard('member')->check());
        //dd(Auth::guard('member')->user());
        //If request comes from logged in seller, he will
        //be redirected to seller's home page.
        if (Auth::guard('member')->check()) {
            return redirect('/my_ads');
        }

        //dd('RedirectIfMemberAuthenticated Middleware');
        //If request comes from logged in user, he will
        //be redirect to home page.
//        if (Auth::guard()->check()) {
//            return redirect('/dashboard');
//        }



        return $next($request);
    }
}
