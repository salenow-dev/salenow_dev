<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Session\Store;

class Expireboostpinsession
{
    protected $session;
    protected $timeout = 1200;

    public function __construct(Store $session)
    {
        $this->session = $session;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $posts = $this->getViewedPosts();
        if (!is_null($posts)) {
            $posts = $this->cleanExpiredViews($posts);
            $this->storePosts($posts);
        }
        return $next($request);
    }

    private function getViewedPosts()
    {
        // Get all the viewed posts from the session. If no
        // entry in the session exists, default to null.
        return $this->session->get('viewed_pin_ads', null);
    }

    private function cleanExpiredViews($posts)
    {
        $nowTime = time();

        // Let the views expire after half an hour.
        $throttleTime = 1800;

        // Filter through the post array. The argument passed to the
        // function will be the value from the array, which is the
        // timestamp in our case.
        return array_filter($posts, function ($timestamp) use ($nowTime, $throttleTime) {
            // If the view timestamp + the throttle time is
            // still after the current timestamp the view
            // has not expired yet, so we want to keep it.
            $status = ($timestamp + $throttleTime) > $nowTime;
            return $status;
        });
    }

    private function storePosts($posts)
    {
        $this->session->put('viewed_pin_ads', $posts);
    }
}
