<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Models\Member;
use App\Models\Memberpremium;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use App\Dialogsms\Sms;


class PremiummanageController extends Controller
{
    public function index(Request $request)
    {

        if (isset($_POST) && !empty($request->all())) {


            $members = '';

            if ($request->search == 'searchMemAll') {
                $company_name = $request->first_name;
                $email = $request->first_name;
                $members = Memberpremium::
                where('company_name', 'LIKE', "%$company_name%")
                    ->orwhere('email', 'LIKE', "%$email%")
                    ->paginate(20);

//                foreach ($members as $member){
//                    dd($member);
//                }
            }
            return redirect()->back();

//            return view('backend.member.managepremiummember', compact('members'));
        }
    }

    public function premiumdeactive(Request $request)
    {
        if ($request->deactivate == 'deactivate') {
            $id = $request->id;

            Memberpremium::where('id', $id)
                ->update(['verified' => 0]);
            $members = Memberpremium::
            where('id', $id)
                ->paginate(20);


            $memberPremiums = Memberpremium::where('id', $id)->first();

//        dd($memberPremiums);
//        exit();

            if (count($memberPremiums->mobile)) {
                $numbers = array_slice($memberPremiums->mobile, 0, 2);
                $smsMessage = 'Your Premium Member account has been deactivated. Please Contact SaleMe Support Team for More information';
                foreach ($numbers as $number) {
                    $verifySMS = Sms::Sendsms($number, $smsMessage, '1');
                    $smsResponce = json_decode($verifySMS, true);

                }
            }


            return redirect()->back();
//            return view('backend.member.managepremiummember', compact('members'));
        }
        if ($request->activate == 'activate') {

            $id = $request->id;

            Memberpremium::where('id', $id)
                ->update(['verified' => 1]);
            $members = Memberpremium::
            where('id', $id)
                ->paginate(20);

            return view('backend.member.managepremiummember', compact('members'));
        }
    }

    public function sendmembersms()
    {

        return view('backend.member.managepremiummember');

    }
}
