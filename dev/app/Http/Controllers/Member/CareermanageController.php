<?php

namespace App\Http\Controllers\Member;

use App\Models\Candidate;
use App\Models\Career;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;


class CareermanageController extends Controller
{
    public function index()
    {
        return view('backend.others.careermanage');
    }

    public function addjob(Request $request)
    {
        $string2 = (!empty($request->description)) ? str_replace(array("\r\n", "\r", "\n"), '<br />', $request->description) : '';
        $string1 = (!empty($request->requirements)) ? str_replace(array("\r\n", "\r", "\n"), '<br />', $request->requirements) : '';
        $string = (!empty($request->benefits)) ? str_replace(array("\r\n", "\r", "\n"), '<br />', $request->benefits) : '';
        $request->merge(['description' => $string2]);
        $request->merge(['requirements' => $string1]);
        $request->merge(['benefits' => $string]);
        $data = $request->except('_token');
        DB::table('careers')->insert($data);
        flash('Success! Job Added');
        return redirect()->back();
    }
    public function careerview()
    {
        $candidatelist = Candidate::select('*')
            ->orderBy('created_at', 'asc')
            ->get();
        return view('backend.others.careerview', compact('candidatelist'));
    }
    public function careerlist()
    {
        $careerlist = Career::select('*')->get();
        return view('backend.others.careeredit', compact('careerlist'));
    }
    public function careeredit(Request $request)
    {
        $careers = Career::select('*')
            ->where('id', $request->jobno)
            ->get();
        return view('backend.others.careermodify', compact('careers'));
    }
    public function careerdelete(Request $request){
        $result = DB::table('careers')
            ->where('id',$request->jobno)
            ->delete();
        flash('Success! Job Deleted');
        return redirect('admin/member/career/edit');
    }
    public function careerupdate(Request $request)
    {
        $string1 = (!empty($request->requirements)) ? str_replace(array("\r\n", "\r", "\n"), '<br />', $request->requirements) : '';
        $string = (!empty($request->benefits)) ? str_replace(array("\r\n", "\r", "\n"), '<br />', $request->benefits) : '';
        $request->merge(['requirements' => $string1]);
        $request->merge(['benefits' => $string]);
        $data = $request->except('_token');
        DB::table('careers')
            ->where('id',$request->id)
            ->update($data);
        flash('Success! Job Updated');
        return redirect('admin/member/career/edit');
    }

}
