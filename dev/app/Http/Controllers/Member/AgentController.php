<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Models\Member;
use App\Models\Memberpremium;
use Illuminate\Http\Request;

class AgentController extends Controller
{
    public function index(Request $request)
    {
        if (isset($_POST) && !empty($request->all())) {
            $email = $request->email;
            $request->flashOnly(['email']);
            $members = '';
            if($request->search == 'searchAgents'){
                $members = Member::where('member_type','agent')->where('email', 'LIKE', "%$email%")
                    ->with('district', 'city')
                    ->paginate(5);
            }else{
                $members = Member::where('member_type','!=','premium')->where('email', 'LIKE', "%$email%")
                    ->with('district', 'city')
                    ->paginate(5);
            }
            //dd($members);
            $shopmembers = Member::pluck('first_name', 'id')->toArray();
        }
        return view('backend.member.agent.index', compact('members', 'shopmembers'));
    }

    public function assignagent(Request $request)
    {
        if (!empty($request->shop_id)) {
            $shopId = $request->shop_id;
            $agent = (!empty($request->agent_id))?$request->agent_id:'';
            //remove agent form member premiums table
            if( array_key_exists('status',$request->all()) && $request->status == 'remove'){
                //$shop = Memberpremium::find($shopId);
                $shop = Member::find($shopId);
                //dd($shop);
                $shop->agent_id = NULL;
                $shop->save();
                return response()->json(['response' => 'success']);
            } else {
                //add new agent for shop
                $member = Member::find($agent);
                if ($member->member_type == 'agent') {
                    //$shop = Memberpremium::find($shopId);
                    $shop = Member::find($shopId);
                    $oldAgent = $shop->agent_id;
                    $email = $shop->email;
                    $shop->agent_id = $agent;
                    $shop->save();
                    return response()->json(['response' => 'success', 'message' => 'Thank you! Agent Successfully assigned to ' . $shop->first_name . '.',
                        'company' => $shop->first_name,'oldAgent'=>$oldAgent,'email'=>$email]);
                } else {
                    return response()->json(['response' => 'error']);
                }
            }
        }
    }

    public function create()
    {
        return view('backend.member.agent.create', compact('member', 'shopmembers'));
    }

    public function store(Request $request)
    {
        if(!empty($request->all())){
            $id = $request->id;
            $member = Member::find($id);
            if($member){
                $member->member_type = 'agent';
                $member->save();
                return response()->json(['response' => 'success']);
            }
        }
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }
}
