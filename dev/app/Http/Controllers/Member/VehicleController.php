<?php
namespace App\Http\Controllers\Member;

use App\Classes\Fileuploader;
use App\Models\Adimage;
use App\Models\Adspam;
use App\Models\Agent;
use App\Models\Alladsmaster;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Categoryfiltervalue;
use App\Models\Member;
use App\Models\Membercontact;
use App\Models\Subcategory;
use App\Models\Tempimage;
use App\Models\Vehiclefuel;
use App\Models\Vehicletransmission;
use App\Models\Vehicletype;
use App\Notifications\AdPublished;
use App\Services\Slug;
use function GuzzleHttp\Promise\all;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Vehiclebrnad;
use App\Models\Allhoteltravelad;
use App\Models\City;
use Illuminate\Support\Facades\DB;
use App\Models\Membermaster;
use App\Models\Advertisementtype;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class VehicleController extends Controller
{
    //select cities by district   
    public function citybydistriclist(Request $request)
    {
        $districtid = $request->all()['districtid'];
        $citylist = City::CityByDistrictId($districtid);
        return response()->json(['success' => true, 'citylist' => $citylist]);
    }

    public function postad(Request $request)
    {
        if (empty(getMemberId())) {
            return view('member.login.login');
        }
        //delete old temp images associates with current member id
        $checkForPastImages = Tempimage::where('member_id', getPrivetMemberId());
        if ($checkForPastImages->count()) {
            $checkForPastImages->delete();
            $imageFile = public_path() . '/saleme/images/temp/' . getPrivetMemberId();
            File::deleteDirectory($imageFile);
        }
        // START - load filters and relevnt filter values associating with the selected sub-category
        $subCategory = Subcategory::where('sub_category_code', $request->sub_cat)->with('subcategoryfilters', 'brands')->first();
        $filtersWithValues = $subCategory->subcategoryfilters->load('categoryfiltervalues');
        $filters = array();
        foreach ($filtersWithValues as $filter) {
            $filtername = ucfirst(str_replace('-', ' ', $filter->filterref));
            $filterValues = (!empty($filter->categoryfiltervalues)) ? $filter->categoryfiltervalues : '';
            foreach ($filterValues as $filterValue) {
                $filters[$filtername][$filterValue->id] = $filterValue->filtervalue;
            }
        }
        /****************************************************************************************************************************************
         * END - load filters and relevnt filter values associating with the selected sub-category
         * load data for frontend
         ****************************************************************************************************************************************/
        $vehiclebrandlist = $subCategory->brands; //all brands assosiate with subcategory - by pivot table
        $locationdistrictslist = DB::table('location_districts')->get()->toArray();

        //get advertiesment types for filter by user clicked on frontend
        $sellAdType = Advertisementtype::where('typeref', $request->type)->select('id')->first();
        if (!$sellAdType) {
            abort(404);
        }

        $allsubcat = Category::subcatlist(); // category with  subcategory query;
        $alldata[] = array();
        $categories_array[] = array();

        foreach ($allsubcat as $sub_cat) {
            $adTypes = unserialize($sub_cat->adtype);
            if (in_array($sellAdType->id, $adTypes)) {
                $alldata[] = $sub_cat;
                $categories_array[$sub_cat->catid] = Category::CatrgorylsitNames($sub_cat->catid);
            } else {
            }
        }
        $categories = (object)$categories_array;
        $ctegory = DB::table('categories')->select('id', 'category_name', 'category_code')->where('id', '=', $request->category)->first();
        //for hidden fields values
        $basedata = array(
            'category_id' => $ctegory->id,
            'category_name' => $ctegory->category_name,
            'category_code' => $ctegory->category_code,
            'sub_cat_id' => $subCategory->id,
            'sub_cat_ref' => $subCategory->sub_category_code,
            'sub_cat_name' => $subCategory->sub_category_name,
            'ad_type' => $request->type,
        );

        $cat_code = (!empty($ctegory->category_code)) ? $ctegory->category_code : '';
        //get logged in member info
        $member = '';
        $locationcities = '';
        if (!empty(checkMember())) {
            $member = Member::where('id', getMemberId())->with('membercontacts')->first();
            $locationcities = City::where('district_id', $member->district_id)->select('id', 'city_name')->get();
        }

        if ($request->type == 'buy') {
            return view('layouts.frontend.want-to-buy')
                ->with('basedata', $basedata)
                ->with('locationdistrictslist', $locationdistrictslist)
                ->with('locationcities', $locationcities)
                ->with('categories', $categories)
                ->with('filters', $filters)
                ->with('member', $member);
        }

        if ($request->type == 'lookforrent') {
            return view('layouts.frontend.look-for-rent')
                ->with('basedata', $basedata)
                ->with('locationdistrictslist', $locationdistrictslist)
                ->with('locationcities', $locationcities)
                ->with('categories', $categories)
                ->with('filters', $filters)
                ->with('member', $member);
        }

        switch ($cat_code) {
            case 'veh':
                $info = $this::vehicle();
                return view('layouts.frontend.layout-master-sub_category')
                    ->with('basedata', $basedata)
                    ->with('locationdistrictslist', $locationdistrictslist)
                    ->with('locationcities', $locationcities)
                    ->with('vehiclebrandlist', $vehiclebrandlist)
                    ->with('vehicleconditionlist', $info['vehicleconditionlist'])
                    ->with('vehicletransmissionslist', $info['vehicletransmissionslist'])
                    ->with('vehiclefuellist', $info['vehiclefuellist'])
                    ->with('vehicleoptions', $info['vehicleoptions'])
                    ->with('vehiclebodytype', $info['vehiclebodytype'])
                    ->with('categories', $categories)
                    ->with('filters', $filters)
                    ->with('member', $member);

                break;
            case 'ele':
                $features = DB::table('electronicoptions')->select('display_name', 'id')->get()->toArray();
                return view('layouts.frontend.layout-master-sub_category')
                    ->with('basedata', $basedata)
                    ->with('locationcities', $locationcities)
                    ->with('vehiclebrandlist', $vehiclebrandlist)
                    ->with('locationdistrictslist', $locationdistrictslist)
                    ->with('features', $features)
                    ->with('categories', $categories)
                    ->with('filters', $filters)
                    ->with('member', $member);
                break;
            case 'pro':
                $features = DB::table('propertyoptions')->select('display_name', 'id')->get()->toArray();
                return view('layouts.frontend.layout-master-sub_category')
                    ->with('basedata', $basedata)
                    ->with('locationdistrictslist', $locationdistrictslist)
                    ->with('locationcities', $locationcities)
                    ->with('features', $features)
                    ->with('categories', $categories)
                    ->with('filters', $filters)
                    ->with('member', $member);
                break;
            case 'hote':
                $features = DB::table('hoteltraveloptions')
                    ->select('display_name', 'id')
                    ->where('sub_cat_id', $subCategory->id)->get()->toArray();
                return view('layouts.frontend.layout-master-sub_category')
                    ->with('basedata', $basedata)
                    ->with('locationdistrictslist', $locationdistrictslist)
                    ->with('locationcities', $locationcities)
                    ->with('features', $features)
                    ->with('categories', $categories)
                    ->with('filters', $filters)
                    ->with('member', $member);
                break;
            default:
                return view('layouts.frontend.layout-master-sub_category')
                    ->with('basedata', $basedata)
                    ->with('locationdistrictslist', $locationdistrictslist)
                    ->with('locationcities', $locationcities)
                    ->with('categories', $categories)
                    ->with('filters', $filters)
                    ->with('member', $member);
        }
    }

    private static function vehicle()
    {
        $vehicleconditionlist = DB::table('vehicle_condition')->get()->toArray();
        //$vehiclefuellist = DB::table('vehicle_fuels')->get()->toArray();
        $vehiclefuellist = Vehiclefuel::orderBy('fuel_name')->get();
        $vehicletransmissionslist = Vehicletransmission::orderBy('transmission_name')->get();
        $vehicleoptions = DB::table('vehicle_options')->select('display_name', 'id')->get()->toArray();
        //$vehiclebodytype = DB::table('vehicle_types')->select('type_name', 'id')->get()->toArray();
        $vehiclebodytype = Vehicletype::select('type_name', 'id')->orderBy('type_name')->get();
        $arr = array(
            'vehicleconditionlist' => $vehicleconditionlist,
            'vehicletransmissionslist' => $vehicletransmissionslist,
            'vehiclefuellist' => $vehiclefuellist,
            'vehicleoptions' => $vehicleoptions,
            'vehiclebodytype' => $vehiclebodytype,
        );
        return $arr;
    }

    public function postJob(Request $request)
    {
        if (empty(getMemberId())) {
            return view('member.login.login');
        }
        //delete old temp images associates with current member id
        $checkForPastImages = Tempimage::where('member_id', getPrivetMemberId());
        if ($checkForPastImages->count()) {
            $checkForPastImages->delete();
            $imageFile = public_path() . '/saleme/images/temp/' . getPrivetMemberId();
            File::deleteDirectory($imageFile);
        }
        // START - load filters and relevnt filter values associating with the selected sub-category
        $subCategory = Subcategory::where('sub_category_code', $request->sub_cat)->with('subcategoryfilters', 'brands')->first();
        (is_null($subCategory)) ? abort(404) : '';

        $filtersWithValues = $subCategory->subcategoryfilters->load('categoryfiltervalues');
        $filters = array();
        foreach ($filtersWithValues as $filter) {
            $filtername = ucfirst(str_replace('-', ' ', $filter->filterref));
            $filterValues = (!empty($filter->categoryfiltervalues)) ? $filter->categoryfiltervalues : '';
            foreach ($filterValues as $filterValue) {
                $filters[$filtername][$filterValue->id] = $filterValue->filtervalue;
            }
        }

        /****************************************************************************************************************************************
         * END - load filters and relevnt filter values associating with the selected sub-category
         * load data for frontend
         ****************************************************************************************************************************************/
        $locationdistrictslist = DB::table('location_districts')->get()->toArray();

        //get advertiesment types for filter by user clicked on frontend
        $sellAdType = Advertisementtype::where('typeref', $_GET['type'])->select('id')->first();

        if (!$sellAdType) {
            abort(404);
        }

        $allsubcat = Category::subcatlist(); // category with  subcategory query;
        $alldata[] = array();
        $categories_array[] = array();

        foreach ($allsubcat as $sub_cat) {
            $adTypes = unserialize($sub_cat->adtype);
            if (in_array($sellAdType->id, $adTypes)) {
                $alldata[] = $sub_cat;
                $categories_array[$sub_cat->catid] = Category::CatrgorylsitNames($sub_cat->catid);
            } else {
            }
        }
        $categories = (object)$categories_array;
        $ctegory = DB::table('categories')->select('id', 'category_name', 'category_code')->where('id', '=', $subCategory->category->id)->first();
        //for hidden fields values
        $basedata = array(
            'category_id' => $ctegory->id,
            'category_name' => $ctegory->category_name,
            'category_code' => $ctegory->category_code,
            'sub_cat_id' => $subCategory->id,
            'sub_cat_ref' => $subCategory->sub_category_code,
            'sub_cat_name' => $subCategory->sub_category_name,
            'ad_type' => $request->type,
        );

        $cat_code = (!empty($ctegory->category_code)) ? $ctegory->category_code : '';
        //get logged in member info
        $member = '';
        $locationcities = '';
        if (!empty(checkMember())) {
            $member = Member::where('id', getMemberId())->with('membercontacts')->first();
            $locationcities = City::where('district_id', $member->district_id)->select('id', 'city_name')->get();
        }

        return view('layouts.frontend.layout-master-job')
            ->with('basedata', $basedata)
            ->with('locationdistrictslist', $locationdistrictslist)
            ->with('locationcities', $locationcities)
            ->with('categories', $categories)
            ->with('filters', $filters)
            ->with('member', $member);
    }

    public function verifynumber(Request $request)
    {
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $adId = substr($_GET['id'], 2);
            $adInfo = Alladsmaster::where('id', $adId)->with('category', 'subcategory', 'district', 'city')->first();

            $contactArr = unserialize($adInfo->contact);
            $checkNum = array();
            if (!empty($contactArr)) {
                foreach ($contactArr as $number) {
                    if (isset($number)) {
                        $checkNum[$number] = true; //set only available contact numbers values id's for cross check in frontend checkbox
                    }
                }
            }
            $adInfo->contact = $checkNum;
            //dd($adInfo);
            //$adInfo = Alladsmaster::where('member_id', getMemberId())->where('status', 'draft')->latest()->first();
            $member = Member::with(['city' => function ($query) {
                $query->select('id', 'city_name');
            }, 'district' => function ($query) {
                $query->select('id', 'district_name');
            }])
                ->select('id', 'first_name', 'first_name', 'last_name', 'first_name', 'district_id', 'city_id', 'member_type', 'district_id', 'district_id')
                ->whereId(getMemberId())->first();

            $agent = Agent::where('member_id',$member->id)->first();
            $is_agent= false;
            if ($agent){
                $is_agent = true;
            }
            $numberCount = json_encode(count($member->membercontacts));
//            dd($numberCount);
//            $member = '';
            return view('postad.verifynumber', compact('member', 'adInfo', 'numberCount','is_agent'));
        }
    }

    public function updatedraftad(Request $request)
    {
        $ad = Alladsmaster::find($request->id);
        if ($ad->status == 'pending') {
            return redirect('reviewsuccess?ref=' . $ad->ad_referance);
        }
        $ad->contact = serialize($request['contactnumber']);
        $ad->status = 'pending';
        $ad->save();
        if (getMemberStatus() == 'active') {
            // START send EMAIL ------------------------------------------------------------
            try {
                $member = Member::where('id', getMemberId())->first();
                $newAdPost = Alladsmaster::with([
                    'member' => function ($query) {
                        $query->select('id', 'first_name');
                    }])
                    ->select('id', 'member_id', 'adtitle', 'slug', 'status')
                    ->find($ad->id);
                $member->notify(new AdPublished($newAdPost, 'update'));
            } catch (\Exception $e) {
                //
            }
            // END send EMAIL ---------------------------------------------------------------
        }
        return response()->json(['success' => true, 'referance' => $request->ad_referance, 'url' => '/reviewsuccess']);
    }

    public function vehicelpost(Request $request)
    {
        if (empty(getMemberId())) {
            return view('member.login.login');
        }
        //check for the ad edit
        if ($request->formtype == 'edit') {
            $ad = self::updatemember_ad($request);
            return $ad;
        }

        // For Ad Title Auto Create-----------------------------------------------------------
        $adTitle = '';
        $catRef = $request->sub_category_ref;
        $adTitleStatus = false;
        //------------------------------------------------------------------------------------
        //by ad type - for validation
        $validateRules = '';
        if ($request->ad_type == 'jobs') {
            $validateRules = array('description' => 'max:5000');
        } else {
            $validateRules = array('description' => 'required|max:5000');
        }
//        if ($request->ad_type == 'sell' || $request->ad_type == 'rent' || $request->ad_type == 'jobs') {
        if ($request->ad_type == 'sell') {
            if ($catRef == 'car' OR $catRef == 'bike' OR $catRef == 'van' OR $catRef == 'mobi') {
                $adTitleStatus = true;
            }
//            dd(getPrivetMemberId());
            $imageCheck = Tempimage::where('member_id', getPrivetMemberId())->get();
            if ($imageCheck->isEmpty()) {
                return response()->json(array('errors' => array('Please select valid images for your Ad!')));
            }
            //main image validate
            $featuredImageCheck = Tempimage::where('member_id', getPrivetMemberId())->where('featuredimage', '1')->get();
            if ($featuredImageCheck->isEmpty()) {
                return response()->json(array('errors' => array('featuredImage' => 'Please select Your Main Image!')));
            }
        }
        if ($request->ad_type == 'jobs') {
            $imageCheck = Tempimage::where('member_id', getPrivetMemberId())->get();
            if ($imageCheck->isEmpty() && empty($request->description)) {
                return response()->json(array('errors' => array('Please Add Job image or Job description')));
            }
        }
        if (!$adTitleStatus) {
            $validateRules['adtitle'] = 'required|max:50';
        }

        $msg = array(
            'contactnumber.required' => 'Please select at least one contact number!',
            'featuredimage.required' => 'Please select a valid image for Featured image!',
        );
        $dat = validate($request, $validateRules, $id = null, $msg);
        if (!is_null($dat)) {
            return $dat;
        }
        if (isset($request->all()['sub_category_id']) && !empty($request->all()['sub_category_id'])) {
            $membermaster = new Alladsmaster();
            //check nego-----------------------------------------------------------------------
            (isset($request->nego)) ? $request->merge(['nego' => true]) : $request->merge(['nego' => false]);

            $city = City::where('id', $request->city_id)->select('city_name')->first();
            $addtionalData = array(
                'ad_type' => $request->ad_type,
                'location' => strtolower($city->city_name),
            );
            $string = (!empty($request->description)) ? str_replace(array("\r\n", "\r", "\n"), "<br />", $request->description) : '';
            $request->merge(['description' => $string]);
            $request->merge(['price' => str_replace(',', '', $request->price)]);
            //get more filters if available - like Gender-----------------------------------------------------------------------------------------------
            $filterVals = array(
                'gender' => (!empty($request->gender)) ? $request->gender : '',
            );
            //ads master table--------------------------------------------------------------------------------------------------------------------------
            // START AD Title --------------------------------------------------------------------------------------------------------------------------
            if ($adTitleStatus) {
                $brand = Brand::where('id', $request->brand_id)->select('brand_name')->first();
                $brandname = $brand->brand_name;
                if ($brandname == 'Other') {
                    $adTitle = $request->model . ' ' . $request->registry_year;
                } else {
                    $adTitle = $brandname . ' ' . $request->model . ' ' . $request->registry_year;
                }
                if ($catRef == 'mobi') {
                    $authenticity = Categoryfiltervalue::whereId($request->authenticity)->select('filtervalue')->first();
                    $adTitle .= $authenticity->filtervalue;
                }
                $request->merge(['adtitle' => $adTitle]);
            }

            // END AD Title ----------------------------------------------------------------------------------------------------------------------------
            // START UPDATE NULL LOCATION IN MEMBER ----------------------------------------------------------------------------------------------------
            $member = Member::where('id', getMemberId())->first();
            if (is_null($member->district_id)) {
                $member->district_id = $request->district_id;
                $member->city_id = $request->city_id;
                $member->save();
            }
            // END UPDATE NULL LOCATION IN MEMBER ------------------------------------------------------------------------------------------------------
            $request->merge(['slug' => Slug::createSlug($request->adtitle, $addtionalData)]);
            $membermasterid = $membermaster->SaveMemberAdds($request->all(), $filterVals);
            $ad_ref = '';
            if (!empty($membermasterid)) {
                // Update the ad referance
                if (is_numeric($membermasterid)) {
                    $last_id = '0' . $membermasterid;
                    $date = date('y') . date('m');
                    $referance = 'ad' . $request->category_id . $request->sub_category_id . '-' . $date . '-' . $last_id;
                    $ad_ref = $membermaster->updateReferance($membermasterid, $referance);
                }
                //add photos
                $adId = $membermasterid;
                $memberId = getPrivetMemberId();
//                $memberId = getMemberId();
//                dd($request->ad_type);
                if ($request->ad_type == 'sell' || $request->ad_type == 'rent' || $request->ad_type == 'jobs') {
                    $tempImages = Tempimage::where('member_id', $memberId)->get();
                    $imageFile = public_path() . '/saleme/images/temp/' . $memberId;
                    $newPath = public_path() . '/saleme/images/uploads/' . $adId;
                    //check the main images folder by member id
                    if (!is_dir($newPath)) {
                        mkdir($newPath, 0777, true);
                    }

                    //check the thumb folder
                    if (!is_dir($newPath . '/thumb/')) {
                        mkdir($newPath . '/thumb/', 0777, true);
                    }

                    $adimages = new Adimage();
                    if (!empty($adimages)) {
                        foreach ($tempImages as $tempImage) {
                            //update main ads table with featured image name
                            if ($tempImage->featuredimage) {
                                Alladsmaster::where('id', $adId)->update(['featuredimage' => $tempImage->imagename]);
                            }
                            $arr = array(
                                'alladsmaster_id' => $adId,
                                'imagename' => $tempImage->imagename
                            );
                            $adimages->createRecord($arr);
                            //move files to ad-images
                            File::move($imageFile . '/' . $tempImage->imagename, $newPath . '/' . $tempImage->imagename);
                            File::move($imageFile . '/thumb/' . $tempImage->imagename, $newPath . '/thumb/' . $tempImage->imagename);
                            $tempImage->delete(); // delete from temp table
                        }
                        //delete directory with images
                        File::deleteDirectory($imageFile);
                    }
                }
                //switch statemnet for select the relevent table
                $querytable = '';
                $table = (!empty($request->tableswitch)) ? $request->tableswitch : '';
                switch ($table) {
                    case 'veh':
                        $querytable = 'Allvehiclead';
                        break;
                    case 'ele':
                        $querytable = 'Allelectrnicad';
                        break;
                    case 'pro':
                        $querytable = 'Allpropertyad';
                        break;
                    case 'hote':
                        $querytable = 'Allhoteltravelad';
                        break;
                    case 'job':
                        $querytable = 'Alljobad';
                        break;
                    default:
                        $querytable = '';
                }

                //dynamic data save - vehicle, electronics & property
                if (!empty($querytable)) {
                    $dynamicObj = 'App\Models\\' . $querytable;
                    $obj = new $dynamicObj();
                    $obj->saveData($request->all(), $membermasterid);
                }
                //return response()->json(['success' => true, 'referance' => $ad_ref, 'url' => '/reviewsuccess']);
                return response()->json(['success' => true, 'referance' => $ad_ref, 'url' => '/post-ad/verify-number?id=' . rand(10, 99) . $adId]);
            }
        }
    }

    private static function updatemember_ad($request)
    {
        // For Ad Title Auto Create-----------------------------------------------------------
        $adTitle = '';
        $catRef = $request->sub_category_ref;
        $adTitleStatus = false;
        //validation
        $validateRules = array(
            'description' => 'required|max:5000',
        );
        //------------------------------------------------------------------------------------
        if ($request->ad_type == 'sell') {
            if ($catRef == 'car' OR $catRef == 'bike' OR $catRef == 'van' OR $catRef == 'mobi') {
                $adTitleStatus = true;
            }
        }

        if (!$adTitleStatus) {
            $validateRules['adtitle'] = 'required|max:50';
        }
        $msg = array(//'contactnumber.required' => 'Please select at least one contact number!'
        );
        $dat = validate($request, $validateRules, $id = null, $msg);
        if (!is_null($dat)) {
            return $dat;
        }

        if (isset($request->all()['adId']) && !empty($request->all()['adId'])) {
            $adId = $request->adId;
            //check for images
            if ($request->ad_type == 'sell' || $request->ad_type == 'rent' || $request->ad_type == 'jobs') {
                $AdimageCheck = Adimage::where('alladsmaster_id', $adId)->get();
                if ($AdimageCheck->isEmpty()) {
                    $tempImgCheck = Tempimage::where('member_id', getPrivetMemberId())->get();
                    if ($tempImgCheck->isEmpty()) {
                        return response()->json(array('errors' => array('featuredImage' => 'Please select valid images for your Ad!')));
                    }
                    $featured = Tempimage::where('member_id', getPrivetMemberId())->where('featuredimage', true)->first();
                    if (is_null($featured)) {
                        return response()->json(array('errors' => array('featuredImage' => 'Please select Your Main Imagess!')));
                    }
                }
            }
            //END check for images
            //check nego-----------------------------------------------------------------------
            if (isset($request->nego)) {
                $request->merge(['nego' => true]);
            } else {
                $request->merge(['nego' => false]);
            }
            //end check nego--------------------------------------------------------------------
            $city = City::where('id', $request->city_id)->select('city_name')->first();
            $addtionalData = array(
                'ad_type' => $request->ad_type,
                'location' => strtolower($city->city_name),
            );
            $string = (!empty($request->description)) ? str_replace(array("\r\n", "\r", "\n"), "<br />", $request->description) : '';
            $request->merge(['description' => $string]);
            $request->merge(['price' => str_replace(',', '', $request->price)]);
            //get more filters if available - like Gender
            $filterVals = array(
                'gender' => (!empty($request->gender)) ? $request->gender : '',
            );
            // START AD Title --------------------------------------------------------------------------------------------------------------------------
            if ($adTitleStatus) {
                $brand = Brand::where('id', $request->brand_id)->select('brand_name')->first();
                $brandname = $brand->brand_name;
                if ($brandname == 'Other') {
                    $adTitle = $request->model . ' ' . $request->registry_year;
                } else {
                    $adTitle = $brandname . ' ' . $request->model . ' ' . $request->registry_year;
                }
                if ($catRef == 'mobi') {
                    $authenticity = Categoryfiltervalue::whereId($request->authenticity)->select('filtervalue')->first();
                    $adTitle .= $authenticity->filtervalue;
                }
                $request->merge(['adtitle' => $adTitle]);
            }

            // START UPDATE NULL LOCATION IN MEMBER ----------------------------------------------------------------------------------------------------
            $member = Member::where('id', getMemberId())->first();
            if (is_null($member->district_id)) {
                $member->district_id = $request->district_id;
                $member->city_id = $request->city_id;
                $member->save();
            }
            // END UPDATE NULL LOCATION IN MEMBER ------------------------------------------------------------------------------------------------------
            // END AD Title ----------------------------------------------------------------------------------------------------------------------------
            $request->merge(['slug' => Slug::createSlug($request->adtitle, $addtionalData, true)]);
            $membermaster = new Alladsmaster();
            $membermasterid = $membermaster->UpdateMemberAdds($request->all(), $filterVals);
            $ad_ref = '';
            if (!empty($membermasterid)) {
                //delete spam reasons if any
                $adspam = Adspam::where('alladsmaster_id', $adId)->first();
                (!empty($adspam)) ? $adspam->delete() : '';

                // START send EMAIL ------------------------------------------------------------
                if (getMemberStatus() == 'active') {
                    try {
                        $member = Member::where('id', getMemberId())->first();
                        $newAdPost = Alladsmaster::with([
                            'member' => function ($query) {
                                $query->select('id', 'first_name');
                            }])
                            ->select('id', 'member_id', 'adtitle', 'slug', 'status')
                            ->find($membermasterid);
                        $member->notify(new AdPublished($newAdPost, 'update'));
                    } catch (\Exception $e) {
                        //
                    }
                }
                // END START send EMAIL ------------------------------------------------------------
                if (is_numeric($membermasterid)) {
                    $last_id = '0' . $membermasterid;
                    $date = date('y') . date('m');
                    $referance = 'ad' . $request->category_id . $request->sub_category_id . '-' . $date . '-' . $last_id;
                    $ad_ref = $membermaster->updateReferance($membermasterid, $referance);
                }

                $adinfo = Alladsmaster::where('id', $adId)->select('ad_referance')->first();
                $ad_ref = $adinfo->ad_referance;

                //switch statemnet for select the relevent table
                $querytable = '';
                $table = (!empty($request->tableswitch)) ? $request->tableswitch : '';
                switch ($table) {
                    case 'veh':
                        $querytable = 'Allvehiclead';
                        break;
                    case 'ele':
                        $querytable = 'Allelectrnicad';
                        break;
                    case 'pro':
                        $querytable = 'Allpropertyad';
                        break;
                    case 'job':
                        $querytable = 'Alljobad';
                        break;
                    default:
                        $querytable = '';
                }

                //dynamic data save - vehicle, electronics & property
                if (!empty($querytable)) {
                    $dynamicObj = 'App\Models\\' . $querytable;
                    $obj = new $dynamicObj();
                    $obj->updateData($request->all(), $membermasterid);
                }

                //update photos
//                $memberId = getMemberId();
                $memberId = getPrivetMemberId();
                if ($request->ad_type == 'sell' || $request->ad_type == 'rent' || $request->ad_type == 'jobs') {
                    $tempImages = Tempimage::where('member_id', $memberId)->get();
                    $imageFile = public_path() . '/saleme/images/temp/' . $memberId;
                    $newPath = public_path() . '/saleme/images/uploads/' . $adId;
                    //check the main images folder by member id
                    if (!is_dir($newPath)) {
                        mkdir($newPath, 0777, true);
                    }

                    //check the thumb folder
                    if (!is_dir($newPath . '/thumb/')) {
                        mkdir($newPath . '/thumb/', 0777, true);
                    }

                    $adimages = new Adimage();
                    foreach ($tempImages as $tempImage) {
                        //update main ads table with featured image name
                        if ($tempImage->featuredimage) {
                            Alladsmaster::where('id', $adId)->update(['featuredimage' => $tempImage->imagename]);
                        }
                        $arr = array(
                            'alladsmaster_id' => $adId,
                            'imagename' => $tempImage->imagename
                        );
                        $adimages->createRecord($arr);
                        //move files to ad-images
                        File::move($imageFile . '/' . $tempImage->imagename, $newPath . '/' . $tempImage->imagename);
                        File::move($imageFile . '/thumb/' . $tempImage->imagename, $newPath . '/thumb/' . $tempImage->imagename);
                        $tempImage->delete(); // delete from temp table
                    }
                    //delete directory with images
                    File::deleteDirectory($imageFile);
                }
                return response()->json(['success' => true, 'referance' => $ad_ref, 'url' => '/post-ad/verify-number?id=' . rand(10, 99) . $adId]);
                //return response()->json(['success' => true, 'referance' => $ad_ref, 'url' => '/reviewsuccess']);
            }
        }
    }

    /*****************************************************************************************************************************************
     * START DROPZONE IMAGE upload
     *****************************************************************************************************************************************/
    public function dropzoneStore(Request $request)
    {
        try {
            /****************************************************************************************************************/
            $imgCount = 0;
            $memberId = getPrivetMemberId();
            if (!empty($request->adId)) {
                $checkForImages = Adimage::where('alladsmaster_id', $request->adId);
                $imgCount = $checkForImages->count();
            }
            $checkForPastImages = Tempimage::where('member_id', $memberId)->get();
            $imgCountAll = count($checkForPastImages) + $imgCount;
            if ($imgCountAll >= 5) {
                return response()->json(['errors' => 'Maximum 5 images are allowed!']);
            }
            $imgPath = public_path('saleme/images/temp/' . $memberId);
            $thumbPath = public_path('saleme/images/temp/' . $memberId . '/thumb');
            $imagename = 'saleme_' . uniqid();

            //check the main images folder by member id
            if (!is_dir($imgPath)) {
                mkdir($imgPath, 0777, true);
                mkdir($thumbPath, 0777, true);
            }

            $watermark = public_path('images/saleme/watermark.png');
            $file = Input::file('file');
            $ext = '.' . $file->getClientOriginalExtension();
            $img = Image::make($file)->orientate()->resize(640, 480, function ($constraint) {
                $constraint->aspectRatio();
            })->resizeCanvas(640, 480, 'center', false, '#ecf0f1');
            $img->insert($watermark, 'center');
            $img->interlace();
            $img->save($imgPath . '/' . $imagename . $ext);

            //Thumb
            $thmb = Image::make($file)->orientate()->resize(200, 160, function ($constraint) {
                $constraint->aspectRatio();
            })->resizeCanvas(200, 160, 'center', false, '#ecf0f1');
            $thmb->interlace();
            $thmb->save($thumbPath . '/' . $imagename . $ext, 50);
            /****************************************************************************************************************/
            $arr = array(
                'member_id' => $memberId,
                'imagename' => $imagename . $ext
            );
            $tempImages = new Tempimage();
            $setFeatured = (!empty($request->adId)) ? false : true;
            $id = $tempImages->saveNewTempImages($arr, $checkForPastImages, $setFeatured);
            $path = asset('') . 'saleme/images/temp/' . $memberId . '/thumb/' . $imagename . $ext;
            return response()->json(['success' => 'success', 'image' => $path, 'lastId' => $id, 'errors' => '']);

        } catch (Exception $e) {
            return response()->json(['errors' => $e->getMessage()]);
        }
    }


    public function dropzoneStoreJob(Request $request)
    {
        try {

            /****************************************************************************************************************/
            $imgCount = 0;
            $memberId = getPrivetMemberId();
            if (!empty($request->adId)) {
                $checkForImages = Adimage::where('alladsmaster_id', $request->adId);
                $imgCount = $checkForImages->count();
            }
            $checkForPastImages = Tempimage::where('member_id', $memberId)->get();
            $imgCountAll = count($checkForPastImages) + $imgCount;
            if ($imgCountAll >= 1) {
                return response()->json(['errors' => 'Only One Job image Allowed']);
            }
            $imgPath = public_path('saleme/images/temp/' . $memberId);
            $thumbPath = public_path('saleme/images/temp/' . $memberId . '/thumb');
            $imagename = 'saleme_' . uniqid();

            //check the main images folder by member id
            if (!is_dir($imgPath)) {
                mkdir($imgPath, 0777, true);
                mkdir($thumbPath, 0777, true);
            }

            $watermark = public_path('images/saleme/watermark-job.png');
            $file = Input::file('file');
            $ext = '.' . $file->getClientOriginalExtension();
            $img = Image::make($file)->orientate()->resize(640, null, function ($constraint) {
                $constraint->aspectRatio();
            })->resizeCanvas(640, null, 'center', false, '#ecf0f1');

            $img->insert($watermark, 'center');
            $img->interlace();
            $img->save($imgPath . '/' . $imagename . $ext);

            //Thumb
            $thmb = Image::make($file)->orientate()->resize(200, 160, function ($constraint) {
                $constraint->aspectRatio();
            })->resizeCanvas(200, 160, 'center', false, '#ecf0f1');
            $thmb->interlace();
            $thmb->save($thumbPath . '/' . $imagename . $ext, 50);
            /****************************************************************************************************************/
            $arr = array(
                'member_id' => $memberId,
                'imagename' => $imagename . $ext
            );
            $tempImages = new Tempimage();
            $setFeatured = (!empty($request->adId)) ? false : true;
            $id = $tempImages->saveNewTempImages($arr, $checkForPastImages, $setFeatured);
            $path = asset('') . 'saleme/images/temp/' . $memberId . '/thumb/' . $imagename . $ext;
            return response()->json(['success' => 'success', 'image' => $path, 'lastId' => $id, 'errors' => '']);

        } catch (Exception $e) {
            return response()->json(['errors' => $e->getMessage()]);
        }
    }

    public function tempfeaturedselect(Request $request, $setFeaturedId)
    {
        $image = Tempimage::find($setFeaturedId);
        if (is_null($image)) {
            //edit get image from the adimages table. so the id will change
            $image = Adimage::find($setFeaturedId);
            $allAdsRecord = Alladsmaster::find($image->alladsmaster_id);
            $allAdsRecord->featuredimage = $image->imagename;
            $allAdsRecord->save();
            return response()->json(['message' => 'success']);
        }
        $memberId = $image->member_id;
        $existFeatured = Tempimage::where('member_id', $memberId)->where('featuredimage', true)->first();

        if (!is_null($existFeatured)) {
            $existFeatured->featuredimage = false;
            $existFeatured->save();
        }
        $image->featuredimage = true;
        $image->save();
        return response()->json(['message' => 'success']);
    }

    public function tempDelete(Request $request, Tempimage $tempimage)
    {
        if (!is_null($tempimage)) {
            $image = $tempimage;
            $tempimage->delete();
            //delete directory with images
            $imageFile = public_path() . '/saleme/images/temp/' . $image->member_id . '/' . $image->imagename;
            $thumbFile = public_path() . '/saleme/images/temp/' . $image->member_id . '/thumb/' . $image->imagename;
            (File::exists($imageFile)) ? File::delete($imageFile) : '';
            (File::exists($thumbFile)) ? File::delete($thumbFile) : '';
            return response()->json(['message' => 'success']);
        }
    }

    public function deleteFromAdImages(Request $request, Adimage $tempimage)
    {
        if (!is_null($tempimage)) {
            $masterId = $tempimage->alladsmaster_id;
            $imageName = $tempimage->imagename;

            $checkIfFeatured = Alladsmaster::whereId($masterId)->first();
            if ($imageName == $checkIfFeatured->featuredimage) {
                $checkIfFeatured->featuredimage = "";
                $checkIfFeatured->save();
            }

            $id = $tempimage->id;
            $image = $tempimage;
            $tempimage->delete();
            //delete directory with images
            $imageFile = public_path() . '/saleme/images/uploads/' . $masterId . '/' . $image->imagename;
            $thumbFile = public_path() . '/saleme/images/uploads/' . $masterId . '/thumb/' . $image->imagename;
            (File::exists($imageFile)) ? File::delete($imageFile) : '';
            (File::exists($thumbFile)) ? File::delete($thumbFile) : '';
            return response()->json(['message' => 'success', 'id' => $id]);
        }
    }

    /*****************************************************************************************************************************************
     * END DROPZONE IMAGE upload
     *****************************************************************************************************************************************/

    public function reviewsuccess()
    {
        try {
            $ref = Input::get('ref');
            $output = substr($ref, strrpos($ref, '-') + 1);
            $membermasterid = substr($output, 1);
            // START send EMAIL ------------------------------------------------------------
            $member = Member::where('id', getMemberId())->first();
            $newAdPost = Alladsmaster::with([
                'member' => function ($query) {
                    $query->select('id', 'first_name');
                }])
                ->select('id', 'member_id', 'adtitle', 'slug', 'status', 'category_id', 'sub_category_id', 'district_id',
                    'city_id', 'featuredimage', 'description', 'price')
                ->with('category', 'subcategory', 'city', 'district')
                ->find($membermasterid);
//            dd($newAdPost);
            if (self::slack($newAdPost)) {

            }
//            $member->notify(new AdPublished($newAdPost));
            // END send EMAIL ---------------------------------------------------------------
        } catch (\Exception $e) {
            //
        }
        return view('member.review');
    }

//    slack
    public static function slack($ad_data)
    {
        $ad_intro = $ad_data->member->first_name . ' Posted ad On SaleMe.lk';
        $ad_title = $ad_data->adtitle;
        $ad_description = strip_tags($ad_data->description);
        $ad_image = "https://saleme.lk/saleme/images/uploads/$ad_data->id/thumb/$ad_data->featuredimage";
        $ad_slug = "https://saleme.lk/$ad_data->slug";

        $channel = "#ads_on_saleme";
        $icon = ':saleme:';
        $bot_name = 'SaleMe.lk Ads Posting API';
        $webhook = 'https://hooks.slack.com/services/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';

        $attachments = array([
            'fallback' => 'fallback',
            'title' => $ad_title,
            'title_link' => $ad_slug,
            'pretext' => $ad_description,
            'color' => '#fea502',
            "footer" => "Via Saleme Ad Posting API",
            "ts" => time(),
            "footer_icon" => "https://saleme.lk/images/saleme/favicon-16x16.png",
            "image_url" => $ad_image,
            'fields' => array(
                [
                    'title' => 'Price',
                    'value' => 'Rs.' . $ad_data->price,
                    'short' => true
                ],
                [
                    'title' => 'Category',
                    'value' => $ad_data->category->category_name . ' > ' . $ad_data->subcategory->sub_category_name,
                    'short' => true
                ],
                [
                    'title' => 'Location',
                    'value' => $ad_data->district->district_name . ' > ' . $ad_data->city->city_name,
                    'short' => true
                ]
            )
        ]);
        $data = array(
            'channel' => $channel,
            'username' => $bot_name,
            'text' => $ad_intro,
            'icon_emoji' => $icon,
            'attachments' => $attachments
        );
        $data_string = "payload=" . json_encode($data);
        $ch = curl_init($webhook);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Execute CURL
//        dd(curl_exec($ch));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;

    }
}
