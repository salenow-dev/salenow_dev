<?php

namespace App\Http\Controllers\Member;

use App\Models\Membercontact;
use App\Models\Paymentdetail;
use Illuminate\Support\Facades\Redirect;
use PDF;
use Illuminate\Support\Facades\DB;
use App\Classes\UUID;
use App\Http\Controllers\Controller;
use App\Models\Alladsmaster;
use App\Models\Boostpack;
use App\Models\Boostviewcount;
use App\Models\Common;
use App\Models\Membervoucher;
use App\Models\Membervouchercode;
use App\Models\Subcategory;
use App\Notifications\PintotopRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

//use PDF;


class PintotopController extends Controller
{
    public function boostad($slug, $boosttype)
    {

        $info = Alladsmaster::where('slug', $slug)
            ->select('id', 'slug', 'ad_referance', 'member_id', 'category_id', 'sub_category_id','city_id')
            ->with('member', 'category','city')
            ->first();
        $member_contacts = Membercontact::select('contactnumber')->where('member_id',$info->member_id)->first();
        $adPacks = Boostpack::where('packtype', 'single')->where('referance', $boosttype)->select('price', 'validitymonths')->first();
//dd($info);
        $subCategory = Subcategory::where('id', $info->sub_category_id)->select('sub_category_name')->first();
        $memberVouchers = new Membervoucher();
        $voucherCodes = $memberVouchers->getAllActiveVouchersByMember(getMemberId(), $boosttype); //get activated boots options

        //check for pending pin-to-top ads
        $chkInactiveVouchers = $memberVouchers->checkInactiveVouchersByMember(getMemberId(), $boosttype);
        $chkInactiveVouchers = Common::object_to_array($chkInactiveVouchers);
        $checkPendinBoostForReleventAd = Membervouchercode::where('ad_referance', $info->ad_referance)
            ->where('status', false)->first();
//        dd($checkPendinBoostForReleventAd);

        //check for current ad already pinned
        $pinStatus = Boostviewcount::where('alladsmaster_id', $info->id)->where('status', 'active')->first();
//        dd($info);
        return view('member.boosting.pin-to-top', compact('info', 'boosttype', 'checkPendinBoostForReleventAd', 'voucherCodes', 'subCategory', 'chkInactiveVouchers', 'pinStatus', 'adPacks','member_contacts'));
    }

    public function boostpintotopreqest(Request $request)
    {
//        dd($request->all());
        //create new boosting
        $voucher = new Membervoucher();
        $voucherInvoiceCode = self::genereateBoostInvoiceNo(); //GENERATE INVOICE CODE FOR ADS PACK
        $request->merge(['boostpackinvoiceno' => $voucherInvoiceCode]);
        $returnURL = '';
        $email = true;

        if (!empty($request->ad_referance)) {
            //if ad_referance is exist any member can pin their ad.
            if (!empty($request->vouchercode) && !empty($request->member_id)) {
                //use the existing voucher code for pin ad to top - AdsreviewController - activateboostpintotopsingle action
                //directly activate the ad by member - if any previous boost options exist
                $voucherCodeId = $request->vouchercode;
                $updatecheck = Membervouchercode::where('id', $voucherCodeId)
                    ->where('boosttype', $request->boosttype)
                    ->update([
                        'ad_referance' => $request->ad_referance,
                        'status' => true,
                        'activated_at' => Carbon::now()
                    ]);
                if ($updatecheck) {
                    Boostviewcount::create([
                        'alladsmaster_id' => $request->adid
                    ]);
                    $ad = Alladsmaster::where('ad_referance', $request->ad_referance)
                        ->update([
                            'ad_boost_status' => $request->boosttype,
                            'boost_expire_on' => Carbon::now()->addDays(7)->toDateTimeString()
                        ]);
                }
                //send boost success email
                $member = getMember();
                $ad = Alladsmaster::find($request->adid);
                $member->notify(new PintotopRequest($ad));
                $email = false;
                flash('Success! Your Ad Pinned Successfully! It will be Removed from Pin-to-Top List after 7 days.', 'success');
                $returnURL = '/myprofile';
            } else {
                //Request a pin-to-top ad by member private
                $code = new UUID('voucher' . $request->adid . '_');
                $checkVoucherCode = Membervouchercode::where('vouchercode', $code)->first();
                if (empty($checkVoucherCode)) {
                    $arr[$request->boosttype] = $code->uuid;
                    $voucher->newVoucherForAd($request->all(), $arr);
                    flash('Success! Your Ad Boost Request is Successfull. Confirm your payment to Activate Automatically.', 'success');
                    $returnURL = '/my_ads';
                }
            }
        } else {
//            if (getMemberType() != 'private') {
            if (isPremium()) {
                //for premium members - they can buy boosting pack as well as boots a single ad
                //private member cannot have an ads boosting pack
                if ($request->ads_pack == 'true') {
                    $pin_count = $request->pintotop_count;
                    $bump_count = $request->bump_up_count;
                    $voucherCodes['pin-to-top'] = self::generateVoucherCodes($pin_count, getMemberId());
                    //$voucherCodes['bump-up'] = self::generateVoucherCodes($bump_count, getMemberId());
                    $voucher->newVoucherForPacks($request->all(), $voucherCodes);
                    flash('Success! Your Ad Boost Pack Request is Successfull! Confirm your payment to Activate Automatically.', 'success');
                    $returnURL = '/myprofile';
                }
            }
        }

        //pintotop email start
        if ($email) {
            if ($request->adid) {
                $mail = Alladsmaster::with('member')->where('id', $request->adid)->first();
            } else {
                $mail = Boostpack::find($request->boostpack_id);
            }
            $member = getMember();
            $member->notify(new PintotopRequest($mail, $request->price));
        }
        return redirect($returnURL);
    }

    protected static function generateVoucherCodes($count, $adId)
    {
        $voucherCodes = array();
        while (count($voucherCodes) < $count) {
            $code = new UUID('voucher' . $adId . '_');
            $checkVoucherCode = Membervouchercode::where('vouchercode', $code)->first();
            if (empty($checkVoucherCode)) {
                $voucherCodes[] = $code->uuid;
            }
        }
        return $voucherCodes;
    }

    protected static function genereateBoostInvoiceNo()
    {
        $string = 'SLM' . getMemberId() . '-' . date('ymd');
        $code = $string . '-' . substr(md5(microtime()), 0, 4);
        $checkBoostVoucherInvoiceCode = Membervoucher::where('boostpackinvoiceno', $code)->first();

        $unique = (empty($checkBoostVoucherInvoiceCode)) ? true : false;
        while (!$unique) {
            $code = $string . '-' . str_random(4); //GENERATE INVOICE CODE FOR ADS PACK
            $checkBoostVoucherInvoiceCode = Membervoucher::where('boostpackinvoiceno', $code)->first();
            if (empty($checkBoostVoucherInvoiceCode)) {
                $unique = true;
            }
        }
        return $code;
    }
    public function pintotopboostpack($slug)
    {

        $ad = Alladsmaster::where('slug', $slug)->select('id', 'slug', 'ad_referance', 'member_id')->with('member')->first();
        $info = $ad;
        return view('member.boosting.packs', compact('info'));
    }


    public function paymentsuccess(Request $request)
    {


    }
    public function payment_detals(Request $request)
    {

        $payment = new Paymentdetail($request->all());
        $result = $payment->save();
        if ($result) {
            return response()->json(['success' => true]);
        } else {
            return response()->json(['success' => false]);
        }
    }
    public function printpdf()
    {
        $pdf = PDF::loadView('member.boosting.invoice');
        return $pdf->download('payment.pdf');
    }
}
