<?php

namespace App\Http\Controllers\Member;

use App\Dialogsms\Sms;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class SmsController extends Controller
{
  public function index(){

 return view('member.sms');
  }
  public function sendmesseage(Request $request){

      $number = $request->number;
      $smsMessage = $request->message;
      $verifySMS = Sms::Sendsms($number, $smsMessage, '9');
      $smsResponce = json_decode($verifySMS, true);

      if($smsResponce['error'] == 0 OR $smsResponce['error'] == '')
      {
          flash('SMS sent Successfuly.', 'success');
      }else
      {
          flash($smsResponce['error'], 'error');
      }
      return back();
  }
}
