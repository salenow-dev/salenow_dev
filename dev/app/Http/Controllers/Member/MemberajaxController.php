<?php

namespace App\Http\Controllers\Member;

use App\Dialogsms\Sms;
use App\Models\Adimage;
use App\Models\Alladsfilter;
use App\Models\Categoryfiltervalue;
use App\Models\Electronicoption;
use App\Models\Membercontact;
use App\Models\Advertisementtype;
use App\Models\Otp;
use App\Models\Vehiclecondition;
use App\Models\Vehicleoption;
use App\Notifications\AdInquiry;
use App\Notifications\MyNotifiable;
use App\Notifications\SendToFriend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Member;
use App\Models\Category;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Session;
use App\Social\Fbsocial;
use App\Social\Googlesocial;
use Illuminate\Support\Facades\Redirect;
use App\Models\Alladsmaster;

//use App\User;


class MemberajaxController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*
     * delete memeber mobile no 
     */
    public function deletecontactno(Request $request)
    {
        if (!empty($request->uid)) {
            $id = $request->uid;
            $memeberconact = Membercontact::find($id);
            $memeberconact->delete();
            return json_encode(array('success' => true, 'response' => 'contact number successfully deleted'));
        }
    }

    /*
     * delete memeber email address 
     */

    public function deleteemailaddress(Request $request)
    {
        if (!empty($request->uid)) {
            $id = $request->uid;
            $memeber = Member::find($id);
            $memeber->mobile = 'null';
            $memeber->save();
            return json_encode(array('success' => true, 'response' => 'Email address update'));
        }
    }

    public function memberupdate(Request $request)
    {
        $memberid = getMemberId();
        //  create member 
        $member = Member::find($memberid);
        $member->first_name = $request->first_name;
        $member->last_name = $request->last_name;
        $member->sex = $request->gender;
        $member->district_id = $request->district_id;
        $member->city_id = $request->city_id;
        $member->save();
        return json_encode(array('success' => true, 'response' => 'User Details Updated Successfully !'));
    }

    /*
     * chenge   password 
     */

    public function memberpassword(Request $request)
    {
        $memberid = getMemberId();
        // check old password 
        $member = Member::where('password', sha1($request->old_password))->first();
        if (!empty($member)) {
            if ($request->old_password != $request->new_password) {
                //  create member 
                $member = Member::find($memberid);
                $member->password = sha1($request->new_password);
                $member->save();
                return json_encode(array('success' => true, 'response' => 'Password Updated Successfully !'));
            } else {
                return json_encode(array('success' => false, 'response' => 'Please enter another new password '));
            }
        } else {
            return json_encode(array('success' => false, 'response' => 'Please check a your old password'));
        }
    }

    public function deleteimages(Request $request)
    {
        //delete only specific image
        if (!empty($request->id)) {
            $imageId = $request->id;
            if ($request->type == 'featured') {
                $ad = Alladsmaster::find($imageId);
                $img = $ad->featuredimage;
                $ad->featuredimage = '';
                $ad->save();

                $adimgesTbl = Adimage::where(['alladsmaster_id' => $imageId, 'imagename' => $img]);
                if (!empty($adimgesTbl)) {
                    $adimgesTbl->delete();
                }
                $filePath = public_path() . '/saleme/images/uploads/' . $ad->id . '/';
                if (File::exists($filePath . $img)) File::delete($filePath . $img); //delete main image
                if (File::exists($filePath . 'thumb/' . $img)) File::delete($filePath . 'thumb/' . $img); //delete thumb
                return response()->json(['message' => 'success',]);
            }

            $images = Adimage::find($imageId);
            if (!empty($images)) {
                $mainImage = $images->imagename;
                $filePath = public_path() . '/saleme/images/uploads/' . $images->alladsmaster_id . '/';
                $images->delete();
                if (File::exists($filePath . $mainImage)) File::delete($filePath . $mainImage); //delete main image
                if (File::exists($filePath . 'thumb/' . $mainImage)) File::delete($filePath . 'thumb/' . $mainImage); //delete thumb
                return response()->json(['message' => 'success',]);
            }
        }
    }

    public function deleteotpnumber(Otp $otp)
    {
        if ($otp) {
            $status = $otp->delete();
            if ($status) {
                return response()->json(['message' => 'success']);
            }
        }
        return response()->json(['message' => 'error']);
    }

    public function verifynumbermember(Request $request)
    {
        $memberId = $request->member_id;
        $otp = $request->number_verify;
        $otpRecord = Otp::where('member_id', $memberId)->where('code', $otp)->first();
        if ($otpRecord) {
            $newNumber = $otpRecord->number;
            $contactinfo = array(
                'member_id' => getMemberId(),
                'contactnumber' => $newNumber,
            );

            $newContact = new Membercontact($contactinfo);
            $statusNew = $newContact->save(); //save new contact for member
            ($statusNew) ? $otpRecord->delete() : ''; //delete otp record
            return response()->json(['response' => array(
                'message' => 'verified',
                'number' => $newNumber,
            )]);

        } else {
            return response()->json(['response' => array(
                'error' => 'OTP is Wrong. Please Check the code and enter Again',
            )]);
        }
    }

    public function sendtofriend(Request $request)
    {
        $features = array();
        $generalinfo = array();
        $ad_info = '';
        $cat_code = $request->category_code;
        $ad_id = $request->adid;
        $mail = $request->to_email;
        $from = $request->sender_name;
        $msg = $request->message;
        switch ($cat_code) {
            case 'veh':
                $ad_info = Alladsmaster::GetVehidatabyid($ad_id);
                //start value added features
                $featuresids = Alladsmaster::GetVehifeaturesbyid($ad_id);
                $optionsids = unserialize($featuresids->features);
                if (!empty($optionsids)) {
                    foreach ($optionsids as $ids) {
                        $dispal = Vehicleoption::OptionsGetById($ids);
                        if (!empty($dispal->display_name)) {
                            $features[] = $dispal->display_name;
                        }
                    }
                }
                $generalinfo = array(
                    'Brand' => (!empty($ad_info->brand_name)) ? $ad_info->brand_name : '',
                    'Model' => (!empty($ad_info->model)) ? $ad_info->model : '',
                    'Model Year' => (!empty($ad_info->registry_year)) ? $ad_info->registry_year : '',
                    'Transmission' => (!empty($ad_info->transmission_name)) ? $ad_info->transmission_name : '',
                    'Fuel Type' => (!empty($ad_info->fuel_name)) ? $ad_info->fuel_name : '',
                    'Engine capacity' => (!empty($ad_info->enginesize)) ? $ad_info->enginesize.' CC' : '',
                    'Body Type' => (!empty($ad_info->bodytype)) ? $ad_info->bodytype : '',
                    'Kms Driven' => (!empty($ad_info->mileage)) ? $ad_info->mileage : '',
                    'Average Mileage' => (!empty($ad_info->mileage)) ? $ad_info->mileage : '',
                );
                break;
            case 'ele':
                $ad_info = Alladsmaster::GetElecdatabyid($ad_id);
                //start value added features
                $optionsids = unserialize($ad_info->features);
                if (!empty($optionsids)) {
                    foreach ($optionsids as $ids) {
                        $dispal = '';
                        $dispal = Electronicoption::where('id', $ids)->select('display_name', 'option_name', 'id')->first();
                        if (!empty($dispal->display_name)) {
                            $features[] = $dispal->display_name;
                        }
                    }
                }
                if (!empty($ad_info->itemtype)) {
                    $itemtype = Categoryfiltervalue::where('id', $ad_info->itemtype)->select('filtervalue')->first();
                }
                if (!empty($ad_info->authenticity)) {
                    $authenticity = Categoryfiltervalue::where('id', $ad_info->authenticity)->select('filtervalue')->first();
                }
                $generalinfo = array(
                    'Device Type' => (!empty($itemtype->filtervalue)) ? $itemtype->filtervalue : '',
                    'Brand' => (!empty($ad_info->brand_name)) ? $ad_info->brand_name : '',
                    'Model' => (!empty($ad_info->model)) ? $ad_info->model : '',
                    'Authenticity' => (!empty($authenticity->filtervalue)) ? $authenticity->filtervalue : '',
                );
                break;
            case 'pro':
                $ad_info = Alladsmaster::GetPropertybyid($ad_id);
                if (!empty($ad_info->itemtype)) {
                    $itemtype = Categoryfiltervalue::where('id', $ad_info->itemtype)->select('filtervalue')->first();
                }
                //load beds
                if (!empty($ad_info->beds)) {
                    $beds = Categoryfiltervalue::where('id', $ad_info->beds)->select('filtervalue')->first();
                }

                //load baths
                if (!empty($ad_info->baths)) {
                    $baths = Categoryfiltervalue::where('id', $ad_info->baths)->select('filtervalue')->first();
                }
                $generalinfo = array(
                    'Address' => (!empty($ad_info->address)) ? $ad_info->address : '',
                    'Type' => (!empty($itemtype->filtervalue)) ? $itemtype->filtervalue : '',
                    'Bedrooms' => (!empty($beds->filtervalue)) ? $beds->filtervalue : '',
                    'Bathrooms' => (!empty($baths->filtervalue)) ? $baths->filtervalue : '',
                    'Land Size' => (!empty($ad_info->landsize)) ? $ad_info->landsize : '',
                    'Property Size' => (!empty($ad_info->propertysize)) ? $ad_info->propertysize : '',
                );
                break;
            default:
                $ad_info = Alladsmaster::GetAllAdMasterSingleFrontend($ad_id);
                if (!empty($ad_info->itemtype)) {
                    $itemtype = Categoryfiltervalue::where('id', $ad_info->itemtype)->select('filtervalue')->first();
                }
                $generalinfo = array(
                    'Type' => (!empty($itemtype->filtervalue)) ? $itemtype->filtervalue : '',
                );
        }
        $adImages = Adimage::where('alladsmaster_id',$ad_id)->select('imagename')->get();
        $allFilters = Alladsfilter::where('alladsmaster_id', $ad_id)->select('filtername', 'categoryfiltervalue_id')->get();
        foreach ($allFilters as $key => $allFilter) {
            $data = Categoryfiltervalue::find($allFilter->categoryfiltervalue_id);
            $generalinfo[ucfirst($allFilter->filtername)] = $data->filtervalue;
        }

        //common
        $itemtype = '';
        $itemcondition = '';

        if (!empty($ad_info->itemtype)) {
            $itemtype = Categoryfiltervalue::where('id', $ad_info->itemtype)->select('filtervalue')->first();
        }
        if ($cat_code == 'veh' && !empty($ad_info->itemcondition)) {
            $conditionInfo = Vehiclecondition::where('id', $ad_info->itemcondition)->select('condition_name')->first();
            $itemcondition = (!empty($conditionInfo->condition_name))?$conditionInfo->condition_name:'';
        }else{
            $filterVal = Categoryfiltervalue::getFilterValue($ad_info->itemcondition);
            if(!empty($filterVal)) {
                $itemcondition = (!empty($filterVal->filtervalue))?$filterVal->filtervalue:'';
            }
        }
//        $spec = array(
//            'Brand' => $ad_info->brand_name,
//            'Model' => $ad_info->model,
//            'Transmission' => $ad_info->transmission_name,
//            'Register Year' => $ad_info->registry_year,
//            'Engine Size' => $ad_info->enginesize,
//            'Fuel' => $ad_info->fuel_name,
//            'Mileage' => $ad_info->mileage,
//            'Body' => $ad_info->bodytype,
//        );
//        $spec['Condition'] = $itemcondition;

        $ad_info->features = $features;
        $ad_info->specification = $generalinfo;
        $ad_info->ad_images = $adImages;
        //dd($ad_info);
        $status = (new Member)->forceFill([
            'first_name' => $from,
            'email' => $mail,
        ])->notify(new SendToFriend($ad_info,$from,$msg));
        return response()->json(['response' => array(
            'message' => 'success',
        )]);
//        $member->notify(new SendToFriend($ad_info));

    }

}
