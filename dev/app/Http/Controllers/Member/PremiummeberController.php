<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Models\Alladsmaster;
use App\Models\Boostpack;
use App\Models\Category;
use App\Models\Member;
use App\Models\Membercontact;
use App\Models\Memberemployee;
use App\Models\Membergroup;
use App\Models\Memberpremium;
use App\Models\Premiumgaleryimage;
use App\Models\Premiumservice;
use App\Notifications\SendEmailInquiry;
use App\Services\Slug;
use Faker\Provider\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class PremiummeberController extends Controller
{
    //Become Premium Member view  
    public function create()
    {
        $categorylist = Category::pluck('category_name', 'id')->toArray();
        return view('member.premium.become-premium', compact('categorylist'));

    }

    // Request NEW premium member
    public function store(Request $request)
    {
//        dd($request->all());
        $request->flash();
        $msg = array(
            'mobile.*' => 'Invalid mobile number.',
            'telephone.*' => 'Invalid fixed phone number.'
        );
        $this->validate($request, [
            'company_name' => 'required',
            //'category_id' => 'required',
//            'address' => 'required',
            //'website' => 'required',
            'email' => 'email',
//            'description' => 'required',
            'telephone.*' => 'regex:/[0-9]/|max:10',
            'mobile.*' => 'regex:/[0-9]/|max:10',
        ], $msg);

        //contact numbers
        $telephone = array_filter($request->telephone);
        $mobile = array_filter($request->mobile);
        if (!empty($telephone)) {
            foreach ($telephone as $key => $tele) {
                if (!empty($tele)) {
                    if (preg_match("~^0\d+$~", $tele)) {
                        $telephone[$key] = "94" . substr($tele, 1);
                    } else {
                        $telephone[$key] = "94" . $tele;
                    }
                }
            }
        }

        if (!empty($mobile)) {
            foreach ($mobile as $key => $tele) {
                if (!empty($tele)) {
                    if (preg_match("~^0\d+$~", $tele)) {
                        $mobile[$key] = "94" . substr($tele, 1);
                    } else {
                        $mobile[$key] = "94" . $tele;
                    }
                }
            }
        }

        if (!empty($request->telephone)) {
            $request->merge(array('telephone' => serialize($telephone)));
        }
        if (!empty($request->mobile)) {
            $request->merge(array('mobile' => serialize($mobile)));
        }
        //dd($request->all());
        //end contact numbers change

        $folder_path = "saleme/images/prime-members/";
        if (!file_exists($folder_path . $request->member_id)) {
            mkdir($folder_path . $request->member_id, 0777, true);
        }
        $memberid = getMemberId();
        $provider = !empty(Session::get('provider')) ? Session::get('provider') : '';
        $memberdetails = Member::MemberDetailsByIdsocial($memberid, $provider);
//        dd($memberdetails);
//        if (empty($memberdetails->cityname)) {
//            $city_name = 'sri+lanka';
//        } else {
//            $city_name = str_replace(' ', '+', strtolower($memberdetails->cityname));
//        }
        //        get location lat lon
//        ini_set("allow_url_fopen", 1);
//        $URL = 'http://maps.googleapis.com/maps/api/geocode/json??key=AIzaSyDMZ1lBPKgmEWvAP6uvBoEv7zouf_73FLw&address=' . $city_name;
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_URL, $URL);
//        $result = curl_exec($ch);
//        curl_close($ch);
//
//        $obj = json_decode($result);
//        $res = reset($obj->results);
//        $geometry = $res->geometry;
//        $cordinates = $geometry->location;

        //create member for company - all activities combines with this member id
        $newMember = new Member();
        $newMember->first_name = $request->company_name;
        $newMember->email = $request->email;
        $newMember->member_type = 'premium';
        $newMember->password = bcrypt($request->password);
        $newMember->status = 'active';
        $status = $newMember->save();
        if ($status) {
            foreach ($telephone as $item) {
                $newMember->membercontacts()->save(new Membercontact(['contactnumber' => $item]));
            }
            foreach ($mobile as $item) {
                $newMember->membercontacts()->save(new Membercontact(['contactnumber' => $item]));
            }
        }
        $lastPremiumMemberId = $newMember->id;

        //company profile
        $slug = Slug::uniqueSlug('Memberpremium', $request->company_name);
        $premiumObj = new Memberpremium($request->all());
        $premiumObj->slug = $slug;
        $premiumObj->profile_image = "saleme-default-profile.jpg";
        $premiumObj->cover_image = "saleme-default-cover.jpg";
//        $premiumObj->lat = $cordinates->lat;
//        $premiumObj->lng = $cordinates->lng;
        $premiumObj->lat = '7.873054';
        $premiumObj->lng = '80.771797';
        $premiumObj->member_id = $lastPremiumMemberId;
        if (!empty($request->telephone)) {
            $premiumObj->telephone = $request->telephone;
        }
        if (!empty($request->mobile)) {
            $premiumObj->mobile = $request->mobile;
        }
        $status = $premiumObj->save();
        $memberPremiumId = $premiumObj->id;

        //save employee info
        $employee = new Memberemployee();
        $employee->member_id = $request->member_id; //original member id
        $employee->memberpremium_id = $memberPremiumId;  //company info id memberpremiums
        $employee->membergroup_id = '1';  //employee group admin id
        $employee->designation = 'N/A';  //employee designation
        $employee->email = $memberdetails->email;  //employee designation
        $employee->status = 'active';  //employee designation
        $employee->save();

        if ($status) {
            $member = Member::find($request->member_id);
            $member->parent_id = $lastPremiumMemberId;
            $member->save();
            //update parent id for premiummembers relation to work
            $updateMember = Member::find($lastPremiumMemberId);
            $updateMember->parent_id = $lastPremiumMemberId;
            $updateMember->save();
            flash('Thank you! Your request for Premium Membership will be processed by SaleMe Team & we will get back to you.', 'success');
            return redirect('myprofile');
        } else {
            flash('Sorry! Something went wrong.', 'danger');
        }
        return redirect()->back();
    }

    public function edit($id)
    {
        $id = substr($id, 3);
        $categorylist = Category::pluck('category_name', 'id')->toArray();
        $premiumMember = Memberpremium::with('member', 'category')->where('id', $id)->first();
        $contacts = Membercontact::where('member_id', $premiumMember->member_id)->select('id', 'contactnumber')->get();
        return view('member.premium.edit', compact('premiumMember', 'categorylist', 'contacts'));
    }

    public function update(Request $request, Memberpremium $premium)
    {
        $telephone = array_filter($request->telephone);
        $mobile = array_filter($request->mobile);
        if (!empty($telephone)) {
            foreach ($telephone as $key => $tele) {
                if (!empty($tele)) {
                    if (preg_match("~^0\d+$~", $tele)) {
                        $telephone[$key] = "94" . substr($tele, 1);
                    } else {
                        $telephone[$key] = "94" . $tele;
                    }
                }
            }
        }

        if (!empty($mobile)) {
            foreach ($mobile as $key => $tele) {
                if (!empty($tele)) {
                    if (preg_match("~^0\d+$~", $tele)) {
                        $mobile[$key] = "94" . substr($tele, 1);
                    } else {
                        $mobile[$key] = "94" . $tele;
                    }
                }
            }
        }

        if (!empty($request->telephone)) {
            $request->merge(array('telephone' => serialize($telephone)));
        }
        if (!empty($request->mobile)) {
            $request->merge(array('mobile' => serialize($mobile)));
        }


        //get premium member contacts from membercontacts table. this is used for post ad cobtact number.
        $premium->load(['member' => function ($q) {
            $q->with(['membercontacts' => function ($q1) {
                $q1->select('id', 'member_id', 'contactnumber');
            }]);
        }]);
        $newMember = $premium->member;

        $folder_path = "saleme/images/prime-members/";
        if ($request->exists('profile')) {
            $profile_image = 'avatar.' . $request->profile->getClientOriginalExtension();
            $request->profile->move(public_path($folder_path . $request->member_id), $profile_image);
            $profile_image = $request->member_id . '/' . $profile_image;
            $request->merge(['profile_image' => $profile_image]);
        }
        if ($request->exists('cover')) {
            $cover_image = 'cover.' . $request->cover->getClientOriginalExtension();
            $request->cover->move(public_path($folder_path . $request->member_id), $cover_image);
            $cover_image = $request->member_id . '/' . $cover_image;
            $request->merge(['cover_image' => $cover_image]);
        }

        $contacts = array();
        foreach ($premium->member->membercontacts as $membercontact) {
            $contacts[] = $membercontact->contactnumber;
        }

        $status = $premium->update($request->all());
        if ($status) {
            foreach ($telephone as $item) {
                if (!in_array($item, $contacts)) {
                    $newMember->membercontacts()->save(new Membercontact(['contactnumber' => $item]));
                }
            }
            foreach ($mobile as $item) {
                if (!in_array($item, $contacts)) {
                    $newMember->membercontacts()->save(new Membercontact(['contactnumber' => $item]));
                }
            }
        }
        $newMember->save();

        if ($status) {
            flash('Thank you! Record updated successfully.', 'success');
        } else {
            flash('Sorry! Something went wrong.', 'danger');
        }
        return back();
    }

    public function deletecontact(Membercontact $contact)
    {
        if (!empty($contact)) {
            $contact->delete();
            return response()->json(['response' => array(
                'message' => 'success',
            )]);
        }
    }

    public function reorderContacts(Request $request, $memberid)
    {
//        dd($request->contactnumber);
        $existing = Membercontact::where('member_id', $memberid)->delete();
//        dd($existing);
        $x = count($request->contactnumber) - 1;
//        dd($request->contactnumber);
        for ($x; $x >= 0; $x--) {
            $charges[] = [
                'contactnumber' => $request->contactnumber[$x],
                'member_id' => $memberid
            ];
        }
//        foreach ($request->contactnumber as $contact){
//            $charges[] = [
//                'contactnumber'=>$contact,
//                'member_id'=>$memberid
//            ];
//
//        }
        $res = Membercontact::insert($charges);
//        dd($res);
//
        if ($res) {
            flash('Thank you! Record updated successfully.', 'success');
            return redirect()->back();
        }
//        dd($existing);
//        if (!empty($contact)) {
//            $contact->delete();
//            return response()->json(['response' => array(
//                'message' => 'success',
//            )]);
//        }
    }

    public function DeleteSingleContact(Request $request, $id)
    {
        Membercontact::where('member_id', $request->member_id)
            ->where('contactnumber', $request->contactnumber)
            ->delete();

        return response()->json(['success' => true, 'response' => 'contact number successfully deleted']);
    }

    public function ContactEditSave(Request $request, $id)
    {
        $premium_member = Memberpremium::where('id', $request->premium_id)->first();
//        dd($premium_member);
        $premium_member->telephone = serialize($request->telephone);
        $premium_member->mobile = serialize($request->mobile);
        $save = $premium_member->update();

        if ($save) {
            return response()->json(['success' => true, 'response' => 'contact number successfully deleted']);
        }
    }

    public function updatecover(Request $request, Memberpremium $member)
    {
        if (!$request->cover) {
            flash('Please select an Image.', 'danger');
            return back();
        }
        if (count($member)) {
            $memberId = $member->member_id;
            $folder_path = "saleme/images/prime-members/" . $memberId . "/";
            if (!is_dir($folder_path)) {
                mkdir($folder_path, 0777, true);
            }
            if ($request->exists('cover')) {
//                delete existing image
                if (file_exists($folder_path . '/' . $member->cover_image)) {
                    unlink($folder_path . '/' . $member->cover_image);
                }

                $file = Input::file('cover');
                $ext = '.' . $file->getClientOriginalExtension();
                $img = Image::make($file)->orientate()->resize(1300, 500, function ($constraint) {
                    $constraint->aspectRatio();
                })->resizeCanvas(1300, 500, 'center', false, '#ecf0f1');
                $img->interlace();
                $cover_image = 'cover_' . rand() . '_' . $memberId . $ext;
                $img->save($folder_path . '/' . $cover_image);
                $request->merge(['cover_image' => $cover_image]);
            }
            $member->cover_image = $request->cover_image;
            if ($member->save()) {
                flash('Cover Updated!', 'success');
            } else {
                flash('Sorry! Something went wrong.', 'danger');
            }
            return back();
        }

    }

    public function updateprofile_image(Request $request, Memberpremium $member)
    {
        if (!$request->profile_image) {
            flash('Please select an Image.', 'danger');
            return back();
        }
        if (count($member)) {
            $memberId = $member->member_id;
            $folder_path = "saleme/images/prime-members/" . $memberId . "/";

            if (!is_dir($folder_path)) {
                mkdir($folder_path, 0777, true);
            }
//dd($member->profile_image);
            if ($request->exists('profile_image')) {
                //delete existing image
                if (file_exists($folder_path . '/' . $member->profile_image)) {
                    unlink($folder_path . '/' . $member->profile_image);
                }
                $file = Input::file('profile_image');
                $ext = '.' . $file->getClientOriginalExtension();
                $img = Image::make($file)->orientate()->resize(400, 400, function ($constraint) {
                    $constraint->aspectRatio();
                })->resizeCanvas(400, 400, 'center', false, '#ecf0f1');
                $profile_image = 'profile_' . rand() . '_' . $memberId . $ext;
                $img->save($folder_path . '/' . $profile_image);

//                CREATE FAVICON
                $fav_path = "saleme/images/prime-members/" . $memberId . "/fav/";
                if (!is_dir($fav_path)) {
                    mkdir($fav_path, 0777, true);
                }
                //delete existing image
                if (file_exists($fav_path . '/' . $member->profile_image)) {
                    unlink($fav_path . '/' . $member->profile_image);
                }
                $favi = Image::make($file)->orientate()->resize(16, 16, function ($constraint) {
                    $constraint->aspectRatio();
                })->resizeCanvas(16, 16, 'center', false, '#ecf0f1');
                $favi_image = $profile_image;
                $favi->save($fav_path . $favi_image);

                $request->merge(['profile_img' => $profile_image]);
            }
            $member->profile_image = $request->profile_img;
            if ($member->save()) {
                flash('Profile image Updated!', 'success');
            } else {
                flash('Sorry! Something went wrong.', 'danger');
            }
            return back();
        }
    }

    public function about_company()
    {
        if (!isEmployeeAdmin()) {
            return redirect('/myprofile');
        }
        $memberid = getMemberId();
        $provider = !empty(Session::get('provider')) ? Session::get('provider') : '';
        $memberdetails = Member::MemberDetailsByIdsocial($memberid, $provider);
        $member = getMember();
//        dd($member->premiummember->telephone);
//        dd($member->premiummember->mobile);
        $membercontacts = Membercontact::where('member_id', $memberid)->get();
        return view('member.premium.premium-manage-about')
            ->with('member', $member)
            ->with('memberid', $memberid)
            ->with('membercontacts', $membercontacts)
            ->with('city_name', $memberdetails->cityname);
    }

    public function shop($companyslug)
    {
        $theme = 'member.premiums_frontend.index';
        $memberCompany = Memberpremium::with('member', 'category')->where('slug', $companyslug)->first();
//        dd($memberCompany->theme);
        if ($memberCompany->theme === 'full-navigation') {
            $theme = 'member.premiums_frontend.theme_01.index';
        }
        if (!count($memberCompany)) {
            abort(404);
        }
        $memberemployees = '';
        if ($memberCompany->teampublic) {
            $memberemployees = Memberemployee::select('id', 'member_id', 'designation')
                ->where('memberpremium_id', $memberCompany->id)
                ->where('status', 'active')
                ->where('teampublic', 1)
                ->with('member')
                ->get();
        }

        $services = Premiumservice::where('memberpremium_id', $memberCompany->id)->where('featured', true)
            ->select('id', 'servicetitle', 'service', 'featured', 'image')->limit(4)->get();
//        dd($services);
        $contacts = Membercontact::where('member_id', $memberCompany->member->id)->get();
        $ads = Alladsmaster::with('category', 'subcategory', 'district', 'city')->where('member_id', $memberCompany->member->id)->where('status', 'confirm')->latest('id')->limit(3)->get();
        $image_count = Premiumgaleryimage::select('image')->where('memberpremium_id', $memberCompany->id)->count();
        $images = Premiumgaleryimage::select('image')->where('memberpremium_id', $memberCompany->id)->where('featured',1)->limit(4)->get();
        $service_count = Premiumservice::where('memberpremium_id', $memberCompany->id)->count();
        return view($theme, compact('memberCompany', 'contacts', 'ads', 'memberemployees', 'services', 'image_count', 'service_count', 'images'));
    }

    public function allads($companyslug)
    {
        $theme = 'member.premiums_frontend.ads';
        $memberCompany = Memberpremium::with('member', 'category')->where('slug', $companyslug)->first();
        if ($memberCompany->theme === 'full-navigation') {
            $theme = 'member.premiums_frontend.theme_01.ads';
        }
        if (!count($memberCompany)) {
            abort(404);
        }
        $contacts = Membercontact::where('member_id', $memberCompany->member->id)->get();
        $ads = Alladsmaster::GetAdsListByMemberIdByStatus($memberCompany->member->id, 'confirm');
        $service_count = Premiumservice::where('memberpremium_id', $memberCompany->id)->count();
        $image_count = Premiumgaleryimage::select('image')->where('memberpremium_id', $memberCompany->id)->count();
        return view($theme, compact('memberCompany', 'contacts', 'ads', 'image_count', 'service_count'));
    }

    public function contact($companyslug)
    {
        $theme = 'member.premiums_frontend.contact';
        $memberCompany = Memberpremium::with('member', 'category')->where('slug', $companyslug)->first();
        if ($memberCompany->theme === 'full-navigation') {
            $theme = 'member.premiums_frontend.theme_01.contact';
        }
        if (!count($memberCompany)) {
            abort(404);
        }
        $service_count = Premiumservice::where('memberpremium_id', $memberCompany->id)->count();
        $image_count = Premiumgaleryimage::select('image')->where('memberpremium_id', $memberCompany->id)->count();
        return view($theme, compact('memberCompany', 'image_count', 'service_count'));
    }

    public function gallery($companyslug)
    {
        $theme = 'member.premiums_frontend.gallery';
        $memberCompany = Memberpremium::with('member', 'category')->where('slug', $companyslug)->first();
        if ($memberCompany->theme === 'full-navigation') {
            $theme = 'member.premiums_frontend.theme_01.gallery';
        }
        if (!count($memberCompany)) {
            abort(404);
        }
        $service_count = Premiumservice::where('memberpremium_id', $memberCompany->id)->count();
        $image_count = Premiumgaleryimage::select('image')->where('memberpremium_id', $memberCompany->id)->count();
        $images = Premiumgaleryimage::select('image')->where('memberpremium_id', $memberCompany->id)->get();

        return view($theme, compact('memberCompany', 'images', 'image_count', 'service_count'));
    }

    public function services($companyslug)
    {
        $theme = 'member.premiums_frontend.services';
        $memberCompany = Memberpremium::with('member', 'category')->where('slug', $companyslug)->first();
        if ($memberCompany->theme === 'full-navigation') {
            $theme = 'member.premiums_frontend.theme_01.services';
        }
        if (!count($memberCompany)) {
            abort(404);
        }
        $services = Premiumservice::where('memberpremium_id', $memberCompany->id)
            ->select('id', 'servicetitle', 'service', 'featured', 'image')->get();
        $service_count = Premiumservice::where('memberpremium_id', $memberCompany->id)->count();
        $image_count = Premiumgaleryimage::select('image')->where('memberpremium_id', $memberCompany->id)->count();

        return view($theme, compact('memberCompany', 'services', 'image_count', 'service_count'));
    }


    //send email inquiry
    public function sendinquirymail(Request $request)
    {
        //dd($request->all());
        if (!empty($request->id)) {
            $id = $request->id;
            $send_email = Memberpremium::select('id', 'company_name', 'email')
                ->where('id', $id)->first();
            //set mail recipient to post ad user email - to mail
            $arr = array(
                'company_name' => $send_email->company_name,
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'subject' => $request->subject,
                'message' => $request->message,
            );
            $status = $send_email->notify(new SendEmailInquiry($arr));
            return response()->json(['response' => array('message' => 'success')]);
        }
    }

    //END send email inquiry

    public function adpacks()
    {
        if (!isPremium()) {
            return redirect('my_ads');
        }
        $adPacks = Boostpack::where('packtype', 'pack')->get();
        return view('member.boosting.packs', compact('adPacks'));
    }

    public function manageemployee(Memberemployee $employee)
    {
        $employee->load('member');
        $checkPremiumActive = Memberpremium::where('member_id', $employee->member->parent_id)->select('id', 'verified')->first();
        if (!$checkPremiumActive->verified) {
            return redirect('myprofile');
        }
        $employeeTypes = Membergroup::pluck('name', 'id');
        $member = getMember();
        return view('member.premium.premium-manage-employee',
            compact('employee', 'employeeTypes', 'member'));
    }

    public function makepublic(Request $request)
    {
        //acceps array or serialize array
        try {
            $member_id = '';
            $teampublic = '';
            if (is_array($request->all())) {
                $member_id = (!empty($request['member_id'])) ? $request['member_id'] : '';
                $teampublic = (!empty($request['teampublic'])) ? $request['teampublic'] : false;
            }
            $member = Memberpremium::where('member_id', $member_id)->first();
            $member->teampublic = $teampublic;
            $status = $member->save();

            if ($status) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Team public status changed successfully'
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'status' => 'error',
                'errors' => $e->getMessage(),
            ]);
        }

    }

    //change theme
    public function change_theme(Request $request)
    {
//        dd($request->all());
        //acceps array or serialize array
        try {
            $member_id = '';
            $theme = '';
            if (is_array($request->all())) {
                $member_id = (!empty($request['memberid'])) ? $request['memberid'] : '';
                $theme = (!empty($request['theme'])) ? $request['theme'] : false;
            }
            $member = Memberpremium::where('member_id', $member_id)->first();
            $member->theme = $theme;
            $status = $member->save();

            if ($status) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Theme Update successfully !'
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'status' => 'error',
                'errors' => $e->getMessage(),
            ]);
        }

    }

    /*----------------------------------Member Loaction------------------------------------------------*/

    public function member_location()
    {
        if (!isEmployeeAdmin()) {
            return redirect('/myprofile');
        }
        $memberid = getMemberId();
        $provider = !empty(Session::get('provider')) ? Session::get('provider') : '';
        $memberdetails = Member::MemberDetailsByIdsocial($memberid, $provider);

        $member = getMember();

        $premiumLocation = Memberpremium::select('lat', 'lng')->where('member_id', $memberid)->first();

        if (!empty($memberdetails->cityname)) {
            $location_name = str_replace(' ', '+', strtolower($memberdetails->cityname));
        } else {
            $location_name = "sri+lanka";
        }
        $lat = '';
        $lng = '';
        if (empty($premiumLocation->lat) OR empty($premiumLocation->lng)) {
            //        get location lat lon
            ini_set("allow_url_fopen", 1);
            $URL = 'http://maps.googleapis.com/maps/api/geocode/json??key=AIzaSyDMZ1lBPKgmEWvAP6uvBoEv7zouf_73FLw&address=' . $location_name;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, $URL);
            $result = curl_exec($ch);
            curl_close($ch);

            $obj = json_decode($result);
            $res = reset($obj->results);
            $geometry = $res->geometry;
            $cordinates = $geometry->location;
            $lat = $cordinates->lat;
            $lng = $cordinates->lng;
        } else {
            $lat = $premiumLocation->lat;
            $lng = $premiumLocation->lng;

//            dd($lng);
        }


//END get lat lon
        return view('member.premium.premium-manage-location')
            ->with('member', $member)
            ->with('memberid', $memberid)
            ->with('city_name', $memberdetails->cityname)
            ->with('lat', $lat)
            ->with('lng', $lng);
    }

    public function updatelocation(Request $request, $premiummember_id)
    {
        $member = Memberpremium::where('member_id', $premiummember_id)->update(['lat' => $request->lat, 'lng' => $request->lng]);
//        $member->lat = $request->lat;
//        $member->lng = $request->lng;
        if ($member) {
            flash('Location Update successfully !', 'success');
        } else {
            flash('Sorry! Something went wrong.', 'danger');
        }
        return back();
    }

    /*----------------------------------END Member Loaction------------------------------------------------*/

    public function services_add()
    {
        $member = getMember();
        $services = Premiumservice::select('id', 'memberpremium_id', 'servicetitle', 'service', 'featured', 'image')
//            ->with(['premiummember'=>function($q){
//                $q->select('member_id');
//            }])
            ->where('memberpremium_id', $member->premiummember->id)
            ->get();

        return view('member.premium.premium-services', compact('member', 'services'));
    }

    public function storeservices(Request $request)
    {
//        dd(Input::file('file'));
        $request->flash();
        $this->validate($request, [
            'servicetitle' => 'required|min:5|max:35',
            'service' => 'required|min:10',
            'file' => 'required',
        ]);

        $imagename = 'saleme_service_' . uniqid();
        $file = Input::file('file');
        $ext = (!empty($file)) ? '.' . $file->getClientOriginalExtension() : '';

        $member = getMember();
        $service = new Premiumservice($request->all());
        $service->memberpremium_id = $member->premiummember->id;
        $service->featured = ($request->has('featured')) ? '1' : '0';
        $service->image = $imagename . $ext;
        $service->save();

        if (!empty($file)) {
            $imgPath = public_path('saleme/images/prime-members/' . $member->premiummember->id);
            if (!is_dir($imgPath)) {
                mkdir($imgPath, 0777, true);
            }
            $img = Image::make($file)->orientate()->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            })->resizeCanvas(200, 200, 'center', false, '#929292');
            $img->interlace();
            $img->save($imgPath . '/' . $imagename . $ext);
        }
        flash('Service saved successfully !', 'success');
        return back();

    }

    public function editservice(Premiumservice $service)
    {
        $member = getMember();
        return view('member.premium.premium-servicesedit', compact('member', 'service'));
    }

    public function updateservice(Request $request, Premiumservice $service)
    {
        $this->validate($request, [
            'servicetitle' => 'required|min:5|max:35:premiumservices,servicetitle,' . $service->id,
            'service' => 'required|min:10',
        ]);

        $imagename = 'saleme_service_' . uniqid();
        $file = Input::file('file');
        $ext = (!empty($file)) ? '.' . $file->getClientOriginalExtension() : '';

        $member = getMember();
        $service->memberpremium_id = $member->premiummember->id;
        $service->featured = ($request->has('featured')) ? '1' : '0';
        $service->image = (!empty($file)) ? $imagename . $ext : $request->dbimage;
        $service->update($request->all());

        if (!empty($file)) {
            $imgPath = public_path('saleme/images/prime-members/' . $member->premiummember->id);
            if (!empty($request->dbimage) && file_exists($imgPath . '/' . $request->dbimage)) {
                unlink($imgPath . '/' . $request->dbimage);
            }


            if (!is_dir($imgPath)) {
                mkdir($imgPath, 0777, true);
            }
            $img = Image::make($file)->orientate()->resize(200, 160, function ($constraint) {
                $constraint->aspectRatio();
            })->resizeCanvas(200, 160, 'left', false, '#ecf0f1');
            $img->interlace();
            $img->save($imgPath . '/' . $imagename . $ext);
        }
        flash('Service update successfully !', 'success');
        return redirect($member->premiummember->slug . '/services-add');
        return back();
    }

    public function deleteservice(Request $request)
    {
        try {
            $id = '';
            if (is_array($request->all())) {
                $id = (!empty($request['id'])) ? $request['id'] : '';
            }
            $member = getMember();
            //delete main record
            $service = Premiumservice::where('id', $id)->firstOrFail();
            $status = $service->delete();
            $filePath = public_path('saleme/images/prime-members/' . $member->premiummember->id . '/' . $service->image);
            unlink($filePath);
            if ($status) {
                return response()->json([
                    'message' => 'Service Successfully Deleted .'
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'errors' => $e->getMessage(),
            ]);
        }
    }

//    gallery
    public function add_gallery()
    {
        $member = getMember();
        $images = Premiumgaleryimage::select('id', 'memberpremium_id', 'image', 'created_at', 'featured')
            ->where('memberpremium_id', $member->premiummember->id)
            ->get();
        return view('member.premium.premium-gallery', compact('member', 'images'));
    }

//    set featured image
    public function setfeaturedimage(Request $request)
    {
        $images = Premiumgaleryimage::where('memberpremium_id', $request->member_id)->where('featured', 1)->count();
        if (!$request->featured) {
            $img = Premiumgaleryimage::where('id', $request->image_id)->first();
            $img->featured = $request->featured;
            $img->save();
            return response()->json(['message' => 'Image Removed From Featured Successfully','success'=>1, 'class'=>'success']);
        } else {
            if ($images < 4) {
                $img = Premiumgaleryimage::where('id', $request->image_id)->first();
                $img->featured = $request->featured;
                $img->save();
                return response()->json(['message' => 'Image Featured Successfully','success'=>1,'class'=>'success']);
            }else{
                return response()->json(['message' => 'Only 4 Images can be Featured','success'=>0,'class'=>'error']);
            }

        }
    }

// save facebook page url
    public function facebookPageAave(Request $request)
    {
        $member = getMember();
        $mbr = Memberpremium::whereId($member->premiummember->id)->first();
        $mbr->facebook_page = $request->facebook_page;
        $result = $mbr->save();
        if ($result) {
            flash('Facebook page url saved successfully', 'success');
            return back();
        } else {
            flash('Facebook page url not saved successfully', 'danger');
            return back();
        }
    }

// save facebook page url
    public
    function websiteSave(Request $request)
    {
        $member = getMember();
        $mbr = Memberpremium::whereId($member->premiummember->id)->first();
        $mbr->website = $request->website;
        $result = $mbr->save();
        if ($result) {
            flash('Website url saved successfully', 'success');
            return back();
        } else {
            flash('Website page url not saved successfully', 'danger');
            return back();
        }
    }


// save company name and slug
    public function saveCompanyNameAndSlug(Request $request)
    {
//        dd($request->all());
        $slug = Slug::uniqueSlug('Memberpremium', $request->company_name);
        $member = getMember();
        $mbr = Memberpremium::whereId($member->premiummember->id)->first();
        $mbr->company_name = $request->company_name;
        $mbr->slug = $slug;
        $result = $mbr->save();
        if ($result) {
            flash('Company name and Slug saved successfully', 'success');
            return back();
        } else {
            flash('Company name and Slug not saved successfully', 'danger');
            return back();
        }
    }

    public function storegallery(Request $request)
    {
//        dd(Input::file('file'));
        $request->flash();
        $this->validate($request, [
            'file' => 'required'
        ]);
        $member = getMember();
        $files = Input::file('file');

        foreach ($files as $file) {
            if (self::imageCount() < 12) {
                $imagename = 'saleme_gallery_image_' . uniqid();
                $ext = (!empty($file)) ? '.' . $file->getClientOriginalExtension() : '';
                $gallery = new Premiumgaleryimage();
                $gallery->memberpremium_id = $member->premiummember->id;
                $gallery->image = $imagename . $ext;
                $gallery->save();

                if (!empty($file)) {
                    $imgPath = public_path('saleme/images/prime-members/' . $member->premiummember->id);
                    if (!is_dir($imgPath)) {
                        mkdir($imgPath, 0777, true);
                    }
                    $img = Image::make($file)->orientate()->resize(960, 540, function ($constraint) {
                        $constraint->aspectRatio();
                    })->resizeCanvas(960, 540, 'center', false, '#ffffff');
                    $img->interlace();
                    $img->save($imgPath . '/' . $imagename . $ext);
                }
            } else {
                flash('Only 12 images acceptable.', 'danger');
                return back();
            }
        }
        flash('Service saved successfully !', 'success');
        return back();

    }

    public function deleteimage(Request $request)
    {
        try {
            $id = '';
            if (is_array($request->all())) {
                $id = (!empty($request['id'])) ? $request['id'] : '';
            }
            $member = getMember();
            //delete main record
            $image = Premiumgaleryimage::where('id', $id)->firstOrFail();
            $status = $image->delete();
            $filePath = public_path('saleme/images/prime-members/' . $member->premiummember->id . '/' . $image->image);
            unlink($filePath);
            if ($status) {
                return response()->json([
                    'message' => 'Image Successfully Deleted .'
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'errors' => $e->getMessage(),
            ]);
        }
    }

    private function imageCount()
    {
        $member = getMember();
        $images = Premiumgaleryimage::select('id', 'memberpremium_id', 'image')
            ->where('memberpremium_id', $member->premiummember->id)
            ->count();
        return $images;
    }

    /**********************************************************************************************************************************************************
     * START Employee info update
     **********************************************************************************************************************************************************/
    //search posted ads by member

    public function managepremiumprofile(Request $request)
    {
        if (!isEmployeeAdmin()) {
            return redirect('/myprofile');
        }
        $member = getMember();
        $searchMembers = '';
        if (isset($_POST) && !empty($request->all())) {
            $email = $request->email;
            $request->flashOnly(['email']);
            $searchMembers = Member::where('email', "$email")
                ->with('premiummember', 'district', 'city')->first();
        }
        $employees = Memberemployee::with('member', 'employeegroup')->where('status', 'active')->where('memberpremium_id', getMember()->premiummember->id)->get();
        $pendingEmployees = Memberemployee::with('member', 'employeegroup')->where('status', 'pending')->where('memberpremium_id', getMember()->premiummember->id)->get();
        $resignedEmployees = Memberemployee::with('member', 'employeegroup')->where('status', 'resigned')->where('memberpremium_id', getMember()->premiummember->id)->get();
        //dd($employees);
        $employeeTypes = Membergroup::pluck('name', 'id');
        return view('member.premium.premium-manage')
            ->with('employees', $employees)
            ->with('employeeTypes', $employeeTypes)
            ->with('searchMembers', $searchMembers)
            ->with('pendingEmployees', $pendingEmployees)
            ->with('resignedEmployees', $resignedEmployees)
            ->with('member', $member);
    }

    public function assignmembertocompany(Request $request)
    {
        //dd($request->all());
        if (!empty($request->company_id) && !empty($request->member_id)) {
            $companyId = $request->company_id;
            $memberId = $request->member_id;
            $checkMember = Member::find($memberId);
            if ($checkMember->member_type == 'private' && !$checkMember->parent_id) {
                $checkMember->parent_id = $companyId;
                $checkMember->save();

                $memberPremiums = Memberpremium::where('member_id', $companyId)->first();
                //save employee info
                $checkEmp = Memberemployee::where('member_id', $memberId)->first();
                if (count($checkEmp) && $checkEmp->memberpremium_id == $memberPremiums->id) {
                    $checkEmp->status = 'pending';
                    $checkEmp->save();
                    flash('Member Successfully assigned to the Company!', 'success');
                    return back();
                }

                $employee = new Memberemployee();
                $employee->member_id = $memberId; //original member id
                $employee->memberpremium_id = $memberPremiums->id;  //company info id memberpremiums
                $employee->membergroup_id = $request->membergroup_id;  //employee group
                $employee->designation = 'N/A';  //employee designation
                $employee->status = 'pending';  //employee designation
                $employee->save();

                flash('Member Successfully assigned to the Company!', 'success');
                return back();
            }
        }
        flash('Cannot Assign this Member to your company!', 'danger');
        return redirect('myprofile');
    }

    public function manageemployeeupdate(Request $request, Memberemployee $employee)
    {
        $inputApprove = Input::get('approveEmployee');
        $inputRemove = Input::get('remove_employee');
        $inputUpdate = Input::get('save');
        //apprvoce employee
        if (isset($inputApprove)) {
            $employee->status = 'active';
            $employee->save();
            flash('Employee successfully Approved!', 'success');
            return back();
        }
        //remove employee from company
        if (isset($inputRemove)) {
            $employee->load('member');
            $member = Member::find($employee->member->id);
            $member->parent_id = NULL;
            $member->save();

            ///status change for resigned employee
            $employee->status = 'resigned';
            $employee->save();
            flash('Employee successfully removed!', 'success');
            return redirect('/company/manage');
        }

        if (isset($inputUpdate)) {
            if (!empty($request->all())) {
                $request->merge(['teampublic' => $request->has('teampublic') ? 1 : 0]);
                //dd($request->all());
                $request->merge(['dob' => (!empty($request->dob)) ? $request->dob : NULL]);
                $request->merge(['joindate' => (!empty($request->joindate)) ? $request->joindate : NULL]);

                //$employee->teampublic = (isset($employee->teampublic)) ? 1 : 0;
                $employee->update($request->all());
                flash('Employee profile successfully updated!', 'success');
                return back();
            }
        }
    }

    public function companyteam()
    {
        $member = getMember();
        $employees = Memberemployee::with('member', 'employeegroup')->where('status', 'active')->where('memberpremium_id', getMember()->premiummember->id)->get();
        //dd($employees);
        return view('member.premium.premium-companyteam', compact('member', 'employees'));
    }

    /**********************************************************************************************************************************************************
     * END Employee info update
     **********************************************************************************************************************************************************/
    public function premiummember_list()
    {
//        dd('ff');
        $premiummembers = Memberpremium::getAllActivePremiumMembers();
//        dd($premiummembers);
        return view('member.premium.premiummember_list')
            ->with('premiummembers', $premiummembers);
    }

    public function saveDescription(Request $request)
    {
//        dd($request->all());
        $member = Memberpremium::where('id', $request->premiumid)->first();
        $member->description = $request->description;
        $res = $member->save();

        if ($res) {
            return response()->json(['status' => 'success', 'message' => 'Description updated successfully']);
        } else {
            return response()->json(['status' => 'error', 'message' => 'Description not updated']);
        }
    }
}
