<?php
namespace App\Http\Controllers\Member;

use App\Classes\UUID;
use App\Dialogsms\Sms;
use App\Models\Alladsfilter;
use App\Models\Alladsmaster_member;
use App\Models\Allhoteltravelad;
use App\Models\Boostpack;
use App\Models\Boostviewcount;
use App\Models\Common;
use App\Models\Deletereason;
use App\Models\Memberemployee;
use App\Models\Membervoucher;
use App\Models\Adimage;
use App\Models\Allelectrnicad;
use App\Models\Allpropertyad;
use App\Models\Allvehiclead;
use App\Models\Memberpremium;
use App\Models\Categoryfiltervalue;
use App\Models\City;
use App\Models\District;
use App\Models\Membercontact;
use App\Models\Advertisementtype;
use App\Models\Membervouchercode;
use App\Models\Otp;
use App\Models\Membergroup;
use App\Models\Subcategory;
use App\Models\Tempimage;
use App\Notifications\AccountActivation;
use App\Notifications\AdInquiry;
use App\Notifications\BoostadSuccess;
use App\Notifications\MemberReg;
use App\Notifications\MyNotifiable;
use App\Notifications\PintotopRequest;
use App\Notifications\SalemeAdminNotification;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Member;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Session;
use App\Social\Fbsocial;
use App\Social\Googlesocial;
use Illuminate\Support\Facades\Redirect;
use App\Models\Alladsmaster;
use App\Models\Businesscategory;


//use App\User;


class MemberController extends Controller
{
    public function index()
    {
        return view('default.index');
    }

    public function addtype()
    {
        $membername = '';
        if (empty(getMemberId())) {
            return view('member.login.login');
        } else {
            //  get name by id
            $memberdetails = Member::MemberNameById(getMemberId());
            $membername = !empty($memberdetails) ? $memberdetails->first_name . ' ' . $memberdetails->last_name : '';
        }
//        if (getMemberStatus() == 'inactive') {
//            return view('member.my_ads_inactive');
//        }
        return view('member.addtype', compact('membername'));
    }

    public function selectcategory()
    {
        $adtype = $_GET['type'];
        $datalist = array();
        //get advertiesment types for filter by user clicked on frontend
        $sellAdType = Advertisementtype::where('typeref', $adtype)->select('id')->first();

        $categories = Category::Catrgorylsit(); // category  list
        $allsubcat = Category::subcatlist(); // category with  subcategory query;
        $alldata = array();
        $categoryIdByAdType = array();


        foreach ($allsubcat as $sub_cat) {
            $adTypes = unserialize($sub_cat->adtype);
            if (in_array($sellAdType->id, $adTypes)) {
                $alldata[] = $sub_cat;
                $categoryIdByAdType[$sub_cat->catid] = $sub_cat->catid;
            }
        }

        if (!empty($alldata)) {
            foreach ($alldata as $data) {
                $datalist[$data->catid][] = $data;
            }
        }

        //select categories by sub_cat
        $allCategories = array();
        foreach ($categories as $category) {
            if (in_array($category->id, $categoryIdByAdType)) {
                $allCategories[] = $category;
            }
        }
        return view('member.selectcategory')
            ->with('categories', $allCategories)
            ->with('alldata', $datalist);
    }

    public function memberregister(Request $request)
    {
        //member registration
        $request->flash();
        $request->merge(['email' => $request->signup_email]);
        $rules = array(
            'email' => 'required|email|unique:members'
        );
        $dat = validate($request, $rules, $id = null, []);
        if (!is_null($dat)) {
            return $dat;
        }

        //  create member
        $membermanage = new Member();
        $id = $membermanage->addMember($request->all());
        if (!empty($id)) {
            $token = hash_hmac('sha256', $request->signup_email . time(), config('app.key'));
            DB::table('member_activations')->insert(['member_id' => $id, 'token' => $token]);

            $arr = $membermanage::find($id);
            $arr->notify(new AccountActivation($arr, $token));
            Auth::guard('member')->login($arr);
            if (isset($request->location)) {
                return \redirect('/playzone/home');
            }
            return \redirect('my_ads');
            return json_encode(array('success' => true, 'url' => 'my_ads', 'response' => 'Account Registered!'));
        }

    }

    public function memberactivation($token)
    {
        //member email activation
        $check = DB::table('member_activations')->select('member_id')->where('token', $token)->first();
        if (!is_null($check)) {
            $member = Member::find($check->member_id);
            if ($member->status == 'active') {
                return redirect('home');
            }

            $status = $member->update(['status' => 'active']);
            if ($status) {
                $member->notify(new MemberReg($member));
                DB::table('member_activations')->where('token', $token)->delete();
                Auth::guard('member')->login($member);
                return redirect('my_ads')->with('member_activate', "Account activated successfully.");
            }
        }
        $error = "Activation link has expired. Please try to login with the email and password you registered.";
        return view('errors.customexception')->with('error', $error);
    }

    public function memberprofile()
    {
//        dd(getMemberType());
        $getParams = Input::all();
        if (!empty($getParams['type']) && $getParams['type'] == 'company') {
            session()->forget('profile_type');
            //Session::save();
        }

        $show_nav = false;
        $memberid = getMemberId();
        $state = "";
        $boost = "";
        $allvouchers = '';
        $activevouchers = '';
        //get ads by this member
        $alladscount = Alladsmaster::MemberAdsCountByStatus($memberid, 'all');
        $adscount = Alladsmaster::MemberAdsCountByStatus($memberid, 'confirm');
        $spamedadscount = Alladsmaster::MemberAdsCountByStatus($memberid, 'cancel');
        $pendingads = Alladsmaster::MemberAdsCountByStatus($memberid, 'pending');
        $draftads = Alladsmaster::MemberAdsCountByStatus($memberid, 'draft');

        $boostedcount = Alladsmaster::MemberBoostAdsCountById($memberid);
        $member_vouchers = Membervoucher::select('id', 'ad_referance')
            ->where('member_id', $memberid)
            ->where('status', 'active')
            ->get();
//        dd($adscount);
        if (count($member_vouchers)) {
            $totalVovchers = 0;
            $allvouchers = self::boostVoucherCount($member_vouchers, $totalVovchers, 'null');
            $activevouchers = self::boostVoucherCount($member_vouchers, $totalVovchers, 'Adrefarence');
        }        // member details by id
        $member = Member::where('id', $memberid)->with(['membercontacts', 'membersocial', 'otpnumbers',
                'premiummember' => function ($query) {
                    $query->with('category');
                }
            ]
        )->first();
        //dd($member);
        $provider = !empty(Session::get('provider')) ? Session::get('provider') : '';
        $memberdetails = Member::MemberDetailsByIdsocial($memberid, $provider);
        if (isset($_GET['boost'])) {
            $boost = $_GET['boost'];
            $allads = Alladsmaster::GetAdsListByMemberIdByStatus($memberid, $state, $boost);
            $allads->appends(array('boost' => $boost))->links();
        } else {
            if (isset($_GET['state'])) {
                $state = $_GET['state'];
                $allads = Alladsmaster::GetAdsListByMemberIdByStatus($memberid, $state);
                if ($state == 'cancel') {
                    $allads = Alladsmaster::GetAdsListByMemberIdByStatus($memberid, $state);
                }
                $allads->appends(array('state' => $state))->links();
            } else {
                $allads = Alladsmaster::GetAdsListByMemberIdByStatus($memberid);
            }
        }
        $delete_resons = Deletereason::select('id', 'reason')->get();
        // all ads active
        // all locations
        $locationdistrictslist = DB::table('location_districts')->get()->toArray();
        $locationcities = '';
        if ($member) {
            $locationcities = City::where('district_id', $member->district_id)->select('id', 'city_name')->get();
        }

        //get member voucher codes if exists
//        $vouches = Membervoucher::where('member_id',getMemberId())->with('vouchercodes')->get();
//        $boostAds = array();
//        foreach ($vouches as $vouch) {
//            $boostAds = $vouch->vouchercodes
//        }
        $layout = 'profile';
        if (checkCompany()) {
            $layout = 'premium.premium-profile';
        }
        if (temporyPrivateLogin()) {
            $layout = 'profile';
        }

//        dd($member);
        return view('member.' . $layout)
            ->with('adscount', $adscount)
            ->with('spamedadscount', $spamedadscount)
            ->with('memberdetails', $memberdetails)
            ->with('pendingads', $pendingads)
            ->with('draftads', $draftads)
            ->with('alladscount', $alladscount)
            ->with('allads', $allads)
            ->with('member', $member)
            ->with('state', $state)
            ->with('boost', $boost)
            ->with('locationdistrictslist', $locationdistrictslist)
            ->with('locationcities', $locationcities)
            ->with('show_nav', $show_nav)
            ->with('boostedcount', $boostedcount)
            ->with('member_vouchers', $member_vouchers)
            ->with('allvouchers', $allvouchers)
            ->with('activevouchers', $activevouchers)
            ->with('delete_resons', $delete_resons);
    }

    private function boostVoucherCount($member_vouchers, $totalVovchers, $status)
    {
        foreach ($member_vouchers as $voucher) {
            $voucherCodes = Membervouchercode::where('membervoucher_id', $voucher->id);
            if ($status !== 'null') {
                $voucherCodes->where('ad_referance', null);
            }
            $count = $voucherCodes->count();
            $totalVovchers += $count;
        }
        return $totalVovchers;
    }

    public function memberads()
    {
        if (!Auth::guard('member')->check()) {
            return redirect('member-login');
        }

        $getParams = Input::all();
        if (!empty($getParams['type']) && $getParams['type'] == 'private') {
            Session::put('profile_type', 'private');
            Session::save();
        }
        if (checkCompany()) {
            if (!temporyPrivateLogin()) {
                return redirect('myprofile');
            }
        }

        $memberid = getMemberId();
        $show_nav = "";
        $state = "";
        $spam_reason = "";
        $allads = Alladsmaster::GetAdsListByMemberIdByStatus($memberid); //get all ads my member id
        if (!empty($allads)) {
            foreach ($allads as $allad) {
                $spamReasons = Alladsmaster::with(['spamreasons' => function ($query) use ($allad) {
                    $query->select('reason');
                }])->where('id', $allad->adid)->where('status', 'cancel')->select('id')->first();
                $allad->spams = (!empty($spamReasons->spamreasons)) ? $spamReasons->spamreasons : '';
            }
        }

        $delete_resons = Deletereason::select('id', 'reason')->get();

        // member details by id
        $member = Member::where('id', $memberid)->with('membercontacts', 'membersocial')->first();
        $provider = !empty(Session::get('provider')) ? Session::get('provider') : '';
        $memberdetails = Member::MemberDetailsByIdsocial($memberid, $provider);
        if (isset($_GET['boost'])) {
            $boost = $_GET['boost'];
            $allads = Alladsmaster::GetAdsListByMemberIdAsBoost($memberid, $boost);
            $allads->appends(array('boost' => $boost))->links();
            $show_nav = false;
        } else {
            if (isset($_GET['state'])) {
                $state = $_GET['state'];
                $allads = Alladsmaster::GetAdsListByMemberIdByStatus($memberid, $state);
                if ($state == 'cancel') {
                    foreach ($allads as $allad) {
                        $spamReasons = Alladsmaster::with(['spamreasons' => function ($query) use ($allad) {
                            $query->select('reason');
                        }])->where('id', $allad->adid)->where('status', $state)->select('id')->first();
                        $allad->spams = (!empty($spamReasons->spamreasons)) ? $spamReasons->spamreasons : '';
                    }
                }
                $allads->appends(array('state' => $state))->links();
            }
            $show_nav = true;
        }

        $alladscount = Alladsmaster::MemberAdsCountByStatus($memberid, 'all');
        $adscount = Alladsmaster::MemberAdsCountByStatus($memberid, 'confirm');
        $spamedadscount = Alladsmaster::MemberAdsCountByStatus($memberid, 'cancel');
        $pendingads = Alladsmaster::MemberAdsCountByStatus($memberid, 'pending');
        $draftads = Alladsmaster::MemberAdsCountByStatus($memberid, 'draft');
        // member details by id
//        $memberdetails = Member::MemberDetailsById($memberid);
        // all ads active

        return view('member.my_ads')
            ->with('adscount', $adscount)
            ->with('spamedadscount', $spamedadscount)
            ->with('memberdetails', $memberdetails)
            ->with('pendingads', $pendingads)
            ->with('draftAds', $draftads)
            ->with('allads', $allads)
            ->with('show_nav', $show_nav)
            ->with('state', $state)
            ->with('member', $member)
            ->with('delete_resons', $delete_resons);
    }

    public function makefavorite($id)
    {
        $ads = Member::select('id')->where('id', getMemberId())->first();
        if (!$ads->allads->contains($id)) {
            $ads->allads()->attach($id);
            return response()->json(['response' => array(
                'message' => 'add_success'
            )]);
        } else {
            $delete = Alladsmaster_member::where('alladsmaster_id', $id)
                ->where('member_id', getMemberId())
                ->delete();
            return response()->json(['response' => array(
                'message' => 'delete_success'
            )]);
        }
    }

    public function wishlist()
    {
        if (!Auth::guard('member')->check()) {
            return redirect('member-login');
        }
        $getParams = Input::all();
        if (!empty($getParams['type']) && $getParams['type'] == 'company') {
            session()->forget('profile_type');
            //Session::save();
        }

        $show_nav = false;
        $memberid = getMemberId();
        // member details by id
        $member = Member::where('id', $memberid)->with(['membercontacts', 'membersocial', 'otpnumbers',
                'premiummember' => function ($query) {
                    $query->with('category');
                }
            ]
        )->first();
        $provider = !empty(Session::get('provider')) ? Session::get('provider') : '';
        $memberdetails = Member::MemberDetailsByIdsocial($memberid, $provider);

        $layout = 'my_wishlist';
        if (checkCompany()) {
            $layout = 'premium.premium-wishlist';
        }
        if (temporyPrivateLogin()) {
            $layout = 'my_wishlist';
        }
        $allads = Alladsmaster_member::getfavoriteAdsForMember(getMemberId());
//        dd($allads);
        return view('member.' . $layout)
            ->with('memberdetails', $memberdetails)
            ->with('member', $member)
            ->with('allads', $allads);
    }

    /*END premium profile
     *****************************************************************************************************************************************************************************************/
    public function searchmyads(Request $request) // both premium and private ads search function
    {
        $memberid = getMemberId();
        $request->flash();
        $searchParm = $request->ad_info;
        $allads = Alladsmaster::where('member_id', $memberid)->search($searchParm)->with('district')->paginate(10);
        //dd($allads);

        $member = Member::where('id', $memberid)->with('membercontacts', 'membersocial')->first();
        $provider = !empty(Session::get('provider')) ? Session::get('provider') : '';
        $memberdetails = Member::MemberDetailsByIdsocial($memberid, $provider);

        return view('member.my_ads_search', compact('allads'))
            ->with('member', $member)
            ->with('memberdetails', $memberdetails);
    }

    public function editpostad($slug)
    {

        $ad = Alladsmaster::where('slug', $slug)->select('id', 'slug', 'member_id')->first();

        if (empty(getMemberId())) {
            if (!empty($ad)) {
                session()->forget('ad_slug');
                session(['ad_slug' => \Illuminate\Support\Facades\Request::getPathInfo()]);
                return redirect('home');
            }
            return view('member.login.login');
        }
//        dd(getPrivetMemberId());
//        dd($ad);
        if ($ad->member_id != getMemberId()) {
           abort(404);
        }


        $ad_id = (!empty($ad->id)) ? $ad->id : abort(404);
        //get only common data
        $catcode = Alladsmaster::GetCatCodedatabyid($ad_id);
        if ($catcode->status == 'pending') {
            return redirect('reviewsuccess?ref=' . $catcode->ad_referance);
        }

        //get all primary info of the ad
        $editAd = Alladsmaster::where('id', $ad_id)->select(
            'id', 'ad_referance', 'category_id', 'sub_category_id', 'member_id', 'district_id', 'city_id',
            'adtitle', 'description', 'price', 'nego', 'featuredimage',
            'itemcondition', 'itemtype', 'contact', 'status', 'slug', 'ad_boost_status', 'ad_type', 'created_at'
        )->first();
        //delete old temp images associates with current member id
        $checkForPastImages = Tempimage::where('member_id', $editAd->member_id)->delete();
        if ($checkForPastImages) {
            $imageFile = public_path() . '/saleme/images/temp/' . $editAd->member_id;
            File::deleteDirectory($imageFile);
        }

        $cat_id = $editAd->category_id;
        $sub_cat_id = $editAd->sub_category_id;
        $adtype = $editAd->ad_type;
        //set edit data

        $editAd->description = str_replace(array("<br />"), "\r\n", $editAd->description);
        $contactArr = unserialize($editAd->contact);
        $checkNum = array();

        if (!empty($contactArr)) {
            foreach ($contactArr as $number) {
                if (isset($number)) {
                    $checkNum[$number] = true; //set only available contact numbers values id's for cross check in frontend checkbox
                }
            }
        }
        $editAd->contact = $checkNum;

        /* ----------------------------------------------------------------------------------------------
         * --------------------START images for the image uploder plugin---------------------------------*/
        $uploadDir = '/saleme/images/uploads/' . $ad_id . '/';
        $images = DB::table('adimages')->where('alladsmaster_id', '=', $ad_id)->select('id', 'imagename as name')->get()->toArray();
        $adImagesJson = $images;
        //db filters from filter values table
        $filtersFromDB = array($editAd->itemcondition, $editAd->itemtype);

        // START - load filters and relevnt filter values associating with the selected sub-category
        $subCategory = Subcategory::where('id', $sub_cat_id)->with('subcategoryfilters', 'brands')->first();
        $filtersWithValues = $subCategory->subcategoryfilters->load('categoryfiltervalues');
        $filters = array();
        foreach ($filtersWithValues as $filter) {
            $filtername = ucfirst(str_replace('-', ' ', $filter->filterref));
            $filterValues = (!empty($filter->categoryfiltervalues)) ? $filter->categoryfiltervalues : '';
            foreach ($filterValues as $filterValue) {
                $filters[$filtername][$filterValue->id] = $filterValue->filtervalue;
            }
        }
        /*
         * END - load filters and relevnt filter values associating with the selected sub-category
         * load data for frontend
         * */
        $vehiclebrandlist = $subCategory->brands; //all brands assosiate with subcategory - by pivot table
        $locationdistrictslist = DB::table('location_districts')->get()->toArray(); //all locations for ads
        // category  list for side menu
        $categories = Category::Catrgorylsit();
        $ctegory = DB::table('categories')->select('id', 'category_name', 'category_code')->where('id', '=', $cat_id)->first();
        //for hidden fields values

        $basedata = array(
            'category_id' => $ctegory->id,
            'category_name' => $ctegory->category_name,
            'category_code' => $ctegory->category_code,
            'sub_cat_id' => $subCategory->id,
            'sub_cat_ref' => $subCategory->sub_category_code,
            'sub_cat_name' => $subCategory->sub_category_name,
            'ad_type' => $adtype,
        );

        $cat_code = (!empty($ctegory->category_code)) ? $ctegory->category_code : '';
        //get logged in member info
        $member = '';
        $locationcities = '';
        if (!empty($editAd->district_id)) {
            $member = Member::where('id', getMemberId())->with('membercontacts')->first();
            $locationcities = City::where('district_id', $editAd->district_id)->select('id', 'city_name')->get();
        }

        $info = Alladsfilter::where('alladsmaster_id', $ad_id)->select('categoryfiltervalue_id')->get();
        if ($info) {
            //add filter value ids to all filters
            foreach ($info as $infoitem) {
                array_push($filtersFromDB, $infoitem->categoryfiltervalue_id); //add filters from electronics table
            }
            $check = array();
            if (!empty($filtersFromDB)) {
                foreach ($filtersFromDB as $filter) {
                    if (isset($filter)) {
                        $check[$filter] = true; //set only available filter values id's for cross check in frontend selectbox
                    }
                }
            }
            $editAd->setAttribute('dbFilters', $check);
        }
        if ($adtype == 'sell' || $adtype == 'rent') {
            switch ($cat_code) {
                case 'veh':
                    $info = $this::vehicle();
                    //get selected ad info by ad type
                    $vehicleInfo = Allvehiclead::where('alladsmaster_id', $ad_id)->select(
                        'brand_id', 'model', 'bodytype_id', 'fuel_id', 'tranmission_id', 'enginesize', 'mileage', 'color', 'registry_year', 'features'
                    )->first();
                    if ($info) {
                        $editAd->setAttribute('brand_id', $vehicleInfo->brand_id);
                        $editAd->setAttribute('model', $vehicleInfo->model);
                        $editAd->setAttribute('bodytype_id', $vehicleInfo->bodytype_id);
                        $editAd->setAttribute('fuel_id', $vehicleInfo->fuel_id);
                        $editAd->setAttribute('tranmission_id', $vehicleInfo->tranmission_id);
                        $editAd->setAttribute('enginesize', $vehicleInfo->enginesize);
                        $editAd->setAttribute('mileage', $vehicleInfo->mileage);
                        $editAd->setAttribute('color', $vehicleInfo->color);
                        $editAd->setAttribute('registry_year', $vehicleInfo->registry_year);
                        $editAd->setAttribute('features', unserialize($vehicleInfo->features));
                    }
                    return view('layouts.frontend.layout-master-sub_category')
                        ->with('basedata', $basedata)
                        ->with('locationdistrictslist', $locationdistrictslist)
                        ->with('locationcities', $locationcities)
                        ->with('vehiclebrandlist', $vehiclebrandlist)
                        ->with('vehicletypelist', $info['vehicletypelist'])
                        ->with('vehicleconditionlist', $info['vehicleconditionlist'])
                        ->with('vehicletransmissionslist', $info['vehicletransmissionslist'])
                        ->with('vehiclefuellist', $info['vehiclefuellist'])
                        ->with('vehicleoptions', $info['vehicleoptions'])
                        ->with('vehiclebodytype', $info['vehiclebodytype'])
                        ->with('categories', $categories)
                        ->with('filters', $filters)
                        ->with('member', $member)
                        ->with('adImagesJson', $adImagesJson)
                        ->with('editAd', $editAd);

                    break;
                case 'ele':
                    $info = Allelectrnicad::where('alladsmaster_id', $ad_id)->select(
                        'brand_id', 'model', 'authenticity', 'features'
                    )->first();
                    if ($info) {
                        $editAd->setAttribute('brand_id', $info->brand_id);
                        $editAd->setAttribute('model', $info->model);
                        $editAd->setAttribute('features', unserialize($info->features));
                        array_push($filtersFromDB, $info->brand_id, $info->authenticity); //add filters from electronics table
                        $check = array();
                        if (!empty($filtersFromDB)) {
                            foreach ($filtersFromDB as $filter) {
                                if (isset($filter)) {
                                    $check[$filter] = true; //set only available filter values id's for cross check in frontend selectbox
                                }
                            }
                        }
                        $editAd->setAttribute('dbFilters', $check);
                    }
                    $features = DB::table('electronicoptions')->select('display_name', 'id')->get()->toArray();
                    return view('layouts.frontend.layout-master-sub_category')
                        ->with('basedata', $basedata)
                        ->with('locationcities', $locationcities)
                        ->with('vehiclebrandlist', $vehiclebrandlist)
                        ->with('locationdistrictslist', $locationdistrictslist)
                        ->with('features', $features)
                        ->with('categories', $categories)
                        ->with('filters', $filters)
                        ->with('member', $member)
                        ->with('adImagesJson', $adImagesJson)
                        ->with('editAd', $editAd);
                    break;
                case 'pro':
                    $info = Allpropertyad::where('alladsmaster_id', $ad_id)->select(
                        'landsize', 'propertysize', 'beds', 'baths', 'address', 'features'
                    )->first();
                    if ($info) {
                        $editAd->setAttribute('landsize', $info->landsize);
                        $editAd->setAttribute('propertysize', $info->propertysize);
                        $editAd->setAttribute('address', $info->address);
                        $editAd->setAttribute('features', unserialize($info->features));

                        array_push($filtersFromDB, $info->beds, $info->baths); //add filters from electronics table
                        $check = array();
                        if (!empty($filtersFromDB)) {
                            foreach ($filtersFromDB as $filter) {
                                if (isset($filter)) {
                                    $check[$filter] = true; //set only available filter values id's for cross check in frontend selectbox
                                }
                            }
                        }
                        $editAd->setAttribute('dbFilters', $check);
                    }

                    $features = DB::table('propertyoptions')->select('display_name', 'id')->get()->toArray();
                    return view('layouts.frontend.layout-master-sub_category')
                        ->with('basedata', $basedata)
                        ->with('locationdistrictslist', $locationdistrictslist)
                        ->with('locationcities', $locationcities)
                        ->with('features', $features)
                        ->with('categories', $categories)
                        ->with('filters', $filters)
                        ->with('member', $member)
                        ->with('adImagesJson', $adImagesJson)
                        ->with('editAd', $editAd);
                    break;
                case 'hote':
                    $info = Allhoteltravelad::where('alladsmaster_id', $ad_id)->select(
                        'numberofguest', 'bedsize', 'beds', 'bathrooms', 'bathroomtypes', 'numberofrooms',
                        'min_check_in_time', 'max_check_out_time', 'address', 'features','type','occupancy','size','price_meal'
                    )->first();
                    if ($info) {
                        $editAd->setAttribute('numberofguest', $info->numberofguest);
                        $editAd->setAttribute('bedsize', $info->bedsize);
                        $editAd->setAttribute('beds', $info->beds);
                        $editAd->setAttribute('bathrooms', $info->bathrooms);
                        $editAd->setAttribute('bathroomtypes', $info->bathroomtypes);
                        $editAd->setAttribute('numberofrooms', $info->numberofrooms);
                        $editAd->setAttribute('min_check_in_time', $info->min_check_in_time);
                        $editAd->setAttribute('max_check_out_time', $info->max_check_out_time);
                        $editAd->setAttribute('type', $info->type);
                        $editAd->setAttribute('address', $info->address);
                        $editAd->setAttribute('occupancy', $info->occupancy);
                        $editAd->setAttribute('size', $info->size);
                        $editAd->setAttribute('features', unserialize($info->features));

                        array_push($filtersFromDB, $info->beds, $info->bathrooms, $info->numberofguest, $info->bedsize,
                            $info->bathroomtypes, $info->numberofrooms, $info->type, $info->occupancy, $info->size); //add filters from electronics table
                        $check = array();
                        if (!empty($filtersFromDB)) {
                            foreach ($filtersFromDB as $filter) {
                                if (isset($filter)) {
                                    $check[$filter] = true; //set only available filter values id's for cross check in frontend selectbox
                                }
                            }
                        }
                        $editAd->setAttribute('dbFilters', $check);

                    }

                    $features = DB::table('hoteltraveloptions')->where('sub_cat_id', $subCategory->id)->select('display_name', 'id')->get()->toArray();
                    return view('layouts.frontend.layout-master-sub_category')
                        ->with('basedata', $basedata)
                        ->with('locationdistrictslist', $locationdistrictslist)
                        ->with('locationcities', $locationcities)
                        ->with('features', $features)
                        ->with('categories', $categories)
                        ->with('filters', $filters)
                        ->with('member', $member)
                        ->with('adImagesJson', $adImagesJson)
                        ->with('editAd', $editAd);
                    break;
                default:
                    array_push($filtersFromDB, $editAd->itemcondition, $editAd->itemtype); //add filters from electronics table
                    $check = array();
                    if (!empty($filtersFromDB)) {
                        foreach ($filtersFromDB as $filter) {
                            if (isset($filter)) {
                                $check[$filter] = true; //set only available filter values id's for cross check in frontend selectbox
                            }
                        }
                    }
                    $editAd->setAttribute('dbFilters', $check);
                    return view('layouts.frontend.layout-master-sub_category')
                        ->with('basedata', $basedata)
                        ->with('locationdistrictslist', $locationdistrictslist)
                        ->with('locationcities', $locationcities)
                        ->with('categories', $categories)
                        ->with('filters', $filters)
                        ->with('member', $member)
                        ->with('adImagesJson', $adImagesJson)
                        ->with('editAd', $editAd);
            }
        } else {
            $layout = ($adtype == 'buy') ? 'want-to-buy' : 'look-for-rent';
            array_push($filtersFromDB, $editAd->itemcondition, $editAd->itemtype); //add filters from electronics table
            $check = array();
            if (!empty($filtersFromDB)) {
                foreach ($filtersFromDB as $filter) {
                    if (isset($filter)) {
                        $check[$filter] = true; //set only available filter values id's for cross check in frontend selectbox
                    }
                }
            }
            $editAd->setAttribute('dbFilters', $check);
            return view('layouts.frontend.' . $layout)
                ->with('basedata', $basedata)
                ->with('locationdistrictslist', $locationdistrictslist)
                ->with('locationcities', $locationcities)
                ->with('categories', $categories)
                ->with('filters', $filters)
                ->with('member', $member)
                ->with('adImagesJson', $adImagesJson)
                ->with('editAd', $editAd);
        }
    }

    private static function vehicle()
    {
        $vehicletypelist = DB::table('vehicle_types')->get()->toArray();
        $vehicleconditionlist = DB::table('vehicle_condition')->get()->toArray();
        $vehiclefuellist = DB::table('vehicle_fuels')->get()->toArray();
        $vehicletransmissionslist = DB::table('vehicle_transmissions')->get()->toArray();
        $vehicleoptions = DB::table('vehicle_options')->select('display_name', 'id')->get()->toArray();
        $vehiclebodytype = DB::table('vehicle_types')->select('type_name', 'id')->get()->toArray();
        $arr = array(
            'vehicletypelist' => $vehicletypelist,
            'vehicleconditionlist' => $vehicleconditionlist,
            'vehicletransmissionslist' => $vehicletransmissionslist,
            'vehiclefuellist' => $vehiclefuellist,
            'vehicleoptions' => $vehicleoptions,
            'vehiclebodytype' => $vehiclebodytype,
        );
        return $arr;
    }

//    public function verifynumber(Request $request)
//    {
//
//        $adId = '';
//        if (!empty($request->adid)) {
//            $adId = $request->adid; //if want real ad id remove first 2 numbers ex: substr($_GET['id'], 2);
//        }
//        //save number with otp code temporally
//        if ((!empty($request->number) && empty($request->number_verify)) OR !empty($request->wrongNumberResend)) {
//            $memberId = getMemberId();
//            //check number exists and removing the trailling 0 if exists
//            $number = '';
//            if (!empty($request->number)) {
//                if (preg_match("~^0\d+$~", $request->number)) {
//                    $number = "94" . substr($request->number, 1);
//                } else {
//                    $number = "94" . $request->number;
//                }
//            }
//
//            //START tempory number add without verify - ********** IF OTP DOESN'T WORK ACTIVATE THIS ********
//            //$newContact = new Membercontact();
//            //$newContact->contactnumber = $number;
//            //$newContact->member_id = $memberId;
//            //$statusNew = $newContact->save();
//            //$insertedId = $newContact->id;
//            //if ($statusNew) {
//            //    return response()->json(['response' => array(
//            //        'message' => 'verified',
//            //        'number' => $number,
//            //        'id' => $insertedId,
//            //        'ad_id' => $adId
//            //    )]);
//            //}
//            //END tempory number add without verify
//
//            if (!empty($request->wrongNumberResend)) {
//                if (preg_match("~^0\d+$~", $request->wrongNumberResend)) {
//                    $number = "94" . substr($request->wrongNumberResend, 1);
//                } else {
//                    $number = "94" . $request->wrongNumberResend;
//                }
//            }
//
//            $request->merge(['number' => $number]);
//            $mobile = DB::table('membercontacts')->where('member_id', '=', $memberId)->where('contactnumber', '=', $number)->first();
//            if (empty($mobile)) { // resend if not verified
//                //check on OPTS table number exists if yes, delete the record
//                $oldOtp = Otp::where('number', $number)->first();
//                if (!empty($oldOtp)) {
//                    if ($oldOtp->count > 2) {
//                        return response()->json(['response' => array(
//                            'message' => 'error',
//                            'error' => 'Your PIN requests are expired. Please contact Saleme Support Team. Thank you.'
//                        )]);
//                    }
//                }
//                $code = rand(10000, 99999);
//                $smsMessage = "Dear Customer Please use the following OTP: " . $code . " to verify number.";
//                $request->merge(['message' => $smsMessage]);
//                $request->merge(['code' => $code]);
//
//                $status = '';
//                $opt = new Otp();
//                /* -------------------------------------------------------------------------------
//                     * START sms gateway code
//                     * ------------------------------------------------------------------------------- */
//                $verifySMS = Sms::Sendsms($number, $smsMessage, '1');
//                $smsResponce = json_decode($verifySMS, true);
//                $resError = !empty($smsResponce['error']) ? $smsResponce['error'] : '';
//                if ($resError == '0' OR $resError == "") {
//                    if (!empty($oldOtp)) {
//                        $status = $opt->updatedata($request->all(), $oldOtp->id); //update the count
//                    } else {
//                        $status = $opt->savedata($request->all());
//                    }
//                } else {
//                    $errormessage = '';
//                    if ($resError == '110') {
//                        $errormessage = 'Invalid Number';
//                        return response()->json(['response' => array('message' => 'error',
//                            'error' => '<strong>' . $errormessage . '</strong>! Please enter a correct Number.', 'developer' => $smsResponce)]);
//                    } elseif ($resError == '104') {
//                        return response()->json(['response' => array('message' => 'error',
//                            'error' => '<strong>Invalid Token. Refresh the Window</strong>!', 'developer' => $smsResponce)]);
//                    } else {
//                        $errormessage = 'PIN sent failed. Code - ' . $resError . '. Please Contact Saleme Support Team.';
//                        $adminUsers = User::role('superadmin')->get();
//                        foreach ($adminUsers as $user) {
//                            $user->notify(new SalemeAdminNotification($errormessage));
//                        }
//                        return response()->json(['response' => array(
//                            'message' => 'error',
//                            'error' => $errormessage,
//                            'developer' => $smsResponce
//                        )]);
//                    }
//                }
//                $res = ($resError == '0' OR $resError == "") ? 'success' : 'error-code';
//                /* -------------------------------------------------------------------------------
//                 * END sms gateway code
//                 * ------------------------------------------------------------------------------- */
//                //send text message
//                if ($status) {
//                    return response()->json(['response' => array(
//                        'developer' => $verifySMS,
//                        'message' => $res,
//                        'number_for_resend' => $number
//                    )]);
//                }
//            } else {
//                return response()->json(['response' => array(
//                    'message' => 'error',
//                    'error' => 'Sorry! This mobile number already exist.',
//                )]);
//            }
//        }
//
//        //verify sent code
//        if (!empty($request->number_verify)) {
//            //set number OLD - $request->number from send otp form.
//            $numberRequest = $request->resend;
//            $number = $numberRequest;
//
//            $request->merge(['number' => $number]);
//            $otp_info = Otp::where('code', $request->number_verify)->where('number', $numberRequest)->first();
//            if (empty($otp_info)) {
//                return response()->json(['response' => array(
//                    'message' => 'error',
//                    'error' => 'OTP is Wrong. Please Check the code and enter Again',
//                )]);
//            }
//            $number = $otp_info->number;
//            //set data to insert
//            $contactinfo = array(
//                'member_id' => getMemberId(),
//                'contactnumber' => $number,
//            );
//
//            $newContact = new Membercontact($contactinfo);
//            $statusNew = $newContact->save();
//            $insertedId = $newContact->id;
//            if ($statusNew) {
//                $otp_info->delete(); //delete opt record
//                return response()->json(['response' => array(
//                    'message' => 'verified',
//                    'number' => $number,
//                    'id' => $insertedId,
//                    'ad_id' => $adId
//                )]);
//            }
//        }
//    }


    public function verifynumber(Request $request)
    {

        $adId = '';
        if (!empty($request->adid)) {
            $adId = $request->adid; //if want real ad id remove first 2 numbers ex: substr($_GET['id'], 2);
        }
        //save number with otp code temporally
        if ((!empty($request->number) && empty($request->number_verify)) OR !empty($request->wrongNumberResend)) {
            $memberId = getMemberId();
            //check number exists and removing the trailling 0 if exists
            $number = '';
//            if (!empty($request->number)) {
//                if (preg_match("~^0\d+$~", $request->number)) {
//                    $number = "94" . substr($request->number, 1);
//                } else {
//                    $number = "94" . $request->number;
//                }
//            }

            $number =$request->number;

            //START tempory number add without verify - ********** IF OTP DOESN'T WORK ACTIVATE THIS ********
            //$newContact = new Membercontact();
            //$newContact->contactnumber = $number;
            //$newContact->member_id = $memberId;
            //$statusNew = $newContact->save();
            //$insertedId = $newContact->id;
            //if ($statusNew) {
            //    return response()->json(['response' => array(
            //        'message' => 'verified',
            //        'number' => $number,
            //        'id' => $insertedId,
            //        'ad_id' => $adId
            //    )]);
            //}
            //END tempory number add without verify

//            if (!empty($request->wrongNumberResend)) {
//                if (preg_match("~^0\d+$~", $request->wrongNumberResend)) {
//                    $number = "94" . substr($request->wrongNumberResend, 1);
//                } else {
//                    $number = "94" . $request->wrongNumberResend;
//                }
//            }

            $request->merge(['number' => $number]);
            $mobile = DB::table('membercontacts')->where('member_id', '=', $memberId)->where('contactnumber', '=', $number)->first();
//            if (empty($mobile)) { // resend if not verified
//                //check on OPTS table number exists if yes, delete the record
//                $oldOtp = Otp::where('number', $number)->first();
//                if (!empty($oldOtp)) {
//                    if ($oldOtp->count > 2) {
//                        return response()->json(['response' => array(
//                            'message' => 'error',
//                            'error' => 'Your PIN requests are expired. Please contact Saleme Support Team. Thank you.'
//                        )]);
//                    }
//                }
//                $code = rand(10000, 99999);
//                $smsMessage = "Dear Customer Please use the following OTP: " . $code . " to verify number.";
//                $request->merge(['message' => $smsMessage]);
//                $request->merge(['code' => $code]);
//
//                $status = '';
//                $opt = new Otp();
//                /* -------------------------------------------------------------------------------
//                     * START sms gateway code
//                     * ------------------------------------------------------------------------------- */
//                $verifySMS = Sms::Sendsms($number, $smsMessage, '1');
//                $smsResponce = json_decode($verifySMS, true);
//                $resError = !empty($smsResponce['error']) ? $smsResponce['error'] : '';
//                if ($resError == '0' OR $resError == "") {
//                    if (!empty($oldOtp)) {
//                        $status = $opt->updatedata($request->all(), $oldOtp->id); //update the count
//                    } else {
//                        $status = $opt->savedata($request->all());
//                    }
//                } else {
//                    $errormessage = '';
//                    if ($resError == '110') {
//                        $errormessage = 'Invalid Number';
//                        return response()->json(['response' => array('message' => 'error',
//                            'error' => '<strong>' . $errormessage . '</strong>! Please enter a correct Number.', 'developer' => $smsResponce)]);
//                    } elseif ($resError == '104') {
//                        return response()->json(['response' => array('message' => 'error',
//                            'error' => '<strong>Invalid Token. Refresh the Window</strong>!', 'developer' => $smsResponce)]);
//                    } else {
//                        $errormessage = 'PIN sent failed. Code - ' . $resError . '. Please Contact Saleme Support Team.';
//                        $adminUsers = User::role('superadmin')->get();
//                        foreach ($adminUsers as $user) {
//                            $user->notify(new SalemeAdminNotification($errormessage));
//                        }
//                        return response()->json(['response' => array(
//                            'message' => 'error',
//                            'error' => $errormessage,
//                            'developer' => $smsResponce
//                        )]);
//                    }
//                }
//                $res = ($resError == '0' OR $resError == "") ? 'success' : 'error-code';
//                /* -------------------------------------------------------------------------------
//                 * END sms gateway code
//                 * ------------------------------------------------------------------------------- */
//                //send text message
//                if ($status) {
//                    return response()->json(['response' => array(
//                        'developer' => $verifySMS,
//                        'message' => $res,
//                        'number_for_resend' => $number
//                    )]);
//                }
//            } else {
//                return response()->json(['response' => array(
//                    'message' => 'error',
//                    'error' => 'Sorry! This mobile number already exist.',
//                )]);
//            }
            $contactinfo = array(
                'member_id' => getMemberId(),
                'contactnumber' => $number,
            );

            $newContact = new Membercontact($contactinfo);
            $statusNew = $newContact->save();
            $insertedId = $newContact->id;
            return response()->json(['response' => array(
                'message' => 'verified',
                'number' => $number,
                'id' => $insertedId,
                'ad_id' => $adId
            )]);
        }

        //verify sent code
//        if (!empty($request->number_verify)) {
//            //set number OLD - $request->number from send otp form.
//            $numberRequest = $request->resend;
//            $number = $numberRequest;
//
//            $request->merge(['number' => $number]);
//            $otp_info = Otp::where('code', $request->number_verify)->where('number', $numberRequest)->first();
//            if (empty($otp_info)) {
//                return response()->json(['response' => array(
//                    'message' => 'error',
//                    'error' => 'OTP is Wrong. Please Check the code and enter Again',
//                )]);
//            }
//            $number = $otp_info->number;
//            //set data to insert
//            $contactinfo = array(
//                'member_id' => getMemberId(),
//                'contactnumber' => $number,
//            );
//
//            $newContact = new Membercontact($contactinfo);
//            $statusNew = $newContact->save();
//            $insertedId = $newContact->id;
//            if ($statusNew) {
//                $otp_info->delete(); //delete opt record
//                return response()->json(['response' => array(
//                    'message' => 'verified',
//                    'number' => $number,
//                    'id' => $insertedId,
//                    'ad_id' => $adId
//                )]);
//            }
//        }
    }

    public function sendinquirymail(Request $request)
    {
        if (!empty($request->ad_id)) {
            $id = $request->ad_id;
            $selectedAd = Alladsmaster::with([
                'member' => function ($query) {
                    $query->select('id', 'first_name', 'email');
                }])
                ->select('id', 'member_id', 'adtitle', 'slug')
                ->find($id);
            //set mail recipient to post ad user email - to mail
            $selectedAd->setAttribute('email', $selectedAd->member->email);
            $arr = array(
                'name' => $request->name,
                'email' => $request->email,
                'mobile' => $request->mobile,
                'message' => $request->message,
                'ad_title' => $selectedAd->adtitle,
                'slug' => $selectedAd->slug,
            );
            $selectedAd->notify(new AdInquiry($arr, $selectedAd));
            return response()->json(['response' => array('message' => 'success')]);
        }
    }

    /***************************************************************************************************************************************************
     * START Members section
     * */
    public function allmemberslist()
    {
        $members = Member::paginate(10);
        return view('backend.member.allmembers', compact('members'));
    }

    public function premiumrequest()
    {
        $premiumRequests = Memberpremium::with('member')->where('verified', false)->paginate(10);
        return view('backend.member.premium-request', compact('premiumRequests'));
    }

    public function approvepremiumrequest(Request $request)
    {
        $member = Member::find($request->memberid);
        $member->member_type = 'premium';
        $member->update();

        $memberPremiums = Memberpremium::where('member_id', $request->memberid)->first();
        $memberPremiums->verified = true;
        $memberPremiums->update();

        if (count($memberPremiums->mobile)) {
            $numbers = array_slice($memberPremiums->mobile, 0, 2);

            $smsMessage = 'Thank you for your payment for SaleMe.lk premium member .Visit :https://saleme.lk/' . $memberPremiums->slug . ' for your website';
            foreach ($numbers as $number) {
                $verifySMS = Sms::Sendsms($number, $smsMessage, '9');
                $smsResponce = json_decode($verifySMS, true);

            }
        }
        if ($smsResponce['error'] == 0 OR $smsResponce['error'] == '') {
            flash('SMS sent Successfuly.', 'success');

        } else {
            flash($smsResponce['error'], 'error');
        }
        flash('Thank you! Member accepted as Premium Member.', 'success');
        return back();
    }

    public function deletepremiumrequest(Request $request){
        $memberPremiums = Memberpremium::where('member_id', $request->memberid)->first();
        $memberPremiums->delete();
        return back();

    }

    public function sendsmspremiummebmer(Request $request)
    {

        $memberPremiums = Memberpremium::where('member_id', $request->memberid)->first();
        if (count($memberPremiums->mobile)) {
            $numbers = array_slice($memberPremiums->mobile, 0, 2);

            $smsMessage = 'Your Payment for Premium Membership.Please deposit LKR 12000/- and send us bank slip by Viber or WhatsApp to 0763818188.Banking details as follows SaleMe.lk (pvt) Limited Sampath Bank - A/C 010010005103 Thank you. 24/7 Support Team 0115 818188';
            foreach ($numbers as $number) {
                $verifySMS = Sms::Sendsms($number, $smsMessage, '9');
                $smsResponce = json_decode($verifySMS, true);

            }
        }
        if ($smsResponce['error'] == 0 OR $smsResponce['error'] == '') {
            flash('SMS sent Successfuly.', 'success');

        } else {
            flash($smsResponce['error'], 'error');
        }
        return back();
    }

    /*
     * END Member section
     * START boosting
     ****************************************************************************************************************************************************/

    public function boostad($slug, $boosttype)
    {
        $info = Alladsmaster::where('slug', $slug)
            ->select('id', 'slug', 'ad_referance', 'member_id', 'category_id', 'sub_category_id')
            ->with('member', 'category')
            ->first();
        $adPacks = Boostpack::where('packtype', 'single')->where('referance', $boosttype)->select('price', 'validitymonths')->first();

        $subCategory = Subcategory::where('id', $info->sub_category_id)->select('sub_category_name')->first();
        $memberVouchers = new Membervoucher();
        $voucherCodes = $memberVouchers->getAllActiveVouchersByMember(getMemberId(), $boosttype); //get activated boots options

        //check for pending pin-to-top ads
        $chkInactiveVouchers = $memberVouchers->checkInactiveVouchersByMember(getMemberId(), $boosttype);
        $chkInactiveVouchers = Common::object_to_array($chkInactiveVouchers);
        $checkPendinBoostForReleventAd = Membervouchercode::where('ad_referance', $info->ad_referance)
            ->where('status', false)->first();
//        dd($checkPendinBoostForReleventAd);

        //check for current ad already pinned
        $pinStatus = Boostviewcount::where('alladsmaster_id', $info->id)->first();
//        dd($info);
        return view('member.boosting.pin-to-top', compact('info', 'boosttype', 'checkPendinBoostForReleventAd', 'voucherCodes', 'subCategory', 'chkInactiveVouchers', 'pinStatus', 'adPacks'));
    }

    public function pintotopboostpack($slug)
    {
        $ad = Alladsmaster::where('slug', $slug)->select('id', 'slug', 'ad_referance', 'member_id')->with('member')->first();
        $info = $ad;
        return view('member.boosting.packs', compact('info'));
    }

    public function boostpintotopreqest(Request $request)
    {
//        dd($request->all());
        //create new boosting
        $voucher = new Membervoucher();
        $voucherInvoiceCode = self::genereateBoostInvoiceNo(); //GENERATE INVOICE CODE FOR ADS PACK
        $request->merge(['boostpackinvoiceno' => $voucherInvoiceCode]);
        $returnURL = '';
        $email = true;

        if (!empty($request->ad_referance)) {
            //if ad_referance is exist any member can pin their ad.
            if (!empty($request->vouchercode) && !empty($request->member_id)) {
                //use the existing voucher code for pin ad to top - AdsreviewController - activateboostpintotopsingle action
                //directly activate the ad by member - if any previous boost options exist
                $voucherCodeId = $request->vouchercode;
                $updatecheck = Membervouchercode::where('id', $voucherCodeId)
                    ->where('boosttype', $request->boosttype)
                    ->update([
                        'ad_referance' => $request->ad_referance,
                        'status' => true,
                        'activated_at' => Carbon::now()
                    ]);
                if ($updatecheck) {
                    Boostviewcount::create([
                        'alladsmaster_id' => $request->adid
                    ]);
                    $ad = Alladsmaster::where('ad_referance', $request->ad_referance)
                        ->update([
                            'ad_boost_status' => $request->boosttype,
                            'boost_expire_on' => Carbon::now()->addDays(7)->toDateTimeString()
                        ]);
                }
                //send boost success email
                $member = getMember();
                $ad = Alladsmaster::find($request->adid);
                $member->notify(new PintotopRequest($ad));
                $email = false;
                flash('Success! Your Ad Pinned Successfully! It will be Removed from Pin-to-Top List after 7 days.', 'success');
                $returnURL = '/myprofile';
            } else {
                //Request a pin-to-top ad by member private
                $code = new UUID('voucher' . $request->adid . '_');
                $checkVoucherCode = Membervouchercode::where('vouchercode', $code)->first();
                if (empty($checkVoucherCode)) {
                    $arr[$request->boosttype] = $code->uuid;
                    $voucher->newVoucherForAd($request->all(), $arr);
                    flash('Success! Your Ad Boost Request is Successfull. Confirm your payment to Activate Automatically.', 'success');
                    $returnURL = '/my_ads';
                }
            }
        } else {
            if (getMemberType() != 'private') {
                //for premium members - they can buy boosting pack as well as boots a single ad
                //private member cannot have an ads boosting pack
                if ($request->ads_pack == 'true') {
                    $pin_count = $request->pintotop_count;
                    $bump_count = $request->bump_up_count;
                    $voucherCodes['pin-to-top'] = self::generateVoucherCodes($pin_count, getMemberId());
                    //$voucherCodes['bump-up'] = self::generateVoucherCodes($bump_count, getMemberId());
                    $voucher->newVoucherForPacks($request->all(), $voucherCodes);
                    flash('Success! Your Ad Boost Pack Request is Successfull! Confirm your payment to Activate Automatically.', 'success');
                    $returnURL = '/myprofile';
                }
            }
        }

        //pintotop email start
        if ($email) {
            if ($request->adid) {
                $mail = Alladsmaster::with('member')->where('id', $request->adid)->first();
            } else {
                $mail = Boostpack::find($request->boostpack_id);
            }
            $member = getMember();
            $member->notify(new PintotopRequest($mail, $request->price));
        }
        return redirect($returnURL);
    }

    protected static function generateVoucherCodes($count, $adId)
    {
        $voucherCodes = array();
        while (count($voucherCodes) < $count) {
            $code = new UUID('voucher' . $adId . '_');
            $checkVoucherCode = Membervouchercode::where('vouchercode', $code)->first();
            if (empty($checkVoucherCode)) {
                $voucherCodes[] = $code->uuid;
            }
        }
        return $voucherCodes;
    }

    protected static function genereateBoostInvoiceNo()
    {
        $string = 'SLM' . getMemberId() . '-' . date('ymd');
        $code = $string . '-' . substr(md5(microtime()), 0, 4);
        $checkBoostVoucherInvoiceCode = Membervoucher::where('boostpackinvoiceno', $code)->first();

        $unique = (empty($checkBoostVoucherInvoiceCode)) ? true : false;
        while (!$unique) {
            $code = $string . '-' . str_random(4); //GENERATE INVOICE CODE FOR ADS PACK
            $checkBoostVoucherInvoiceCode = Membervoucher::where('boostpackinvoiceno', $code)->first();
            if (empty($checkBoostVoucherInvoiceCode)) {
                $unique = true;
            }
        }
        return $code;
    }

}
