<?php

namespace App\Http\Controllers\playzone;

use App\Mail\RedeemItem;
use App\Models\HomeBanner;
use App\Models\Member;
use App\Models\Playzone_buy_request;
use App\Models\Playzone_dailyquestion;
use App\Models\Playzone_extraquestion;
use App\Models\Playzone_member;
use App\Models\Playzone_mission;
use App\Models\Playzone_store;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class playzoneController extends Controller
{
    public function playzoneLanding(){
        session(['is_game' => true]);
        if (!empty(getMemberId())) {
            return redirect('/playzone/home');
        }
//        banner random
        $image = HomeBanner::select('filename')
            ->where('status',1)
            ->where('location','playzone')
            ->orderByRaw("RAND()")
            ->first();
        return view('gamezone.playzone-enter')
            ->with('image',$image);
    }

    public function playzoneRegister(){
//        dd(asset(''));
        session(['is_game' => true]);
        if (!empty(getMemberId())) {
            return redirect('/playzone/home');
        }
        $image = HomeBanner::select('filename')
            ->where('status',1)
            ->where('location','playzone')
            ->orderByRaw("RAND()")
            ->first();
        return view('gamezone.playzone-register')
            ->with('image',$image);
    }

    public function index()
    {
        $memberid = getPrivetMemberId();
        $provider = !empty(Session::get('provider')) ? Session::get('provider') : '';
        $memberdetails = Member::MemberDetailsByIdsocial($memberid, $provider);
        $missiondata = Playzone_mission::where('member_id', $memberid)->first();
        if (!is_null($missiondata)) {
            $level = self::checkLevel($missiondata->final_score);
        }
        $level = array('currentlevel' => 'Elite', 'nextLevel' => 'Knight', 'current' => 1, 'next' => 2);

        $image = HomeBanner::select('filename')
            ->where('status',1)
            ->where('location','playzone_inner')
            ->orderByRaw("RAND()")
            ->first();

        return view('gamezone.index')
            ->with('memberdetails', $memberdetails)
            ->with('level', $level)
            ->with('missiondata', $missiondata)
            ->with('image',$image);
    }


    public function prizes()
    {
        $memberid = getPrivetMemberId();
        $provider = !empty(Session::get('provider')) ? Session::get('provider') : '';
        $memberdetails = Member::MemberDetailsByIdsocial($memberid, $provider);
        $missiondata = Playzone_mission::where('member_id', $memberid)->first();
        $prizes = playzone_store::where('is_prize', 1)->get()->toArray();
        if (!is_null($missiondata)) {
            $level = self::checkLevel($missiondata->final_score);
        }
        $level = array('currentlevel' => 'Elite', 'nextLevel' => 'Knight', 'current' => 1, 'next' => 2);
        return view('gamezone.prizes')
            ->with('missiondata', $missiondata)
            ->with('memberdetails', $memberdetails)
            ->with('level', $level)
            ->with('prizes', $prizes);
    }

    public function store()
    {
        $memberid = getPrivetMemberId();
        $provider = !empty(Session::get('provider')) ? Session::get('provider') : '';
        $memberdetails = Member::MemberDetailsByIdsocial($memberid, $provider);
        $missiondata = Playzone_mission::where('member_id', $memberid)->first();
        $prizes = playzone_store::where('status', 'active')
            ->get()->toArray();
        if (!is_null($missiondata)) {
            $level = self::checkLevel($missiondata->final_score);
        }
        $level = array('currentlevel' => 'Elite', 'nextLevel' => 'Knight', 'current' => 1, 'next' => 2);
        return view('gamezone.store')
            ->with('missiondata', $missiondata)
            ->with('level', $level)
            ->with('memberdetails', $memberdetails)
            ->with('prizes', $prizes);
    }

    public function itemSingle($id)
    {
        $memberid = getPrivetMemberId();
        $provider = !empty(Session::get('provider')) ? Session::get('provider') : '';
        $memberdetails = Member::MemberDetailsByIdsocial($memberid, $provider);
        $missiondata = Playzone_mission::where('member_id', $memberid)->first();
        $item = Playzone_store::where('id', $id)->first();
        $playzonemember = Playzone_member::where('member_id', $memberid)->first();
        if (!is_null($missiondata)) {
            $level = self::checkLevel($missiondata->final_score);
        }
        $level = array('currentlevel' => 'Elite', 'nextLevel' => 'Knight', 'current' => 1, 'next' => 2);
        return view('gamezone.item-single')
            ->with('missiondata', $missiondata)
            ->with('level', $level)
            ->with('playzonemember', $playzonemember)
            ->with('memberdetails', $memberdetails)
            ->with('item', $item);
    }

    public function profile()
    {
        $memberid = getPrivetMemberId();
        $provider = !empty(Session::get('provider')) ? Session::get('provider') : '';
        $memberdetails = Member::MemberDetailsByIdsocial($memberid, $provider);
        $missiondata = Playzone_mission::where('member_id', $memberid)->first();
        if (!is_null($missiondata)) {
            $level = self::checkLevel($missiondata->final_score);
        }
        $level = array('currentlevel' => 'Elite', 'nextLevel' => 'Knight', 'current' => 1, 'next' => 2);
        $playzone_member = Playzone_member::where('member_id', $memberid)->with('member')->first();
        $purchase = Playzone_buy_request::purchesHistory($memberid);
//        dd($purchase);

        $image = HomeBanner::select('filename')
            ->where('status',1)
            ->where('location','playzone_inner')
            ->orderByRaw("RAND()")
            ->first();
        return view('gamezone.profile')
            ->with('missiondata', $missiondata)
            ->with('level', $level)
            ->with('purchase', $purchase)
            ->with('playzone_member', $playzone_member)
            ->with('memberdetails', $memberdetails)
            ->with('image', $image);
    }

    public function ranks()
    {
        $memberid = getPrivetMemberId();
        $provider = !empty(Session::get('provider')) ? Session::get('provider') : '';
        $memberdetails = Member::MemberDetailsByIdsocial($memberid, $provider);
        $missiondata = Playzone_mission::where('member_id', $memberid)->first();
        if (!is_null($missiondata)) {
            $level = self::checkLevel($missiondata->final_score);
        }
        $level = array('currentlevel' => 'Elite', 'nextLevel' => 'Knight', 'current' => 1, 'next' => 2);
        $playzone_member = Playzone_member::where('member_id', $memberid)->with('member')->first();
        $purchase = Playzone_buy_request::purchesHistory($memberid);
//        dd($purchase);
        $image = HomeBanner::select('filename')
            ->where('status',1)
            ->where('location','playzone_inner')
            ->orderByRaw("RAND()")
            ->first();
        return view('gamezone.rank')
            ->with('missiondata', $missiondata)
            ->with('level', $level)
            ->with('purchase', $purchase)
            ->with('playzone_member', $playzone_member)
            ->with('image', $image)
            ->with('memberdetails', $memberdetails);
    }

    public function playzone()
    {
        $memberid = getPrivetMemberId();
        $provider = !empty(Session::get('provider')) ? Session::get('provider') : '';
        $memberdetails = Member::MemberDetailsByIdsocial($memberid, $provider);
        $missiondata = Playzone_mission::where('member_id', $memberid)->first();
        if (!is_null($missiondata)) {
            $level = self::checkLevel($missiondata->final_score);
        }
        $level = array('currentlevel' => 'Elite', 'nextLevel' => 'Knight', 'current' => 1, 'next' => 2);
        $image = HomeBanner::select('filename')
            ->where('status',1)
            ->where('location','playzone_inner')
            ->orderByRaw("RAND()")
            ->first();
        return view('gamezone.playzone')
            ->with('memberdetails', $memberdetails)
            ->with('level', $level)
            ->with('image', $image)
            ->with('missiondata', $missiondata);
    }

    public function dailyQuestion()
    {
        $memberid = getPrivetMemberId();
        $provider = !empty(Session::get('provider')) ? Session::get('provider') : '';
        $memberdetails = Member::MemberDetailsByIdsocial($memberid, $provider);
        $missiondata = Playzone_mission::where('member_id', $memberid)->first();
        if (!is_null($missiondata)) {
            if ($missiondata->daily_question_last_update_at == date('Y-m-d')) {
                return redirect('playzone/home');
            }
        }
        $count = Playzone_dailyquestion::where('status', 'active')->count();
        $ids = Playzone_dailyquestion::select('id')->where('status', 'active')->get()->toArray();
        $id = $ids[rand(0, $count - 1)];
        $question = Playzone_dailyquestion::where('id', $id)->first();
        if (!is_null($missiondata)) {
            $level = self::checkLevel($missiondata->final_score);
        }
        $level = array('currentlevel' => 'Elite', 'nextLevel' => 'Knight', 'current' => 1, 'next' => 2);
        return view('gamezone.dailyquestion')
            ->with('memberdetails', $memberdetails)
            ->with('level', $level)
            ->with('missiondata', $missiondata)
            ->with('question', $question);
    }

    public function extraQuestion()
    {
        $memberid = getPrivetMemberId();
        $provider = !empty(Session::get('provider')) ? Session::get('provider') : '';
        $memberdetails = Member::MemberDetailsByIdsocial($memberid, $provider);
        $missiondata = Playzone_mission::where('member_id', $memberid)->first();
//        dd($missiondata->daily_question_last_update_at);
        if (!is_null($missiondata) AND !is_null($missiondata->extra_question_last_update_at)) {
            if (date('Y-m-d', strtotime($missiondata->extra_question_last_update_at . ' + 2 days')) > date('Y-m-d')) {
                return redirect('playzone/home');
            }
        }
        $count = Playzone_extraquestion::where('status', 'active')->count();
        $ids = Playzone_extraquestion::select('id')->where('status', 'active')->get()->toArray();
        $id = $ids[rand(0, $count - 1)];
        $question = Playzone_extraquestion::where('id', $id)->first();
        if (!is_null($missiondata)) {
            $level = self::checkLevel($missiondata->final_score);
        }
        $level = array('currentlevel' => 'Elite', 'nextLevel' => 'Knight', 'current' => 1, 'next' => 2);
//        dd(unserialize($question->answers));
        return view('gamezone.extraquestion')
            ->with('memberdetails', $memberdetails)
            ->with('level', $level)
            ->with('missiondata', $missiondata)
            ->with('question', $question);
    }

    public function facebookmissionslist()
    {
        $memberid = getPrivetMemberId();
        $provider = !empty(Session::get('provider')) ? Session::get('provider') : '';
        $memberdetails = Member::MemberDetailsByIdsocial($memberid, $provider);
        $missiondata = Playzone_mission::where('member_id', $memberid)->first();
        if (!is_null($missiondata)) {
            $level = self::checkLevel($missiondata->final_score);
        }
        $level = array('currentlevel' => 'Elite', 'nextLevel' => 'Knight', 'current' => 1, 'next' => 2);
//        dd(unserialize($question->answers));
        return view('gamezone.facebook-missions')
            ->with('memberdetails', $memberdetails)
            ->with('level', $level)
            ->with('missiondata', $missiondata);
    }

    public function googlemissionslist()
    {
        $memberid = getPrivetMemberId();
        $provider = !empty(Session::get('provider')) ? Session::get('provider') : '';
        $memberdetails = Member::MemberDetailsByIdsocial($memberid, $provider);
        $missiondata = Playzone_mission::where('member_id', $memberid)->first();
        if (!is_null($missiondata)) {
            $level = self::checkLevel($missiondata->final_score);
        }
        $level = array('currentlevel' => 'Elite', 'nextLevel' => 'Knight', 'current' => 1, 'next' => 2);
//        dd(unserialize($question->answers));
        return view('gamezone.google-missions')
            ->with('memberdetails', $memberdetails)
            ->with('level', $level)
            ->with('missiondata', $missiondata);
    }

    public function othermissionslist()
    {
        $memberid = getPrivetMemberId();
        $provider = !empty(Session::get('provider')) ? Session::get('provider') : '';
        $memberdetails = Member::MemberDetailsByIdsocial($memberid, $provider);
        $missiondata = Playzone_mission::where('member_id', $memberid)->first();
        if (!is_null($missiondata)) {
            $level = self::checkLevel($missiondata->final_score);
        }
        $level = array('currentlevel' => 'Elite', 'nextLevel' => 'Knight', 'current' => 1, 'next' => 2);
//        dd(unserialize($question->answers));
        return view('gamezone.other-social-missions')
            ->with('memberdetails', $memberdetails)
            ->with('level', $level)
            ->with('missiondata', $missiondata);
    }

//    check user levels
    private function checkLevel($finalScore)
    {
        $levels = array('Elite', 'Knight', 'Champion', 'Legend', 'King', 'Emperor', 'God', 'Titan');
        if ($finalScore < 150) {
            $data = array('currentlevel' => $levels[0], 'nextLevel' => $levels[1], 'current' => 1, 'next' => 2);
            return $data;
        } elseif ($finalScore >= 150 and $finalScore < 250) {
            $data = array('currentlevel' => $levels[1], 'nextLevel' => $levels[2], 'current' => 2, 'next' => 3);
            return $data;
        } elseif ($finalScore >= 250 and $finalScore < 400) {
            $data = array('currentlevel' => $levels[2], 'nextLevel' => $levels[3], 'current' => 3, 'next' => 4);
            return $data;
        } elseif ($finalScore >= 400 and $finalScore < 650) {
            $data = array('currentlevel' => $levels[3], 'nextLevel' => $levels[4], 'current' => 4, 'next' => 5);
            return $data;
        } elseif ($finalScore >= 650 and $finalScore < 1000) {
            $data = array('currentlevel' => $levels[4], 'nextLevel' => $levels[5], 'current' => 5, 'next' => 6);
            return $data;
        } elseif ($finalScore >= 1000 and $finalScore < 1400) {
            $data = array('currentlevel' => $levels[5], 'nextLevel' => $levels[6], 'current' => 6, 'next' => 7);
            return $data;
        } elseif ($finalScore >= 1400 and $finalScore < 2000) {
            $data = array('currentlevel' => $levels[6], 'nextLevel' => $levels[7], 'current' => 7, 'next' => 8);
            return $data;
        } else {
            $data = array('currentlevel' => $levels[7], 'nextLevel' => $levels[7], 'current' => 8, 'next' => 8);
            return $data;
        }
    }

//    save data of missions
    public function collectDailyPoint(Request $request)
    {
        $memberid = getPrivetMemberId();
        $member = Playzone_mission::where('member_id', $memberid)->first();
        if (!$member) {
            $member = new Playzone_mission();
            $member->member_id = $memberid;
            $member->final_score = 1;
            $member->current_score = 1;
        } else {
            $member->final_score = $member->final_score + 1;
            $member->current_score = $member->current_score + 1;
        }
        $member->daily_coin_last_update_at = date('Y-m-d');
        $result = $member->save();
        if ($result) {
            return response()->json([
                'success' => $result,
                'message' => 'Daily SaleMe Coin added successfully',
                'css' => 'alert-success',
                'icon' => 'now-ui-icons ui-2_like',
            ]);
        } else {
            return response()->json([
                'success' => $result,
                'message' => 'Daily SaleMe Coin not added successfully',
                'css' => 'alert-danger',
                'icon' => 'now-ui-icons travel_info',
            ]);
        }
    }

    public function collectDailyQuestionPoint(Request $request)
    {
        $memberid = getPrivetMemberId();
        $member = Playzone_mission::where('member_id', $memberid)->first();
        if (!$member) {
            $member = new Playzone_mission();
            $member->member_id = $memberid;
            $member->final_score = 1;
            $member->current_score = 1;
        } else {
            $member->current_score = $member->current_score + 1;
            $member->final_score = $member->final_score + 1;
        }
        $member->daily_question_last_update_at = date('Y-m-d');
        $result = $member->save();
        if ($result) {
            return response()->json([
                'success' => $result,
                'message' => 'Daily Question answered successfully',
                'css' => 'alert-success',
                'icon' => 'now-ui-icons ui-2_like',
            ]);
        } else {
            return response()->json([
                'success' => $result,
                'message' => 'Daily Question not answered successfully',
                'css' => 'alert-danger',
                'icon' => 'now-ui-icons travel_info',
            ]);
        }
    }

    public function collectExtraQuestionPoint(Request $request)
    {

        $memberid = getPrivetMemberId();
        $member = Playzone_mission::where('member_id', $memberid)->first();
        if (!$member) {
            $member = new Playzone_mission();
            $member->member_id = $memberid;
            $member->final_score = 1;
            $member->current_score = 1;
        } else {
//            dd($member->current_score);
            $member->final_score = $member->final_score + 1;
            $member->current_score = $member->current_score + 1;
        }
        $member->extra_question_last_update_at = date('Y-m-d');
        $result = $member->save();
        if ($result) {
            return response()->json([
                'success' => $result,
                'message' => 'Extra Question answered successfully',
                'css' => 'alert-success',
                'icon' => 'now-ui-icons ui-2_like',
            ]);
        } else {
            return response()->json([
                'success' => $result,
                'message' => 'Extra Question not answered successfully',
                'css' => 'alert-danger',
                'icon' => 'now-ui-icons travel_info',
            ]);
        }
    }

    public function collectfacebooklike(Request $request, $share = null)
    {
//        $share= $request->all();
//        dd($share);
        $memberid = getPrivetMemberId();
        $member = Playzone_mission::where('member_id', $memberid)->first();
        if (!$member) {
            $member = new Playzone_mission();
            $member->member_id = $memberid;
            $member->final_score = 5;
            $member->current_score = 5;
        } else {
            if ($share == 'share') {
                if ($member->facebook_page_share) {
                    $request->session()->flash('alert-danger', 'You have already shared page');
                    return redirect('playzone/play');
                } else {
                    $member->current_score = $member->current_score + 5;
                    $member->final_score = $member->final_score + 5;
                }
            } else {
                if ($member->facebook_page_like) {
                    $request->session()->flash('alert-danger', 'You have already liked to page');
                    return redirect('playzone/play');
                } else {
                    $member->final_score = $member->final_score + 5;
                    $member->current_score = $member->current_score + 5;
                }
            }

        }
        if ($share == 'share') {
            $member->facebook_page_share = 1;
            $result = $member->save();
            if ($result) {
                $request->session()->flash('alert-success', 'Facebook page shared. 5 points added to your score');
                return redirect('playzone/play');

            } else {
                $request->session()->flash('alert-danger', 'Facebook page not shared. please try again');
                return redirect('playzone/play');
            }
        }

        $member->facebook_page_like = 1;
        $result = $member->save();
        if ($result) {
            $request->session()->flash('alert-success', 'Facebook page liked. 5 points added to your score');
            return redirect('playzone/play');

        } else {
            $request->session()->flash('alert-danger', 'Facebook page not liked. please try again');
            return redirect('playzone/play');
        }
    }

    public function collectfacebooklikeremove(Request $request)
    {
        $memberid = getPrivetMemberId();
        $member = Playzone_mission::where('member_id', $memberid)->first();
        if (!$member) {
            $member = new Playzone_mission();
            $member->member_id = $memberid;
            $member->final_score = 0;
        } else {
            $member->final_score = $member->final_score - 5;
        }
        $member->facebook_page_like = '';
        $result = $member->save();
        if ($result) {
            $request->session()->flash('alert-success', 'Facebook page is unliked. 5 points deduct from your score');
            return redirect('playzone/play');

        } else {
            $request->session()->flash('alert-danger', 'Facebook page not unliked. please try again');
            return redirect('playzone/play');
        }
    }

//    google points
    public function collectgooglepoints(Request $request, $share = null)
    {
//        $share= $request->all();
//        dd($share);
        $memberid = getPrivetMemberId();
        $member = Playzone_mission::where('member_id', $memberid)->first();
        if (!$member) {
            $member = new Playzone_mission();
            $member->member_id = $memberid;
            $member->final_score = 5;
            $member->current_score = 5;
        } else {
            if ($share == 'follow') {
                if ($member->google_plus_follow) {
                    $request->session()->flash('alert-danger', 'You have already followed page');
                    return redirect('playzone/play');
                } else {
                    $member->final_score = $member->final_score + 5;
                    $member->current_score = $member->current_score + 5;
                }
            } elseif ($share == 'share') {
                if ($member->google_plus_share) {
                    $request->session()->flash('alert-danger', 'You have already shared page');
                    return redirect('playzone/play');
                } else {
                    $member->final_score = $member->final_score + 5;
                    $member->current_score = $member->current_score + 5;
                }
            } elseif ($share == 'youtube') {
                if ($member->youtube_subscribe) {
                    $request->session()->flash('alert-danger', 'You have already Subscribed');
                    return redirect('playzone/play');
                } else {
                    $member->final_score = $member->final_score + 5;
                    $member->current_score = $member->current_score + 5;
                }
            }

        }
        if ($share == 'follow') {
            $member->google_plus_follow = 1;
            $result = $member->save();
            if ($result) {
                $request->session()->flash('alert-success', 'Google Plus page followed. 5 points added to your score');
                return redirect('playzone/play');

            } else {
                $request->session()->flash('alert-danger', 'Google Plus page not followed. please try again');
                return redirect('playzone/play');
            }
        } elseif ($share == 'share') {
            $member->google_plus_share = 1;
            $result = $member->save();
            if ($result) {
                $request->session()->flash('alert-success', 'Google Plus page Shared. 5 points added to your score');
                return redirect('playzone/play');

            } else {
                $request->session()->flash('alert-danger', 'Google Plus page not Shared. please try again');
                return redirect('playzone/play');
            }
        } elseif ($share == 'youtube') {
            $member->youtube_subscribe = 1;
            $result = $member->save();
            if ($result) {
                $request->session()->flash('alert-success', 'Youtube channel Subscribed. 5 points added to your score');
                return redirect('playzone/play');

            } else {
                $request->session()->flash('alert-danger', 'Youtube channel not Subscribed. please try again');
                return redirect('playzone/play');
            }
        }

        $member->facebook_page_like = 1;
        $result = $member->save();
        if ($result) {
            $request->session()->flash('alert-success', 'Facebook page liked. 5 points added to your score');
            return redirect('playzone/play');

        } else {
            $request->session()->flash('alert-danger', 'Facebook page not liked. please try again');
            return redirect('playzone/play');
        }
    }

// other social missions save
    public function collectothersocialpoints(Request $request, $share = null)
    {
//        $share= $request->all();
//        dd($share);
        $memberid = getPrivetMemberId();
        $member = Playzone_mission::where('member_id', $memberid)->first();
        if (!$member) {
            $member = new Playzone_mission();
            $member->member_id = $memberid;
            $member->final_score = 5;
            $member->current_score = 5;
        } else {
            if ($share == 'twitterfollow') {
                if ($member->twitter_follow) {
                    $request->session()->flash('alert-danger', 'You have already followed page');
                    return redirect('playzone/play');
                } else {
                    $member->final_score = $member->final_score + 5;
                    $member->current_score = $member->current_score + 5;
                }
            } elseif ($share == 'instagramfollow') {
                if ($member->instagram_follow) {
                    $request->session()->flash('alert-danger', 'You have already followed on Instagram');
                    return redirect('playzone/play');
                } else {
                    $member->final_score = $member->final_score + 5;
                    $member->current_score = $member->current_score + 5;
                }
            } elseif ($share == 'linkedin') {
                if ($member->linkedin_follow) {
                    $request->session()->flash('alert-danger', 'You have already Followed on Linkedin');
                    return redirect('playzone/play');
                } else {
                    $member->final_score = $member->final_score + 5;
                    $member->current_score = $member->current_score + 5;
                }
            }

        }
        if ($share == 'twitterfollow') {
            $member->twitter_follow = 1;
            $result = $member->save();
            if ($result) {
                $request->session()->flash('alert-success', 'SaleMe.lk Twitter followed. 5 points added to your score');
                return redirect('playzone/play');

            } else {
                $request->session()->flash('alert-danger', 'SaleMe.lk Twitter not followed. please try again');
                return redirect('playzone/play');
            }
        } elseif ($share == 'instagramfollow') {
            $member->instagram_follow = 1;
            $result = $member->save();
            if ($result) {
                $request->session()->flash('alert-success', 'Followed on Instagram. 5 points added to your score');
                return redirect('playzone/play');

            } else {
                $request->session()->flash('alert-danger', 'Followed on Instagram unsuccessful. please try again');
                return redirect('playzone/play');
            }
        } elseif ($share == 'linkedin') {
            $member->linkedin_follow = 1;
            $result = $member->save();
            if ($result) {
                $request->session()->flash('alert-success', 'Followed on Linkedin. 5 points added to your score');
                return redirect('playzone/play');

            } else {
                $request->session()->flash('alert-danger', 'Followed on Linkedin unsuccessful. please try again');
                return redirect('playzone/play');
            }
        }

        $member->facebook_page_like = 1;
        $result = $member->save();
        if ($result) {
            $request->session()->flash('alert-success', 'Facebook page liked. 5 points added to your score');
            return redirect('playzone/play');

        } else {
            $request->session()->flash('alert-danger', 'Facebook page not liked. please try again');
            return redirect('playzone/play');
        }
    }

    public function redeemPrize(Request $request, $member_id, $item_id)
    {
//        add playzone member details
        $request->merge(['member_id' => $member_id]);
        $memberdetails = new Playzone_member($request->all());
        $res = $memberdetails->save();
//        dd($res);

        if ($res) {
            $buyData = array(
                'playzone_member_id' => $memberdetails->id,
                'playzone_store_id' => $item_id,
                'is_prize' => 1,
            );
//            dd($buyData);
            $buy = new Playzone_buy_request($buyData);
            $buy->member_id = $request->member_id;
            $buy_res = $buy->save();

            $current_score = Playzone_mission::where('member_id', $member_id)->first();
            $itemMinScore = Playzone_store::select('min_point')
                ->where('id', $item_id)
                ->where('status', 'active')->first();

            $score = $current_score->current_score - $itemMinScore->min_point;
            $current_score->current_score = $score;
            $scoreUpdate = $current_score->update();

        }

        if ($buy_res AND $scoreUpdate) {
            try {
                $itemDetails = Playzone_store::where('id', $item_id)
                    ->where('status', 'active')->first();

                $data = array(
                    'email' => $request->email,
                    'name' => $request->name,
                    'address' => $request->address . ', ' . $request->post_code,
                    'mobile' => $request->mobile,
                    'itemTitle' => $itemDetails->title,
                    'itemDescription' => $itemDetails->description,
                    'itemImage' => $itemDetails->image,
                    'itemMinPints' => $itemDetails->min_point,
                    'prize' => true
                );
                Mail::to($request->email)->send(new RedeemItem($data));
                return response()->json([
                    'success' => $buy_res,
                    'css' => 'alert-success',
                    'icon' => 'now-ui-icons travel_info',
                    'message' => 'Order Success. You will receive item soon.']);

            } catch (Exception $ex) {

                return response()->json([
                    'success' => $buy_res,
                    'css' => 'alert-success',
                    'icon' => 'now-ui-icons travel_info',
                    'message' => 'Order Success. But email not sent. You will receive item soon.']);
            }


        } else {
            return response()->json([
                'success' => $buy_res,
                'css' => 'alert-danger',
                'icon' => 'now-ui-icons travel_info',
                'message' => 'Order not Success. Please try again.']);
        }
    }

//    save profile data
    public function saveProfileData(Request $request, $id)
    {
//        save member name
        $member = Member::where('id', $id)->first();
        $member->first_name = $request->name;
        $membername = $member->update();
        unset($request['name']);

        if ($membername) {
            $playzone = Playzone_member::where('member_id', $id)->first();
            $playzone->address = $request->address;
            $playzone->post_code = $request->post_code;
            $playzone->mobile = $request->mobile;
            $playzone->gender = $request->gender;
            $playzone->dob = $request->dob;
            $memberdetails = $playzone->update();
        }
        if ($memberdetails) {
            return response()->json([
                'success' => $memberdetails,
                'css' => 'alert-success',
                'icon' => 'now-ui-icons travel_info',
                'message' => 'Member details updated successfully.']);
        } else {
            return response()->json([
                'success' => $memberdetails,
                'css' => 'alert-danger',
                'icon' => 'now-ui-icons travel_info',
                'message' => 'Member details not updated successfully.']);
        }
    }

    //games
//    Kokis challenge
    public function kokisChallenge()
    {
        $last_play = date('Y-m-d');
        $check = Playzone_mission::where('member_id', getPrivetMemberId())->first();
        if (!is_null($check)) {
            $played_times = $check->game_01_palyed_times + 1;
            if ($last_play == $check->game_01_last_played_at AND $played_times > 3) {
                return redirect('/playzone/games/error/kokis-challage/102');
            }
        }
        return view('gamezone.games.kokis_challenge.index');
    }

    public function share()
    {
        $value = Session::get('score');
        if (!$value) {
            return redirect('/playzone/games/kokis-challenge/play');
        }
        return view('gamezone.games.kokis_challenge.user-details')
            ->with('score', $value);
    }

    public function score(Request $request)
    {
        $request->session()->forget('score');
        Session::set('score', $request->score);
    }

    public function saveScore(Request $request, $member)
    {
        $memberid = getPrivetMemberId();
        $check = Playzone_mission::where('member_id', $memberid)->first();
        if (!$check) {
            $check = new Playzone_mission();
            $check->member_id = $memberid;
            $check->final_score = $request->marks;
            $check->current_score = $request->marks;
        }

        $last_play = date('Y-m-d');
        $played_times =1;
        if ($last_play == $check->game_01_last_played_at){
            $played_times = $check->game_01_palyed_times + 1;
        }

//        dd($check);
        if ($last_play == $check->game_01_last_played_at AND $played_times > 3) {
            $request->session()->forget('score');
            return response()->json([
                'success' => false,
                'message' => 'Exceed times to play today. Please try again tomorrow',
                'css' => 'alert-danger',
                'icon' => 'now-ui-icons travel_info',
            ]);
        } else {
            $check->game_01_last_played_at = $last_play;
            $check->game_01_palyed_times = $played_times;
            $check->current_score = $check->current_score + $request->marks;
            $check->final_score = $check->final_score + $request->marks;

            $res = $check->save();
            if ($res) {
                $request->session()->forget('score');
                return response()->json([
                    'success' => true,
                    'message' => 'Your marks of ' . $request->marks . ' Added to your score.',
                    'css' => 'alert-success',
                    'icon' => 'now-ui-icons ui-2_like',
                ]);
            } else {
                $request->session()->forget('score');
                return response()->json([
                    'success' => false,
                    'message' => 'Your marks not added to score. Please try again',
                    'css' => 'alert-danger',
                    'icon' => 'now-ui-icons travel_info',
                ]);
            }
        }
//        dd($request->all());
    }

    public function errorPage($game, $errorCode)
    {
        $memberid = getPrivetMemberId();
        $provider = !empty(Session::get('provider')) ? Session::get('provider') : '';
        $memberdetails = Member::MemberDetailsByIdsocial($memberid, $provider);
        $missiondata = Playzone_mission::where('member_id', $memberid)->first();
        if (!is_null($missiondata)) {
            $level = self::checkLevel($missiondata->final_score);
        }
        $level = array('currentlevel' => 'Elite', 'nextLevel' => 'Knight', 'current' => 1, 'next' => 2);
        $errors = [
            '101' => 'Your marks not added to score. Please try again',
            '102' => 'Exceed times to play today. Please try again tomorrow'
        ];
//        dd(unserialize($question->answers));
        return view('gamezone.games.error')
            ->with('game', $game)
            ->with('message', 'Exceed times to play today. Please try again tomorrow')
            ->with('memberdetails', $memberdetails)
            ->with('level', $level)
            ->with('error', $errors[$errorCode])
            ->with('missiondata', $missiondata);
    }

    //2048
    public function game_2048()
    {
        $memberid = getPrivetMemberId();
        $provider = !empty(Session::get('provider')) ? Session::get('provider') : '';
        $memberdetails = Member::MemberDetailsByIdsocial($memberid, $provider);
        $missiondata = Playzone_mission::where('member_id', $memberid)->first();
        if (!is_null($missiondata)) {
            $level = self::checkLevel($missiondata->final_score);
        }
        $level = array('currentlevel' => 'Elite', 'nextLevel' => 'Knight', 'current' => 1, 'next' => 2);

        return view('gamezone.games.2048.index')
            ->with('memberdetails', $memberdetails)
            ->with('level', $level)
            ->with('missiondata', $missiondata);
    }

    public function game_share_2048($id)
    {
        $value = Session::get('score');
        if (!$value) {
            return redirect('game/play');
        }
        return view('gamezone.games.2048.user-details')
            ->with('score', $value);
    }

    public function saveScore2048(Request $request, $member)
    {
        $memberid = getPrivetMemberId();
        $check = Playzone_mission::where('member_id', $memberid)->first();
        if (!$check) {
            $check = new Playzone_mission();
            $check->member_id = $memberid;
            $check->final_score = $request->marks;
            $check->current_score = $request->marks;
        }
        $last_play = date('Y-m-d');

        $played_times =1;
        if ($last_play == $check->game_02_palyed_times){
            $played_times = $check->game_02_palyed_times + 1;
        }

//        $played_times = $check->game_02_palyed_times + 1;
        if ($last_play == $check->game_02_last_played_at AND $played_times > 3) {
            $request->session()->forget('score');
            return response()->json([
                'success' => false,
                'message' => 'Exceed times to play today. Please try again tomorrow',
                'css' => 'alert-danger',
                'icon' => 'now-ui-icons travel_info',
            ]);
        } else {
            $check->game_02_last_played_at = $last_play;
            $check->game_02_palyed_times = $played_times;
            $check->current_score = $check->current_score + $request->marks;
            $check->final_score = $check->final_score + $request->marks;

            $res = $check->save();
            if ($res) {
                $request->session()->forget('score');
                return response()->json([
                    'success' => true,
                    'message' => 'Your marks of ' . $request->marks . ' Added to your score.',
                    'css' => 'alert-success',
                    'icon' => 'now-ui-icons ui-2_like',
                ]);
            } else {
                $request->session()->forget('score');
                return response()->json([
                    'success' => false,
                    'message' => 'Your marks not added to score. Please try again',
                    'css' => 'alert-danger',
                    'icon' => 'now-ui-icons travel_info',
                ]);
            }
        }
//        dd($request->all());
    }

}
