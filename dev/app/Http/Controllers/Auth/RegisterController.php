<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Kodeine\Acl\Models\Eloquent\Role;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showUserlist()
    {
        $users = User::all();
        return view('auth.userlist', compact('users'));
    }

    public function showEditForm(User $user)
    {
//        dd($authUser);
//        dd($user->isRole('admin'));
//        dd(Auth::user()->getRoles());
        if(!in_array('superadmin',Auth::user()->getRoles())){
            if(Auth()->user()->id != $user->id){
                abort(404);
            }
        }

        $roles = Role::all();
        $user->getRoles();
        return view('auth.user-edit', compact('user', 'roles'));
    }

    //my account
    public function myaccount(User $user)
    {

        return view('auth.user-edit', compact('user'));
    }

    public function showAssignUserRoles()
    {
        $users = User::all();
        $roles = Role::all();
        return view('admin.admin', compact('users', 'roles'));
    }

    public function showRegistrationForm()
    {
        $roles = Role::all();
        return view('auth.user-register',compact('roles'));
    }

    public function assignUserRoles(Request $request)
    {
        $user = User::where('email', $request['email'])->first();
        if (!empty($user)) {
            $user->revokeAllRoles();
            if(!empty($request['role'])){
                $roles = implode(',', $request['role']);
                $user->assignRole($roles);
            }
        }
        return redirect()->back();
    }

    public function showUserLog()
    {

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));
        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }
    protected function create(array $data)
    {
        $rolesulg = '';
        if(!empty($data['role'])){
            $rolesulg = implode(',', $data['role']);
        }
        $user = new User();
        $user->email = $data['email'];
        $user->name = $data['name'];
        $user->password = bcrypt($data['password']);
        $user->save();
        $user->assignRole($rolesulg);
        return $user;
    }

    public function update(Request $request, User $user)
    {
        $user->password = bcrypt($request->password);
        $user->save($request->all());
        return back();
    }
}
