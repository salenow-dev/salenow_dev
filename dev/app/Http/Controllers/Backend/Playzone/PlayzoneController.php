<?php

namespace App\Http\Controllers\Backend\Playzone;

use App\Mail\OrderShipped;
use App\Models\Member;
use App\Models\Playzone_buy_request;
use App\Models\Playzone_dailyquestion;
use App\Models\Playzone_extraquestion;
use App\Models\Playzone_member;
use App\Models\Playzone_mission;
use App\Models\Playzone_store;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class PlayzoneController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

   public function playersList()
    {
        $members = Playzone_mission::getPlayzoneMember();
//        dd($members);
        return view('backend.playzone.players-list')
            ->with('players',$members);
    }

    public function ListDailyQuestions()
    {
        $questions = Playzone_dailyquestion::get();
        return view('backend.playzone.daily-questions-list')->with('questions', $questions);
    }

    public function AddDailyQuestions()
    {
        $questions = Playzone_dailyquestion::get();
        return view('backend.playzone.daily-questions-add')->with('questions', $questions);
    }

    public function SaveDailyQuestions(Request $request)
    {
        $file = $request->image_file;
        $ext = $file->guessExtension();
        $filename = $request->image_file->getClientOriginalName();
        $filename = pathinfo($filename, PATHINFO_FILENAME);
        $filename = uniqid();
        $file_name = $filename . '.' . $ext;
        Input::file('image_file')->move('./gamezone/img/dailyquestions/', $file_name);

//        dd($data);
        $save = new Playzone_dailyquestion;
        $save->title = $request->title;
        $save->answer1 = $request->answer1;
        $save->answer2 = $request->answer2;
        $save->answer3 = $request->answer3;
        $save->image = $file_name;
        $result = $save->save();
        if ($result) {
            flash('Daily Question added successfully.', 'success');
            return redirect()->back();
        } else {
            flash('Error! Daily Question not added', 'error');
            return redirect()->back();
        }
//        $questions = Playzone_dailyquestion::get();
//        return view('backend.playzone.daily-questions-add')->with('questions',$questions);
    }

    public function EditDailyQuestions($id)
    {
        $daily = Playzone_dailyquestion::where('id', $id)->first();
//        dd($daily);
        return view('backend.playzone.daily-questions-edit')
            ->with('id', $id)
            ->with('daily', $daily);
    }

    public function UpdateEditDailyQuestions(Request $request, $id)
    {
        $details = Playzone_dailyquestion::where('id', $id)->first();
        if (!is_null($request->image_file)) {
            $existingFile = './gamezone/img/dailyquestions/' . $details->image;
            if (file_exists($existingFile)) {
                unlink($existingFile);
            }
            $file = $request->image_file;
            $ext = $file->guessExtension();
            $filename = $request->image_file->getClientOriginalName();
            $filename = pathinfo($filename, PATHINFO_FILENAME);
            $filename = uniqid();
            $file_name = $filename . '.' . $ext;
            Input::file('image_file')->move('./gamezone/img/dailyquestions/', $file_name);
        } else {
            $file_name = $details->image;
        }

        $details->title = $request->title;
        $details->answer1 = $request->answer1;
        $details->answer2 = $request->answer2;
        $details->answer3 = $request->answer3;
        $details->image = $file_name;
        $result = $details->save();
        if ($result) {
            flash('Daily Question updated successfully.', 'success');
            return redirect()->back();
        } else {
            flash('Error! Daily Question not updated', 'error');
            return redirect()->back();
        }

    }

    public function ChangeStatusDailyQuestions(Request $request)
    {
        $que = Playzone_dailyquestion::where('id', $request->id)->first();
        $que->status = $request->status;
        $result = $que->save();
        if ($result) {
            return response()->json(['message' => 'Status updated successfully', 'status' => 'success']);
        } else {
            return response()->json(['message' => 'Status not updated successfully', 'status' => 'error']);
        }
    }

    public function DeleteDailyQuestions(Request $request)
    {
        $que = Playzone_dailyquestion::where('id', $request->id)->first();
        $existingFile = './gamezone/img/dailyquestions/' . $que->image;
        if (file_exists($existingFile)) {
            unlink($existingFile);
        }
        $result = $que->delete();
        if ($result) {
            return response()->json(['message' => 'Question deleted successfully', 'status' => 'success']);
        } else {
            return response()->json(['message' => 'Question not deleted successfully', 'status' => 'error']);
        }
    }

    //extra questions
    public function ListExtraQuestions()
    {
        $questions = Playzone_extraquestion::get();
        return view('backend.playzone.extra-questions-list')->with('questions', $questions);
    }

    public function AddExtraQuestions()
    {
        $questions = Playzone_extraquestion::get();
        return view('backend.playzone.extra-questions-add')->with('questions', $questions);
    }

    public function SaveExtraQuestions(Request $request)
    {
        $file = $request->image_file;
        $ext = $file->guessExtension();
        $filename = $request->image_file->getClientOriginalName();
        $filename = pathinfo($filename, PATHINFO_FILENAME);
        $filename = uniqid();
        $file_name = $filename . '.' . $ext;
        Input::file('image_file')->move('./gamezone/img/extraquestions/', $file_name);

//        dd($data);
        $save = new Playzone_extraquestion;
        $save->title = $request->title;
        $save->answer1 = $request->answer1;
        $save->answer2 = $request->answer2;
        $save->answer3 = $request->answer3;
        $save->image = $file_name;
        $result = $save->save();
        if ($result) {
            flash('Daily Question added successfully.', 'success');
            return redirect()->back();
        } else {
            flash('Error! Daily Question not added', 'error');
            return redirect()->back();
        }
//        $questions = Playzone_dailyquestion::get();
//        return view('backend.playzone.daily-questions-add')->with('questions',$questions);
    }

    public function EditExtraQuestions($id)
    {
        $daily = Playzone_extraquestion::where('id', $id)->first();
//        dd($daily);
        return view('backend.playzone.extra-questions-edit')
            ->with('id', $id)
            ->with('daily', $daily);
    }

    public function UpdateEditExtraQuestions(Request $request, $id)
    {
        $details = Playzone_extraquestion::where('id', $id)->first();
        if (!is_null($request->image_file)) {
            $existingFile = './gamezone/img/extraquestions/' . $details->image;
            if (file_exists($existingFile)) {
                unlink($existingFile);
            }
            $file = $request->image_file;
            $ext = $file->guessExtension();
            $filename = $request->image_file->getClientOriginalName();
            $filename = pathinfo($filename, PATHINFO_FILENAME);
            $filename = uniqid();
            $file_name = $filename . '.' . $ext;
            Input::file('image_file')->move('./gamezone/img/extraquestions/', $file_name);
        } else {
            $file_name = $details->image;
        }

        $details->title = $request->title;
        $details->answer1 = $request->answer1;
        $details->answer2 = $request->answer2;
        $details->answer3 = $request->answer3;
        $details->image = $file_name;
        $result = $details->save();
        if ($result) {
            flash('Daily Question updated successfully.', 'success');
            return redirect()->back();
        } else {
            flash('Error! Daily Question not updated', 'error');
            return redirect()->back();
        }

    }

    public function ChangeStatusExtraQuestions(Request $request)
    {
        $que = Playzone_extraquestion::where('id', $request->id)->first();
        $que->status = $request->status;
        $result = $que->save();
        if ($result) {
            return response()->json(['message' => 'Status updated successfully', 'status' => 'success']);
        } else {
            return response()->json(['message' => 'Status not updated successfully', 'status' => 'error']);
        }
    }

    public function DeleteExtraQuestions(Request $request)
    {
        $que = Playzone_extraquestion::where('id', $request->id)->first();
        $existingFile = './gamezone/img/extraquestions/' . $que->image;
        if (file_exists($existingFile)) {
            unlink($existingFile);
        }

        $result = $que->delete();
        if ($result) {
            return response()->json(['message' => 'Question deleted successfully', 'status' => 'success']);
        } else {
            return response()->json(['message' => 'Question not deleted successfully', 'status' => 'error']);
        }
    }

//    Store
    public function ListPrizes()
    {
        $questions = playzone_store::get();
        return view('backend.playzone.store')->with('questions', $questions);
    }

    public function AddPrizes()
    {
        $questions = Playzone_dailyquestion::get();
        return view('backend.playzone.store-add')->with('questions', $questions);
    }

    public function SavePrizes(Request $request)
    {
//        dd($request->all());
        $file = $request->image_file;
        $ext = $file->guessExtension();
        $filename = $request->image_file->getClientOriginalName();
        $filename = pathinfo($filename, PATHINFO_FILENAME);
        $filename = uniqid();
        $file_name = $filename . '.' . $ext;
        Input::file('image_file')->move('./gamezone/img/store/', $file_name);

        if (is_null($request->is_prize)) {
            $isPrize = 0;
        } else {
            $isPrize = $request->is_prize;
        }

        $save = new Playzone_store();
        $save->title = $request->title;
        $save->description = $request->description;
        $save->price = $request->price;
        $save->qty = $request->qty;
        $save->is_prize = $isPrize;
        $save->min_point = $request->min_point;
        $save->image = $file_name;
        $result = $save->save();
        if ($result) {
            flash('Daily Question added successfully.', 'success');
            return redirect()->back();
        } else {
            flash('Error! Daily Question not added', 'error');
            return redirect()->back();
        }

        $questions = Playzone_dailyquestion::get();
        return view('backend.playzone.store-add')->with('questions', $questions);
    }

    public function EditStore($id)
    {
        $store = Playzone_store::where('id', $id)->first();
//        dd($daily);
        return view('backend.playzone.store-edit')
            ->with('id', $id)
            ->with('store', $store);
    }

    public function UpdateEditStore(Request $request, $id)
    {

        $details = Playzone_store::where('id', $id)->first();
        if (!is_null($request->image_file)) {
            $existingFile = './gamezone/img/store/' . $details->image;
            if (file_exists($existingFile)) {
                unlink($existingFile);
            }
            $file = $request->image_file;
            $ext = $file->guessExtension();
            $filename = $request->image_file->getClientOriginalName();
            $filename = pathinfo($filename, PATHINFO_FILENAME);
            $filename = uniqid();
            $file_name = $filename . '.' . $ext;
            Input::file('image_file')->move('./gamezone/img/store/', $file_name);
        } else {
            $file_name = $details->image;
        }

        if (is_null($request->is_prize)) {
            $isPrize = 0;
        } else {
            $isPrize = $request->is_prize;
        }

        $details->title = $request->title;
        $details->description = $request->description;
        $details->price = $request->price;
        $details->qty = $request->qty;
        $details->is_prize = $isPrize;
        $details->min_point = $request->min_point;
        $details->image = $file_name;
        $result = $details->update();

        if ($result) {
            flash('Daily Question updated successfully.', 'success');
            return redirect()->back();
        } else {
            flash('Error! Daily Question not updated', 'error');
            return redirect()->back();
        }

    }

    public function ChangeStatusStore(Request $request)
    {
        $que = Playzone_store::where('id', $request->id)->first();
        $que->status = $request->status;
        $result = $que->save();
        if ($result) {
            return response()->json(['message' => 'Status updated successfully', 'status' => 'success']);
        } else {
            return response()->json(['message' => 'Status not updated successfully', 'status' => 'error']);
        }
    }

    public function DeleteStore(Request $request)
    {
        $que = Playzone_store::where('id', $request->id)->first();
        $existingFile = './gamezone/img/store/' . $que->image;
        if (file_exists($existingFile)) {
            unlink($existingFile);
        }
        $result = $que->delete();
        if ($result) {
            return response()->json(['message' => 'Question deleted successfully', 'status' => 'success']);
        } else {
            return response()->json(['message' => 'Question not deleted successfully', 'status' => 'error']);
        }
    }

    public function MakePriceStore(Request $request)
    {
        $que = Playzone_store::where('id', $request->id)->first();
        $que->is_prize = $request->status;
        $result = $que->save();
        if ($result) {
            return response()->json(['message' => 'Item made as prize successfully', 'status' => 'success']);
        } else {
            return response()->json(['message' => 'Error! Item make prize not success', 'status' => 'error']);
        }
    }

//    Prizes
    public function PrizesList($status)
    {
        $prizes = Playzone_buy_request::allPrizesList($status);
        $qued_count = Playzone_buy_request::where('status', 'ship_qued')->count();
        $pending_count = Playzone_buy_request::where('status', 'pending')->count();
        $ignore_count = Playzone_buy_request::where('status', 'ignored')->count();
        $shiped_count = Playzone_buy_request::where('status', 'shiped')->count();
        return view('backend.playzone.list-prizes')
            ->with('prizes', $prizes)
            ->with('qued_count', $qued_count)
            ->with('pending_count', $pending_count)
            ->with('ignore_count', $ignore_count)
            ->with('shiped_count', $shiped_count)
            ->with('status', $status);
    }

    public function PrizesStatusChange(Request $request)
    {
        $prize_status = Playzone_buy_request::where('id', $request->id)->first();
        $prize_status->status = $request->status;
        $prize_status->shiped_date = date('Y-m-d H:i:s');
        $res = $prize_status->save();

        if ($request->status == 'shiped'){

            //change qty on order
            $qty = Playzone_store::where('id',$prize_status->playzone_store_id)->first();
            $qty->qty=$qty->qty -1;
            $qty->update();

            try {
                $itemDetails = Playzone_store::where('id', $prize_status->playzone_store_id)->first();
                $member=Playzone_member::where('member_id',$prize_status->member_id)->first();
                $member_name=Member::select('first_name')->where('id',$prize_status->member_id)->first();

                $data = array(
                    'email'=> $member->email,
                    'name'=> $member_name->first_name,
                    'address'=> $member->address .', ' . $member->post_code,
                    'mobile'=>$member->mobile,
                    'itemTitle'=>$itemDetails->title,
                    'itemDescription'=>$itemDetails->description,
                    'itemImage'=>$itemDetails->image,
                    'itemMinPints'=>$itemDetails->min_point,
                    'prize'=>true
                );
                Mail::to($member->email)->send(new OrderShipped($data));

            } catch (Exception $ex) {}

        }

        if ($res) {
            $message = array(
                'ignored' => 'Item ignored',
                'shiped' => 'Item shipped',
                'ship_qued' => 'Item added shipping que',
                'delivered' => 'Item delivered',
                'pending' => 'Item status pending',
            );

            return response()->json(['message' => $message[$request->status], 'status' => 'success']);
        } else {
            $error = array(
                'ignored' => 'Item ignored',
                'shiped' => 'Item shipped',
                'ship_qued' => 'Item added shipping que',
                'delivered' => 'Item delivered',
                'pending' => 'Item status pending',
            );
            return response()->json(['message' => $error[$request->status], 'status' => 'error']);
        }
//                dd($request->all());
    }

}
