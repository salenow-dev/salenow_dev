<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Alladsmaster;
use App\Models\Member;
use App\Models\Memberpremium;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        $totalImpressions = Alladsmaster::select(DB::raw("SUM(views) as views"), DB::raw("SUM(clicks) as clicks"))
            ->get();
        $one_week_ago = Carbon::now()->subWeeks(2);
        $yesterday = Carbon::now()->subDays(1);

        $expirThisWeek = Memberpremium::select('company_name','mobile','expire_on')
        ->whereBetween('expire_on',[Carbon::today(),Carbon::today()->addDays(7)])
            ->where('verified',1)
            ->where('list_show',1)
            ->get();
        $expiredMembers = Memberpremium::select('company_name','mobile','expire_on')
            ->where('verified',1)
            ->where('list_show',1)
            ->Where('expire_on','<=',Carbon::today())
            ->get();
//
//        dd($expiredMembers);

        $memberCount = Member::count();
        $todatAds = Alladsmaster::whereDate('created_at', date('Y-m-d'))->count();

        $pastWeekAds = Alladsmaster::selectRaw('date(created_at) as date, COUNT(*) as count')->whereBetween(DB::raw('date(created_at)'), [$one_week_ago, $yesterday])
            ->groupBy('date')->orderBy('date', 'DESC')->get();

        $todayMemberCount = Member::whereDate('created_at', date('Y-m-d'))->count();
        return view('dashboard', compact('memberCount', 'todatAds', 'todayMemberCount', 'pastWeekAds','totalImpressions','expirThisWeek','expiredMembers'));

    }
}
