<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommonController extends Controller
{
    //common function for change record status
    public function statuschange()
    {
        if (!empty($_POST['id'])) {
            $id = $_POST['id'];
            $model = $_POST['model'];

            $dynamicObj = 'App\Model\\' . $model;
            $obj = new $dynamicObj;

            $obj = $obj::find($id);
            $obj->status = $_POST['status'];
            $obj->updated_by = Auth::user()->id;
            if ($obj->save()) {
                return response()->json(['response' => array(
                    'message' => 'Status chaged!'
                )]);
            } else {
                return response()->json(['response' => array(
                    'error' => 'Something went wrong!'
                )]);
            }
        }
    }

    //common function for delete record
    public function deleterecord()
    {
        if (!empty($_POST['id'])) {
            $id = $_POST['id'];
            $model = $_POST['model'];
            $dynamicObj = 'App\Models\\' . ucfirst($model) ;
            $obj = new $dynamicObj;

            $obj = $obj::find($id);
            if ($obj->delete()) {
                return response()->json(['response' => array(
                    'message' => 'success'
                )]);
            } else {
                return response()->json(['response' => array(
                    'error' => 'error'
                )]);
            }
        }
    }
}
