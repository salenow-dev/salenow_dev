<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Kodeine\Acl\Models\Eloquent\Role;

class DefaultsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function adminpanel()
    {
        $users = User::all();
        $roles = Role::all();
        return view('admin.admin', compact('users', 'roles'));
    }

    public function userpanel()
    {
        return view('user');
    }

    public function dashboard()
    {
        return view('dashboard');
    }


}
