<?php

namespace App\Http\Controllers\Backend;

use App\Classes\UUID;
use App\Dialogsms\Sms;
use App\Models\Allhoteltravelad;
use App\Models\Alljobad;
use App\Models\Boostpack;
use App\Models\Game_detail;
use App\Models\Membercontact;
use App\Models\Memberpremium;
use App\Models\Vehiclecondition;
use App\Models\Alladsfilter;
use App\Models\Boostviewcount;
use App\Models\Membervouchercode;
use App\Models\Adimage;
use App\Models\Adspam;
use App\Models\Alladsmaster;
use App\Models\Allelectrnicad;
use App\Models\Allpropertyad;
use App\Models\Allvehiclead;
use App\Models\Categoryfiltervalue;
use App\Models\Member;
use App\Models\Membervoucher;
use App\Models\Otp;
use App\Models\Spamreason;
use App\Notifications\AdPublished;
use App\Notifications\DeleteAd;
use App\Notifications\PintotopRequest;
use App\Notifications\PintotopRequestFromBackend;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Vehicleoption;
use App\Models\Propertyoption;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Kodeine\Acl\Models\Eloquent\Permission;
use Kodeine\Acl\Models\Eloquent\Role;


class AdsreviewController extends Controller
{
    public function index()
    {
//        $permission = new Permission();
//        $permUser = $permission->create([
//            'name'        => 'ads-manager',
//            'slug'        => [          // pass an array of permissions.
//                'index'     => true,
//                'publishads' => true,
//                'allspamads' => true,
//                'edit'     => true,
//            ],
//            'description' => 'admin manage ads manage permissions'
//        ]);
//
//        $permissionSupervisor = Permission::create([
//            'name'        => 'moderator.ads-manager',
//            'slug'        => [ // an array of permissions only for supervisor
//                'publishads' => false,
//            ],
//            // we use permission inheriting.
//            'inherit_id' => $permUser->getKey(),
//            'description' => 'moderator manage ads manage permissions'
//        ]);
//        $roleAdmin = Role::first(); // administrator
//        $roleAdmin->assignPermission($permUser);
//
//        $roleSuper = Role::find(2);// supervisor
//        $roleSuper->assignPermission($permissionSupervisor);

//        dd(Auth::user());
//        dd(Auth::user()->getRoles());
//        dd(Auth::user()->hasPermission());
//        dd(Auth::user()->getPermissions());

        $todayOtpCount = Otp::whereDate('created_at', date('Y-m-d'))->count();
        $allPendingVehicleAds = Alladsmaster::getAllPendingVehiclesAds();
        return view('backend.adsmanager.adsreview', compact('allPendingVehicleAds', 'todayOtpCount'));
    }

    public function publishads()
    {
        $allPendingVehicleAds = Alladsmaster::getAllConfirmedAds();
        return view('backend.adsmanager.publish-ads', compact('allPendingVehicleAds'));
    }

    public function draftads()
    {
        $allPendingVehicleAds = Alladsmaster::getAllDraftAds();
        return view('backend.adsmanager.draft-ads', compact('allPendingVehicleAds'));
    }

    public function searchads(Request $request)
    {
        $request->flash();
        $searchParm = $request->ad_info;
        $adStatus = $request->status;
        $allPendingVehicleAds = Alladsmaster::status($adStatus)->search($searchParm)->paginate(10);
        return view('backend.adsmanager.search', compact('allPendingVehicleAds'));
    }

    public function allspamads()
    {
        $allAds = Alladsmaster::getAllSpamdAds('cancel');
        return view('backend.adsmanager.spam-ads', compact('allAds'));
    }

    public function blockedads()
    {
        $allAds = Alladsmaster::getAllSpamdAds('blocked');
        return view('backend.adsmanager.blocked-ads', compact('allAds'));
    }

    public function deletedads()
    {
        $allAds = Alladsmaster::getAllDeletedAds();
//        dd($allAds);
        return view('backend.adsmanager.deleted-ads', compact('allAds'));
    }

    public function edit($id)
    {
        //spam reason
        $spamreasons = Spamreason::select('id', 'reason')->get(); //spam reason data table

        $allinfo = '';
        $masterTable = Alladsmaster::GetAllAdMasterSingle($id); //get all data
//        dd($masterTable->staus);
        if ($masterTable->itemcondition) {
            $filterVal = Categoryfiltervalue::getFilterValue($masterTable->itemcondition);
            if (!empty($filterVal->filtervalue)) {
                $masterTable->itemcondition = (!empty($filterVal->filtervalue)) ? $filterVal->filtervalue : '';
            } else {
                //only vehicle conditions - other category conditions will get from filter values
                $vehicleCondition = Vehiclecondition::where('id', $masterTable->itemcondition)->select('condition_name')->first();
                $masterTable->itemcondition = $vehicleCondition->condition_name;
            }
        }

        if ($masterTable->itemtype) {
            $filterVal = Categoryfiltervalue::getFilterValue($masterTable->itemtype);
            $masterTable->itemtype = (!empty($filterVal->filtervalue)) ? $filterVal->filtervalue : '';
        }

        //contact number
        $contacts = (!empty($masterTable->contact)) ? unserialize($masterTable->contact) : '';
        $masterTable->contact = $contacts;
        $masterTable->created_at = Carbon::parse($masterTable->created_at);
        $masterTable->updated_at = Carbon::parse($masterTable->updated_at);

        //spams
        $spams = unserialize($masterTable->spams);
        $db_spamreasons = '';
        if (!empty($spams)) {
            foreach ($spams as $spam) {
                $reasons = Spamreason::where('id', $spam)->select('reason')->get();
                $db_spamreasons[] = DB::table('spamreasons')->where('id', $spam)->select('reason')->first();
            }
        }
        $member_contacts = Membercontact::where('member_id', $masterTable->mbr_id)->select('contactnumber')->get();
        $images = Adimage::where('alladsmaster_id', $id)->select('imagename')->get();
        $primaryDetails = '';
        $features = '';

        switch ($masterTable->cat_code) {
            case 'veh':
                $vehicleinfo = Alladsmaster::getVehicleAdSingle($id);
                if (!empty($vehicleinfo)) {
                    $primaryDetails = array(
                        'Brand' => $vehicleinfo->brand,
                        'Model' => $vehicleinfo->model,
                        'Body Type' => $vehicleinfo->bodytype,
                        'Fuel' => $vehicleinfo->fuel,
                        'Transmission' => $vehicleinfo->transmission,
                        'Enginesize' => $vehicleinfo->enginesize,
                        'Mileage' => $vehicleinfo->mileage,
                        'Color' => $vehicleinfo->color,
                        'Registry Year' => $vehicleinfo->registry_year,
                    );
                    //start value added features
                    $optionsids = unserialize($vehicleinfo->features);
                    if (!empty($optionsids)) {
                        foreach ($optionsids as $ids) {
                            $dispal = Vehicleoption::OptionsGetById($ids);
                            $features[] = $dispal->display_name;
                        }
                    }
                }
                $allinfo = $masterTable;
                break;
            case 'ele':
                $elecinfo = Alladsmaster::getElectronicAdSingle($id);
                $allinfo = $masterTable;
                break;
            case 'pro':
                $propertyinfo = Alladsmaster::getPropertyAdSingle($id);
                if (!empty($propertyinfo)) {
                    $beds = Categoryfiltervalue::getFilterValue($propertyinfo->beds);
                    $baths = Categoryfiltervalue::getFilterValue($propertyinfo->baths);
                    $primaryDetails = array(
                        'Land Size' => $propertyinfo->landsize,
                        'Property Size' => $propertyinfo->propertysize,
                        'Beds' => (!empty($beds->filtervalue)) ? $beds->filtervalue : '',
                        'Baths' => (!empty($baths->filtervalue)) ? $baths->filtervalue : '',
                        'Address' => $propertyinfo->address
                    );
                    //start value added features
                    $optionsids = unserialize((!empty($propertyinfo->features)) ? $propertyinfo->features : '');
                    if (!empty($optionsids)) {
                        foreach ($optionsids as $ids) {
                            $dispal = Propertyoption::OptionsGetById($ids);
                            $features[] = $dispal->display_name;
                        }
                    }
                }
                $allinfo = $masterTable;
                break;
            default:
                $allinfo = $masterTable;
        }

        $allAdsFilters = Alladsfilter::where('alladsmaster_id', $id)->select('categoryfiltervalue_id', 'filtername')->get();
        foreach ($allAdsFilters as $allAdsFilter) {
            $data = Categoryfiltervalue::find($allAdsFilter->categoryfiltervalue_id);
            $primaryDetails[ucfirst($data->filtername)] = $data->filtervalue;
        }

        if ($primaryDetails != '') {
            $primaryDetails = array_filter($primaryDetails, function ($value) {
                return $value != '';
            });
        }

        $duplicateAds = Alladsmaster::where('id', '!=', $masterTable->adid)->where('slug', 'like', '%' . $masterTable->slug . '%')->get();
//        $similarAds = Alladsmaster::where('id', '!=', $masterTable->adid)->search($masterTable->adtitle)->get();
        $similarAds = '';
        return view('backend.adsmanager.adreviewsingle',
            compact('allinfo', 'images', 'masterTable', 'primaryDetails', 'features', 'spamreasons', 'db_spamreasons', 'member_contacts', 'similarAds', 'duplicateAds'));
    }

    private static function vehicle()
    {
        $vehicletypelist = DB::table('vehicle_types')->get()->toArray();
        $vehicleconditionlist = DB::table('vehicle_condition')->get()->toArray();
        $vehiclefuellist = DB::table('vehicle_fuels')->get()->toArray();
        $vehicletransmissionslist = DB::table('vehicle_transmissions')->get()->toArray();
        $vehicleoptions = DB::table('vehicle_options')->select('display_name', 'id')->get()->toArray();
        $arr = array(
            'vehicletypelist' => $vehicletypelist,
            'vehicleconditionlist' => $vehicleconditionlist,
            'vehicletransmissionslist' => $vehicletransmissionslist,
            'vehiclefuellist' => $vehiclefuellist,
            'vehicleoptions' => $vehicleoptions,
        );
        return $arr;
    }

    public function update(Request $request, $id)
    {
        /***************************************************************************************************************************
         * backend review ad - publish, spam, delete
         * Publish Ad
         ***************************************************************************************************************************/
        if ($request->status == 'confirm') {
            try {
                $ads = Alladsmaster::findOrFail($id);
                $sendSMS = $ads->published_at;
                $member = Member::where('id', $ads->member_id)->first();
                $update = Alladsmaster::updateStatus($ads, $request->status);
                if ($update) {
                    // START send EMAIL ----------------------------------------------
                    if ($ads->status != 'confirm') {
                        $adPublished = Alladsmaster::with([
                            'member' => function ($query) {
                                $query->select('id', 'first_name', 'status as memberStatus');
                            }])
                            ->select('id', 'member_id', 'adtitle', 'slug', 'status')
                            ->find($id);
                        if ($adPublished->member->memberStatus == 'active') {
                            $member->notify(new AdPublished($adPublished));
                        }
                    }
                    /* END send EMAIL ---------------------------------------------------------------
                     * START sms gateway code
                     * ------------------------------------------------------------------------------- */
                    //delete spam reasons if any
                    $adspam = Adspam::where('alladsmaster_id', $id)->first();
                    (!empty($adspam)) ? $adspam->delete() : '';

                    $verifySMS = '';
                    if (!$sendSMS) {
                        if (!empty($ads->contact)) {
                            $arr = unserialize($ads->contact);
                            if (array_key_exists('0', $arr) && !empty($arr[0])) {
                                $smsMessage = 'Your ad ' . $ads->adtitle . ' is now live on Saleme . lk!Thank you . ';
                                $verifySMS = Sms::Sendsms($arr[0], $smsMessage, '1');
                            }
                        }
                    }
                    /* -------------------------------------------------------------------------------
                     * END sms gateway code
                     * ------------------------------------------------------------------------------- */
                    return response()->json([
                        'sms' => $verifySMS,
                        'message' => 'Success!Ad Successfully Published . ',
                        'url' => ' /ads-manage/ad-review'
                    ]);
                } else {
                    return response()->json([
                        'errors' => 'Cannot Publish the Ad!Member not activated . ',
                    ]);
                }
            } catch (Exception $e) {
                return response()->json([
                    'errors' => $e->getMessage(),
                ]);
            }
        }

        /***************************************************************************************************************************
         * Un-Publish Ad
         ***************************************************************************************************************************/
        if ($request->status == 'pending') {
            try {
                $ads = Alladsmaster::findOrFail($id);
                $update = Alladsmaster::updateStatus($ads, $request->status);
                if ($update) {
                    return response()->json([
                        'message' => 'Success!Ad status chenge to Pending . ',
                        'url' => '/ads-manage/ad-review'
                    ]);
                }
            } catch (Exception $e) {
                return response()->json([
                    'errors' => $e->getMessage(),
                ]);
            }
        }

        /****************************************************************************************************************************
         * Spam Ad
         ****************************************************************************************************************************/
        if ($request->status == 'spam') {
            if (empty($request->spamreason_id)) {
                return response()->json([
                    'errors' => 'Please select a reason for spamming this ad!',
                ]);
            }
            try {
                //delete spam reasons if any
                $adspam = Adspam::where('alladsmaster_id', $request->ad_id)->first();
                (!empty($adspam)) ? $adspam->delete() : '';

                $ads = Alladsmaster::findOrFail($request->ad_id);
                $update = Alladsmaster::updateStatus($ads, 'cancel');

                $spmObj = new Adspam();
                $spmObj->saveNewSpam($request->all());
                $spam_details = $request->all();
                if ($update) {
                    $ads->spamreasons()->attach($spam_details['spamreason_id']);
                    // START send EMAIL ------------------------------------------------------------
                    $member = Member::where('id', $ads->member_id)->first();
                    $adPublished = Alladsmaster::with([
                        'member' => function ($query) {
                            $query->select('id', 'first_name');
                        }])
                        ->select('id', 'member_id', 'adtitle', 'slug', 'status')
                        ->find($id);
                    $member->notify(new AdPublished($adPublished));
                    /* END send EMAIL ---------------------------------------------------------------
                     * START sms gateway code
                     * ----------------------------------------------------------------------------*/
                    $verifySMS = '';
                    if (in_array('2', $spam_details['spamreason_id'])) { //check id no 2 = number in description
                        if (!empty($ads->contact)) {
                            $arr = unserialize($ads->contact);
                            if (array_key_exists('0', $arr) && !empty($arr[0])) {
                                $smsMessage = 'Your ad Moved to Spam!Ad Ref: ' . $ads->ad_referance . ' . Reason: Include phone number in Description . Update Ad from your profile . ';
                                $verifySMS = Sms::Sendsms($arr[0], $smsMessage, '1');
                            }
                        }
                    }
                    /* -------------------------------------------------------------------------------
                     * END sms gateway code
                     * ------------------------------------------------------------------------------- */
                    return response()->json([
                        'message' => 'Success!Ad moved to Spam . ',
                        'url' => '/ads-manage/ad-review'
                    ]);
                }
            } catch (Exception $e) {
                return response()->json([
                    'errors' => $e->getMessage(),
                ]);
            }
        }
        /****************************************************************************************************************************
         * Blocked Ad
         ****************************************************************************************************************************/
        if ($request->status == 'blocked') {
            try {
                $ads = Alladsmaster::findOrFail($request->id);

                $update = Alladsmaster::updateStatus($ads, 'blocked');
                if ($update) {
                    return response()->json([
                        'message' => 'Success!Ad moved to Blocked Ads . ',
                        'url' => '/ads-manage/ad-review'
                    ]);
                }
            } catch (Exception $e) {
                return response()->json([
                    'errors' => $e->getMessage(),
                ]);
            }
        }
    }


    // DELETE Post-Ad

    public function deletepostad(Request $request)
    {

        //acceps array or serialize array
        try {
            $id = '';
            $table = '';
            if (is_array($request->all())) {
                $id = (!empty($request['id'])) ? $request['id'] : '';
                $table = (!empty($request['table'])) ? $request['table'] : false;
            }

            //delete main record
            $memberAd = Alladsmaster::where('id', $id)->with('member')->firstOrFail();

            $ad_referance = $memberAd->ad_referance;
//            $status = $memberAd->delete();
            $memberAd->status = 'deleted';
            $status = $memberAd->save();

            if ($status) {
                return response()->json([
                    'message' => 'Ad Successfully Deleted . ',
                    'url' => '/ads-manage/ad-review'
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'errors' => $e->getMessage(),
            ]);
        }
    }

//    delete ad with resons
    public function deletePostAdWithResons(Request $request)
    {
        //acceps array or serialize array
        try {
            $id = '';
            $table = '';
            if (is_array($request->all())) {
                $id = (!empty($request['id'])) ? $request['id'] : '';
                $delete_reson_id = (!empty($request['delete_reson_id'])) ? $request['delete_reson_id'] : false;
            }

            //delete main record
            $memberAd = Alladsmaster::where('id', $id)->with('member')->firstOrFail();
            $memberAd->deletereason_id = $delete_reson_id;
            $memberAd->status = 'deleted';
            $status = $memberAd->save();

            if ($status) {
                return response()->json([
                    'message' => 'Ad Successfully Deleted . ',
                    'url' => '/ads-manage/ad-review'
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'errors' => $e->getMessage(),
            ]);
        }
    }

    public function deletepostadPermanatly(Request $request)
    {
        dd('dsd');

        //acceps array or serialize array
//        try {
//            $id = '';
//            $table = '';
//            if (is_array($request->all())) {
//                $id = (!empty($request['id'])) ? $request['id'] : '';
//                $table = (!empty($request['table'])) ? $request['table'] : false;
//            }
//
//            //delete main record
//            $memberAd = Alladsmaster::where('id', $id)->with('member')->firstOrFail();
//
//            $ad_referance = $memberAd->ad_referance;
//            $status = $memberAd->delete();
//
//            //delete spam messages
//            $adspam = Adspam::where('alladsmaster_id', $id)->delete();
//
//            //delete directory with images
//            $filePath = public_path() . '/saleme/images/uploads/' . $id;
//            (File::exists($filePath)) ? File::deleteDirectory($filePath) : '';
//
//            $nexttable = '';
//            if ($status) {
//                if ($table) {
//                    switch ($table) {
//                        case 'veh':
//                            $nexttable = Allvehiclead::where('alladsmaster_id', $id)->delete();
//                            break;
//                        case 'ele':
//                            $nexttable = Allelectrnicad::where('alladsmaster_id', $id)->delete();
//                            break;
//                        case 'pro':
//                            $nexttable = Allpropertyad::where('alladsmaster_id', $id)->delete();
//                            break;
//                    }
//                }
//            }
//
//            //CHECK FOR BOOST ADS - delete
//            //delete from view count table
//            Boostviewcount::where('alladsmaster_id', $id)->delete();
//
//            //delete from member voucher table
//            $voucher = Membervoucher::where('ad_referance', $ad_referance)->whereNull('boostpack_id')->delete();
//
//            //delete from member voucher codes table
//            $voucherCode = Membervouchercode::where('ad_referance', $ad_referance)->where('status', true)->delete();
//
//            if ($status) {
//                //START send EMAIL ----------------------------------------------
////                $member = Member::where('id', $memberAd->member_id)->first();
////                $member->notify(new DeleteAd($memberAd));
//                //END send EMAIL ------------------------------------------------
//                return response()->json([
//                    'message' => 'Ad Successfully Deleted . ',
//                    'url' => '/ads-manage/ad-review'
//                ]);
//            }
//        } catch (Exception $e) {
//            return response()->json([
//                'errors' => $e->getMessage(),
//            ]);
//        }
    }

//----------republish deleted ads backend------------
    public function deletead_repblish(Request $request)
    {

        //acceps array or serialize array
        try {
            $id = '';
            if (is_array($request->all())) {
                $id = (!empty($request['id'])) ? $request['id'] : '';
            }

            //delete main record
            $memberAd = Alladsmaster::where('id', $id)->with('member')->firstOrFail();
            $memberAd->status = 'pending';
            $memberAd->deletereason_id = null;
            $status = $memberAd->save();

            if ($status) {
                return response()->json([
                    'message' => 'Ad Successfully Re - Published . ',
                    'url' => '/ads-manage/ad-review/deleted-ads'
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'errors' => $e->getMessage(),
            ]);
        }
    }
//----------END republish deleted ads backend------------

    /******************************************************************************
     * END delete post ad
     * START boosting ads - review backend
     ******************************************************************************/
    public function sendpintopsms(Request $request)
    {

        if($request->member_type == "private"){

            $memberPintotop = Membercontact::where('member_id', $request->member_id)->first();

            if (count($memberPintotop->contactnumber)) {
                $numbers = $memberPintotop->contactnumber;
                $smsMessage = 'Your Payment for Pin to Top.Please deposit LKR 1000/- and send us bank slip by Viber or WhatsApp to 0763818188.Banking details as follows SaleMe.lk (pvt) Limited Sampath Bank - A/C 010010005103 Thank you. 24/7 Support Team 0115 818188';
                $verifySMS = Sms::Sendsms($numbers, $smsMessage, '9');
                $smsResponce = json_decode($verifySMS, true);

            }
            if ($smsResponce['error'] == 0 OR $smsResponce['error'] == '') {
                flash('SMS sent Successfuly.', 'success');

            } else {
                flash($smsResponce['error'], 'error');
            }
            return back();

        }elseif ($request->member_type == 'premium'){

            $memberPremiums = Memberpremium::where('member_id', $request->member_id)->first();


            if(count($memberPremiums->mobile))
            {
                $numbers = array_slice($memberPremiums->mobile, 0,2);
                $smsMessage = 'Your Payment for Pin to Top.Please deposit LKR 750/- and send us bank slip by Viber or WhatsApp to 0763818188.Banking details as follows SaleMe.lk (pvt) Limited Sampath Bank - A/C 010010005103 Thank you. 24/7 Support Team 0115 818188';
                foreach($numbers as $number )
                {

                    $verifySMS = Sms::Sendsms($number, $smsMessage, '9');
                    $smsResponce = json_decode($verifySMS, true);
                }
            }
            if ($smsResponce['error'] == 0 OR $smsResponce['error'] == '') {
                flash('SMS sent Successfuly.', 'success');

            } else {
                flash($smsResponce['error'], 'error');
            }
            return back();


        }else{
            return back();
        }
    }

    public function boostpintotoppending()
    {
        $adPackRequests = Membervoucher::getAllAdPackRequests();
        $adPackRequestsSingle = Membervoucher::getAllAdPackRequestsSingle();
        $requestCount = count($adPackRequestsSingle) + count($adPackRequests);
        return view('backend.adsmanager.boosting.pin-to-top-pending',
            compact('adPackRequests', 'adPackRequestsSingle', 'requestCount'));
    }

    public function activateboostpintotopadspack(Request $request)
    {
//        dd($request->all());
        if (!empty($request->boostpackinvoiceno) && !empty($request->member_id)) {
            $updatecheck = Membervoucher::where('boostpackinvoiceno', $request->boostpackinvoiceno)
                ->where('member_id', $request->member_id)
                ->update([
                    'status' => 'active',
                    'activated_at' => Carbon::now()
                ]);
            flash('Success!Boost Ads Pack Activated Successfully . ', 'success');
            return back();
        }
    }

    public function boostpintotoplist()
    {
        $pintotopList = Boostviewcount::select('id', 'alladsmaster_id', 'status', 'created_at')->with(['allads' => function ($q) {
            $q->select('id', 'adtitle', 'slug', 'ad_boost_status', 'boost_expire_on', 'member_id')->with(['member' => function ($q2) {
                $q2->select('id', 'email', 'first_name', 'member_type');
            }]);
        }])->get();
        //dd($pintotopList);
        return view('backend.adsmanager.boosting.pin-to-top-list',
            compact('pintotopList'));
    }

    public function activateboostpintotopsingle(Request $request, $boosttype, Membervouchercode $vouchercode)
    {
        $voucher = $vouchercode->load('voucher')->voucher;
        if (!empty($request->boostpackinvoiceno) && !empty($request->member_id)) {
            //update membervouchers table
            $updatecheck = Membervoucher::where('boostpackinvoiceno', $voucher->boostpackinvoiceno)
                ->where('member_id', $request->member_id)
                ->update([
                    'status' => 'active',
                    'activated_at' => Carbon::now()
                ]);
            //update membervouchercode table
            $voucherCodeId = $request->vouchercode_id;
            $updatecheck = Membervouchercode::where('id', $voucherCodeId)
                ->where('boosttype', $request->boosttype)
                ->update([
                    'status' => true,
                    'activated_at' => Carbon::now()
                ]);
            //update allads master relevent record boost status
            if ($updatecheck) {
                Boostviewcount::create([
                    'alladsmaster_id' => $request->adid
                ]);
                Alladsmaster::where('ad_referance', $request->ad_referance)
                    ->update([
                        'ad_boost_status' => $request->boosttype,
                        'boost_expire_on' => Carbon::now()->addDays(7)->toDateTimeString(),
                        'updated_by' => getUserId()
                    ]);
            }
            //pintotop email start - to associate member
            $ad = Alladsmaster::with('member')->where('id', $request->adid)->first();
            $ad->member->notify(new PintotopRequest($ad));

            flash('Success ! Boost Ad Activated Successfully . ', 'success');
            return back();
        }
    }

    public function deleteboostpintotopsingle(Request $request, $boosttype, Membervoucher $voucher)
    {
        if ($boosttype == 'pin-to-top') {
            $voucher->load('vouchercodes');
            if (!empty($voucher->vouchercodes)) {
                $voucher->vouchercodes()->delete();
                $voucher->delete();
                flash('Success ! Pin-to-top Request Removed . ', 'success');
                return back();
            }
        }
    }

    public function boostpintotopremove(Request $request, Boostviewcount $boostad)
    {
        $submitDelete = Input::get('delete');
        $bootad = $boostad->load(['allads' => function ($q) {
            $q->select('id', 'ad_referance', 'ad_boost_status', 'boost_expire_on');
        }]);
        //update main table record on remove boost
        $adId = $bootad->alladsmaster_id;
        $voucher = Membervoucher::with('vouchercodes')->where('ad_referance', $bootad->allads->ad_referance)->first();
        if (!empty($voucher->vouchercodes)) {
            $voucher->vouchercodes()->delete();
            $status = $voucher->delete();
            $bootad->delete();
            if ($status) {
                if (isset($submitDelete)) {
                    $bootad->delete();
                } else {
                    $bootad->status = 'deleted';
                    $bootad->save();
                }

                $allads = Alladsmaster::where('id', $adId)->first();
                $allads->ad_boost_status = 'none';
                $allads->boost_expire_on = NULL;
                $allads->save();
            }
        } else {
            if (isset($submitDelete)) {
                $bootad->delete();
            }
        }

        flash('Success ! Boost Ad Removed . ', 'success');
        return back();
    }

    //    ignore pintotop
    public function ignoreBoostPintotoRequest(Request $request)
    {
        if (!empty($request->ad_referance)) {
            if (!empty($request->vouchercode) && !empty($request->member_id)) {
                $checkRecord = Membervoucher::where('ad_referance', $request->ad_referance)
                    ->where('member_id', $request->member_id)
                    ->update([
                        'status' => 'ignore',
                        'updated_at' => Carbon::now()
                    ]);
//                ->count();
            }
            if ($checkRecord) {
                flash('Success!Boosted ad request ignored successfully!.', 'success');
                return back();
            }
        }
    }

    public function postfacebookoffer()
    {

//        dd(Auth::user()->name);
//        $page_access_token = 'EAACEdEose0cBAOInZCnK3P2wmTQvDX4t0jg00H7M7vRT7Iitsr1T0IgRaslkrQX3S9vPqX2C5xZBfcsbLq4M7VyFTlutZCvZBzMDA4u61w7yULXB7dMnysRHw2mamqZAJgvTVO45biHITf2YHLUqrtNR6MyTjPihN2ZBTB7ZCoINMZCQ6uqGLlTJjaWphS9SLdJeYFYNZCIUpQmfmJzYR0NHd';
//        $page_id = '1940483436198573';
//
//        $data['picture'] = "https://saleme.lk/images/saleme/advertisments/squre-desktop.jpg";
//        $data['link'] = "https://saleme.lk/";
//        $data['type'] = "offer";
//        $data['application'] = Auth::user()->name;
//        $data['message'] = "Message - Sri Lanka's Fastest Classified Website . Hurry up !!Post your all classified ads absolutely free . Visit: https://saleme.lk";
//
//        $data['access_token'] = $page_access_token;
//        $post_url = 'https://graph.facebook.com/'.$page_id.'/feed';
//
////        dd($data);
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $post_url);
//        curl_setopt($ch, CURLOPT_POST, 1);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//        $return = curl_exec($ch);
//        curl_close($ch);
//
//        return response()->json([
//            'message' => $return,
//        ]);

    }

    public function pendingotp()
    {
        $otpNumbers = Otp::with('member')->orderBy('created_at', 'DESC')->paginate(10);
        return view('backend.adsmanager.check-otp', compact('otpNumbers'));
    }

    public function confirmotp(Request $request)
    {
        $otp = Otp::find($request->otpId);
        $number = $otp->number;
        $memberId = $otp->member_id;

        $newContact = new Membercontact();
        $newContact->member_id = $memberId;
        $newContact->contactnumber = $number;
        $statusNew = $newContact->save();
        if ($statusNew) {
            $otp->delete();
            Otp::where('number', $number)->delete();
            flash('Success! Number Confirmed!.', 'success');
            return back();
        }
    }

//    game sms
    public function gameSms()
    {
        $yesterday = \Carbon\Carbon::yesterday('Asia/Jayapura')->addHour(18)->toDateTimeString();
        $today = \Carbon\Carbon::yesterday('Asia/Jayapura')->addHour(42)->toDateTimeString();
//        dd(\Carbon\Carbon::yesterday()->addHour(42)->toDateTimeString());
        $leadboard = Game_detail::select('*')
            // ->whereBetween('updated_at', array($yesterday, $today))
            ->orderBy('score', 'DEC')
            ->limit(20)
            ->get();
        return view('backend.member.gamesms')->with('lead', $leadboard);

    }

    public function sendGameSMS(Request $request)
    {
        Sms::Sendsms("94" . $request->phone, $request->message, '9');
        flash('Success!SMS Sent!.', 'success');
        return back();
    }

//    assign ads packs from backend
    public function assignadspacks()
    {
        $member_premiums = Memberpremium::where('verified', 1)
            ->where('pintotopactivated', 0)
            ->get();
        return view('backend.adsmanager.boosting.assign-ads-packs')
            ->with('member_premiums', $member_premiums);
//        dd($member_premiums);
    }
     public function assignadspacksAll()
    {
        $member_premiums = Memberpremium::where('verified', 1)->orderBy('created_at', 'DESC')->get();
        return view('backend.adsmanager.boosting.assign-ads-packs-view-all')
            ->with('member_premiums', $member_premiums);
//        dd($member_premiums);
    }


    public function boostPinToTopAdsPacksReqest(Request $request)
    {
        $member = Memberpremium::whereId($request->premium_id)->first();
        if ($member->pintotopactivated) {
            flash('The pin to top already offered!', 'error');
            return back();
        }
//        dd($request->all());
        //create new boosting
        $voucher = new Membervoucher();
        $voucherInvoiceCode = self::genereateBoostInvoiceNo(); //GENERATE INVOICE CODE FOR ADS PACK
        $request->merge(['boostpackinvoiceno' => $voucherInvoiceCode]);
        $request->merge(['boosttype' => 'pin-to-top']);
        $request->merge(['status' => 'active']);
        $returnURL = '';
        $email = true;

        $pintotopcountoffer = 0;
        if (!empty($request->period) || $request->period != 0) {
            if ($request->period == 3) {
                $pintotopcountoffer = $request->period;
            } elseif ($request->period == 6) {
                $pintotopcountoffer = 5;
            } else {
                $pintotopcountoffer = 10;
            }
        }

        if ($request->ads_pack == 'true') {
            $pin_count = $pintotopcountoffer;
            $voucherCodes['pin-to-top'] = self::generateVoucherCodes($pin_count, $request->member_id);
            //$voucherCodes['bump-up'] = self::generateVoucherCodes($bump_count, getMemberId());
            $voucher->newVoucherForPacks($request->all(), $voucherCodes);
//            save pintotop offerd
            $member->period = $request->period;
            $member->pintotopactivated = 1;
            $member->save();
            flash('Success! Your Ad Boost Pack Request is Successfull! Confirm your payment to Activate Automatically.', 'success');
//            $returnURL = '/myprofile';
        }

        //pintotop email start
        if ($email) {
//            $member = getMember();
//            dd($member);
            $member->notify(new PintotopRequestFromBackend($pintotopcountoffer, $request->period));
        }
        return back();
    }

    protected static function genereateBoostInvoiceNo()
    {
        $string = 'SLM' . getMemberId() . '-' . date('ymd');
        $code = $string . '-' . substr(md5(microtime()), 0, 4);
        $checkBoostVoucherInvoiceCode = Membervoucher::where('boostpackinvoiceno', $code)->first();

        $unique = (empty($checkBoostVoucherInvoiceCode)) ? true : false;
        while (!$unique) {
            $code = $string . '-' . str_random(4); //GENERATE INVOICE CODE FOR ADS PACK
            $checkBoostVoucherInvoiceCode = Membervoucher::where('boostpackinvoiceno', $code)->first();
            if (empty($checkBoostVoucherInvoiceCode)) {
                $unique = true;
            }
        }
        return $code;
    }

    protected static function generateVoucherCodes($count, $adId)
    {
        $voucherCodes = array();
        while (count($voucherCodes) < $count) {
            $code = new UUID('voucher' . $adId . '_');
            $checkVoucherCode = Membervouchercode::where('vouchercode', $code)->first();
            if (empty($checkVoucherCode)) {
                $voucherCodes[] = $code->uuid;
            }
        }
        return $voucherCodes;
    }

    public function saveSpamAd(Request $request){
//        dd($request->all());
        $ad = Alladsmaster::where('id',$request->id)->first();
        $ad->description= $request->description;
        $res = $ad->save();

        if ($res){
            flash('Spam Ad edited Successfully.', 'success');
            return redirect()->back();

        }else{
            flash('Spam Ad not edited Successfully.', 'error');
            return redirect()->back();
        }

    }

    public function deleteAllDeletedAdsWithImages()
    {
//        dd('sddsd');
        $deleted = Alladsmaster::where('status','deleted')->limit(500)->get();
        $all = Alladsmaster::where('status','deleted')->count();
        echo 'Found '.count($deleted).' ads to delete out of '.$all.'<br><br>';
        $x = 0;
        foreach ($deleted as $del){
            echo 'Ad ID -> '.$del->id.',   ';
            $ad = Alladsmaster::where('id',$del->id)->first();
            $ad->delete();
//            dd($del->id);
            //delete directory with images
            $filePath = public_path() . '/saleme/images/uploads/' . $del->id;
            (File::exists($filePath)) ? File::deleteDirectory($filePath) : '';

            $veh = Allvehiclead::where('alladsmaster_id',$del->id)->first();
            if ($veh){
                $veh->delete();
            }
            $prop = Allpropertyad::where('alladsmaster_id',$del->id)->first();
            if ($prop){
                $prop->delete();
            }
            $ele = Allelectrnicad::where('alladsmaster_id',$del->id)->first();
            if ($ele){
                $ele->delete();
            }
            $hote = Allhoteltravelad::where('alladsmaster_id',$del->id)->first();
            if ($hote){
                $hote->delete();
            }
            $job = Alljobad::where('alladsmaster_id',$del->id)->first();
            if ($job){
                $job->delete();
            }
        $x++;
        }
//        dd($deleted);
        echo 'Deleted '.$x.' ads';
    }
}
