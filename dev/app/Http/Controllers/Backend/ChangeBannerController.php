<?php

namespace App\Http\Controllers\Backend;

use App\Models\HomeBanner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ChangeBannerController extends Controller
{
    //

    public function changebanner()
    {
        $data = HomeBanner::select('id', 'filename','location', 'status', 'created_at')
            ->get();
        return view('backend.settings.changebanner')->with('banners', $data);
    }

    public function uploadbanner(Request $request)
    {
        $file = $request->file;
        $ext = $file->guessExtension();
        $filename = $request->file->getClientOriginalName();
        $filename = pathinfo($filename, PATHINFO_FILENAME);
        $filename = uniqid();
        $file_name = $filename . '.' . $ext;

        Input::file('file')->move('./images/homepage/main-banner/', $file_name);

        $table = new HomeBanner();
        $table->filename = $file_name;
        $table->location = $request->location;
        $table->status = 1;
        $table->save();


        return redirect()->back();
    }


    public function changeStatus(Request $request, $id)
    {

        $banner = HomeBanner::where('id', $request->id)->first();
        $banner->status = $request->status;
        $res = $banner->update();

        if ($res) {
            if ($request->status) {
                return response()->json([
                    'status' => $res,
                    'message' => 'Active Success',
                ]);
            } else {
                return response()->json([
                    'status' => $res,
                    'message' => 'Deactive Success',
                ]);
            }

        } else {
            return response()->json([
                'status' => $res,
                'message' => 'error',
            ]);
        }


    }

    public function deleteimage(Request $request, $id)
    {

        $banner = HomeBanner::where('id', $request->id)->first();
        $exefile = 'images/homepage/main-banner/'.$banner->filename;
//        dd(file_exists($exefile));
        if (file_exists($exefile)) {
            unlink($exefile);
        }
        $res = $banner->delete();

        if ($res) {
            return response()->json(['message' => 'delete success', 'status' => 'success']);
        } else {
            return response()->json(['message' => 'delete not success', 'status' => 'danger']);
        }

    }
}
