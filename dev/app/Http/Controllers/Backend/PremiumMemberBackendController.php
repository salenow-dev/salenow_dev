<?php

namespace App\Http\Controllers\Backend;

use App\Dialogsms\Sms;
use App\Models\Memberpremium;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PremiumMemberBackendController extends Controller
{
    public function premiumList()
    {
        $members = Memberpremium::with('member')->where('list_show',1)->paginate(10);
        //       dd($members);
        return view('backend.member.allPremiummembers')
            ->with('members', $members);
    }

    public function premiumChangePeriod(Request $request, $id)
    {
        $premiummember = Memberpremium::where('id', $request->premiumid)->first();
        $premiummember->period = $request->period;
        $premiummember->expire_on=$premiummember->created_at->addMonths($request->period);
        $res = $premiummember->save();
        $endDate = $premiummember->created_at->addMonths($request->period)->toFormattedDateString();

        if ($premiummember->created_at->addMonths($premiummember->period)->gt(Carbon::now())) {
            $Expire = $premiummember->created_at->addMonths($premiummember->period)->diffInDays(Carbon::now());
            $toExpire = 'Expire Within <b>' . $Expire . ' Day(s)</b> ';
            $class = 'text-success';
        }else{
            $Expire = Carbon::now()->diffInDays($premiummember->created_at->addMonths($premiummember->period));
            $toExpire = "Account has Expired <b>". $Expire ." Day(s) ago</b>";
            $class = 'text-danger';
        }

        if ($res) {
            return response()->json(['message' => 'Premium Member Valid period Updated successfully', 'status' => 'success', 'end_date' => $endDate, 'to_expire' => $toExpire,'class'=>$class]);
        } else {
            return response()->json(['message' => 'Premium Member Valid period Not Updated', 'status' => 'danger']);
        }
    }

    public function premiumListExport(){
        $members = Memberpremium::with('member')
            ->where('list_show',1)
            ->where('verified',1)
            ->get();
//        dd($members);
        return view('backend.member.export-premium-details')
            ->with('members',$members);
    }

    public function sendRenewSMS(Request $request){
//        dd($request->all());

        $member = Memberpremium::where('id',$request->premiummemberid)->first();
        $member->sms_sent_on = date('Y-m-d');
        $member->update();

        $verifySMS = Sms::Sendsms($request->number, $request->message, '9');
        $smsResponce = json_decode($verifySMS, true);

        if ($smsResponce['error'] == 0 OR $smsResponce['error'] == '') {
            return response()->json(['status'=>true, 'message'=> 'SMS sent successfully!']);

        } else {
            return response()->json(['status'=>false, 'message'=> $smsResponce['error']]);
        }
    }
}
