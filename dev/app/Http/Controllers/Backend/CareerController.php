<?php

namespace App\Http\Controllers\Backend;

use App\Mail\SendCv;
use App\Models\Career;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use function Sodium\compare;
use App\Mail\ApplyNow;

class CareerController extends Controller
{
    public function index()
    {
        $careerlist = Career::select('id', 'title', 'position', 'description', 'salary', 'date', 'experiencelevel', 'requirements', 'benefits', 'created_at', 'updated_at')
            ->get();
        return view('pages.careers', compact('careerlist'));
    }

    public function apply(Request $request)
    {
        $careerlist = Career::select('id', 'title', 'position', 'description', 'salary', 'date', 'experiencelevel', 'requirements', 'benefits', 'created_at', 'updated_at')
            ->where('id', $request->jobno)
            ->get();
        return view('pages.careerdetail', compact('careerlist'));
    }
    public function applyform(Request $request)
    {
        $file = $request->attachment;
        $ext = $file->guessExtension();
        $filename = $request->attachment->getClientOriginalName();
        $filename = pathinfo($filename, PATHINFO_FILENAME);
        $filename = $request->first_name . ' ' . $request->last_name . '- CV';
        $file_name = $filename . '.' . $ext;
        input::file('attachment')->move('./pdf_files', $file_name);
        $data = array('first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'position' => $request->position,
            'gender' => $request->gender,
            'birthday' => $request->birthday,
            'marital_status' => $request->marital_status,
            'date' => $request->date,
            'month' => $request->month,
            'attachment' => $file_name,
        );
        $msg = "You have Successfully apply to " . $request->position;
        DB::table('candidates')->insert($data);
        Mail::to('pramodrandima@gmail.com')->send(new SendCv($data, $file));
//        try {
//            $data = $request->all();
//            $file = input::file('attachment');
//            Mail::to('pramodrandima@gmail.com')->send(new SendCv($data, $file));
//            return "success";
////            return response()->json(['success' => true, 'message' => "Successfully applied for ".$request->apply_for, 'class' => 'alert-success']);
//
//        } catch (Exception $ex) {
//            return response()->json(['success' => false, 'message' => "Apply job not success", 'class' => 'alert-danger']);
//        }
        session()->flash('msg', $msg);
        return redirect('careers');
    }
}
