<?php

namespace App\Http\Controllers\Backend;

use App\Models\Categoryfilter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryfilterController extends Controller
{
    public function index()
    {
        $filterValues = null;
        $allList = Categoryfilter::all();
        return view('backend.categoryfilters.manage', compact('allList','filterValues'));
    }

    public function store(Request $request)
    {
//        dd($request->all());
//        $this->validate($request, [
//            'filtername' => 'required|unique:categoryfilters',
//            'sortby' => 'required|numeric|unique:categoryfilters'
//        ]);
        if (!empty($request->filtervalue)) {
            $filtervalues = explode(',', $request->filtervalue);
            $request->merge(['filtervalue' => $filtervalues]);
        }

        $obj = new Categoryfilter();
        $status = $obj->createRecord($request->all());
        if ($status) {
            flash('Thank you! New Record added successfully.', 'success');
        } else {
            flash('Sorry! Something went wrong.', 'danger');
        }
        return redirect()->back();
    }

    public function edit(Categoryfilter $filter)
    {
        $filter->load('categoryfiltervalues')->select('filtervalue');
        $filterValuesArr = array();
        foreach ($filter->categoryfiltervalues as $categoryfiltervalue) {
            $filterValuesArr[] = $categoryfiltervalue->filtervalue;
        }
        $filterValues = implode(', ', $filterValuesArr);
        return view('backend.categoryfilters.edit', compact('filter', 'filterValues'));
    }

    public function update(Request $request, Categoryfilter $filter)
    {
//        dd($filter->id);
//        $this->validate($request, [
//            'filtername' => 'required|unique:categoryfilters,filtername,' . $filter->id,
//            'sortby' => 'required|numeric|unique:categoryfilters,sortby,' . $filter->id
//        ]);

        if (!empty($request->filtervalue)) {
            $filtervalues = explode(',', $request->filtervalue);
            $request->merge(['filtervalue' => $filtervalues]);
        }

        $obj = new Categoryfilter();
        $status = $obj->updateRecord($request->all(), $filter);
        if ($status) {
            flash('Thank you! Record updated successfully.', 'success');
        } else {
            flash('Sorry! Something went wrong.', 'danger');
        }
        return redirect('settings/filters');
    }
}
