<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Advertisementtype;
use Illuminate\Http\Request;

class AdvertisementtypeController extends Controller
{
    public function index()
    {
        $allList = Advertisementtype::all();
        return view('backend.advertismenttypes.manage', compact('allList'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'typename' => 'required|unique:advertisementtypes',
            'typeref' => 'required|unique:advertisementtypes',
        ]);

        $obj = new Advertisementtype();
        $status = $obj->createRecord($request->all());
        if ($status) {
            flash('Thank you! New Record added successfully.', 'success');
        } else {
            flash('Sorry! Something went wrong.', 'danger');
        }
        return redirect()->back();
    }

    public function edit(Advertisementtype $adtype)
    {
        return view('backend.advertismenttypes.edit', compact('adtype'));
    }

    public function update(Request $request, Advertisementtype $adtype)
    {
        $this->validate($request, [
            'typename' => 'required|unique:advertisementtypes,typename,'.$adtype->id,
            'typeref' => 'required|unique:advertisementtypes,typeref,'.$adtype->id,
        ]);

        $obj = new Advertisementtype();
        $status = $obj->updateRecord($request->all(), $adtype);
        if ($status) {
            flash('Thank you! Record updated successfully.', 'success');
        } else {
            flash('Sorry! Something went wrong.', 'danger');
        }
        return redirect('settings/adtypes');
    }
}
