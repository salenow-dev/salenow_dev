<?php

namespace App\Http\Controllers\Backend;

use App\AgentInvoice;
use App\Dialogsms\Sms;
use App\Mail\AgentInvoiceMail;
use App\Models\Agent;
use App\Models\Alladsmaster;
use App\Models\Member;
use App\Models\Memberpremium;
//use Barryvdh\DomPDF\PDF;
use Barryvdh\DomPDF\Facade as PDF;
//use Barryvdh\DomPDF\PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class AgentController extends Controller
{
    //
    public function addagent()
    {


        return view('backend.agent.addagent');
    }

    public function searchmember(Request $request)
    {

        $email = $request->email;
        $user = Memberpremium::where('email', $email)
            ->where('verified', 1)->get();
        $exsisting = Agent::where('email', $email)->first();


        if (!count($user)) {
            $message = 'Sorry ! No Any Members in this Email - ' . $request->email;
            return view('backend.agent.addagent')
                ->with('message', $message)
                ->with('alert', 'alert-danger')
                ->with('email', $email);
        }
        if ($exsisting) {
            $message = ' allready a Agent name as ' . $exsisting->company_name;
            return view('backend.agent.addagent')
                ->with('message', $message)
                ->with('alert', 'alert-success')
                ->with('email', $email);
        } else {
            return view('backend.agent.addagent')
                ->with('users', $user)
                ->with('email', $email);
        }
    }

    public function newagent(Request $request)
    {
        $agent = Memberpremium::where('id', $request->id)->first();
        if (count($agent->mobile)) {
            $number = $agent->mobile;
            $number = $number[0];
        } else {
            $number = "";
        }

//        $request->merge(['contact_no', $number[1]]);
//        dd($request->all());
        $data = new Agent();

        $data->memberpremiums_id = $agent->id;
        $data->member_id = $agent->member_id;
        $data->company_name = $agent->company_name;
        $data->email = $agent->email;
        $data->contact_no = $number;
        $data->status = 'active';

        $a = $data->save();


        return redirect('agents/allagents');

    }

    public function allAgents()
    {
        $agents = Agent::where('status', 'active')->get();
        return view('backend.agent.allagents')->with('agent', $agents);
    }

    public function agentSingle(Request $request)
    {

        $user = Agent::where('id', $request->id)->first();

        $premeium = Memberpremium::where('id', $user->memberpremiums_id)->first();
        $id = $user->member_id;


//        Vehicle Ads Count Today//=================
        $vehicleadcount = new Alladsmaster();
        $vadscount = $vehicleadcount->AgentVehicleAdsToday($id);

        if (empty($vadscount)) {
            $vehicle = 0;
        } else {
            $vehicle = $vadscount[0]->vehicleadcount;
        }
//        Vehicle Ads Count Month//================
        $vehicleadcountmonth = new Alladsmaster();
        $vadscountmonth = $vehicleadcountmonth->AgentVehicleAdsMonth($id);

        if (empty($vadscountmonth)) {
            $vehiclemonth = 0;
        } else {
            $vehiclemonth = $vadscountmonth[0]->vehicleadcount;
        }

//        Property Ads Count Today//===============
        $proadscount = new Alladsmaster();
        $padscount = $proadscount->AgentPropertyAdsToday($id);

        if (empty($padscount)) {
            $property = 0;
        } else {
            $property = $padscount[0]->agentadcount;
        }

//        Property Ads Count Month//================
        $proadscountmonth = new Alladsmaster();
        $padscountmonth = $proadscountmonth->AgentPropertyAdsMonth($id);

        if (empty($padscountmonth)) {
            $padscountmonth = 0;
        } else {
            $propertymonth = $padscountmonth[0]->agentadcount;
        }
        $one_week_ago = Carbon::now()->subWeeks(2);
        $yesterday = Carbon::now()->subDays(1);
        $pastWeekAds = Alladsmaster::where('member_id', $id)->selectRaw('date(created_at) as date, COUNT(*) as count')->whereBetween(DB::raw('date(created_at)'), [$one_week_ago, $yesterday])
            ->groupBy('date')->orderBy('date', 'DESC')->get();

        $total = $vehiclemonth + $propertymonth;
        $earn = $total * 50;


        return view('backend.agent.agentsingle')
            ->with('agent', $user)
            ->with('member', $premeium)
            ->with('earning', $earn)
            ->with('vehicle', $vehicle)
            ->with('vehiclemonth', $vehiclemonth)
            ->with('propertymonth', $propertymonth)
            ->with('pastWeekAds', $pastWeekAds)
            ->with('property', $property);
    }

    public function makepayment($id)
    {
        //        Vehicle Ads Count Month//================
        $vehicleadcountmonth = new Alladsmaster();
        $vadscountmonth = $vehicleadcountmonth->AgentVehicleAdsMonth($id);

        if (empty($vadscountmonth)) {
            $vehiclemonth = 0;
        } else {
            $vehiclemonth = $vadscountmonth[0]->vehicleadcount;
        }
        //        Property Ads Count Month//================
        $proadscountmonth = new Alladsmaster();
        $padscountmonth = $proadscountmonth->AgentPropertyAdsMonth($id);

        if (empty($padscountmonth)) {
            $padscountmonth = 0;
        } else {
            $propertymonth = $padscountmonth[0]->agentadcount;
        }


        $total = $vehiclemonth + $propertymonth;
        $earn = $total * 50;
        $user = Agent::where('member_id', $id)->first();
        $premeium = Memberpremium::where('member_id', $id)->first();

        $today = Carbon::today()->toDateString();
        $agentid = Agent::where('member_id', $id)->first();

//        invoice ref number generate  Y/M/AgentId/InvoiceId
        $refday = $carbon = new Carbon('today');
        $date = new Carbon($refday);
        $last = DB::table('agent_invoices')->count();
        $last++;
        $invoice_number = str_pad($last, 5, '0', STR_PAD_LEFT);
        $invoice_ref = $date->year . "-" . $date->month . "-" . $agentid->id . "-" . $invoice_number;


        $invoice = new AgentInvoice();
        $invoice->agents_id = $agentid->id;
        $invoice->invoice_ref = $invoice_ref;
        $invoice->save();


        $arr = ($premeium->mobile);

        if (array_key_exists('0', $arr) && !empty($arr[0])) {
            $smsMessage = 'Dear Valued Agent, Your have post' . $total . ' ads and Earn ' . $earn . '.00 LKR for this Month.Your payment will recived from SaleMe.lk.Thank You!';
            $verifySMS = Sms::Sendsms($arr[0], $smsMessage, '1');
        }


        Mail::to($user->email)->send(new AgentInvoiceMail($vehiclemonth, $propertymonth, $invoice_ref, $earn, $today, $premeium, $total));


        return view('backend.agent.agentinvoice')
            ->with('vehiclemonth', $vehiclemonth)
            ->with('propertymonth', $propertymonth)
            ->with('invoiceref', $invoice_ref)
            ->with('earning', $earn)
            ->with('today', $today)
            ->with('member', $premeium)
            ->with('total', $total);

    }


}
