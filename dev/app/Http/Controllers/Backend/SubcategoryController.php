<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Advertisementtype;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Categoryfilter;
use App\Models\Subcategory;
use Illuminate\Http\Request;

class SubcategoryController extends Controller
{
    public function index()
    {
        $categoryfilters = Categoryfilter::pluck('filtername', 'id')->toArray();
        $adtypes = Advertisementtype::pluck('typename', 'id')->toArray();
        $categories = Category::pluck('category_name', 'id')->toArray();
        $subcategories = Subcategory::with('category')->get();
        $allSubCategories = Subcategory::pluck('sub_category_name', 'id')->toArray();
        $allBrands = Brand::pluck('brand_name', 'id')->toArray();
        return view('backend.subcategory.categories', compact('categories', 'adtypes', 'subcategories', 'categoryfilters', 'allBrands', 'allSubCategories'));
    }


    public function store(Request $request)
    {
        if (!empty($request->adtype)) {
            $adtypes = serialize($request->adtype);
            $request->merge(['adtype' => $adtypes]);
        }

        $this->validate($request, [
            'sub_category_name' => 'required|unique:sub_categories',
            'sub_category_code' => 'required|unique:sub_categories',
//            'widget_id' => 'required|numeric:sub_categories,widget_id',
        ]);

        //validate from helper function - for ajax
//        $validateRules = array(
//            'sub_category_name' => 'required|unique:sub_categories,sub_category_name',
//            'sub_category_code' => 'required|unique:sub_categories,sub_category_code',
//            'widget_id' => 'required|numeric:sub_categories,widget_id',
//        );
//        $dat = validate($request, $validateRules);
//        if (!is_null($dat)) {
//            return $dat;
//        }

        $obj = new Subcategory();
        $status = $obj->createRecord($request->all());
        if ($status) {
            flash('Thank you! New Record added successfully.', 'success');
//            return response()->json(['response' => array(
//                'message' => 'Successfully Created!',
//                'redirect' => '/categories/subcategory'
//            )]);
        } else {
            flash('Sorry! Something went wrong.', 'danger');
//            return response()->json(['response' => array(
//                'error' => 'Something went wrong!'
//            )]);
        }
        return redirect()->back();
    }

    public function edit(Subcategory $subcategory)
    {
        $subcatFilters = $subcategory->subcategoryfilters;
        $filters = array();
        foreach ($subcatFilters as $filter) {
            $filters[] = $filter->id;
        }
        $subcategory->categoryfilter_id = $filters;

        $categoryfilters = Categoryfilter::pluck('filtername', 'id')->toArray();
        $dbadtypes = unserialize($subcategory->adtype);
        $subcategory->adtype = $dbadtypes;
        $adtypes = Advertisementtype::pluck('typename', 'id')->toArray();
        $categories = Category::pluck('category_name', 'id')->toArray();
        return view('backend.subcategory.edit', compact('subcategory', 'categories', 'adtypes', 'dbadtypes', 'categoryfilters'));
    }

    public function update(Request $request, Subcategory $subcategory)
    {

        $this->validate($request, [
            'sub_category_name' => 'required|unique:sub_categories,sub_category_name,' . $subcategory->id,
            'sub_category_code' => 'required|unique:sub_categories,sub_category_code,' . $subcategory->id,
            'widget_id' => 'required|numeric:sub_categories,widget_id,' . $subcategory->id,
        ]);

        if (!empty($request->adtype)) {
            $adtypes = serialize($request->adtype);
            $request->merge(['adtype' => $adtypes]);
        }
        $obj = new Subcategory();
        $status = $obj->updateRecord($request->all(), $subcategory);
        if ($status) {
            flash('Thank you! New Record updated successfully.', 'success');
        } else {
            flash('Sorry! Something went wrong.', 'danger');
        }
        return redirect('settings/subcategory');
    }

    public function assignbrands(Request $request)
    {
//        dd($request->all());
        $obj = Subcategory::find($request->subcategory_id);
        $obj->brands()->detach();
        $status = $obj->brands()->attach($request['brand_id']);
//        dd($request['brand_id']);
        flash('Brands Assign Successfully!', 'success');
        return back();
    }

    public function getassignedbrands(Request $request)
    {
        if (!empty($request->id)) {
            $id = $request->id;
            $allBrands = Brand::pluck('brand_name', 'id')->toArray();
            $subcat = Subcategory::where('id', $id)->with([
                'brands' => function ($query) {
                    $query->select('id', 'brand_name');
                }])->first();
            $assignedBrands = $subcat->brands;
            $brands = '';
            foreach ($assignedBrands as $assignedBrand) {
                $brands[$assignedBrand->id] = true;
            }

            $options = "";
            foreach ($allBrands as $key => $allBrand) {
                $checked = '';
                if(isset($brands[$key])){
                    $checked = 'checked';
                }
//                $options .= '<span>'.$allBrand . ' <input type="checkbox" name="brand_id[]" value="'.$key.'" '.$checked.'></span>';
                $options .= '<div class="col-md-4"><div class="checkbox checkbox-primary">
                        <input id="id'.$key.'" name="brand_id[]" type="checkbox" value="'.$key.'" '.$checked.'>
                        <label for="id'.$key.'"><span class="chk-span" tabindex="2"></span> '.$allBrand . '</label>
                    </div></div> ';

            }
            $element = $options;
            return response()->json(['response' => array(
                'brands' => $element,
            )]);
        }
    }
}


