<?php

namespace App\Http\Controllers\Backend;

use App\HomeBanner;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderBy('sequence')->get();
//        dd($categories);
        return view('backend.category.categories', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $validate = $this->validate($request, [
//            'category_name' => 'required|unique:categories',
//            'category_code' => 'required|unique:categories',
//            'sequence' => 'required|unique:categories,sequence',
//        ]);

        $rules = array(
            'category_name' => 'required|unique:categories,category_name' ,
            'category_code' => 'required|unique:categories,category_code',
            'sequence' => 'required|unique:categories,sequence',
        );
        $dat = validate($request, $rules, $id = null, []);
        if (!is_null($dat)) {
            return $dat;
        }

        $obj = new Category();
        $status = $obj->createRecord($request->all());
        if ($status) {
            flash('Thank you! New Record added successfully.', 'success');
//            return response()->json(['response' => array(
//                'message' => 'Successfully Created!',
//                'redirect' => '/categories/subcategory'
//            )]);
        } else {
            flash('Sorry! Something went wrong.', 'danger');
//            return response()->json(['response' => array(
//                'error' => 'Something went wrong!'
//            )]);
        }
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $mainCategories = Category::pluck('category_name', 'id')->toArray();
        $subCategories = Subcategory::where('category_id', $category->id)->get();
        return view('backend.category.edit', compact('category', 'subCategories', 'mainCategories'));
    }

    public function update(Request $request, Category $category)
    {
        $rules = array(
            'category_name' => 'required|unique:categories,category_name,' . $category->id,
            'category_code' => 'required|unique:categories,category_code,' . $category->id,
//            'sequence' => 'required|unique:categories,sequence',
        );
        $dat = validate($request, $rules, $id = null, []);
        if (!is_null($dat)) {
            return $dat;
        }

        $obj = new Category();
        $obj->updateRecord($request->all(), $category);
        return redirect('settings/category');
    }

    public function changecategoryorder()
    {
        $categories = Category::orderBy('sequence')->get();
        return view('backend.category.changecategoryorder', compact('categories'));
    }

    public function ordercategory()
    {
        try {
            $catids = $_POST['item'];
            $i = 1;
            foreach ($catids as $catid) {
                Category::where('id', $catid)->update(['sequence' => $i]);
                $i++;
            }
            return response()->json(['response' => array(
                'message' => 'Category Order Changed!',
            )]);
        } catch (Exception $e) {
            return response()->json(['response' => array(
                'error' => 'Something went wrong!'
            )]);
        }
    }

    public function changesubcategory(Request $request)
    {
//        dd($request->all());
        $obj = Subcategory::find($request->sub_cat_id);
        $obj->category_id = $request->category_id;
        if ($obj->save()) {
            return response()->json(['response' => array(
                'message' => 'Sub-Category Changed!',
            )]);
        }
    }

}
