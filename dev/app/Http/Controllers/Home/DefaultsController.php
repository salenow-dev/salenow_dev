<?php

namespace App\Http\Controllers\Home;

use App\Classes\Boostpinintotop;
use App\Classes\TrackVisitor;
use App\Events\Pintotopviewcounter;
use App\Models\HomeBanner;
use App\Mail\ApplyNow;
use App\Models\Alladsfilter;
use App\Models\Alladsmaster;
use App\Models\Alladsmaster_member;
use App\Models\Alljobad;
use App\Models\Boostviewcount;
use App\Models\Categoryfiltervalue;
use App\Models\Electronicoption;
use App\Models\Propertyoption;
use App\Models\Vehiclecondition;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Membermaster;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use App\Models\Adimage;
use App\Models\Member;
use App\Models\Vehicleoption;
use App\Dialogsms\Sms; //   sms 
use App\Models\Category;
use App\Models\Common;
use App\Models\District;
use App\Models\City;
use App\Models\Subcategory;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Models\Advertisementtype;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

//use App\User;

class DefaultsController extends Controller
{
    public function adsense()
    {
        return view('default.adsense');
    }

    public function visitorclisks(Request $request)
    {
        if (!empty($request->adid) && !empty($request->type)) {
            $id = $request->adid;
            $type = $request->type;
            $track = new TrackVisitor();
            $track->trackVisitor($id, $type);
        }
    }

    public function index()
    {
        //Saleme Home Route
        $catlist = Category::withCount('allAds')->get(); // category  list

        $citylist = City::select('id', 'district_id', 'city_name', 'slug')->get(); // for popup search
        $districlist = District::select('id', 'district_name', 'district_code', 'slug')->get()->toArray();
        $allcatandsubcatlist = Category::with('subcategories')->get()->toArray(); // category  list

        $showCity = 1;
        $citywithdis = District::select('id', 'province_id', 'district_name', 'slug', 'district_code')->withCount(['allads' => function ($q) {
            $q->where('status', 'confirm');
        }])->orderBy('allads_count', 'desc')->with(['cities' => function ($query) {
            $query->select('id', 'district_id', 'city_name', 'slug', 'city_code');
        }])->get();
        $alldidcity = $citywithdis; // for pop up
        $categories = Category::select('id', 'sequence', 'category_name', 'category_code', 'slug', 'icon', 'status')->withCount(['allAds' => function ($q) {
            $q->where('status', 'confirm');
        }])->with(['subcategories' => function ($q) {
            $q->select('id', 'category_id', 'sub_category_code', 'sub_category_name', 'slug', 'icon', 'widget_id', 'adtype', 'status');
        }])->get();
        //dd($categories);

//        banner random
        $images = HomeBanner::select('filename')
            ->where('status',1)
            ->where('location','home')
            ->orderByRaw("RAND()")
            ->first();

        //        banner random end


        return view('default.index')
            ->with('catlist', $catlist)
//            ->with('alldidcity', $alldidcity)
            ->with('citywithdis', $citywithdis)
            ->with('allcatandsubcatlist', $allcatandsubcatlist)
            ->with('citylist', $citylist)
            ->with('dislist', $districlist)
            ->with('alldidcity', $alldidcity)
            ->with('showCity', $showCity)
            ->with('allcategory', $categories)
            ->with('image',$images);
    }

    public function ad_details(Request $request)
    {
        $currentURL = URL::current();
        $slug = $request->slug;
//        $favorites = array();
//        $favorites = Alladsmaster_member::select('alladsmaster_id')->where('member_id',getMemberId())->get()->toArray();
//        dd($favorites);

        $adById = Alladsmaster::where('slug', $slug)->select('id')->first();
        if (!$adById) {
            abort(404);
        }

        $ad_id = $adById->id;
        //get only common data
        $catcode = Alladsmaster::where('id', $ad_id)->with(['category' => function ($qq) {
            $qq->select('id', 'category_code');
        }])->select('id', 'ad_referance', 'member_id', 'category_id', 'sub_category_id', 'status')->first();
        if ($catcode->status == 'pending') {
            return redirect('reviewsuccess?ref=' . $catcode->ad_referance);
        }

        $images = Adimage::where('alladsmaster_id', $ad_id)->select('alladsmaster_id', 'imagename')->get();
        // memeber detaisl
        $memeberdetails = Member::MemberDetailsByIdforAdDetails($catcode->member_id);
        $categorycode = $catcode->category->category_code;
        $view = '';
        $features = array();
        $generalinfo = array();
        $relatedads = '';
        $layout = 'default.details';
        $query_data = '';
        $favorites='';
        if(getMemberId()){
            $favorites=Alladsmaster_member::select('alladsmaster_id')->where('member_id',getMemberId())->groupBy('alladsmaster_id')->get()->toArray();
            $favorites=array_pluck($favorites,'alladsmaster_id');
//            dd(array_pluck($favorites,'alladsmaster_id'));
        }
        switch ($categorycode) {
            case 'veh':
                $ad_info = Alladsmaster::GetVehidatabyid($ad_id);
                if (!empty($ad_info)) {
                    $ad_info->created_at = Carbon::parse($ad_info->created_at);
                    $related = Alladsmaster::relatedAdsBysubCatid($catcode->sub_category_id, $ad_id, $categorycode, $ad_info->brand_id); //Related ads
                    $relatedads = (!empty($related)) ? $related : $relatedads;
                    //contact info
                    $contacts = (!empty($ad_info->contact)) ? unserialize($ad_info->contact) : '';
                    $featuresids = Alladsmaster::GetVehifeaturesbyid($ad_id);
                    //start value added features
                    $optionsids = unserialize($featuresids->features);
                    if (!empty($optionsids)) {
                        foreach ($optionsids as $ids) {
                            $dispal = Vehicleoption::OptionsGetById($ids);
                            if (!empty($dispal->display_name)) {
                                $features[] = $dispal->display_name;
                            }
                        }
                    }
                    //end value added features
                    if (!empty($ad_info->itemtype)) {
                        $itemtype = Categoryfiltervalue::where('id', $ad_info->itemtype)->select('filtervalue')->first();
                    }
                    if (!empty($ad_info->itemcondition)) {
                        $itemcondition = Vehiclecondition::where('id', $ad_info->itemcondition)->select('condition_name')->first();
                    }
                }
                $icons = array(
                    'Condition' => (!empty($ad_info->condition_name)) ? $ad_info->condition_name : '',
                    'Transmission' => (!empty($ad_info->transmission_name)) ? $ad_info->transmission_name : '',
                    'Fuel Type' => (!empty($ad_info->fuel_name)) ? $ad_info->fuel_name : '',
                    'Kms Driven' => (!empty($ad_info->mileage)) ? $ad_info->mileage : ''
                );
                $generalinfo = array(
                    'Brand' => (!empty($ad_info->brand_name)) ? $ad_info->brand_name : '',
                    'Model' => (!empty($ad_info->model)) ? $ad_info->model : '',
                    'Model Year' => (!empty($ad_info->registry_year)) ? $ad_info->registry_year : '',
                    'Engine capacity' => (!empty($ad_info->enginesize)) ? $ad_info->enginesize : '',
                    'Body Type' => (!empty($ad_info->bodytype)) ? $ad_info->bodytype : '',
                );

                $bodytype_id = !empty($ad_info->bodytype_id) ? 'vehi[type][]=' . $ad_info->bodytype_id : '';
                $brand_id = !empty($ad_info->brand_id) ? 'vehi[brand][]=' . $ad_info->brand_id : '';
                $query_data = $bodytype_id . '&' . $brand_id;
                break;
            case 'ele':
                $ad_info = Alladsmaster::GetElecdatabyid($ad_id);
                if (!empty($ad_info)) {
                    $ad_info->created_at = Carbon::parse($ad_info->created_at);
                    //contact info
                    $contacts = (!empty($ad_info->contact)) ? unserialize($ad_info->contact) : '';
                    //Related ads
                    $related = Alladsmaster::relatedAdsBysubCatid($catcode->sub_category_id, $ad_id, $categorycode, $ad_info->brand_id);
                    $relatedads = (!empty($related)) ? $related : $relatedads;

                    //start value added features
                    $optionsids = unserialize($ad_info->features);
                    if (!empty($optionsids)) {
                        foreach ($optionsids as $ids) {
                            $dispal = '';
                            $dispal = Electronicoption::where('id', $ids)->select('display_name', 'option_name', 'id')->first();
                            if (!empty($dispal->display_name)) {
                                $features[] = $dispal->display_name;
                            }
                        }
                    }

                    //end value added features
                    if (!empty($ad_info->itemtype)) {
                        $itemtype = Categoryfiltervalue::where('id', $ad_info->itemtype)->select('filtervalue')->first();
                    }
                    if (!empty($ad_info->itemcondition)) {
                        $itemcondition = Categoryfiltervalue::where('id', $ad_info->itemcondition)->select('filtervalue')->first();
                    }
                    if (!empty($ad_info->authenticity)) {
                        $authenticity = Categoryfiltervalue::where('id', $ad_info->authenticity)->select('filtervalue')->first();
                    }
                }
                $icons = array(
                    'Condition' => (!empty($itemcondition->filtervalue)) ? $itemcondition->filtervalue : '',
                    'Authenticity' => (!empty($authenticity->filtervalue)) ? $authenticity->filtervalue : '',
                );
                $generalinfo = array(
                    'Device Type' => (!empty($itemtype->filtervalue)) ? $itemtype->filtervalue : '',
                    'Brand' => (!empty($ad_info->brand_name)) ? $ad_info->brand_name : '',
                    'Model' => (!empty($ad_info->model)) ? $ad_info->model : '',
                );
                $bodytype_id = !empty($ad_info->bodytype_id) ? 'vehi[type][]=' . $ad_info->bodytype_id : '';
                $brand_id = !empty($ad_info->brand_id) ? 'vehi[brand][]=' . $ad_info->brand_id : '';
                $query_data = $bodytype_id . '&' . $brand_id;
                break;
            case 'pro':
                $ad_info = Alladsmaster::GetPropertybyid($ad_id);
                $related = Alladsmaster::relatedAdsBysubCatid($catcode->sub_category_id, $ad_id, $categorycode); //Related ads
                $relatedads = (!empty($related)) ? $related : $relatedads;
                if (!empty($ad_info)) {
                    $ad_info->created_at = Carbon::parse($ad_info->created_at);
                    //contact info
                    $contacts = (!empty($ad_info->contact)) ? unserialize($ad_info->contact) : '';
                    //load beds
                    if (!empty($ad_info->beds)) {
                        $beds = Categoryfiltervalue::where('id', $ad_info->beds)->select('filtervalue')->first();
                    }
                    //load baths
                    if (!empty($ad_info->baths)) {
                        $baths = Categoryfiltervalue::where('id', $ad_info->baths)->select('filtervalue')->first();
                    }

                    if (!empty($ad_info->itemtype)) {
                        $itemtype = Categoryfiltervalue::where('id', $ad_info->itemtype)->select('filtervalue')->first();
                    }
                    if (!empty($ad_info->itemcondition)) {
                        $itemcondition = Categoryfiltervalue::where('id', $ad_info->itemcondition)->select('filtervalue')->first();
                    }
                }

                $icons = array(
                    'Bedrooms' => (!empty($beds->filtervalue)) ? $beds->filtervalue : '',
                    'Bathrooms' => (!empty($baths->filtervalue)) ? $baths->filtervalue : '',
                    'Land Size' => (!empty($ad_info->landsize)) ? $ad_info->landsize : '',
                    'Property Size' => (!empty($ad_info->propertysize)) ? $ad_info->propertysize : '',
                );
                $generalinfo = array(
                    'Address' => (!empty($ad_info->address)) ? $ad_info->address : '',
                    //'Condition' => (!empty($itemcondition->filtervalue)) ? $itemcondition->filtervalue : '',
                    'Type' => (!empty($itemtype->filtervalue)) ? $itemtype->filtervalue : '',
                );

                //start value added features
                $optionsids = unserialize($ad_info->features);
                if (!empty($optionsids)) {
                    foreach ($optionsids as $ids) {
                        $dispal = '';
                        $dispal = Propertyoption::where('id', $ids)->select('display_name', 'option_name', 'id')->first();
                        if (!empty($dispal->display_name)) {
                            $features[] = $dispal->display_name;
                        }
                    }
                }
                //end value added features
                break;
            case 'hote':
                $ad_info = Alladsmaster::GetHotelAdsbyid($ad_id);
                $related = Alladsmaster::relatedAdsBysubCatid($catcode->sub_category_id, $ad_id, $categorycode); //Related ads
                $relatedads = (!empty($related)) ? $related : $relatedads;
                if (!empty($ad_info)) {
                    $ad_info->created_at = Carbon::parse($ad_info->created_at);
                    //contact info
                    $contacts = (!empty($ad_info->contact)) ? unserialize($ad_info->contact) : '';
                    //load beds
                    if (!empty($ad_info->bedsize)) {
                        $bedsize = Categoryfiltervalue::where('id', $ad_info->bedsize)->select('filtervalue')->first();
                    }
                    if (!empty($ad_info->beds)) {
                        $beds = Categoryfiltervalue::where('id', $ad_info->beds)->select('filtervalue')->first();
                    }
                    if (!empty($ad_info->bathroomtypes)) {
                        $bathroomtypes = Categoryfiltervalue::where('id', $ad_info->bathroomtypes)->select('filtervalue')->first();
                    }
                    if (!empty($ad_info->bathrooms)) {
                        $bathrooms = Categoryfiltervalue::where('id', $ad_info->bathrooms)->select('filtervalue')->first();
                    }
                    if (!empty($ad_info->numberofrooms)) {
                        $numberofrooms = Categoryfiltervalue::where('id', $ad_info->numberofrooms)->select('filtervalue')->first();
                    }
                    if (!empty($ad_info->type)) {
                        $type = Categoryfiltervalue::where('id', $ad_info->type)->select('filtervalue')->first();
                    }
                    if (!empty($ad_info->occupancy)) {
                        $occupancy = Categoryfiltervalue::where('id', $ad_info->occupancy)->select('filtervalue')->first();
                    }
                    if (!empty($ad_info->size)) {
                        $size = Categoryfiltervalue::where('id', $ad_info->size)->select('filtervalue')->first();
                    }

                }

                $icons = array(
                    'Number of rooms' => (!empty($numberofrooms->filtervalue)) ? $numberofrooms->filtervalue : '',
                    'Type' => (!empty($type->filtervalue)) ? $type->filtervalue : '',
                    'Occupancy' => (!empty($occupancy->filtervalue)) ? $occupancy->filtervalue : '',
                    'Size' => (!empty($size->filtervalue)) ? $size->filtervalue : '',
                );
                $generalinfo = array(
                    'Bed Size' => (!empty($bedsize->filtervalue)) ? $bedsize->filtervalue : '',
                    'Beds' => (!empty($beds->filtervalue)) ? $beds->filtervalue . ' Bed(s)' : '',
                    'Bathrooms' => (!empty($bathrooms->filtervalue)) ? $bathrooms->filtervalue . ' Bathroom(s)' : '',
                    'Bathroom Type' => (!empty($bathroomtypes->filtervalue)) ? $bathroomtypes->filtervalue : '',
                    'Address' => (!empty($ad_info->address)) ? $ad_info->address : '',
                    'Min Check In Time' => (!empty($ad_info->min_check_in_time)) ? $ad_info->min_check_in_time : '',
                    'Max Check Out Time' => (!empty($ad_info->max_check_out_time)) ? $ad_info->max_check_out_time : '',
                    'Price as Meal Type' => (!empty($ad_info->price_meal)) ? unserialize($ad_info->price_meal) : ''
                );

                //start value added features
                $optionsids = unserialize($ad_info->features);
                if (!empty($optionsids)) {
                    foreach ($optionsids as $ids) {
                        $dispal = '';
                        $dispal = Propertyoption::where('id', $ids)->select('display_name', 'option_name', 'id')->first();
                        if (!empty($dispal->display_name)) {
                            $features[] = $dispal->display_name;
                        }
                    }
                }

                //end value added features
                break;
            case 'job':
                $ad_info = Alladsmaster::GetJobsbyid($ad_id);
                $alljobdata = Alljobad::where('alladsmaster_id', $ad_id)->first();

                if (!$alljobdata) {
                    abort(404, 'No Advertiesment Found!');
                }
                if (!empty($ad_info)) {
                    $ad_info->created_at = Carbon::parse($ad_info->created_at);
                    //contact info
                    $contacts = (!empty($ad_info->contact)) ? unserialize($ad_info->contact) : '';
                    if (!empty($ad_info->itemtype)) {
                        $itemtype = Categoryfiltervalue::where('id', $ad_info->itemtype)->select('filtervalue')->first();
                        $ad_info->itemtype = $itemtype->filtervalue;
                    }
                    if (!empty($alljobdata->industry)) {
                        $industry = Categoryfiltervalue::where('id', $alljobdata->industry)->select('filtervalue')->first();
                        $ad_info->industry = $industry->filtervalue;
                    }
                    if (!empty($alljobdata->closingdate)) {
                        $ad_info->closingdate = $alljobdata->closingdate;
                    }
                    if (!empty($alljobdata->salary)) {
                        $salary = Categoryfiltervalue::where('id', $alljobdata->salary)->select('filtervalue')->first();
                        $ad_info->salary = $salary->filtervalue;
                    }
                    $ad_info->company = $alljobdata->company;
                }
                $related = Alladsmaster::relatedAdsJobs($catcode->sub_category_id, $ad_id, $categorycode); //Related ads
                $relatedads = (!empty($related)) ? $related : $relatedads;

                $icons = array();
                $generalinfo = array();
                break;
            default:
                //this is master table- update later for better profomance
                $ad_info = Alladsmaster::GetAllAdMasterSingleFrontend($ad_id);
                $ad_info->created_at = Carbon::parse($ad_info->created_at);
                $related = Alladsmaster::relatedAdsBysubCatid($catcode->sub_category_id, $ad_id); //Related ads
                $relatedads = (!empty($related)) ? $related : $relatedads;
                //contact info
                $contacts = (!empty($ad_info->contact)) ? unserialize($ad_info->contact) : '';
                if (!empty($ad_info->itemtype)) {
                    $itemtype = Categoryfiltervalue::where('id', $ad_info->itemtype)->select('filtervalue')->first();
                }
                if (!empty($ad_info->itemcondition)) {
                    $itemcondition = Categoryfiltervalue::where('id', $ad_info->itemcondition)->select('filtervalue')->first();
                }
                $icons = array(
                    'Condition' => (!empty($itemcondition->filtervalue)) ? $itemcondition->filtervalue : '',
                );
                $generalinfo = array(
                    //'Condition' => (!empty($itemcondition->filtervalue)) ? $itemcondition->filtervalue : '',
                    'Type' => (!empty($itemtype->filtervalue)) ? $itemtype->filtervalue : '',
                );
        }

        $relatedByPremium = false;
        if (!empty($memeberdetails->member_type)) {
            if ($memeberdetails->member_type == 'premium') {
                $related = Alladsmaster::relatedAdsBysubCatid(null, $ad_id, null, null, $memeberdetails->id); //Related ads
                $relatedads = (!empty($related)) ? $related : $relatedads;
                $relatedByPremium = (!empty($related)) ? true : false;
            }
        }

        //START get featured image to top

        $feaImg = $ad_info->featuredimage;
        if (!empty($feaImg)) {
            $featured = $images->first(function ($value, $key) use ($feaImg) {
                if ($value->imagename == $feaImg) {
                    return $value;
                }
            });
            $images = $images->map(function ($value) use ($feaImg, $images) {
                if ($value->imagename != $feaImg) {
                    return $value;
                }
            })->reject(function ($value) {
                return empty($value);
            });
            $images->prepend($featured);
        }
        //END get featured image to top

        //get extra filters from alladsfilter table - this can save multiple filters from categoryfiltes table with relevent value
        $allFilters = Alladsfilter::where('alladsmaster_id', $ad_id)->select('filtername', 'categoryfiltervalue_id')->get();
        if (!empty($allFilters)) {
            foreach ($allFilters as $key => $allFilter) {
                $data = Categoryfiltervalue::where('id', $allFilter->categoryfiltervalue_id)->select('id', 'filtervalue')->first();
                $generalinfo[ucfirst($allFilter->filtername)] = $data->filtervalue;
            }
        }
        $filtered = array_filter($generalinfo);
        if ($categorycode == 'job') {
            $layout = 'default.details_job';
        }
//        dd($relatedads);
        return view($layout . $view)
//        return view('default.details_job' . $view)
            ->with('ad_details', $ad_info)
            ->with('generalinfo', $filtered)
            ->with('icons', $icons)
            ->with('memeberdetails', $memeberdetails)
            ->with('images', $images)
            ->with('features', $features)
            ->with('favorites', $favorites)
            ->with('relatedads', $relatedads)
            ->with('query_data', $query_data)
            ->with('currentURL', $currentURL)
            ->with('relatedByPremium', $relatedByPremium)
            ->with('contacts', $contacts);
    }

//apply job function
    public function applyJob(Request $request)
    {
        $currentURL = URL::current();
        $slug = $request->slug;
        $favorites = array();
        $adById = Alladsmaster::where('slug', $slug)->select('id')->first();
        if (!$adById) {
            abort(404);
        }
        $ad_id = $adById->id;

        //get only common data
        $catcode = Alladsmaster::where('id', $ad_id)->with(['category' => function ($qq) {
            $qq->select('id', 'category_code');
        }])->select('id', 'ad_referance', 'member_id', 'category_id', 'sub_category_id', 'status')->first();
        if ($catcode->status == 'pending') {
            return redirect('reviewsuccess?ref=' . $catcode->ad_referance);
        }
        // memeber detaisl
        $memeberdetails = Member::GetMemberDetailsByIdforJobApply($catcode->member_id);
        $categorycode = $catcode->category->category_code;
        if ($categorycode != 'job') {
            return redirect('/ad/' . $slug);
        }
//        --------------------------
        $ad_info = Alladsmaster::GetJobsbyid($ad_id);
        $alljobdata = Alljobad::where('alladsmaster_id', $ad_id)->first();

        if (!empty($ad_info)) {
            $ad_info->created_at = Carbon::parse($ad_info->created_at);
            //contact info
            $contacts = (!empty($ad_info->contact)) ? unserialize($ad_info->contact) : '';
            if (!empty($ad_info->itemtype)) {
                $itemtype = Categoryfiltervalue::where('id', $ad_info->itemtype)->select('filtervalue')->first();
                $ad_info->itemtype = $itemtype->filtervalue;
            }
            if (!empty($alljobdata->industry)) {
                $industry = Categoryfiltervalue::where('id', $alljobdata->industry)->select('filtervalue')->first();
                $ad_info->industry = $industry->filtervalue;
            }
            if (!empty($alljobdata->closingdate)) {
                $ad_info->closingdate = $alljobdata->closingdate;
            }
            if (!empty($alljobdata->salary)) {
                $salary = Categoryfiltervalue::where('id', $alljobdata->salary)->select('filtervalue')->first();
                $ad_info->salary = $salary->filtervalue;
            }
            $ad_info->company = $alljobdata->company;
        }
        //        --------------------------
        return view('default.apply-job')
            ->with('ad_details', $ad_info)
            ->with('memeberdetails', $memeberdetails);
    }

    public function applyNow(Request $request)
    {
        try {
            $data = $request->all();
            $file = $request->file('apply_job_file');
//            dd(Member::first());
//            Mail::to(Member::first())->send(new ApplyNow($data, $file));
            Mail::to($request->reciveemail)->send(new ApplyNow($data, $file));
//            return "success";
            return response()->json(['success' => true, 'message' => "Successfully applied for " . $request->apply_for, 'class' => 'alert-success']);

        } catch (Exception $ex) {
            return response()->json(['success' => false, 'message' => "Apply job not success", 'class' => 'alert-danger']);
        }
    }

//END apply job

    public function allads(Request $request)
    {
//        dd(\Request::getQueryString());
        $REDIRECT_QUERY_STRING = isset($_SERVER['REDIRECT_QUERY_STRING']) && !empty($_SERVER['REDIRECT_QUERY_STRING']) ? $_SERVER['REDIRECT_QUERY_STRING'] : '';
        $queryString = (!empty(\Request::getQueryString())) ? '?' . \Request::getQueryString() : '';
        $allgetdata = Input::all();  //  get  all   get  params
        $sort = !empty($allgetdata['sort']) ? $allgetdata['sort'] : ''; //  sort  value
        $query = !empty($allgetdata['query']) ? $allgetdata['query'] : ''; //  query  value

        $citylist = City::select('id', 'district_id', 'city_name', 'slug')->get(); // for popup search

        $showCity = 1;
        $citywithdis = District::select('id', 'province_id', 'district_name', 'slug', 'district_code')->withCount(['allads' => function ($q) {
            $q->where('status', 'confirm');
        }])->orderBy('allads_count', 'desc')->with(['cities' => function ($query) {
            $query->select('id', 'district_id', 'city_name', 'slug', 'city_code');
        }])->get();
        $alldidcity = $citywithdis; // for pop up

        $categories = Category::select('id', 'sequence', 'category_name', 'category_code', 'slug', 'icon', 'status')->withCount(['allAds' => function ($q) {
            $q->where('status', 'confirm');
        }])->with(['subcategories' => function ($q) {
            $q->select('id', 'category_id', 'sub_category_code', 'sub_category_name', 'slug', 'icon', 'widget_id', 'adtype', 'status');
        }])->get();

        $querydata = array(
            'sort' => $sort,
            'query' => $query
        );
        if (!empty($query)) {
            $addlist = Alladsmaster::getFristPageAllAds($querydata);
        } else {
            $addlist = Alladsmaster::getFristPageAllAdsNoSearch();
        }


//        $addlist = Alladsmaster::with(['category' => function ($q) {
//            $q->select('category_name');
//        }, 'subcategory' => function ($q1) {
//            $q1->select('sub_category_name');
//        }])->select('id as adid', 'adtitle', 'featuredimage', 'created_at', 'updated_at', 'published_at', 'price', 'slug', 'adtitle')->paginate(20);

        /***********************************************************************************************
         * START PIN-TO-TOP Ads
         ***/
        $pin_to_top_Ads = Boostpinintotop::getBoostPintoTop($querydata);
        $addlist = Boostpinintotop::removePinAdsFromCurrentPage($addlist, $pin_to_top_Ads);
        $boostAdCount = (!empty($pin_to_top_Ads)) ? count($pin_to_top_Ads) : 0;
        /*
         * END fire the hit counter increment event for all pinned ads in current page
         * END PIN-TO-TOP Ads
         *************************************************************************************************/
        $seo = 'SaleMe.lk - Used cars in sri lanka | used vehicles in sri lanka | mobile phones in sri lanka | electronics in sri lanka | lands in sri lanka | properties in sri lanka.';
        return view('default.allads')
            ->with('citylist', $citylist)
            ->with('alldidcity', $alldidcity)
            ->with('citywithdis', $citywithdis)
            ->with('showCity', $showCity)
            ->with('allcategory', $categories)
            ->with('addlist', $addlist)
            ->with('allBoostAds', $pin_to_top_Ads)
            ->with('boostAdCount', $boostAdCount)
            ->with('querydata', $querydata)
            ->with('seo', $seo)
            ->with('queryString', $queryString)
            ->with('REDIRECT_QUERY_STRING', $REDIRECT_QUERY_STRING);
    }

    /*
     * ads  serch by loaction
     */
    public function adslocation(Request $request)
    {
        $REDIRECT_QUERY_STRING = isset($_SERVER['REDIRECT_QUERY_STRING']) && !empty($_SERVER['REDIRECT_QUERY_STRING']) ? $_SERVER['REDIRECT_QUERY_STRING'] : '';
        $queryString = (!empty(\Request::getQueryString())) ? '?' . \Request::getQueryString() : '';

        $allgetdata = Input::all();  //  get  all   get  params 
        $sort = !empty($allgetdata['sort']) ? $allgetdata['sort'] : ''; //  sort  value
        $query = !empty($allgetdata['query']) ? $allgetdata['query'] : ''; //  query  value
        $location = !empty($request->location) ? $request->location : '';
        //  get District slug name or city slug  separatelly
        $locations_slug = explode("all_of_", $location);
        if (!empty($locations_slug[1])) {
            $district = District::where('slug', $location)->select('id', 'district_name', 'slug')->first();
        } else {
            $city = City::where('slug', $location)->select('id', 'city_name', 'slug')->first();
        }
        $city_slug = !empty($city->slug) ? $city->slug : '';
        $district_slug = !empty($district->slug) ? $district->slug : '';
        $querydata = array(
            'cityid' => !empty($city->id) ? $city->id : '',
            'districtid' => !empty($district->id) ? $district->id : '',
            'sort' => $sort,
            'query' => $query
        );


        $alldidcity = District::select('id', 'province_id', 'district_name', 'slug', 'district_code')->withCount(['allads' => function ($q) {
            $q->where('status', 'confirm');
        }])->orderBy('allads_count', 'desc')->with(['cities' => function ($query) {
            $query->select('id', 'district_id', 'city_name', 'slug', 'city_code');
        }])->get(); //  all  District and cities in popup

        $citylist = City::select('id', 'district_id', 'city_name', 'slug')->get(); // for popup search
        //$alldidcity = District::with('cities')->get()->toArray(); //  all  District and cities in popup
        $searchDistrict = (!empty($district->id)) ? $district->id : '';
        $searchCity = (!empty($city->id)) ? $city->id : '';
        $allcatandsubcatlist = Category::with('subcategories')->get()->toArray(); // category  list
        /***************************************************************************************************************************
         * START Location filter
         ***************************************************************************************************************************/
        $citywithdis = array();
        $district_id = City::where('slug', $city_slug)->select('district_id')->first();
        $showCity = 1;
        $citywithdisData = District::withCount(['allads' => function ($q) {
            $q->where('status', 'confirm');
        }])->orderBy('allads_count', 'desc')
            ->with(['cities' => function ($q1) {
                $q1->withCount(['alladscity' => function ($q3) {
                    $q3->where('status', 'confirm');
                }])->orderBy('alladscity_count', 'desc');
            }]);
        if (!empty($district_slug)) {
            $citywithdisData->where('slug', '=', $district_slug);
        }
        //filter through location
        if (!empty($district_id->district_id)) {
            $citywithdisData->where('id', $district_id->district_id);
        }
        $citywithdis = $citywithdisData->get();
        (!empty($city_slug)) ? $showCity = 0 : (!empty($district_slug)) ? $showCity = 0 : 1; // show and hide the ads count
        /*****************************************************************************************************************************
         * END Location filter
         * START Category filter
         *****************************************************************************************************************************/
        $categories = Category::withCount(['allAds' => function ($q) use ($querydata) {
            $q->where('status', 'confirm');
            if (!empty($querydata['cityid'])) {
                $q->where('city_id', $querydata['cityid']);
            }
            if (!empty($querydata['districtid'])) {
                $q->where('district_id', $querydata['districtid']);
            }
        }])->with(['subcategories'])->get();
        $addlist = Alladsmaster::SearchPublishedAds($querydata);
        //END Category filter -------------------------------------------------------------------------------------------------------
        /*****************************************************************************************************************************
         * START PIN-TO-TOP Ads
         ***/
        $pin_to_top_Ads = Boostpinintotop::getBoostPintoTop($querydata);
        $addlist = Boostpinintotop::removePinAdsFromCurrentPage($addlist, $pin_to_top_Ads);
        $boostAdCount = (!empty($pin_to_top_Ads)) ? count($pin_to_top_Ads) : 0;
        /*
         * END fire the hit counter increment event for all pinned ads in current page
         * END PIN-TO-TOP Ads
         *****************************************************************************************************************************/

        return view('default.allads')
            ->with('citylist', $citylist)
            ->with('allcategory', $categories)
            ->with('allBoostAds', $pin_to_top_Ads)
            ->with('boostAdCount', $boostAdCount)
            ->with('alldidcity', $alldidcity)
            ->with('allcatandsubcatlist', $allcatandsubcatlist)
            ->with('citywithdis', $citywithdis)
            ->with('showCity', $showCity)
            ->with('location_slug', $location)
            ->with('city_slug', $city_slug)
            ->with('district_slug', $district_slug)
            ->with('addlist', $addlist)
            ->with('querydata', $querydata)
            ->with('queryString', $queryString)
            ->with('REDIRECT_QUERY_STRING', $REDIRECT_QUERY_STRING);
    }

    /*
     * by location and category 
     */

    public function alladssearch(Request $request)
    {
//        dd('allads search');
        $fullURL = $request->fullUrl();
        $REDIRECT_QUERY_STRING = (isset($_SERVER['REDIRECT_QUERY_STRING']) && !empty($_SERVER['REDIRECT_QUERY_STRING'])) ? $_SERVER['REDIRECT_QUERY_STRING'] : '';

        $queryString = (!empty(\Request::getQueryString())) ? '?' . \Request::getQueryString() : '';
        $getParamQuery = !empty($request->query('query')) ? $request->query('query') : '';
        $allgetdata = Input::all();  //  get  all   get  params
        //dd($allgetdata);
        //dd($request->getQueryString());

        $location = !empty($request->location) ? $request->location : '';
        $query = !empty($allgetdata['query']) ? $allgetdata['query'] : $getParamQuery; //  query  value

        $sort = !empty($allgetdata['sort']) ? $allgetdata['sort'] : ''; //  sort  value
        $adtype = !empty($allgetdata['adtype']) ? $allgetdata['adtype'] : ''; //  adtype  value
        $comn_data = !empty($allgetdata['comn']) ? $allgetdata['comn'] : ''; //  vehicel data

        $vehi_data = !empty($allgetdata['vehi']) ? $allgetdata['vehi'] : ''; //  vehicel data 
        $elec_data = !empty($allgetdata['elec']) ? $allgetdata['elec'] : ''; //  electic data
        $prop_data = !empty($allgetdata['prop']) ? $allgetdata['prop'] : ''; //  property data
        $from_price = !empty($allgetdata['f_price']) ? $allgetdata['f_price'] : ''; //  price  from
        $to_price = !empty($allgetdata['t_price']) ? $allgetdata['t_price'] : ''; //  price  to
        $from_year = !empty($allgetdata['f_year']) ? $allgetdata['f_year'] : ''; //  year  from
        $to_year = !empty($allgetdata['t_year']) ? $allgetdata['t_year'] : ''; // to  year
        // get category or sub categiry which selected
        $subandcategory = !empty($request->scat) ? $request->scat : '';
        //  get District slug name or city slug  separatelly
        $locations_slug = explode("all_of_", $location);
        if (!empty($locations_slug[1])) {
            $district = District::where('slug', $location)->select('id', 'district_name', 'slug')->first();
        } else {
            $city = City::where('slug', $location)->select('id', 'city_name', 'slug')->first();
        }
        $city_slug = !empty($city->slug) ? $city->slug : '';
        $district_slug = !empty($district->slug) ? $district->slug : '';
        $cityname = '';
        $districname = '';
        $subcategorycode = '';
        $vehiclebrandlist = '';
        $citywithdis = array();
        $filters = array();
        $boostAdCount = '';
        $addtypelist = array();
        $category = Category::where('slug', $subandcategory)->select('id', 'category_name', 'category_code')->first();
        $categorycode = !empty($category->category_code) ? $category->category_code : '';
        if (empty($category)) {
            $subcategory = Subcategory::where('slug', $subandcategory)->select('id', 'sub_category_name', 'sub_category_code', 'category_id', 'adtype')->first();
            $category_code = Category::where('id', $subcategory->category_id)->select('category_code')->first();
            $categorycode = $category_code->category_code;

            $subcategorycode = !empty($subcategory->sub_category_code) ? $subcategory->sub_category_code : '';
            // START - load filters and relevnt filter values associating with the selected sub-category
            $subCategorys = Subcategory::where('sub_category_code', $subcategory->sub_category_code)->with('subcategoryfilters', 'brands')->first();
            $filtersWithValues = $subCategorys->subcategoryfilters->load('categoryfiltervalues');
            $filters = array();

            foreach ($filtersWithValues as $filter) {
                $filtername = ucfirst(str_replace('-', ' ', $filter->filterref));
                $filterValues = (!empty($filter->categoryfiltervalues)) ? $filter->categoryfiltervalues : '';
                foreach ($filterValues as $filterValue) {
                    $filters[$filtername][$filterValue->id] = $filterValue->filtervalue;
                }
            }

            $vehiclebrandlist = $subCategorys->brands; //all brands assosiate with subcategory - by pivot table
            // 
            //get add type
//            dd($subcategory->adtype);
//            dd(unserialize($subcategory->adtype));
            if (!empty(unserialize($subcategory->adtype))) {
                $adtypes = unserialize($subcategory->adtype);
                foreach ($adtypes as $addtypeid) {
                    $addtypelist[] = Advertisementtype::where('id', $addtypeid)->first()->toArray();
                }
            }
        }


        if ($location == 'new-zealand') {  //  this load when index page category
            $locationId = '';
            $cityname = 'All New Zealand';

            $scategoryname = !empty($subcategory->sub_category_name) ? $subcategory->sub_category_name : '';
            $categoryname = !empty($category->category_name) ? $category->category_name : '';
        } elseif ($location == 'new-zealand') { //  this load when index page sub category
            $locationId = '';
            $cityname = 'All New Zealand';
            $scategoryname = !empty($subcategory->sub_category_name) ? $subcategory->sub_category_name : '';
            $categoryname = !empty($category->category_name) ? $category->category_name : '';
        } else {
            $cityname = !empty($city->city_name) ? $city->city_name : '';
            $scategoryname = !empty($subcategory->sub_category_name) ? $subcategory->sub_category_name : '';
            $categoryname = !empty($category->category_name) ? $category->category_name : '';
        }
        $querydata = array(
            'categorycode' => !empty($categorycode) ? $categorycode : '',
            'cityid' => !empty($city->id) ? $city->id : '',
            'districtid' => !empty($district->id) ? $district->id : '',
            'categoryid' => !empty($category->id) ? $category->id : '',
            'subcategoryid' => !empty($subcategory->id) ? $subcategory->id : '',
            'sort' => $sort,
            'addtype' => $adtype,
            'comn_data' => $comn_data,
            'elec_data' => $elec_data,
            'prop_data' => $prop_data,
            'vehi_data' => $vehi_data,
            'from_price' => $from_price,
            'to_price' => $to_price,
            'from_year' => $from_year,
            'to_year' => $to_year,
            'query' => $query
        );

        $citylist = City::select('id', 'district_id', 'city_name', 'slug')->get(); // for popup search
        //  categorywise  count 
        $categorylist = Category::Catrgorylsit();   //  category list 
        $array1 = Common::object_to_array($categorylist);
        $catcountlist = Alladsmaster::categoryCountByid();   //  categorywise  count 
        $array2 = Common::object_to_array($catcountlist);
        $catlist = array_replace_recursive($array1, $array2); //  replce  $array2  value  to $array 1

        $alldidcity = District::select('id', 'province_id', 'district_name', 'slug', 'district_code')->withCount(['allads' => function ($q) {
            $q->where('status', 'confirm');
        }])->orderBy('allads_count', 'desc')->with(['cities' => function ($query) {
            $query->select('id', 'district_id', 'city_name', 'slug', 'city_code');
        }])->get(); //  all  District and cities in popup

        $allcatandsubcatlist = Category::with('subcategories')->get()->toArray(); // category  list
        $addlist = Alladsmaster::SearchPublishedAds($querydata);

        /***************************************************************************************************************************
         * START Location filter
         ***************************************************************************************************************************/
        $citywithdis = array();
        $district_id = City::where('slug', $city_slug)->select('district_id')->first();
        $showCity = 1;
        $citywithdisData = District::withCount(['allads' => function ($q) use ($querydata) {
            $q->where('status', 'confirm');
            //filter for districts by category & sub category change
            if (!empty($querydata['categoryid'])) {
                $q->where('category_id', $querydata['categoryid']);
            }
            if (!empty($querydata['subcategoryid'])) {
                $q->where('sub_category_id', $querydata['subcategoryid']);
            }
        }])->orderBy('allads_count', 'desc')
            ->with(['cities' => function ($q1) use ($querydata) {
                $q1->withCount(['alladscity' => function ($q3) use ($querydata) {
                    $q3->where('status', 'confirm');
                    //filter for cities by category & sub category change
                    if (!empty($querydata['categoryid'])) {
                        $q3->where('category_id', $querydata['categoryid']);
                    }
                    if (!empty($querydata['subcategoryid'])) {
                        $q3->where('sub_category_id', $querydata['subcategoryid']);
                    }
                }])->orderBy('alladscity_count', 'desc');
            }]);
        if (!empty($district_slug)) {
            $citywithdisData->where('slug', '=', $district_slug);
        }
        //filter through location
        if (!empty($district_id->district_id)) {
            $citywithdisData->where('id', $district_id->district_id);
        }
        $citywithdis = $citywithdisData->get();
        (!empty($city_slug)) ? $showCity = 0 : (!empty($district_slug)) ? $showCity = 0 : 1; // show and hide the ads count


        /* END Location filter----------------------------------------------------------------------------------------------
         * START CATEGORY filters by selectrd category or sub category
         */
        $searchDistrict = $searchDistrict = (!empty($district->id)) ? $district->id : '';
        $categoriesData = Category::withCount(['allAds' => function ($q) use ($querydata) {
            $q->where('status', 'confirm');
            //location
            if (!empty($querydata['cityid'])) {
                $q->where('city_id', $querydata['cityid']);
            }
            if (!empty($querydata['districtid'])) {
                $q->where('district_id', $querydata['districtid']);
            }
        }]);
        $categoriesData->with(['subcategories' => function ($query) use ($subcategorycode, $querydata) {
            $query->withCount(['allads' => function ($q) use ($querydata) {
                $q->where('status', 'confirm');
                //location
                if (!empty($querydata['cityid'])) {
                    $q->where('city_id', $querydata['cityid']);
                }
                if (!empty($querydata['districtid'])) {
                    $q->where('district_id', $querydata['districtid']);
                }
            }]);
        }]);
        $categories = $categoriesData->get();
        //END Category filter ------------------------------------------------------------------------------------------
        /***********************************************************************************************
         * START PIN-TO-TOP Ads
         ***/
        $pin_to_top_Ads = Boostpinintotop::getBoostPintoTop($querydata);
        $addlist = Boostpinintotop::removePinAdsFromCurrentPage($addlist, $pin_to_top_Ads);
        $boostAdCount = (!empty($pin_to_top_Ads)) ? count($pin_to_top_Ads) : 0;
        /*
         * END fire the hit counter increment event for all pinned ads in current page
         * END PIN-TO-TOP Ads
         *************************************************************************************************/
        $seo = self::seoKeyWordsForTitle($categorycode, $subcategorycode, $scategoryname, $adtype, $location);

        switch ($categorycode) {
            case 'veh':
                $info = self::vehicle_Related_Data();
                return view('default.allads')
                    ->with('citylist', $citylist)
                    ->with('addtypelist', $addtypelist)
                    ->with('allcatandsubcatlist', $allcatandsubcatlist)
                    ->with('allcategory', $categories)
                    ->with('allBoostAds', $pin_to_top_Ads)
                    ->with('boostAdCount', $boostAdCount)
                    ->with('alldidcity', $alldidcity)
                    ->with('citywithdis', $citywithdis)
                    ->with('showCity', $showCity)
                    ->with('location_slug', $location)
                    ->with('category_slug', $subandcategory)
                    ->with('scategory_slug', $subandcategory)
                    ->with('cityname', $cityname)
                    ->with('category', $categoryname)
                    ->with('scategory', $scategoryname)
                    ->with('category_code', $categorycode)
                    ->with('subcategory_code', $subcategorycode)
                    ->with('addlist', $addlist)
                    ->with('querydata', $querydata)
                    ->with('vehiclebrandlist', $vehiclebrandlist)
                    ->with('vehicletypelist', $info['vehicletypelist'])
                    ->with('vehicleconditionlist', $info['vehicleconditionlist'])
                    ->with('vehicletransmissionslist', $info['vehicletransmissionslist'])
                    ->with('vehiclefuellist', $info['vehiclefuellist'])
                    ->with('vehicleoptions', $info['vehicleoptions'])
                    ->with('vehiclebodytype', $info['vehiclebodytype'])
                    ->with('categories', $categories)
                    ->with('filters', $filters)
                    ->with('seo', $seo)
                    ->with('queryString', $queryString)
                    ->with('REDIRECT_QUERY_STRING', $REDIRECT_QUERY_STRING);
                break;
            case 'ele':
                $features = DB::table('electronicoptions')->select('display_name', 'id')->get()->toArray();
                return view('default.allads')
                    ->with('citylist', $citylist)
                    ->with('addtypelist', $addtypelist)
                    ->with('allcatandsubcatlist', $allcatandsubcatlist)
                    ->with('allcategory', $categories)
                    ->with('allBoostAds', $pin_to_top_Ads)
                    ->with('boostAdCount', $boostAdCount)
                    ->with('alldidcity', $alldidcity)
                    ->with('citywithdis', $citywithdis)
                    ->with('showCity', $showCity)
                    ->with('location_slug', $location)
                    ->with('category_slug', $subandcategory)
                    ->with('scategory_slug', $subandcategory)
                    ->with('cityname', $cityname)
                    ->with('category', $categoryname)
                    ->with('scategory', $scategoryname)
                    ->with('category_code', $categorycode)
                    ->with('subcategory_code', $subcategorycode)
                    ->with('addlist', $addlist)
                    ->with('querydata', $querydata)
                    ->with('elecclebrandlist', $vehiclebrandlist)
                    ->with('features', $features)
                    ->with('filters', $filters)
                    ->with('seo', $seo)
                    ->with('queryString', $queryString)
                    ->with('REDIRECT_QUERY_STRING', $REDIRECT_QUERY_STRING);
                break;
            case 'pro':
                $features = DB::table('propertyoptions')->select('display_name', 'id')->get()->toArray();
                return view('default.allads')
                    ->with('citylist', $citylist)
                    ->with('addtypelist', $addtypelist)
                    ->with('allcatandsubcatlist', $allcatandsubcatlist)
                    ->with('allcategory', $categories)
                    ->with('allBoostAds', $pin_to_top_Ads)
                    ->with('boostAdCount', $boostAdCount)
                    ->with('alldidcity', $alldidcity)
                    ->with('citywithdis', $citywithdis)
                    ->with('showCity', $showCity)
                    ->with('location_slug', $location)
                    ->with('category_slug', $subandcategory)
                    ->with('cityname', $cityname)
                    ->with('category', $categoryname)
                    ->with('scategory', $scategoryname)
                    ->with('scategory_slug', $subandcategory)
                    ->with('category_code', $categorycode)
                    ->with('subcategory_code', $subcategorycode)
                    ->with('features', $features)
                    ->with('filters', $filters)
                    ->with('addlist', $addlist)
                    ->with('querydata', $querydata)
                    ->with('seo', $seo)
                    ->with('queryString', $queryString)
                    ->with('REDIRECT_QUERY_STRING', $REDIRECT_QUERY_STRING);
                break;
            default:
                return view('default.allads')
                    ->with('citylist', $citylist)
                    ->with('addtypelist', $addtypelist)
                    ->with('allcatandsubcatlist', $allcatandsubcatlist)
                    ->with('catlist', $catlist)
                    ->with('allcategory', $categories)
                    ->with('allBoostAds', $pin_to_top_Ads)
                    ->with('boostAdCount', $boostAdCount)
                    ->with('alldidcity', $alldidcity)
                    ->with('citywithdis', $citywithdis)
                    ->with('showCity', $showCity)
                    ->with('location_slug', $location)
                    ->with('category_slug', $subandcategory)
                    ->with('scategory_slug', $subandcategory)
                    ->with('cityname', $cityname)
                    ->with('category', $categoryname)
                    ->with('scategory', $scategoryname)
                    ->with('category_code', $categorycode)
                    ->with('subcategory_code', $subcategorycode)
                    ->with('addlist', $addlist)
                    ->with('querydata', $querydata)
                    ->with('filters', $filters)
                    ->with('seo', $seo)
                    ->with('queryString', $queryString)
                    ->with('REDIRECT_QUERY_STRING', $REDIRECT_QUERY_STRING);
        }
    }

    private static function vehicle_Related_Data()
    {
        $vehicletypelist = DB::table('vehicle_types')->get()->toArray();
        $vehicleconditionlist = DB::table('vehicle_condition')->get()->toArray();
        $vehiclefuellist = DB::table('vehicle_fuels')->get()->toArray();
        $vehicletransmissionslist = DB::table('vehicle_transmissions')->get()->toArray();
        $vehicleoptions = DB::table('vehicle_options')->select('display_name', 'id')->get()->toArray();
        $vehiclebodytype = DB::table('vehicle_types')->select('type_name', 'image_name', 'id')->get()->toArray();
        $arr = array(
            'vehicletypelist' => $vehicletypelist,
            'vehicleconditionlist' => $vehicleconditionlist,
            'vehicletransmissionslist' => $vehicletransmissionslist,
            'vehiclefuellist' => $vehiclefuellist,
            'vehicleoptions' => $vehicleoptions,
            'vehiclebodytype' => $vehiclebodytype,
        );
        return $arr;
    }

    protected function seoKeyWordsForTitle($category_code, $subCode, $subName, $adtype, $location)
    {
        $key = '';
        $adtype = (!empty($adtype)) ? 'for ' . $adtype : 'for sell';
        if (!empty($category_code)) {
            $categoryRef = $category_code;
            switch ($categoryRef) {
                case 'veh':
                    $key = 'New and Used Cars and Vehicles for sale and rent';
                    if (!empty($subCode)) {
                        ($subCode == 'auto-ser') ? $key = $subName . ' ' : $key = 'New and Used ' . $subName . ' ' . $adtype . '';
                    }
                    break;
                case 'ele':
                    $key = 'New and Used Electronic equipments for sale and rent';
                    if (!empty($subCode)) {
                        $key = 'New and Used ' . $subName . ' ' . $adtype;
                    }
                    break;
                case 'pro':
                    $key = 'House, Commercial Properties, Apartments for sale and rent, lands, real estate';
                    if (!empty($subCode)) {
                        $key = $subName . ' ' . $adtype;
                        if ($subCode == 'room' OR $subCode == 'holi-rent') {
                            $key = $subName . ' for rent';
                        }
                        if ($subCode == 'com-prop') {
                            $key = $subName . ' for sell and rent';
                        }
                        if ($subCode == 'land') {
                            $key = $subName . ' for sell';
                        }
                    }
                    break;
                case 'fas':
                    $key = 'Fashion, Health and Beauty items and services for sale';
                    if (!empty($subCode)) {
                        $key = $subName . ' ' . $adtype;
                    }
                    break;
                case 'hob':
                    $key = 'Hobby, Sports and Kids items for sale';
                    if (!empty($subCode)) {
                        $key = $subName . ' ' . $adtype;
                        if ($subCode == 'sp-sup') {
                            $key = $subName . ' for sell';
                        }
                    }
                    break;
                case 'bis':
                    $key = 'Business and Industry services';
                    if (!empty($subCode)) {
                        $key = $subName . ' ' . $adtype;
                        if ($subCode == 'other-bis') {
                            $key = $subName;
                        }
                    }
                    break;
                case 'hom':
                    $key = 'Home and Garden services and items for sale';
                    if (!empty($subCode)) {
                        $key = $subName . ' ' . $adtype;
                        if ($subCode == 'gard') {
                            $key = $subName . ' Services';
                        }
                    }
                    break;
                case 'ani':
                    $key = 'Animals and services';
                    if (!empty($subCode)) {
                        $key = $subName . ' ' . $adtype;
                        if ($subCode == 'pets' OR $subCode == 'vet-ser' OR $subCode == 'farm-ani' OR $subCode == 'oth-ani') {
                            $key = $subName;
                        }
                    }
                    break;
                case 'edu':
                    $key = 'Education services';
                    if (!empty($subCode)) {
                        $key = $subName . ' ' . $adtype;
                        if ($subCode == 'high-edu' OR $subCode == 'tution' OR $subCode == 'voc-ins' OR $subCode == 'oth-edu') {
                            $key = $subName;
                        }
                    }
                    break;
                case 'agr':
                    $key = 'Food & Agriculture services, equipments and items sale';
                    if (!empty($subCode)) {
                        $key = $subName . ' ' . $adtype;
                    }
                    break;
                case 'ser':
                    $key = 'Services and items for sale';
                    if (!empty($subCode)) {
                        $key = $subName;
                    }
                    break;
                case 'oth':
                    $key = 'Other Service and equipments for sale';
                    break;
                default:
                    $key = $categoryRef;
                    break;
            }
        }
        $key .= (!empty($location)) ? ' in ' . preg_replace('~[_-]~', ' ', $location) : ' in Sri Lanka';
        return $key;
    }

    public function getadstoblog()
    {
        $adslist = Alladsmaster::getAdsFeedToBlog();
//        dd($adslist);
        return view('blog_feed.index', compact('adslist', $adslist));
    }

}
