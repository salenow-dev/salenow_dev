<?php

namespace App\Http\Controllers\Mobile;

use App\Models\Member;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Notifications\AccountActivation;
use Illuminate\Support\Facades\Password;


class Auth extends Controller
{
    use SendsPasswordResetEmails;

    public function memberregister(Request $request)
    {
        $errors = '';
        //get data from json
        $json = file_get_contents('php://input');
        $obj = json_decode($json, true);

        if (!filter_var($obj['email'], FILTER_VALIDATE_EMAIL)) {
            $errors .= "Invalid email format \n";
        }
        if (!preg_match("/^[a-zA-Z ]*$/", $obj['first_name'])) {
            $errors .= "Only letters For First Name\n";
        }
        if (!preg_match("/^[a-zA-Z ]*$/", $obj['last_name'])) {
            $errors .= "Only letters for Last Name\n";
        }
        if (empty($errors)) {
            $email = $obj['email'];
            $count = Member::where('email', $email)->count();

            if (!$count) {
                $result = DB::table('members')->insert(
                    ['first_name' => $obj['first_name'], 'last_name' => $obj['last_name'], 'email' => $obj['email'],
                        'status' => 'inactive', 'password' => bcrypt($obj['first_name'])]
                );
                if ($result) {
                    $member = Member::select('id')->where('email', $email)->first();
                    $id = $member->id;

                    $token = hash_hmac('sha256', $email . time(), config('app.key'));
                    DB::table('member_activations')->insert(['member_id' => $id, 'token' => $token]);
//
                    $arr = Member::find($id);
                    $arr->notify(new AccountActivation($arr, $token));
                    $output = array('out' => 'Account created successfully', 'success' => 1, 'errors' => $errors);
                }
            } else {
                $output = array('out' => 'You already have a account', 'success' => 0, 'errors' => $errors);
            }
        } else {
            $output = array('out' => 'Account not Created. please check the form', 'success' => 0, 'errors' => $errors);
        }


        return json_encode($output);
    }

    public function memberlogin(Request $request)
    {
        $errors = '';
        //get data from json
        $json = file_get_contents('php://input');
        $obj = json_decode($json, true);

        if (!filter_var($obj['email'], FILTER_VALIDATE_EMAIL)) {
            $errors .= "Invalid email format \n";
        }

//        if (empty($errors)) {
//            $email = $obj['email'];
//            $count = Member::where('email', $email)->count();
//
//            if (!$count) {
//                $result = DB::table('members')->insert(
//                    ['first_name' => $obj['first_name'], 'last_name' => $obj['last_name'], 'email' => $obj['email'],
//                        'status' => 'inactive', 'password' => bcrypt($obj['first_name'])]
//                );
//                if ($result) {
//                    $member = Member::select('id')->where('email', $email)->first();
//                    $id = $member->id;
//
//                    $token = hash_hmac('sha256', $email . time(), config('app.key'));
//                    DB::table('member_activations')->insert(['member_id' => $id, 'token' => $token]);
////
//                    $arr = Member::find($id);
//                    $arr->notify(new AccountActivation($arr, $token));
//                    $output = array('out' => 'Account created successfully', 'success' => 1, 'errors' => $errors);
//                }
//            } else {
//                $output = array('out' => 'You already have a account', 'success' => 0, 'errors' => $errors);
//            }
//        } else {
//            $output = array('out' => 'Account not Created. please check the form', 'success' => 0, 'errors' => $errors);
//        }


        return json_encode("ssdsd");
    }

    public function reset_password(Request $request)
    {
        $errors = '';
        //get data from json
        $json = file_get_contents('php://input');
        $obj = json_decode($json, true);
//
        if (!filter_var($obj['email'], FILTER_VALIDATE_EMAIL)) {
            $errors .= "Invalid email format \n";
        }
//        dd($obj);

        if (empty($errors)) {
            $email = $obj['email'];
            $count = Member::where('email', $email)->count();
            $output = array('out' => 'Invalid email format. Please check Email', 'success' => 0, 'errors' => $count);
            if ($count) {
                $response = $this->sendResetLinkEmailApp($email);

                $output = array('out' => 'dsdsd', 'success' => 1, 'errors' => $errors);

            } else {
                $output = array('out' => 'Email not Exist. Please check Email', 'success' => 0, 'errors' => $errors);
            }
        } else {
            $output = array('out' => 'Invalid email format. Please check Email', 'success' => 0, 'errors' => $errors);
        }
        return json_encode($output);
    }

    public function broker()
    {
        return Password::broker('members');
    }

}
