<?php

namespace App\Http\Controllers\Mobile;

use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryDataFeed extends Controller
{
    public function categoriesList()
    {
        $data = Category::withCount('allAds')->get();
        return response()->json($data);
    }
    public function subcategoriesListById(Request $request)
    {
        $data = Subcategory::withCount('allAds')->where('category_id', $request->id)->get();
        return response()->json($data);
    }
}
