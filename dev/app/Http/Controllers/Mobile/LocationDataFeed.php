<?php

namespace App\Http\Controllers\Mobile;

use App\Models\City;
use App\Models\District;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocationDataFeed extends Controller
{
    public function districtsList()
    {
        $data = District::select('id', 'district_name', 'district_code', 'slug')->get()->toArray();
        return response()->json($data);
    }

    public function citiesListById(Request $request)
    {
        $data = City::select('id', 'district_id', 'city_name', 'slug')->where('district_id', $request->id)->get();
        return response()->json($data);
    }
}
