<?php

namespace App\Http\Controllers\Mobile;

use App\Classes\Boostpinintotop;
use App\Models\Adimage;
use App\Models\Alladsmaster;
use App\Models\Electronicoption;
use App\Models\Memberpremium;
use App\Models\Premiumgaleryimage;
use App\Models\Premiumservice;
use App\Models\Propertyoption;
use App\Models\Vehicleoption;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MobileAdDataFeed extends Controller
{
//    get all first ads 25
    public function allads()
    {
        $addlist = Alladsmaster::select('*')
            ->where('status', 'confirm')
            ->orderBy('published_at', 'desc')
            ->paginate(25);
        $pin_to_top_Ads = Boostpinintotop::getBoostPintoTop();
        $addlist = Boostpinintotop::removePinAdsFromCurrentPage($addlist, $pin_to_top_Ads);
        $data =array('allads'=>$addlist , 'topads'=>$pin_to_top_Ads);
        return response()->json($data);
    }

//    feed single ad data with retated ads
    public function ad_details(Request $request)
    {
        $features = array();
        $icondata = array();
        $related='';
        $premium_logo='';
        $slug = $request->slug;
        $ad_details = Alladsmaster::getAdDetailsApi($slug);
//        plus ad view count
        $ad_views = Alladsmaster::where('slug',$slug)->update(['views' => intval($ad_details->views) + 1]);
//        dd($ad_details);
        if (!is_null($ad_details->veh_features)) {
            $optionsids = unserialize($ad_details->veh_features);
            if (!empty($optionsids)) {
                foreach ($optionsids as $ids) {
                    $dispal = '';
                    $dispal = Vehicleoption::where('id', $ids)->select('display_name', 'option_name', 'id')->first();
                    if (!empty($dispal->display_name)) {
                        $features[] = $dispal->display_name;
                    }
                }
            }
        }
        if (!is_null($ad_details->pro_features)) {
            $optionsids = unserialize($ad_details->pro_features);
            if (!empty($optionsids)) {
                foreach ($optionsids as $ids) {
                    $dispal = '';
                    $dispal = Propertyoption::where('id', $ids)->select('display_name', 'option_name', 'id')->first();
                    if (!empty($dispal->display_name)) {
                        $features[] = $dispal->display_name;
                    }
                }
            }
        }
        //        electronic features
        if (!is_null($ad_details->ele_features)) {
            $optionsids = unserialize($ad_details->ele_features);
            if (!empty($optionsids)) {
                foreach ($optionsids as $ids) {
                    $dispal = '';
                    $dispal = Electronicoption::where('id', $ids)->select('display_name', 'option_name', 'id')->first();
                    if (!empty($dispal->display_name)) {
                        $features[] = $dispal->display_name;
                    }
                }
            }
        }
        if ($ad_details->category_code === 'veh') {
            if (!empty($ad_details->condition)) {
                $icondata[] = array('key' => 'Condition','value' => $ad_details->condition);
            }
            if (!empty($ad_details->mileage)) {
                $icondata[] = array('key' => 'Mileage','value' => $ad_details->mileage . ' KM');
            }
            if (!empty($ad_details->brand)) {
                $icondata[] = array('key' => 'Brand','value' => $ad_details->brand);
            }
            if (!empty($ad_details->model)) {
                $icondata[] = array('key' => 'Model','value' => $ad_details->model);
            }
            if (!empty($ad_details->fuel)) {
                $icondata[] = array('key' => 'Fuel','value' => $ad_details->fuel);
            }
            if (!empty($ad_details->enginesize)) {
                $icondata[] = array('key' => 'Engine Capacity','value' => $ad_details->enginesize.' cc',);
            }
            if (!empty($ad_details->registry_year)) {
                $icondata[] = array('key' => 'Model Year','value' => $ad_details->registry_year);
            }
            if (!empty($ad_details->bodytype_name)) {
                $icondata[] = array('key' => 'Body Type','value' => $ad_details->bodytype_name);
            }
            if (!empty($ad_details->transmission)) {
                $icondata[] = array('key' => 'Transmission','value' => $ad_details->transmission);
            }
            if ($ad_details->member_type === 'premium'){
                $related= Alladsmaster::relatedAdsByPremium($ad_details->adid, $ad_details->member_id);
//                $related=Alladsmaster::where('member_id', $ad_details->member_id)->limit(20)->get();
            }else{
                $related = Alladsmaster::relatedAdsBysubCatid($ad_details->sub_category_id, $ad_details->adid, $ad_details->category_id, $ad_details->brand_id);
            }

        }elseif ($ad_details->category_code === 'pro') {
            if (!empty($ad_details->landsize)) {
                $icondata[] = array('key' => 'Land Size','value' => $ad_details->landsize);
            }
            if (!empty($ad_details->propertysize)) {
                $icondata[] = array('key' => 'Property Size','value' => $ad_details->propertysize . ' KM');
            }
            if (!empty($ad_details->beds)) {
                $icondata[] = array('key' => 'Beds','value' => $ad_details->beds.' Bed(s)');
            }
            if (!empty($ad_details->baths)) {
                $icondata[] = array('key' => 'Bathrooms','value' => $ad_details->baths.' Bathroom(s)');
            }
            if (!empty($ad_details->address)) {
                $icondata[] = array('key' => 'Address','value' => $ad_details->address);
            }
            if ($ad_details->member_type === 'premium'){
                $related= Alladsmaster::relatedAdsByPremium($ad_details->adid, $ad_details->member_id);
//                $related=Alladsmaster::where('member_id', $ad_details->member_id)->limit(20)->get();
            }else{
                $related = Alladsmaster::relatedAdsBysubCatid($ad_details->sub_category_id, $ad_details->adid,$ad_details->category_code); //Related ads
            }

        }elseif ($ad_details->category_code === 'ele') {
            if (!empty($ad_details->ele_brand)) {
                $icondata[] = array('key' => 'Brand','value' => $ad_details->ele_brand);
            }
            if (!empty($ad_details->ele_model)) {
                $icondata[] = array('key' => 'Model','value' => $ad_details->ele_model );
            }
            if (!empty($ad_details->itemtype)) {
                $icondata[] = array('key' => 'Device Type','value' => $ad_details->itemtype);
            }
            if (!empty($ad_details->authenticity)) {
                $icondata[] = array('key' => 'Authenticity','value' => $ad_details->authenticity);
            }
            if ($ad_details->member_type === 'premium'){
                $related= Alladsmaster::relatedAdsByPremium($ad_details->adid, $ad_details->member_id);
//                $related=Alladsmaster::where('member_id', $ad_details->member_id)->limit(20)->get();
            }else{
                $related = Alladsmaster::relatedAdsBysubCatid($ad_details->sub_category_id, $ad_details->adid, $ad_details->category_id, $ad_details->ele_brand_id);
            }

        }

        if ($ad_details->member_type === 'premium'){
            $premium_logo = Memberpremium::select('profile_image')->where('member_id',$ad_details->member_id)->first();
        }

        $description = str_replace('<br />', "\n", $ad_details->description);
        $contacts = unserialize($ad_details->contact);
        $images = Adimage::where('alladsmaster_id', $ad_details->adid)->select('imagename')->get();
        $data = array('data' => $ad_details, 'images' => $images, 'features' => $features, 'description' => $description, 'contacts' => $contacts, 'icondata' => $icondata, 'relatedads'=>$related, 'premiumlogo'=>$premium_logo);
        if (!$data) {
            abort(404);
        }
        return response()->json($data);
    }

    public function incrementClickCount(Request $request){
        $slug = $request->slug;
//        plus ad Click count
        $ad_clicks = Alladsmaster::where('slug',$slug)->firstOrFail();
        $ad_clicks->clicks=intval($ad_clicks->clicks)+1;
        $ad_clicks->save();
    }

//    filters
//filter by location
    public function filterAdsByCity(Request $request){
        $data=Alladsmaster::select('*')->where('city_id',$request->id)->where('status','confirm')->get();
        // $data =City::select('id', 'district_id', 'city_name', 'slug')->where('district_id',$request->id)->get();
        return response()->json($data);
    }
    //Premium member by ID
    public function premiumMemberById(Request $request){
        $images='';
        $memberdetails=Memberpremium::select('*')->where('member_id',$request->member_id)->first();
        $ads=Alladsmaster::select('id','adtitle','category_id','sub_category_id','district_id','slug','city_id','price','featuredimage','published_at')
            ->where('member_id',$request->member_id)
            ->where('status','confirm')
            ->orderBy('published_at','DESC')
            ->limit(2)
            ->with('category','subcategory','city','district')
            ->get();
        $services=Premiumservice::select('servicetitle','service','image')
            ->where('memberpremium_id',$memberdetails->id)
            ->where('featured',1)
            ->limit(2)
            ->get();
        $images=Premiumgaleryimage::select('image')->where('memberpremium_id',$memberdetails->id)->limit(4)->get();
        $data = array('memberdetails'=>$memberdetails, 'ads'=>$ads, 'services' => $services, 'images'=>$images);
        return response()->json($data);
    }

    //premium Member Gallery Images By Id
    public function premiumMembergalleryImagesById(Request $request){
        $images='';
        $images=Premiumgaleryimage::select('image')->where('memberpremium_id',$request->member_id)
            ->get();
        return response()->json($images);
    }

    //premium Member Services By Id
    public function premiumMemberServicesById(Request $request){
        $services=Premiumservice::select('servicetitle','service','image')
            ->where('memberpremium_id',$request->member_id)
            ->get();
        return response()->json($services);
    }

    //premium Member Services By Id
    public function premiumMemberAdsById(Request $request){
        $data = Alladsmaster::select('*')
            ->where('status', 'confirm')
            ->where('member_id', $request->member_id )
            ->orderBy('published_at', 'desc')
            ->get();
        return response()->json($data);
    }
    //premium Member Services By Id
    public function premiumMemberContactsById(Request $request){
        $data = Memberpremium::select('member_id','email','telephone','mobile','address','website','lat','lng','cover_image')
            ->where('member_id', $request->member_id )
            ->first();
        return response()->json($data);
    }
}
