<?php
/**
 * Created by PhpStorm.
 * User: Niranjana
 * Date: 4/25/2017
 * Time: 6:51 PM
 */
function getUserId()
{
    $isAuthorized = Illuminate\Support\Facades\Auth::check();
    if ($isAuthorized) {
        return Auth::id();
    }
    return false;
}

function checkMember()
{
    return \Illuminate\Support\Facades\Auth::guard('member')->check();
}

function getMember()
{
    if (\Illuminate\Support\Facades\Auth::guard('member')->check()) {
        return \Illuminate\Support\Facades\Auth::guard('member')->user()->load('premiummember', 'employee');
    }
    return false;
}

function getMemberId()
{
    $type = (session()->get('profile_type')) ? session()->get('profile_type') : '';
    if ($type && $type == 'private') {
        if (\Illuminate\Support\Facades\Auth::guard('member')->check()) {
            return \Illuminate\Support\Facades\Auth::guard('member')->user()->id;
        }
    }
    if (\Illuminate\Support\Facades\Auth::guard('member')->check()) {
        $member = \Illuminate\Support\Facades\Auth::guard('member')->user()->load('premiummember');
        if (!empty($member)) {
            if ($member->parent_id) {
                if (!empty($member->premiummember) && $member->premiummember->verified) {
                    return $member->premiummember->member_id;
                }
            }
            // if member not assign to any company default member id will rerurn
            return $member->id;
        }
        //return \Illuminate\Support\Facades\Auth::guard('member')->user()->id;
    }
    return false;
}

function temporyPrivateLogin()
{
    if (checkCompany()) {
        if (session()->has('profile_type')) {
            return true;
        }
    }
    return false;
}

function getMemberType()
{
    if (\Illuminate\Support\Facades\Auth::guard('member')->check()) {
        return \Illuminate\Support\Facades\Auth::guard('member')->user()->member_type;
    }
    return false;
}

function getMemberStatus()
{
    if (\Illuminate\Support\Facades\Auth::guard('member')->check()) {
        return \Illuminate\Support\Facades\Auth::guard('member')->user()->status;
    }
    return false;
}

function checkCompany()
{
    if (\Illuminate\Support\Facades\Auth::guard('member')->check()) {
        $member = \Illuminate\Support\Facades\Auth::guard('member')->user()->load('premiummember');
        if (!empty($member)) {
            if ($member->parent_id) {
                if (!empty($member->premiummember) && $member->premiummember->verified) {
                    return true;
                }
            }
            if (getMemberType() == 'premium') {
                return true;
            }
        }
    }
    return false;
}

function isEmployeeAdmin()
{
    if (\Illuminate\Support\Facades\Auth::guard('member')->check()) {
        $employee = \Illuminate\Support\Facades\Auth::guard('member')->user()->load(['premiummember', 'employee' => function ($query) {
            $query->where('status', 'active'); //check weather employee active for this company
            $query->with('employeegroup');
        }]);
//        dd($employee);
        if ($employee->member_type != 'premium') {
            if ($employee->employee->employeegroup->slug == 'admin') {
                return true;
            }
        } else {
            return true;
        }
    }
    return false;
}

function validate($request, $validaterules, $id = null, $msg = null)
{
    $rules = '';
    foreach ($validaterules as $key => $rule) {
        $rules[$key] = $rule . ',' . $id;
    }
    $validator = Illuminate\Support\Facades\Validator::make($request->all(), $validaterules, $msg);
    if ($validator->fails()) {
        if ($request->ajax()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $messages = $validator->messages();
            return Redirect::back()->withErrors($validator);
        }
    }
}

function flash($message, $level = 'success')
{
    session()->flash('flash_message', $message);
    session()->flash('flash_message_level', $level);
}

function paginate($items, $perPage)
{

    $pageStart = request('page', 1);
    $offSet = ($pageStart * $perPage) - $perPage;
    if ($items instanceof Illuminate\Support\Collection) {
        $itemsForCurrentPage = $items->slice($offSet, $perPage);
    } else {
        $itemsForCurrentPage = array_slice($items, $offSet, $perPage, TRUE);
    }

    return new Illuminate\Pagination\LengthAwarePaginator(
        $itemsForCurrentPage, $items->count(), $perPage,
        Illuminate\Pagination\Paginator::resolveCurrentPage(),
        ['path' => Illuminate\Pagination\Paginator::resolveCurrentPath()]
    );
}

function truncateText($text, $chars = 25)
{
    $text = $text . " ";
    $text = substr($text, 0, $chars);
    $text = substr($text, 0, strrpos($text, ' '));
    $text = $text . "...";
    return $text;
}

function timeForUser($time)
{
    $publishedDate = Carbon\Carbon::parse($time);
    $minutes = $publishedDate->diffInMinutes(Carbon\Carbon::now());
    $days = $publishedDate->diffInDays(Carbon\Carbon::now());
    $yesterday = Carbon\Carbon::yesterday()->toDateString();
    $publishedTime = '';
    if ($minutes < 1) {
        $publishedTime = 'Just now  ' . $publishedDate->diffForHumans();
    } elseif ($minutes >= 1 && $minutes < 60) {
        $publishedTime = 'Today ' . $publishedDate->diffForHumans();
    } elseif ($minutes > 60 && $yesterday < $publishedDate->format('Y-m-d')) {
        $publishedTime = 'Today ' . $publishedDate->format('g:ia');
    } elseif ($yesterday == $publishedDate->format('Y-m-d')) {
        $publishedTime = 'Yesterday ' . $publishedDate->format('g:ia');
    } elseif ($days >= 2) {
        $publishedTime = $publishedDate->format('j M Y, g:ia');
    } else {
        $publishedTime = $publishedDate->format('j M Y, g:ia');
    }
    return $publishedTime;
}

//pin to top ads count
function pintotopRecordsCount($model, $value)
{
//    \Illuminate\Support\Facades\Session::forget('adsStatus');
    $dynamicObj = 'App\Models\\' . $model;
    $class = new $dynamicObj;
    $rowCount = $class::whereNull($value)->count();
//    if($rowCount != 0){
//        \Illuminate\Support\Facades\Session::set('adsStatus', true);
//    }
    return $rowCount;
}

//pending OTPs count
function pendingOtpCount()
{
//    \Illuminate\Support\Facades\Session::forget('adsStatus');
    $dynamicObj = 'App\Models\Otp';
    $class = new $dynamicObj;
    $rowCount = $class::whereDate('created_at', date('Y-m-d'))->count();
//    if($rowCount != 0){
//        \Illuminate\Support\Facades\Session::set('adsStatus', true);
//    }
    return $rowCount;
}

//pending ads count
function adsStatusCount($status)
{
//    \Illuminate\Support\Facades\Session::forget('adsStatus');
    $dynamicObj = 'App\Models\Alladsmaster';
    $class = new $dynamicObj;
    $rowCount = $class::where('status', $status)->count();
//    if($rowCount != 0){
//        \Illuminate\Support\Facades\Session::set('adsStatus', true);
//    }
    return $rowCount;
}

function slugUTF($title, $separator = '-')
{
    //$title = static::ascii($title);

    // Convert all dashes/underscores into separator
    $flip = $separator == '-' ? '_' : '-';

    $title = preg_replace('![' . preg_quote($flip) . ']+!u', $separator, $title);

    // Remove all characters that are not the separator, letters, numbers, or whitespace.
    //$title = preg_replace('![^'.preg_quote($separator).'\pL\pN\s]+!u', '', mb_strtolower($title));
    //$title  = preg_replace(array('/^\[/','/\]$/'), '',$title);

    // Replace all separator characters and whitespace by a single separator
    $title = preg_replace('![' . preg_quote($separator) . '\s]+!u', $separator, $title);
    $title = strtolower(trim($title, $separator));

    return $title;
}