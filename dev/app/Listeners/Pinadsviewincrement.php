<?php

namespace App\Listeners;

use App\Events\Pintotopviewcounter;
use App\Models\Boostviewcount;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\DB;

class Pinadsviewincrement
{
    /**
     * @var Store
     */
    private $session;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Store $session)
    {
        //
        $this->session = $session;
    }

    /**
     * Handle the event.
     *
     * @param Pintotopviewcounter $pinAd
     * @return void
     * @internal param Pintotopviewcounter $event
     */
    public function handle($pinAd)
    {
//        $this->session->forget('viewed_pin_ads');
//        dd($this->session->get('viewed_pin_ads'));
        if ( ! $this->isPostViewed($pinAd->pin_ad_id)) {
            DB::table('boostviewcounts')->where('id', $pinAd->pin_ad_id)->increment('views');
            //Boostviewcount::where('id', $pinAd->pin_ad_id)->increment('views');
            $this->storePost($pinAd->pin_ad_id);
        }
    }

    private function isPostViewed($id)
    {
        // Get all the viewed posts from the session. If no
        // entry in the session exists, default to an
        // empty array.
        $viewed = $this->session->get('viewed_pin_ads', []);

        // Check the viewed posts array for the existance
        // of the id of the post
        //return in_array($id, $viewed);
        return array_key_exists($id, $viewed);
    }

    private function storePost($id)
    {
        // Push the post id onto the viewed_posts session array.
        //$this->session->push('viewed_pin_ads', $id);

        // First make a key that we can use to store the timestamp
        // in the session. Laravel allows us to use a nested key
        // so that we can set the post id key on the viewed_posts
        // array.
        $key = 'viewed_pin_ads.' . $id;

        // Then set that key on the session and set its value
        // to the current timestamp.
        $this->session->put($key, time());
    }
}
