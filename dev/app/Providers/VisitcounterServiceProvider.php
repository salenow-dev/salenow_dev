<?php
/**
 * Created by PhpStorm.
 * User: Niranjana
 * Date: 8/21/2017
 * Time: 12:07 PM
 */

namespace App\Providers;


use App\Classes\TrackVisitor;
use Illuminate\Support\ServiceProvider;

class VisitcounterServiceProvider extends ServiceProvider
{
    //ioc container
    public function register()
    {
        $this->app->bind('visicount', TrackVisitor::class);
    }
}