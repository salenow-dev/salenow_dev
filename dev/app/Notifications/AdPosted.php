<?php

namespace App\Notifications;

use App\Models\Alladsmaster;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AdPosted extends Notification
{
    use Queueable;

    protected $post_ad;
    /**
     * @var null
     */
    private $update;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Alladsmaster $alladsmaster, $update = NULL)
    {
        $this->post_ad = $alladsmaster;
        $this->update = $update;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
//        dd($this->post_ad);
        if ($this->update == 'update') {
            return (new MailMessage)
                ->greeting('Hello ' . $this->post_ad->member->first_name . ' ! your Ad on Saleme.lk was Updated')
                ->subject('Your Ad - ' . $this->post_ad->adtitle . ' was Updated on Saleme.lk.')
                ->line('Your ad ' . $this->post_ad->adtitle . ' is now on on review!')
                ->action('View Ad', url('/ad/' . $this->post_ad->slug))
                ->line('Thank you for using Saleme.lk!');
        } else {
            return (new MailMessage)
                ->greeting('Hello ' . $this->post_ad->member->first_name . ' ! Thank You for posting on Saleme.lk')
                ->subject('New Ad - ' . $this->post_ad->adtitle . ' was posted on Saleme.lk.')
                ->line('Your ad ' . $this->post_ad->adtitle . ' is now on on review!')
                ->action('View Ad', url('/ad/' . $this->post_ad->slug))
                ->line('Thank you for using Saleme.lk!');
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
