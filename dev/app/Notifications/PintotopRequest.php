<?php

namespace App\Notifications;

use App\Models\Alladsmaster;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PintotopRequest extends Notification
{
    use Queueable;
    /**
     * @var Alladsmaster
     */
    private $pinAd;
    /**
     * @var
     */
    private $voucherPrice;

    /**
     * Create a new notification instance.
     *
     * @param Alladsmaster $pinAd
     * @param string $voucherPrice
     */
    public function __construct($pinAd, $voucherPrice = '')
    {
        $this->pinAd = $pinAd;
        $this->voucherPrice = $voucherPrice;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $pinAd = $this->pinAd;
        $voucherPrice = '';
        $subject = '';
        if(!empty($this->voucherPrice)){
            $voucherPrice =$this->voucherPrice;
            $subject = 'Ad Boosting Request';
        }else{
            $voucherPrice ='success';
            $subject = 'Ad Boost Successfull!';
        }
        return (new MailMessage)
            ->view('vendor.notifications.boost-pintotop',compact('pinAd','voucherPrice'))
            ->subject($subject);
//            ->line('The introduction to the notification.')
//            ->action('Notification Action', 'https://laravel.com')
//            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
