<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SendEmailInquiry extends Notification
{
    use Queueable;
    public $name;
    public $email;
    public $phone;
    public $subject;
    public $message;
    public $company_name;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($info)
    {
        $this->company_name = $info['company_name'];
        $this->name = $info['name'];
        $this->email = $info['email'];
        $this->phone = $info['phone'];
        $this->subject = $info['subject'];
        $this->message = $info['message'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
//            ->to('jayawardanasalinda@gmail.com')
            ->from($this->email,$this->name)
            ->view('vendor.notifications.saleme-sendInquiryEmail')
            ->greeting('Hi '.$this->company_name)
            ->subject(' '.$this->name.' have sent you an E-mail regarding '.$this->subject.' Via SaleMe.lk')
            ->line('You have a message from '.$this->name.' regarding '.$this->subject.'')
            ->line('Name : '. $this->name . '')
            ->line('E-mail : '. $this->email . '')
            ->line('Contact No : '. $this->phone . '')
            ->line('Subject : '. $this->subject . '')
            ->line('Message : '. $this->message . '')
            ->line('Thank you for using Saleme.lk!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
