<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SendToFriend extends Notification
{
    use Queueable;
    /**
     * @var
     */
    public $adInfo;
    /**
     * @var
     */
    public $from;
    /**
     * @var
     */
    public $msg;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($adInfo, $from, $msg)
    {
        //
        $this->adInfo = $adInfo;
        $this->from = $from;
        $this->msg = $msg;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $adinfo = $this->adInfo;
        $from = $this->from;
        $msg = $this->msg;
        return (new MailMessage)
            ->subject('Ad Suggession from ' . $from . '  | SaleMe.lk')
            ->view('vendor.notifications.send-to-friend', compact('adinfo', 'from', 'msg'))
            ->line('The introduction to the notification.')
            ->action('Go to Ad', url('/ad/' . $adinfo->slug))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
