<?php

namespace App\Notifications;

use App\Models\Member;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AccountActivation extends Notification
{
    use Queueable;
    /**
     * @var Member
     */
    private $member;
    /**
     * @var
     */
    private $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Member $member,$token)
    {
        //
        $this->member = $member;
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $statusforTitle = 'activate';
        $greating = 'Hello '.$this->member->first_name.',';

        return (new MailMessage)
            ->view('vendor.notifications.emailpublish',
                compact('statusforTitle', 'greating'))
            ->subject('Saleme.lk | Account Activation')
            ->line('Please confirm your email address, to activate your account. If you received this by mistake or weren\'t expecting it, please disregard this email.')
            ->action('activate', url('/member/activate/' . $this->token));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
