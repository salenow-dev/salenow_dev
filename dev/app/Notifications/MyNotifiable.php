<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MyNotifiable extends Notification
{
    public $email;

    public function __construct($email)
    {
        $this->email = $email;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
//            ->to($this->post_ad->member->email)
            ->greeting('Hello '.$this->post_ad->member->first_name.' ! Thank You for posting on Saleme.lk')
            ->subject('New Ad - '. $this->post_ad->adtitle . ' was posted on Saleme.lk. ')
            ->line('Your ad '. $this->post_ad->adtitle . ' is now on on review!')
            ->action('View Ad', url('/ad/'.$this->post_ad->slug))
            ->line('Thank you for using Saleme.lk!');
    }
}