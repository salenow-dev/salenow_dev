<?php

namespace App\Notifications;

use App\Models\Adspam;
use App\Models\Alladsmaster;
use App\Models\Member;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\DB;

class AdPublished extends Notification
{
    use Queueable;
    /**
     * @var
     */
    public $post_ad;
    public $name;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Alladsmaster $post_ad)
    {
        $this->post_ad = $post_ad;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
//        return (new MailMessage)
//            ->greeting('Hello '.$this->post_ad->member->first_name.' ! Thank You for posting on Saleme.lk')
//            ->subject('New Ad - '. $this->post_ad->adtitle . ' is now Published on Saleme.lk. ')
//            ->line('Your ad '. $this->post_ad->adtitle . 'Live on Saleme.lk!')
//            ->action('Go to Your Ad', url('/ad/'.$this->post_ad->slug))
//            ->line('Thank you for using Saleme.lk!');
        $statusforTitle = $this->post_ad->status;
        $greating = 'Hello ' . $this->post_ad->member->first_name . ',';
        $string = '';
        $spamReasons = '';
        if ($statusforTitle != 'draft') {
            if ($statusforTitle == 'pending') {
                $string = ' is now on Review!';
            } else if ($statusforTitle == 'confirm') {
                $string = ' is now Live on Saleme.lk!';
            } else if ($statusforTitle == 'cancel') {
                $spamReasonsIDs = Adspam::where('alladsmaster_id', $this->post_ad->id)->select('spamreason_id')->first();
                $spams = unserialize($spamReasonsIDs->spamreason_id);
                if (!empty($spams)) {
                    foreach ($spams as $spam) {
                        $spamReasons[] = DB::table('spamreasons')->where('id', $spam)->select('reason')->first();
                    }
                }
                $string = ' moved to the Spam!';
            } else {
                $string = ' Expired!';
            }
        }
        return (new MailMessage)
            ->view('vendor.notifications.emailpublish',
                compact('statusforTitle', 'greating', 'spamReasons'))
            ->subject('Your ad ' . $this->post_ad->adtitle . $string)
            ->line('Your ad ' . $string)
            ->line('Ad Title : ' . $this->post_ad->adtitle)
            ->buttons('view', url('/ad/' . $this->post_ad->slug))
            ->buttons('edit', url('/ad/edit/' . $this->post_ad->slug))//            ->level( ($statusforTitle =='cancel')?'error':'')
            ;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
