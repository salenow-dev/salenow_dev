<?php

namespace App\Notifications;

use App\Models\Alladsmaster;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AdInquiry extends Notification
{
    use Queueable;
    /**
     * @var
     */
    private $name;
    private $email;
    private $mobile;
    private $message;
    private $ad_title;
    private $slug;

    protected $ad;

    /**
     * Create a new notification instance.
     *
     * @param Alladsmaster $post_ad
     */
    public function __construct($info, Alladsmaster $ad)
    {
        $this->name = $info['name'];
        $this->email = $info['email'];
        $this->mobile = $info['mobile'];
        $this->message = $info['message'];
        $this->ad_title = $info['ad_title'];
        $this->slug = $info['slug'];
        $this->ad = $ad;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
//            ->to('niranjana688@gmail.com')
//            ->from('krishnarao@gmail.com','Krishna Rao')
            ->view('vendor.notifications.saleme-email')
            ->greeting('Hello '.$this->ad->member->first_name.'')
            ->subject(' '.$this->name.' have sent you an E-mail reguarding '.$this->ad_title.' ')
            ->line('Name : '. $this->name . '')
            ->line('E-mail : '. $this->email . '')
            ->line('Contact No : '. $this->mobile . '')
            ->line('Message : '. $this->message . '')
            ->line('Ad Title : '. $this->ad_title . '')
            ->action('View Ad', url('/ad/'.$this->slug))
            ->line('Thank you for using Saleme.lk!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
