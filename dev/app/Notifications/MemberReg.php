<?php

namespace App\Notifications;

use App\Models\Member;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MemberReg extends Notification
{
    use Queueable;
    private $member;


    /**
     * Create a new notification instance.
     *
     * @param Member $member
     * @internal param Alladsmaster $post_ad
     */
    public function __construct(Member $member)
    {
        $this->member = $member;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $statusforTitle = 'activation';
        $greating = 'Hello '.$this->member->first_name.' '.$this->member->last_name.' ';
        return (new MailMessage)
            ->view('vendor.notifications.emailpublish',
                compact('statusforTitle', 'greating'))
            ->subject(' '.$this->member->first_name.' Your registration on Saleme.lk is complete!')
            ->line('Name : '. $this->member->first_name .' '.$this->member->last_name.'')
            ->line('E-mail : '. $this->member->email . '')
            ->line('Contact No : '. $this->member->mobile . '')
            ->line('Thank you for using Saleme.lk!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
