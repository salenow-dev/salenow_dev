<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PintotopRequestFromBackend extends Notification
{
    use Queueable;
    private $pintotops;
    private $period;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($pintotops, $period)
    {
        $this->pintotops = $pintotops;
        $this->period = $period;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $pintotops = $this->pintotops;
        $period = $this->period;
        $subject = 'SaleMe.lk offered ' . $pintotops . ' of Pin To Top ads';
        return (new MailMessage)
            ->view('vendor.notifications.assign-pintotop-from-backend', compact('pintotops', 'period'))
            ->subject($subject);
//            ->line('The introduction to the notification.')
//            ->action('Notification Action', 'https://laravel.com')
//            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
