<?php

namespace App\Services;

use App\Models\Alladsmaster;
use App\Models\Memberpremium;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Slug extends Model
{
    /**
     * @param $title
     * @param $addtional
     * @param int $id
     * @param bool $update
     * @return string
     * @throws \Exception
     */
    public static function createSlug($title, $addtional, $update = false, $id = 0)
    {
        $i = 1;
        // Normalize the title
        $title = str_replace(str_split('\\/:*?"<>|^%$&@?#'), ' ', $title);

        $type = (!empty($addtional['ad_type'])) ? self::getAdType($addtional['ad_type']) : '';
        $city = (!empty($addtional['location'])) ? $addtional['location'] : '';
        $slug = slugUTF($title . ' ' . $type . ' ' . $city);
        if ($update) {
            return $slug;
        }
        // Get any that could possibly be related.
        // This cuts the queries down by doing it once.

        $allSlugs = self::getRelatedSlugs($slug, $id);
        // If we haven't used it before then we are all good.
        //if (!$allSlugs->contains('slug', $slug)) {
        //    return $slug;
        //}
        if ($allSlugs->isEmpty()) {
            return $slug;
        }

        $newSlug = $slug.'-'.$i;
        while ($allSlugs->contains('slug', $newSlug)){
            $i++;
            $newSlug = $slug.'-'.$i;
        }
        return $newSlug;
//        // Just append numbers like a savage until we find not used.
//        for ($i = 1; $i <= 10000; $i++) {
//            $newSlug = $slug . '-' . $i;
//            if (!$allSlugs->contains('slug', $newSlug)) {
//                return $newSlug;
//            }
//        }
//        throw new \Exception('Can not create a unique slug');
    }

    protected static function getRelatedSlugs($slug, $id = 0)
    {
        return Alladsmaster::select('slug')->where('slug', 'like', $slug . '%')->where('id', '<>', $id)->get();
    }

    protected static function getAdType($type)
    {
        $newType = '';
        switch ($type) {
            case 'sell':
                $newType = 'for sell';
                break;
            case 'buy':
                $newType = 'for buy';
                break;
            case 'rent':
                $newType = 'for rent';
                break;
            case 'lookforrent':
                $newType = 'for rent';
                break;
            default:
                $newType = '';
        }
        return $newType;
    }

    public static function uniqueSlug($model, $value)
    {
        $i = 1;
        $slug = slugUTF($value);
        $allSlugs = self::getExistingSlugs($model,$slug, $id = 0);

        if ($allSlugs->isEmpty()) {
            return $slug;
        }

        if(!$allSlugs->contains('slug', $slug)){
            return $slug;
        }
        $newSlug = $slug.'-'.$i;
        while ($allSlugs->contains('slug', $newSlug)){
            $i++;
            $newSlug = $slug.'-'.$i;
        }
        return $newSlug;
    }

    protected static function getExistingSlugs($model,$slug, $id = 0)
    {
        $dynamicObj = 'App\Models\\' . ucfirst($model);
        $class = new $dynamicObj;
        return $class::select('slug')->where('slug', 'like', $slug . '%')->where('id', '<>', $id)->get();
    }
}
