<?php
/**
 * Created by PhpStorm.
 * User: Niranjana
 * Date: 8/9/2017
 * Time: 4:04 PM
 */

namespace App\Classes;

use App\Events\Pintotopviewcounter;
use App\Models\Adimage;
use App\Models\Alladsmaster;
use App\Models\Boostviewcount;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Session;

class Boostpinintotop
{
    function __construct()
    {
    }

    public static function getBoostPintoTop($querydata = null)
    {
        /***********************************************************************************************
         * START PIN-TO-TOP Ads
         * get all ads ids in current page
         ***/
        //$allCount = Boostviewcount::count();
        $boostList = '';
        $boostList = Alladsmaster::allPinToTopAds($querydata);
//        if ($allCount == count(Session::get('viewed_pin_ads'))) {
//            $boostList = Alladsmaster::allPinToTopAds($querydata, 'views');
//        } else {
//            $boostList = Alladsmaster::allPinToTopAds($querydata);
//        }

        if (!empty($boostList)) {
            foreach ($boostList as $pin_to_top_Ad) {
                $images = Adimage::where('alladsmaster_id',$pin_to_top_Ad->adid)->where('imagename', '!=', $pin_to_top_Ad->featuredimage )->select('imagename')->get();
                $pin_to_top_Ad->adimages =$images;
//                dd($pin_to_top_Ad);
                Event::fire(new Pintotopviewcounter($pin_to_top_Ad->views_id));
            }
        }
        return $boostList;
        /*
         * END fire the hit counter increment event for all pinned ads in current page
         * END PIN-TO-TOP Ads
         *************************************************************************************************/
    }

    public static function removePinAdsFromCurrentPage($allAdList, $pin_to_top_Ads)
    {

        if (!empty($allAdList)) {
            //remove pin ad from page
            $pageItems = $allAdList->items();
            if (!empty($pageItems)) {
                foreach ($pin_to_top_Ads as $ad) {
                    $pinAdsIds[$ad->adid] = true;
                }
            }
            foreach ($allAdList as $key => $item) {
                if (isset($pinAdsIds[$item->adid])) {
                    unset($allAdList[$key]);
                }
            }
            return $allAdList;
        }
    }

}