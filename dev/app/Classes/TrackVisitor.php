<?php
/**
 * Created by PhpStorm.
 * User: Niranjana
 * Date: 8/21/2017
 * Time: 12:10 PM
 */

namespace App\Classes;


use App\Models\Alladsmaster;
use Illuminate\Support\Facades\Cookie;

class TrackVisitor
{
    public function trackVisitor($id, $type)
    {
        $ad = Alladsmaster::find($id);
        switch ($type) {
            case 'view':
                if ($ad) {
                    $ad->plusViews();
//                    $savedVisitorCookie = Cookie::get('saleme_visitorview');
//                    if (!isset($savedVisitorCookie[$ad->id])) {
//                        $status = $ad->plusViews();
//                        if ($status) {
//                            $visitorCookie = cookie('saleme_visitorview['.$ad->id.']', true, 10080);
//                            Cookie::queue($visitorCookie);
//                        }
//                    }
                }
                break;
            case 'click':
                if ($ad) {
//                    Cookie::forget('saleme_visitorclick');
                    $savedVisitorCookie = Cookie::get('saleme_visitorclick');
                    if (!isset($savedVisitorCookie[$ad->id])) {
                        $status = $ad->plusClicks();
                        if ($status) {
                            $visitorCookie = cookie('saleme_visitorclick['.$ad->id.']', true, 1440);
                            Cookie::queue($visitorCookie);
                        }
                    }
                }
                break;
        }
    }
}