<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Facades\Request;
use Psy\Exception\ErrorException;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        $messege = $exception->getMessage();
        $line = $exception->getLine();
        $file = $exception->getFile();
        $error = $exception->getTraceAsString();

        echo $messege . '/<br>' . $line . '/ <br>' . $file;exit;
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
//        switch ($exception) {
//            case ($exception instanceof ModelNotFoundException):
//                return $this->renderException($exception);
//                break;
//            case ($exception instanceof MethodNotAllowedHttpException):
//                return $this->renderException($exception);
//                break;
//            case ($exception instanceof TokenMismatchException):
//                return $this->renderException($exception);
//                break;
//            case ($exception instanceof \BadMethodCallException):
//                return $this->renderException($exception);
//                break;
//            case ($exception instanceof FatalErrorException):
//                return $this->renderException($exception);
//                break;
//            case ($exception instanceof \ErrorException):
//                return $this->renderException($exception);
//                break;
//            case ($exception instanceof QueryException):
//                return $this->renderException($exception);
//                break;
//            default:
//                return parent::render($request, $exception);
//                return $this->renderException($exception);
//        }
        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Illuminate\Auth\AuthenticationException $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('login');
    }

    protected function renderException($e)
    {
    //        switch ($e) {
    //            case ($e instanceof ModelNotFoundException):
    //                return response()->view('errors.404', [], 404);
    //                break;
    //            case ($e instanceof MethodNotAllowedHttpException):
    //                return response()->view('errors.404', [], 404);
    //                break;
    //            case ($e instanceof \ErrorException):
    //                return response()->view('errors.404', [], 404);
    //                break;
    //            case ($e instanceof QueryException):
    //                $error = "Your Search keywords not compatible with the our Records! Please change the search keyword and try again.";
    //                return response()->view('errors.customexception', compact('error'));
    //                break;
    //            case ($e instanceof TokenMismatchException):
    //                if (Request::ajax()) {
    //                    return response()->json(['response' => 'Sorry, your session seems to have expired. Refresh the page & try again.']);
    //                } else {
    //                    return redirect()->back();
    //                }
    //                break;
    //            default:
    //                return response()->view('errors.503', [], 503);
    //                return (new SymfonyDisplayer(config('app.debug')))
    //                    ->createResponse($e);
    //        }
    }
}
