<?php

namespace App\Social;

use App\Models\Member;
use App\Models\Membersocial;

if (!session_id()) {
    session_start();
}



require_once 'ggautoload.php';

use GoogleApi\Config;
use GoogleApi\Client;
use GoogleApi\Contrib\apiOauth2Service;
use GoogleApi\Exception;

class Googlesocial {

    // constructors
    function __construct() {
        
    }

    public static function GetGoogleUser() {

        try {



            $client = new Client();

            $client->setAccessType('offline'); // default: offline
            $client->setApplicationName('Saleme.lk');
            $client->setClientId('751409729418-v313glk3nkqt8kjp4lm6j5c9733gr0f0.apps.googleusercontent.com');
            $client->setClientSecret('iWJeXmHLwOQF4bkiCvGSj3xO');
            $client->setRedirectUri('http://dev.salemelk.com/sociallogin/google');
            
            $client->setScopes("https://www.googleapis.com/auth/userinfo.email");
//$client->setDeveloperKey('INSERT HERE'); // API key


            $oauth2 = new apiOauth2Service($client);
    
    if (isset($_GET['code'])) {

              $client->authenticate($_GET['code']);
                $_SESSION['token'] = $client->getAccessToken();
                $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
                header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
                return;
                 if (isset($_SESSION['token'])) {
                $client->setAccessToken($_SESSION['token']);
            }
        

            if ($client->getAccessToken() != '') {
 
               
                $_SESSION['token'] = $client->getAccessToken();
                $user = $oauth2->userinfo->get();
          
                $ggid = $user['id'];
                $firstname = !empty($user['given_name']) ? $user['given_name']:'';
                $lastname = !empty($user['family_name']) ? $user['family_name'] :'';
                $fullname = !empty($user['name']) ? $user['name']:'';
                $email = !empty($user['email']) ? $user['email']:'';
                $gender = !empty($user['gender']) ? $user['gender'] :'';

                //  check  user  by email in member table
                $memeberdetails = Member::Checkemailaddress($email);
                $memeberexist = Membersocial::CheckMemberByauthid($ggid,'google');
                $profileimage = !empty($user['picture']) ? $user['picture'] :'';
          
                if (!empty($ggid)) {

                    if (empty($memeberexist)) {
                        if (!empty($memeberdetails) && !empty($ggid)) {
                                
                                $socialmanage = new Membersocial();
                                $manage_member = $socialmanage->addSocialMember($memeberdetails->id, 'google', $ggid,$profileimage);
                                $data2['success'] = 'success';
                                $data2['loginurl'] = '';
                                $data2['checkuser'] = $memeberdetails;
                                return $data2;
                            
                        } elseif (empty($memeberdetails) && !empty($ggid)) {

                            
                              // inser member  and social  data by facebook
                       $memebermanage = new Member();
                        $memberid = $memebermanage->addMemberFromfb($email,$firstname,$lastname,$gender);
                         
                    $socialmanage = new Membersocial();
                $manage_member = $socialmanage->addSocialMember($memberid,'google',$ggid,$profileimage);
                            
                            
                       $memeberdatasbyid = Member::MemberDetailsById($memberid);
                            $data3['success'] = 'success';
                            $data3['loginurl'] = '';
                            $data3['checkuser'] = $memeberdatasbyid;
                            return $data3;
                        }
                       
                    } else {

                        // memeber detais  get by id 
                       $memeberdatasbyid = Member::MemberDetailsById($memeberexist->member_id);
                 
                        $data4['success'] = 'success';
                        $data4['loginurl'] = '';
                        $data4['checkuser'] = $memeberdatasbyid;
                        return $data4;
                    }
                }
    } else {
                $authUrl = $client->createAuthUrl();

                $data5['success'] = '';
                $data5['checkuser'] = '';
                $data5['loginurl'] = $authUrl;
                return $data5;
    } } else {
                $authUrl = $client->createAuthUrl();

                $data5['success'] = '';
                $data5['checkuser'] = '';
                $data5['loginurl'] = $authUrl;
                return $data5;
            }
        } catch (FacebookRequestException $ex) {
            // When Facebook returns an error
//        
//        } catch (FacebookApiException $e) {
//            echo $e->getMessage();
//            exit;
//            error_log($e);
//            $user = null;
//        }
        }
    }

    public static function Profile() {
        $facebook = new Facebook(array('appId' => APP_ID, 'secret' => APP_SECRET));  //  access  fb  config  data 
        $fbUserProfile = $facebook->api('/me?fields=id,first_name,last_name,email,link,gender,locale,picture');

//    return $loginURL;	
    }

    public static function fblogout() {



        if (isset($_REQUEST['logout'])) {
            unset($_SESSION['token']);
            $client->revokeToken();
        }

        return true;
    }

    public static function object_to_array($data) {
        if (is_array($data) || is_object($data)) {
            $result = array();
            foreach ($data as $key => $value) {
                $result[$key] = self::object_to_array($value);
            }
            return $result;
        }
        return $data;
    }

}

?>
