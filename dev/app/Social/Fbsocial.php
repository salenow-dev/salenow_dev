<?php
namespace App\Social;
use App\Models\Member;
use App\Models\Membersocial;

if (!session_id()) {
    session_start();
}


require_once 'fbautoload.php';

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\GraphLocation;
use Facebook\GraphUser;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;

class Fbsocial
{

    // constructors
    function __construct()
    {

    }

    public static function GetFBUser()
    {

        try {
            // init app with app id and secret
            FacebookSession::setDefaultApplication('1867918153484824', '59bee5c3ddead1c1478d05696950f138');
// login helper with redirect_uri
            $helper = new FacebookRedirectLoginHelper('http://saleme.me/sociallogin/facebook');
            $session = $helper->getSessionFromRedirect();

            if (isset($session) && !empty($session)) {

                // graph api request for user data

                $request = new FacebookRequest($session, 'GET', '/me?fields=id,first_name,last_name,email,gender,birthday,hometown');
                $response = $request->execute();

                $user = $response->getGraphObject(GraphUser::classname());
                $location = $response->getGraphObject(GraphLocation::classname());
                $graphObject = $response->getGraphObject();

                $fbid = $graphObject->getProperty('id');
                $firstname = $graphObject->getProperty('first_name');
                $lastname = $graphObject->getProperty('last_name');
                $fullname = $graphObject->getProperty('first_name') . ' ' . $graphObject->getProperty('last_name');
                $email = $graphObject->getProperty('email');
                $gender = $graphObject->getProperty('gender');
                $profileimage = "https://graph.facebook.com/" . $fbid . "/picture?width=140&height=110";
                //  check  user  by email in member table
                $memeberdetails = Member::Checkemailaddress($email);
                $memeberexist = Membersocial::CheckMemberByauthid($fbid, 'facebook');
                if (!empty($fbid)) {

                    if (empty($memeberexist)) {
                        if (!empty($memeberdetails) && !empty($fbid)) {

                            $socialmanage = new Membersocial();
                            $manage_member = $socialmanage->addSocialMember($memeberdetails->id, 'facebook', $fbid, $profileimage);
                            $data2['success'] = 'success';
                            $data2['loginurl'] = '';
                            $data2['checkuser'] = $memeberdetails;
                            return $data2;


                        } elseif (empty($memeberdetails) && !empty($fbid)) {

                            // inser member  and social  data by facebook
                            $memebermanage = new Member();
                            $memberid = $memebermanage->addMemberFromfb($email, $firstname, $lastname, $gender);

                            $socialmanage = new Membersocial();
                            $manage_member = $socialmanage->addSocialMember($memberid, 'facebook', $fbid, $profileimage);

                            $memeberdatasbyid = Member::MemberDetailsById($memberid);
                            $data3['success'] = 'success';
                            $data3['loginurl'] = '';
                            $data3['checkuser'] = $memeberdatasbyid;
                            return $data3;
                        }
                    } else {

                        // memeber detais  get by id
                        $memeberdatasbyid = Member::MemberDetailsById($memeberexist->member_id);

                        $data4['success'] = 'success';
                        $data4['loginurl'] = '';
                        $data4['checkuser'] = $memeberdatasbyid;
                        return $data4;
                    }

                }
            } else {
                $permissions = ['email', 'user_birthday', 'user_hometown']; //  ask permission from user
                $loginUrl = $helper->getLoginUrl($permissions);

                $data5['success'] = '';
                $data5['checkuser'] = '';
                $data5['loginurl'] = $loginUrl;
                return $data5;
            }
        } catch (FacebookRequestException $ex) {
            // When Facebook returns an error
//        
//        } catch (FacebookApiException $e) {
//            echo $e->getMessage();
//            exit;
//            error_log($e);
//            $user = null;
//        }
        }
    }

    public static function Profile()
    {
        $facebook = new Facebook(array('appId' => APP_ID, 'secret' => APP_SECRET));  //  access  fb  config  data 
        $fbUserProfile = $facebook->api('/me?fields=id,first_name,last_name,email,link,gender,locale,picture');

//    return $loginURL;	
    }

    public static function fblogout()
    {


        if (isset($_SESSION['fb_access_token'])) {
            unset($_SESSION['fb_access_token']);

        }

        return true;
    }

    public static function object_to_array($data)
    {
        if (is_array($data) || is_object($data)) {
            $result = array();
            foreach ($data as $key => $value) {
                $result[$key] = self::object_to_array($value);
            }
            return $result;
        }
        return $data;
    }

}

?>
