<?php

namespace App\Console\Commands;

use App\Models\Adrepotrdetail;
use App\Models\Alladsmaster;
use App\Models\Boostviewcount;
use App\Models\Membervoucher;
use App\Models\Membervouchercode;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class Expirepintotop extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:exprirepintotop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatically expire pin-to-top ads';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $data = DB::table('alladsmasters as mm')
//            ->where('mm.ad_boost_status', '=', 'pin-to-top')
//            ->whereDate('mm.boost_expire_on', '<', Carbon::now()->format('Y-m-d'))
//            ->select('mm.id','mm.ad_referance','mm.boost_expire_on')
//            ->get()->toArray();
        $data = Alladsmaster::with('boostads')->where('ad_boost_status','pin-to-top')->where('boost_expire_on','<',Carbon::now()->format('Y-m-d'))
            ->select('id','ad_referance','boost_expire_on')->get();

        $count = 0;
        foreach ($data as $item) {
            $adReportInfo = new Adrepotrdetail();
            $adReportInfo->saveAdInfo($item);
            $status = DB::table('alladsmasters')
                ->where('id', $item->id)
                ->update([
                    'ad_boost_status' => 'none',
                    'boost_expire_on' => NULL,
                ]);

            if($status){
                //delete from view count table
                Boostviewcount::where('alladsmaster_id', $item->id)->delete();

                //delete from member voucher table
                $voucher = Membervoucher::where('ad_referance',$item->ad_referance)->whereNull('boostpack_id')->delete();

                //delete from member voucher codes table
                $voucherCode = Membervouchercode::where('ad_referance',$item->ad_referance)->where('status',true)->delete();
                $count++;
            }
        }
        dd($count. ' Pin-to-Top Ad(s) Expired!');
    }
}
