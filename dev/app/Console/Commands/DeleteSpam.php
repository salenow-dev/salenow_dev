<?php

namespace App\Console\Commands;

use App\Http\Controllers\Backend\AdsreviewController;
use App\Models\Alladsmaster;
use App\Models\Allelectrnicad;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DeleteSpam extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:deletespam';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $spams = DB::table('alladsmasters as mm')
//            ->leftJoin('categories as cat', 'mm.category_id', '=', 'cat.id')
//            ->where('mm.status', '=', 'cancel')
//            ->where('mm.updated_at', '<=', Carbon::now()->subDays(14)->toDateTimeString())
//            ->select('mm.id','cat.category_code as cat_ref')
//            ->get()->toArray();
//        $count = 0;
//        foreach ($spams as $spam) {
//            if($spam->id){
//                $arr = array(
//                    'id' => $spam->id,
//                    'table' => $spam->cat_ref,
//                );
//                $obj = new AdsreviewController();
//                $obj->deletepostad($arr);
//                $count ++;
//            }
//        }
//        dd($count. ' Records deleted');
    }
}
