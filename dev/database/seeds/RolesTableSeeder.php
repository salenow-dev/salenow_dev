<?php

use Illuminate\Database\Seeder;
use Kodeine\Acl\Models\Eloquent\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $roleAdmin = $role->create([
            'name' => 'Administrator',
            'slug' => 'admin',
            'description' => 'manage administration privileges'
        ]);

        $role = new Role();
        $roleModerator = $role->create([
            'name' => 'Moderator',
            'slug' => 'moderator',
            'description' => 'manage moderator privileges'
        ]);
    }
}
