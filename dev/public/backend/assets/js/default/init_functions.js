/**
 * Created by Niranjana on 10/21/2016.
 */

/*
 STATUS CHANGE
 <a href="javascript:;" class="statuschange" id="status_{{$result->id}}"
 uid="{{$result->id}}" status="{{$result->status}}" action="vmodel">
 {{$result->status}}
 </a>
 */
function statuschange() {
    $.ajaxSetup({
        headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')}
    });
    $('.statuschange').click(function () {
        var self = $(this);
        var id = self.attr("uid");
        var model = self.attr("action");
        var status = self.attr("status");
        var updatedStatus = (status == 'active') ? 'inactive' : 'active';
        $.ajax({
            url: '/default/statuschange',
            type: "post",
            data: 'id=' + id + '&status=' + updatedStatus + '&model=' + model,
            success: function (data) {
                if (data.response.message != '') {
                    self.html(updatedStatus);
                    self.attr('status', updatedStatus)
                    toastr["success"](data.response.message, "Success")
                } else {
                    toastr["danger"](data.response.error, "Danger")
                }
            }
        });
    });
}

/*
 RECORD DELETE
 <a href="javascript:;" class="delete" id="delete_{{$result->id}}" uid="{{$result->id}}" model="vmodel">
 <i class="fa fa-trash fa-1x" title="Delete"></i>
 </a>
 */
$('.delete').click(function () {
    var self = $(this);
    var id = self.attr("uid");
    var model = self.attr("tag");
    noty({
        text: 'Do you want to continue?',
        layout: 'topRight',
        buttons: [{
            addClass: 'btn btn-success btn-clean',
            text: 'Ok',
            onClick: function ($noty) {
                $noty.close();
                self.closest("tr").hide('slow');
                $.ajaxSetup({
                    headers: {'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')}
                });
                $.ajax({
                    url: '/com/deleterecord',
                    type: "post",
                    data: 'id=' + id + '&model=' + model,
                    success: function (res) {
                        if (res.response.message == 'success') {
                            noty({
                                text: 'Record Delete Successfully!',
                                layout: 'topRight',
                                type: 'success',
                                timeout: 3000,
                            });
                            $(this).closest("tr").hide('slow');
                        } else {
                            noty({
                                text: 'Your file is safe :)',
                                layout: 'topRight',
                                type: 'error',
                                timeout: 3000,
                            });
                        }
                    }
                });
            }
        }, {
            addClass: 'btn btn-danger btn-clean',
            text: 'Cancel',
            onClick: function ($noty) {
                $noty.close();
                noty({
                    text: 'You clicked "Cancel" button',
                    layout: 'topRight',
                    type: 'error',
                    timeout: 3000,
                });
            }
        }
        ]
    })
});

$(document).ready(function () {
    setTimeout(function () {
        $('.alert').fadeOut('fast');
    }, 6000);
})

function blocbtn() {
    $('.blockui').block({message: msg});
}

function unblocbtn() {
    $('.blockui').unblock();
}

