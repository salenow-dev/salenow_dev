/**
 * Created by Niranjana on 11/1/2016.
 */
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })

    $("#frmRegister").validate({
        submitHandler: function () {
            submitInsertDynamic('frmRegister');
        }
    });
});
function submitInsertDynamic(form) {
    var URL = '';
    URL = System.root + $("#" + form).attr('action');

    var $form = $("#" + form);
    var formData = $form.serialize();
    insert(formData, URL);
}

function insert(formData, URL) {
    $.ajax({
        url: URL,
        type: 'POST',
        data: formData,
        success: function (result) {
            if (result.errors) {
                var li = '';
                $.each(result.errors, function (key, value) {
                    li += '<li>' + value + '</li>'
                });
                $('#error-message').html(li);
                // $('#error-div').show();
                $('#error-div').removeClass( "hide" );
                return false;
            }
            if (result.response != '') {
                if (result.response.message != '') {
                    toastr.success(result.response.message, 'Success');
                }
                if (result.response.redirect != null) {
                    var path = System.root + result.response.redirect;
                    location.href = path;
                }
                if (result.response.error == "error") {
                    toastr.error(result.response.error, 'Alert!');
                }
            }
        }
    });
}
