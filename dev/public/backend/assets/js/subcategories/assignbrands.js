/**
 * Created by Niranjana on 5/26/2017.
 */
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#btn_submit_signup').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
    $('#loading').hide();

    // $(document).ajaxStart(function () {
    //     blocbtn(null);
    //     $('#loading').removeClass('hide');
    // }).ajaxStop(function () {
    //     unblocbtn();
    //     $('#loading').addClass('hide');
    // });

    $('#subcategory_id').change(function (e) {
        e.preventDefault();
        var id = $(this).val();
        var data = {'id': id};
        $.ajax({
            type: "post",
            url: '/settings/subcategory/assignedbrands',
            data: data,
            success: function (res) {
                var data  = res.response;
                $("#brands-div").html(data.brands);
                // $("#brand_id").addClass('select');

                // $("select[name='brand_id']").html('');
                // $("select[name='brand_id']").html(data.brands);

                // $('#loading').hide();
                // if (typeof res.message != 'undefined' && res.url != '') {
                //     $('#error-div').remove();
                //     noty({
                //         text: res.message,
                //         layout: 'topRight',
                //         type: 'success',
                //         timeout: 3000,
                //     });
                //
                //     var delay = 1000;
                //     setTimeout(function () {
                //         window.location = res.url;
                //     }, delay);
                // }
            }
        });
    });
});