
$('#frm_login').validate({
    rules: {
            signin_username: "required",
            signin_password: "required",
            //signin_captcha: "required",
        },
        
        // Specify the validation error messages
        messages: {
            signin_username: "This field is required",
            signin_password: "This field is required",
            //signin_captcha: "This field is required",
        },
        
        submitHandler: function(form) {
         $.ajax({
            url: 'Home',
            type: 'POST',
            //async:false,
            dataType: 'json',
            data: $("#frm_login").serialize(),
            beforeSend: function(d){
                 $('#login_submit').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
            },
            success: function (data) {
                //console.log(data);
                //alert(JSON.stringify(data)); return;
                if (data.success) {
                    if (data.first_time == '1') {
                        $('.message').show();
                        $('#login-container').hide();
                        $('#signup-container').hide();
                        $('#reset-container').hide();
                        $('#change_password_container').show();
                        $('#signin_username_code').val(data.user_code);
                        $('#signin_username_code').attr('readonly', true);
                        $('.message').html('<strong>' + data.response + '</strong>');
                        $('#frm_login')[0].reset();
                        $("button[type='submit']").attr('disabled', false);
                    } else {
                        localStorage.setItem('role', data.role);
                        $('.message').hide();
                        //var url = window.location.href;
                        var url = data.response;
                         if(data.preuser == 'EC') {
                        window.location = url + 'emain_home';
                    }else {
                          window.location = url + 'main_home';
                    }
                       
                    }
                    $('#login_submit').text('LOG IN');

                }
                else {
                    $("button[type='submit']").attr('disabled', false);
                    $('#login_submit').text('LOG IN');
                    $('.message').show();
                    $('.message').html('<strong>' + data.response + '</strong>');
                }
            }


        });
        },
        errorPlacement: function (error, element) {
            
            error.appendTo(element.parent().next(".validation"));
           

        }
});
$(document).ready(function () {
    $('#register-form').validate({
        rules: {
           username: {
            required: true,
            maxlength: 15,
//            regex: /^[a-zA-Z0-9 ]+$/,
   remote:{
            type:"POST",
            url:'membercheck',
            dataType:'json',
            data:{'username':function(){
                $('#username').val();
            },ajax:'validate'},
            success:function(resp){

                $.each(resp,function(index,value){
                    if(index == "username")
                        return false;
                });

            }
        }
        },
            signup_email: {
                required: true,
                email: true
            },
            signup_phone: {
                minlength: 2,
                required: true
            }
        },
            messages: {
          username: {
            required: "Please enter a username",
         
            maxlength: "Your username should not exceed 15 characters.",
            remote: "This username is taken."
        },
            lastname: "Enter your Last Name",
            signup_email: {
                required: "Enter your Email",
                email: "Please enter a valid email address.",

            }
        },
        highlight: function (element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) {
            element.text('OK!').addClass('valid')
                .closest('.control-group').removeClass('error').addClass('success');
        }
    });
   });
//$('#register-form').validate({
//
//
//                focusInvalid: false, 
//                ignore: "",
//                rules: {
//                    signup_phone: {
//                        //required: true,
//                    },
//                    signup_nic_number: {
//                        //required: true,
//                        minlength : 10,
//                    },
//                    signup_given_name: {
//                        //required: true,
//                    },
//                    signin_number: {
//                        //required: true,
//                        number:true,
//                    },
//                    signin_email: {
//						//~ required: function(element) {
//							//~ return ($("#signin_number").is(':empty'));
//						//~ },
//                        email: true
//                    },
//                },
//
//                invalidHandler: function (event, validator) {
//					//display error alert on form submit    
//                },
//
//                errorPlacement: function (label, element) { // render error placement for each input type
//               		$('<span class="error"></span>').insertAfter($(element).parent('div')).append(label);
//                    var parent = $(element).parent('.input-with-icon');
//                    parent.removeClass('success-control').addClass('error-control');  
//                },
//
//                highlight: function (element) { // hightlight error inputs
//					
//                },
//
//                unhighlight: function (element) { // revert the change done by hightlight
//                    
//                },
//
//                success: function (label, element) {
//					
//					var parent = $(element).parent('.input-with-icon');
//					parent.removeClass('error-control').addClass('success-control'); 
//                },
//			    submitHandler: function(form) {
//						$("button[type='submit']").attr('disabled','disabled');
//						$('#btn_submit_signup').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
//					 $.ajax({
//								url : 'memberlogin',
//								type: 'POST',
//								//async:false,
//								dataType:'json',			
//								data:$("#register-form").serialize(),
//								success:function(data) {
//									//alert(JSON.stringify(data));
//									 if(data.success == true){
//									 	//localStorage.setItem('role',data.role);
//									 		$('.message').show();
//									 		$('.message').html('<strong>'+data.response+'</strong>');
//									 		//var url = window.location.href;
//											//var url = data.response;						
//										//window.location = url+'/public/Home';
//										//$('#btn_submit_reset').text('SIGN UP');
//											//$('#signup-container').hide();
//											//$('#reset-container').hide();
//											$('#register-form')[0].reset();
//											$('.show-login').click();
//											$("button[type='submit']").attr('disabled',false);
//											$('#btn_submit_signup').text('SIGN UP');
//											return false;
//																			 		
//										}else{
//											$("button[type='submit']").attr('disabled',false);
//											$('#btn_submit_signup').text('SIGN UP');
//											$('.message').show();
//											$('.message').html('<strong>'+data.response+'</strong>');
//										}
//								 }
//								
//
//				    });
//				}
//            });	


			
			
		
			
			
