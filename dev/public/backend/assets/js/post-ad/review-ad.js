/**
 * Created by Niranjana on 5/26/2017.
 */

$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    // $('#btn_submit_signup').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
    $('#loading').hide();

    $(".review_status").click(function (e) {
        e.preventDefault();
        $.blockUI();
        var status = $(this).attr('status');
        var id = $(this).attr('ad-id');
        var data = {'id': id, 'status': status};
        $.ajax({
            type: "PATCH",
            url: '/ads-manage/ad-review/' + id,
            data: data,
            success: function (res) {
                $.unblockUI();
                $('#loading').hide();
                if (typeof res.message != 'undefined' && res.url != '') {
                    $('#error-div').remove();
                    noty({
                        text: res.message,
                        layout: 'topRight',
                        type: 'success',
                        timeout: 3000,
                    });
                    var delay = 1000;
                    setTimeout(function () {
                        window.location = res.url;
                    }, delay);
                }
            }
        });
    });

    $(".login_superadmin").click(function (e) {
        e.preventDefault();
        var username = $(this).attr('username');
        var password = $(this).attr('password');
        var data = {'password': password, 'email': username};
        $.ajax({
            url: '/member_login',
            type: 'POST',
            async: true,
            dataType: 'json',
            data: data,
            success: function (res) {
                window.open(res.url,'_blank');
            }
        });
    });

    // spam post ad from backend
    $(".spam").click(function (e) {
        e.preventDefault();
        $.blockUI();
        var id = $("#ad_id").val();
        var formData = $("#form_spam").serialize();
        $.ajax({
            type: "PATCH",
            url: '/ads-manage/ad-review/' + id,
            data: formData,
            success: function (res) {
                $.unblockUI();
                if (res.errors) {
                    $('#message').html(res.errors);
                    $('#message').removeClass("alert-success");
                    $('#message').addClass("alert-danger");
                    $('#message').removeClass("hide");
                    $('#message').css('display', 'block');
                    return false;
                }
                if (typeof res.message != 'undefined' && res.url != '') {
                    $('#message').html(res.message);
                    $('#message').removeClass("alert-danger");
                    $('#message').addClass("alert-success");

                    var delay = 1500;
                    setTimeout(function () {
                        window.location = res.url;
                    }, delay);
                }
            }
        });
    });

    // delete post ad from backend
    $("#delete").click(function (e) {
        e.preventDefault();
        $.blockUI();
        $('#loading').show();
        var id = $(this).attr('ad-id');
        var data = {'id': id};
        // var data = $.param({'id': id, 'status': status});
        $.ajax({
            type: "post",
            url: '/ads-manage/ad-review/delete/' + id,
            data: data,
            success: function (res) {
                $.unblockUI();
                $('#loading').hide();
                if (res.errors) {
                    var li = '';
                    $.each(res.errors, function (key, value) {
                        li += '<li>' + value + '</li>'
                    });
                    $('#error-message').html(li);
                    $('#error-div').removeClass("hide");
                    $('html, body').animate({
                        scrollTop: $(".category-title").offset().top
                    }, 1000);

                    return false;
                }
                if (typeof res.message != 'undefined' && res.url != '') {
                    $('#error-div').remove();
                    noty({
                        text: res.message,
                        layout: 'topRight',
                        type: 'success',
                        timeout: 3000,
                    });

                    var delay = 1000;
                    setTimeout(function () {
                        window.location = res.url;
                    }, delay);
                }
            }
        });
    });
});

function notyConfirmBackend(id, table) {
    noty({
        text: 'Do you want to continue?',
        layout: 'topRight',
        buttons: [{
            addClass: 'btn btn-success btn-clean', text: 'Delete', onClick: function ($noty) {
                deleteRecordBacked(id, table);
                $noty.close();
            }
        },
            {
                addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function ($noty) {
                $noty.close();
            }
            }
        ]
    })
}


function deleteRecordBacked(id, table) {
    $.blockUI();
    var data = {'id': id, 'table': table};
    // var data = $.param({'id': id, 'status': status});
    $.ajax({
        type: "post",
        url: '/ads-manage/ad-review/delete/' + id,
        data: data,
        success: function (res) {
            $.unblockUI();
            $('#loading').hide();
            if (res.errors) {
                var li = '';
                $.each(res.errors, function (key, value) {
                    li += '<li>' + value + '</li>'
                });
                $('#error-message').html(li);
                $('#error-div').removeClass("hide");
                $('html, body').animate({
                    scrollTop: $(".category-title").offset().top
                }, 1000);

                return false;
            }
            if (typeof res.message != 'undefined' && res.url != '') {
                $('#error-div').remove();
                noty({
                    text: res.message,
                    layout: 'topRight',
                    type: 'success',
                    timeout: 3000,
                });

                var delay = 1000;
                setTimeout(function () {
                    window.location = res.url;
                }, delay);
            }
        }
    });
}
