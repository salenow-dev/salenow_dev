/**
 * Created by Salinda on 12/23/2017.
 */

$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

});

function notyConfirmBackend(id, table) {
    noty({
        text: 'Do you want to Delete this ad Permanatly?',
        layout: 'topRight',
        buttons: [{
            addClass: 'btn btn-success btn-clean', text: 'Delete', onClick: function ($noty) {
                deleteRecordBacked(id, table);
                $noty.close();
            }
        },
            {
                addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function ($noty) {
                $noty.close();
            }
            }
        ]
    })
}

function deleteRecordBacked(id, table) {
    var data = {'id': id, 'table': table};
    // var data = $.param({'id': id, 'status': status});
    $.ajax({
        type: "post",
        url: '/adsmanage/adreview/delete-permanatly/' + id,
        data: data,
        success: function (res) {
            if (res.errors) {
                var li = '';
                $.each(res.errors, function (key, value) {
                    li += '<li>' + value + '</li>'
                });
                $('#error-message').html(li);
                $('#error-div').removeClass("hide");
                $('html, body').animate({
                    scrollTop: $(".category-title").offset().top
                }, 1000);

                return false;
            }
            if (typeof res.message != 'undefined' && res.url != '') {
                $('#error-div').remove();
                noty({
                    text: res.message,
                    layout: 'topRight',
                    type: 'success',
                    timeout: 3000,
                });

                var delay = 1000;
                setTimeout(function () {
                    // window.location = res.url;
                    window.location = '/ads-manage/ad-review/deleted-ads';
                }, delay);
            }
        }
    });
}
// ----------------republish---------------
function notyRepublishBackend(id) {
    noty({
        text: 'Do you want to republish this ad?',
        layout: 'topRight',
        buttons: [{
            addClass: 'btn btn-success btn-clean', text: 'Republish', onClick: function ($noty) {
                RepublishRecordBacked(id);
                $noty.close();
            }
        },
            {
                addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function ($noty) {
                $noty.close();
            }
            }
        ]
    })
}
function RepublishRecordBacked(id) {
    var data = {'id': id};
    $.ajax({
        type: "post",
        url: '/adsmanage/adreview/deletead-republish/' + id,
        data: data,
        success: function (res) {
            if (res.errors) {
                var li = '';
                $.each(res.errors, function (key, value) {
                    li += '<li>' + value + '</li>'
                });
                $('#error-message').html(li);
                $('#error-div').removeClass("hide");
                $('html, body').animate({
                    scrollTop: $(".category-title").offset().top
                }, 1000);

                return false;
            }
            if (typeof res.message != 'undefined' && res.url != '') {
                $('#error-div').remove();
                noty({
                    text: res.message,
                    layout: 'topRight',
                    type: 'success',
                    timeout: 3000,
                });

                var delay = 1000;
                setTimeout(function () {
                    // window.location = res.url;
                    window.location = res.url;
                }, delay);
            }
        }
    });
}
