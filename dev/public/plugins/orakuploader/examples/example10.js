$(document).ready(function(){
	$('#images10ex').orakuploader({
		orakuploader_path : 'orakuploader/',
		
		orakuploader_main_path : 'files',
		orakuploader_thumbnail_path : 'files/tn',
		
		orakuploader_add_image : 'orakuploader/images/add.png',
		orakuploader_add_label : 'Browser for images',
		
		orakuploader_use_rotation: true,
		orakuploader_use_sortable : true,
		
		orakuploader_resize_to : 700,
		orakuploader_thumbnail_size : 220
	});
});