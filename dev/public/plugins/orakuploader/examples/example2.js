$(document).ready(function(){
	$('#images2ex').orakuploader({
		orakuploader : true,
		orakuploader_path : 'orakuploader/',
	
		orakuploader_main_path : 'files',
		orakuploader_thumbnail_path : 'files/tn',
		
		orakuploader_use_main : false,
		orakuploader_use_sortable : false,
		orakuploader_use_dragndrop : true,
		
		orakuploader_add_image : 'orakuploader/images/add.png',
		orakuploader_add_label : 'Browser for images',
		
		orakuploader_resize_to : 600,
		orakuploader_thumbnail_size : 150
	});
});