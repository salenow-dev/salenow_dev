$(document).ready(function(){
	$('#images11ex').orakuploader({
		orakuploader : true,
		orakuploader_path : 'orakuploader/',
	
		orakuploader_main_path : 'files',
		orakuploader_thumbnail_path : 'files/tn',
		
		orakuploader_use_dragndrop : true,
		
		orakuploader_add_image : 'orakuploader/images/add.png',
		orakuploader_add_label : 'Browser for images',
		
		orakuploader_crop_to_width: 600,
		orakuploader_crop_to_height: 420,
		
		orakuploader_crop_thumb_to_width: 135,
		orakuploader_crop_thumb_to_height: 100
	});
});