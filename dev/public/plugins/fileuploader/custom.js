$(document).ready(function() {
    var dbImages=[];
    if( imageData != "empty") {
        dbImages = imageData;
    }
    var adId = $("#adId").val();
	// enable fileuploader plugin
	$('input[name="files"]').fileuploader({
        extensions: ['jpg', 'jpeg', 'png'],
		changeInput: ' ',
		limit:5,
		theme: 'thumbnails',
        enableApi: true,
		addMore: true,
        maxSize: 30,
        fileMaxSize: 5,
		files: dbImages,
		thumbnails: {
			box: '<div class="fileuploader-items">' +
                      '<ul class="fileuploader-items-list" id="imageuploader">' +
					      '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner"><i class="ion-plus-circled"></i></div></li>' +
                      '</ul>' +
                  '</div>',
			item: '<li class="fileuploader-item">' +
				       '<div class="fileuploader-item-inner">' +
                           '<div class="thumbnail-holder">${image}</div>' +
                           '<div class="actions-holder">' +
                               '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
                           '</div>' +
                       	   '<div class="progress-holder">${progressBar}</div>' +
                       '</div>' +
                   '</li>',
			item2: '<li class="fileuploader-item">' +
				       '<div class="fileuploader-item-inner">' +
                           '<div class="thumbnail-holder">${image}</div>' +
                           '<div class="actions-holder">' +
                               '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
                           '</div>' +
                       '</div>' +
                   '</li>',
            removeConfirmation: false,
			startImageRenderer: true,
			canvasImage: false,
			_selectors: {
				list: '.fileuploader-items-list',
				item: '.fileuploader-item',
				start: '.fileuploader-action-start',
				retry: '.fileuploader-action-retry',
				remove: '.fileuploader-action-remove'
			},
			onItemShow: function(item, listEl) {
				var plusInput = listEl.find('.fileuploader-thumbnails-input');
				plusInput.insertAfter(item.html);
				if(item.format == 'image') {
					item.html.find('.fileuploader-item-icon').hide();
				}
			},
            onItemRemove: function(html) {
                html.children().animate({'opacity': 0}, 200, function() {
                    setTimeout(function() {
                        html.slideUp(200, function() {
                            html.remove();
                        });
                    }, 50);
                });
            },
		},
		afterRender: function(listEl, parentEl, newInputEl, inputEl) {
			var plusInput = listEl.find('.fileuploader-thumbnails-input'),
				api = $.fileuploader.getInstance(inputEl.get(0));

			plusInput.on('click', function() {
				api.open();
			});
		},
		onRemove: function(item) {
			$.post('/ad/deleteimage', {
				id: item.data.id
			});
        },
        upload: {
            url: '/salemeupload/basicimagestore',
            data: null,
            type: 'POST',
            enctype: 'multipart/form-data',
            start: true,
            synchron: true,
            beforeSend: null,
            onSuccess: function(data, item) {
                setTimeout(function() {
                    item.html.find('.progress-holder').hide();
                    item.renderImage();
                }, 400);
            },
            onError: function(item) {
                item.html.find('.progress-holder').hide();
                item.html.find('.fileuploader-item-icon i').text('Failed!');

                setTimeout(function() {
                    item.remove();
                }, 1500);
            },
            onProgress: function(data, item) {
                var progressBar = item.html.find('.progress-holder');

                if(progressBar.length > 0) {
                    progressBar.show();
                    progressBar.find('.fileuploader-progressbar .bar').width(data.percentage + "%");
                }
            }
        },

    });
});

// function getfeturedimage() {
//     // $( "ul#imageuploader li" ).each(function( index ) {
//     //     console.log( index + ": " + $( this ).text() );
//     // });
//     if( $('ul#imageuploader li img:first').length){
//         // var featured = $('ul#imageuploader li img:first').attr( "imagename" );
//         console.log(featured);
//         $('#featuredimage').val(featured);
//     }
// }