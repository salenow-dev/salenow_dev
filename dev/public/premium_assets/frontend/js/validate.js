$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $("#contact_form").validate({
        ignore: ":hidden",
        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
            },
            phone: {
                required: true,
            },
            subject: {
                required: true,
            },
            message: {
                required: true,
            }
        },
        messages: {
            name: {
                required: "Please fill your Name",
            },
            email: {
                required: "Please fill your Email address",
            },
            phone: {
                required: "Please fill your Phone Number",
            },
            subject: {
                required: "Please fill Subject",
            },
            message: {
                required: "Please fill Message",
            }
        },
        submitHandler: function (form) {
            form.preventDefault();
            var formdata = $("#contact_form").serializeArray();
            alert(formdata);
            $.ajax({
                type: 'POST',
                url: '/premiummember/sendinquiryemail',
                data: formdata,
                beforeSend: function (d) {
                    $('#contact_form').block({
                        message: '<h5>Sending Message</h5>',
                        css: {border: '1px solid #fff',
                            'border-radius':'2px',
                        'padding': '3px'}
                    });
                },
                success: function (res) {
                    $('#contact_form').unblock();
                    $('#responce').show();
                    setTimeout(function() {
                        location.reload();
                    },1000);
                }
            });
        }
    });




});
