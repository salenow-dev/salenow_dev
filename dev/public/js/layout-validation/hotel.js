/**
 * Created by Niranjana on 5/26/2017.
 */
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $("#from-submit").validate({
        ignore: ":hidden:not(.selectpicker)",
        rules: {
            district_id: {
                required: true,
            },
            city_id: {
                required: true,
            },
            adtitle: {
                required: true,
            },
            description: {
                required: true,
            },
            price: {
                required: true,
                // notEqual: "0"
                // min: 1
            },
            numberofguest: {
                required: true,
            },
            bedsize: {
                required: true,
            },
            beds: {
                required: true,
            },
            bathrooms: {
                required: true,
            },
            bathroomtypes: {
                required: true,
            },
            number_of_rooms: {
                required: true,
            },

        },
        messages: {
            district_id: {
                required: "Please Select District",
            },
            city_id: {
                required: "Please Select City ",
            },
            adtitle: {
                required: "You must fill out Title of Hotel / Room",
            },
            description: {
                required: "You must fill out description of Hotel / Room",
            },
            price: {
                required: "Enter price for your ad.",
            },
            numberofguest: {
                required: "Enter maximum nuber of guests",
            },
            bedsize: {
                required: "please Select Bed Size",
            },
            beds: {
                required: "Select Number of Beds",
            },
            bathrooms: {
                required: "select Number of Bathrooms",
            },
            bathroomtypes: {
                required: "select Bathroom types",
            },
            number_of_rooms: {
                required: "select Number of Rooms",
            }
        },
        submitHandler: function (form) {
            var myform = document.getElementById("from-submit");
            var formData = new FormData(myform);
            submitHandlerForAjax(formData);
        }
    });
    jQuery.validator.addMethod("notEqual", function (value, element, param) {
        return this.optional(element) || value != param;
    }, "Price Can not be Zero (0)");
});
