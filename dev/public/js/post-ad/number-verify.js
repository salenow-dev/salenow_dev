$(document).ready(function () {
    //$('#verifynumber').hide();
    //$('#message').hide();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#formNumberVerify").validate({
        rules: {
            number: {
                required: true,
                number: true,
                minlength: 9,
                maxlength: 10,
            },
        },
        messages: {
            number: {
                required: "Please enter a valid number!",
            }
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: '/verifynumber',
                data: $("#formNumberVerify").serializeArray(),
                beforeSend: function (d) {
                    $.blockUI({message: "Please Wait..."});
                },
                success: function (data) {
                    $.unblockUI();
                    $('#message').html('');
                    var info = data.response

                    // $('#message').html(info.message);
                    // $('#message').toggle(200);
                    location.reload();
                    if (info.message == 'success') {
                        $('#message').removeClass('hide alert-danger');
                        $('#message').addClass('show alert-success');
                        $('#message').html('<i class="fa fa-thumbs-up"></i>&nbsp;<strong>OTP PIN-Code sent Successfully! </strong> Please check your mobile. If you did not get PIN, Try again in few seconds.');


                        //hide send otp fields
                        setTimeout(function () {
                            $("#verify-div").addClass('hide').removeClass('show');
                            $('#verifynumber').addClass('show').removeClass('hide');
                            $('#resend').val(info.number_for_resend);
                            $('#otp_number').html(info.number_for_resend);

                            setTimeout(function () {
                                $('#resendpinnumber').toggleClass("hide");
                            }, 8000);
                        }, 1000);


                    }
                    //check if sms push a error code
                    if (info.message == 'error-code') {
                        $('#message').html('');
                        $('#message').removeClass('hide alert-success');
                        $('#message').addClass('show alert-danger');
                        $('#message').html('<i class="fa fa-exclamation-circle"></i> ' + 'SORRY! Something went wrong. Try again.');
                    }
                    //check if sms push a error code
                    if (info.message == 'error') {
                        $('#message').html('');
                        $('#message').removeClass('hide alert-success');
                        $('#message').addClass('show alert-danger');
                        $('#message').html('<i class="fa fa-exclamation-circle"></i> ' + info.error);
                    }

                    // if (info.error != '') {
                    //     $('#message').html('<i class="fa fa-exclamation-circle"></i> '+info.error);
                    // }
                    $('#message').delay(8000).fadeOut("slow");
                }
            })
        }
    });

    //verify number
    $("#veryfyotp").validate({
        rules: {
            number_verify: {
                required: true,
                number: true,
                maxlength: 5,
            },
        },
        messages: {
            number_verify: {
                required: "Please enter a valid number!",
            }
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: '/verifynumber',
                data: $("#veryfyotp").serializeArray(),
                beforeSend: function (d) {
                    $('#verifynumber').block({message: null});
                },
                success: function (data) {
                    $('#verifynumber').unblock();
                    var info = data.response;
                    if (info.message == 'verified') {
                        $("#otpmessage").addClass('show alert-success').removeClass('hide alert-danger');
                        $('#otpmessage').html('<i class="fa fa-thumbs-up"></i>&nbsp;<strong>Number VERIFIED!</strong> & Added to the Profile.');
                        window.setTimeout(function () {
                            window.location.href = "/post-ad/verify-number?id=" + info.ad_id;
                        }, 500);
                    }

                    //check if sms push a error code
                    if (info.message == 'error') {
                        $("#otpmessage").addClass('show alert-danger').removeClass('hide alert-success');
                        $('#otpmessage').html('<i class="fa fa-exclamation-circle"></i> ' + info.error);
                    }
                }
            })
        }
    });

});
$('#numberSubmit').on('click', function (e) {
    e.preventDefault();
    $('#formNumberVerify').submit();
});

$('#verify').on('click', function (e) {
    e.preventDefault();
    $('#veryfyotp').submit();
});

//
function resendPIN() {
    var resendNumber = $('#resend').val();
    var data = {resend: resendNumber}
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "POST",
        url: '/verifynumber',
        beforeSend: function (d) {
            $.blockUI({message: "Please Wait..."});
        },
        data: $("#formNumberVerify").serializeArray(),
        success: function (data) {
            $.unblockUI();
            var info = data.response
            if (info.message == 'success') {
                $("#otpmessage").addClass('show alert-success').removeClass('hide alert-danger');
                $('#otpmessage').html('OTP PIN-Code Resend Successfully! Please check your mobile. If you did not get PIN, Try again in 15 seconds.');

                //$('#newnumber').val('');
                //$('#newnumber').hide();
                $('#resend').val(info.number_for_resend);
                $('#otpmessage').delay(8000).fadeToggle();
            }
            if (info.message == 'error') {
                $("#otpmessage").addClass('show alert-danger').removeClass('hide alert-success');
                $('#otpmessage').html('');
                $('#otpmessage').html(info.error);
                $('#resend').val('');
            }
        }
    });
}

//
// function reEnterNumber(){
//     var resendNumber = $('#wrong_').val();
//     var data = {wrongNumberResend: resendNumber}
//     $.ajaxSetup({
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         }
//     });
//     $.ajax({
//         type: "POST",
//         url: '/verifynumber',
//         beforeSend: function (d) {
//             $('#wrong_number').block({message: null});
//         },
//         data: data,
//         success: function (data) {
//             $('#wrong_number').unblock();
//             var info = data.response
//             if (info.message == 'success') {
//                 $('#verifynumber').show();
//                 $('#message').html('OTP PIN-Code Resend Successfully! Please check your mobile. If you did not get PIN, Try again in 30 seconds.');
//                 $('#newnumber').val('');
//                 $('#newnumber').hide();
//                 $('#resend').val(info.number_for_resend);
//                 $('#message').toggle(200);
//                 $('#message').delay(10000).fadeToggle();
//             }
//             if (info.message == 'error') {
//                 $('#message').html('');
//                 $('#message').html(info.error);
//                 $('#message').toggle(200);
//                 $('#resend').val('');
//             }
//         }
//     });
// }
//
