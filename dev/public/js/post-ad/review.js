/**
 * Created by Niranjana on 10/28/2017.
 */
/**
 * Created by Niranjana on 5/26/2017.
 */
$(document).ready(function () {
    jQuery.validator.setDefaults({
        errorPlacement: function(error, element) {
            error.appendTo('#error-message');
        }
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#btn_submit_signup').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
    $("#verify-number-submit").validate({
        //ignore: ":hidden:not(.selectpicker)",
        rules: {
            'contactnumber[]': {
                required: true,
            },
            'nonumber': {
                required: true,
            }
        },
        messages: {
            'contactnumber[]': {
                required: "Please Select at least One Contact Number !",
            },
            'nonumber': {
                required: "You do not have any Contact Numbers. Please Add new number and continue!",
            }
        },
        submitHandler: function (form) {
            //alert('ddd');
            var myform = document.getElementById("verify-number-submit");
            var formData = new FormData(myform);
            $.ajax({
                type: "POST",
                url: '/post-ad/post',
                async: true,
                cache: false,
                contentType: false,
                processData: false,
                data: formData,
                beforeSend: function () {
                    $.blockUI({message: '<h1>Just a moment...</h1>'});
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    if (data.errors) {
                        $.unblockUI();
                        if('featuredImage' in data.errors){
                            if(data.errors.featuredImage != ''){
                                $("#dropzone_previews").addClass('featured-image-error');
                                $('#error-message-image').css('color','#CC0000');
                                $('#error-message-image').html(data.errors.featuredImage);
                                $('html, body').animate({
                                    scrollTop: $("#image-upload").offset().top
                                }, 500);
                                return false;
                            }
                        }

                        var li = '';
                        $.each(data.errors, function (key, value) {
                            li += '<li>' + value + '</li>'
                        });
                        $('#error-message').html(li);
                        $('#error-div').removeClass("hide");
                        $('html, body').animate({
                            scrollTop: $(".category-title").offset().top
                        }, 1000);

                        return false;
                    }
                    if (typeof data.url != 'undefined' && data.url != '') {
                        $('#error-div').remove();
                        var url = data.url;
                        window.location = url + '?ref=' + data.referance;
                    }
                }
            });
        }
    });
    jQuery.validator.addMethod("notEqual", function (value, element, param) {
        return this.optional(element) || value != param;
    }, "Price Can not be Zero (0)");

});
