/**
 * Created by Niranjana on 5/26/2017.
 */
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#btn_submit_signup').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
    $("#from-submit").validate({
        ignore: ":hidden:not(.selectpicker)",
        rules: {
            district_id: {
                required: true,
            },
            city_id: {
                required: true,
            },
            adtitle: {
                required: true,
            },
            description: {
                required: true,
            },
            price: {
                required: true,
                notEqual: "0"
                // min: 1
            },
            itemtype: {
                required: true,
            },
            landunit: {
                required: true,
            },
            landsize: {
                required: true,
                min: 1
            },
        },
        messages: {
            location_district: {
                required: "Please Select District",
            },
            location_city: {
                required: "Please Select City",
            },
            adtitle: {
                required: "You must fill othe Title for your Ad.",
            },
            description: {
                required: "You must fill out this field.",
            },
            price: {
                required: "Price cannot be 0.",
            },
            itemtype: {
                required: "Please Select Land Type.",
            },
            landunit: {
                required: "Please Select Land Unit",
            },
            landsize: {
                required: "Please fill Land Size",
            },


        },
        submitHandler: function (form) {
            var formData = new FormData($("form")[0]);
            submitHandlerForAjax(formData);
        }
    });
    jQuery.validator.addMethod("notEqual", function(value, element, param) {
        return this.optional(element) || value != param;
    }, "Price Can not be Zero (0)");
});