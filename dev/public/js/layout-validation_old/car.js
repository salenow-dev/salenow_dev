/**
 * Created by Niranjana on 5/26/2017.
 */
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $("#from-submit").validate({
        ignore: ":hidden:not(.selectpicker)",
        rules: {
            district_id: {
                required: true,
            },
            city_id: {
                required: true,
            },
            adtitle: {
                required: true,
            },
            description: {
                required: true,
            },
            price: {
                required: true,
                notEqual: "0"
                // min: 1
            },
            itemcondition: {
                required: true,
            },
            brand_id: {
                required: true,
            },
            registry_year: {
                required: true,
            },
            tranmission_id: {
                required: true,
            },
            capacity: {
                required: true,
            },
            fuel_id: {
                required: true,
            },
            mileage: {
                required: true,
                min: 0
            },
            model: {
                required: true
            },
            enginesize: {
                required: true
            },
            bodytype_id: {
                required: true
            }
        },
        messages: {
            district_id: {
                required: "Please Select District",
            },
            city_id: {
                required: "Please Select City",
            },
            adtitle: {
                required: "You must fill out the Title for your Ad.",
            },
            description: {
                required: "You must fill out description for your Ad..",
            },
            price: {
                required: "Price cannot be 0.",
            },
            itemcondition: {
                required: "please select condition.",
            },
            brand_id: {
                required: "You must fill out Brand field.",
            },
            registry_year: {
                required: "You must fill out vehicale year field.",
            },
            tranmission_id: {
                required: "please select tansmission type .",
            },
            capacity: {
                required: "please fill engine capacity.",
            },
            mileage: {
                required: "please fill mileage.",
            },
            fuel_id: {
                required: "please select fuel type.",
            },
            model: {
                required: "please enter model",
            },
            enginesize: {
                required: "please enter engine capacity",
            },
            bodytype_id: {
                required: "please select body type",
            }
        },
        submitHandler: function (form) {
            var formData = new FormData($("form")[0]);
            submitHandlerForAjax(formData);
        }
    });
    jQuery.validator.addMethod("notEqual", function(value, element, param) {
        return this.optional(element) || value != param;
    }, "Price Can not be Zero (0)");
});
