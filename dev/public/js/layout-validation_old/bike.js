/**
 * Created by Niranjana on 5/26/2017.
 */
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#btn_submit_signup').html('<i class="fa fa-spinner fa-pulse"></i> Loading');
    $("#from-submit").validate({
        ignore: ":hidden:not(.selectpicker)",
        rules: {
            district_id: {
                required: true,
            },
            city_id: {
                required: true,
            },
            adtitle: {
                required: true,
            },
            description: {
                required: true,
            },
            price: {
                required: true,
                notEqual: "0"
            },
            itemcondition: {
                required: true,
            },
            brand_id: {
                required: true,
            },
            model: {
                required: true,
            },
            registry_year: {
                required: true,
            },
            mileage: {
                required: true,

            },
            enginesize: {
                required: true,
                min: 1
            },
        },
        messages: {
            district_id: {
                required: "Please Select District",
            },
            city_id: {
                required: "Please Select City",
            },
            adtitle: {
                required: "Please fill out the Title for your Ad.",
            },
            description: {
                required: "Please fill out description for your Ad..",
            },
            price: {
                required: "Price cannot be 0.",
            },
            brand_id: {
                required: "Please select out Brand",
            },
            model: {
                required: "Please fill out model",
            },
            registry_year: {
                required: "Please select vehicale year",
            },
            enginesize: {
                required: "please fill engine capacity.",
            },
            mileage: {
                required: "please fill mileage.",
            },
            itemcondition: {
                required: "please select condition.",
            },
        },
        submitHandler: function (form) {
            var formData = new FormData($("form")[0]);
            submitHandlerForAjax(formData);
        }
    });
    jQuery.validator.addMethod("notEqual", function(value, element, param) {
        return this.optional(element) || value != param;
    }, "Price Can not be Zero (0)");
});