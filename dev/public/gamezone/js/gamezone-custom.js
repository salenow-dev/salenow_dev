/**
 * Created by Salinda on 4/26/2018.
 */

$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#dailypoint').click(function () {
        var data = {'point': 1};
        $.ajax({
            type: "post",
            url: '/playzone/mission/savedailypoint',
            data: data,
            success: function (res) {
                // alert(res.success);
                if (res.success) {
                    $('#dailypoint').addClass('isDisabled');
                    $('.daily_point_added').html('SaleMe Daily Point Added');
                }

                $('.alert_display').show().addClass(res.css);
                $('.icon').addClass(res.icon);
                $('.alert_display .msg').html(res.message);

                var delay = 1500;
                setTimeout(function () {
                    location.reload();
                }, delay);
            }
        });
    });
    $('#answer_btn').click(function () {
        var data = {'point': 1};
        $.ajax({
            type: "post",
            url: '/playzone/mission/savedailyquestion',
            data: data,
            success: function (res) {
                // alert(res.success);
                if (res.success) {
                    $('#dailypoint').addClass('isDisabled');
                    $('.daily_point_added').html('SaleMe Daily Point Added');
                }

                $('.alert_display').show().addClass(res.css);
                $('.icon').addClass(res.icon);
                $('.alert_display .msg').html(res.message);

                var delay = 1500;
                setTimeout(function () {
                    window.location='/playzone/home';
                }, delay);
            }
        });
    });
    $('#answer_btn_extra').click(function () {
        var data = {'point': 1};
        $.ajax({
            type: "post",
            url: '/playzone/mission/saveextraquestion',
            data: data,
            success: function (res) {
                // alert(res.success);
                if (res.success) {
                    $('#dailypoint').addClass('isDisabled');
                    $('.daily_point_added').html('SaleMe Daily Point Added');
                }

                $('.alert_display').show().addClass(res.css);
                $('.icon').addClass(res.icon);
                $('.alert_display .msg').html(res.message);

                var delay = 1500;
                setTimeout(function () {
                    window.location='/playzone/home';
                }, delay);
            }
        });
    });

});