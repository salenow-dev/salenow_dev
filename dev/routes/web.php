<?php
//Route::get('payment', 'PaymentController@payment');
//Route::post('payment_notify', 'PaymentController@payment_notify');
//Route::get('return', 'PaymentController@returnuri');
//Route::get('cancel', 'PaymentController@cancel');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*-----------------------------------------------------------------------------------------------------------
 * START frontend  routes
 -----------------------------------------------------------------------------------------------------------*/
//send inquiry
Route::post('premiummember/sendinquiryemail', 'Member\PremiummeberController@sendinquirymail'); //ad inquiry email
Route::group(['middleware' => 'timeout_boost'], function () {
    Route::get('post-ad/{category}/{sub_cat}/{type}', 'Member\VehicleController@postad');
    Route::get('post-job/{sub_cat}/new-job', 'Member\VehicleController@postJob');
    Route::get('', 'Home\DefaultsController@index');
//    Route::get('home', 'Home\DefaultsController@index');
//    Route::get('ads', 'Home\DefaultsController@allads');

    Route::get('', 'Home\DefaultsController@index');
    Route::get('home', 'Home\DefaultsController@index');
//    Route::get('ads', 'Home\DefaultsController@allads');
//    Route::get('', 'Home\DefaultsController@allads');
//    Route::get('home', 'Home\DefaultsController@allads');
    Route::get('ads', 'Home\DefaultsController@allads');
    Route::get('ad/{slug}', 'Home\DefaultsController@ad_details');

    //apply job
    Route::get('ad/{slug}/apply', 'Home\DefaultsController@applyJob');
    Route::post('applynow', 'Home\DefaultsController@applyNow');
    //END send inquiry
    /*-----------------------------------------------------------------------------------------------------------
     * START frontend  help and support
    -----------------------------------------------------------------------------------------------------------*/
    /*----------------------Career Front End Routes------------------------------------*/
    Route::get('careers', 'Backend\CareerController@index');
    Route::post('/careers/apply', 'Backend\CareerController@apply');
    Route::post('/career/applyform', 'Backend\CareerController@applyform');
    /*--------------------CareerPage Routes-------------------------------------------------------*/
    Route::get('membership', function () {
        return view('pages.membership');
    });

    Route::get('about', function () {
        return view('pages.about');
    });

    Route::get('membership', function () {
        return view('pages.membership');
    });

    Route::get('faq', function () {
        return view('pages.faq');
    });
    Route::get('stay-safe', function () {
        return view('pages.stay-safe');
    });
    Route::get('contact-us', function () {
        return view('pages.contact-us');
    });
    Route::get('help-support', function () {
        return view('pages.help');
    });
    Route::get('advertise-with-us', function () {
        return view('pages.advertise-with-us');
    });
    Route::get('sell-fast', function () {
        return view('pages.sell-fast');
    });
    Route::get('banner-advertising', function () {
        return view('pages.banner-ads');
    });
    Route::get('privacy-policy', function () {
        return view('pages.privacy');
    });
    Route::get('terms-conditions', function () {
        return view('pages.terms');
    });
    /*-----------------------------------------------------------------------------------------------------------
     * end frontend  help and support
     * START frontend  SEARCH
    -----------------------------------------------------------------------------------------------------------*/
    Route::get('ads/{location}', 'Home\DefaultsController@adslocation');
    Route::get('ads/{location}/{scat}', 'Home\DefaultsController@alladssearch');
    /*-----------------------------------------------------------------------------------------------------------
     * START frontend  SEARCH
    -----------------------------------------------------------------------------------------------------------*/
    // Member
    Route::post('verifynumber', 'Member\MemberController@verifynumber'); // verify number
    Route::post('memberregister', 'Member\MemberController@memberregister');
    Route::post('deletecontactno', 'Member\MemberajaxController@deletecontactno');
    Route::post('deleteemail', 'Member\MemberajaxController@deleteemailaddress');
    Route::post('updatepass', 'Member\MemberajaxController@memberpassword');
    Route::post('memberupdate', 'Member\MemberajaxController@memberupdate');
    Route::post('member/sendinquirymail', 'Member\MemberController@sendinquirymail'); //ad inquiry email

    //post new ad
    Route::get('reviewsuccess', 'Member\VehicleController@reviewsuccess');
    Route::get('selectcat', 'Member\MemberController@selectcategory');
    Route::get('addtype', 'Member\MemberController@addtype');
    Route::post('citylist', 'Member\VehicleController@citybydistriclist');
    Route::post('vehicelpost', 'Member\VehicleController@vehicelpost');

    //START social media login
    Route::get('{provider}/authorize', 'MemberAuth\LoginController@redirectToProvider');
    Route::get('sociallogin/{provider}', 'MemberAuth\LoginController@handleProviderCallback');
    //END social media login

    Route::post('ad/email-to-friend', 'Member\MemberajaxController@sendtofriend');
});


/***********************************************************************************************************
 *  Member Routes...
 * Logged in users/member cannot access or send requests these pages
 ***********************************************************************************************************/
Route::group(['middleware' => 'member_guest'], function () {
    Route::get('member_login', 'MemberAuth\LoginController@showLoginForm');
    Route::post('member_login', 'MemberAuth\LoginController@login');
    Route::get('member-login', 'MemberAuth\LoginController@loginpage');
    Route::get('member/register', 'MemberAuth\LoginController@registerpage');
    Route::post('memberlogin', 'MemberAuth\LoginController@login');
});

//Only logged in members can access or send requests to these pages
Route::group(['middleware' => 'member_auth'], function () {
    Route::get('member_logout', 'MemberAuth\LoginController@logout');
    Route::get('member_logout_playzone', 'MemberAuth\LoginController@logout_playzone');
    Route::get('/member_home', function () {
        return view('member.home');
    });
    Route::get('my_ads', 'Member\MemberController@memberads');
    Route::get('myads/search', 'Member\MemberController@searchmyads');
    Route::get('myprofile', 'Member\MemberController@memberprofile');
    Route::get('wishlist', 'Member\MemberController@wishlist');

    //START Employee Routes
    Route::get('{company}/manage', 'Member\PremiummeberController@managepremiumprofile');
    Route::get('{company}/team', 'Member\PremiummeberController@companyteam');
    Route::get('employee/{employee}/edit', 'Member\PremiummeberController@manageemployee'); //manage employee type, designation
    Route::post('employee/update/{employee}', 'Member\PremiummeberController@manageemployeeupdate'); //manage employee type, designation
    Route::post('company/manage', 'Member\PremiummeberController@managepremiumprofile');
    Route::post('premium/memberassign', 'Member\PremiummeberController@assignmembertocompany');
    //END Employee Routes

//    member location
    Route::get('{company}/location', 'Member\PremiummeberController@member_location');
    Route::get('{company}/about', 'Member\PremiummeberController@about_company');

    Route::post('/service/delete/{delete_ad}', 'Member\PremiummeberController@deleteservice');

    Route::post('ad/deleteimage', 'Member\MemberajaxController@deleteimages');
    Route::post('adsmanage/adreview/delete/{delete_ad}', 'Backend\AdsreviewController@deletepostad');
    Route::post('adsmanage/adreview-front/delete/{delete_ad}', 'Backend\AdsreviewController@deletePostAdWithResons');
    Route::post('adsmanage/adreview/delete-permanatly/{delete_ad}', 'Backend\AdsreviewController@deletepostadPermanatly');
    Route::post('adsmanage/adreview/deletead-republish/{delete_ad}', 'Backend\AdsreviewController@deletead_repblish');
    //START ad boosting options
    Route::get('/boosting/packages', 'Member\PintotopController@pintotopboostpack');
    Route::get('myprofile/adpacks', 'Member\PremiummeberController@adpacks');
    Route::get('myprofile/{ad}/{boosttype}', 'Member\PintotopController@boostad');
    Route::post('myprofile/pin-to-top', 'Member\PintotopController@boostpintotopreqest');
    //END ad boosting options
    Route::post('makepublic/{member_id}', 'Member\PremiummeberController@makepublic');
    Route::post('change_theme/{member_id}', 'Member\PremiummeberController@change_theme');

    //slack
    Route::get('feed/slack', 'Member\VehicleController@slack');

    //premium member
    Route::resource('premium', 'Member\PremiummeberController', ['except' => ['index', 'show', 'destroy']]);
    Route::post('premium/cover/update/{member}', 'Member\PremiummeberController@updatecover');
    Route::post('premium/profile_image/update/{member}', 'Member\PremiummeberController@updateprofile_image');
    //update location
    Route::post('premium/contact/delete/{contact}', 'Member\PremiummeberController@deletecontact');
    Route::post('premium/location/update/{member}', 'Member\PremiummeberController@updatelocation');
    //premium services
    Route::get('{company}/services-add', 'Member\PremiummeberController@services_add');
    Route::post('premium/services/store', 'Member\PremiummeberController@storeservices');
    Route::get('premium/services/{service}/edit', 'Member\PremiummeberController@editservice');
    Route::post('premium/services/{service}', 'Member\PremiummeberController@updateservice');
    Route::post('/service/delete/{delete_ad}', 'Member\PremiummeberController@deleteservice');

    //premium gallery
    Route::get('{company}/gallery-add', 'Member\PremiummeberController@add_gallery');
    Route::post('premium/gallery-add/store', 'Member\PremiummeberController@storegallery');
    Route::post('/image/delete/{image_id}', 'Member\PremiummeberController@deleteimage');
    Route::post('/setfeaturedimage/{image_id}', 'Member\PremiummeberController@setfeaturedimage');

//    save facebook page url
    Route::post('/facebookpage/save', 'Member\PremiummeberController@facebookPageAave');
    Route::post('/website/save', 'Member\PremiummeberController@websiteSave');
    Route::post('/company-name/save', 'Member\PremiummeberController@saveCompanyNameAndSlug');

    //OTP
    Route::post('delete_otp/{otp}', 'Member\MemberajaxController@deleteotpnumber');
    Route::post('verifynumberMember', 'Member\MemberajaxController@verifynumbermember'); // verify number

    //save temp images and main images
    Route::post('salemeupload/store', 'Member\VehicleController@dropzoneStore');
    Route::post('/salemeupload/storejob', 'Member\VehicleController@dropzoneStoreJob');
    Route::post('salemeupload/featured/{tempfeaturedimage}', 'Member\VehicleController@tempfeaturedselect');
    Route::post('salemeupload/delete/{tempimage}', 'Member\VehicleController@tempDelete');
    Route::post('uploadimages/delete/{tempimage}', 'Member\VehicleController@deleteFromAdImages');

    //post ad
    Route::get('post-ad/verify-number', 'Member\VehicleController@verifynumber');
    Route::post('post-ad/post', 'Member\VehicleController@updatedraftad');

    Route::post('myads/favorite/{id}', 'Member\MemberController@makefavorite');

//    reorder phone
    Route::post('premium/contact/reorder/{id}', 'Member\PremiummeberController@reorderContacts');
    Route::post('premium/contact/edit-save/{id}', 'Member\PremiummeberController@ContactEditSave');
    Route::post('premium/contact/delete-single/{id}', 'Member\PremiummeberController@DeleteSingleContact');

//    edit description
    Route::post('premium/description/save/{id}', 'Member\PremiummeberController@saveDescription');
});

Route::get('ad/edit/{slug}', 'Member\MemberController@editpostad'); //will redirect to home if member isn'y logged in
//Member Password Reset Routes...
Route::get('member/password/reset', 'Member\MemberForgotPasswordController@showLinkRequestForm');
Route::post('member/password/email', 'Member\MemberForgotPasswordController@sendResetLinkEmail');
Route::get('member/password/reset/{token}', 'Member\MemberResetPasswordController@showResetForm');
Route::post('member/password/reset', 'Member\MemberResetPasswordController@reset');

Route::get('salenow-admin-dash-login', 'Auth\LoginController@showLoginForm')->name('salemedmx');
Route::get('dashboard', 'Backend\DashboardController@dashboard');
Auth::routes();

// member activation
Route::get('member/activate/{token}', 'Member\MemberController@memberactivation');

//premium menber front end
Route::get('{companyslug}', 'Member\PremiummeberController@shop'); //view premium member shop view
Route::get('{companyslug}/ads', 'Member\PremiummeberController@allads'); //view premium member shop view
Route::get('{companyslug}/contact', 'Member\PremiummeberController@contact'); //view premium member shop view
Route::get('{companyslug}/gallery', 'Member\PremiummeberController@gallery'); //view premium member shop view
Route::get('{companyslug}/services', 'Member\PremiummeberController@services'); //view premium member shop view

Route::get('members/premium', 'Member\PremiummeberController@premiummember_list'); //view premium members list

//error page
Route::get('error', function () {
    return view('errors.error_page');
});
/*********************************************************************************************************************************
 * | END Member Routes...
 * | END frontend  routes
 * |
 * | START BACKEND  routes
 **********************************************************************************************************************************/


//common routes
Route::post('com/statuschange', 'Backend\DefaultsController@statuschange');
Route::post('com/deleterecord', 'Backend\CommonController@deleterecord');
Route::post('visitor-click', 'Home\DefaultsController@visitorclisks');
Route::get('user/{user}/edit', 'Auth\RegisterController@myaccount');

Route::group(['prefix' => 'user',
    'middleware' => ['auth', 'acl'],
    'is' => 'superadmin'
],
    function () {
        // Registration Routes...
        Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
        Route::get('{user}/edit', 'Auth\RegisterController@showEditForm')->name('useredit');
        Route::put('update/{user}', 'Auth\RegisterController@update');

        Route::get('list', 'Auth\RegisterController@showUserlist');
        Route::get('roles', 'Auth\RegisterController@showAssignUserRoles');
        Route::post('assignroles', 'Auth\RegisterController@assignUserRoles');

        Route::get('auth', 'User\UserController@adminpanel');
        Route::post('assignroles', 'User\UserController@postAdminAssignRoles');
    });

//Settings Routes
Route::group(['namespace' => 'Backend', 'prefix' => 'settings',
    'middleware' => ['auth', 'acl'],
    'is' => 'superadmin|admin'],
    function () {
        Route::get('category/categoryorder', 'CategoryController@changecategoryorder');
        Route::post('category/changesubcategory', 'CategoryController@changesubcategory');
        Route::post('category/order', 'CategoryController@ordercategory');
        Route::resource('category', 'CategoryController', ['except' => ['create', 'show', 'destroy']]);

        Route::post('subcategory/assignedbrands', 'SubcategoryController@getassignedbrands');
        Route::post('subcategory/assign-brands', 'SubcategoryController@assignbrands');
        Route::resource('subcategory', 'SubcategoryController', ['except' => ['create', 'show', 'destroy']]);

        Route::resource('adtypes', 'AdvertisementtypeController', ['except' => ['create', 'show', 'destroy']]);
        Route::resource('filters', 'CategoryfilterController', ['except' => ['create', 'show', 'destroy']]);

//        Chnange Banner Home Routes
        Route::get('changebanner', 'ChangeBannerController@changebanner');
        Route::post('uploadbanner', 'ChangeBannerController@uploadbanner');
        Route::post('banners/change-status/{id}', 'ChangeBannerController@changeStatus');
        Route::post('banners/delete-image/{id}', 'ChangeBannerController@deleteimage');
    });
Route::post('ads-manage/ad-review/confirmotp', 'Backend\AdsreviewController@confirmotp');
Route::post('ads-manage/ad-review/delete/{delete_ad}', 'Backend\AdsreviewController@deletepostad'); //  this  route not working  inside below
Route::get('ads-manage/delete-all-deleted-ads-with-images', 'Backend\AdsreviewController@deleteAllDeletedAdsWithImages'); //  delete all deletd ads with images

Route::group(['namespace' => 'Backend', 'prefix' => 'playzone',
    'middleware' => ['auth', 'acl'],
    'is' => 'admin'],
    function () {
//    daily questions
        Route::get('daily-questions/list', 'Playzone\PlayzoneController@ListDailyQuestions');
        Route::get('daily-questions/add', 'Playzone\PlayzoneController@AddDailyQuestions');
        Route::post('daily-questions/save', 'Playzone\PlayzoneController@SaveDailyQuestions');
        Route::get('daily-questions/{id}/edit/', 'Playzone\PlayzoneController@EditDailyQuestions');
        Route::patch('daily-questions/{id}/update/', 'Playzone\PlayzoneController@UpdateEditDailyQuestions');
        Route::post('daily-questions/change-status/{id}', 'Playzone\PlayzoneController@ChangeStatusDailyQuestions');
        Route::post('daily-questions/delete/{id}', 'Playzone\PlayzoneController@DeleteDailyQuestions');
//        extra questions
        Route::get('extra-questions/list', 'Playzone\PlayzoneController@ListExtraQuestions');
        Route::get('extra-questions/add', 'Playzone\PlayzoneController@AddExtraQuestions');
        Route::post('extra-questions/save', 'Playzone\PlayzoneController@SaveExtraQuestions');
        Route::get('extra-questions/{id}/edit/', 'Playzone\PlayzoneController@EditExtraQuestions');
        Route::patch('extra-questions/{id}/update/', 'Playzone\PlayzoneController@UpdateEditExtraQuestions');
        Route::post('extra-questions/change-status/{id}', 'Playzone\PlayzoneController@ChangeStatusExtraQuestions');
        Route::post('extra-questions/delete/{id}', 'Playzone\PlayzoneController@DeleteExtraQuestions');

//        prizes
        Route::get('prizes-buy-requests/list/{status}', 'Playzone\PlayzoneController@PrizesList');
        Route::post('prizes-buy-requests/change-status/{id}', 'Playzone\PlayzoneController@PrizesStatusChange');

        Route::get('prizes/list', 'Playzone\PlayzoneController@ListPrizes');
        Route::get('prizes/add', 'Playzone\PlayzoneController@AddPrizes');
        Route::post('prizes/save', 'Playzone\PlayzoneController@SavePrizes');
        Route::get('prizes/{id}/edit/', 'Playzone\PlayzoneController@EditStore');
        Route::patch('prizes/{id}/update/', 'Playzone\PlayzoneController@UpdateEditStore');
        Route::post('store/change-status/{id}', 'Playzone\PlayzoneController@ChangeStatusStore');
        Route::post('store/delete/{id}', 'Playzone\PlayzoneController@DeleteStore');
        Route::post('/store/make-prize/{id}', 'Playzone\PlayzoneController@MakePriceStore');
        Route::get('/players/list', 'Playzone\PlayzoneController@playersList');
//        Route::post('category/changesubcategory', 'CategoryController@changesubcategory');
//        Route::post('category/order', 'CategoryController@ordercategory');
//        Route::resource('category', 'CategoryController', ['except' => ['create', 'show', 'destroy']]);
//
//        Route::post('subcategory/assignedbrands', 'SubcategoryController@getassignedbrands');
//        Route::post('subcategory/assign-brands', 'SubcategoryController@assignbrands');
//        Route::resource('subcategory', 'SubcategoryController', ['except' => ['create', 'show', 'destroy']]);
//
//        Route::resource('adtypes', 'AdvertisementtypeController', ['except' => ['create', 'show', 'destroy']]);
//        Route::resource('filters', 'CategoryfilterController', ['except' => ['create', 'show', 'destroy']]);
    });
//Ads Manager Routes
Route::group(['namespace' => 'Backend', 'prefix' => 'ads-manage',
    'middleware' => ['auth', 'acl'],
    'is' => 'superadmin|admin|moderator',
],
    function () {
        Route::resource('ad-review', 'AdsreviewController', ['only' => ['index', 'edit', 'update']]);
        //edit spam ad from the backend
        Route::get('/spam-ad/details-edit/save', 'AdsreviewController@saveSpamAd');

        Route::get('/publish-ads', 'AdsreviewController@publishads');
        Route::get('/publish-ads/search', 'AdsreviewController@searchads');
        Route::post('/ad-review/{ad_review}', 'AdsreviewController@update');
        Route::post('/ad-review/delete/{ad_review}', 'AdsreviewController@deletepostad');
        Route::get('/spam', 'AdsreviewController@allspamads');
        Route::get('/ad-review/draft-ads', 'AdsreviewController@draftads');
        Route::get('/ad-review/blocked-ads', 'AdsreviewController@blockedads');
        Route::get('/ad-review/deleted-ads', 'AdsreviewController@deletedads');
        Route::get('/ad-review/pending-otp', 'AdsreviewController@pendingotp');
//        assign ads pack
        Route::get('/assign-ads-pack', 'AdsreviewController@assignadspacks');
        Route::get('/assign-ads-pack/view-all', 'AdsreviewController@assignadspacksAll');
        Route::post('/assign-ads-pack/assign', 'AdsreviewController@boostPinToTopAdsPacksReqest');

        //boosting
        //Route::get('/boosting/pin-to-top-ads', 'PintotopController@pintotopads');
        Route::get('/boosting/pin-to-top-pending', 'AdsreviewController@boostpintotoppending');
        Route::post('/sendpintopsms', 'AdsreviewController@sendpintopsms');

        Route::post('/activate/{boosttype}', 'AdsreviewController@activateboostpintotopadspack');
        Route::post('/activate/{boosttype}/{vouchercode}', 'AdsreviewController@activateboostpintotopsingle');
        Route::post('/delete/{boosttype}/{voucher}', 'AdsreviewController@deleteboostpintotopsingle');
        Route::post('postfacebookoffer', 'AdsreviewController@postfacebookoffer');

        Route::get('/boost/pintotop', 'AdsreviewController@boostpintotoplist');
        Route::post('/boost/pintotop/remove/{boostad}', 'AdsreviewController@boostpintotopremove');
        //ignore boost pin to top
        Route::post('/deactivate/{boosttype}/{vouchercode}', 'AdsreviewController@ignoreBoostPintotoRequest');

//        game sms
        Route::get('game/sms', 'AdsreviewController@gameSms'); //view game page
        Route::post('game/sendsms', 'AdsreviewController@sendGameSMS'); //view game page
    });

//Members
Route::group(['prefix' => 'admin',
    'middleware' => ['auth', 'acl'],
    'is' => 'superadmin|admin|moderator'],
    function () {
//        Route::get('dashboard', 'Backend\DashboardController@dashboard');
//        Route::resource('member', 'Member\MemberController', ['only' => ['show', 'edit', 'update']]);
        Route::get('member', 'Member\MemberController@allmemberslist');
        Route::get('member/premium-request', 'Member\MemberController@premiumrequest');
        Route::get('member/premium-list', 'Backend\PremiumMemberBackendController@premiumList');
        Route::post('member/premium/change-period/{id}', 'Backend\PremiumMemberBackendController@premiumChangePeriod');
        Route::get('member/premium/export', 'Backend\PremiumMemberBackendController@premiumListExport');
        Route::Post('member/premium/send-renew-sms', 'Backend\PremiumMemberBackendController@sendRenewSMS');

        Route::post('approvepremiumrequest', 'Member\MemberController@approvepremiumrequest');

        Route::post('deletepremiumrequest', 'Member\MemberController@deletepremiumrequest');

        Route::post('sendsmspremiummebmer', 'Member\MemberController@sendsmspremiummebmer');
        Route::get('member/managemember', 'Member\PremiummanageController@sendmembersms');
        Route::get('member/sendsms', 'Member\SmsController@index');
        Route::post('member/sendmesseage', 'Member\SmsController@sendmesseage');
        Route::post('member/searchmember', 'Member\PremiummanageController@index');
        Route::post('premiummember/deactive', 'Member\PremiummanageController@premiumdeactive');
        Route::post('premiummember/premiumdelete', 'Member\PremiummanageController@premiumdelete');


        /*------------------ Career Controler Routes -----------------------------*/

        Route::get('member/career/edit', 'Member\CareermanageController@careerlist');
        Route::post('member/career/edit/job', 'Member\CareermanageController@careeredit');
        Route::post('member/career/delete/job', 'Member\CareermanageController@careerdelete');
        Route::post('member/career/update', 'Member\CareermanageController@careerupdate');
        Route::get('member/career', 'Member\CareermanageController@index');
        Route::get('member/careerview', 'Member\CareermanageController@careerview');
        Route::post('member/career/addjob', 'Member\CareermanageController@addjob');

        /*------------------ Career Controler Routes -----------------------------*/

        Route::post('sendsmspremiummebmer', 'Member\MemberController@sendsmspremiummebmer');
//        Route::post('sendsmspremiummebmer', 'Member\MemberController@sendsmspremiummebmer');

        //AGENT ROUTES
        Route::post('agent', 'Member\AgentController@index');
        Route::post('agent/assign', 'Member\AgentController@assignagent');
        Route::post('agent/store', 'Member\AgentController@store');
        Route::resource('agent', 'Member\AgentController', ['only' => ['index', 'create', 'edit', 'update']]);
    });
/*-----------------------------------------------------------------------------------------------------------
 * END backend  routes
-----------------------------------------------------------------------------------------------------------*/


/*------------------ Career Controler Routes -----------------------------*/

Route::get('member/career', 'Member\CareermanageController@index');
Route::get('member/careerview', 'Member\CareermanageController@careerview');
Route::post('member/career/addjob', 'Member\CareermanageController@addjob');

/*------------------ Career Controler Routes -----------------------------*/

Route::post('sendsmspremiummebmer', 'Member\MemberController@sendsmspremiummebmer');
//Route::post('sendsmspremiummebmer', 'Member\MemberController@sendsmspremiummebmer');
/*-----------------------------------------------------------------------------------------------------------
 * END backend  routes
-----------------------------------------------------------------------------------------------------------*/
Route::get('/cache/clear-cache', function () {
    $exitCode = Artisan::call('view:clear');
    dd('cleared');
});
Route::get('/cache/configclear', function () {
    $exitCode = Artisan::call('config:clear');
    dd('cleared');
});


/*------------------get ads to blogger-----------------------------*/
Route::get('blog/getadstoblog', 'Home\DefaultsController@getadstoblog');
/*------------------END get ads to blogger-----------------------------*/

//game path

Route::get('game/play', 'Other\GameController@index'); //view game page
Route::post('game/score/{id}', 'Other\GameController@score'); //set score into session
Route::get('game/user-details/{id}', 'Other\GameController@user_details'); //get user details
Route::post('game/save-user-details/{id}', 'Other\GameController@save_user_details'); //save user details


/*--------------Payment--------------------------------*/
Route::get('member/payment', function () {
    return view('member.boosting.payment');
});
Route::get('member/printpdf', 'Member\PintotopController@printpdf');

Route::post('member/payment_detals', 'Member\PintotopController@payment_detals');

Route::get('member/paymentsuccess', function () {
    return view('member.boosting.paymentsuccess');
});

/*---------------------------------------------------------------------------*/


Route::get('game/score-board', 'Other\GameController@score_board'); //view game page
Route::get('game/prizes', 'Other\GameController@prizes'); //view game page
//Route::get('game/view', 'Other\GameController@trackViews'); //view game page

//saleme play zone
Route::group(['middleware' => ['auth', 'acl']], function () {

});
Route::group(['prefix' => 'playzone',
    'middleware' => 'playzone_member_auth'],
    function () {
        //    ---pages---
        Route::get('/home', 'playzone\PlayzoneController@index');
        Route::get('/profile', 'playzone\PlayzoneController@profile');
        Route::get('/rankings', 'playzone\PlayzoneController@ranks');
        Route::get('/prizes', 'playzone\PlayzoneController@prizes');
        Route::get('/store', 'playzone\PlayzoneController@store');
        Route::get('/play', 'playzone\PlayzoneController@playzone');
        Route::get('/daily-question', 'playzone\PlayzoneController@dailyQuestion');
        Route::get('/extra-question', 'playzone\PlayzoneController@extraQuestion');
        Route::get('/facebook-missions', 'playzone\PlayzoneController@facebookmissionslist');
        Route::get('/google-missions', 'playzone\PlayzoneController@googlemissionslist');
        Route::get('/other-social-missions', 'playzone\PlayzoneController@othermissionslist');
        Route::get('/item/{id}', 'playzone\PlayzoneController@itemSingle');
        //    ---END pages---

        Route::post('/mission/savedailypoint', 'playzone\PlayzoneController@collectDailyPoint');
        Route::post('/mission/savedailyquestion', 'playzone\PlayzoneController@collectDailyQuestionPoint');
        Route::post('/mission/saveextraquestion', 'playzone\PlayzoneController@collectExtraQuestionPoint');
        Route::get('/mission/savefacebooklike/{share}', 'playzone\PlayzoneController@collectfacebooklike');
        Route::get('/mission/savefacebooklikeremove', 'playzone\PlayzoneController@collectfacebooklikeremove');
        Route::get('/mission/savegooglelike/{share}', 'playzone\PlayzoneController@collectgooglepoints');
        Route::get('/mission/saveothersocial/{share}', 'playzone\PlayzoneController@collectothersocialpoints');

        Route::post('/prize/redeem/{member_id}/{item_id}', 'playzone\PlayzoneController@redeemPrize'); //redeem prize submit
        //        profile
        Route::post('user-details/save/{id}', 'Playzone\PlayzoneController@saveProfileData');

        //kokis challenge
        Route::get('/games/kokis-challenge/play', 'playzone\PlayzoneController@kokisChallenge');
        Route::get('/games/kokis-challenge/share/{id}', 'playzone\PlayzoneController@share');
        Route::post('/games/kokis-challenge/score/{id}', 'playzone\PlayzoneController@score'); //set score into session
        Route::post('/games/kokis-challenge/save-score/{member}', 'playzone\PlayzoneController@saveScore'); //set score into session
        Route::get('/games/error/{game}/{errorCode}', 'playzone\PlayzoneController@errorPage'); //set score into session

//        2048
        Route::get('/games/cube-champ/play', 'playzone\PlayzoneController@game_2048');
        Route::get('/games/cube-champ/share/{id}', 'playzone\PlayzoneController@game_share_2048');
        Route::post('/games/cube-champ/save-score/{member}', 'playzone\PlayzoneController@saveScore2048'); //set score into session
    });
Route::get('/playzone/start', 'playzone\PlayzoneController@playzoneLanding');
Route::get('/playzone/start/register', 'playzone\PlayzoneController@playzoneRegister');

//saleme play zone

//Sells Agents Routes
Route::group(['namespace' => 'Backend', 'prefix' => 'agents',
    'middleware' => ['auth', 'acl'],
    'is' => 'superadmin|moderator|admin'],
    function (){
        Route::get('addagent', 'AgentController@addagent');
        Route::post('searchmember', 'AgentController@searchmember');
        Route::post('newagent', 'AgentController@newagent');

        Route::get('allagents', 'AgentController@allAgents');


        Route::post('agentsingle', 'AgentController@agentSingle');
        Route::get('makepayment/{id}', 'AgentController@makepayment');


    });

//Sells Agents Routes