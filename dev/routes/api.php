<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('/allads', 'Mobile\MobileAdDataFeed@allads'); //all ads
Route::get('/ad/{slug}', 'Mobile\MobileAdDataFeed@ad_details'); //single ad details
Route::get('/clickcount/{slug}', 'Mobile\MobileAdDataFeed@incrementClickCount'); //increment click count
//premium member details
Route::get('/premiummember/{member_id}', 'Mobile\MobileAdDataFeed@premiumMemberById'); //premium member details by ID
Route::get('/premiummember/gallery/{member_id}', 'Mobile\MobileAdDataFeed@premiumMembergalleryImagesById'); //premium member gallery images by ID
Route::get('/premiummember/ads/{member_id}', 'Mobile\MobileAdDataFeed@premiumMemberAdsById'); //premium member Ads by ID
Route::get('/premiummember/contact/{member_id}', 'Mobile\MobileAdDataFeed@premiumMemberContactsById'); //premium member Ads by ID
Route::get('/premiummember/services/{member_id}', 'Mobile\MobileAdDataFeed@premiumMemberServicesById'); //premium member services by ID
/*Category Filters*/
Route::get('/categories', 'Mobile\CategoryDataFeed@categoriesList'); //all categories
Route::get('/subcategories/{id}', 'Mobile\CategoryDataFeed@subcategoriesListById'); // subcategories by id
Route::get('/districts', 'Mobile\LocationDataFeed@districtsList'); // all districts
Route::get('/cities/{id}', 'Mobile\LocationDataFeed@citiesListById'); //city list by id
Route::get('/filter-by-city/{id}', 'Mobile\MobileAdDataFeed@filterAdsByCity'); //filter ads by city

//user
Route::post('/user-register/{id}', 'Mobile\Auth@memberregister'); //filter ads by city
Route::post('/user-login/{id}', 'Mobile\Auth@memberlogin'); //filter ads by city
Route::post('/send-forget-password-link/', 'Mobile\Auth@reset_password'); //filter ads by city
//Route::post('/send-forget-password-link/{id}', 'Mobile\Auth@sendResetLinkEmailApp'); //filter ads by city