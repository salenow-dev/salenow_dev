-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 28, 2020 at 10:21 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `salenow_dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `sequence` int(3) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `category_code` varchar(5) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `sequence`, `category_name`, `category_code`, `slug`, `icon`, `updated_by`, `created_by`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 'Cars & Vehicle ', 'veh', 'cars-and-vehicle', 'icofont-car-alt-2 cat-ico', NULL, 1, '2020-06-04 17:12:22', NULL, 'active'),
(2, 2, 'Home & living', 'pro', 'home-and-living', 'icofont-home cat-ico', NULL, 1, '2020-06-04 17:24:09', NULL, 'active'),
(3, 3, 'Jobs', 'job', 'jobs', 'icofont-search-job cat-ico', NULL, 1, '2020-06-04 17:24:09', NULL, 'active'),
(4, 4, 'Antiques & collectables', 'antiq', 'antiques-and-collectables', 'icofont-archive cat-ico', NULL, 1, '2020-06-04 17:24:09', NULL, 'active'),
(5, 5, 'Art', 'art', 'art', 'icofont-picture cat-ico', NULL, 1, '2020-06-04 17:24:09', NULL, 'active'),
(6, 6, 'Baby & Children', 'baby', 'baby-and-children', 'icofont-baby-trolley cat-ico', NULL, 1, '2020-06-04 17:24:09', NULL, 'active'),
(7, 7, 'Books', 'books', 'books', 'icofont-book cat-ico', NULL, 1, '2020-06-04 17:24:09', NULL, 'active'),
(8, 8, 'Building & renovation', 'build', 'building-and-renovation', 'icofont-building-alt cat-ico', NULL, 1, '2020-06-04 17:24:09', NULL, 'active'),
(9, 9, 'Business, farming & industry', 'busin', 'business-farming-and-industry', 'icofont-briefcase cat-ico', NULL, 1, '2020-06-04 17:24:10', NULL, 'active'),
(10, 10, 'Clothing & Jewellery', 'Cloth', 'clothing-and-jewellery', 'icofont-jewlery cat-ico', NULL, 1, '2020-06-04 17:24:10', NULL, 'active'),
(11, 11, 'Computer', 'compu', 'computer', 'icofont-laptop-alt cat-ico', NULL, 1, '2020-06-04 17:24:10', NULL, 'active'),
(12, 12, 'Crafts', 'craft', 'crafts', 'icofont-handshake-deal cat-ico', NULL, 1, '2020-06-04 17:24:10', NULL, 'active'),
(13, 13, 'Electronics & photography', 'ele', 'electronics-and-photography', 'icofont-camera-alt cat-ico', NULL, 1, '2020-06-04 17:24:10', NULL, 'active'),
(14, 14, 'Farming', 'farmi', 'farming', 'icofont-tractor cat-ico', NULL, 1, '2020-06-04 17:24:10', NULL, 'active'),
(15, 15, 'Gaming', 'gamin', 'gaming', 'icofont-game-controller cat-ico', NULL, 1, '2020-06-04 17:24:10', NULL, 'active'),
(16, 16, 'Health & beauty', 'healt', 'health-and-beauty', 'icofont-stethoscope-alt cat-ico', NULL, 1, '2020-06-04 17:24:10', NULL, 'active'),
(17, 17, 'Mobile phones', 'mobil', 'mobile-phones', 'icofont-mobile-phone cat-ico', NULL, 1, '2020-06-04 17:24:10', NULL, 'active'),
(18, 18, 'Movies & TV', 'movie', 'movies-and-tv', ' icofont-computer cat-ico', NULL, 1, '2020-06-04 17:24:10', NULL, 'active'),
(19, 19, 'Music & instruments', 'music', 'music-and-instruments', 'icofont-music-note cat-ico', NULL, 1, '2020-06-04 17:24:10', NULL, 'active'),
(20, 20, 'Pets & animals', 'pet', 'pets-and-animals', 'icofont-dog cat-ico', NULL, 1, '2020-06-04 17:24:10', NULL, 'active'),
(21, 21, 'Pottery & glass', 'Potte', 'pottery-and-glass', 'icofont-glass cat-ico', NULL, 1, '2020-06-04 17:24:10', NULL, 'active'),
(22, 22, 'Real estate', 'real-', 'real-estate', 'icofont-breakdown cat-ico', NULL, 1, '2020-06-04 17:25:15', NULL, 'active'),
(23, 23, 'Services', 'servi', 'services', 'icofont-worker cat-ico', NULL, 1, '2020-06-05 04:56:46', NULL, 'active'),
(24, 24, 'Sports', 'sport', 'sports', 'icofont-football cat-ico', NULL, 1, '2020-06-05 04:59:29', NULL, 'active'),
(25, 25, 'Toys & models', 'toys-', 'toys-and-models', 'icofont-toy-horse cat-ico', NULL, 1, '2020-06-05 04:59:29', NULL, 'active'),
(26, 26, 'Travel, events & activities', 'trave', 'travel-events-and-activities', 'icofont-travelling cat-ico', NULL, 1, '2020-06-05 04:59:29', NULL, 'active'),
(27, 27, 'Other', 'other', 'other', 'icofont-box cat-ico', NULL, 1, '2020-06-05 04:59:29', NULL, 'active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `category_code` (`category_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
